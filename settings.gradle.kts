pluginManagement {
    resolutionStrategy {
        eachPlugin {
            when {
                requested.id.id == "kotlinx-serialization" -> useModule("org.jetbrains.kotlin:kotlin-serialization:${requested.version}")
            }
        }
    }
}

plugins {
    id("com.gradle.enterprise") version "3.6.3"
}

gradleEnterprise {
    buildScan {
        termsOfServiceUrl = "https://gradle.com/terms-of-service"
        termsOfServiceAgree = "yes"
    }
}

rootProject.name = "flowstorm"

arrayOf(
    "core/api",
    "core/app",
    "core/lib",
    "studio/api",
    "studio/app",
    "common/lib",
    "common/app",
    "common/client",
    "client/lib",
    "client/app",
    "client/stressor",
    "channel/api",
    "channel/lib",
    "channel/socket",
    "channel/alexa",
    "channel/google",
    "nlp/pipeline",
    "nlp/dm",
    "nlp/skimmer",
    "nlp/normalizer",
    "nlp/profanity",
    "nlp/stt/api",
    "nlp/stt/amazon",
    "nlp/stt/google",
    "nlp/stt/microsoft",
    "nlp/stt/deepgram",
    "nlp/tts/api",
    "nlp/tts/google",
    "nlp/tts/amazon",
    "nlp/tts/microsoft",
    "nlp/tts/lovo",
    "nlp/illusionist/app",
    "nlp/illusionist/lib",
    "nlp/generative/lib",
    "storage/local",
    "storage/google",
    "storage/aws",
    "storage/azure",
    "connector/lib",
    "connector/reddit",
    "connector/mailgun",
).forEach {
    val projectPath = ":${rootProject.name}-${it.replace('/', '-')}"
    include(projectPath)
    project(projectPath).projectDir = file(it)
}
