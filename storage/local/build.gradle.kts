group = "ai.flowstorm.storage"
description = "Flowstorm Storage Local"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-lib"))
}

