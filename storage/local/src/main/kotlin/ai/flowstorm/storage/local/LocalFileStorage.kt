package ai.flowstorm.storage.local

import ai.flowstorm.core.storage.StorageFile
import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.io.WritableFileStream
import ai.flowstorm.io.WritableStream
import ai.flowstorm.util.*
import java.io.*
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.util.*

class LocalFileStorage(private val base: File) : FileStorage {

    private val logger by LoggerDelegate()

    init {
        logger.info("Created with base $base")
    }

    override fun readFile(path: String, output: OutputStream) {
        val file = File(base, path)
        if (!file.exists())
            throw FileStorage.NotFoundException("File $path not found")
        FileInputStream(file).copyTo(output)
    }

    override fun getFile(path: String): StorageFile {
        val file = File(base, path)
        if (file.exists()) {
            val attr = Files.readAttributes(file.toPath(),  BasicFileAttributes::class.java)
            val properties = Properties()
            val propertiesFile = File(base, "$path.properties")
            val contentType = (if (propertiesFile.exists()) {
                properties.load(FileInputStream(propertiesFile))
                properties.remove("contentType") as String
            } else null) ?: FileStorage.defaultContentType
            @Suppress("UNCHECKED_CAST")
            return StorageFile(file.name, file.length(), contentType,
                    attr.creationTime().toMillis(), attr.lastModifiedTime().toMillis(), properties as Map<String, String>)
        } else {
            throw FileStorage.NotFoundException("File $path not found")
        }
    }

    override fun getWritableStream(path: String, contentType: String, meta: List<String>): WritableStream {
        val file = File(base, path)
        val propertiesFile = File(base, "$path.properties")
        val properties = Properties()
        properties.setProperty("contentType", contentType)
        properties.putAll(meta.associate { s -> s.split(":").let { it[0] to it[1] } })
        file.parentFile.mkdirs()
        FileOutputStream(propertiesFile).use { properties.store(it, null) }
        return WritableFileStream(file)
    }

    override fun writeFile(path: String, contentType: String, meta: List<String>, input: InputStream) {
        getWritableStream(path, contentType, meta).use { it.write(input) }
    }

    override fun deleteFile(path: String) = File(base, path).delete()

    override fun listFiles(path: String, recursive: Boolean) =
        when(recursive) {
            true -> File(base, path).walk().toList()
            else -> File(base, path).listFiles().toList()
        }.filter { (!recursive || it.isFile) && !it.path.endsWith(".properties") }
            .map { it.path.replace(base.path + "/", "") }

    override fun toString(): String = "${this::class.simpleName}(base=$base)"
}