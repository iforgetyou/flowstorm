package ai.flowstorm.storage.local

import ai.flowstorm.core.storage.FileStorage
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.ByteArrayOutputStream
import java.io.File
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class LocalFileStorageTest {

    @Test
    fun `test LocalFileStorage`() {

        val storage = LocalFileStorage(File(System.getProperty("java.io.tmpdir"), "storage-test"))

        val path = "test/test1"
        val contentType = "text/plain"
        val content = "Hello world!"
        val metadata = mapOf("prop1" to "first", "prop2" to "second")

        storage.writeFile(path, contentType, metadata.map { "${it.key}:${it.value}" }, content.byteInputStream())

        val fileObject = storage.getFile(path)
        println(fileObject)
        assertEquals(path.substringAfter('/'), fileObject.name)
        assertEquals(content.length, fileObject.size.toInt())
        assertEquals(contentType, fileObject.contentType)
        assertEquals(true, fileObject.createTime > 0)
        assertEquals(true, fileObject.updateTime > 0)
        assertEquals(metadata, fileObject.metadata)

        val buf = ByteArrayOutputStream()
        storage.readFile(path, buf)
        val readContent = String(buf.toByteArray())
        assertEquals(content, readContent)

        assertEquals(true, storage.deleteFile(path))

        assertEquals(false, try {
            storage.getFile(path)
            true
        } catch (e: FileStorage.NotFoundException) {
            false
        })
    }
}