package ai.flowstorm.storage.aws

import ai.flowstorm.common.config.Config
import ai.flowstorm.core.storage.StorageFile
import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.io.WritableStream
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.ByteBuffer
import ai.flowstorm.util.LoggerDelegate
import org.apache.commons.text.StringEscapeUtils
import software.amazon.awssdk.auth.credentials.AwsCredentials
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.core.sync.ResponseTransformer
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.*
import java.io.InputStream
import java.io.OutputStream

class AmazonS3Storage(private val bucket: String, config: Config): FileStorage {

    private val s3 = with(S3Client.builder()) {
        val accessKey = config.getOrNull("aws.access-key")
        val secretKey = config.getOrNull("aws.secret-key")
        if (accessKey != null && secretKey != null)
            credentialsProvider {
                object : AwsCredentials {
                    override fun accessKeyId() = accessKey
                    override fun secretAccessKey() = secretKey
                }
            }
        build()
    }
    private val logger by LoggerDelegate()

    init {
        logger.info("Created with bucket $bucket")
    }

    override fun readFile(path: String, output: OutputStream) {
        s3.getObject(
            GetObjectRequest.builder()
                .bucket(bucket)
                .key(path)
                .build(),
            ResponseTransformer.toOutputStream(output)
        )
    }

    override fun getFile(path: String): StorageFile {
        val objectRequest = GetObjectRequest.builder()
            .bucket(bucket)
            .key(path)
            .build()
        try {
            s3.getObject(objectRequest).use {
                val metadata = it.response().metadata()
                    .mapKeys { StringEscapeUtils.unescapeJava(it.key) }
                    .mapValues { StringEscapeUtils.unescapeJava(it.value) }
                    .toSortedMap(java.lang.String.CASE_INSENSITIVE_ORDER)
                val timeCreated = metadata.remove("time_created")?.toLong() ?: 0L
                return StorageFile(path, it.response().contentLength(), it.response().contentType(),
                    timeCreated, it.response().lastModified().toEpochMilli(), metadata)
            }
        } catch (e: NoSuchKeyException) {
            throw FileStorage.NotFoundException("File $path not found in bucket $bucket")
        }
    }

    override fun getWritableStream(path: String, contentType: String, meta: List<String>) : WritableStream {
        val metadata = meta.map { StringEscapeUtils.escapeJava(it).split(":").let { it[0] to it[1] } }.toMap() +
                mapOf("time_created" to currentTime().toString()) // Different from lastModified even for the first revision!
        // First create a multipart upload and get the upload id
        val createMultipartUploadRequest = CreateMultipartUploadRequest.builder()
            .bucket(bucket)
            .key(path)
            .metadata(metadata)
            .contentType(contentType)
            .build()

        val response = s3.createMultipartUpload(createMultipartUploadRequest)
        val uploadId = response.uploadId()

        return object : WritableStream {

            var partIdx = 1
            val uploadedParts = mutableListOf<CompletedPart>()
            private val blockSize = 5 * 1024 * 1024 // S3 requires each part (except for the last one) to be at least 5 MB
            private val buffer = ByteBuffer(6 * 1024 * 1024) // Larger capacity than the block size to store additional bytes before sending

            private fun uploadPart(block: ByteArray, count: Int) {
                val buffer = java.nio.ByteBuffer.wrap(block, 0, count)
                val uploadPartRequest = UploadPartRequest.builder()
                    .bucket(bucket)
                    .key(path)
                    .uploadId(uploadId)
                    .partNumber(partIdx)
                    .build()
                val etag = s3.uploadPart(uploadPartRequest, RequestBody.fromByteBuffer(buffer)).eTag()
                uploadedParts.add(CompletedPart.builder().partNumber(partIdx).eTag(etag).build())
                partIdx++
            }

            override fun write(data: ByteArray, offset: Int, count: Int) {
                buffer.write(data, offset, count)
                buffer.read(blockSize, false) { block, count ->
                    uploadPart(block, count)
                }
            }

            override fun close() {
                buffer.read(blockSize, true) { block, count ->
                    uploadPart(block, count)
                }
                // Finally, call completeMultipartUpload operation to tell S3 to merge all uploaded
                // parts and finish the multipart operation.
                val completedMultipartUpload = CompletedMultipartUpload.builder()
                    .parts(uploadedParts).build()

                val completeMultipartUploadRequest: CompleteMultipartUploadRequest =
                    CompleteMultipartUploadRequest.builder()
                        .bucket(bucket)
                        .key(path)
                        .uploadId(uploadId)
                        .multipartUpload(completedMultipartUpload)
                        .build()

                s3.completeMultipartUpload(completeMultipartUploadRequest)
            }
        }
    }

    override fun writeFile(path: String, contentType: String, meta: List<String>, input: InputStream) {
        getWritableStream(path, contentType, meta).use { it.write(input) }
    }

    override fun deleteFile(path: String): Boolean {
        val deleteObjectRequest = DeleteObjectRequest.builder()
            .bucket(bucket)
            .key(path)
            .build()

        s3.deleteObject(deleteObjectRequest)
        return true
    }

    override fun listFiles(path: String, recursive: Boolean): List<String> {
        val prefix = if (path.endsWith("/")) path else "$path/"
        val delimiter = if (recursive) "" else "/"
        val listObjects = ListObjectsRequest
            .builder()
            .bucket(bucket)
            .prefix(prefix)
            .delimiter(delimiter)
            .build()
        return s3
            .listObjects(listObjects).let {
                it.commonPrefixes().map { it.prefix() } + it.contents().map { it.key() }
            }

    }

    override fun toString() = "${this::class.simpleName}(bucket=$bucket)"
}
