group = "ai.flowstorm.storage"
description = "Flowstorm Storage AWS"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-lib"))
    implementation("software.amazon.awssdk:s3:2.17.99")
    implementation("org.apache.commons:commons-text:1.9")
}

