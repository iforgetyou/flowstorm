package ai.flowstorm.storage.google

import com.google.cloud.storage.BlobId
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageOptions
import ai.flowstorm.core.storage.StorageFile
import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.io.WritableStream
import ai.flowstorm.util.LoggerDelegate
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.nio.channels.Channels
import java.nio.channels.WritableByteChannel

class GoogleStorage(private val bucket: String): FileStorage {

    companion object {
        const val BUFFER_SIZE = 65536
    }

    private val logger by LoggerDelegate()

    init {
        logger.info("Created with bucket $bucket")
    }

    private val storage = StorageOptions.getDefaultInstance().service // StorageOptions.newBuilder().setProjectId(..).build().service

    override fun readFile(path: String, output: OutputStream) {
        logger.debug("Reading file $path")
        storage.reader(bucket, path).use { reader ->
            val writer: WritableByteChannel = Channels.newChannel(output)
            val buffer: ByteBuffer = ByteBuffer.allocate(BUFFER_SIZE)
            while (reader.read(buffer) > 0) {
                buffer.flip()
                writer.write(buffer)
                buffer.clear()
            }
        }
    }

    override fun getWritableStream(path: String, contentType: String, meta: List<String>): WritableStream {
        logger.debug("Writing file $path of content type $contentType")
        val metadata = meta.associate { it.split(":").let { s -> s[0] to s[1] } }
        val blobId = BlobId.of(bucket, path)
        val blobInfo = BlobInfo.newBuilder(blobId).setContentType(contentType).setMetadata(metadata).build()
        val writer = storage.writer(blobInfo)
        return object : WritableStream {
            override fun write(data: ByteArray, offset: Int, count: Int) {
                val buffer = ByteBuffer.wrap(data, offset, count)
                writer.write(buffer)
            }

            override fun close() = writer.close()
        }
    }

    override fun writeFile(path: String, contentType: String, meta: List<String>, input: InputStream) {
        getWritableStream(path, contentType, meta).use { it.write(input) }
        /*storage.writer(blobInfo).use { writer ->
            val reader: ReadableByteChannel = Channels.newChannel(input)
            val buffer: ByteBuffer = ByteBuffer.allocate(BUFFER_SIZE)
            while (reader.read(buffer) > 0) {
                buffer.flip()
                writer.write(buffer)
                buffer.clear()
            }
        }*/
    }

    override fun deleteFile(path: String): Boolean {
        logger.debug("Deleting file $path")
        return storage.delete(bucket, path)
    }

    override fun listFiles(path: String, recursive: Boolean): List<String> {
        TODO()
    }

    override fun getFile(path: String): StorageFile {
        logger.debug("Getting file $path")
        val blob = storage.get(bucket, path, Storage.BlobGetOption.fields(
                Storage.BlobField.NAME,
                Storage.BlobField.SIZE,
                Storage.BlobField.CONTENT_TYPE,
                Storage.BlobField.TIME_CREATED,
                Storage.BlobField.UPDATED,
                Storage.BlobField.METADATA)
        )?: throw FileStorage.NotFoundException("File $path not found in bucket $bucket")
        return StorageFile(blob.name, blob.size, blob.contentType
                ?: "application/octet-stream", blob.createTime, blob.updateTime, blob.metadata ?: mapOf())
    }

    override fun toString() = "${this::class.simpleName}($bucket)"
}