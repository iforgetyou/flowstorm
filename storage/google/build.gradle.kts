group = "ai.flowstorm.storage"
description = "Flowstorm Storage Google"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-lib"))
    implementation("com.google.cloud:google-cloud-storage:1.101.0")
}

