group = "ai.flowstorm.storage"
description = "Flowstorm Storage Azure"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-lib"))
}

