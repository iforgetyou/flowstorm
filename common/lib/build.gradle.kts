group = "ai.flowstorm.common"
description = "Flowstorm Common Lib"

val kotlinVersion = findProperty("kotlin.version") as String
val kmongoVersion = findProperty("kmongo.version") as String
val jacksonVersion = findProperty("jackson.version") as String
val sentryVersion = findProperty("sentry.version") as String
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
}

kotlin {
    nativeTargets(true)
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
                implementation("io.sentry:sentry:$sentryVersion")
                api("ch.qos.logback:logback-classic:1.2.3")
                api("com.auth0:java-jwt:3.11.0") {
                    exclude("com.fasterxml.jackson.core", "jackson-databind")
                }
                api("com.auth0:jwks-rsa:0.15.0") {
                    exclude("com.fasterxml.jackson.core", "jackson-databind")
                    exclude("commons-codec", "commons-codec")
                }
                api("javax.ws.rs:javax.ws.rs-api:2.0")
                api("io.swagger:swagger-annotations:1.5.22")
                api("org.litote.kmongo:kmongo-id-jackson:$kmongoVersion") {
                    exclude("com.fasterxml.jackson.core", "jackson-databind")
                    exclude("com.fasterxml.jackson.module", "jackson-module-kotlin")
                }
                api("org.glassfish.jersey.media:jersey-media-json-jackson:2.32") {
                    exclude("com.fasterxml.jackson.core", "jackson-core")
                    exclude("com.fasterxml.jackson.core", "jackson-databind")
                    exclude("com.fasterxml.jackson.core", "jackson-annotations")
                    exclude("jakarta.activation", "jakarta.activation-api")
                    exclude("jakarta.xml.bind", "jakarta.xml.bind-api")
                }
                implementation("javax.activation:activation:1.1.1")
                implementation("javax.xml.bind:jaxb-api:2.2.11")

                implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.0")
            }
        }
        val nativeMain by creating {
            dependsOn(commonMain)
        }
        nativeSourceSets(nativeMain, true)
    }
}

