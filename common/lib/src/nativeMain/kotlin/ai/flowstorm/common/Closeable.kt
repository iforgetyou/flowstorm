package ai.flowstorm.common

actual interface Closeable {
    actual fun close()
}