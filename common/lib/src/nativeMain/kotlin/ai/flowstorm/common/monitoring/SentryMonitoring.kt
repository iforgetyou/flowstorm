package ai.flowstorm.common.monitoring

import ai.flowstorm.common.monitoring.performance.PerformanceTransaction

actual class SentryMonitoring actual constructor(
    val dsn: String?,
    val release: String?,
    val environment: String?,
    val tracesSampleRate: Double,
    val isTraceSampling: Boolean
) : AbstractMonitoring() {

    override fun capture(text:String?, e: Throwable?, tags: Map<String, String>) = TODO()
    override fun message(text: String?) = TODO()

    override fun createPerformanceTransaction(
        name: String,
        operation: String,
        user: User?,
        tags: MutableMap<String, String>,
        persistent: Boolean
    ): PerformanceTransaction = TODO()
    override fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction? = TODO()
    override fun removePerformanceTransaction(name: String, operation: String) = TODO()

    override fun setForceLogging(toValue: Boolean) = TODO()
}