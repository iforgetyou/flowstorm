package ai.flowstorm.util

actual open class Queue<E> actual constructor() {

    private val items = mutableListOf<E>()

    actual val size get() = items.size

    actual fun get(index: Int) = items[index]

    actual fun add(element: E) = items.add(element)

    actual fun remove(element: E) = items.remove(element)

    actual fun remove() = items.removeAt(0)

    actual fun clear() = items.clear()
}