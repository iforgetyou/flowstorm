package ai.flowstorm.util

actual interface Logger {
    actual fun getName(): String
    actual fun trace(msg: String)
    actual fun debug(msg: String)
    actual fun info(msg: String)
    actual fun warn(msg: String)
    actual fun error(msg: String)
}