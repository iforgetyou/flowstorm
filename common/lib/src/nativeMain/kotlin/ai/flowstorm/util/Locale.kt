package ai.flowstorm.util

actual class Locale actual constructor(private val language: String, private val country: String) {
    actual fun getLanguage() = language
    actual fun getCountry() = country
}