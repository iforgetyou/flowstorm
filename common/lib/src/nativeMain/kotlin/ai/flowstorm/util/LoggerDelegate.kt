package ai.flowstorm.util

import kotlin.reflect.KProperty

actual class LoggerDelegate actual constructor() {

    class SimpleLogger(private val name: String) : Logger {
        override fun getName() = name
        private fun log(severity: String, msg: String) = println("$severity $name - $msg")
        override fun trace(msg: String) = log("TRACE", msg)
        override fun debug(msg: String) = log("DEBUG", msg)
        override fun info(msg: String) = log("INFO", msg)
        override fun warn(msg: String) = log("WARN", msg)
        override fun error(msg: String) = log("ERROR", msg)
    }

    companion object {
        private val loggers = mutableMapOf<String, Logger>()
        private val defaultLogger = SimpleLogger("<default>")
    }

    actual operator fun getValue(thisRef: Any?, property: KProperty<*>) = thisRef?.let {
        val name = it::class.qualifiedName ?: "<unknown>"
        loggers.getOrPut(name) {
            SimpleLogger(name)
        }
    }?: defaultLogger
}