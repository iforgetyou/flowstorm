package ai.flowstorm.concurrent

//TODO implement coroutines

actual fun sleep(millis: Long) {}

actual inline fun <R> synchronize(lock: Any, block: () -> R): R = block()

actual fun Runnable.launch() {
    run()
}
