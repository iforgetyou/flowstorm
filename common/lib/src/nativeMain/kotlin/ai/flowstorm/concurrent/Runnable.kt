package ai.flowstorm.concurrent

actual fun interface Runnable {
    actual fun run()
}