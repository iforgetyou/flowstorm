package ai.flowstorm.time

actual abstract class ZoneId {
    actual abstract fun getId(): String
}
actual fun zoneId(zoneId: String): ZoneId = ZoneIdImpl(zoneId)