package ai.flowstorm.time

class ZoneIdImpl(private val id: String) : ZoneId() {
    override fun getId() = id
}