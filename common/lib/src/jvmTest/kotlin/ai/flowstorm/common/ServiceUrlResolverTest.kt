package ai.flowstorm.common

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ServiceUrlResolverTest {
    @Test
    fun `local core endpoint`() {
        val url = ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.core, ServiceUrlResolver.RunMode.local)
        assertEquals("http://localhost:8080", url)
    }
}