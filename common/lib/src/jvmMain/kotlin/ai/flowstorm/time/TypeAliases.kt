package ai.flowstorm.time

actual typealias ZoneId = java.time.ZoneId
actual fun zoneId(zoneId: String): ZoneId = ZoneId.of(zoneId)