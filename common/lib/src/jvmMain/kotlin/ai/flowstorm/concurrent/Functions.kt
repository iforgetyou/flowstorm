package ai.flowstorm.concurrent

import kotlin.concurrent.thread

actual fun sleep(millis: Long) = Thread.sleep(millis)
actual inline fun <R> synchronize(lock: Any, block: () -> R) = synchronized(lock, block)
actual fun Runnable.launch() {
    Thread(this).start()
}

fun <T> call(
    timeout: Int = 10000,
    timeCallback: ((Int) -> Unit)? = null,
    block: (() -> T)
): T {
    var error: Throwable? = null
    var result: T? = null
    val thread = thread {
        try {
            result = block()
        } catch (e: Throwable) {
            error = e
        }
    }
    var i = timeout / 5
    var t = 0
    while (i > -200 && result == null && error == null) {
        sleep(5)
        t += 5
        timeCallback?.invoke(t)
        i--
        if (i == 0)
            thread.interrupt() // try to interrupt first, if thread is stuck, we will stop it hard after one more second
    }
    return when {
        error is InterruptedException -> error("Thread interrupted after timeout $timeout milliseconds")
        error != null -> throw error!!
        result == null -> {
            thread.stop()
            error("Thread stopped after timeout $timeout milliseconds")
        }
        else -> result!!
    }
}