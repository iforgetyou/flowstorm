package ai.flowstorm.common

import ai.flowstorm.util.LoggerDelegate

object ServiceUrlResolver {

    enum class Service(val localPort: Int, val serviceName: String, val servicePort: Int) {
        bot(3000, "flowstorm-client", 80),
        core(8080, "flowstorm-core", 8080),
        studio(8089, "flowstorm-studio", 8080),
        illusionist(8090, "flowstorm-illusionist-app", 8080),
        illusionist_training(8095, "flowstorm-illusionist-training", 8080),
        duckling(8096, "flowstorm-duckling", 8000),
        triton(8097, "triton", 8000),
        nrg(8098, "nrg", 8080),
        dialogrpt(8099, "dialogrpt", 8080)
    }
    enum class RunMode { local, container, dist, detect }
    enum class Protocol { http, ws }

    private val logger by LoggerDelegate()
    private val baseDomain = "flowstorm.ai"//AppConfig.get("baseDomain", "flowstorm.ai")

    val detectedRunMode by lazy {
        RunMode.valueOf(System.getenv("RUN_MODE") ?: AppConfig.get("runmode", RunMode.dist.name))
    }

    fun getEndpointUrl(
        service: Service,
        runMode: RunMode = RunMode.detect,
        protocol: Protocol = Protocol.http,
        nameSuffix: String? = AppConfig.nameSuffix,
        domain: String = baseDomain,
        log: Boolean = true
    ): String {
        return (AppConfig.getOrNull("${service.name}.url")?.let { url ->
            url.replaceFirst("http", protocol.name)
        } ?: when (runMode) {
            RunMode.local -> "${protocol}://localhost:${service.localPort}"
            RunMode.container -> "${protocol}://${service.serviceName}:${service.servicePort}"
            RunMode.dist -> "${protocol}s://${service.name}" + (if (nameSuffix != "default" && nameSuffix != null && nameSuffix != "") "-$nameSuffix" else "") + "." + domain
            RunMode.detect -> getEndpointUrl(service, detectedRunMode, protocol, nameSuffix, domain, false)
        }).also {
            if (log)
                logger.info("Resolved URL $it for service ${service.name} (runmode=${runMode.name}, nameSuffix=$nameSuffix)")
        }
    }
}