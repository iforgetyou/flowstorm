package ai.flowstorm.common.annotation

actual typealias Transient = com.fasterxml.jackson.annotation.JsonIgnore
