package ai.flowstorm.common.datetime

import com.fasterxml.jackson.databind.module.SimpleModule
import kotlinx.datetime.Instant

class KotlinXDateTimeModule : SimpleModule() {
    init {
        addSerializer(Instant::class.java, InstantSerializer())
        addDeserializer(Instant::class.java, InstantDeserializer())
    }
}
