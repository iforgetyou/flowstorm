package ai.flowstorm.common

import ai.flowstorm.common.config.Config
import ai.flowstorm.common.config.FileAppConfig
import java.io.Serializable

/**
 * Allow static access
 */
object AppConfig : Serializable, Cloneable, Config by FileAppConfig() {
    val version by lazy {
        get("app.version", get("git.ref", get("git.commit", "unknown")))
    }

    @JvmStatic
    fun main(args: Array<String>) {
        println(this)
    }
}
