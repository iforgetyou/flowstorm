package ai.flowstorm.common.model

actual typealias Id<T> = org.litote.kmongo.Id<T>
actual fun <T> newId() = org.litote.kmongo.newId<T>()