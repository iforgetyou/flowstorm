package ai.flowstorm.common.query

import javax.ws.rs.client.WebTarget

data class Query(
    val limit: Int = LIMIT_DEFAULT,
    val seek_id: String?,
    val filters: List<Filter> = listOf(),
    val sort: Sort? = null,
    val search: String? = null,
) {
    data class Filter(val path: String, val operator: Operator, val value: String)
    data class Sort(val path: String, val dir: SortDirection = SortDirection.Asc)

    enum class Operator {
        eq, ne, gt, gte, lt, lte, `in`, like, regex
    }

    enum class SortDirection {
        Asc, Desc
    }

    companion object {
        const val LIMIT_DEFAULT = 100
    }
}

fun WebTarget.query(query: Query): WebTarget {
    var wt = this.queryParam(query::limit.name, query.limit)
    wt = wt.queryParam(query::seek_id.name, query.seek_id)

    query.sort?.let {
        wt = wt.queryParam("sort[${it.path}]", it.dir.name)
    }

    query.search?.let {
        wt = wt.queryParam("search", it)
    }

    for (f in query.filters) {
        wt = wt.queryParam("${f.path}[${f.operator.name}]", f.value)
    }

    return wt
}