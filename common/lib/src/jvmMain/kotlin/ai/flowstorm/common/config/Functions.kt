package ai.flowstorm.common.config

import ai.flowstorm.common.ObjectUtil.yamlMapper
import java.io.InputStream
import java.util.*

fun Properties.loadYaml(input: InputStream) {
    val yaml = yamlMapper.readValue(input, LinkedHashMap::class.java)
    loadMap(yaml)
}

fun Properties.loadMap(from: LinkedHashMap<*, *>, path: String = "") {
    val prefix = if (path.isNotBlank()) "$path." else ""
    from.forEach { (key, value) ->
        if (value is LinkedHashMap<*, *>) {
            loadMap(value, "$prefix$key")
        } else if (value != null) {
            setProperty("$prefix$key", value.toString())
        }
    }
}