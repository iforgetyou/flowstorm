package ai.flowstorm.common.jackson

import com.fasterxml.jackson.annotation.JsonIgnore
import org.litote.kmongo.Id

interface IgnoreIdMixin {
    @JsonIgnore
    fun get_id(): Id<*>
}
