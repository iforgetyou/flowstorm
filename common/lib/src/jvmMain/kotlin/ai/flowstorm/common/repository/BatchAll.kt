package ai.flowstorm.common.repository


interface BatchAll<E> {
    fun allAsIterable(skip: Int, batchSize: Int, ascending: Boolean): Iterable<E>
}
