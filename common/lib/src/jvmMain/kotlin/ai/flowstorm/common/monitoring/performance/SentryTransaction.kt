package ai.flowstorm.common.monitoring.performance

import ai.flowstorm.common.monitoring.User
import io.sentry.Sentry
import io.sentry.SentryTraceHeader
import io.sentry.SpanStatus
import io.sentry.TransactionContext
import io.sentry.protocol.User as SentryUser

class SentryTransaction(
    override val name: String,
    operation: String,
    override val user: User?,
    tags: MutableMap<String, String>
) : PerformanceTransaction, SentrySpan(null, operation, tags) {

    init {
        Sentry.configureScope {
            if (user != null) {
                it.user = SentryUser().apply {
                    id = user.id
                    username = user.username
                    others = user.others
                }
            }
        }
    }

    override val trace get() = span.toSentryTrace().value

    override fun start(trace: String?) {
       if (trace != null) {
           span = Sentry.startTransaction(TransactionContext.fromSentryTrace(name, operation, SentryTraceHeader(trace)))
           span.status = SpanStatus.OK
       } else {
           start()
       }
    }

    override fun start() {
        span = Sentry.startTransaction(name, operation)
        span.status = SpanStatus.OK
    }
}