package ai.flowstorm.common.config

import ai.flowstorm.util.LoggerDelegate
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.*

/**
 * This class implements application configuration. Configuration files named app(.local).(yaml|properties) can be placed in
 * current working directory or directory specified by system property app.dir or env var APP_DIR and also in classpath
 * (root namespace). Properties defined in config placed in classpath will be overriden by values defined in
 * config taken from filesystem.
 */

class FileAppConfig : Config {

    private val dir = System.getenv("APP_DIR") ?: System.getProperty("app.dir") ?: "."
    private val properties = Properties()
    private val logger by LoggerDelegate()

    init {
        var loaded = false
        try {
            // from resource
            "/app.yaml".let { name ->
                javaClass.getResourceAsStream(name)?.let {
                    logger.info("Loading config resource $name")
                    properties.loadYaml(it)
                    loaded = true
                }
            }
            "/app.properties".let { name ->
                javaClass.getResourceAsStream(name)?.let {
                    logger.info("Loading config resource $name")
                    properties.load(it)
                    loaded = true
                }
            }
            // from files
            listOf("app.yaml", "app.properties", "app.local.yaml", "app.local.properties").forEach { name ->
                File("$dir/$name").let { file ->
                    if (file.exists()) {
                        logger.info("Loading config file ${file.absolutePath}")
                        FileInputStream(file).use {
                            if (name.endsWith(".yaml"))
                                properties.loadYaml(it)
                            else
                                properties.load(it)
                        }
                        loaded = true
                    }
                }
            }
            if (get("config.log", "false") == "true")
                properties.forEach {
                    logger.info("${it.key}=${it.value}")
                }
        } catch (e: FileNotFoundException) {
            if (!loaded)
                logger.warn(e.message)
        } catch (e: Throwable) {
            logger.error("Config load error")
        }
    }

    override fun getOrNull(key: String): String? = System.getProperty(key) ?: properties[key] as? String

    override operator fun set(key: String, value: String) {
        properties[key] = value
    }

    override fun toString() = javaClass.simpleName!!
}