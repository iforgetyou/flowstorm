package ai.flowstorm.util

import java.text.SimpleDateFormat
import java.util.*
import javax.xml.stream.XMLStreamWriter
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties

interface Xml {

    @Target(AnnotationTarget.CLASS, AnnotationTarget.PROPERTY)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Serialize(val using: KClass<out Serializer>)

    @Target(AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Attribute(val name: String, val value: String)

    @Target(AnnotationTarget.PROPERTY)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Property(val ignored: Boolean = false, val flattened: Boolean = false, val wrapped: Boolean = true)

    companion object : Serializer() {

        const val XSL_NAMESPACE_URI = "http://www.w3.org/1999/XSL/Transform"
        const val XSLT_NAMESPACE_URI = "http://xml.apache.org/xslt"

        val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXX").apply {
            timeZone = TimeZone.getTimeZone("CET")
        }

        override fun serialize(writer: XMLStreamWriter, obj: Any, refs: List<Any>) = serialize(writer, obj, refs, obj::class.simpleName!!)

        fun serialize(writer: XMLStreamWriter, obj: Any) = serialize(writer, obj, listOf())
    }

    abstract class Serializer {

        abstract fun serialize(writer: XMLStreamWriter, obj: Any, refs: List<Any>)

        val Any.isPrimitive get() = this is CharSequence || this is Number || this is Boolean || this is Date

        private fun XMLStreamWriter.writeStartElement(name: String, objClass: KClass<Any>) {
            writeStartElement(name)
            objClass.annotations.filterIsInstance<Attribute>().forEach {
                writeAttribute(it.name, it.value)
            }
        }

        fun serialize(writer: XMLStreamWriter, obj: Any, refs: List<Any>, name: String = obj::class.simpleName!!.decapitalize(), flatten: Boolean = false, wrap: Boolean = true, disableObjectSerializer: Boolean = false) {
            val objClass = (obj::class as KClass<Any>)
            val objSerializer = objClass.findAnnotation<Serialize>()?.using?.createInstance()
            val refs = refs + listOf(obj)
            when {
                objSerializer != null && !disableObjectSerializer -> {
                    objSerializer.serialize(writer, obj, refs)
                }
                obj is Collection<*> -> {
                    if (obj.isNotEmpty()) {
                        if (wrap)
                            writer.writeStartElement(name, objClass)
                        obj.forEach { item ->
                            if (item != null)
                                serialize(writer, item, refs, name, flatten)
                        }
                        if (wrap)
                            writer.writeEndElement()
                    }
                }
                flatten || obj.isPrimitive -> {
                    if ((obj is CharSequence && obj.isNotBlank()) || obj !is CharSequence) {
                        writer.writeStartElement(name, objClass)
                        writer.writeCharacters(if (obj is Date) dateFormatter.format(obj) else obj.toString())
                        writer.writeEndElement()
                    }
                }
                else -> {
                    if (wrap)
                        writer.writeStartElement(name, objClass)
                    objClass.memberProperties.forEach { property ->
                        val transientAnnotation = property.findAnnotation<Transient>()
                        if (transientAnnotation == null) {
                            val propertyAnnotation = property.findAnnotation<Property>()
                            val propertySerializer = property.findAnnotation<Serialize>()?.using?.createInstance()
                            if (property.visibility?.name != "PRIVATE" && propertyAnnotation?.ignored != true) {
                                property.get(obj)?.let { propertyValue ->
                                    val propertyClass = propertyValue::class
                                    if (propertySerializer != null) {
                                        propertySerializer.serialize(writer, propertyValue, refs)
                                    } else {
                                        serialize(writer, propertyValue, refs, property.name,
                                                propertyAnnotation?.flattened ?: propertyClass.java.isEnum,
                                                propertyAnnotation?.wrapped ?: true)
                                    }
                                }
                            }
                        }
                    }
                    if (wrap)
                        writer.writeEndElement()
                }
            }
        }

    }

}


