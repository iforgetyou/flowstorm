package ai.flowstorm.util

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.AppenderBase
import ai.flowstorm.time.currentTime
import kotlinx.datetime.Instant
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

class RunLog(name: String = "RunLog", private val id: String? = null) : Log {

    override val entries: MutableList<Log.Entry> = mutableListOf()
    private val context = LoggerContext()
    private val root: ch.qos.logback.classic.Logger = context.getLogger("ROOT")

    override val logger: Logger get() = root

    var level
        get() = root.level!!
        set(value) {
            root.level = value
        }

    init {
        with(root) {
            level = Level.ALL
            addAppender(Appender(name, entries).also { it.context = loggerContext; it.start() })
        }
    }

    fun getLogger(loggerName: String): Logger {
        return context.getLogger(loggerName)
    }

    fun getLogger(clazz: KClass<Any>): Logger {
        return getLogger(clazz.qualifiedName!!)
    }

    inner class Appender(name: String, private val log: MutableList<Log.Entry>) : AppenderBase<ILoggingEvent>() {
        private val systemLogger = LoggerFactory.getLogger(name)
        private val start = currentTime()

        override fun append(e: ILoggingEvent) {
            log.add(
                Log.Entry(
                    Instant.fromEpochMilliseconds(e.timeStamp),
                    (e.timeStamp - start).toFloat() / 1000,
                    Log.Entry.Level.valueOf(e.level.toString()),
                    e.message)
            )

            when (e.level) {
                Level.TRACE -> systemLogger.trace((id?.let { "[$id] "}?:"") + e.formattedMessage)
                Level.DEBUG -> systemLogger.debug((id?.let { "[$id] "}?:"") + e.formattedMessage)
                Level.INFO -> systemLogger.info((id?.let { "[$id] "}?:"") + e.formattedMessage)
                Level.WARN -> systemLogger.warn((id?.let { "[$id] "}?:"") + e.formattedMessage)
                Level.ERROR -> systemLogger.error((id?.let { "[$id] "}?:"") + e.formattedMessage)
            }
        }
    }
}