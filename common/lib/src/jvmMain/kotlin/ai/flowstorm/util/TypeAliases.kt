package ai.flowstorm.util

actual typealias Logger = org.slf4j.Logger
actual typealias Locale = java.util.Locale
actual typealias Queue<E> = java.util.LinkedList<E>

actual fun Any.getResourceBytes(name: String) = javaClass.getResourceAsStream(name).readBytes()

fun Locale.withCountry(): Locale =
    if (country == "")
        when (language) {
            "en" -> Locale.US
            "de" -> Locale("de", "DE")
            "fr" -> Locale("fr", "FR")
            "es" -> Locale("es", "ES")
            "pt" -> Locale("pt", "BR")
            "it" -> Locale("it", "IT")
            else -> error("Missing country for language $language")
        }
    else this

fun Locale.withSupportedCountry(supportedCountries: Map<String, List<String>> = emptyMap()) =
    if (country != null) {
        supportedCountries[language]?.let {
            if (it.contains(country))
                this
            else
                Locale(language, it[0])
        }?: this
    } else {
        withCountry()
    }

