package ai.flowstorm.util

import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

object DataConverter {

    /**
     * Write PCM data as WAV file
     * @param os  Stream to save file to
     * @param pcmdata  8 bit PCMData
     * @param sampleRate  Sample rate - 8000, 16000, etc.
     * @param channels Number of channels - Mono = 1, Stereo = 2, etc..
     * @param sampleSizeInBits Number of bits per sample (16 here)
     * @throws IOException
     */
    @Throws(IOException::class)
    fun pcmToWav(input: InputStream, output: OutputStream, size: Long, sampleRate: Int = 16000, channels: Int = 1, sampleSizeInBits: Int = 16) {
        val header = wavHeader(size, sampleRate, channels, sampleSizeInBits)
        output.write(header, 0, header.size)
        input.copyTo(output)
    }

    fun wavHeader(size: Long, sampleRate: Int = 16000, channels: Int = 1, sampleSizeInBits: Int = 16, encoding: AudioEncoding = AudioEncoding.LINEAR16): ByteArray {
        val header = ByteArray(44)
        //val data = get16BitPcm(pcmdata)

        val totalDataLen = (size + 36)
        val bitrate = (sampleRate * channels * sampleSizeInBits).toLong()

        header[0] = 'R'.code.toByte()
        header[1] = 'I'.code.toByte()
        header[2] = 'F'.code.toByte()
        header[3] = 'F'.code.toByte()
        header[4] = (totalDataLen and 0xff).toByte()
        header[5] = (totalDataLen shr 8 and 0xff).toByte()
        header[6] = (totalDataLen shr 16 and 0xff).toByte()
        header[7] = (totalDataLen shr 24 and 0xff).toByte()
        header[8] = 'W'.code.toByte()
        header[9] = 'A'.code.toByte()
        header[10] = 'V'.code.toByte()
        header[11] = 'E'.code.toByte()
        header[12] = 'f'.code.toByte()
        header[13] = 'm'.code.toByte()
        header[14] = 't'.code.toByte()
        header[15] = ' '.code.toByte()
        header[16] = 16
        header[17] = 0
        header[18] = 0
        header[19] = 0
        header[20] = when (encoding) {
            AudioEncoding.LINEAR16 -> 1
            AudioEncoding.MULAW -> 6
        }
        header[21] = 0
        header[22] = channels.toByte()
        header[23] = 0
        header[24] = (sampleRate and 0xff).toByte()
        header[25] = (sampleRate shr 8 and 0xff).toByte()
        header[26] = (sampleRate shr 16 and 0xff).toByte()
        header[27] = (sampleRate shr 24 and 0xff).toByte()
        header[28] = (bitrate / 8 and 0xff).toByte()
        header[29] = (bitrate / 8 shr 8 and 0xff).toByte()
        header[30] = (bitrate / 8 shr 16 and 0xff).toByte()
        header[31] = (bitrate / 8 shr 24 and 0xff).toByte()
        header[32] = (channels * sampleSizeInBits / 8).toByte()
        header[33] = 0
        header[34] = sampleSizeInBits.toByte()
        header[35] = 0
        header[36] = 'd'.code.toByte()
        header[37] = 'a'.code.toByte()
        header[38] = 't'.code.toByte()
        header[39] = 'a'.code.toByte()
        header[40] = (size and 0xff).toByte()
        header[41] = (size shr 8 and 0xff).toByte()
        header[42] = (size shr 16 and 0xff).toByte()
        header[43] = (size shr 24 and 0xff).toByte()

        return header
    }

    fun valueFromString(name: String, type: String, str: String): Any = when (type) {
        Int::class.simpleName -> str.toInt()
        Long::class.simpleName -> str.toLong()
        Float::class.simpleName -> str.toFloat()
        Double::class.simpleName -> str.toDouble()
        Boolean::class.simpleName -> str.toBoolean()
        String::class.simpleName -> str
        else -> error("Property $name has unsupported type $type")
    }
}
