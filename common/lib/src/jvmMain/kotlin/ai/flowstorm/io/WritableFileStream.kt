package ai.flowstorm.io

import java.io.File
import java.io.FileOutputStream

class WritableFileStream(val file: File) : WritableOutputStream(FileOutputStream(file)) {

    override fun toString() = "${this::class.simpleName}(file=$file)"
}