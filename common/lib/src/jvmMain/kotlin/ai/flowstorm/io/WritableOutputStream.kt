package ai.flowstorm.io

import java.io.OutputStream

open class WritableOutputStream(private val output: OutputStream) : WritableStream {

    override fun write(data: ByteArray, offset: Int, count: Int) = output.write(data, offset, count)
    override fun close() = output.close()
}