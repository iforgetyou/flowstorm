package ai.flowstorm.util

expect interface Logger {
    fun getName(): String
    fun trace(msg: String)
    fun debug(msg: String)
    fun info(msg: String)
    fun warn(msg: String)
    fun error(msg: String)
}