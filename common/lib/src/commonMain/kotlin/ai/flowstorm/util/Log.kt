package ai.flowstorm.util

import kotlinx.datetime.Instant

interface Log {

    data class Entry(val time: Instant, val relativeTime: Float, val level: Level, val text: String) {
        enum class Level { ERROR, WARN, INFO, DEBUG, TRACE }
    }

    val logger: Logger
    val entries: MutableList<Entry>
}