package ai.flowstorm.util

enum class AudioEncoding(val sampleSizeInBits: Int) {

    LINEAR16(16),
    MULAW(8);

    companion object {
        fun valueOf(sampleSizeInBits: Int) = when (sampleSizeInBits) {
            16 -> LINEAR16
            8 -> MULAW
            else -> error("Unsupported sample size in bits: $sampleSizeInBits")
        }
    }
}