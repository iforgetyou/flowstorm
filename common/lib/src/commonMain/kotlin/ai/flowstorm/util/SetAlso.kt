package ai.flowstorm.util

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

class SetAlso<in T, V : P, P>(private var value: V, private val property: KMutableProperty0<P>) :
    ReadWriteProperty<T, V> {

    init {
        this.property.set(value)
    }

    override fun getValue(thisRef: T, property: KProperty<*>): V {
        return value
    }

    override fun setValue(thisRef: T, property: KProperty<*>, v: V) {
        value = v
        this.property.set(v)
    }
}