package ai.flowstorm.util

expect fun Any.getResourceBytes(name: String): ByteArray

inline fun <T> use(receiver: T, block: T.() -> Unit) {
    receiver.block()
}

fun <T> lazyExpiring(expiration: Int, initializer: () -> T) = LazyExpiringDelegate(expiration, initializer)
