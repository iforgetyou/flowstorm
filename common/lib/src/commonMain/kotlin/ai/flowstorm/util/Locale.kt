package ai.flowstorm.util

expect class Locale(language: String, country: String = "") {
    fun getLanguage(): String
    fun getCountry(): String
}

fun locale(language: String, country: String = "") = Locale(language, country)

fun Locale.similar(locale: Locale) =
    getLanguage() == locale.getLanguage() && (getCountry().isBlank() || getCountry() == locale.getCountry())
