package ai.flowstorm.util

expect open class Queue<E>() {
    val size: Int
    fun get(index: Int): E
    fun add(element: E): Boolean
    fun remove(element: E): Boolean
    fun remove(): E
    fun clear()
}