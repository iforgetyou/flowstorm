package ai.flowstorm.util

import ai.flowstorm.concurrent.synchronize
import ai.flowstorm.time.currentTime

class LazyExpiringDelegate<T>(private val expiration: Int, private val initializer: () -> T) : Lazy<T> {

    private var lazy: Lazy<T> = lazy(initializer)
    private var lastTime: Long = 0

    override val value: T
        get() = synchronize(this) {
            val now = currentTime()
            if (lastTime + expiration * 1000 < now) {
                lazy = lazy(initializer)
                lastTime = now
            }
            lazy.value
        }

    override fun isInitialized(): Boolean = lazy.isInitialized()
}
