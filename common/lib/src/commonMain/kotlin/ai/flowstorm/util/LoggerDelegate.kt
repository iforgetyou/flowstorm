package ai.flowstorm.util

import kotlin.reflect.KProperty

expect class LoggerDelegate() {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): Logger
}