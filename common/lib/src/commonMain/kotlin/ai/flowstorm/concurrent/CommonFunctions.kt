package ai.flowstorm.concurrent

import ai.flowstorm.time.currentTime

fun waitUntil(millis: Long = 0, predicate: () -> Boolean) {
    if (!predicate()) {
        val timeout = currentTime() + millis
        while (millis == 0L || currentTime() < timeout) {
            if (predicate())
                return
            sleep(5)
        }
    }
}