package ai.flowstorm.concurrent

expect fun interface Runnable {
    fun run()
}