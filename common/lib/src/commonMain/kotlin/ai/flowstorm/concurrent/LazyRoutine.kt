package ai.flowstorm.concurrent

abstract class LazyRoutine(private val delay: Int) : Runnable {
    var running = false

    override fun run() {
        running = true
        var counter = 0
        while (running && ++counter < delay)
            sleep(1000)
        running = false
        if (counter == delay)
            lazy()
    }

    abstract fun lazy()
}