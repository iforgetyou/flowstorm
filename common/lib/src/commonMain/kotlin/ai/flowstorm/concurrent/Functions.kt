package ai.flowstorm.concurrent

expect fun sleep(millis: Long)
expect inline fun <R> synchronize(lock: Any, block: () -> R): R
expect fun Runnable.launch()
