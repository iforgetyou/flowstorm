package ai.flowstorm.common.transport

enum class DataType(val contentType: String) {
    Text("text/plain"),
    Object("application/json"),
    Binary("application/octet-stream"),
    PCM("audio/basic"),
    WAV("audio/wav"),
    MP3("audio/mpeg")
}