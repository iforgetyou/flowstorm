package ai.flowstorm.common.monitoring

import ai.flowstorm.util.LoggerDelegate

abstract class AbstractMonitoring : Monitoring {

    val logger by LoggerDelegate()

    init {
        logger.info("Monitoring created")
    }
}