package ai.flowstorm.common.monitoring.performance

enum class Result { Processed, Failed }