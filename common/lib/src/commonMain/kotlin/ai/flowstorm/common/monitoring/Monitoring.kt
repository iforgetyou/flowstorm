package ai.flowstorm.common.monitoring

import ai.flowstorm.common.monitoring.performance.PerformanceTransaction

interface Monitoring {

    fun capture(
        text: String?,
        error: Throwable?,
        tags: Map<String, String> = mapOf()
    )

    fun message(
        text: String?,
    )

    fun createPerformanceTransaction(
        name: String,
        operation: String,
        user: User? = null,
        tags: MutableMap<String, String> = mutableMapOf(),
        persistent: Boolean = false
    ) : PerformanceTransaction

    fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction?

    fun removePerformanceTransaction(name: String, operation: String)

    fun setForceLogging(toValue: Boolean)
}