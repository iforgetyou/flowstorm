package ai.flowstorm.common.model

expect fun <T> newId(): Id<T>