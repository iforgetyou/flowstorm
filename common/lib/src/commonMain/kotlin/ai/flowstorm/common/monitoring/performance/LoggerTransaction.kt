package ai.flowstorm.common.monitoring.performance

import ai.flowstorm.common.monitoring.User

class LoggerTransaction(
    override val name: String,
    operation: String,
    override val user: User?,
    tags: MutableMap<String, String>
) : PerformanceTransaction, LoggerSpan(null, operation, tags) {

    override val trace: String? = null

    override fun start(trace: String?) {
        logger.info("[$this] started for $user with tags $tags")
    }
}