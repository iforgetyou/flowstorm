package ai.flowstorm.common.monitoring

import ai.flowstorm.common.monitoring.performance.LoggerTransaction
import ai.flowstorm.common.monitoring.performance.PerformanceTransaction

class LoggerMonitoring : AbstractMonitoring() {

    private var persistentTransactions: MutableMap<Pair<String, String>, PerformanceTransaction> = mutableMapOf()

    override fun capture(text: String?, error: Throwable?, tags: Map<String, String>) {
        println("Monitoring event: ${text?:error?.message?:"Unknown"}")
        tags.forEach {
            println("${it.key} = ${it.value}")
        }
    }

    override fun message(text: String?) {
        println(text)
    }

    override fun createPerformanceTransaction(
        name: String,
        operation: String,
        user: User?,
        tags: MutableMap<String, String>,
        persistent: Boolean
    ): PerformanceTransaction {
        return if (persistent) {
            persistentTransactions[Pair(name, operation)] = LoggerTransaction(name, operation, user, tags)
            getPerformanceTransaction(name, operation)!!
        } else {
            LoggerTransaction(name, operation, user, tags)
        }
    }

    override fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction? {
        return persistentTransactions[Pair(name, operation)]
    }

    override fun removePerformanceTransaction(name: String, operation: String) {
        persistentTransactions.remove(Pair(name, operation))
    }

    override fun setForceLogging(toValue: Boolean) {}
}