package ai.flowstorm.common.transport

class Cookie(val name: String, val value: String) {

    companion object {
        fun from(text: String): Cookie {
            val parts = text.split('=')
            return Cookie(parts[0].trim(), parts[1].substringBefore(';').trim())
        }
    }

    override fun equals(other: Any?) = if (other is Cookie)
        name == other.name
    else
        false

    override fun hashCode() = name.hashCode()

    override fun toString() = "$name=$value"
}
