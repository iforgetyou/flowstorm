package ai.flowstorm.common.transport

interface HttpSocketClientFactory<I : Any, O : Any> {
    fun create(
        listener: HalfDuplexSocketClient.Listener<I, O>,
        method: HttpMethod = HttpMethod.GET,
        context: HttpSocketContext = HttpSocketContext(),
    ): HttpSocketClient<I, O>
}