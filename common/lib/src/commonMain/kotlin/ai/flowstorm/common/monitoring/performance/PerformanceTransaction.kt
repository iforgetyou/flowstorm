package ai.flowstorm.common.monitoring.performance

import ai.flowstorm.common.monitoring.User

interface PerformanceTransaction : PerformanceSpan {
    val name: String
    val user: User?
    val trace: String?
    fun start(trace: String?)

    fun <T> measure(trace: String? = null, callback: ((Result, Long) -> Unit)? = null, block: () -> T): T {
        start(trace)
        return run(callback, block)
    }
}