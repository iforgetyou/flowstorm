package ai.flowstorm.common.monitoring.performance

import ai.flowstorm.time.currentTime

fun <T> measure(callback: (Result, Long) -> Unit, block: () -> T): T {
    val time = currentTime()
    return try {
        block().also {
            callback.invoke(Result.Processed, currentTime() - time)
        }
    } catch (e: Throwable) {
        callback.invoke(Result.Failed, currentTime() - time)
        throw e
    }
}
