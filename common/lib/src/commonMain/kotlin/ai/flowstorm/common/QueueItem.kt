package ai.flowstorm.common

import ai.flowstorm.time.currentTime

data class QueueItem(val value: Any, val time: Long = currentTime())