package ai.flowstorm.common.transport

fun interface HttpSocketClientListener<I : Any, O : Any> {
    fun onChange(socket: HttpSocketClient<I, O>)
}