package ai.flowstorm.common.transport

enum class TransmissionMode { Writing, Reading }