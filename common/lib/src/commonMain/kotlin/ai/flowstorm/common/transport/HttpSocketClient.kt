package ai.flowstorm.common.transport

interface HttpSocketClient<I : Any, O : Any> : HalfDuplexSocketClient<I, O> {

    val method: HttpMethod
    val context: HttpSocketContext
    val statusCode: Int
    fun connect()
    fun disconnect()
}