package ai.flowstorm.common.monitoring

import ai.flowstorm.common.monitoring.performance.BlankSpan
import ai.flowstorm.common.monitoring.performance.PerformanceTransaction

object NoMonitoring : AbstractMonitoring() {

    override fun capture(text: String?, error: Throwable?, tags: Map<String, String>) {}
    override fun message(text: String?) {}

    override fun createPerformanceTransaction(
        name: String,
        operation: String,
        user: User?,
        tags: MutableMap<String, String>,
        persistent: Boolean
    ): PerformanceTransaction =
        object : PerformanceTransaction, BlankSpan(null, operation, tags) {
            override val name = name
            override val user = user
            override val trace: String? = null
            override fun start(trace: String?) {}
        }

    override fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction? = null
    override fun removePerformanceTransaction(name: String, operation: String) {}
    override fun setForceLogging(toValue: Boolean) {}
}