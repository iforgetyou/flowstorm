package ai.flowstorm.common.transport

interface SocketAdapter<I: Any, O : Any> : Socket.Listener<I, O> {
    var socket: Socket<O>
}