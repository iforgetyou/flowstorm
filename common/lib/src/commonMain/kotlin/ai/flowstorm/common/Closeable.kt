package ai.flowstorm.common

expect interface Closeable {
    fun close()
}