package ai.flowstorm.common.monitoring

data class User(
    val id: String,
    val username: String,
    val others: Map<String, String> = mapOf()
)