package ai.flowstorm.common.transport

data class HttpSocketContext(
    val headers: MutableMap<String, String> = mutableMapOf(),
    val cookies: MutableSet<Cookie> = mutableSetOf(),
    val connectTimeout: Int = 5000,
    val readTimeout: Int = 15000,
    val failureTimeout: Int = 1000,
    val failureCount: Int = 5,
)