package ai.flowstorm.common.transport

interface FullDuplexSocketClient<I : Any, O : Any> : SocketClient<I, O> {
    var listener: Socket.Listener<I, O>
}