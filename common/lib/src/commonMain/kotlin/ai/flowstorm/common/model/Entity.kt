package ai.flowstorm.common.model

interface Entity {
    @Suppress("PropertyName")
    val _id: Id<*>
}