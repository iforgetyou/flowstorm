package ai.flowstorm.common.transport

/**
 * Half duplex socket client allows only writing (optional) followed by reading data from it subsequently in form
 * of separated transmissions (e.g. HTTP requests). You can use it for multiple subsequent linear data exchanges however
 * if you need to continuously read and write data in random order, use different full duplex socket implementation
 * (e.g. WebSocket or HttpPollingSocket)
 */
interface HalfDuplexSocketClient<I : Any, O : Any> : SocketClient<I, O> {

    interface Listener<I : Any, O : Any> : Socket.Listener<I, O> {
        fun onWriting(socket: HttpSocketClient<I, O>)
        fun onReading(socket: HttpSocketClient<I, O>)
    }

    val transmissionMode: TransmissionMode
    var listener: Listener<I, O>
    fun read()
}