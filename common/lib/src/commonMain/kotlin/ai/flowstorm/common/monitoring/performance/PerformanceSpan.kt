package ai.flowstorm.common.monitoring.performance

import ai.flowstorm.time.currentTime

interface PerformanceSpan {
    val parent: PerformanceSpan?
    val operation: String
    val tags: MutableMap<String, String>
    val qualifiedName get() = "${if (this is PerformanceTransaction) "$name:" else ""}${parent?.let { "$it/" } ?:""}$operation"
    val children: MutableList<PerformanceSpan>

    fun createChild(operation: String, tags: MutableMap<String, String> = mutableMapOf()): PerformanceSpan
    fun start()
    fun abort()
    fun abort(e: Throwable)
    fun finish()
    fun mark(operation: String, tags: MutableMap<String, String> = mutableMapOf()) =
        with (createChild(operation, tags)) {
            start()
            finish()
        }

    fun <T> run(callback: ((Result, Long) -> Unit)? = null, block: () -> T): T {
        val time = currentTime()
        return try {
            block().also {
                callback?.invoke(Result.Processed, currentTime() - time)
            }
        } catch (e: Throwable) {
            abort(e)
            callback?.invoke(Result.Failed, currentTime() - time)
            throw e
        } finally {
            finish()
        }
    }

    fun <T> measure(callback: ((Result, Long) -> Unit)? = null, block: () -> T): T {
        start()
        return run(callback, block)
    }
}