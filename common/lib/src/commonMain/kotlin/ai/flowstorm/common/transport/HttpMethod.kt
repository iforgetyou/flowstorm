package ai.flowstorm.common.transport

enum class HttpMethod { GET, POST, PUT }