package ai.flowstorm.common.transport

import kotlin.reflect.KClass

interface SocketClient<I : Any, O : Any> : Socket<O> {
    val url: String
    val inputClass: KClass<I>
    val isConnected: Boolean
}