package ai.flowstorm.common.transport

interface Socket<O : Any> {

    interface Listener<I : Any, O : Any> {

        fun onOpen(socket: Socket<O>)

        fun onClosed(socket: Socket<O>)

        fun onObject(socket: Socket<O>, obj: I)

        fun onBinary(socket: Socket<O>, data: ByteArray)

        fun onFailure(socket: Socket<O>, t: Throwable)
    }

    enum class State { New, Open, Closing, Closed, Failed }

    var state: State

    fun open()

    fun close()

    fun write(obj: O)

    fun write(data: ByteArray)
}