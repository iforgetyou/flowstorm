package ai.flowstorm.common.model

data class Message(val text: String)