package ai.flowstorm.time

import kotlinx.datetime.Clock

fun currentTime() = Clock.System.now().toEpochMilliseconds()