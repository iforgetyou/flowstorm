package ai.flowstorm.time

expect abstract class ZoneId {
    abstract fun getId(): String
}
expect fun zoneId(zoneId: String): ZoneId