group = "ai.flowstorm.common"
description = "Flowstorm Common App"

val kmongoVersion = findProperty("kmongo.version")
val jettyVersion = findProperty("jetty.version")
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-common-client"))
    api(project(":flowstorm-connector-mailgun"))
    api("org.glassfish.jersey.core:jersey-server:2.32")
    api("org.eclipse.jetty.websocket:websocket-servlet:$jettyVersion")
    api("org.eclipse.jetty:jetty-server:$jettyVersion")
    api("org.eclipse.jetty:jetty-servlet:$jettyVersion")
    api("org.glassfish.jersey.inject:jersey-hk2:2.32")
    api("org.elasticsearch.client:elasticsearch-rest-high-level-client:7.9.2")
    api("org.litote.kmongo:kmongo:$kmongoVersion") {
        exclude("org.jetbrains.kotlin", "kotlin-stdlib")
        exclude("org.jetbrains.kotlin", "kotlin-reflect")
        exclude("com.fasterxml.jackson.core", "jackson-databind")
        exclude("com.fasterxml.jackson.module", "jackson-module-kotlin")

    }
    api("org.litote.kmongo:kmongo-id:$kmongoVersion") {
        exclude("org.jetbrains.kotlin", "kotlin-stdlib")
        exclude("org.jetbrains.kotlin", "kotlin-reflect")
        exclude("com.fasterxml.jackson.core", "jackson-databind")
        exclude("com.fasterxml.jackson.module", "jackson-module-kotlin")
    }
    api("org.glassfish.jersey.containers:jersey-container-netty-http:2.32")
    implementation("org.glassfish.jersey.containers:jersey-container-servlet-core:2.32")
    implementation("org.glassfish.jersey.containers:jersey-container-servlet:2.32")
    implementation("com.amazonaws:aws-java-sdk-dynamodb:1.12.17")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.0")
}

