package ai.flowstorm.common.repository

import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.UpdateOptions
import org.bson.conversions.Bson
import org.litote.kmongo.*
import ai.flowstorm.common.model.Entity
import ai.flowstorm.common.query.MongoFiltersFactory
import ai.flowstorm.common.query.Query
import com.mongodb.client.AggregateIterable
import org.bson.types.ObjectId
import org.litote.kmongo.id.toId
import org.litote.kmongo.util.KMongoUtil
import javax.inject.Inject
import kotlin.collections.toList
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

open class MongoAbstractEntityRepository<E : Entity>(val model: KClass<E>) : EntityRepository<E>, BatchAll<E> {

    @Inject
    lateinit var database: MongoDatabase

    protected open val collection: MongoCollection<E> by lazy {
        database.getCollection(KMongoUtil.defaultCollectionName(model), model.java)
    }

    override fun all() = collection.find().toList()

    override fun find(id: Id<E>): E? = collection.findOneById(id)

    override fun find(query: Query): List<E> = query(query).run()

    override fun create(entity: E): E {
        collection.insertOne(entity)
        return entity
    }

    override fun update(entity: E, upsert: Boolean): E {
        collection.updateOneById(entity._id, entity, if (upsert) upsert() else UpdateOptions())
        return entity
    }

    override fun remove(id: Id<E>) {
        collection.deleteOneById(id)
    }

    fun updateById(id: Id<E>, entity: E): E {
        collection.updateOneById(id, entity)
        return entity
    }

    private val seekProperty = Entity::_id

    protected fun seekFilters(seekId: Id<E>) = Pipeline(
        listOf(
            match(seekProperty lt seekId),
        )
    )

    protected fun sortSeekFilters(seekId: Id<E>, sortProperty: KProperty1<E, *>, ascending: Boolean): Pipeline {
        val e = collection.findOneById(seekId)!!
        val value: Any? = sortProperty.get(e)
        return Pipeline(
            listOf(
                match(
                    or(
                        if (ascending) sortProperty gt value else sortProperty lt value,
                        and(
                            sortProperty eq value,
                            if (ascending) seekProperty gt seekId else seekProperty lt seekId,
                        )
                    )
                ),
            )
        )
    }

    fun query(query: Query) = Pipeline().apply {
        if (query.sort != null) {
            val sortProperty = model.memberProperties.first { it.name == query.sort!!.path }
            val dir = query.sort!!.dir == Query.SortDirection.Asc
            query.seek_id?.let {
                add(
                    sortSeekFilters(
                        ObjectId(it).toId(),
                        sortProperty,
                        dir
                    )
                )
            }
            add(sort(sortProperty, dir))
        } else {
            query.seek_id?.let {
                add(seekFilters(ObjectId(it).toId()))
            }
            add(sort())
        }
        add(queryFilters(query))
        limit = query.limit
    }

    fun query(query: Query, sortProperty: KProperty1<E, Any>, ascending: Boolean) = Pipeline().apply {
        query.seek_id?.let {
            add(sortSeekFilters(ObjectId(it).toId(), sortProperty, ascending))
        }
        add(sort(sortProperty, ascending))
        add(queryFilters(query))
        limit = query.limit
    }

    protected fun sort() = Pipeline(
        listOf(
            sort(descending(seekProperty))
        )
    )

    protected fun sort(sortProperty: KProperty1<E, *>, ascending: Boolean) = Pipeline(
        listOf(
            sort(if (ascending) ascending(sortProperty, seekProperty) else descending(sortProperty, seekProperty)),
        )
    )

    protected fun queryFilters(query: Query) = Pipeline(
        listOf(
            match(MongoFiltersFactory.createFilter(model, query)),
        )
    )

    inner class Pipeline(list: List<Bson> = listOf()) : MutableList<Bson> by mutableListOf() {
        var limit: Int = 0

        init {
            addAll(list)
        }

        fun match(filters: List<Bson>) = add(match(and(filters)))
        fun add(list: List<Bson>) = addAll(list)

        fun aggregate(): AggregateIterable<E> {
            if (limit > 0) add(limit(limit)) //limit has to be last item in pipeline
            return collection.aggregate(this)
        }

        @Deprecated("Use list method.", ReplaceWith("list()"))
        fun run(): List<E> = list()

        fun list(): List<E> = aggregate().toList()

        fun iterable(): Iterable<E> = aggregate()
    }

    fun pipeline() = Pipeline()

    override fun allAsIterable(
        skip: Int,
        batchSize: Int,
        ascending: Boolean,
    ): Iterable<E> = Pipeline().apply {
        add(sort(if (ascending) ascending(Entity::_id) else descending(Entity::_id)))
        add(skip(skip))
    }.aggregate().batchSize(batchSize)
}
