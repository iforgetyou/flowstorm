package ai.flowstorm.common

interface ServerConfigProvider {

    val serverConfig: ServerConfig
}