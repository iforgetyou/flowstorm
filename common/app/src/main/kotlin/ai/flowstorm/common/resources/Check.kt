package ai.flowstorm.common.resources

data class Check(
    val health: Double,
    val namespace: String?,
    val name: String?,
    val `package`: String?,
    val version: String?,
    val git_ref: String?,
    val git_commit: String?,
    val app_image: String?,
    var hostname: String?
)