package ai.flowstorm.common.mongo.datetime

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.core.util.JsonParserSequence
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import de.undercouch.bson4jackson.BsonConstants
import de.undercouch.bson4jackson.BsonParser
import kotlinx.datetime.Instant
import kotlinx.datetime.toKotlinInstant
import java.util.*

internal class BsonInstantDeserializer : JsonDeserializer<Instant?>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Instant? {
        val parser = if (p is JsonParserSequence) p.delegate() else p
        require(parser is BsonParser)

        return when {
            parser.currentToken == JsonToken.VALUE_EMBEDDED_OBJECT
                    && parser.currentBsonType == BsonConstants.TYPE_DATETIME -> {

                val date = parser.embeddedObject as Date
                date.toInstant().toKotlinInstant()
            }
            parser.currentToken == JsonToken.VALUE_NULL -> null
            else -> throw ctxt!!.handleUnexpectedToken(Date::class.java, parser) as Throwable
        }
    }
}
