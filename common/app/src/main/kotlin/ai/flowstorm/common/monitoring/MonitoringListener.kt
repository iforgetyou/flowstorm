package ai.flowstorm.common.monitoring

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.monitoring.performance.PerformanceTransaction
import ai.flowstorm.util.LoggerDelegate
import org.glassfish.jersey.server.monitoring.ApplicationEvent
import org.glassfish.jersey.server.monitoring.ApplicationEventListener
import org.glassfish.jersey.server.monitoring.RequestEvent
import org.glassfish.jersey.server.monitoring.RequestEventListener
import javax.inject.Inject

class MonitoringListener(
    @ConfigValue("name") val name: String,
    @ConfigValue("monitoring.httpRequest", "false") val httpRequestMonitoring: Boolean = false,
): ApplicationEventListener {

    @Inject lateinit var monitoring: Monitoring

    private val logger by LoggerDelegate()

    override fun onEvent(event: ApplicationEvent) {
        when (event.type) {
            ApplicationEvent.Type.INITIALIZATION_FINISHED ->
                logger.info("${monitoring::class.simpleName} listener initialized")
            ApplicationEvent.Type.DESTROY_FINISHED -> {}
        }
    }

    override fun onRequest(requestEvent: RequestEvent?) = object : RequestEventListener {

        private var performanceTransaction: PerformanceTransaction? = null

        override fun onEvent(event: RequestEvent) {
            when (event.type) {
                RequestEvent.Type.RESOURCE_METHOD_START -> {
                    with (event.uriInfo) {
                        if (httpRequestMonitoring && path != "check" && !path.endsWith("/versions")) {
                            val transactionName = "${matchedResourceMethod.httpMethod} $name:$path"
                            val userPrincipal = event.containerRequest.securityContext.userPrincipal
                            logger.info("$transactionName (user=${userPrincipal?.name})")
                            performanceTransaction =
                                monitoring.createPerformanceTransaction(transactionName, "request",
                                    userPrincipal?.let {
                                        User(userPrincipal.name, userPrincipal.name)
                                    }
                                ).apply {
                                    start()
                                }
                        }
                    }
                }
                RequestEvent.Type.FINISHED ->
                    performanceTransaction?.finish()
            }
        }
    }
}