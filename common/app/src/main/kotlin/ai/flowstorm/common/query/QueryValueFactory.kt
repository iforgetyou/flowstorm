package ai.flowstorm.common.query

import ai.flowstorm.common.query.Query.*
import org.glassfish.hk2.api.Factory
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.core.Context

class QueryValueFactory : Factory<Query> {
    @Context
    lateinit var context: ContainerRequestContext

    companion object {
        const val LIMIT = "limit"
        const val SEEK_ID = "seek_id"
        const val SORT = "sort"
    }

    override fun provide(): Query {
        val filterRegex = "(?<field>[A-Za-z0-9_.]*)\\[(?<operator>\\w*)]".toRegex()
        val sortRegex = "$SORT\\[(?<field>\\w*)]".toRegex()
        val filters = mutableListOf<Filter>()

        var sort: Sort? = null
        var search:String? = null

        for (param in context.uriInfo.queryParameters) {
            when {
                sortRegex.matches(param.key) -> {
                    val field = sortRegex.matchEntire(param.key)!!.groups[1]!!.value
                    if (sort != null) {
                        throw Exception("Multiple sort properties are not supported.")
                    }

                    sort = Sort(field, SortDirection.valueOf(param.value.first()))

                }
                filterRegex.matches(param.key) -> {
                    val field = filterRegex.matchEntire(param.key)!!.groups[1]!!.value
                    val operator = filterRegex.matchEntire(param.key)!!.groups[2]!!.value
                    filters.add(Filter(field, Operator.valueOf(operator), param.value.first()))
                }
                param.key == "search" -> {
                    search = param.value.first()
                }
            }
        }

        return Query(
            limit = context.uriInfo.queryParameters.getFirst(LIMIT)?.toInt() ?: Query.LIMIT_DEFAULT,
            seek_id = context.uriInfo.queryParameters.getFirst(SEEK_ID),
            filters = filters,
            sort = sort,
            search = search,
        )
    }

    override fun dispose(instance: Query) {}
}