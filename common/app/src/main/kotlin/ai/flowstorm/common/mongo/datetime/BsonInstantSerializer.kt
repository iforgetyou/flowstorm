package ai.flowstorm.common.mongo.datetime

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import de.undercouch.bson4jackson.BsonGenerator
import kotlinx.datetime.Instant
import kotlinx.datetime.toJavaInstant
import java.util.*

internal class BsonInstantSerializer : JsonSerializer<Instant?>() {
    override fun serialize(value: Instant?, gen: JsonGenerator, serializers: SerializerProvider) {
        require(gen is BsonGenerator)
        if (value == null) {
            gen.writeNull()
            return
        }

        gen.writeDateTime(Date.from(value.toJavaInstant()))
    }
}
