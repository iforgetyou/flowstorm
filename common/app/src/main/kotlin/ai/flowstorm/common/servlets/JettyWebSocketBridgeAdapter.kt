package ai.flowstorm.common.servlets

import ai.flowstorm.common.ObjectUtil.defaultMapper as mapper
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.common.transport.SocketAdapter
import ai.flowstorm.util.LoggerDelegate
import org.eclipse.jetty.websocket.api.WebSocketAdapter
import java.nio.ByteBuffer
import kotlin.reflect.KClass

class JettyWebSocketBridgeAdapter<I : Any, O : Any> : WebSocketAdapter(), Socket<O> {

    lateinit var socketAdapter: SocketAdapter<I, O>
    lateinit var inputClass: KClass<I>
    private val logger by LoggerDelegate()

    override var state = Socket.State.Open

    override fun open() {}

    override fun close() {
        try {
            session.close()
        } catch (e: Throwable) {
            logger.warn("Cannot close socket", e)
        } finally {
            state = Socket.State.Closed
        }
    }

    override fun write(obj: O) {
        try {
            if (session.isOpen)
                session.remote.sendString(mapper.writeValueAsString(obj))
            else
                logger.warn("Cannot write ${obj::class.simpleName} object to CLOSED socket")
        } catch (e: Throwable) {
            logger.warn("Cannot write ${obj::class.simpleName} object to socket", e)
        }
    }

    override fun write(data: ByteArray) {
        try {
            if (session.isOpen)
                session.remote.sendBytes(ByteBuffer.wrap(data))
            else
                logger.warn("Cannot write ${data.size} byte(s) to CLOSED socket")
        } catch (e: Throwable) {
            logger.warn("Cannot write ${data.size} byte(s) to socket", e)
        }
    }

    override fun onWebSocketText(json: String) {
        socketAdapter.onObject(this, mapper.readValue(json, inputClass.java))
    }

    override fun onWebSocketBinary(payload: ByteArray, offset: Int, length: Int) {
        socketAdapter.onBinary(this, payload.copyOfRange(offset, offset + length))
    }

    override fun onWebSocketClose(statusCode: Int, reason: String?) {
        super.onWebSocketClose(statusCode, reason)
        socketAdapter.onClosed(this)
    }

    override fun onWebSocketError(cause: Throwable) {
        socketAdapter.onFailure(this, cause)
    }
}