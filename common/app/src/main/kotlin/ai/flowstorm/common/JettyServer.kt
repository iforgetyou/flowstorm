package ai.flowstorm.common

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.glassfish.jersey.servlet.ServletContainer
import org.glassfish.jersey.server.ResourceConfig
import org.slf4j.LoggerFactory
import javax.servlet.Servlet

class JettyServer(resourceConfig: ResourceConfig, serviceLocatorProvider: ServiceLocatorProvider? = null, servlets: Map<Class<out Servlet>, String> = emptyMap()) {

    constructor(config: ServerConfig, serviceLocatorProvider: ServiceLocatorProvider?) : this(config.resourceConfig, serviceLocatorProvider, config.servlets)

    init {
        System.getProperty("ai.flowstorm.common.server.logLevel", null)?.let {
            (LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME) as Logger).level = Level.toLevel(it)
        }
        val server = Server(AppConfig.get("server.port", System.getProperty("ai.flowstorm.common.server.port", "8080")).toInt())
        val servlet = ServletHolder(ServletContainer(resourceConfig))
        val context = ServletContextHandler(server, "/")
        context.setAttribute(ServiceLocatorProvider::class.qualifiedName, serviceLocatorProvider)
        context.addServlet(servlet, "/*")
        servlets.forEach { (servletClass, pathSpec) -> context.addServlet(servletClass, pathSpec) }
        server.start()
        server.join()
        Runtime.getRuntime().addShutdownHook(Thread {
            server.destroy()
        })
    }

    companion object {
        fun run(configProvider: ServerConfigProvider, serviceLocatorProvider: ServiceLocatorProvider?) {
            JettyServer(configProvider.serverConfig, serviceLocatorProvider)
        }
    }
}