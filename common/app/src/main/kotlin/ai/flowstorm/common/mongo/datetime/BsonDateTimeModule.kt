package ai.flowstorm.common.mongo.datetime

import com.fasterxml.jackson.databind.module.SimpleModule
import kotlinx.datetime.Instant

class BsonDateTimeModule : SimpleModule() {
    init {
        addSerializer(Instant::class.java, BsonInstantSerializer())
        addDeserializer(Instant::class.java, BsonInstantDeserializer())
    }
}
