package ai.flowstorm.common.servlets

import ai.flowstorm.common.transport.SocketAdapter
import org.eclipse.jetty.websocket.servlet.WebSocketCreator
import org.eclipse.jetty.websocket.servlet.WebSocketServlet
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory
import kotlin.reflect.KClass

abstract class JettyWebSocketServlet : WebSocketServlet() {

    /**
     * Configures web socket adapter of specified class using custom dependency injection.
     */
    fun <I : Any, O : Any> configure(factory: WebSocketServletFactory, inputClass: KClass<I>, socketAdapterFactory: () -> SocketAdapter<I, O>) {
        factory.register(JettyWebSocketBridgeAdapter::class.java)
        val creator = factory.creator
        factory.creator = WebSocketCreator { servletUpgradeRequest, servletUpgradeResponse ->
            val socketAdapter = socketAdapterFactory()
            @Suppress("UNCHECKED_CAST")
            val socketBridge = creator.createWebSocket(servletUpgradeRequest, servletUpgradeResponse) as JettyWebSocketBridgeAdapter<I, O>
            serviceLocatorProvider.serviceLocator.inject(socketAdapter)
            socketAdapter.socket = socketBridge
            socketBridge.socketAdapter = socketAdapter
            socketBridge.inputClass = inputClass
            socketBridge
        }
    }
}