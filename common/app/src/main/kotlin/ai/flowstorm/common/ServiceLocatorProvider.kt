package ai.flowstorm.common

import org.glassfish.hk2.api.ServiceLocator

interface ServiceLocatorProvider {

    val serviceLocator: ServiceLocator
}