package ai.flowstorm.common.servlets

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.QueueItem
import ai.flowstorm.common.transport.DataType
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.common.transport.SocketAdapter
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.LoggerDelegate
import java.io.ByteArrayOutputStream
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.concurrent.thread
import kotlin.reflect.KClass

abstract class HttpPollingServlet<I : Any, O : Any>(
    private val inputClass: KClass<I>,
    private val readTimeout: Long = 5000,
    private val socketTimeout: Long = 15000
) : HttpServlet() {

    inner class PollingSocket(
        val sid: String,
        var listener: Socket.Listener<I, O>
    ) : Socket<O> {

        override var state: Socket.State = Socket.State.New
        var isConnected = false
        var lastTime = currentTime()
        val queue = mutableListOf<QueueItem>()

        init {
            sockets.add(this)
        }

        override fun open() {
            logger.info("$this open")
            isConnected = true
            state = Socket.State.Open
            listener.onOpen(this)
        }

        override fun close() {
            try {
                logger.info("$this close")
                sockets.remove(this)
                queue.clear()
                isConnected = false
                state = Socket.State.Closed
                listener.onClosed(this)
            } catch (e: Throwable) {
                logger.error("$this close error", e)
            }
        }

        override fun write(obj: O) {
            queue += QueueItem(obj)
        }

        override fun write(data: ByteArray) {
            queue += QueueItem(data)
        }

        override fun toString() = "${this::class.simpleName}(sid=$sid)"
    }

    private val logger by LoggerDelegate()
    private val sockets = mutableSetOf<PollingSocket>()

    init {
        thread {
            while (true) {
                sleep(1000)
                closeIdleSockets()
            }
        }
    }

    private fun closeIdleSockets() = sockets.filter { it.lastTime + socketTimeout < currentTime() }.forEach {
        it.close()
    }

    abstract fun createSocketAdapter(sid: String): SocketAdapter<I, O>

    @Synchronized
    private fun HttpServletRequest.socket(): PollingSocket {
        val sid = getParameter("sid") ?: error("Missing sid")
        return sockets.firstOrNull { it.sid == sid } ?: createSocketAdapter(sid).let { adapter ->
            PollingSocket(sid, adapter).also { socket ->
                adapter.socket = socket
                sockets.add(socket)
                socket.open()
            }
        }.apply {
            lastTime = currentTime()
        }
    }

    override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        closeIdleSockets()
        val socket = request.socket()
        var item: QueueItem? = null
        val time = currentTime() + readTimeout
        while (socket.isConnected && item == null && time > currentTime()) {
            sleep(5)
            item = socket.queue.removeFirstOrNull()
        }
        when (val value = item?.value) {
            is ByteArray -> {
                response.contentType = DataType.Binary.contentType
                response.outputStream.write(value)
            }
            is CharSequence -> {
                response.contentType = DataType.Text.contentType
                response.outputStream.write((if (value is String) value else value.toString()).toByteArray())
            }
            else -> {
                response.contentType = DataType.Object.contentType
                ObjectUtil.defaultMapper.writeValue(response.outputStream, value)
            }
        }
    }

    override fun doPost(request: HttpServletRequest, response: HttpServletResponse) {
        val socket = request.socket()
        when (request.contentType) {
            DataType.Object.contentType -> {
                val obj = ObjectUtil.defaultMapper.readValue(request.inputStream, inputClass.java)
                socket.listener.onObject(socket, obj)
            }
            DataType.Text.contentType -> {
                val buf = ByteArrayOutputStream()
                request.inputStream.copyTo(buf)
                val obj = buf.toString()
                if (obj.startsWith('#')) {
                    when (obj) {
                        "#close" -> socket.close()
                        "#ping" -> socket.queue += QueueItem(obj)
                    }
                } else {
                    socket.listener.onObject(socket, obj as I)
                }
            }
            else -> {
                val buf = ByteArrayOutputStream()
                request.inputStream.copyTo(buf)
                socket.listener.onBinary(socket, buf.toByteArray())
            }
        }
    }
}