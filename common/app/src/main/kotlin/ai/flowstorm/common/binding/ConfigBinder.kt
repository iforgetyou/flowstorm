package ai.flowstorm.common.binding

import ai.flowstorm.common.config.Config
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.config.ConfigValueInjectionResolver
import org.glassfish.hk2.api.InjectionResolver
import org.glassfish.hk2.api.TypeLiteral
import org.glassfish.hk2.utilities.binding.AbstractBinder
import javax.inject.Singleton

class ConfigBinder(private val config: Config) : AbstractBinder() {

    override fun configure() {
        bind(config).to(Config::class.java)
        bind(ConfigValueInjectionResolver::class.java)
            .to(object : TypeLiteral<InjectionResolver<ConfigValue>>() {})
            .`in`(Singleton::class.java)
    }
}