package ai.flowstorm.common

import org.glassfish.jersey.logging.LoggingFeature
import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.server.spi.Container
import org.glassfish.jersey.server.spi.ContainerLifecycleListener
import ai.flowstorm.common.binding.ConfigBinder
import ai.flowstorm.common.config.Config
import ai.flowstorm.common.config.FileAppConfig
import ai.flowstorm.util.LoggerDelegate
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.reflect.KClass
import kotlin.reflect.full.superclasses
import ai.flowstorm.common.mongo.datetime.BsonDateTimeModule
import ai.flowstorm.common.mongo.datetime.InstantCodec
import ai.flowstorm.common.monitoring.MonitoringListener
import com.fasterxml.jackson.jaxrs.xml.JacksonXMLProvider
import org.glassfish.hk2.api.ServiceLocator
import org.glassfish.jersey.internal.inject.InjectionManager
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJaxbJsonProvider
import org.litote.kmongo.util.KMongoConfiguration
import org.litote.kmongo.util.ObjectMappingConfiguration

open class JerseyApplication(protected val config: Config) : ResourceConfig(), ServerConfigProvider, ServiceLocatorProvider {

    private lateinit var injectionManager: InjectionManager
    protected val logger by LoggerDelegate()
    private val defaultProvider = JacksonJaxbJsonProvider().apply {
        setMapper(ObjectUtil.defaultMapper)
    }
    private val xmlProvider = JacksonXMLProvider().apply {
        setMapper(ObjectUtil.xmlMapper)
    }
    override val serverConfig get() = ServerConfig(this)
    override val serviceLocator: ServiceLocator get() = injectionManager.getInstance(ServiceLocator::class.java)

    init {
        register(ConfigBinder(config))
        register(MonitoringListener::class.java)

        config.let {
            logger.info("Starting application (version=${it.get("git.ref", "unknown")}, namespace=${it.namespace})")
        }
        registerDefaultPackages()

        register(object : ContainerLifecycleListener {
            override fun onStartup(container: Container) {
                injectionManager = container.applicationHandler.injectionManager
            }

            override fun onReload(container: Container) {
            }

            override fun onShutdown(container: Container) {
            }
        })

        register(defaultProvider)
        register(xmlProvider)

        if ("TRUE" == config["app.logging"])
            register(LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, 10000))

        KMongoConfiguration.registerBsonModule(BsonDateTimeModule())
        ObjectMappingConfiguration.addCustomCodec(InstantCodec())
    }

    // Make register final to avoid calling non final method in constructor
    final override fun register(component: Any?): ResourceConfig = super.register(component)

    /**
     * Automatically register default packages from all child classes
     */
    private fun registerDefaultPackages() {
        var superClasses = this::class.superclasses
        val classes = mutableListOf<KClass<*>>(this::class)

        while (superClasses.isNotEmpty()) {
            superClasses = superClasses.firstOrNull { !it::class.java.isInterface }?.let {
                classes.add(it)
                if (it != JerseyApplication::class) {
                    it.superclasses
                } else null
            } ?: listOf()
        }

        classes.map { it.java.`package`.name }.distinct().forEach {
            packages("$it.filters", "$it.resources")
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) = JettyServer.run(JerseyApplication(FileAppConfig()), null)
    }
}
