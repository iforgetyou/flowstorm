package ai.flowstorm.common.indexer

import ai.flowstorm.common.config.Config
import ai.flowstorm.common.model.Entity
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import org.bson.types.ObjectId
import org.elasticsearch.action.bulk.BulkRequest
import org.elasticsearch.action.bulk.BulkResponse
import org.elasticsearch.action.get.GetRequest
import org.elasticsearch.action.get.GetResponse
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.action.update.UpdateRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.jvnet.hk2.annotations.Optional
import javax.inject.Inject
import kotlin.reflect.KClass

class Elastic {

    @Inject
    lateinit var config: Config

    @Inject
    @Optional
    lateinit var client: RestHighLevelClient

    fun save(jsonWriter: ObjectMapper, entity: Entity, includeDate: Boolean = true) {
        if (!::client.isInitialized) return

        val id = entity._id.toString()
        val index = createIndexName(id, entity::class, includeDate)
        UpdateRequest(index, id).apply {
            doc(jsonWriter.writeValueAsBytes(entity), XContentType.JSON)
            docAsUpsert(true)
            client.update(this, RequestOptions.DEFAULT)
        }
    }

    fun saveAll(jsonWriter: ObjectMapper, entities: List<Entity>, includeDate: Boolean = true): BulkResponse? {
        if (!::client.isInitialized) return null //todo should be non nullable

        val bulk = BulkRequest()

        entities.forEach { entity ->
            val id = entity._id.toString()
            val index = createIndexName(id, entity::class, includeDate)

            UpdateRequest(index, id).apply {
                doc(jsonWriter.writeValueAsBytes(entity), XContentType.JSON)
                docAsUpsert(true)
            }.also {
                bulk.add(it)
            }
        }

        return client.bulk(bulk, RequestOptions.DEFAULT)
    }

    fun getById(entityClass: KClass<out Entity>, id: String, size: Int = 1): GetResponse {
        val index = createIndexName(id, entityClass,true)
        GetRequest(index, id).apply {
            return client.get(this, RequestOptions.DEFAULT)
        }
    }

    fun updateById(entityClass: KClass<out Entity>, id: String, updateString: String) {
        val index = createIndexName(id, entityClass,true)
        UpdateRequest(index, id).doc(updateString, XContentType.JSON).apply {
            client.update(this, RequestOptions.DEFAULT)
        }
    }

    fun matchAllQuery(index: String): SearchResponse{
        SearchRequest(index).apply {
            source(SearchSourceBuilder().query(QueryBuilders.functionScoreQuery(QueryBuilders.matchAllQuery(), ScoreFunctionBuilders.randomFunction())))
            return client.search(this, RequestOptions.DEFAULT)
        }
    }

    fun matchQuery(index: String, queryString: String): SearchResponse{
        SearchRequest(index).apply {
            source(SearchSourceBuilder().query(QueryBuilders.matchQuery("title", queryString)))
            return client.search(this, RequestOptions.DEFAULT)
        }
    }

    private fun createIndexName(id: String, indexClass: KClass<out Entity>, includeDate: Boolean): String {
        val dateSuffix = if (includeDate) {
            val time = Instant.fromEpochSeconds(ObjectId(id).timestamp.toLong()).toLocalDateTime(TimeZone.UTC)

            time.year.toString() + "." + time.monthNumber.toString().padStart(2, '0')
        } else null

        return listOfNotNull(
            indexClass.simpleName!!,
            config.namespace,
            dateSuffix
        ).joinToString("-").lowercase()
    }
}
