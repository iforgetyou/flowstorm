package ai.flowstorm.common.servlets

import ai.flowstorm.common.ServiceLocatorProvider
import javax.servlet.http.HttpServlet

val HttpServlet.serviceLocatorProvider get() =
    servletContext.getAttribute(ServiceLocatorProvider::class.qualifiedName) as ServiceLocatorProvider