package ai.flowstorm.common.client

import ai.flowstorm.common.transport.*
import kotlin.reflect.KClass

class JavaHttpSocketClientFactory<I : Any, O : Any>(
    private val url: String,
    private val inputClass: KClass<I>
) : HttpSocketClientFactory<I, O> {

    override fun create(
        listener: HalfDuplexSocketClient.Listener<I, O>,
        method: HttpMethod,
        context: HttpSocketContext,
    ) = JavaHttpSocketClient(url, inputClass, listener, method, context)
}