package ai.flowstorm.common.client

import ai.flowstorm.common.ObjectUtil.defaultMapper
import ai.flowstorm.common.QueueItem
import ai.flowstorm.common.model.Message
import ai.flowstorm.common.transport.*
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.security.Digest
import ai.flowstorm.time.currentTime
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread
import kotlin.reflect.KClass

class HttpPollingSocketClient<I : Any, O : Any>(
    override val url: String,
    override val inputClass: KClass<I>,
    private val objectMapper: ObjectMapper = defaultMapper,
    private val connectTimeout: Int = 5000,
    private val readTimeout: Int = 5000,
    private val recoveryTimeout: Long = 1000,
    private val failureTimeout: Long = 15000
) : FullDuplexSocketClient<I, O> {

    val sid = Digest.sha1()
    override lateinit var listener: Socket.Listener<I, O>
    override var state: Socket.State = Socket.State.New
    override val isConnected get() = state == Socket.State.Open
    private val queue = mutableListOf<QueueItem>()
    private var errorTime: Long? = null
    private var lastError: Throwable? = null

    private val Any.type get() = when (this) {
        is CharSequence -> DataType.Text
        is ByteArray -> DataType.Binary
        else -> DataType.Object
    }

    private fun doRequest(method: HttpMethod, output: Any? = null) =
        with (URL("$url?sid=$sid").openConnection() as HttpURLConnection) {
            readTimeout = this@HttpPollingSocketClient.readTimeout * 2
            connectTimeout = this@HttpPollingSocketClient.connectTimeout
            requestMethod = method.name
            when (method) {
                HttpMethod.POST -> {
                    doOutput = true
                    setRequestProperty("Content-Type", output?.type?.contentType?: DataType.Object.contentType)
                    when (output) {
                        is ByteArray -> outputStream.use {
                            it.write(output)
                        }
                        is String -> outputStream.use {
                            it.write(output.toByteArray())
                        }
                        else -> OutputStreamWriter(outputStream).use {
                            objectMapper.writeValue(it, output)
                        }
                    }
                    if (responseCode != 200)
                        throw IOException("Server returned unexpected HTTP response code: $responseCode for URL: $url")
                    null
                }
                HttpMethod.GET -> {
                    doOutput = false
                    connect()
                    when (getHeaderField("Content-Type")) {
                        DataType.Binary.contentType -> ByteArrayOutputStream().let {
                            inputStream.copyTo(it)
                            it.toByteArray()
                        }
                        DataType.Text.contentType -> ByteArrayOutputStream().let {
                            inputStream.copyTo(it)
                            it.toString()
                        }
                        else -> {
                            inputStream.use {
                                objectMapper.readValue(it, inputClass.java)
                            }
                        }
                    }
                }
                else -> throw IOException("Unsupported method: $method")
            }
        }.also {
            lastError = null
            errorTime = null
        }

    private fun onError(e: Throwable) {
        lastError = e
        if (errorTime == null)
            errorTime = currentTime()
        if (errorTime!! + failureTimeout < currentTime()) {
            state = Socket.State.Failed
            listener.onFailure(this, e)
        } else {
            sleep(recoveryTimeout)
        }
    }

    override fun open() {
        state = Socket.State.Open
        // receiving
        thread {
            while (isConnected) {
                try {
                    doRequest(HttpMethod.GET)?.let {
                        if (it is ByteArray)
                            listener.onBinary(this, it)
                        else
                            listener.onObject(this, it as I)
                    }
                } catch (e: Throwable) {
                    onError(e)
                }
            }
        }
        // sending
        thread {
            while (isConnected) {
                if (queue.isNotEmpty()) {
                    try {
                        doRequest(HttpMethod.POST, queue[0].value)
                        queue.removeAt(0)
                    } catch (e: Throwable) {
                        onError(e)
                    }
                } else {
                    sleep(5)
                }
            }
        }
        listener.onOpen(this)
    }

    override fun close() {
        state = Socket.State.Closed
        queue.clear()
        try {
            doRequest(HttpMethod.POST, "#close")
        } finally {
            listener.onClosed(this)
        }
    }

    override fun write(obj: O) {
        queue += QueueItem(obj)
    }

    override fun write(data: ByteArray) {
        queue += QueueItem(data)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val socket = HttpPollingSocketClient<Message, Message>("https://zayda.eu.ngrok.io/polling.test", Message::class)
            socket.listener = object : Socket.Listener<Message, Message> {
                override fun onOpen(socket: Socket<Message>) = println("$socket open")
                override fun onClosed(socket: Socket<Message>) = println("$socket closed")
                override fun onObject(socket: Socket<Message>, obj: Message) {
                    println("$socket object $obj")
                }
                override fun onBinary(socket: Socket<Message>, data: ByteArray) = println("$socket binary ${data.toList()}")
                override fun onFailure(socket: Socket<Message>, t: Throwable) {
                    println("$socket failure")
                    t.printStackTrace()
                }
            }
            var i = 0
            socket.open()
            while (i < 7) {
                sleep(2000)
                socket.write(Message("PING ďábelský kůň ${i++}"))
                if (i.rem(5) == 0)
                    socket.write(ByteArray(5))
            }
            socket.close()
        }
    }

    override fun toString() = "${this::class.simpleName}[$url]"
}