package ai.flowstorm.common.client

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.common.transport.FullDuplexSocketClient
import ai.flowstorm.util.LoggerDelegate
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.*
import okhttp3.WebSocket
import okio.ByteString
import okio.ByteString.Companion.toByteString
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit
import kotlin.reflect.KClass

class WebSocketClient<I : Any, O : Any>(
    override val url: String,
    override val inputClass: KClass<I>,
    private val headers: Map<String, String> = mapOf(),
    private val objectMapper: ObjectMapper = ObjectUtil.defaultMapper,
    private val socketPing: Long = 0
) : FullDuplexSocketClient<I, O> {

    override lateinit var listener: Socket.Listener<I, O>
    override val isConnected get() = (state == Socket.State.Open)
    override var state = Socket.State.New
    private val logger by LoggerDelegate()
    private lateinit var socket: WebSocket
    private val socketListener = object : WebSocketListener() {
        override fun onOpen(webSocket: WebSocket, response: Response) {
            state = Socket.State.Open
            listener.onOpen(this@WebSocketClient as Socket<O>)
        }

        override fun onMessage(webSocket: WebSocket, text: String) {
            try {
                objectMapper.readValue(text, inputClass.java)
            } catch (e: Throwable) {
                logger.warn("Cannot read $inputClass from message: $text")
                null
            }?.let {
                logger.debug("Message: $text")
                listener.onObject(this@WebSocketClient as Socket<O>, it)
            }
        }

        override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
            logger.debug("Message ${bytes.size} bytes")
            listener.onBinary(this@WebSocketClient as Socket<O>, bytes.toByteArray())
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
            logger.info("Closing (webSocket=$webSocket, code=$code, reason=$reason)")
            state = Socket.State.Closing
            close()
        }

        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            logger.info("Closed (webSocket=$webSocket, code=$code, reason=$reason)")
            state = Socket.State.Closed
            listener.onClosed(this@WebSocketClient as Socket<O>)
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            logger.error(t.stackTraceToString())
            logger.info("Failure (webSocket=$webSocket, t=$t, response=$response)")
            if (!t.stackTraceToString().contains("writePing")) {
                state = Socket.State.Failed
                listener.onFailure(this@WebSocketClient as Socket<O>, t)
            }
        }
    }

    override fun open() {
        val url = url.replace("http", "ws")
        logger.info("Opening (url=$url)")
        val request = Request.Builder().url(url).apply {
            headers.forEach { (name, value) -> header(name, value) }
        }.build()
        val socketBuilder = OkHttpClient.Builder()
        if (socketPing > 0)
            socketBuilder.pingInterval(socketPing, TimeUnit.SECONDS)
        socket = socketBuilder.build().newWebSocket(request, socketListener)
    }

    override fun close() {
        if (this::socket.isInitialized)
            socket.close(1000, "CLIENT_CLOSE")
    }

    override fun write(obj: O) {
        val text = objectMapper.writeValueAsString(obj)
        logger.debug("Sending $text")
        socket.send(text)
    }

    override fun write(data: ByteArray) {
        logger.debug("Sending ${data.size} byte(s)")
        socket.send((ByteBuffer.wrap(data)).toByteString()
        )
    }

    override fun toString() = "${this::class.simpleName}[$url]"
}