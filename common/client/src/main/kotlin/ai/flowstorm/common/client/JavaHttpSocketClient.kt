package ai.flowstorm.common.client

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.transport.*
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.util.LoggerDelegate
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.internal.wait
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import kotlin.reflect.KClass

class JavaHttpSocketClient<I : Any, O : Any>(
    override val url: String,
    override val inputClass: KClass<I>,
    override var listener: HalfDuplexSocketClient.Listener<I, O>,
    override val method: HttpMethod = HttpMethod.GET,
    override val context: HttpSocketContext = HttpSocketContext(),
    private val objectMapper: ObjectMapper = ObjectUtil.defaultMapper
) : HttpSocketClient<I, O> {

    override var isConnected = false
    override var statusCode = 0
    override var state: Socket.State = Socket.State.New
    override var transmissionMode: TransmissionMode = TransmissionMode.Writing
    private val logger by LoggerDelegate()
    private val jsonFactory = JsonFactory(objectMapper)
    private lateinit var conn: HttpURLConnection

    private fun OutputStream.writeValue(obj: O) {
        val json = objectMapper.writeValueAsBytes(obj)
        write(json)
        write('\n'.code)
        flush()
    }

    override fun open() {
        state = Socket.State.Open
        listener.onOpen(this)
    }

    override fun connect() {
        logger.info("$this connect")
        conn = URL(url).openConnection() as HttpURLConnection
        conn.readTimeout = context.readTimeout
        conn.connectTimeout = context.connectTimeout
        conn.requestMethod = method.name
        if (method != HttpMethod.GET) {
            conn.doOutput = true
            conn.setChunkedStreamingMode(0) // chunked transfer required to stream output data
        }
        context.headers.forEach { (name, value) ->
            conn.setRequestProperty(name, value)
        }
        if (!context.headers.containsKey("Content-Type")) {
            conn.setRequestProperty("Content-Type", DataType.Object.contentType)
        }
        conn.setRequestProperty("Cookie", context.cookies.joinToString("; ") { it.toString() })
        conn.connect()
        isConnected = true
        statusCode = 0
        state = Socket.State.Open
        transmissionMode = TransmissionMode.Writing
        listener.onWriting(this)
    }

    private fun connected(failureCount: Int = context.failureCount, block: (HttpURLConnection.() -> Unit)) {
        for (counter in 1..failureCount) try {
            if (!isConnected)
                connect()
            block(conn)
            break
        } catch (e: Throwable) {
            val message = "$this ${if (isConnected) transmissionMode.name.lowercase() else "connection"} failure $counter of $failureCount"
            if (counter == failureCount) {
                logger.error(message, e)
                disconnect()
                state = Socket.State.Failed
                listener.onFailure(this@JavaHttpSocketClient as Socket<O>, e)
            } else {
                logger.warn(message)
                sleep(context.failureTimeout.toLong())
            }
        }
    }

    override fun write(obj: O) = connected {
        if (transmissionMode != TransmissionMode.Writing) {
            logger.warn("${this@JavaHttpSocketClient} cannot write ${obj::class.simpleName} when reading")
        } else {
            logger.info("${this@JavaHttpSocketClient} writing ${obj::class.simpleName}")
            outputStream.writeValue(obj)
        }
    }

    override fun write(data: ByteArray) = connected {
        if (transmissionMode != TransmissionMode.Writing) {
            logger.warn("${this@JavaHttpSocketClient} cannot write ${data.size} byte(s) when reading")
        } else {
            logger.info("${this@JavaHttpSocketClient} writing ${data.size} byte(s)")
            outputStream.write(data)
        }
    }

    override fun read() = connected(1) { // try to read only once (in the future, server can support re-read of the last turn)
        if (transmissionMode == TransmissionMode.Reading) {
            logger.warn("${this@JavaHttpSocketClient} cannot read when reading")
            return@connected
        }
        logger.info("${this@JavaHttpSocketClient} reading")
        if (doOutput)
            outputStream.close()
        transmissionMode = TransmissionMode.Reading
        statusCode = responseCode
        headerFields["Set-Cookie"]?.forEach {
            context.cookies.add(Cookie.from(it))
        }
        listener.onReading(this@JavaHttpSocketClient)
        var count = 0
        objectMapper.readValues(jsonFactory.createParser(inputStream), inputClass.java)?.forEachRemaining {
            logger.info("${this@JavaHttpSocketClient} read ${it::class.simpleName}")
            listener.onObject(this@JavaHttpSocketClient as Socket<O>, it)
            count++
        }
        inputStream.close()
        this@JavaHttpSocketClient.disconnect()
    }

    override fun disconnect() {
        logger.info("$this disconnect")
        isConnected = false
        conn.disconnect()
    }

    override fun close() {
        state = Socket.State.Closed
        listener.onClosed(this as Socket<O>)
    }

    override fun toString() = "[$method $url]"
}