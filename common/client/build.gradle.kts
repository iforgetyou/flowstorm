group = "ai.flowstorm.common"
description = "Flowstorm Common Client"

val okHttpVersion = findProperty("okhttp.version")

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-common-lib"))
    api("com.squareup.okhttp3:okhttp:$okHttpVersion")
    implementation("org.glassfish.jersey.ext:jersey-proxy-client:2.32")
}

