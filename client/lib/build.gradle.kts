group = "ai.flowstorm.client"
description = "Flowstorm Client Lib"

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
}

kotlin {
    nativeTargets(true)
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":flowstorm-core-lib"))
                api(project(":flowstorm-channel-api"))
            }
        }
        val jvmMain by getting {
            dependencies {
                api(project(":flowstorm-common-client"))
                api("org.apache.tika:tika-core:2.1.0")
                api("org.apache.tika:tika-parser-audiovideo-module:2.1.0")
                api("com.github.goxr3plus:java-stream-player:10.0.2")
                implementation("ai.kitt:snowboy:1.3.0")
            }
        }
        val nativeMain by creating {
            dependsOn(commonMain)
        }
        nativeSourceSets(nativeMain, true)
    }
}

