package ai.flowstorm.client.io

interface StreamCallback {

    fun onStart()

    fun onData(data: ByteArray, size: Int): Boolean

    fun onWake()

    fun onStop()
}