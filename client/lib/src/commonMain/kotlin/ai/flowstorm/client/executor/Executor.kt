package ai.flowstorm.client.executor

interface Executor {
    fun execute()
}