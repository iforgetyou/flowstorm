package ai.flowstorm.client

import ai.flowstorm.common.HttpRequest
import ai.flowstorm.util.Log

interface ClientCallback {

    fun onOpen(client: Client)

    fun onReady(client: Client)

    fun onClose(client: Client)

    fun onError(client: Client, text: String, source: String)

    fun onFailure(client: Client, t: Throwable)

    fun onRecognizedText(client: Client, text: String, isFinal: Boolean)

    fun onSessionId(client: Client, sessionId: String?)

    fun onLog(client: Client, logs: List<Log.Entry>)

    fun onStateChange(client: Client, newState: Client.State)

    fun onWakeWord(client: Client)

    fun audioCancel()

    fun httpRequest(client: Client, url: String, request: HttpRequest? = null): ByteArray?
}