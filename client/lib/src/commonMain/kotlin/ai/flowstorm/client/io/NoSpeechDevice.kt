package ai.flowstorm.client.io

object NoSpeechDevice : SpeechDevice {
    override val isSpeechDetected = false
    override val speechAngle = -1
    override fun close() {}
    override fun toString(): String = this::class.simpleName!!
}