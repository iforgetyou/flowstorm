package ai.flowstorm.client.io

import ai.flowstorm.concurrent.Runnable
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.util.LoggerDelegate

abstract class InputStreamDevice(val speechDevice: SpeechDevice = NoSpeechDevice) : StreamDevice, Runnable {

    private val logger by LoggerDelegate()
    override var isStarted = false
    override var isClosed = false
    private var doClose = false
    override var callback: StreamCallback? = null
        get() = field
        set(value) { field = value }
    private val buffer = ByteArray(65536)

    override fun start() {
        logger.info("Starting")
        isStarted = true
        doClose = false
    }

    override fun stop() {
        logger.info("Stopping")
        isStarted = false
    }

    abstract fun read(buffer: ByteArray): Int

    override fun close(waitFor: Boolean) {
        logger.info("Closing (waitFor=$waitFor)")
        isStarted = false
        doClose = true
        if (waitFor)
            while (!isClosed)
                sleep(50)
    }

    override fun run() {
        logger.info("Running")
        try {
            while (!doClose) {
                if (isStarted) {
                    callback!!.onStart()
                    while (isStarted) {
                        val count = read(buffer)
                        if (count > 0)
                            callback!!.onData(buffer, count)
                    }
                    callback!!.onStop()
                } else {
                    sleep(50)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            stop()
        }
        isClosed = true
    }
}