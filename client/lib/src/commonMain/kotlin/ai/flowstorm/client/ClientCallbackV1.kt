package ai.flowstorm.client

@Deprecated("Deprecated V1 version", replaceWith = ReplaceWith("ClientCallbackV2"))
interface ClientCallbackV1 : ClientCallback {

    fun text(client: Client, text: OutputItem.Text)

    fun texts(client: Client, texts: List<OutputItem.Text>)

    fun audio(client: Client, data: ByteArray, url: String? = null, isSpeech: Boolean = true, handler: PlaybackHandler? = null)

    fun image(client: Client, url: String)

    fun background(client: Client, url: String)

    fun video(client: Client, url: String)

    fun command(client: Client, command: String, code: String?)
}