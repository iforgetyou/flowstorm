package ai.flowstorm.client


fun interface PlaybackHandler {

    fun onPlayback(position: Double, duration: Double)

}