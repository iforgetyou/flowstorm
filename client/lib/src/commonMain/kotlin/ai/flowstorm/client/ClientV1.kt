package ai.flowstorm.client

import ai.flowstorm.channel.ChannelConfig
import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.event.*
import ai.flowstorm.client.io.StreamDevice
import ai.flowstorm.client.io.StreamRecorder
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.common.transport.FullDuplexSocketClient
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.common.monitoring.NoMonitoring
import ai.flowstorm.concurrent.Runnable
import ai.flowstorm.core.*
import ai.flowstorm.concurrent.launch
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.util.Log

@Deprecated("Using deprecated V1 protocol", replaceWith = ReplaceWith("ClientV2"))
class ClientV1(
    config: ClientConfig,
    val socket: FullDuplexSocketClient<ChannelEvent, ChannelEvent>,
    callback: ClientCallback,
    log: Log,
    inputStreamDevice: StreamDevice? = null,
    ttsFileType: AudioFileType = AudioFileType.mp3,
    sttMode: SttMode = SttMode.SingleUtterance,
    pauseMode: Boolean = false,
    inputStreamRecorder: StreamRecorder? = null,
    builtinAudioEnabled: Boolean = true,
    val raiseExceptions: Boolean = false,
    var reopenFailedSocket: Boolean = true,
    monitoring: Monitoring = NoMonitoring
) : Client(config, callback, log, inputStreamDevice, ttsFileType, sttMode, pauseMode, inputStreamRecorder, builtinAudioEnabled, monitoring) {

    inner class SocketWatcher : Runnable {

        override fun run() {
            with (socket) {
                while (state != Socket.State.Closed) {
                    sleep(1000)
                    if (state == Socket.State.Failed && reopenFailedSocket){
                        reopenFailedSocket = false
                        open()
                    }
                }
            }
            log.logger.info("Watcher run end")
        }
    }

    override val inputStreamCloseEvent = InputAudioStreamCloseEvent()

    override fun open() {
        super.open()
        socket.listener = this
        socket.open()
        SocketWatcher().launch()
    }

    override fun close() {
        socket.close()
        super.close()
    }

    override fun onOpen(socket: Socket<ChannelEvent>) {
        reopenFailedSocket = true
        log.logger.info("$socket open")
        state = State.Open
        val channelConfig = ChannelConfig(
            config.locale,
            config.zoneId,
            sttMode,
            sttInterimResults = config.sttInterimResults,
            sendResponseItems = config.sendResponseItems,
            ttsFileType = ttsFileType
        )
        writeEvent(InitEvent(this.config.key, this.config.token, this.config.deviceId, channelConfig))
    }

    override fun onObject(socket: Socket<ChannelEvent>, obj: ChannelEvent) {
        log.logger.debug("Received ${obj::class.simpleName}")
        when (obj) {
            is ReadyEvent -> onReady()
            is SessionStartedEvent -> sessionStart(obj.sessionId)
            is SessionEndedEvent -> sessionEnd()
            is InputAudioStreamOpenEvent -> inputStreamOpen()
            is RecognizedEvent -> recognizedText(obj.text, obj.isFinal)
            else -> super.onObject(socket, obj)
        }
        if (obj is ResponseEvent)
            proceed()
    }

    private fun onReady() {
        previousWorkingState?.let { log.logger.info("Session recovered to state $it") }
        when(previousWorkingState) {
            null, State.Sleeping, State.Open -> {
                callback.onReady(this)
                state = State.Sleeping
                if (config.autoStart && previousWorkingState != State.Sleeping) {
                    intro()
                } else {
                    builtinAudio("${config.locale.getLanguage()}/bot_ready")
                }
            }
            State.Listening -> {
                state = State.Listening
                text("#silence")
            }
            else -> {
                state = previousWorkingState ?: state
            }
        }
        previousWorkingState = null
    }

    override fun onFailure(socket: Socket<ChannelEvent>, t: Throwable) {
        reopenFailedSocket = true
        super.onFailure(socket, t)
    }

    override fun nextTurn() = super.nextTurn().also { withInputStream ->
        if (withInputStream)
            sendInputAudioStreamOpenEvent()
    }

    fun sendInputAudioStreamOpenEvent() =
        writeEvent(InputAudioStreamOpenEvent(config.sessionId, turnPerformanceTransaction?.trace))

    override fun inputStreamOpen() {
        if (!isInputStreamOpen) {
            inputStreamDevice?.start()
        }
        super.inputStreamOpen()
    }


    override fun writeEvent(event: ChannelEvent) {
        if (socket.state != Socket.State.Open) {
            val message = "Socket not open for sending $event"
            if (raiseExceptions)
                error(message)
            log.logger.warn(message)
        } else {
            log.logger.info("Sending $event")
            socket.write(event)
        }
    }

    override fun writeBinary(data: ByteArray) {
        if (socket.state != Socket.State.Open) {
            val message = "Socket not open for sending binary ${data.size} bytes"
            if (raiseExceptions)
                error(message)
            log.logger.warn(message)
        } else {
            socket.write(data)
        }
    }
}
