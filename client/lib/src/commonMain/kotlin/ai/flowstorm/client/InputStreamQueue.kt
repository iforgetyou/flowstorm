package ai.flowstorm.client

import ai.flowstorm.concurrent.Runnable
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.concurrent.synchronize
import ai.flowstorm.util.Queue

internal class InputStreamQueue(val client: Client) : Queue<ByteArray>(), Runnable {

    var isRunning = false

    override fun run() {
        isRunning = true
        while (isRunning) {
            synchronize(this) {
                if (client.isInputStreamOpen && client.state == Client.State.Listening && size > 0)
                    client.writeBinary(remove())
                else
                    sleep(50)
            }
        }
    }

    fun close() {
        isRunning = false
    }
}