package ai.flowstorm.client.io

interface StreamDevice {

    var callback: StreamCallback?
    val isStarted: Boolean
    val isClosed: Boolean

    /**
     * Start audio processing.
     */
    fun start()

    /**
     * Stop audio procesing.
     */
    fun stop()

    /**
     * Close audio device.
     */
    fun close(waitFor: Boolean = false)
}