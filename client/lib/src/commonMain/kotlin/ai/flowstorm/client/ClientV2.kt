package ai.flowstorm.client

import ai.flowstorm.channel.Channel.sessionCookieName
import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.event.*
import ai.flowstorm.client.io.StreamDevice
import ai.flowstorm.client.io.StreamRecorder
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.common.transport.*
import ai.flowstorm.concurrent.Runnable
import ai.flowstorm.concurrent.launch
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.core.ClientConfig
import ai.flowstorm.core.SttMode
import ai.flowstorm.util.Log

class ClientV2(
    config: ClientConfig,
    socketFactory: HttpSocketClientFactory<ChannelEvent, ChannelEvent>,
    callback: ClientCallback,
    log: Log,
    inputStreamDevice: StreamDevice? = null,
    ttsFileType: AudioFileType = AudioFileType.mp3,
    sttMode: SttMode = SttMode.SingleUtterance,
    pauseMode: Boolean = false,
    inputStreamRecorder: StreamRecorder? = null,
    builtinAudioEnabled: Boolean = true,
    monitoring: Monitoring,
) : Client(config, callback, log, inputStreamDevice, ttsFileType, sttMode, pauseMode, inputStreamRecorder, builtinAudioEnabled, monitoring),
    HalfDuplexSocketClient.Listener<ChannelEvent, ChannelEvent> {

    private val socketContext = HttpSocketContext(
        mutableMapOf(
            "Accept-Language" to config.locale.toString().replace('_', '-'),
            "X-Key" to config.key,
            "X-DeviceId" to config.deviceId,
            "X-TimeZone" to config.zoneId.getId(),
            "X-SttMode" to sttMode.name,
            //"X-SttModel" to ?
            //"X-SttSampleRate" to ?
            //"X-SttEncoding" to ?
            "X-TtsFileType" to ttsFileType.name
        ).also {
            if (config.token != null)
                it["Authorization"] = "Bearer ${config.token}"
        }
    ).apply {
        config.sessionId?.let {
            cookies.add(Cookie(sessionCookieName, it))
        }
    }

    private val mainSocket = socketFactory.create(this, HttpMethod.PUT, socketContext)
    private val sideSocket = socketFactory.create(this, HttpMethod.GET, socketContext)
    private val eventSocket = socketFactory.create(this, HttpMethod.POST, socketContext)
    private val streamTurn = Runnable {
        inputStreamOpen()
        sideSocket.read()
        mainSocket.read()
        proceed()
    }

    override fun open() {
        super.open()
        state = State.Sleeping
        if (config.autoStart)
            intro()
    }

    override fun onOpen(socket: Socket<ChannelEvent>) = log.logger.info("$socket open")

    override fun onClosed(socket: Socket<ChannelEvent>) = log.logger.info("$socket closed")

    override fun onWriting(socket: HttpSocketClient<ChannelEvent, ChannelEvent>) = log.logger.info("$socket writing")

    override fun onReading(socket: HttpSocketClient<ChannelEvent, ChannelEvent>) {
        log.logger.info("$socket ${socket.statusCode} reading")
        socketContext.cookies.firstOrNull { it.name == sessionCookieName }?.let {
            if (config.sessionId != it.value)
                sessionStart(it.value)
        }
    }

    override fun sessionEnd() = super.sessionEnd().also {
        socketContext.cookies.clear()
    }

    override fun nextTurn() = super.nextTurn().also { withInputStream ->
        if (withInputStream)
            streamTurn.launch()
    }

    override fun writeEvent(event: ChannelEvent) {
        when (event) {
            is InputStreamCloseEvent, is BinaryEvent -> mainSocket.write(event)
            is InputEvent -> if (mainSocket.isConnected) {
                mainSocket.write(event)
            } else {
                mainSocket.write(event)
                mainSocket.read()
                proceed()
            }
            is LogEvent, is VoteEvent -> {
                mainSocket.write(event)
                mainSocket.read()
            }
            is ClientEvent -> {
                if (!eventSocket.isConnected) {
                    eventSocket.write(event)
                    eventSocket.read()
                } else {
                    log.logger.warn("Cannot write client event due to busy socket: $event")
                }
            }
            else -> log.logger.warn("Unsupported output event type: ${event::class.simpleName}")
        }
    }
}