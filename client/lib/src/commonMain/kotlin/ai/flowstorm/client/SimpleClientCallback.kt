package ai.flowstorm.client

abstract class SimpleClientCallback : ClientCallbackV2 {

    val lastBackgroundUrl: String? = null

    open fun text(text: String) = println(text)

    open fun log(log: String) = text("{$log}")

    override fun onOpen(client: Client) = log("Open")

    override fun onReady(client: Client) = log("Ready")

    override fun onClose(client: Client) = log("Closed")

    override fun onError(client: Client, text: String, source: String) = log("Error from $source: $text")

    override fun onFailure(client: Client, t: Throwable) = log("Failure: ${t.message}")

    override fun onRecognizedText(client: Client, text: String, isFinal: Boolean) = text((if (isFinal) ">" else "~") + " $text")

    override fun onSessionId(client: Client, sessionId: String?) = log("Session $sessionId")

    override fun onStateChange(client: Client, newState: Client.State) = log("${client.state} > $newState")

    override fun onWakeWord(client: Client) = log("Wake word detected")

    override fun text(client: Client, item: OutputItem.Text) = with(item) {
        text("< [$name] $text")
    }

    override fun audio(client: Client, item: OutputItem.Audio) = with(item) {
        log((if (isSpeech) "Speech" else "Audio") + (if (url != null) " [$url]" else ""))
    }

    override fun audioCancel() {}

    override fun background(client: Client, item: OutputItem.Background) = with(item) {
        log("Background [$spec]")
    }

    override fun image(client: Client, item: OutputItem.Image) = with(item) {
        log("Image [$url]")
    }

    override fun video(client: Client, item: OutputItem.Video) = with(item) {
        log("Video [$url]")
    }

    override fun command(client: Client, item: OutputItem.Command) = with(item) {
        log("Command #$command" + if (code != null) "\n$code\n" else "")
    }
}