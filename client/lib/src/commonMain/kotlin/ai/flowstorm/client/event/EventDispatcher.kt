package ai.flowstorm.client.event

import ai.flowstorm.util.LoggerDelegate

class EventDispatcher(
    val events: Array<Event>,
    val listeners: Array<EventListener>
) {
    private val logger by LoggerDelegate()

    private fun getHandlersByEventType(type: Event.Type) = mutableListOf<EventListener>().apply {
        events.filter { it.type == type }.forEach { event ->
            addAll(listeners.filter { it.name == event.listener })
        }
    }

    fun onSpeechBegin()  {
        logger.info("Triggered")
        getHandlersByEventType(Event.Type.SpeechBegin).forEach {
            it.onSpeechBegin()
        }
    }

    fun onSpeechEnd() {
        logger.info("Triggered")
        getHandlersByEventType(Event.Type.SpeechEnd).forEach {
            it.onSpeechEnd()
        }
    }

    fun onCommand(name: String, code: String?) {
        logger.info("Triggered with arguments name=$name, code=$code")
        getHandlersByEventType(Event.Type.Command).forEach {
            it.onCommand(name, code)
        }
    }
}
