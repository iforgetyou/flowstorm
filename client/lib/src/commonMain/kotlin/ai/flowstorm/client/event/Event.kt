package ai.flowstorm.client.event

open class Event(val type: Type, val listener: String) {
    enum class Type { SpeechBegin, SpeechEnd, Command }
}