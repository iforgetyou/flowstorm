package ai.flowstorm.client.io

interface StreamRecorder {

    fun start(name: String)
    fun write(data: ByteArray)
    fun stop()
}