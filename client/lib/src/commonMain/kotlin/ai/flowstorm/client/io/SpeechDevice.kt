package ai.flowstorm.client.io

import ai.flowstorm.common.Closeable

interface SpeechDevice : Closeable {

    val isSpeechDetected: Boolean
    val speechAngle: Int
}