package ai.flowstorm.client

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.event.*
import ai.flowstorm.client.io.StreamCallback
import ai.flowstorm.client.io.StreamDevice
import ai.flowstorm.client.io.StreamRecorder
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.common.monitoring.User
import ai.flowstorm.common.monitoring.performance.Blank
import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.common.monitoring.performance.PerformanceTransaction
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.concurrent.Runnable
import ai.flowstorm.concurrent.launch
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.core.*
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.core.type.beautify
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.Log
import ai.flowstorm.util.getResourceBytes

/**
 * This class implements common client logic w/o communication protocol to the server.
 */
abstract class Client(
    val config: ClientConfig,
    val callback: ClientCallback,
    val log: Log,
    val inputStreamDevice: StreamDevice?,
    val ttsFileType: AudioFileType,
    val sttMode: SttMode,
    private val pauseMode: Boolean,
    internal val inputStreamRecorder: StreamRecorder?,
    private val builtinAudioEnabled: Boolean,
    private val monitoring: Monitoring
) : Socket.Listener<ChannelEvent, ChannelEvent> {

    enum class State { Listening, Processing, Responding, Paused, Sleeping, Open, Closed, Failed, PausedListening, Typing, Tiles }

    enum class SkipMode { None, Single, All }

    var state: State = State.Closed
        set(state) {
            if (turnPerformanceTransaction == null) {
                turnPerformanceTransaction = startPerformanceTransaction()
            }
            statePerformanceSpan?.finish()
            if (field != state) {
                log.logger.info("State changed to $state")
                callback.onStateChange(this, state)
                if (state == State.Processing) {
                    when (sttMode) {
                        SttMode.Continuous, SttMode.Duplex ->
                            nextTurn()
                        SttMode.SingleUtterance, SttMode.Default -> {
                            inputStreamClose(false)
                            callback.audioCancel()
                        }
                    }
                }
                if (state == State.Failed) {
                    inputStreamClose(false)
                }
            }
            statePerformanceSpan = turnPerformanceTransaction?.createChild(state.toString())
            statePerformanceSpan?.start()
            if (state == State.Failed || state == State.Closed || state == State.Sleeping || state == State.Paused) {
                statePerformanceSpan?.finish()
                turnPerformanceTransaction?.finish()
                turnPerformanceTransaction = null
            }
            field = state
        }

    var turnCount = 0

    internal var isInputStreamOpen = false
    protected var sessionEnded = false
    var turnPerformanceTransaction: PerformanceTransaction? = null
    private var statePerformanceSpan: PerformanceSpan? = null
    protected open val inputStreamCloseEvent: ChannelEvent = InputStreamCloseEvent()
    var previousWorkingState: State? = null

    private val inputStreamQueue = InputStreamQueue(this).apply {
        launch()
    }
    private val outputItemQueue = OutputItemQueue(this).apply {
        launch()
    }
    private var builtinAudioData = mutableMapOf<String, ByteArray?>()
    private var sleepLimitTime = 0L

    private var waitingMessage: String = ""
    private var waitingAttributes: PropertyMap = mapOf()

    private fun startPerformanceTransaction(operation: String = "turn") =
        monitoring.createPerformanceTransaction("Client", operation,
            User(config.deviceId, config.deviceId),
            mutableMapOf<String, String>().also {
                it["key"] = config.key
                if (config.sessionId != null)
                    it["sessionId"] = config.sessionId!!
            }
        ).also {
            it.start()
        }

    open fun open() {
        log.logger.info("Open")
        if (state != State.Closed)
            onError("Cannot open client when it is $state")
        if (inputStreamDevice != null) {
            inputStreamDevice.callback = object : StreamCallback {
                override fun onStart() = log.logger.info("Input stream start")
                override fun onStop() = log.logger.info("Input stream stop")
                override fun onData(data: ByteArray, size: Int) = data.copyOf(size).let {
                    inputStreamRecorder?.write(it)
                    if (isInputStreamOpen && state == State.Listening)
                        inputStreamQueue.add(it)
                    else
                        true
                }

                override fun onWake() {
                    callback.onWakeWord(this@Client)
                    touch()
                }
            }
            inputStreamDevice.start()
            if (inputStreamDevice is Runnable)
                inputStreamDevice.launch()
        }
        if (!outputItemQueue.isRunning)
            outputItemQueue.launch()
        if (!inputStreamQueue.isRunning)
            inputStreamQueue.launch()
        callback.onOpen(this)
    }

    open fun close() {
        log.logger.info("Close")
        outputAudioPlayCancel()
        state = State.Closed

        inputStreamRecorder?.stop()
        inputStreamDevice?.close()
        inputStreamQueue.close()
        outputItemQueue.close()
        outputItemQueue.clear()

        callback.onClose(this)
        inputStreamClose(false)
        sessionEnd()
    }

    override fun onClosed(socket: Socket<ChannelEvent>) {
        log.logger.info("$socket closed")
        state = State.Closed
        turnPerformanceTransaction?.finish()
    }

    private fun skipAll() {
        outputItemQueue.isSuspended = true
        if (callback is ClientCallbackV1) {
            callback.texts(this,
                outputItemQueue.getTextItems()
                    .map { (it as OutputItem.Text) }
                    .filter { it.text.isNotEmpty() }
            )
        }

        outputAudioPlayCancel()
    }

    internal fun builtinAudio(name: String, type: OutputItem.Type = OutputItem.Type.Client) {
        if (builtinAudioEnabled)
            outputItemQueue.add(OutputItem.Audio(null, null, builtinAudioData.getOrPut(name) {
                if (name.endsWith("/bot_ready") || name.endsWith("/connection_lost") || name.endsWith("/error"))
                    getResourceBytes("/audio/$name.mp3")
                else
                    callback.httpRequest(this, "https://repository.flowstorm.ai/audio/client/$name.mp3")
            } ?: error("missing builtin audio $name"), type = type))
    }

    protected open fun nextTurn(): Boolean {
        outputItemQueue.isSkipping = false
        state = if (config.hasInputAudio) State.Listening else State.Typing
        return if (waitingMessage.isNotEmpty()) {
            text(waitingMessage, waitingAttributes)
            false
        } else if (inputStreamDevice != null && config.hasInputAudio) {
            if (!isInputStreamOpen) {
                log.logger.info("Input Stream Open")
                inputStreamQueue.clear()
                inputStreamDevice.start()
            }
            true
        } else
            false
    }

    /**
     * Proceed in interaction. Should be called after response has been received from the server.
     * If session has not ended, it will wait for the end of output queue processing and start next turn.
     */
    protected fun proceed() {
        while ((outputItemQueue.hasNext() || outputItemQueue.isPlaying) && listOf(State.Processing, State.Responding, State.Paused).contains(state))
            sleep(20)
        if (sessionEnded || state == State.Failed) {
            sessionEnd()
        } else {
            sendLog()
            if (state == State.Responding)
                nextTurn()
        }
    }

    protected open fun inputStreamOpen() {
        log.logger.info("Input Stream Opened")
        isInputStreamOpen = true
    }

    protected open fun inputStreamClose(sendEvent: Boolean = true) {
        if (inputStreamDevice != null) {
            if (inputStreamDevice.isStarted)
                inputStreamDevice.stop()
            if (isInputStreamOpen) {
                log.logger.info("Input Stream Close")
                if (sendEvent)
                    writeEvent(inputStreamCloseEvent)
            }
            isInputStreamOpen = false
        }
    }

    private fun outputAudioPlayCancel() {
        outputItemQueue.clear(OutputItem.Type.Server)
        callback.audioCancel()
    }

    override fun onBinary(socket: Socket<ChannelEvent>, data: ByteArray) {
        outputItemQueue.add(OutputItem.Audio(null, null, data))
    }

    protected fun sessionStart(sessionId: String) {
        log.logger.info("Session $sessionId started")
        turnCount = 0
        config.sessionId = sessionId
        outputItemQueue.isSuspended = false
        callback.onSessionId(this, config.sessionId)
        inputStreamRecorder?.start(sessionId)
        state = State.Sleeping
    }

    internal open fun sessionEnd() {
        if (state != State.Sleeping && config.sessionId != null) {
            log.logger.info("Session ended when $state")
            if (state != State.Failed)
                sendLog()
            config.sessionId = null
            inputStreamClose(false)
            inputStreamRecorder?.stop()
            callback.onSessionId(this, config.sessionId)
            if (state == State.Failed) {
                builtinAudio("${config.locale.getLanguage()}/connection_lost")
                previousWorkingState = null
                outputItemQueue.clear()
                outputAudioPlayCancel()
            } else {
                state = State.Sleeping
            }
        }
    }

    override fun onObject(socket: Socket<ChannelEvent>, obj: ChannelEvent) {
        log.logger.debug("Received ${obj::class.simpleName}")
        when (obj) {
            is RecognizedInputEvent -> recognizedText(obj.input.transcript.text.beautify(), obj.isFinal)
            is ResponseItemEvent -> onResponseItem(obj.responseItem)
            is ResponseEvent -> onResponse(obj.response)
            is ErrorEvent -> onError(obj.text, obj.source)
            is PingEvent -> log.logger.info("Ping ${obj.text}")
            else -> log.logger.warn("Unsupported event type: ${obj::class.simpleName}")
        }
    }

    protected fun recognizedText(text: String, isFinal: Boolean) {
        log.logger.info("Recognized " + (if (isFinal) "final" else "interim") + " text: $text")
        callback.onRecognizedText(this@Client, text, isFinal)
        if (isFinal) {
            state = State.Processing
            if (sttMode != SttMode.Continuous)
                inputStreamClose(false)
            callback.audioCancel()
            turnPerformanceTransaction?.mark("recognized",
                mutableMapOf(
                    "text" to text,
                    "isFinal" to isFinal.toString()
                )
            )
        }
    }

    private fun onResponseItem(item: Response.Item) {
        log.logger.debug("on$item")
        //state = State.Responding
        with (outputItemQueue) {
            isSuspended = true
            try {
                log.logger.debug(item.toString())
                var commandAdded = false
                val text = (item.text ?: "").replace(Regex("\\#([a-zA-Z_]+)")) {
                    val command = it.groupValues[1]
                    add(OutputItem.Command(item.turnId, item.dialogueNodeId, command, item.code))
                    commandAdded = true
                    ""
                }.trim()
                if (!commandAdded && item.code != null) {
                    add(OutputItem.Command(item.turnId, item.dialogueNodeId, "item", item.code))
                }
                add(OutputItem.Text(item.turnId, item.dialogueNodeId, text, item.ttsConfig?.name ?: TtsConfig.forLanguage("en").name))
                if (!item.background.isNullOrBlank()) {
                    val spec = (if (item.background!!.startsWith('/')) config.url else "") + item.background
                    add(OutputItem.Background(item.turnId, item.dialogueNodeId, spec))
                }
                if (!item.audio.isNullOrBlank()) {
                    val url = (if (item.audio!!.startsWith('/')) config.url else "") + item.audio
                    val data = callback.httpRequest(this@Client, url)
                    if (data == null) {
                        onError("Missing audio data from url $url")
                    } else {
                        val isAudioSpeech = !(item.text.isNullOrBlank()) && item.text != "..."
                        add(OutputItem.Audio(item.turnId, item.dialogueNodeId, data, url, isSpeech = isAudioSpeech))
                    }
                }
                if (!item.image.isNullOrBlank()) {
                    val url = (if (item.image!!.startsWith('/')) config.url else "") + item.image
                    add(OutputItem.Image(item.turnId, item.dialogueNodeId, url))
                }
                if (!item.video.isNullOrBlank()) {
                    val url = (if (item.video!!.startsWith('/')) config.url else "") + item.video
                    add(OutputItem.Video(item.turnId, item.dialogueNodeId, url))
                }
            } finally {
                isSuspended = false
            }
        }
    }

    private fun onResponse(response: Response) {
        turnCount += 1
        log.logger.debug("on$response")
        state = State.Responding

        if (response.logs.isNotEmpty()) // show logs
            callback.onLog(this, response.logs)

        response.turnId?.let { id ->
            turnPerformanceTransaction?.tags?.set("turn_id", id)
        }

        for (item in response.items)
            onResponseItem(item)

        if (response.sleepTimeout > 0)
            sleepLimitTime = currentTime() + response.sleepTimeout * 1000

        sessionEnded = response.sessionEnded
    }

    private fun onError(text: String, source: String = "Client") {
        log.logger.error("onError(text = $text, source = $source)")
        builtinAudio("${config.locale.getLanguage()}/error")
        state = State.Failed
        callback.onError(this, text, source)

    }

    override fun onFailure(socket: Socket<ChannelEvent>, t: Throwable) {
        if (state != State.Failed) {
            log.logger.error("$socket failure ${t::class.simpleName}: ${t.message}" + (t.cause?.let { " caused by ${it::class.simpleName}: ${it.message}" }
                ?: ""))
            if (state != State.Sleeping) // do not capture failures in Sleeping state (e.g. EOFException in WebSocketClient when idle and ping is disabled)
                monitoring.capture(null, t)
            state = State.Failed
            callback.onFailure(this, t)
        }
    }

    fun touch(listen: Boolean = true, skip: SkipMode = SkipMode.None) {
        log.logger.info("touch(listen=$listen, state=$state)")
        when (state) {
            State.Closed -> {
                builtinAudio("${config.locale.getLanguage()}/connection_lost")
            }
            State.Open -> {
                callback.audioCancel()
                if (listen)
                    nextTurn()
            }
            State.Responding -> {
                if (pauseMode || skip == SkipMode.None) {
                    state = State.Paused
                    previousWorkingState?.let { previousWorkingState = State.Paused }
                } else {
                    if (skip == SkipMode.All)
                        skipAll()
                    else
                        callback.audioCancel()
                }
            }
            State.Listening -> {
                if (isInputStreamOpen)
                    inputStreamClose(true)
                state = State.PausedListening
            }
            State.Typing -> {

            }
            State.Paused -> {
                state = State.Responding
                if (skip == SkipMode.Single)
                    callback.audioCancel()
                else if (skip == SkipMode.All)
                    skipAll()
                previousWorkingState?.let {
                    previousWorkingState = State.Responding
                }
            }
            State.Processing -> {
                outputItemQueue.isSkipping = true
                callback.audioCancel()
            }
            State.Sleeping, State.Failed -> {
                state = State.Sleeping
                callback.audioCancel()
                intro()
            }
            State.PausedListening -> {
                state = State.Listening
            }
            State.Tiles -> {

            }
        }
    }

    internal fun onOutputItem(item: OutputItem) {
        log.logger.info("on$item")
        when (item) {
            is OutputItem.Text -> {
                if (item.text.isNotEmpty()) {
                    (turnPerformanceTransaction?.createChild(
                        "text", mutableMapOf(
                            "text" to item.text
                        )
                    ) ?: Blank).measure {
                        when (callback) {
                            is ClientCallbackV1 -> callback.text(this, item)
                            is ClientCallbackV2 -> callback.text(this, item)
                        }
                    }
                }
            }
            is OutputItem.Audio -> if (config.hasOutputAudio || !item.isSpeech) {
                (turnPerformanceTransaction?.createChild(
                    "audio", mutableMapOf(
                        "url" to item.url.toString(),
                        "isSpeech" to item.isSpeech.toString()
                    )
                ) ?: Blank).measure {
                    when (callback) {
                        is ClientCallbackV1 -> callback.audio(this, item.data, item.url, item.isSpeech, null)
                        is ClientCallbackV2 -> callback.audio(this, item)
                    }
                }
            }
            is OutputItem.Image -> {
                (turnPerformanceTransaction?.createChild(
                    "image", mutableMapOf(
                        "url" to item.url
                    )
                ) ?: Blank).measure {
                    when (callback) {
                        is ClientCallbackV1 -> callback.image(this, item.url)
                        is ClientCallbackV2 -> callback.image(this, item)
                    }
                }
            }
            is OutputItem.Video -> {
                (turnPerformanceTransaction?.createChild(
                    "video", mutableMapOf(
                        "url" to item.url
                    )
                ) ?: Blank).measure {
                    when (callback) {
                        is ClientCallbackV1 -> callback.video(this, item.url)
                        is ClientCallbackV2 -> callback.video(this, item)
                    }
                }
            }
            is OutputItem.Background -> {
                (turnPerformanceTransaction?.createChild(
                    "background", mutableMapOf(
                        "spec" to item.spec
                    )
                ) ?: Blank).measure {
                    when (callback) {
                        is ClientCallbackV1 -> callback.background(this, item.spec)
                        is ClientCallbackV2 -> callback.background(this, item)
                    }
                }
            }
            is OutputItem.Command -> {
                (turnPerformanceTransaction?.createChild(
                    "command", mutableMapOf(
                        "command" to item.command
                    )
                ) ?: Blank).measure {
                    when (callback) {
                        is ClientCallbackV1 -> callback.command(this, item.command, item.code)
                        is ClientCallbackV2 -> callback.command(this, item)
                    }
                }
            }
        }
    }

    private fun sendLog() {
        if (log.entries.isNotEmpty()) {
            writeEvent(LogEvent(log.entries))
            log.entries.clear()
        }
    }

    internal open fun writeBinary(data: ByteArray) = writeEvent(BinaryEvent(data))

    internal abstract fun writeEvent(event: ChannelEvent)

    fun text(text: String, attributes: PropertyMap = mapOf()) {
        waitingMessage = ""
        waitingAttributes = mapOf()
        if (sleepLimitTime > 0 && sleepLimitTime < currentTime())
            config.sessionId = null
        if (state == State.Sleeping || state == State.Listening || state == State.Typing) {
            inputStreamClose(true)
            state = State.Processing
            val inputAttributes = mutableMapOf<String, Any>()
            inputAttributes.putAll(config.attributes)
            inputAttributes.putAll(attributes)
            this.input(input(text, config.locale, config.zoneId, provider = "Client", attributes = inputAttributes))
        } else if (state == State.Responding || state == State.Processing){
            waitingMessage = text
            waitingAttributes = attributes
        } else
            onError("Client cannot send \"$text\" (state is $state, session ${config.sessionId})")
    }

    fun text(text: String, key: String, value: Any) = text(text, mapOf(key to value))

    fun input(input: Input) = writeEvent(InputEvent(
            input,
            turnPerformanceTransaction?.trace
        )
    )

    fun vote(turnId: String, nodeId: String? = null, vote: Int) = writeEvent(VoteEvent(turnId, nodeId, vote))

    fun intro() = text(config.introText)

    fun event(type: String, category: String, text: String, turnId: String? = null, nodeId: String? = null) =
        writeEvent(ClientEvent(type, category, text, turnId, nodeId))

}