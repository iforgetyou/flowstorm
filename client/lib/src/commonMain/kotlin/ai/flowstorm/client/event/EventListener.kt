package ai.flowstorm.client.event

expect abstract class EventListener {
    open val name: String
    abstract fun onSpeechBegin()
    abstract fun onSpeechEnd()
    abstract fun onCommand(name: String, code: String?)
}