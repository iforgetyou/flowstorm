package ai.flowstorm.client

import ai.flowstorm.concurrent.Runnable
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.concurrent.synchronize
import ai.flowstorm.util.Queue

internal class OutputItemQueue(val client: Client) : Runnable, Iterator<OutputItem>  {

    var isSuspended = true
    var isRunning = false
    var isPlaying = false
    var isSkipping = false
    private val items = Queue<OutputItem>()

    override fun run() {
        isRunning = true
        while (isRunning) {
            if (!isSuspended) {
                with (client) {
                    val nextItem = if (items.size > 0) items.remove() else null
                    if (nextItem == null) {
                        if (!hasNext()) {
                            isSuspended = true
                            if (state == Client.State.Failed && config.sessionId != null)
                                sessionEnd()
                        }
                    } else {
                        isPlaying = true
                        if (!isSkipping)
                            client.onOutputItem(nextItem)
                        isPlaying = hasNext()
                    }
                }
            }
        }
    }

    fun add(item: OutputItem) = items.add(item)

    fun clear(type: OutputItem.Type? = null) = synchronize(items) {
        val toRemove = mutableListOf<OutputItem>()
        for (index in 0 until items.size) {
            val item = items.get(index)
            if (type == null || type == item.type)
                toRemove.add(item)
        }
        toRemove.forEach { items.remove(it) }
    }

    fun close() {
        isRunning = false
    }

    override fun hasNext(): Boolean {
        return items.size != 0
    }

    override fun next(): OutputItem {
        return items.remove()
    }

    fun getTextItems(): List<OutputItem> {
        val result = mutableListOf<OutputItem>()
        this.forEach {
            if (it is OutputItem.Text){
                result.add(it)
            }
        }
        return result
    }
}