package ai.flowstorm.client.io

interface WakeWordDetector {

    fun detect(buffer: ByteArray, count: Int): Boolean
}