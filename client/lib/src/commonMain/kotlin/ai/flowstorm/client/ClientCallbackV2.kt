package ai.flowstorm.client

interface ClientCallbackV2 : ClientCallback {

    fun text(client: Client, item: OutputItem.Text)

    fun audio(client: Client, item: OutputItem.Audio)

    fun image(client: Client, item: OutputItem.Image)

    fun video(client: Client, item: OutputItem.Video)

    fun background(client: Client, item: OutputItem.Background)

    fun command(client: Client, item: OutputItem.Command)
}