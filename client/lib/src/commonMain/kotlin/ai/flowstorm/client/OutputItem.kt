package ai.flowstorm.client

open class OutputItem(val turnId: String?, val nodeId: String?, val type: Type) {

    enum class Type { Server, Client }

    class Text(
        turnId: String?,
        nodeId: String?,
        val text: String,
        val name: String,
        type: Type = Type.Server
    ) : OutputItem(turnId, nodeId, type)

    class Audio(
        turnId: String?,
        nodeId: String?,
        val data: ByteArray,
        val url: String? = null,
        val isSpeech: Boolean = false,
        val handler: PlaybackHandler? = null,
        type: Type = Type.Server
    ) : OutputItem(turnId, nodeId, type)

    class Image(
        turnId: String?,
        nodeId: String?,
        val url: String,
        type: Type = Type.Server
    ) : OutputItem(turnId, nodeId, type)

    class Video(
        turnId: String?,
        nodeId: String?,
        val url: String,
        type: Type = Type.Server
    ) : OutputItem(turnId, nodeId, type)

    class Background(
        turnId: String?,
        nodeId: String?,
        val spec: String,
        type: Type = Type.Server
    ) : OutputItem(turnId, nodeId, type)

    class Command(
        turnId: String?,
        nodeId: String?,
        val command: String,
        val code: String?,
        type: Type = Type.Server
    ) : OutputItem(turnId, nodeId, type)

    override fun toString() = "${this::class.simpleName}(turnId=$turnId, nodeId=$nodeId, type=$type)"
}