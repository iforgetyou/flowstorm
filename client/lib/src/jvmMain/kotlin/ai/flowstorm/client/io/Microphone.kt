package ai.flowstorm.client.io

import ai.flowstorm.core.AudioFormat
import ai.flowstorm.core.Defaults
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.DataLine
import javax.sound.sampled.TargetDataLine

class Microphone(
    speechDevice: SpeechDevice = NoSpeechDevice,
    wakeWord: WakeWordConfig? = null,
    private val audioFormat: AudioFormat = Defaults.audioFormat,
    private val channel: Int = 0
) : InputStreamDevice(speechDevice) {

    private val format = javax.sound.sampled.AudioFormat(
        audioFormat.sampleRate.toFloat(),
        audioFormat.encoding.sampleSizeInBits,
        audioFormat.channels,
        audioFormat.signed,
        audioFormat.bigEndian
    )
    private val sampleSize = format.sampleSizeInBits / 8
    private val info = DataLine.Info(TargetDataLine::class.java, format)
    private val line = AudioSystem.getLine(info) as TargetDataLine
    private val lineBuffer = ByteArray(line.bufferSize / 2)
    private val wakeWordDetector by lazy {
        when (wakeWord?.type ?: WakeWordConfig.Type.none) {
            WakeWordConfig.Type.snowboy -> SnowboyWakeWordDetector(
                wakeWord!!,
                lineBuffer.size / 2 / audioFormat.channels
            )
            WakeWordConfig.Type.none -> null
        }
    }

    override fun start() {
        if (!isStarted) {
            line.open(format)
            line.start()
            super.start()
        }
    }

    override fun stop() {
        try {
            line.flush()
            line.close()
        } finally {
            super.stop()
        }
    }

    private fun readChannel(sourceBuffer: ByteArray, sourceCount: Int, buffer: ByteArray): Int {
        var count = 0
        for (i in 0 until sourceCount step audioFormat.channels * sampleSize) {
            buffer[count++] = sourceBuffer[i + channel * sampleSize]
            buffer[count++] = sourceBuffer[i + 1 + channel * sampleSize]
        }
        return count
    }

    override fun read(buffer: ByteArray): Int {
        val sourceCount = line.read(lineBuffer, 0, lineBuffer.size)
        val count = readChannel(lineBuffer, sourceCount, buffer)
        if (wakeWordDetector?.detect(buffer, count) == true)
            callback?.onWake()
        return count
    }

    override fun toString() = this::class.simpleName + "(speechDevice=$speechDevice, audioFormat=$audioFormat, channel=$channel)"
}