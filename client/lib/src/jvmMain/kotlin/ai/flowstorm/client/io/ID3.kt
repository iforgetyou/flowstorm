package ai.flowstorm.client.io

import javazoom.jl.decoder.Bitstream
import java.io.ByteArrayInputStream
import java.io.InputStream

class ID3(mp3in: InputStream) {
    
    constructor(data: ByteArray) : this(ByteArrayInputStream(data))
    
    private val bs = Bitstream(mp3in)
    private val id3in = bs.rawID3v2

    val header = bs.readFrame()

    val size = id3in.available()
    val max_number_of_frames = header.max_number_of_frames(mp3in.available())
    val min_number_of_frames = header.min_number_of_frames(mp3in.available())
    val frames_per_second = (1.0 / header.ms_per_frame() * 1000.0).toFloat()
    val total_ms = header.total_ms(mp3in.available())
}