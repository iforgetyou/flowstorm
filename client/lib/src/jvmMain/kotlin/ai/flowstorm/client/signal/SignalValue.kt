package ai.flowstorm.client.signal

import ai.flowstorm.time.currentTime

class SignalValue(var value: Any, var time: Long = currentTime(), var lastEmittedValue: Any? = null, var lastEmittedTime: Long? = null)