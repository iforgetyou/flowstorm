package ai.flowstorm.client.io

import ai.flowstorm.client.PlaybackHandler
import com.goxr3plus.streamplayer.stream.StreamPlayer
import com.goxr3plus.streamplayer.stream.StreamPlayerEvent
import com.goxr3plus.streamplayer.stream.StreamPlayerListener
import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.ParseContext
import org.apache.tika.parser.mp3.Mp3Parser
import org.apache.tika.sax.BodyContentHandler
import java.io.BufferedInputStream
import java.io.ByteArrayInputStream
import java.util.logging.Level
import java.util.logging.Logger

class AudioPlayer(data: ByteArray, private val handler: PlaybackHandler?) :
    StreamPlayer(Logger.getLogger(AudioPlayer::class.java.name).apply {
        level = Level.WARNING
    }), StreamPlayerListener {

    private val duration: Double

    init {
        val parser = Mp3Parser()
        val context = ParseContext()
        val handler = BodyContentHandler()
        val metadata = Metadata()
        parser.parse(ByteArrayInputStream(data), handler, metadata, context)
        //metadata.names().forEach {
        //    println("$it = " + metadata.get(it))
        //}
        duration = metadata.get("xmpDM:duration").toDouble()

        addStreamPlayerListener(this)
        open(BufferedInputStream(ByteArrayInputStream(data)))
    }

    override fun opened(dataSource: Any?, properties: MutableMap<String, Any>?) {
        //println("opened")
    }

    override fun progress(encodedBytes: Int, msPosition: Long, pcmData: ByteArray?, properties: MutableMap<String, Any>) {
        val position = ((properties["mp3.position.microseconds"] as? Long) ?: 0) / 1000000.0
        handler?.onPlayback(position, duration)
        //val progress = position / duration
        //println("progress = ${(progress * 100).format(0)} %, time = ${(progress * duration).format(2)} sec")
    }

    override fun statusUpdated(event: StreamPlayerEvent?) {
        //println("event = $event")
    }

    private fun Number.format(digits: Int) = "%.${digits}f".format(this)
}