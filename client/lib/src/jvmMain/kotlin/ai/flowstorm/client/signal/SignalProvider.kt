package ai.flowstorm.client.signal

import ai.flowstorm.concurrent.Runnable

interface SignalProvider : Runnable {

    var processor: SignalProcessor
}