package ai.flowstorm.client.event

import ai.flowstorm.util.LoggerDelegate
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.io.BufferedReader
import java.io.InputStreamReader

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
        JsonSubTypes.Type(value = EventListener.Process::class, name = "Process")
)
actual abstract class EventListener(actual open val name: String) {

    data class Process(override val name: String, val cmd: String) : EventListener(name) {

        private val logger by LoggerDelegate()

        override fun onSpeechBegin() = execute("SpeechBegin")

        override fun onSpeechEnd() = execute("SpeechEnd")

        override fun onCommand(name: String, code: String?) = execute("Command $name $code")

        private fun execute(args: String) {
            val cmd = "$cmd $args"
            logger.info("Executing $cmd")
            ProcessBuilder(*cmd.split(' ').toTypedArray()).apply {
                val proc = start()
                Thread {
                    BufferedReader(InputStreamReader(proc.inputStream)).use {
                        while (true) System.out.println(it.readLine() ?: break)
                    }
                }.start()
                Thread {
                    BufferedReader(InputStreamReader(proc.errorStream)).use {
                        while (true) System.err.println(it.readLine() ?: break)
                    }
                }.start()
                val exit = proc.waitFor()
                if (exit != 0) {
                    logger.error("Error $exit")
                    Thread.sleep(5000)
                }
            }
        }
    }

    actual abstract fun onSpeechBegin()

    actual abstract fun onSpeechEnd()

    actual abstract fun onCommand(name: String, code: String?)
}