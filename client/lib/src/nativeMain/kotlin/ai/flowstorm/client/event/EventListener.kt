package ai.flowstorm.client.event

actual abstract class EventListener {
    actual open val name: String = ""
    actual abstract fun onSpeechBegin()
    actual abstract fun onSpeechEnd()
    actual abstract fun onCommand(name: String, code: String?)
}