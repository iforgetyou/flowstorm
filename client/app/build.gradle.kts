group = "ai.flowstorm.client"
description = "Flowstorm Client App"

val kotlinxCliVersion = findProperty("kotlinx-cli.version") as String

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
    id("org.openjfx.javafxplugin") version "0.0.9"
    id("com.github.johnrengelman.shadow") version "7.0.0"
    application
}

javafx {
    version = "14"
    modules = listOf("javafx.controls", "javafx.media")
}

kotlin {
    //nativeTargets(desktop = true)
    sourceSets {
        val commonMain by getting {
            dependencies {
                api("org.jetbrains.kotlinx:kotlinx-cli:$kotlinxCliVersion")
                implementation(project(":flowstorm-common-lib"))
                implementation(project(":flowstorm-client-lib"))
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("com.pi4j:pi4j-core:1.2")
                implementation("com.pi4j:pi4j-device:1.2")
                implementation("com.pi4j:pi4j-gpio-extension:1.2")
                implementation("org.usb4java:usb4java:1.3.0")
                implementation("org.codehaus.janino:janino:3.1.2")
            }
        }
        //val nativeMain by creating {
        //    dependsOn(commonMain)
        //}
        //nativeSourceSets(nativeMain)
    }
}

tasks.getByName<Jar>("shadowJar") {
    isZip64 = true
}

tasks.named<JavaExec>("run") {
    standardInput = System.`in`
}

application {
    mainClass.set("ai.flowstorm.client.Application")
}
