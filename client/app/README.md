# Standalone (CLI) client

### Supported device types
* (default) = any desktop platform supported by Java8 SE (Windows, Mac, Linux)
* model1 = Rasberry Pi 3 model B, Pi 4 + Google Voice Kit or Respeaker mic array

#### Building OS package
see https://centerkey.com/mac/java/ for more information
```
javapackager -deploy -native pkg -name Flowstorm -BappVersion=2.0.0 -Bicon=Flowstorm.icns -srcdir target -srcfiles flowstorm.jar -appclass ai.flowstorm.client.standalone.Application -outdir target -outfile p -v
scp target/bundles/* jump.promethist.ai:/ext/cluster/default/default/repository/dist
```

#### Client version
```
java -jar flowstorm.jar version
```

#### Bot client
```
# no auto update
java -jar flowstorm.jar bot -u https://core.flowstorm.ai -k brainquist -s RPT01 -l cs -na

# no input/output audio (just texting)
java -jar flowstorm.jar bot -u https://core.flowstorm.ai -k brainquist -s RPT01 -l cs -nia -noa

# automated run from/to text file
java -jar flowstorm.jar bot -u https://core.flowstorm.ai -k performance-test -s RPT01 -l en -na -nia -noa -nol -ex -i input.txt -o output.txt
```

#### Tool actions
Converting MP3 to PCM file
```
java -jar flowstorm.jar tool -a play -i local/test.mp3 -o local/test.pcm
```

Mixing MP3 or PCM with microphone input to PCM file
```
java -jar flowstorm.jar tool -a play -i local/test.pcm -m -o local/test2.pcm
```

Convert PCM to WAV
```
ffmpeg -f s16le -ar 16k -ac 1 -i test.pcm test.wav
```

#### Building GraalVM native image
```
/opt/graalvm/bin/native-image -jar flowstorm.jar -H:+ReportExceptionStackTraces --no-fallback --allow-incomplete-classpath --no-server
```
(remove `--no-fallback --allow-incomplete-classpath` to get just bootstrap image)
