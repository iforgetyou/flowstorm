package ai.flowstorm.client.config

import ai.flowstorm.client.ClientTransport

interface JvmClientConfig : CommonClientConfig {
    val input: String
    val output: String
    val transport: ClientTransport
    val speechDevice: String
    val micChannel: String
    val speakerName: String?
}