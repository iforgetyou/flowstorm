package ai.flowstorm.client.config

interface ToolConfig : JvmClientConfig {

    enum class Action { play, sample, audio, test, respeaker2, nmea, signal, props }

    val action: Action
    val microphone: Boolean
}