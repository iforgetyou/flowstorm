package ai.flowstorm.client.audio

import ai.flowstorm.client.io.NoSpeechDevice

object SpeechDeviceFactory {

    fun getSpeechDevice(name: String) = when (name) {
        "respeaker2" -> RespeakerMicArrayV2
        else -> NoSpeechDevice
    }
}