package ai.flowstorm.client.config

import ai.flowstorm.client.io.WakeWordConfig
import ai.flowstorm.client.io.WavFileAudioRecorder
import ai.flowstorm.client.event.EventDispatcher
import ai.flowstorm.client.signal.SignalProcessor
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.core.SttMode

interface ClientConfig : JvmClientConfig {

    val fileConfig: String?
    val serverConfig: Boolean
    val device: String
    val environment: String?
    val noCache: Boolean
    val deviceId: String
    val introText: String?
    val autoStart: Boolean
    val sttInterimResults: Boolean
    val sendResponseItems: Boolean
    val exitOnError: Boolean
    val noOutputLogs: Boolean
    val showLogs: Boolean

    // audio

    val sttMode: SttMode
    val ttsFileType: AudioFileType
    val portName: String
    val volume: Int?
    val noInputAudio: Boolean
    val noOutputAudio: Boolean
    val audioRecordUpload: WavFileAudioRecorder.UploadMode
    val pauseMode: Boolean

    // GUI

    val screen: String
    val noAnimations: Boolean

    // networking

    val socketPing: Int
    val autoUpdate: Boolean
    val distUrl: String

    val signalProcessor: SignalProcessor?
    val eventDispatcher: EventDispatcher?
    val wakeWord: WakeWordConfig?

}