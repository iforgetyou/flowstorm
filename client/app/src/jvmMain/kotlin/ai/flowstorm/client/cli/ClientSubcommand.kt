package ai.flowstorm.client.cli

import ai.flowstorm.client.io.WavFileAudioRecorder
import ai.flowstorm.client.config.ClientConfig
import ai.flowstorm.client.executor.ClientExecutor
import ai.flowstorm.client.util.InetInterface
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.core.SttMode
import kotlinx.cli.ArgType
import kotlinx.cli.default

object ClientSubcommand : JvmClientSubcommand("client", "Bot client"), ClientConfig {

    override val fileConfig by option(ArgType.String, "config", "c", "Configuration file")

    override val serverConfig by option(ArgType.Boolean, "server-config", "sc", "Allow server configuration")
        .default(false)

    override val device by option(ArgType.String, "device", "d", "Device type (e.g. desktop, rpi)")
        .default("desktop")

    override val environment by option(ArgType.String, "environment", "e", "Environment (develop, preview) - this superseeds -u value")

    override val noCache by option(ArgType.Boolean, "no-cache", "nc", "Do not cache anything")
        .default(false)

    override val deviceId by option(ArgType.String, "sender", "s", "Device identification")
        .default("standalone:" + (InetInterface.getActive()?.hardwareAddress?.replace(":", "") ?: "default"))

    override val introText by option(ArgType.String, "intro-text", "intro", "Intro text")

    override val autoStart by option(ArgType.Boolean, "auto-start", "as", "Start conversation automatically")
        .default(false)

    override val sttInterimResults by option(ArgType.Boolean, "interim-results", "ir", "Interim results")
        .default(true)

    override val sendResponseItems by option(ArgType.Boolean, "response-items", "ri", "Response items")
        .default(true)

    override val exitOnError by option(ArgType.Boolean, "exit-on-error", "ex", "Raise exception")
        .default(false)

    override val noOutputLogs by option(ArgType.Boolean, "no-output-logs", "nol", "No output logs")
        .default(false)

    override val showLogs by option(ArgType.Boolean, "show-logs", "log", "Show contextual logs")
        .default(false)

    // audio

    override val sttMode by option(ArgType.Choice<SttMode>(), "stt-mode", "stt", "STT mode")
        .default(SttMode.SingleUtterance)

    override val ttsFileType by option(ArgType.Choice<AudioFileType>(), "tts-file-type", "tts", "TTS file type")
        .default(AudioFileType.mp3)

    override val portName by option(ArgType.String, "port-name", "pn", "Audio output port name")
        .default("SPEAKER")

    override val volume by option(ArgType.Int, "volume", "vo", "Audio output volume")

    override val noInputAudio by option(ArgType.Boolean, "no-input-audio", "nia", "No input audio (text input only)")
        .default(false)

    override val noOutputAudio by option(ArgType.Boolean, "no-output-audio", "noa", "No output audio (text output only)")
        .default(false)

    override val audioRecordUpload by option(ArgType.Choice<WavFileAudioRecorder.UploadMode>(), "audio-record-upload", "aru", "Audio record with upload ${WavFileAudioRecorder.UploadMode.values().toList()}")
        .default(WavFileAudioRecorder.UploadMode.none)

    override val pauseMode by option(ArgType.Boolean, "pause-mode", "pm", "Pause mode (wake word or button will pause output audio instead of stopping it and listening)")
        .default(false)

    // GUI

    override val screen by option(ArgType.String, "screen", "src", "Screen view (none, window, fullscreen)")
        .default("none")

    override val noAnimations by option(ArgType.Boolean, "no-animations", "nan", "No animations")
        .default(false)

    // networking

    override val socketPing by option(ArgType.Int, "socket-ping", "sp", "Socket ping period (in seconds, 0 = do not ping)")
        .default(0)

    override val autoUpdate by option(ArgType.Boolean, "auto-update", "aa", "Auto update JAR file")
        .default(false)

    override val distUrl by option(ArgType.String, "dist-url", "du", "Distribution URL for auto updates")
        .default("https://repository.flowstorm.ai/dist")

    override fun execute() = ClientExecutor(this).execute()
}