package ai.flowstorm.client.cli

import ai.flowstorm.client.executor.VersionExecutor
import kotlinx.cli.Subcommand

object VersionSubcommand: Subcommand("version", "Show application version") {

    override fun execute() = VersionExecutor.execute()
}