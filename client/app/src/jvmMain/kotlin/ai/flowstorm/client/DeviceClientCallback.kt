package ai.flowstorm.client

import ai.flowstorm.client.io.AudioPlayer
import ai.flowstorm.client.io.OutputAudioDevice
import ai.flowstorm.client.event.EventDispatcher
import ai.flowstorm.client.ui.Screen
import ai.flowstorm.common.HttpRequest
import ai.flowstorm.common.client.HttpUtil
import ai.flowstorm.concurrent.launch
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.util.Log
import ai.flowstorm.util.LoggerDelegate
import java.io.ByteArrayInputStream
import java.io.File
import java.io.PrintWriter
import kotlin.system.exitProcess

open class DeviceClientCallback(
    private val output: PrintWriter,
    private val distUrl: String? = null,
    private val autoUpdate: Boolean = false,
    private val noOutputLogs: Boolean = false,
    private val noOutputAudio: Boolean = false,
    private val eventDispatcher: EventDispatcher? = null,
    private val portName: String = "SPEAKER",
    private val exitOnError: Boolean = false,
    private val showLogs: Boolean = false
) : SimpleClientCallback() {

    private var jarUpdater: JarUpdater? = null
    private var audioCancelled = false
    private val logger by LoggerDelegate()

    override fun onOpen(client: Client) {
        val sourcePath = this::class.java.protectionDomain.codeSource.location.toURI().path
        if (distUrl != null) {
            if (sourcePath.endsWith(".jar")) {
                logger.info("Starting auto update for file $sourcePath from $distUrl")
                jarUpdater = JarUpdater(distUrl, File(sourcePath), doUpdate = autoUpdate).apply {
                    launch()
                }
            } else {
                logger.warn("Auto update requested but source path $sourcePath is not JAR file")
            }
        }
    }

    override fun text(text: String) {
        output.println(text)
    }

    override fun log(text: String) {
        if (!noOutputLogs)
            super.log(text)
    }

    override fun onError(client: Client, text: String, subject: String) = super.onError(client, text, subject).also {
        if (exitOnError)
            exitProcess(1)
    }

    override fun onFailure(client: Client, t: Throwable) = super.onFailure(client, t).also {
        if (exitOnError)
            exitProcess(2)
    }

    override fun onRecognizedText(client: Client, text: String, isFinal: Boolean) {
        if (isFinal)
            Screen.instance?.viewUserText(text)
        super.onRecognizedText(client, text, isFinal)
    }

    override fun text(client: Client, item: OutputItem.Text) {
        Screen.instance?.viewBotText(item.text)
        super.text(client, item)
    }

    override fun onLog(client: Client, logs: List<Log.Entry>) = logs.forEach {
        if (this.showLogs) {
            val time = "%.2f".format(it.relativeTime)
            output.println("+$time:${it.level}:[${it.text}]")
        }
    }

    override fun onStateChange(client: Client, newState: Client.State) {
        if (client.state != newState) {
            super.onStateChange(client, newState)
            Screen.instance?.stateChange(client.state, newState)
        }
        jarUpdater?.allowed = (newState == Client.State.Sleeping)
    }

    override fun audio(client: Client, item: OutputItem.Audio) = with(item) {
        if (!noOutputAudio) {
            if (!isSpeech)
                super.audio(client, item)
            audioCancelled = false
            val player = AudioPlayer(data, handler)
            if (isSpeech)
                eventDispatcher?.onSpeechBegin()
            player.play()
            while (!player.isStopped && !audioCancelled) {
                if (client.state == Client.State.Paused) {
                    if (!player.isPaused)
                        player.pause()
                } else {
                    if (!player.isPlaying)
                        player.play()
                }
                sleep(20)
            }
            if (isSpeech)
                eventDispatcher?.onSpeechEnd()
        } else {
            super.audio(client, item)
        }
    }

    override fun audioCancel() {
       audioCancelled = true
    }

    override fun image(client: Client, item: OutputItem.Image) = with(item) {
        if (Screen.instance != null) {
            Screen.instance?.viewImage(ByteArrayInputStream(HttpUtil.httpRequest(url)))
        }
        super.image(client, item)
    }

    override fun video(client: Client, item: OutputItem.Video) = with(item) {
        if (Screen.instance != null) {
            Screen.instance?.viewMedia(url)
        }
        super.video(client, item)
    }

    override fun command(client: Client, item: OutputItem.Command) = with(item) {
        super.command(client, item)
        eventDispatcher?.onCommand(command, code)
        if (command.startsWith("volume")) {
            with (OutputAudioDevice) {
                val value = volume(portName,
                    when {
                        code?.equals("up", true) == true -> VOLUME_UP
                        code?.equals("down", true) == true -> VOLUME_DOWN
                        else -> code?.toInt() ?: 7
                    }
                )
                log("{Volume $value}")
            }
        }
    }

    override fun httpRequest(client: Client, url: String, request: HttpRequest?): ByteArray? = HttpUtil.httpRequest(url, request)
}