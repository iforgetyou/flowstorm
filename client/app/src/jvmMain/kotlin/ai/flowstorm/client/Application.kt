package ai.flowstorm.client

import ai.flowstorm.client.cli.ClientSubcommand
import ai.flowstorm.client.cli.ToolSubcommand
import ai.flowstorm.client.cli.VersionSubcommand
import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.monitoring.SentryMonitoring
import kotlinx.cli.ArgParser
import kotlinx.cli.ExperimentalCli

object Application {

    private const val name = "flowstorm"

    val monitoring = with (AppConfig) {
        SentryMonitoring(
            getOrNull("sentry.dsn"),
            getOrNull("app.version"),
            getOrNull("namespace")
        )
    }

    fun getEndpointUrl(serviceName: ServiceUrlResolver.Service, environment: String, protocol: String = "http") =
        when (val nameSuffix = if (listOf("production", "default").contains(environment)) "default" else environment) {
            "local" -> ServiceUrlResolver.getEndpointUrl(serviceName, ServiceUrlResolver.RunMode.local)
            else -> ServiceUrlResolver.getEndpointUrl(
                serviceName,
                ServiceUrlResolver.RunMode.dist,
                nameSuffix = nameSuffix,
                protocol = ServiceUrlResolver.Protocol.valueOf(protocol)
            )
        }

    @OptIn(ExperimentalCli::class)
    @JvmStatic
    fun main(args: Array<String>) {
        if (args.isEmpty()) {
            println("Usage: $name command options_list")
            println("       $name -h to get list of supported commands")
        } else {
            with (ArgParser(name)) {
                subcommands(ClientSubcommand, ToolSubcommand, VersionSubcommand)
                parse(args)
            }
        }
    }
}