package ai.flowstorm.client.cli

import ai.flowstorm.client.config.ToolConfig
import ai.flowstorm.client.executor.ToolExecutor
import kotlinx.cli.ArgType
import kotlinx.cli.default

object ToolSubcommand : JvmClientSubcommand("tool", "Tool actions"), ToolConfig {

    override val action by option(ArgType.Choice<ToolConfig.Action>(), "action", "a", "Action").default(ToolConfig.Action.audio)

    override val microphone by option(ArgType.Boolean, "microphone", "m", "User microphone").default(false)

    override fun execute() = ToolExecutor(this).execute()
}