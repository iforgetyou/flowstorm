package ai.flowstorm.client.io

import java.awt.Color

interface ColorLight: Light {

    fun set(color: Color)
}