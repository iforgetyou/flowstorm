package ai.flowstorm.client.cli

import ai.flowstorm.client.ClientTransport
import ai.flowstorm.client.io.WakeWordConfig
import ai.flowstorm.client.config.JvmClientConfig
import ai.flowstorm.client.event.EventDispatcher
import ai.flowstorm.client.signal.SignalProcessor
import kotlinx.cli.ArgType
import kotlinx.cli.default

abstract class JvmClientSubcommand(name: String, description: String) : CommonClientSubcommand(name, description),
    JvmClientConfig {

    override val input by option(ArgType.String, "input", "i", "Input file (e.g. stdin, none, /path/to/input.txt)")
        .default("stdin")

    override val output by option(ArgType.String, "output", "o", "Output file (e.g. stdout, /path/to/output.txt)")
        .default("stdout")

    override val transport by option(ArgType.Choice<ClientTransport>(), "client-transport", "ct", "Client type ${ClientTransport.values().toList()}")
        .default(ClientTransport.HttpSocket)

    override val speechDevice by option(ArgType.String, "speech-device", "sd", "Speech device (respeaker2)")
        .default("none")

    override val micChannel by option(ArgType.String, "mic-channel", "mc", "Microphone channel (count:selected)")
        .default("1:0")

    override val speakerName by option(ArgType.String, "speaker-name", "spk", "Speaker name")

    val signalProcessor: SignalProcessor? = null

    val eventDispatcher: EventDispatcher? = null

    val wakeWord: WakeWordConfig? = null
}