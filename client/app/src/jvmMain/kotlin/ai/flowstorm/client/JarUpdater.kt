package ai.flowstorm.client

import ai.flowstorm.concurrent.Runnable
import okhttp3.OkHttpClient
import okhttp3.Request
import ai.flowstorm.util.LoggerDelegate
import java.io.File
import java.io.FileOutputStream
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import kotlin.system.exitProcess

class JarUpdater(
    distUrl: String,
    private val jarFile: File,
    private val sleepTime: Long = 15,
    private val doUpdate: Boolean = true
) : Runnable {

    var allowed = true
    private val jarFilename = jarFile.path.substring(jarFile.path.lastIndexOf(File.separatorChar))
    private val jarFileUrl = distUrl + jarFilename
    private val headRequest = Request.Builder().url(jarFileUrl).head().build()
    private val getRequest = Request.Builder().url(jarFileUrl).build()
    private val client = OkHttpClient()
    private val logger by LoggerDelegate()

    override fun run() {
        while (true) {
            if (allowed) {
                try {
                    val response = client.newCall(headRequest).execute()
                    val lastModifiedHeader = response.header("Last-Modified")
                    if (lastModifiedHeader != null) {
                        val remoteLastModifiedDateTime =
                                ZonedDateTime.parse(lastModifiedHeader, DateTimeFormatter.RFC_1123_DATE_TIME)
                        val remoteLastModified = remoteLastModifiedDateTime.toInstant().toEpochMilli()
                        val localLastModified = jarFile.lastModified()
                        if (remoteLastModified > localLastModified) {
                            if (doUpdate) {
                                val jarDestFile = File("${jarFile.path}.update")
                                logger.info("Auto update download from $jarFileUrl to $jarDestFile")
                                val response = client.newCall(getRequest).execute()
                                response.body?.byteStream()?.use { input ->
                                    FileOutputStream(jarDestFile).use { output ->
                                        input.copyTo(output)
                                    }
                                }
                                logger.info("AutoUpdate: $jarDestFile")
                                exitProcess(1)
                            } else {
                                logger.info("AutoUpdate: newer version of application available. Please download manualy or re-run with auto update enabled.")
                                break
                            }
                        }
                    }
                    logger.debug("Auto update check $jarFileUrl (lastModified=$lastModifiedHeader)")
                } catch (t: Throwable) {
                    logger.warn("Auto update failed: ${t.message}")
                }
            }
            Thread.sleep(1000 * sleepTime)
        }
    }
}