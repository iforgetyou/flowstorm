package ai.flowstorm.client.config

import ai.flowstorm.client.ClientTransport
import ai.flowstorm.client.io.WakeWordConfig
import ai.flowstorm.client.io.WavFileAudioRecorder
import ai.flowstorm.client.event.EventDispatcher
import ai.flowstorm.client.signal.SignalProcessor
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.core.SttMode

data class ClientConfigObject(

    override val logLevel: String,
    override val url: String,
    override val key: String,
    override val token: String?,
    override val locale: String,

    override val input: String,
    override val output: String,
    override val transport: ClientTransport,
    override val speechDevice: String,
    override val micChannel: String,
    override val speakerName: String?,

    override val fileConfig: String?,
    override val serverConfig: Boolean,
    override val device: String,
    override val environment: String?,
    override val noCache: Boolean,
    override val deviceId: String,
    override val introText: String?,
    override val autoStart: Boolean,
    override val sttInterimResults: Boolean,
    override val sendResponseItems: Boolean,
    override val exitOnError: Boolean,
    override val noOutputLogs: Boolean,
    override val showLogs: Boolean,

    // audio

    override val sttMode: SttMode,
    override val ttsFileType: AudioFileType,
    override val portName: String,
    override val volume: Int?,
    override val noInputAudio: Boolean,
    override val noOutputAudio: Boolean,
    override val audioRecordUpload: WavFileAudioRecorder.UploadMode,
    override val pauseMode: Boolean,

    // GUI

    override val screen: String,
    override val noAnimations: Boolean,

    // networking

    override val socketPing: Int,
    override val autoUpdate: Boolean,
    override val distUrl: String
) : ClientConfig {
    override val signalProcessor: SignalProcessor? = null
    override val eventDispatcher: EventDispatcher? = null
    override val wakeWord: WakeWordConfig? = null
}
