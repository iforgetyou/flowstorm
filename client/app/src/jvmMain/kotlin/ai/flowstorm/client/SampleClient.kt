package ai.flowstorm.client

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.client.util.InetInterface
import ai.flowstorm.common.HttpRequest
import ai.flowstorm.common.TextConsole
import ai.flowstorm.common.client.HttpUtil
import ai.flowstorm.common.client.JavaHttpSocketClientFactory
import ai.flowstorm.common.monitoring.NoMonitoring
import ai.flowstorm.core.ClientConfig
import ai.flowstorm.util.Log
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.util.RunLog

class SampleClient(clientConfig: ClientConfig) : TextConsole() {

    val client = ClientV2(clientConfig, JavaHttpSocketClientFactory("${clientConfig.url}/client", ChannelEvent::class), object : SimpleClientCallback() {
        override fun onLog(client: Client, logs: List<Log.Entry>) {}
        override fun httpRequest(client: Client, url: String, request: HttpRequest?) = HttpUtil.httpRequest(url, request)
    }, RunLog("SampleClientLog"), monitoring = NoMonitoring)

    fun open() = client.open()

    override fun afterInput(text: String) = client.text(text)

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val context = ClientConfig(
                "https://core.flowstorm.ai",
                if (args.isNotEmpty()) args[0] else error("Missing app key"), // application ID
                "sample:" + InetInterface.getActive()?.hardwareAddress?.replace(":", ""),
                Dynamic("clientType" to "sample:1.0.0-SNAPSHOT"), // CHANGE TO your-client-name:version
                autoStart = false,
                sttInterimResults = true
            )
            SampleClient(context).apply {
                open()
                run()
            }
        }
    }
}