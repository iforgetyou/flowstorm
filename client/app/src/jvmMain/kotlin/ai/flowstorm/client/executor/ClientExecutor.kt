package ai.flowstorm.client.executor

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.client.*
import ai.flowstorm.core.ClientConfig
import ai.flowstorm.client.audio.*
import ai.flowstorm.client.config.ClientConfig as ExecutorConfig
import ai.flowstorm.client.config.ClientConfigObject
import ai.flowstorm.client.io.*
import ai.flowstorm.client.signal.SignalGroup
import ai.flowstorm.client.signal.SignalProvider
import ai.flowstorm.client.io.SpeechDevice
import ai.flowstorm.client.ui.Screen
import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.client.WebSocketClient
import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.client.HttpPollingSocketClient
import ai.flowstorm.common.client.HttpUtil
import ai.flowstorm.common.client.JavaHttpSocketClientFactory
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.core.AudioFormat
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.util.Locale
import ai.flowstorm.util.RunLog
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.type.TypeReference
import com.pi4j.io.gpio.GpioFactory
import com.pi4j.io.gpio.PinPullResistance
import com.pi4j.io.gpio.PinState
import com.pi4j.io.gpio.RaspiPin
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import java.awt.Color
import java.io.*
import kotlin.concurrent.thread
import kotlin.reflect.KProperty1
import kotlin.system.exitProcess

class ClientExecutor(var config: ExecutorConfig) : Executor {

    private var version = 1
    private var output: PrintWriter? = null
    private val stdout = PrintWriter(OutputStreamWriter(System.out), true)
    lateinit var clientConfig: ClientConfig
    lateinit var client: Client
    lateinit var callback: DeviceClientCallback
    lateinit var speechDevice: SpeechDevice
    var light: Light? = null
    var responded = false

    private fun exit(): Nothing {
        client.close()
        sleep(2000)
        exitProcess(0)
    }

    private fun log(text: String) {
        if (config.showLogs)
            stdout.println("[$text]")
    }

    private fun setOutput() {
        output = if (config.output == "stdout")
            null
        else
            PrintWriter(OutputStreamWriter(FileOutputStream(config.output)), true)
    }

    private fun setVolume() {
        if (config.volume != null) {
            OutputAudioDevice.volume(config.portName, config.volume!!)
            log("Volume ${config.portName} set to ${config.volume}")
        }
    }

    private fun loadConfig(input: InputStream) = input.use {
        config = with (ClientConfigObject::class.constructors.first()) {
            val arguments = mutableListOf<Any?>()
            parameters.forEach { parameter ->
                val getter = this::class.members.first { member -> member.name == parameter.name } as KProperty1<Any, *>
                arguments += getter.get(this)
            }
            call(*arguments.toTypedArray())
        }
        ObjectUtil.defaultMapper.readerForUpdating(config).readValue(JsonFactory().createParser(it), object : TypeReference<ClientConfig>() {})
    }

    private fun loadConfig() {
        if (config.fileConfig != null) {
            log("Configuration from ${config.fileConfig}")
            loadConfig(FileInputStream(config.fileConfig))
        } else if (config.serverConfig) {
            val url = Application.getEndpointUrl(ServiceUrlResolver.Service.studio, config.environment ?: "production") + "/client/deviceConfig/${config.deviceId}"
            try {
                HttpUtil.httpRequestStream(url, raiseExceptions = true)?.let {
                    loadConfig(it)
                    log("Configuration from $url")
                }
            } catch (e: Throwable) {
                log("Configuration from $url error: ${e.message}")
            }
        }
    }

    private fun createContext() {
        ServiceUrlResolver
        clientConfig = ClientConfig(
            url = if (config.url != null)
                config.url!!
            else
                Application.getEndpointUrl(ServiceUrlResolver.Service.core, config.environment ?: "production"),
            key = config.key,
            token = config.token,
            deviceId = config.deviceId,
            autoStart = config.autoStart,
            sttInterimResults = config.sttInterimResults,
            sendResponseItems = config.sendResponseItems,
            locale = Locale.forLanguageTag(config.locale.replace('_', '-')),
            attributes = Dynamic(
                "clientType" to "standalone:${AppConfig.version}",
                "clientScreen" to (config.screen != "none")
            )
        )
        if (config.introText != null)
            clientConfig.introText = config.introText!!
    }

    private fun createCallback() {
        callback = object : DeviceClientCallback(
            stdout,
            config.distUrl,
            config.autoUpdate,
            config.noOutputLogs,
            config.noOutputAudio || output != null,
            config.eventDispatcher,
            config.portName,
            config.exitOnError,
            config.showLogs
        ) {
            override fun onStateChange(client: Client, newState: Client.State) {
                super.onStateChange(client, newState)
                when (newState) {
                    Client.State.Listening ->
                        if (light is ColorLight)
                            (light as ColorLight).set(Color.GREEN)
                        else
                            light?.high()
                    Client.State.Processing ->
                        if (light is ColorLight)
                            (light as ColorLight).set(Color.BLUE)
                    Client.State.Failed ->
                        if (light is ColorLight)
                            (light as ColorLight).set(Color.RED)
                    Client.State.Open -> {}
                    else ->
                        light?.low()
                }
                client.event("cli", "state", newState.toString())
            }

            override fun onReady(client: Client) {
                super.onReady(client)
                light?.apply {
                    blink(0)
                    low()
                }
            }

            override fun text(client: Client, item: OutputItem.Text) {
                output?.apply { println(item.text) }
                super.text(client, item)
                responded = true
            }

            override fun onFailure(client: Client, t: Throwable) {
                super.onFailure(client, t)
                responded = true
            }

            override fun onSessionId(client: Client, sessionId: String?) {
                super.onSessionId(client, sessionId)
                if (sessionId == null) {
                    responded = true
                    val version = version
                    loadConfig()
                    if (version != version) {
                        log("{Configuration version changed from $version to $version - exiting to be reloaded}")
                        Thread.sleep(5000)
                        exit()
                    }
                }
            }
        }
    }

    private fun setSpeechDevice() {
        speechDevice = SpeechDeviceFactory.getSpeechDevice(config.speechDevice)
    }

    private fun launchScreen() {
        if (config.screen != "none") {
            Screen.client = client
            Screen.fullScreen = (config.screen == "fullscreen")
            Screen.animations = !config.noAnimations
            thread {
                Screen.launch()
            }
        }
    }

    private fun createClient() {
        val micChannel = config.micChannel.split(':').map { it.toInt() }
        val inputAudioDevice =
            if (config.noInputAudio || !listOf("none", "stdin").contains(config.input))
                null
            else
                Microphone(speechDevice, config.wakeWord, AudioFormat(channels = micChannel[0]), micChannel[1])
        val inputAudioRecorder =
            if (config.noInputAudio || (config.audioRecordUpload == WavFileAudioRecorder.UploadMode.none))
                null
            else
                WavFileAudioRecorder(
                    File("."),
                    if (clientConfig.url.startsWith("http://localhost"))
                        ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.core, ServiceUrlResolver.RunMode.local)
                    else
                        clientConfig.url,
                    config.audioRecordUpload
                )
        val runLog = RunLog("ClientLog")

        client = when (config.transport) {
            ClientTransport.HttpSocket ->
                ClientV2(
                    clientConfig,
                    JavaHttpSocketClientFactory("${clientConfig.url}/client", ChannelEvent::class),
                    callback,
                    runLog,
                    inputAudioDevice,
                    config.ttsFileType,
                    config.sttMode,
                    config.pauseMode,
                    inputAudioRecorder,
                    true,
                    monitoring = Application.monitoring
                )
            else ->
                ClientV1(
                    clientConfig,
                    when (config.transport) {
                        ClientTransport.HttpPolling -> HttpPollingSocketClient("${clientConfig.url}/polling", ChannelEvent::class)
                        else -> WebSocketClient("${clientConfig.url}/socket", ChannelEvent::class, socketPing = config.socketPing.toLong())
                    },
                    callback,
                    runLog,
                    inputAudioDevice,
                    config.ttsFileType,
                    config.sttMode,
                    config.pauseMode,
                    inputAudioRecorder,
                    monitoring = Application.monitoring
                )
        }
    }

    private fun enableSignalProcessing() = config.signalProcessor?.apply {
        log("Enabling signal processor")
        emitter = { signalGroup: SignalGroup, values: PropertyMap ->
            when (signalGroup.type) {
                SignalGroup.Type.Text ->
                    if (client.state == Client.State.Sleeping) {
                        log("Signal text '${signalGroup.name}' values $values")
                        client.text(signalGroup.name, values)
                    }
                SignalGroup.Type.Touch ->
                    client.touch()
            }
        }
        if (speechDevice is SignalProvider)
            providers.add(speechDevice as SignalProvider)
        run()
    }

    private fun enableHardware() {
        if (listOf("rpi", "model1", "model2", "model3").contains(config.device)) {
            val gpio = GpioFactory.getInstance()
            light = if (config.device == "model2")
                Vk2ColorLed().apply {
                    set(Color.MAGENTA)
                }
            else
                BinLed(gpio).apply {
                    blink(500)
                }
            val button = gpio.provisionDigitalInputPin(RaspiPin.GPIO_04, PinPullResistance.PULL_DOWN)
            button.setShutdownOptions(true)
            button.addListener(GpioPinListenerDigital { event ->
                when (event.state) {
                    PinState.LOW -> {
                        light?.high()
                        client.touch()
                    }
                    PinState.HIGH -> {
                        light?.low()
                    }
                }
            })
        }
    }

    private fun handleInput(inputName: String) {
        InputStreamReader(
            if (inputName == "stdin")
                System.`in`
            else
                FileInputStream(inputName)
        ).use {
            val input = BufferedReader(it)
            while (true) {
                if (inputName == "stdin") {
                    while (!responded && client.state != Client.State.Failed) {
                        sleep(50)
                    }
                }
                val text = input.readLine()?.trim() ?: exit()
                when (text) {
                    "" -> {
                        log("Click when ${client.state}")
                        client.touch()
                    }
                    "exit", "quit" -> {
                        exit()
                    }
                    else -> {
                        responded = false
                        stdout.println("> $text")
                        client.text(text)
                    }
                }
            }
        }
    }

    override fun execute() {
        setLogLevel(config.logLevel)
        setOutput()
        loadConfig()
        setVolume()
        createContext()
        createCallback()
        setSpeechDevice()
        createClient()
        launchScreen()
        enableSignalProcessing()
        enableHardware()

        log("context = $clientConfig")
        log("clientType = ${config.transport}")
        log("inputAudioDevice = ${client.inputStreamDevice}")
        log("sttMode = ${client.sttMode}")
        log("device = ${config.device}")
        client.open()
        if (config.input != "none")
            handleInput(config.input)
    }
}