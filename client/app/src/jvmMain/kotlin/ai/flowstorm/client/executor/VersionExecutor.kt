package ai.flowstorm.client.executor

import ai.flowstorm.common.AppConfig

object VersionExecutor : Executor {
    override fun execute() {
        println("version ${AppConfig.version} commit ${AppConfig["git.commit"]}")
    }
}