package ai.flowstorm.client

enum class ClientTransport {
    HttpSocket, HttpPolling, WebSocket
}