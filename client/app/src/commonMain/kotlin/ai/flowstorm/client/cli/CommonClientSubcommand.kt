package ai.flowstorm.client.cli

import ai.flowstorm.client.config.CommonClientConfig
import kotlinx.cli.ArgType
import kotlinx.cli.Subcommand
import kotlinx.cli.default

abstract class CommonClientSubcommand(name: String, description: String) : Subcommand(name, description),
    CommonClientConfig {

    override val logLevel by option(ArgType.String, "log-level", "L", "Set logging severity").default("WARN")

    override val url by option(ArgType.String, "url", "u", "Core URL")

    override val key by option(ArgType.String, "key", "k", "Application key")
        .default("6058c10767f4ca2fa0d0cb14")

    override val token by option(ArgType.String, "token", "t", "Authorization token")

    override val locale by option(ArgType.String, "locale", "l", "Preferred conversation locale").default("en")
}