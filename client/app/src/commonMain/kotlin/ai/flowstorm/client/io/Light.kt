package ai.flowstorm.client.io

interface Light {

    fun high()
    fun low()
    fun blink(ms: Long)
}