package ai.flowstorm.client.config

interface CommonClientConfig {
    val logLevel: String
    val url: String?
    val key: String
    val token: String?
    val locale: String
}