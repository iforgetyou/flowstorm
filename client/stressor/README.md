stress web
```
java -jar stressor.jar master -s test/web.csv -u https://promethist.ai 
```

stress bot
```
java -jar stressor.jar master -wt bot -s test/bot.txt -u http://localhost:8080 -okey=performance-test -odeviceId=RPT01 -odelay=50 -w 5 -t 60
```