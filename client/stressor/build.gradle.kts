group = "ai.flowstorm.client"
description = "Flowstorm Client Stressor"

val kotlinxCliVersion = findProperty("kotlinx-cli.version") as String
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
    id("com.github.johnrengelman.shadow") version "7.0.0"
    application
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":flowstorm-client-lib"))
                api("org.jetbrains.kotlinx:kotlinx-cli:$kotlinxCliVersion")
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
            }
        }
    }
}

application {
    mainClass.set("ai.flowstorm.client.stressor.Application")
}


