package ai.flowstorm.client.stressor.web

import ai.flowstorm.client.stressor.model.Hit
import java.io.IOException
import java.net.HttpURLConnection

class WebRequestHandler : AbstractRequestHandler() {
    @Throws(IOException::class)
    override fun doRequest(request: WebScenario.Item, conn: HttpURLConnection, hit: Hit, worker: WebWorker) {
        conn.connect()
        hit.code = conn.responseCode
        hit.info = "length=" + readResponseBody(conn).length
    }
}