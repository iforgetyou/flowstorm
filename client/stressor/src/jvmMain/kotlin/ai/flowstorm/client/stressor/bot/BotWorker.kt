package ai.flowstorm.client.stressor.bot

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.client.*
import ai.flowstorm.client.stressor.Worker
import ai.flowstorm.client.stressor.model.Hit
import ai.flowstorm.common.HttpRequest
import ai.flowstorm.common.client.HttpUtil
import ai.flowstorm.common.client.JavaHttpSocketClientFactory
import ai.flowstorm.common.monitoring.NoMonitoring
import ai.flowstorm.core.ClientConfig
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.util.Log
import ai.flowstorm.util.RunLog
import java.io.PrintStream
import java.io.Serializable

class BotWorker(output: PrintStream, endpoint: String, options: Map<String, Serializable>, scenarioItems: List<String>) :
        Worker(output, options, BotScenario(scenarioItems)) {

    var code = -1
    private val delay = if (options.containsKey("delay")) (options["delay"] as String).toLong() else 0
    val client =
        ClientV2(
            ClientConfig(
                url = endpoint,
                key = options["key"] as String,
                deviceId = options["deviceId"] as String,
                autoStart = false,
                attributes = Dynamic("clientType" to "stressor")
            ),
            JavaHttpSocketClientFactory("$endpoint/client", ChannelEvent::class),
            object : SimpleClientCallback() {
                override fun onReady(client: Client) {
                    super.onReady(client)
                    closed = false
                }

                override fun onError(client: Client, text: String, subject: String) {
                    super.onError(client, text, subject)
                    code = 1
                }

                override fun onLog(client: Client, logs: List<Log.Entry>) {}

                override fun text(client: Client, item: OutputItem.Text) {
                    code = 0
                    super.text(client, item)
                }

                override fun httpRequest(client: Client, url: String, request: HttpRequest?): ByteArray? = HttpUtil.httpRequest(url, request)
            },
            RunLog("WorkerLog"),
            monitoring = NoMonitoring
        ).apply {
            open()
        }
    override var closed = true

    override fun hit(item: Any): Hit {
        code = -1
        val hit = Hit()
        hit.item = item as String
        client.text(hit.item!!)
        while (code == -1) {
            sleep(50)
        }
        hit.code = code
        hit.time = System.currentTimeMillis() - hit.time
        if (delay > 0) {
            val delay = hit.item!!.length * delay
            sleep(delay)
            hit.info = "delay=$delay"
        }
        return hit
    }
}