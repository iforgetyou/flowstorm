package ai.flowstorm.client.stressor.model

import kotlinx.datetime.Clock
import java.io.Serializable
import java.util.*

class Report(var workerCount: Int, var testTime: Long) : Serializable {
    class Item : Serializable {
        var hitCount = 0
        var codes = Hashtable<Int, Int?>()
        var minTime: Long = 99999999
        var maxTime: Long = 0
        var sumTime: Long = 0
    }

    var startTime = Clock.System.now()
    var realTime: Long
    private var items: MutableMap<String, Item> = LinkedHashMap()
    fun getItem(key: String): Item {
        return items.computeIfAbsent(key) { Item() }
    }

    val keySet: Set<String>
        get() = items.keys

    fun fill(hits: List<Hit>) {
        for (hit in hits) {
            for (item in Arrays.asList(getItem("*"), getItem(hit.item.toString()))) {
                item.hitCount++
                if (item.minTime > hit.time) {
                    item.minTime = hit.time
                }
                if (item.maxTime < hit.time) {
                    item.maxTime = hit.time
                }
                item.sumTime += hit.time
                if (!item.codes.containsKey(hit.code)) {
                    item.codes[hit.code] = 0
                }
                item.codes[hit.code] = item.codes[hit.code]!! + 1
            }
        }
    }

    fun getItems(): Map<String, Item> {
        return items
    }

    fun setItems(items: MutableMap<String, Item>) {
        this.items = items
    }

    init {
        realTime = System.currentTimeMillis()
        getItem("*")
    }
}
