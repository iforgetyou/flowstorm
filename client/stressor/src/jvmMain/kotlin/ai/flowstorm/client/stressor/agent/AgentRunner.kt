package ai.flowstorm.client.stressor.agent

import ai.flowstorm.client.stressor.Worker
import ai.flowstorm.client.stressor.model.Hit
import java.io.PrintStream
import java.io.Serializable
import java.rmi.RemoteException
import java.rmi.registry.LocateRegistry

class AgentRunner(agentHost: String,
                  private val workerType: Worker.Type,
                  private val out: PrintStream,
                  private val endpoint: String,
                  private val scenarioItems: List<String>,
                  private val options: Map<String, Serializable>,
                  private val workerCount: Int,
                  private val testTime: Long
) : Thread() {

    private val agentHost: String? = null
    private var agent: Agent? = null
    var hits: List<Hit>? = null

    override fun run() {
        runningAppCount++
        try {
            hits = agent!!.run(workerType, endpoint, scenarioItems, options, workerCount, testTime)
        } catch (e: RemoteException) {
            e.printStackTrace(out)
        } finally {
            runningAppCount--
        }
    }

    override fun toString() =
        "${this::class.qualifiedName}(agentHost:$agentHost, endpoint:$endpoint, workerCount:$workerCount, testTime:$testTime)"

    companion object {
        var runningAppCount = 0
        private const val RMI_PORT = 6666
    }

    init {
        agent = if ("local" == agentHost) {
            AgentImpl(out)
        } else {
            val registry = LocateRegistry.getRegistry(agentHost, RMI_PORT)
            registry.lookup("agent") as Agent
        }
    }
}
