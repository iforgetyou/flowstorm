package ai.flowstorm.client.stressor.web

import ai.flowstorm.client.stressor.Worker
import ai.flowstorm.client.stressor.model.Hit
import java.io.PrintStream
import java.io.Serializable
import java.net.HttpURLConnection
import java.net.URL

class WebWorker(out: PrintStream, private val endpoint: String, options: Map<String, Serializable>, scenarioItems: List<String>) :
        Worker(out, options, WebScenario(scenarioItems)) {

    private val requestHandler = WebRequestHandler()

    override fun hit(item: Any): Hit {
        val hit = Hit()
        item as WebScenario.Item
        hit.item = endpoint + item.uri
        try {
            val conn = URL(hit.item!!).openConnection() as HttpURLConnection
            try {
                conn.requestMethod = item.method
                requestHandler.doRequest(item, conn, hit, this)
                hit.code = conn.responseCode
            } finally {
                conn.disconnect()
            }
            hit.time = System.currentTimeMillis() - hit.time
            try {
                sleep(random.nextInt(1000) + item.wait)
            } catch (ignored: InterruptedException) {
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return hit
    }
}
