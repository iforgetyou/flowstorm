package ai.flowstorm.client.stressor.cli

import ai.flowstorm.client.stressor.agent.AgentRunner
import ai.flowstorm.client.stressor.Worker
import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import ai.flowstorm.client.stressor.model.Hit
import ai.flowstorm.client.stressor.model.Report
import kotlinx.cli.ArgType
import kotlinx.cli.default
import kotlinx.cli.required
import kotlinx.cli.vararg
import org.slf4j.LoggerFactory
import java.io.File

object MasterSubcommand : CommonSubcommand("master", "Run in master mode.") {

    private val scenarioFile by option(ArgType.String, "scenario", "s", "Path to file with scenario")
        .required()

    private val workerType by option(ArgType.Choice<Worker.Type>(), "worker-type", "wt", "Worker type")
        .default(Worker.Type.web)

    private val url by option(ArgType.String, "url", "u", "URL to be stressed")
        .required()

    private val options by option(ArgType.String, "options", "o", "Worker options (name1=value1,name2=value2,...)")

    private val testTimeSeconds by option(ArgType.Int, "time", "t", "Test time in seconds")
        .default(30)

    private val workerCount by option(ArgType.Int, "workers", "w", "Number of workers to use")
        .default(5)

    private val agentHosts by argument(ArgType.String, "agents", "List of agent addresses (IP, domain) which will be used for testing")
        .vararg()

    private val testTimeMillis: Long
        get() = testTimeSeconds.toLong() * 1000

    override fun execute() {
        (LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME) as Logger).level = Level.toLevel(logLevel)

        // run stress testing
        val out = System.out
        val scenarioItems = File(scenarioFile).readLines()
        val workerCount: Int = workerCount
        val testTime: Long = testTimeMillis
        val report = Report(workerCount, testTime)
        val runners = mutableSetOf<AgentRunner>()
        agentHosts.ifEmpty {
            listOf("local")
        }.forEach { agentHost ->
            val runner = AgentRunner(agentHost, workerType, System.out, url, scenarioItems, options?.split(',')
                ?.associate { item ->
                    item.split('=').let {
                        it[0].trim() to it[1].trim()
                    }
                } ?: emptyMap(), workerCount, testTime)
            runners.add(runner)
            runner.run()
        }

        // wait until all apps finish their work
        while (AgentRunner.runningAppCount > 0)
            Thread.sleep(testTime)

        // collect hits from executors
        val hits = mutableListOf<Hit>()
        for (executor in runners)
            hits.addAll(executor.hits!!)

        // finish report
        report.fill(hits)
        report.realTime = System.currentTimeMillis() - report.realTime

        // output report
        out.println("REPORT")
        out.println("startTime,%tc".format(report.startTime))
        out.println("testTime,${report.testTime}")
        out.println("realTime,${report.realTime}")
        out.println("workerCount,${report.workerCount}")
        out.println("item,hitCount,codes,hit/sec,minTime,maxTime,avgTime")
        for (key in report.keySet) {
            val item: Report.Item = report.getItem(key)
            out.println("\"%s\",%d,\"%s\",%.2f,%d,%d,%.2f".format(
                key,item.hitCount, item.codes,
                    item.hitCount.toFloat() / (report.realTime / 1000),
                    item.minTime, item.maxTime,
                    item.sumTime.toFloat() / item.hitCount.toFloat()
            ))
        }
    }
}