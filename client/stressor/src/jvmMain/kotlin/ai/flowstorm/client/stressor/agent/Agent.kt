package ai.flowstorm.client.stressor.agent

import ai.flowstorm.client.stressor.Worker
import ai.flowstorm.client.stressor.model.Hit
import java.io.Serializable
import java.rmi.Remote
import java.rmi.RemoteException

interface Agent : Remote {

    @Throws(RemoteException::class)
    fun run(workerType: Worker.Type, endpoint: String, scenarioItems: List<String>, options: Map<String, Serializable>, workerCount: Int, testTime: Long): List<Hit>?
}
