package ai.flowstorm.client.stressor

import ai.flowstorm.client.stressor.model.Hit
import ai.flowstorm.client.stressor.model.Scenario
import kotlinx.datetime.Clock
import java.io.PrintStream
import java.io.Serializable
import java.util.*

abstract class Worker(
    private val out: PrintStream,
    val options: Map<String, Serializable>,
    private val scenario: Scenario
) : Thread() {

    enum class Type { web, bot }

    val hits: MutableList<Hit> = mutableListOf<Hit>()
    var running = false
    open var closed = false

    abstract fun hit(item: Any): Hit

    override fun run() {
        running = true
        var requestIndex = 0
        while (running) {
            if (closed) {
                sleep(50)
                continue
            }
            val item = scenario.items.get(requestIndex)
            val hit = hit(item)
            hits.add(hit)
            out.println("LOG,%tc,%d,%s,%d,%d,\"%s\"".format(Clock.System.now(), ++hitCount, hit.item, hit.code, hit.time, hit.info))
            if (++requestIndex == scenario.items.size) {
                requestIndex = 0
            }
        }
        closed = true
    }

    fun close() {
        running = false
    }

    companion object {
        val random = Random()
        var hitCount = 0
    }
}