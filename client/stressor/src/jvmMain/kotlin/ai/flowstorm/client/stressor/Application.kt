package ai.flowstorm.client.stressor

import ai.flowstorm.client.stressor.cli.AgentSubcommand
import ai.flowstorm.client.stressor.cli.MasterSubcommand
import kotlinx.cli.ArgParser

object Application {

    private const val NAME = "flowstorm-client-stressor"

    @JvmStatic
    fun main(args: Array<String>) {
        if (args.isEmpty()) {
            println("Usage: $NAME command options_list")
            println("       $NAME -h to get list of supported commands")
        } else {
            with (ArgParser(NAME)) {
                subcommands(AgentSubcommand, MasterSubcommand)
                parse(args)
            }
        }
    }

}