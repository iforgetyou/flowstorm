package ai.flowstorm.client.stressor.agent

import ai.flowstorm.client.stressor.Worker
import ai.flowstorm.client.stressor.bot.BotWorker
import ai.flowstorm.client.stressor.model.Hit
import ai.flowstorm.client.stressor.web.WebWorker
import java.io.PrintStream
import java.io.Serializable
import java.rmi.RemoteException
import java.rmi.server.UnicastRemoteObject
import java.util.*

class AgentImpl(private val out: PrintStream) : UnicastRemoteObject(), Agent {

    private val random = Random()

    @Throws(RemoteException::class)
    override fun run(workerType: Worker.Type, endpoint: String, scenarioItems: List<String>, options: Map<String, Serializable>, workerCount: Int, testTime: Long): List<Hit> {
        out.println("LOG,running stress testing with $workerCount worker threads for $testTime ms")
        val workers: MutableSet<Worker> = HashSet()
        // create workers
        out.println("LOG,creating worker threads for endpoint $endpoint")
        for (i in 0 until workerCount) {
            val worker = when (workerType) {
                Worker.Type.bot -> BotWorker(out, endpoint, options, scenarioItems)
                Worker.Type.web -> WebWorker(out, endpoint, options, scenarioItems)
            }
            workers.add(worker)
            worker.start()
            Thread.sleep(random.nextInt(200).toLong())
        }
        // let workers do their work
        try {
            Thread.sleep(testTime)
        } catch (e: InterruptedException) {
            e.printStackTrace(out)
        }
        // close workers
        for (worker in workers) {
            worker.close()
        }
        // wait for workers to close
        out.println("LOG,waiting for worker threads to close")
        var closed: Int
        var count = 0
        do {
            closed = 0
            for (worker in workers) if (worker.closed) closed++
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        } while (closed < workers.size && ++count < THREAD_CLOSE_LIMIT)
        if (count == THREAD_CLOSE_LIMIT) {
            out.println("LOG,worker thread close limit $THREAD_CLOSE_LIMIT seconds reached")
        }
        out.println("LOG,finished stress testing with $workerCount worker threads for $testTime ms")
        val hits: MutableList<Hit> = Vector<Hit>()
        for (worker in workers) {
            hits.addAll(worker.hits)
        }
        return hits
    }

    companion object {
        private const val THREAD_CLOSE_LIMIT = 30
    }

}