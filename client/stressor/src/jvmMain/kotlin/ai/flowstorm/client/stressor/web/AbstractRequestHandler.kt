package ai.flowstorm.client.stressor.web

import ai.flowstorm.client.stressor.model.Hit
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection

abstract class AbstractRequestHandler {
    @Throws(IOException::class)
    abstract fun doRequest(request: WebScenario.Item, conn: HttpURLConnection, hit: Hit, worker: WebWorker)

    @Throws(IOException::class)
    protected fun writeRequestBody(conn: HttpURLConnection, str: String?) {
        conn.doOutput = true
        val out = OutputStreamWriter(conn.outputStream)
        out.write(str)
        out.flush()
    }

    @Throws(IOException::class)
    protected fun readResponseBody(conn: HttpURLConnection): StringBuffer {
        val reader = BufferedReader(InputStreamReader(conn.inputStream))
        val buf = StringBuffer()
        var inputLine: String?
        while (reader.readLine().also { inputLine = it } != null) {
            buf.append(inputLine)
        }
        reader.close()
        return buf
    }
}