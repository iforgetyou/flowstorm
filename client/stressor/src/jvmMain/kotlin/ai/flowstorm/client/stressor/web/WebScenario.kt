package ai.flowstorm.client.stressor.web

import ai.flowstorm.client.stressor.model.Scenario
import java.io.Serializable

class WebScenario(items: List<String>) : Scenario(items) {

    class Item internal constructor(var method: String, var uri: String, var wait: Long, var info: String?) : Serializable

    init {
        val webItems = mutableListOf<Item>()
        for (item in items) {
            val parts = item.split(",").toTypedArray()
            webItems.add(Item(parts[0], parts[1], if (parts.size > 2) java.lang.Long.valueOf(parts[2]) else 0, if (parts.size > 3) parts[3] else null))
        }
        this.items = webItems
    }
}
