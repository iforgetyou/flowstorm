package ai.flowstorm.client.stressor.cli

import ai.flowstorm.client.stressor.agent.AgentImpl
import java.rmi.registry.LocateRegistry

object AgentSubcommand : CommonSubcommand("agent", "Run as an agent, which will be controlled by master.") {

    override fun execute() {
        try {
            val registry = LocateRegistry.createRegistry(rmiPort)
            registry.bind("agent", AgentImpl(System.out))
            println("LOG,agent listening on port $rmiPort")
        } catch (e: Exception) {
            System.err.println("Error: ${e.javaClass.name} - ${e.message}")
        }
    }
}