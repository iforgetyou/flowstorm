package ai.flowstorm.client.stressor.model

abstract class Scenario(items: List<String>) {

    var items: List<Any> = items
}