package ai.flowstorm.client.stressor.model

import ai.flowstorm.time.currentTime

class Hit {
    var item: String? = null
    var time = currentTime()
    var code = 0
    var info = ""
}