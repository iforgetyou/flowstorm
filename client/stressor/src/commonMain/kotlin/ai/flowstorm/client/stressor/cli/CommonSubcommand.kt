package ai.flowstorm.client.stressor.cli

import kotlinx.cli.ArgType
import kotlinx.cli.Subcommand
import kotlinx.cli.default

abstract class CommonSubcommand(name: String, description: String) : Subcommand(name, description) {

    val logLevel by option(ArgType.String, "log-level", "L", "Set logging severity").default("WARN")

    val rmiPort by option(ArgType.Int, "rmi", "r", "Port for RMI communication from master to agents.")
        .default(6666)
}