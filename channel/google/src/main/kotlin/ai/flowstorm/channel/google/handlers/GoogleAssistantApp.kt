package ai.flowstorm.channel.google.handlers

import ai.flowstorm.channel.client.Core
import com.google.actions.api.ActionRequest
import com.google.actions.api.ActionsSdkApp
import com.google.actions.api.ForIntent
import com.google.actions.api.response.ResponseBuilder
import com.google.api.services.actions_fulfillment.v2.model.BasicCard
import com.google.api.services.actions_fulfillment.v2.model.Image
import com.google.api.services.actions_fulfillment.v2.model.SimpleResponse
import ai.flowstorm.common.AppConfig
import ai.flowstorm.core.ClientConfig
import ai.flowstorm.core.Response
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.util.LoggerDelegate
import java.util.*

class GoogleAssistantApp : ActionsSdkApp() {

    companion object {
        val appKey = object : ThreadLocal<String>() {
            override fun initialValue() = "default"
        }
    }

    val appTitle = AppConfig["title"]

    private val logger by LoggerDelegate()

    inner class ConfiguredBlock(val request: ActionRequest, val config: ClientConfig) {
        fun addResponse(response: Response) = getResponseBuilder(request).apply {
            add(SimpleResponse().apply {
                displayText = response.text()
                ssml = response.ssml(Response.IVA.GoogleAssistant)
            })
            response.items.forEach {
                if (it.image != null) {
                    add(BasicCard().apply {
                        title = appTitle
                        if (!it.text.isNullOrBlank())
                            subtitle = it.text
                        image = Image().apply {
                            url = it.image
                            accessibilityText = "(alt)"
                        }
                    })
                }
            }
            config.sessionId ?: endConversation()
        }
    }

    private fun withConfig(request: ActionRequest, block: ConfiguredBlock.() -> ResponseBuilder): ResponseBuilder {
        val config = Core.config(
                request.sessionId!!,
                "google-device",
                appKey.get(),
                null,
                Locale.ENGLISH/*, request.appRequest?.user?.locale?*/,
                Dynamic(
                        "clientType" to "google-assistant:${AppConfig.version}"
                )
        )
        logger.info("${this::class.simpleName}.withConfig(request=$request, config=$config)")
        return block(ConfiguredBlock(request, config))
    }


    @ForIntent("actions.intent.MAIN")
    fun onMainIntent(request: ActionRequest) = withConfig(request) {
        val response = Core.doIntro(config)
        addResponse(response)
    }.build()

    @ForIntent("actions.intent.TEXT")
    fun onTextIntent(request: ActionRequest) = withConfig(request) {
        val text = request.getArgument("text")!!.textValue
        val response = Core.doText(config, text)
        addResponse(response)
    }.build()

}