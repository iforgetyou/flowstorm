group = "ai.flowstorm.channel"
description = "Flowstorm Channel Google"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-common-app"))
    implementation(project(":flowstorm-channel-lib"))
    implementation("com.google.http-client:google-http-client:1.31.0")
    implementation("com.google.actions:actions-on-google:1.0.0") {
        exclude("org.slf4j","slf4j-log4j12")
    }
}

