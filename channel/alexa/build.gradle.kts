group = "ai.flowstorm.channel"
description = "Flowstorm Channel Alexa"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-common-app"))
    implementation(project(":flowstorm-channel-lib"))
    api("com.amazon.alexa:ask-sdk:2.30.0") {
        exclude("org.apache.logging.log4j", "log4j-slf4j-impl")
        exclude("com.fasterxml.jackson.core", "jackson-core")
        exclude("com.fasterxml.jackson.core", "jackson-databind")
        exclude("com.fasterxml.jackson.core", "jackson-annotations")
        exclude("com.fasterxml.jackson.datatype", "jackson-datatype-jsr310")
    }
}

