package ai.flowstorm.channel.alexa.servlets

import com.amazon.ask.Skills
import com.amazon.ask.servlet.SkillServlet
import ai.flowstorm.channel.alexa.handlers.*
import javax.servlet.annotation.WebServlet

@WebServlet(name = "Amazon Alexa Servlet", urlPatterns = ["/alexa"])
class AmazonAlexaServlet : SkillServlet(
        Skills.standard()
            .addRequestHandlers(
                    CancelAndStopIntentHandler(),
                    MessageIntentHandler(),
                    HelpIntentHandler(),
                    LaunchRequestHandler(),
                    SessionEndedRequestHandler())
            .build()
)