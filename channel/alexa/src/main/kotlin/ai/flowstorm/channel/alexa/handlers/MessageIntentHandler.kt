package ai.flowstorm.channel.alexa.handlers

import ai.flowstorm.channel.client.Core
import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.model.IntentRequest
import com.amazon.ask.model.Response
import com.amazon.ask.request.Predicates.intentName
import java.util.*

class MessageIntentHandler : AmazonAlexaHandler(intentName("MessageIntent")) {

    override fun handle(input: HandlerInput): Optional<Response> = withConfig(input) {
        val text = (input.requestEnvelope.request as IntentRequest).intent.slots["text"]!!.value
        if (text == "tell device id") {
            input.attributesManager.sessionAttributes["deviceId"] = config.deviceId
            input.responseBuilder
                    .withSpeech("")
                    .withShouldEndSession(true)
        } else {
            val speech = Core.doText(config, text)
            addResponse(speech)
        }
    }.build()
}