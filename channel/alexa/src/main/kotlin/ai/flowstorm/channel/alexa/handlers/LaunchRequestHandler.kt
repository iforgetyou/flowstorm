package ai.flowstorm.channel.alexa.handlers

import ai.flowstorm.channel.client.Core
import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.model.LaunchRequest
import com.amazon.ask.model.Response
import com.amazon.ask.request.Predicates.requestType
import java.util.*

class LaunchRequestHandler : AmazonAlexaHandler(requestType(LaunchRequest::class.java)) {

    override fun handle(input: HandlerInput): Optional<Response> = withConfig(input) {
        val speech = Core.doIntro(config)
        addResponse(speech)
    }.build()
}