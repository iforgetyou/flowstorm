package ai.flowstorm.channel.alexa.handlers

import ai.flowstorm.channel.client.Core
import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.model.Response
import com.amazon.ask.model.SessionEndedRequest
import com.amazon.ask.request.Predicates.requestType
import java.util.*

class SessionEndedRequestHandler : AmazonAlexaHandler(requestType(SessionEndedRequest::class.java)) {

    override fun handle(input: HandlerInput): Optional<Response> {
        val context = getConfig(input)
        //context.attributes["alexaSessionEndedReason"] = (input.requestEnvelope as SessionEndedRequest).reason.toString()
        Core.doBye(context)
        return Optional.empty()
    }
}