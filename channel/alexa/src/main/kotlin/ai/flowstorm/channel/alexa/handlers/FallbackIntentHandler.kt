package ai.flowstorm.channel.alexa.handlers

import ai.flowstorm.channel.client.Core
import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.request.Predicates.intentName
import com.amazon.ask.model.Response
import java.util.*

class FallbackIntentHandler : AmazonAlexaHandler(intentName("AMAZON.FallbackIntent")) {

    override fun handle(input: HandlerInput): Optional<Response> = withConfig(input) {
        val speech = Core.doHelp(config)
        addResponse(speech)
    }.build()
}