package ai.flowstorm.channel.alexa.handlers

import ai.flowstorm.channel.client.Core
import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.model.Response
import com.amazon.ask.request.Predicates.intentName
import java.util.*

class HelpIntentHandler : AmazonAlexaHandler(intentName("AMAZON.HelpIntent")) {

    override fun handle(input: HandlerInput): Optional<Response> = withConfig(input) {
        val speech = Core.doHelp(config)
        addResponse(speech)
    }.build()
}