package ai.flowstorm.channel.client

import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.client.RestClient
import ai.flowstorm.core.*
import ai.flowstorm.core.resources.CoreResource
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.util.Locale

object Core {

    val url = ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.core, ServiceUrlResolver.RunMode.local)
    val resource = RestClient.resource(CoreResource::class.java, url)

    private val configs = mutableMapOf<String, ClientConfig>()

    fun config(sessionId: String, deviceId: String, appKey: String, token: String? = null, locale: Locale = Defaults.locale, attributes: Dynamic = Dynamic()) =
        configs.computeIfAbsent(sessionId) {
            ClientConfig(url, appKey, deviceId, token = token, sessionId = sessionId, locale = locale, attributes = attributes)
        }

    private fun createRequest(config: ClientConfig, text: String): Request =
        Request(config.key, config.deviceId, config.token, config.sessionId!!, config.initiationId,
            input(text, config.locale, config.zoneId), config.attributes)

    private fun doRequest(request: Request) = resource.process(request)

    fun doIntro(config: ClientConfig) = doText(config, config.introText)

    fun doText(config: ClientConfig, text: String) = doRequest(createRequest(config, text))

    fun doHelp(config: ClientConfig) = doRequest(createRequest(config, "help"))

    fun doBye(config: ClientConfig) = doRequest(createRequest(config, "stop"))
}
