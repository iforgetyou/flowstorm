group = "ai.flowstorm.channel"
description = "Flowstorm Channel Client"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-common-client"))
    api(project(":flowstorm-channel-api"))
    api(project(":flowstorm-core-api"))
}

