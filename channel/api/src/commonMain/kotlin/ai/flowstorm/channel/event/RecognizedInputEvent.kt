package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.core.Input

data class RecognizedInputEvent(val input: Input, val isFinal: Boolean = true) : ChannelEvent() {
    override fun toString() = "${super.toString()}(input=$input, isFinal=$isFinal)"
}