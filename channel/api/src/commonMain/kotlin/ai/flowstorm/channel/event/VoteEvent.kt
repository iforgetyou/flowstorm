package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

/**
 * @var vote -1, 1
 */
class VoteEvent(val turnId: String, val nodeId: String? = null, val vote: Int) : ChannelEvent() {
    override fun toString() = "${super.toString()}(turnId=$turnId, nodeId=$nodeId, vote=$vote)"
}