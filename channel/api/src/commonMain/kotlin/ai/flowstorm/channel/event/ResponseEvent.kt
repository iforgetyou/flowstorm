package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.core.Response

data class ResponseEvent(val response: Response) : ChannelEvent()