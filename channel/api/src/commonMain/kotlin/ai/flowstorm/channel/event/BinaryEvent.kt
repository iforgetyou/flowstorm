package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

data class BinaryEvent(val data: ByteArray) : ChannelEvent() {

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other == null || this::class != other::class) return false
        if (!data.contentEquals((other as BinaryEvent).data)) return false
        return true
    }

    override fun hashCode() = data.contentHashCode()

    override fun toString() = "${super.toString()}(${data.size})"
}