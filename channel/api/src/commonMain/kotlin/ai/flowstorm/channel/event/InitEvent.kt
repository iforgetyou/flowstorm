package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelConfig
import ai.flowstorm.channel.ChannelEvent

@Deprecated("Used by V1 protocol only")
data class InitEvent(val key: String, val token: String?, val deviceId: String, val config: ChannelConfig) : ChannelEvent()