package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.core.Request

@Deprecated("Used by V1 protocol only", replaceWith = ReplaceWith("InputEvent"))
data class RequestEvent(val request: Request) : ChannelEvent()