package ai.flowstorm.channel

import ai.flowstorm.core.*
import ai.flowstorm.time.ZoneId
import ai.flowstorm.util.AudioEncoding
import ai.flowstorm.util.Locale

data class ChannelConfig(
    val locale: Locale = Defaults.locale,
    val zoneId: ZoneId = Defaults.zoneId,
    val sttMode: SttMode = SttMode.SingleUtterance,
    val sttModel: SttModel = SttModel.General,
    val sttSampleRate: Int = Defaults.audioFormat.sampleRate,
    val sttEncoding: AudioEncoding = Defaults.audioFormat.encoding,
    @Deprecated("V2 protocol is not using it anymore (client decides whether to open separate socket to receive recognized inputs or not)")
    val sttInterimResults: Boolean = false,
    val sendResponseItems: Boolean = false,
    val ttsFileType: AudioFileType = AudioFileType.mp3,
    val test: Boolean = false
)