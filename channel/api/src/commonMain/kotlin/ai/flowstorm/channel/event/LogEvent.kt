package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.util.Log

class LogEvent(val entries: List<Log.Entry>) : ChannelEvent() {
    override fun toString() = "${super.toString()}(${entries.size})"
}