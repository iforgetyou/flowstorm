package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.core.Response

data class ResponseItemEvent(val responseItem: Response.Item) : ChannelEvent()