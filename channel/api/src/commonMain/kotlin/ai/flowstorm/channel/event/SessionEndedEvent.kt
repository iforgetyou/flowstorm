package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

@Deprecated("Used by V1 protocol only")
class SessionEndedEvent : ChannelEvent()