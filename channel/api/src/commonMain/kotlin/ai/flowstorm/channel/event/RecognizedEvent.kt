package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

@Deprecated("Used by V1 protocol only", replaceWith = ReplaceWith("RecognizedInputEvent"))
data class RecognizedEvent(val text: String, val isFinal: Boolean = true) : ChannelEvent()