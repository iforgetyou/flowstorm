package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

data class ErrorEvent(val text: String, val source: String = "Unknown") : ChannelEvent() {
    override fun toString() = "${super.toString()}(text=$text, source=$source)"

}