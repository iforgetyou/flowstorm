package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

class InputStreamCloseEvent : ChannelEvent()