package ai.flowstorm.channel

fun interface ChannelEventSubscriber {
    fun onEvent(event: ChannelEvent)
}