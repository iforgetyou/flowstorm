package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

@Deprecated("Used by V1 protocol only")
class InputAudioStreamOpenEvent(val sessionId: String? = null, val trace: String? = null) : ChannelEvent()