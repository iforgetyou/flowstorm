package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

class ClientEvent(
    val type: String,
    val category: String,
    val text: String,
    val turnId: String? = null,
    val nodeId: String? = null,
) : ChannelEvent() {
    override fun toString() = "${super.toString()}(type=$type, category=$category, text=$text, turnId=$turnId, nodeId=$nodeId)"
}