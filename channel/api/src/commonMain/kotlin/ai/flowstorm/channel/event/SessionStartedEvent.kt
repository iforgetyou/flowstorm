package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent

@Deprecated("Used by V1 protocol only")
data class SessionStartedEvent(val sessionId: String) : ChannelEvent()