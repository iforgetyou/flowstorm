package ai.flowstorm.channel.event

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.core.Input

data class InputEvent(val input: Input, val trace: String? = null) : ChannelEvent() {
    override fun toString() = "${super.toString()}(input=$input, trace=$trace)"
}