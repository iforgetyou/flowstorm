package ai.flowstorm.channel

import ai.flowstorm.channel.event.*
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
        JsonSubTypes.Type(value = LogEvent::class, name = "Log"),
        JsonSubTypes.Type(value = VoteEvent::class, name = "Vote"),
        JsonSubTypes.Type(value = PingEvent::class, name = "Ping"),
        JsonSubTypes.Type(value = InitEvent::class, name = "Init"),
        JsonSubTypes.Type(value = ReadyEvent::class, name = "Ready"),
        JsonSubTypes.Type(value = ErrorEvent::class, name = "Error"),
        JsonSubTypes.Type(value = InputEvent::class, name = "Input"),
        JsonSubTypes.Type(value = ClientEvent::class, name = "Client"),
        JsonSubTypes.Type(value = BinaryEvent::class, name = "Binary"),
        JsonSubTypes.Type(value = RequestEvent::class, name = "Request"),
        JsonSubTypes.Type(value = ResponseEvent::class, name = "Response"),
        JsonSubTypes.Type(value = ResponseItemEvent::class, name = "ResponseItem"),
        JsonSubTypes.Type(value = RecognizedEvent::class, name = "Recognized"),
        JsonSubTypes.Type(value = RecognizedInputEvent::class, name = "RecognizedInput"),
        JsonSubTypes.Type(value = SessionStartedEvent::class, name = "SessionStarted"),
        JsonSubTypes.Type(value = SessionEndedEvent::class, name = "SessionEnded"),
        JsonSubTypes.Type(value = InputStreamCloseEvent::class, name = "InputStreamClose"),
        JsonSubTypes.Type(value = InputAudioStreamOpenEvent::class, name = "InputAudioStreamOpen"),
        JsonSubTypes.Type(value = InputAudioStreamCloseEvent::class, name = "InputAudioStreamClose")
)
actual open class ChannelEvent {
    override fun toString(): String = this::class.simpleName!!
}
