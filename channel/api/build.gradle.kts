group = "ai.flowstorm.channel"
description = "Flowstorm Channel API"

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
}

kotlin {
    nativeTargets(true)
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":flowstorm-core-lib"))
            }
        }
        val jvmMain by getting
        val nativeMain by creating {
            dependsOn(commonMain)
        }
        nativeSourceSets(nativeMain, true)
    }
}
dependencies {
    api(project(":flowstorm-core-lib"))
}

