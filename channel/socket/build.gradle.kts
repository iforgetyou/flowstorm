group = "ai.flowstorm.channel"
description = "Flowstorm Channel Socket"
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-common-app"))
    implementation(project(":flowstorm-channel-lib"))
    implementation(project(":flowstorm-nlp-pipeline"))
    implementation(project(":flowstorm-nlp-stt-api"))
    implementation(project(":flowstorm-nlp-tts-api"))
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
}

