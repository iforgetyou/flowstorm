package ai.flowstorm.channel.socket.servlets

import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory
import ai.flowstorm.common.servlets.JettyWebSocketServlet
import ai.flowstorm.channel.socket.adapter.TwilioSocketAdapter

class TwilioWebSocketServlet : JettyWebSocketServlet() {

    override fun configure(factory: WebSocketServletFactory) =
        configure(factory, TwilioSocketAdapter.InputMessage::class) {
            TwilioSocketAdapter()
        }
}