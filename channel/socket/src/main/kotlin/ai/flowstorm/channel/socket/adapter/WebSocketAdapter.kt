package ai.flowstorm.channel.socket.adapter

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.event.*
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.core.Input
import ai.flowstorm.core.Response
import ai.flowstorm.core.SttMode
import ai.flowstorm.core.indexer.RuntimeDataIndexer
import ai.flowstorm.core.model.FullContext
import ai.flowstorm.core.nlp.pipeline.*
import ai.flowstorm.core.repository.ClientEventRepository
import ai.flowstorm.core.repository.TurnRepository
import ai.flowstorm.core.type.beautify
import ai.flowstorm.util.Log
import org.bson.types.ObjectId
import org.litote.kmongo.id.toId
import java.io.IOException
import javax.inject.Inject

class WebSocketAdapter : AbstractSocketAdapter<ChannelEvent, ChannelEvent>() {

    @Inject
    private lateinit var turnRepository: TurnRepository

    @Inject
    private lateinit var clientEventRepository: ClientEventRepository

    @Inject
    private lateinit var indexer: RuntimeDataIndexer

    private val responseItems = mutableListOf<Response.Item>()

    override fun onRecognizedInput(input: Input, isFinal: Boolean) {
        if (channelConfig.sttInterimResults || isFinal)
            send(RecognizedEvent(input.transcript.text.beautify(), isFinal))
    }

    override fun onProcessError(obj: Any, e: Throwable) {
        send(ErrorEvent(e.message ?: e::class.qualifiedName ?: "unknown", obj::class.simpleName ?: "unknown"))
    }

    override fun onRecognitionError(error: Throwable, source: String) = send(ErrorEvent(error.message ?: "Unknown", source))

    override fun onResponseItem(item: Response.Item) {
        if (channelConfig.sendResponseItems)
            send(ResponseItemEvent(item))
        else
            responseItems += item
    }

    override fun onResponseDone(logs: List<Log.Entry>, sessionEnded: Boolean, sleepTimeout: Int, turnId: String) {
        send(ResponseEvent(Response(logs, responseItems, sessionEnded, sleepTimeout, turnId)))
        if (sessionEnded && !isScopeClosed)
            closeScope()
        responseItems.clear()
    }

    override fun createSessionId() = super.createSessionId().also { id ->
        send(SessionStartedEvent(id))
    }

    override fun closeScope() = super.closeScope().also { closed ->
        if (closed)
            send(SessionEndedEvent())
    }

    override fun onObject(socket: Socket<ChannelEvent>, event: ChannelEvent) {
        try {
            logger.info("Receiving event $event")
            when (event) {
                is InitEvent -> {
                    key = event.key
                    token = event.token
                    deviceId = event.deviceId
                    channelConfig = event.config.copy(sttMode = when (event.config.sttMode) {
                        SttMode.Default -> SttMode.SingleUtterance
                        SttMode.Duplex -> SttMode.Continuous
                        else -> event.config.sttMode
                    })
                    send(ReadyEvent())
                    if (channelConfig.sttMode == SttMode.Continuous)
                        scope.openStream()
                }
                is SessionStartedEvent -> {
                    sessionId = event.sessionId
                    send(SessionStartedEvent(sessionId!!))
                }
                is SessionEndedEvent -> {
                    closeScope()
                }
                is RequestEvent -> with(event) {
                    val newId = request.sessionId ?: createSessionId()
                    if (sessionId == null) { send(SessionStartedEvent(newId)) }
                    sessionId = newId
                    val inputAttributes = mutableMapOf<String, Any>()
                    inputAttributes.putAll(request.attributes)
                    inputAttributes.putAll(request.input.attributes)
                    request.input.attributes = inputAttributes
                    scope.process(request.input, null)
                }
                is LogEvent -> {
                    scopeInstance?.addClientLogEntries(event.entries)
                }
                is ClientEvent -> {
                    val clientEvent = ai.flowstorm.core.model.ClientEvent(
                        type = event.type,
                        category = event.category,
                        text = event.text,
                        deviceId = deviceId ?: "unknown",
                        sessionId = sessionId,
                        turnId = event.turnId,
                        nodeId = event.nodeId
                    )
                    clientEventRepository.create(clientEvent)
                    indexer.save(clientEvent)
                }
                is VoteEvent -> turnRepository.find(ObjectId(event.turnId).toId())?.let { turn ->
                    turn.votes[event.nodeId ?: "unknown"] = event.vote
                    turnRepository.update(turn)
                    indexer.save(turn)
                    indexer.updateById(FullContext::class, event.turnId, "{\"turn\" : {\"votes\": {\"${event.nodeId}\":${event.vote}}}}")
                }
                is PingEvent -> {
                    send(event)
                }
                is InputEvent -> {
                    if (!isChannelConfigured)
                        error("Channel is not configured (send InitEvent first)")
                    scope.process(event.input, event.trace)
                }
                is InputAudioStreamOpenEvent -> {
                    if (!isScopeClosed) {
                        scope.log.logger.info("Client opened input audio stream")
                        if (channelConfig.sttMode == SttMode.SingleUtterance)
                            scope.openStream(trace = event.trace)
                        send(InputAudioStreamOpenEvent(event.sessionId))
                    }
                }
                is InputAudioStreamCloseEvent -> {
                    if (!isScopeClosed) {
                        scope.log.logger.info("Client closed input audio stream")
                        scope.closeStreams(PipelineStreamType.Stt)
                    }
                }
                else -> error("Unexpected event of type ${event::class.simpleName}")
            }
        } catch (e: Throwable) {
            monitoring.capture(null, e)
            logger.error("Error", e)
            (e.cause?:e).apply {
                send(ErrorEvent(message ?: this::class.qualifiedName ?: "unknown"))
            }
        }
    }

    @Synchronized
    @Throws(IOException::class)
    private fun send(event: ChannelEvent) {
        logger.info("Sending event ${event::class.simpleName}")
        socket.write(event)
    }
}