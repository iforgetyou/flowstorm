package ai.flowstorm.channel.socket.servlets

import ai.flowstorm.channel.ChannelEvent
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory
import ai.flowstorm.common.servlets.JettyWebSocketServlet
import ai.flowstorm.channel.socket.adapter.WebSocketAdapter

class ClientWebSocketServlet : JettyWebSocketServlet() {

    override fun configure(factory: WebSocketServletFactory) =
        configure(factory, ChannelEvent::class) {
            WebSocketAdapter()
        }
}