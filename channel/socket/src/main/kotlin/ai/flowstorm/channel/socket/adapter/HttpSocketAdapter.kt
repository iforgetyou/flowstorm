package ai.flowstorm.channel.socket.adapter

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.ChannelEventSubscriber
import ai.flowstorm.channel.event.*
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.concurrent.waitUntil
import ai.flowstorm.core.Input
import ai.flowstorm.core.Response
import ai.flowstorm.core.indexer.RuntimeDataIndexer
import ai.flowstorm.core.model.FullContext
import ai.flowstorm.core.nlp.pipeline.Pipeline
import ai.flowstorm.core.nlp.pipeline.PipelineConfig
import ai.flowstorm.core.nlp.pipeline.PipelineHandler
import ai.flowstorm.core.repository.ClientEventRepository
import ai.flowstorm.core.repository.TurnRepository
import ai.flowstorm.util.Log
import org.bson.types.ObjectId
import org.litote.kmongo.id.toId
import java.io.InputStream

/**
 * This adapter creates pipeline scope used per one turn executed by HTTP channel protocol (@see ClientChannelServlet)
 */
class HttpSocketAdapter(
    pipeline: Pipeline,
    val pipelineConfig: PipelineConfig,
    private val turnRepository: TurnRepository,
    private val clientEventRepository: ClientEventRepository,
    private val sendResponseItems: Boolean = true,
    private val indexer: RuntimeDataIndexer
) : PipelineHandler {

    val sessionId get() = pipelineConfig.sessionId
    private val scope = pipeline.createScope(pipelineConfig, this)
    private val subscribers = mutableSetOf<ChannelEventSubscriber>()
    private val responseItems = mutableListOf<Response.Item>()

    private fun writeEvent(event: ChannelEvent) = subscribers.forEach {
        it.onEvent(event)
    }

    fun waitFor() {
        waitUntil { !scope.isStreamingInput && !scope.isProcessingInput }
        subscribers.clear()
    }

    fun readBinaryEvents(inputStream: InputStream, isRealtime: Boolean) {
        val buf = ByteArray(4000)
        val delay = if (isRealtime)
            0
        else
            1000L / (pipelineConfig.sttSampleRate * (pipelineConfig.sttEncoding.sampleSizeInBits / 8) / buf.size)
        while (true) {
            val count = inputStream.read(buf)
            if (count <= 0)
                break
            onEvent(BinaryEvent(if (count == buf.size) buf else buf.copyOfRange(0, count)))
            if (delay > 0)
                sleep(delay)
        }
    }

    fun onEvent(event: ChannelEvent) {
        when (event) {
            is BinaryEvent -> with(scope) {
                if (!isStreamingInput)
                    openStream(trace = pipelineConfig.trace)
                write(event.data, 0, event.data.size)
            }
            is InputStreamCloseEvent -> scope.closeStreams()
            is InputEvent -> with(scope) {
                closeStreams()
                process(event.input, event.trace ?: pipelineConfig.trace)
            }
            is VoteEvent -> turnRepository.find(ObjectId(event.turnId).toId())?.let { turn ->
                turn.votes[event.nodeId ?: "unknown"] = event.vote
                turnRepository.update(turn)
                indexer.save(turn)
                indexer.updateById(FullContext::class, event.turnId, "{\"turn\" : {\"votes\": {\"${event.nodeId}\":${event.vote}}}}")
            }
            is ClientEvent -> {
                val clientEvent = ai.flowstorm.core.model.ClientEvent(
                    type = event.type,
                    category = event.category,
                    text = event.text,
                    deviceId = pipelineConfig.deviceId,
                    sessionId = pipelineConfig.sessionId,
                    turnId = event.turnId,
                    nodeId = event.nodeId
                )
                clientEventRepository.create(clientEvent)
                indexer.save(clientEvent)
            }
            is LogEvent -> scope.addClientLogEntries(event.entries)
            else -> writeEvent(ErrorEvent("Unsupported event type: ${event::class.simpleName}", "Client"))
        }
    }

    fun subscribe(subscriber: ChannelEventSubscriber) {
        subscribers += subscriber
    }

    override fun onRecognizedInput(input: Input, isFinal: Boolean) = writeEvent(RecognizedInputEvent(input, isFinal))

    override fun onRecognitionError(error: Throwable, source: String) = writeEvent(ErrorEvent(error.message ?: "Unknown", source))

    override fun onResponseItem(item: Response.Item) =
        if (sendResponseItems)
            writeEvent(ResponseItemEvent(item))
        else
            responseItems += item

    override fun onResponseDone(logs: List<Log.Entry>, sessionEnded: Boolean, sleepTimeout: Int, turnId: String) {
        writeEvent(ResponseEvent(Response(logs, responseItems, sessionEnded, sleepTimeout, turnId)))
        responseItems.clear()
    }

    override fun onProcessError(obj: Any, e: Throwable) {
        writeEvent(ErrorEvent(e.message ?: e::class.qualifiedName ?: "unknown", obj::class.simpleName ?: "Unknown"))
    }

    override fun toString() = "${this::class.simpleName}@${hashCode().toString(16)}(sessionId=$sessionId)"
}