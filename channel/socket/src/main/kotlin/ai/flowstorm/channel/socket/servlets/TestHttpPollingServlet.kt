package ai.flowstorm.channel.socket.servlets

import ai.flowstorm.common.transport.Socket
import ai.flowstorm.common.model.Message
import ai.flowstorm.common.transport.SocketAdapter
import ai.flowstorm.common.servlets.HttpPollingServlet
import ai.flowstorm.concurrent.sleep

class TestHttpPollingServlet : HttpPollingServlet<Message, Message>(Message::class) {

    override fun createSocketAdapter(sid: String) = object : SocketAdapter<Message, Message> {
        override lateinit var socket: Socket<Message>
        override fun onOpen(socket: Socket<Message>) {
            println("$socket socket: Socket<Message>")
            sleep(1500)
            socket.write(Message("OPEN"))
            socket.write(listOf<Byte>(1, 2, 3).toByteArray())
        }
        override fun onClosed(socket: Socket<Message>) = println("$socket closed")
        override fun onObject(socket: Socket<Message>, obj: Message) {
            println("$socket object $obj")
            socket.write(obj)
        }
        override fun onBinary(socket: Socket<Message>, data: ByteArray) = println("$socket binary ${data.toList()}")
        override fun onFailure(socket: Socket<Message>, t: Throwable) {
            println("$socket failure")
            t.printStackTrace()
        }
    }
}