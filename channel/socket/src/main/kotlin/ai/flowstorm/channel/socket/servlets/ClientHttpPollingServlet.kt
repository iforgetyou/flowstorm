package ai.flowstorm.channel.socket.servlets

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.socket.adapter.WebSocketAdapter
import ai.flowstorm.common.servlets.HttpPollingServlet
import ai.flowstorm.common.servlets.serviceLocatorProvider

class ClientHttpPollingServlet : HttpPollingServlet<ChannelEvent, ChannelEvent>(ChannelEvent::class) {
    override fun createSocketAdapter(sid: String) = WebSocketAdapter().also {
        serviceLocatorProvider.serviceLocator.inject(it)
    }
}