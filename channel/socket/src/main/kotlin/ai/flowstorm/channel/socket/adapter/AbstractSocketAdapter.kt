package ai.flowstorm.channel.socket.adapter

import ai.flowstorm.channel.ChannelConfig
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.common.transport.SocketAdapter
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.core.nlp.pipeline.*
import ai.flowstorm.security.Digest
import ai.flowstorm.util.LoggerDelegate
import javax.inject.Inject

abstract class AbstractSocketAdapter<I : Any, O : Any> : SocketAdapter<I, O>, PipelineHandler {

    override lateinit var socket: Socket<O>

    @Inject
    lateinit var monitoring: Monitoring

    @Inject
    lateinit var pipeline: Pipeline

    protected lateinit var key: String
    protected lateinit var channelConfig: ChannelConfig
    protected var token: String? = null
    protected val logger by LoggerDelegate()
    protected var sessionId: String? = null
    protected var deviceId: String = "unknown"
    protected val scope: PipelineScope
        get() {
            if (scopeInstance == null || isScopeClosed) {
                val pipelineConfig = with(channelConfig) {
                    if (sessionId == null)
                        sessionId = createSessionId()
                    PipelineConfig(key, token, deviceId, sessionId!!, locale, zoneId.id, null, sttMode, sttModel, sttSampleRate, sttEncoding, ttsFileType)
                }
                scopeInstance = pipeline.createScope(pipelineConfig, this)
                logger.info("Created pipeline scope for session $sessionId")
                isScopeClosed = false
            }
            return scopeInstance!!
        }
    protected var scopeInstance: PipelineScope? = null
    protected var isScopeClosed = true
    protected val isChannelConfigured get() = ::channelConfig.isInitialized

    open fun createSessionId() = Digest.sha1()

    open fun closeScope() = if (scopeInstance != null) {
        scopeInstance?.close()
        isScopeClosed = true
        sessionId = null
        true
    } else false

    override fun onBinary(socket: Socket<O>, payload: ByteArray) {
        if (payload.isNotEmpty())
            scopeInstance?.write(payload, 0, payload.size)
    }

    override fun onOpen(socket: Socket<O>) {
    }

    override fun onFailure(socket: Socket<O>, t: Throwable) {
        t.printStackTrace()
    }

    override fun onClosed(socket: Socket<O>) {
        closeScope()
    }
}