package ai.flowstorm.channel.socket.adapter

import ai.flowstorm.channel.ChannelConfig
import ai.flowstorm.common.client.HttpUtil
import ai.flowstorm.common.transport.Socket
import ai.flowstorm.core.*
import ai.flowstorm.core.util.AudioUtil
import ai.flowstorm.util.AudioEncoding
import ai.flowstorm.util.Log
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.util.*

class TwilioSocketAdapter : AbstractSocketAdapter<TwilioSocketAdapter.InputMessage, TwilioSocketAdapter.OutputMessage>() {

    data class Mark(val name: String)

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "event")
    @JsonSubTypes(
        JsonSubTypes.Type(value = InputMessage.Connected::class, name = "connected"),
        JsonSubTypes.Type(value = InputMessage.Start::class, name = "start"),
        JsonSubTypes.Type(value = InputMessage.Stop::class, name = "stop"),
        JsonSubTypes.Type(value = InputMessage.Mark::class, name = "mark"),
        JsonSubTypes.Type(value = InputMessage.Media::class, name = "media")
    )
    open class InputMessage {
        data class Connected(val protocol: String, val version: String) : InputMessage()
        data class Start(val sequenceNumber: String, val start: Start, val streamSid: String) : InputMessage() {
            data class MediaFormat(val encoding: String, val sampleRate: Int, val channels: Int)
            data class Start(val accountSid: String, val streamSid: String, val callSid: String, val tracks: List<String>, val customParameters: Map<String, String>, val mediaFormat: MediaFormat)
        }
        data class Stop(val sequenceNumber: String) : InputMessage()
        data class Mark(val sequenceNumber: String, val mark: TwilioSocketAdapter.Mark) : InputMessage()
        data class Media(val sequenceNumber: String, val media: Media, val streamSid: String) : InputMessage() {
            data class Media(val track: String, val chunk: String, val timestamp: String, val payload: String) {
                val payloadBytes: ByteArray get() = Base64.getDecoder().decode(payload)
            }
        }
    }

    open class OutputMessage(val event: String) {
        data class Media(val streamSid: String, val media: Media) : OutputMessage("media") {
            class Media(bytes: ByteArray) {
                val payload = Base64.getEncoder().encodeToString(bytes)
            }
        }
        data class Clear(val streamSid: String) : OutputMessage("clear")
        data class Mark(val streamSid: String, val mark: TwilioSocketAdapter.Mark) : OutputMessage("mark")
    }

    private var streamSid: String? = null
    //private val outSound = getResourceBytes("/audio/out.mp3")

    override fun onRecognizedInput(input: Input, isFinal: Boolean) {
        if (scope.streamInput.textRecognized == 1) // first text recognized - clear media
            send(OutputMessage.Clear(streamSid!!))
        //if (isFinal)
        //    send(outSound)
    }

    override fun onRecognitionError(error: Throwable, source: String) {
        logger.error("Recognition error in $source", error)
    }

    override fun onResponseItem(item: Response.Item) {
        item.audio?.let { url ->
            HttpUtil.httpRequest(url)?.let {
                val size = send(it)
                // extend updated time by the length of media in order to make pipeline scope silence detection code work properly
                scope.streamInput.updatedTime += size * 1000 / channelConfig.sttSampleRate
            }
        }
    }

    override fun onResponseDone(logs: List<Log.Entry>, sessionEnded: Boolean, sleepTimeout: Int, turnId: String) {
        if (sessionEnded)
            send(OutputMessage.Mark(streamSid!!, Mark("Sleeping")))
    }

    override fun onProcessError(obj: Any, e: Throwable) {
        logger.error("Process error from ${obj::class.qualifiedName}", e.message)
    }

    override fun onObject(socket: Socket<OutputMessage>, message: InputMessage) {
        when (message) {
            is InputMessage.Start -> {
                streamSid = message.streamSid
                sessionId = message.start.callSid
                channelConfig = ChannelConfig(
                    Defaults.locale,
                    Defaults.zoneId,
                    SttMode.Continuous,
                    SttModel.PhoneCall,
                    8000,
                    AudioEncoding.MULAW
                )
                message.start.customParameters.let {
                    deviceId = it["deviceId"] ?: it["sender"] ?: "anonymous"
                    key = it["appKey"] ?: "flowstorm"
                    logger.info("Call from $deviceId")
                    with(scope) {
                        openStream()
                        process(input("#intro", channelConfig.locale), null)
                    }
                }
            }
            is InputMessage.Stop -> {
                socket.close()
            }
            is InputMessage.Mark -> {
                if (message.mark.name == "Sleeping")
                    socket.close()
            }
            is InputMessage.Media -> {
                val payload = message.media.payloadBytes
                scope.write(payload, 0, payload.size)
            }
        }
    }

    private fun send(message: OutputMessage) {
        logger.info("Sending $message")
        socket.write(message)
    }

    private fun send(data: ByteArray): Int {
        val payload = AudioUtil.convert(data, AudioFileType.mulaw)
        val message = OutputMessage.Media(streamSid!!, OutputMessage.Media.Media(payload))
        logger.info("Sending audio ${data.size} byte MP3 as ${payload.size} byte MULAW")
        send(message)
        return payload.size
    }
}