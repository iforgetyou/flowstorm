package ai.flowstorm.channel.socket.servlets

import ai.flowstorm.channel.Channel.sessionCookieName
import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.event.*
import ai.flowstorm.channel.socket.adapter.HttpSocketAdapter
import ai.flowstorm.common.ObjectUtil.defaultMapper as objectMapper
import ai.flowstorm.common.servlets.serviceLocatorProvider
import ai.flowstorm.common.transport.DataType
import ai.flowstorm.concurrent.waitUntil
import ai.flowstorm.core.*
import ai.flowstorm.core.indexer.RuntimeDataIndexer
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.nlp.pipeline.Pipeline
import ai.flowstorm.core.nlp.pipeline.PipelineConfig
import ai.flowstorm.core.repository.ClientEventRepository
import ai.flowstorm.core.repository.TurnRepository
import ai.flowstorm.core.util.AudioUtil
import ai.flowstorm.security.Digest
import ai.flowstorm.time.currentTime
import ai.flowstorm.time.zoneId
import ai.flowstorm.util.AudioEncoding
import ai.flowstorm.util.LoggerDelegate
import com.fasterxml.jackson.core.JsonFactory
import org.jetbrains.kotlin.utils.addToStdlib.ifNotEmpty
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.nio.charset.Charset
import java.util.*
import javax.inject.Inject
import javax.servlet.ServletConfig
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.sound.sampled.AudioSystem

class ClientHttpSocketServlet : HttpServlet() {

    companion object {
        private val jsonFactory = JsonFactory(objectMapper)
        private val adapters = mutableMapOf<String, HttpSocketAdapter>()
    }

    @Inject
    private lateinit var pipeline: Pipeline

    @Inject
    private lateinit var turnRepository: TurnRepository

    @Inject
    private lateinit var clientEventRepository: ClientEventRepository

    @Inject
    private lateinit var indexer: RuntimeDataIndexer

    private val logger by LoggerDelegate()

    private val HttpServletRequest.traceHeader get() = getHeader("sentry-trace")

    private fun HttpServletResponse.addSessionId(sessionId: String) =
        addCookie(Cookie(sessionCookieName, sessionId))

    private fun HttpServletRequest.getProperty(name: String) =
        getHeader("X-${name.replaceFirstChar(Char::uppercase)}") ?: getParameter(name)

    private val HttpServletRequest.adapter: HttpSocketAdapter get() {
        val sessionId = cookies?.firstOrNull { it.name == sessionCookieName }?.value ?: Digest.sha1()
        return adapters.getOrPut(sessionId) {
            HttpSocketAdapter(
                pipeline,
                PipelineConfig(
                    getProperty("key") ?: error("Missing key"),
                    getHeader("Authorization"),
                    getProperty("deviceId") ?: error("Missing device ID"),
                    sessionId,
                    getHeader("Accept-Language")?.let { Locale.forLanguageTag(it) } ?: Defaults.locale,
                    getProperty("timeZone") ?: Defaults.zoneId.id,
                    traceHeader,
                    getProperty("sttMode")?.let { SttMode.valueOf(it) } ?: SttMode.SingleUtterance,
                    getProperty("sttModel")?.let { SttModel.valueOf(it) } ?: SttModel.General,
                    getProperty("sttSampleRate")?.toInt() ?: Defaults.audioFormat.sampleRate,
                    getProperty("sttEncoding")?.let { AudioEncoding.valueOf(it) } ?: Defaults.audioFormat.encoding,
                    getProperty("ttsFileType")?.let { AudioFileType.valueOf(it) } ?: AudioFileType.mp3
                ),
                turnRepository,
                clientEventRepository,
                !listOf("0", "no", "false").contains(getProperty("sendResponseItems")),
                indexer
            )
        }
    }

    private fun OutputStream.writeValue(value: Any) {
        logger.info("Writing $value")
        write(
            if (value is String)
                value.toByteArray(Charset.forName("UTF-8"))
            else
                objectMapper.writeValueAsBytes(value)
        )
        write('\n'.code)
        flush()
    }

    private val ChannelEvent.isMain get() = this !is RecognizedInputEvent || isFinal

    override fun init(config: ServletConfig) {
        super.init(config)
        serviceLocatorProvider.serviceLocator.inject(this)
    }

    /**
     * POST method is used to handle specific types of events (currently ClientEvent)
     */
    override fun doPost(request: HttpServletRequest, response: HttpServletResponse) = with(request.adapter) {
        val event = objectMapper.readValue(jsonFactory.createParser(request.inputStream), ChannelEvent::class.java)
        onEvent(event)
    }

    /**
     * PUT method is used to establish main socket which is used to transmit various events and get response to them.
     * Client can send multiple Binary events containing audio data or Input event containing text. Audio data can be
     * also followed by text input when client user decides to type rather than speak. However, in that case client should
     * send InputAudioStreamClose event to notify server that no more audio data will come and therefore pipeline can
     * close writable streams processing it. As soon as pipeline recognizes final input it will send RecognizedInput
     * event (isFinal=true) followed by 0..n ResponseItem event(s) (if sendResponseItem property was set to true)
     * followed by Response event.
     * If any error occurs during turn processing Error event will be sent and request will close.
     * If clients want to receive interim and final results of STT it can use GET request at the same time.
     * In the near future, client will be allowed to send-stream different audio/video formats instead of sending
     * events in JSON format (e.g. WAV, MP3 or PCM for audio/MP4 for video). In case of realtime input streaming, when
     * client does not know the length of audio/video input in advance, it should always do GET request in parallel and
     * continue streaming until it gets final recognized input.
     */
    override fun doPut(request: HttpServletRequest, response: HttpServletResponse) = with(request.adapter) {
        val isPlainText = request.contentType == DataType.Text.contentType
        val isRealtime = listOf("1", "yes", "true").contains(request.getProperty("realtime"))
        logger.info("PUT $this reading")
        val writtenEvents = mutableListOf<ChannelEvent>()
        subscribe { event ->
            if (event.isMain) {
                if (!response.isCommitted) {
                    // write headers
                    logger.info("PUT $this writing")
                    if (event is ErrorEvent)
                        response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
                    response.contentType = (if (isPlainText) DataType.Text else DataType.Object).contentType
                    response.addSessionId(sessionId)
                }
                if (isPlainText) with (response.outputStream) {
                    // plain text output
                    when (event) {
                        is RecognizedInputEvent ->
                            writeValue("> ${event.input.transcript.text}")
                        is ResponseItemEvent ->
                            with(event.responseItem) {
                                val name = ttsConfig?.name ?: TtsConfig.forLanguage("en").name
                                mapOf("audio" to audio, "image" to image, "video" to video, "code" to code, "background" to background)
                                    .filter { !it.value.isNullOrBlank() }
                                    .map { "${it.key}=${it.value}" }
                                    .ifNotEmpty {
                                        writeValue(joinToString(", ", "# (", ")"))
                                    }
                                if (text != null)
                                    writeValue("< [$name] $text")
                            }
                        is ResponseEvent ->
                            if (event.response.sessionEnded)
                                writeValue(".")
                        is ErrorEvent ->
                            writeValue("! ${event.source}: ${event.text}")
                        else ->
                            writeValue("? $event")
                    }
                } else {
                    // event object output
                    response.outputStream.writeValue(event)
                }
                writtenEvents += event
            }
        }
        when (request.contentType) {
            DataType.Object.contentType -> {
                // event object input
                val readEvents = mutableListOf<ChannelEvent>()
                objectMapper.readValues(jsonFactory.createParser(request.inputStream), ChannelEvent::class.java)?.let {
                    while (it.hasNext() && !response.isCommitted) {
                        val event = it.next()
                        logger.info("PUT $this reading $event")
                        onEvent(event)
                    readEvents += event
                    }
                }
                logger.info("PUT $this read $readEvents")
            }
            DataType.Text.contentType -> {
                // plain text input
                val buf = ByteArrayOutputStream()
                request.inputStream.copyTo(buf)
                val text = buf.toString("UTF-8")
                onEvent(InputEvent(input(text, pipelineConfig.locale, zoneId(pipelineConfig.timeZone)), request.traceHeader))
            }
            DataType.PCM.contentType -> {
                readBinaryEvents(request.inputStream, isRealtime)
            }
            DataType.MP3.contentType,
            DataType.WAV.contentType -> {
                try {
                    //FIXME currently does not support realtime streaming and non-realtime simulates realtime
                    val buf = ByteArrayOutputStream()
                    request.inputStream.copyTo(buf)
                    val data = buf.toByteArray().let {
                        if (request.contentType == DataType.MP3.contentType)
                            AudioUtil.convert(it, AudioFileType.wav, AudioFileType.mp3)
                        else
                            it
                    }
                    val audioInputStream = AudioSystem.getAudioInputStream(ByteArrayInputStream(data))
                    val audioFormat = audioInputStream.format
                    if (audioFormat.channels == 1) {
                        pipelineConfig.sttSampleRate = audioFormat.sampleRate.toInt()
                        pipelineConfig.sttEncoding = AudioEncoding.valueOf(audioFormat.sampleSizeInBits)
                        readBinaryEvents(audioInputStream, false)
                    } else {
                        response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Media type ${request.contentType} can contain only 1 channel")
                    }
                } catch (e: Throwable) {
                    e.printStackTrace()
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.message)
                }
            }
            else -> {
                response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Unsupported media type: ${request.contentType}")
            }
        }
        waitFor()
        logger.info("PUT $this done ${response.status} written $writtenEvents")
    }

    /**
     * GET method is used to establish side socket which is used to receive turn interim events.
     * Client can open it in parallel to the PUT method in order to receive Recognized events containing
     * interim results (isFinal=false). Final result will be provided via PUT.
     * It is necessary to use it if client is streaming input data and does not know where to stop
     * (has no end of utterance/silence detection implemented). In that case, as soon as server has closed GET socket,
     * client has to stop data writing and start to read response from PUT socket.
     */
    override fun doGet(request: HttpServletRequest, response: HttpServletResponse) = with(request.adapter) {
        logger.info("GET $this writing")
        val writtenEvents = mutableListOf<ChannelEvent>()
        if (!listOf(DataType.Object.contentType, DataType.Text.contentType).contains(request.contentType)) {
            response.status = HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE
        } else {
            var isCompleted = false
            var lastTime = currentTime()
            response.contentType = request.contentType
            response.addSessionId(sessionId)
            subscribe { event ->
                lastTime = currentTime()
                if (!event.isMain) {
                    with(response.outputStream) {
                        if (request.contentType == DataType.Text.contentType) {
                            // plain text output
                            when (event) {
                                is RecognizedInputEvent ->
                                    writeValue("~ ${event.input.transcript.text}")
                                else ->
                                    writeValue("? $event")
                            }
                        } else {
                            // event object output
                            writeValue(event)
                        }
                    }
                    writtenEvents += event
                } else {
                    isCompleted = true
                }
            }
            waitUntil { isCompleted/* || lastTime + 5000 < currentTime()*/ }
            if (!isCompleted && !response.isCommitted)
                response.status = HttpServletResponse.SC_REQUEST_TIMEOUT
        }
        logger.info("GET $this done ${response.status} written $writtenEvents")
    }
}