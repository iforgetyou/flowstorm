package ai.flowstorm.core.dialogue

import ai.flowstorm.common.AppConfig
import ai.flowstorm.core.Context
import ai.flowstorm.core.model.DialogueEvent

abstract class BasicCzechDialogue <C : Context> : BasicDialogue<C>() {

    override fun evaluateTextTemplate(text: String) = super.evaluateTextTemplate(text).run {
        if (gender == "female") { //Inflections based on User Gender
            replace("l@", "la").
            replace("sám@", "sama").
            replace("ý@", "á").
            replace("@", "a")
        } else {
            replace("@", "")
        }. //Automatic addition of <s> tags
        replace(Regex("^"), "<s>").
        replace(Regex("$"), "</s>").
        replace(Regex("(?<=[\\!\\?])\\s+"), "</s> <s>").
        replace(Regex("(?<=\\.)\\s+(?=[A-ZĚŠČŘŽÝÁÍÉÚŮŤĎŇ])"), "</s> <s>")
    }

    //Nodes
    val _goBack = GoBack(_basicId++, repeat = true)

    val _basicVersionResponse = Response(_basicId++, false, { "Verze serveru ${AppConfig.version}, prostor ${AppConfig.namespace}, dialogový model $dialogueName verze $version" })
        .also { _basicId++ } // Additional increment for a deleted GlobalIntent to keep the same IDs for the rest of the nodes

    val _basicVolumeUpCommand = Command(_basicId++,  "volume", "up").also { _basicId++ }
    val _basicVolumeUpResponse = Response(_basicId++, false, { "zvyšuji hlasitost" })

    val _basicVolumeDownCommand = Command(_basicId++,  "volume", "down").also { _basicId++ }
    val _basicVolumeDownResponse = Response(_basicId++, false, { "snižuji hlasitost" })

    val _basicLogApplicationErrorResponse1 = Response(_basicId++, { "O co jde?" }).also { _basicId++ }
    val _basicLogApplicationErrorResponse2 = Response(_basicId++, false, { "Díky, pojďme zpátky." })
    val _basicLogApplicationErrorUserInput = UserInput(_basicId++, arrayOf(), arrayOf()) {
        val transition = Transition(_basicLogApplicationErrorResponse2)
        dialogueEvent = DialogueEvent(this, this@BasicCzechDialogue, DialogueEvent.Type.UserError, input.alternatives[0].text)
        transition
    }

    val _basicLogApplicationCommentResponse1 = Response(_basicId++, { "O co jde?" }).also { _basicId++ }
    val _basicLogApplicationCommentResponse2 = Response(_basicId++, false, { "Díky, pojďme zpátky." })
    val _basicLogApplicationCommentUserInput = UserInput(_basicId++, arrayOf(), arrayOf()) {
        val transition = Transition(_basicLogApplicationCommentResponse2)
        dialogueEvent = DialogueEvent(this, this@BasicCzechDialogue, DialogueEvent.Type.UserComment, input.alternatives[0].text)
        transition
    }

    init {
        _basicVersionResponse.next = _goBack

        _basicVolumeUpCommand.next = _basicVolumeUpResponse
        _basicVolumeUpResponse.next = _goBack

        _basicVolumeDownCommand.next = _basicVolumeDownResponse
        _basicVolumeDownResponse.next = _goBack

        _basicLogApplicationErrorResponse1.next = _basicLogApplicationErrorUserInput
        _basicLogApplicationErrorResponse2.next = _goBack

        _basicLogApplicationCommentResponse1.next = _basicLogApplicationCommentUserInput
        _basicLogApplicationCommentResponse2.next = _goBack
    }
}