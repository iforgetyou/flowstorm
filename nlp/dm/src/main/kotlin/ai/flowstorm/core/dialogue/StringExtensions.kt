package ai.flowstorm.core.dialogue

import ai.flowstorm.core.Input
import java.util.*

fun String.startsWithVowel() = Regex("[aioy].*").matches(this)

fun String.tokenize(keepPunctuation: Boolean = false): List<Input.Token> {
    val whitespace = " \t\n\r"
    val punctuation = ",.:;?![]"
    val tokens = mutableListOf<Input.Token>()
    val tokenizer = StringTokenizer(this, whitespace + punctuation, keepPunctuation)
    while (tokenizer.hasMoreTokens()) {
        val token = tokenizer.nextToken()
        if (token.isNotBlank()) {
            if (punctuation.contains(token)) {
                tokens.add(Input.Punctuation(token))
            } else {
                tokens.add(Input.Word(token.lowercase()))
            }
        }
    }
    return tokens
}

fun String.endsWith(suffixes: Collection<String>): Boolean {
    for (suffix in suffixes)
        if (endsWith(suffix))
            return true
    return false
}

infix fun String.similarityTo(tokens: List<Input.Token>): Double {
    val theseWords = tokenize().map { it.text.lowercase() }
    val thoseWords = tokens.map { it.text.lowercase() }
    var matches = 0
    for (word in theseWords)
        if (thoseWords.contains(word))
            matches++
    return matches / theseWords.size.toDouble()
}

infix fun String.similarityTo(input: Input) = similarityTo(input.words)

infix fun String.similarityTo(text: String) = similarityTo(text.tokenize())



