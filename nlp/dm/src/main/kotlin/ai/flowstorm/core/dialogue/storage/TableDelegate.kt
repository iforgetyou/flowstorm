package ai.flowstorm.core.dialogue.storage

import ai.flowstorm.common.ObjectUtil.defaultMapper as mapper
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.runtime.DialogueRuntime
import com.fasterxml.jackson.core.type.TypeReference
import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReaderBuilder
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStreamReader
import java.net.URL
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.full.primaryConstructor

class TableDelegate<R: Any>(private val path: String, private val clazz: KClass<R>, private val skipFirst: Boolean = true) {

    operator fun getValue(thisRef: AbstractDialogue<*>, property: KProperty<*>): List<R> {
        val buf = ByteArrayOutputStream()
        if (path.startsWith("http")) {
            URL(path).openStream().use { it.copyTo(buf) }
        } else {
            DialogueRuntime.run.context.file.readFile("assets/spaces/$path", buf)
        }
        val bytes = buf.toByteArray()
        val input = ByteArrayInputStream(bytes)
        return if (bytes[0].toChar() == '[') {
            // JSON array of row arrays
            val rows = mapper.readValue(input, object : TypeReference<Array<Array<Any>>>() {})
            return load(rows, skipFirst)
        } else {
            // CSV
            val csvReader = CSVReaderBuilder(InputStreamReader(input)).apply {
                withCSVParser(CSVParserBuilder().withSeparator(';').build())
                if (skipFirst)
                    withSkipLines(1)
            }.build()
            val rows = mutableListOf<Array<String>>()
            while (true) {
                val row = csvReader.readNext() ?: break
                rows.add(row)
            }
            load(rows.toTypedArray() as Array<Array<Any>>, false)
        }
    }

    private fun load(rows: Array<Array<Any>>, skipFirst: Boolean = true) = mutableListOf<R>().apply {
        val constructor = clazz.primaryConstructor ?: error("Missing primary constructor for $clazz")
        for (index in rows.indices) {
            if (index == 0 && skipFirst)
                continue
            val row = rows[index]
            if (row.size != constructor.parameters.size)
                error("Table row #$index contains ${row.size} item(s) but $clazz constructor requires ${constructor.parameters.size} parameter(s)")
            val obj = constructor.call(*rows[index])
            add(obj)
        }
    }
}