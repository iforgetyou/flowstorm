package ai.flowstorm.core.dialogue

import ai.flowstorm.common.ObjectUtil

fun Any.toJson() = ObjectUtil.defaultMapper.writeValueAsString(this)