package ai.flowstorm.core.dialogue

import com.fasterxml.jackson.core.type.TypeReference
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.core.Context
import ai.flowstorm.core.Response.Item as ResponseItem
import ai.flowstorm.core.Defaults.globalNamespace
import ai.flowstorm.core.dialogue.attribute.*
import ai.flowstorm.core.dialogue.metric.MetricDelegate
import ai.flowstorm.core.dialogue.storage.TableDelegate
import ai.flowstorm.core.model.ClientCommand
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.model.enumContains
import ai.flowstorm.core.runtime.Api
import ai.flowstorm.core.runtime.DialogueRuntime.run
import ai.flowstorm.core.type.*
import ai.flowstorm.core.type.entity.Person
import ai.flowstorm.core.type.profile.UserProfile
import ai.flowstorm.util.TextExpander
import java.io.File
import java.io.FileInputStream
import java.net.URL
import java.time.Duration
import kotlin.random.Random
import kotlin.reflect.full.memberProperties
import ai.flowstorm.core.dialogue.attribute.ContextualAttributeDelegate.Scope as ContextScope

abstract class BasicDialogue <C : Context> : AbstractDialogue<C>(), SelectorModel {

    companion object {

        const val LOW = 0
        const val MEDIUM = 1
        const val HIGH = 2

        val pass: Transition? = null

        @Deprecated("Use pass instead, toIntent will be removed")
        val toIntent = pass
        val api = Api // deprecated, newly built models should use api object from BasicContext
    }

    enum class SpeechDirection {
        Unknown, Front, FrontLeft, FrontRight, Left, Right, BackLeft, BackRight, Back
    }

    val now: DateTime get() = DateTime.now(run.context.turn.input.zoneId)
    val today get() = now.date
    val tomorrow get() = today + 1.day
    val yesterday get() = today - 1.day
    val DateTime.isToday get() = this isDay 0..0
    val DateTime.isTomorrow get() = this isDay 1..1
    val DateTime.isYesterday get() = this isDay -1..-1
    val DateTime.isHoliday get() = isWeekend // && TODO check holidays in context.turn.input.locale.country
    val DateTime.holidayName get() = null // && TODO check holidays in context.turn.input.locale.country
    val DateTime.isPast get() = !Duration.between(this, now).isNegative
    infix fun DateTime.isDay(range: IntRange) =
            this >= today + range.first.day && this < today + range.last.day + 1.day
    infix fun DateTime.isDay(day: Int) = this isDay day..day

    override val clientLocation get() = clientUserLocation.value
    val clientUserLocation by client { Memory(Location()) }
    var clientType by client { "unknown" }
    var clientScreen by client { false }
    var clientTemperature by client { -273.15 }
    var clientAmbientLight by client { 0.0 }
    var clientSpatialMotion by client { 0.0 }
    var clientSpeechAngle by client { -1 }
    var clientSpeechDetected by client { false }
    val clientSpeechDirection get() = clientSpeechAngle.let {
        when (it) {
            in 0..21 -> SpeechDirection.Right
            in 22..67 -> SpeechDirection.FrontRight
            in 68..111 -> SpeechDirection.Front
            in 112..157 -> SpeechDirection.FrontLeft
            in 158..201 -> SpeechDirection.Left
            in 202..247 -> SpeechDirection.BackLeft
            in 248..291 -> SpeechDirection.Back
            in 292..337 -> SpeechDirection.BackRight
            in 338..359 -> SpeechDirection.Right
            else -> SpeechDirection.Unknown
        }
    }
    var clientTurnError by client { "" }
    var serverTurnError by turn { "" }

    var gender by globalUser { "" }
    var nickname by globalUser(true) { user.nickname }
    var userProfile by globalUser { UserProfile(Person(session.user.name, turn.input.zoneId)) }

    var dynamic by turn { Dynamic.EMPTY }

    @Attribute("speakingRate")
    var turnSpeakingRate by globalTurn { 1.0 }

    @Attribute("speakingRate")
    var sessionSpeakingRate by globalSession { 1.0 }

    @Attribute("speakingRate")
    var userSpeakingRate by globalUser { 1.0 }

    @Attribute("speakingPitch")
    var turnSpeakingPitch by globalTurn { 0.0 }

    @Attribute("speakingPitch")
    var sessionSpeakingPitch by globalSession { 0.0 }

    @Attribute("speakingPitch")
    var userSpeakingPitch by globalUser { 0.0 }

    @Attribute("speakingVolumeGain")
    var turnSpeakingVolumeGain by globalTurn { 1.0 }

    @Attribute("speakingVolumeGain")
    var sessionSpeakingVolumeGain by globalSession { 1.0 }

    @Attribute("speakingVolumeGain")
    var userSpeakingVolumeGain by globalUser { 1.0 }

    override val selectorState by session { SelectorState() }

    @Attribute("nodeStates")
    val sessionNodeStates by session { mutableSetOf<NodeState>() }

    @Attribute("nodeStates")
    val userNodeStates by user { mutableSetOf<NodeState>() }

    var _basicId = 1
    val _dummyStopFunction = Function(_basicId++) { Transition(stopSession) } // Backward compatibility node

    inline fun <reified V: Any> temp(noinline default: (Context.() -> V)? = null) = TempAttributeDelegate(default)

    inline fun <reified V: Any> turn(namespace: String? = null, indexable: Boolean = false, noinline default: (Context.() -> V)) =
            ContextualAttributeDelegate(ContextScope.Turn, V::class, { namespace ?: dialogueNameWithoutVersion },null, indexable,  default)

    inline fun <reified V: Any> globalTurn(indexable: Boolean = false, noinline default: (Context.() -> V)) = turn(globalNamespace, indexable, default)

    inline fun <reified V: Any> session(namespace: String? = null, indexable: Boolean = false, noinline default: (Context.() -> V)) =
            ContextualAttributeDelegate(ContextScope.Session, V::class, { namespace ?: dialogueNameWithoutVersion }, null, indexable, default)

    inline fun <reified V: Any> globalSession(indexable: Boolean = false, noinline default: (Context.() -> V)) = session(globalNamespace, indexable, default)

    inline fun <reified V: Any> client(expiration: DateTimeUnit, indexable: Boolean = false, noinline default: (Context.() -> V)) =
            ContextualAttributeDelegate(ContextScope.Client, V::class, { globalNamespace }, expiration, indexable, default)

    inline fun <reified V: Any> client(indexable: Boolean = false, noinline default: (Context.() -> V)) = client(1.hour, indexable, default)

    inline fun <reified V: Any> user(namespace: String? = null, localize: Boolean = false, expiration: DateTimeUnit? = null, indexable: Boolean = false, noinline default: (Context.() -> V)) =
            ContextualAttributeDelegate(ContextScope.User, V::class, {
                (namespace ?: dialogueNameWithoutVersion) + (if (localize) "/$language" else "")
            }, expiration, indexable, default)

    inline fun <reified V: Any> user(namespace: String? = null, localize: Boolean = false, indexable: Boolean = false, noinline default: (Context.() -> V)) =
            user(namespace, localize, null, indexable, default)

    inline fun <reified V: Any> globalUser(localize: Boolean = false, expiration: DateTimeUnit? = null, indexable: Boolean = false, noinline default: (Context.() -> V)) =
            user(globalNamespace, localize, expiration, indexable, default)

    inline fun <reified V: Any> globalUser(localize: Boolean = false, indexable: Boolean = false, noinline default: (Context.() -> V)) =
            user(globalNamespace, localize, indexable, default)

    inline fun <reified V: Any> community(communityName: String, namespace: String? = null, localize: Boolean = false, noinline default: (Context.() -> V)) =
            CommunityAttributeDelegate(V::class, communityName, {
                (namespace ?: dialogueNameWithoutVersion) + (if (localize) "/$language" else "")
            }, default)

    inline fun <reified V: Any> globalCommunity(communityName: String, localize: Boolean = false, noinline default: (Context.() -> V)) =
            community(communityName, globalNamespace, localize, default)

    inline fun sessionSequence(list: List<String>, namespace: String? = null, noinline nextValue: (SequenceAttribute<String, String>.() -> String?) = { nextRandom() }) =
            StringSequenceAttributeDelegate(list, ContextScope.Session, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    inline fun userSequence(list: List<String>, namespace: String? = null, noinline nextValue: (SequenceAttribute<String, String>.() -> String?) = { nextRandom() }) =
            StringSequenceAttributeDelegate(list, ContextScope.User, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    inline fun <reified E: NamedEntity> sessionSequence(entities: List<E>, namespace: String? = null, noinline nextValue: (SequenceAttribute<E, String>.() -> E?) = { nextRandom() }) =
            EntitySequenceAttributeDelegate(entities, ContextScope.Session, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    inline fun <reified E: NamedEntity> userSequence(entities: List<E>, namespace: String? = null, noinline nextValue: (SequenceAttribute<E, String>.() -> E?) = { nextRandom() }) =
            EntitySequenceAttributeDelegate(entities, ContextScope.User, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    inline fun <reified E: NamedEntity> turnEntityList(entities: Collection<E>, namespace: String? = null) =
            NamedEntityListAttributeDelegate(entities, ContextScope.Turn) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: NamedEntity> sessionEntityList(entities: Collection<E>, namespace: String? = null) =
            NamedEntityListAttributeDelegate(entities, ContextScope.Session) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: NamedEntity> userEntityList(entities: Collection<E>, namespace: String? = null) =
            NamedEntityListAttributeDelegate(entities, ContextScope.User) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: NamedEntity> turnEntitySet(entities: Collection<E>, namespace: String? = null) =
            NamedEntitySetAttributeDelegate(entities, ContextScope.Turn) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: NamedEntity> sessionEntitySet(entities: Collection<E>, namespace: String? = null) =
            NamedEntitySetAttributeDelegate(entities, ContextScope.Session) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: NamedEntity> userEntitySet(entities: Collection<E>, namespace: String? = null) =
            NamedEntitySetAttributeDelegate(entities, ContextScope.User) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: Any> sessionMap(entities: Map<String, E>, namespace: String? = null) =
            MapAttributeDelegate(entities, ContextScope.Session) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: Any> turnMap(entities: Map<String, E>, namespace: String? = null) =
            MapAttributeDelegate(entities, ContextScope.Turn) { namespace ?: dialogueNameWithoutVersion }

    inline fun <reified E: Any> userMap(entities: Map<String, E>, namespace: String? = null) =
            MapAttributeDelegate(entities, ContextScope.User) { namespace ?: dialogueNameWithoutVersion }

    // deprecated delegates with *Attribute suffix in name
    @Deprecated("Use turn instead", replaceWith = ReplaceWith("turn"))
    inline fun <reified V: Any> turnAttribute(namespace: String? = null, noinline default: (Context.() -> V)) =
            ContextualAttributeDelegate(ContextScope.Turn, V::class, { namespace ?: dialogueNameWithoutVersion }, default)

    @Deprecated("Use session instead", replaceWith = ReplaceWith("session"))
    inline fun <reified V: Any> sessionAttribute(namespace: String? = null, noinline default: (Context.() -> V)) =
            ContextualAttributeDelegate(ContextScope.Session, V::class, { namespace ?: dialogueNameWithoutVersion }, default)

    @Deprecated("Use user instead", replaceWith = ReplaceWith("user"))
    inline fun <reified V: Any> userAttribute(namespace: String? = null, noinline default: (Context.() -> V)) =
            ContextualAttributeDelegate(ContextScope.User, V::class, { namespace ?: dialogueNameWithoutVersion }, default)

    @Deprecated("Use community instead", replaceWith = ReplaceWith("community"))
    inline fun <reified V: Any> communityAttribute(communityName: String, namespace: String? = null, noinline default: (Context.() -> V)) =
            CommunityAttributeDelegate(V::class, communityName, { namespace?:dialogueNameWithoutVersion }, default)

    @Deprecated("Use sessionSequence instead", replaceWith = ReplaceWith("sessionSequence"))
    inline fun sessionSequenceAttribute(list: List<String>, namespace: String? = null, noinline nextValue: (SequenceAttribute<String, String>.() -> String?) = { nextRandom() }) =
            StringSequenceAttributeDelegate(list, ContextScope.Session, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    @Deprecated("Use userSequence instead", replaceWith = ReplaceWith("userSequence"))
    inline fun userSequenceAttribute(list: List<String>, namespace: String? = null, noinline nextValue: (SequenceAttribute<String, String>.() -> String?) = { nextRandom() }) =
            StringSequenceAttributeDelegate(list, ContextScope.User, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    @Deprecated("Use sessionSequence instead", replaceWith = ReplaceWith("sessionSequence"))
    inline fun <reified E: NamedEntity> sessionSequenceAttribute(entities: List<E>, namespace: String? = null, noinline nextValue: (SequenceAttribute<E, String>.() -> E?) = { nextRandom() }) =
            EntitySequenceAttributeDelegate(entities, ContextScope.Session, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    @Deprecated("Use userSequence instead", replaceWith = ReplaceWith("userSequence"))
    inline fun <reified E: NamedEntity> userSequenceAttribute(entities: List<E>, namespace: String? = null, noinline nextValue: (SequenceAttribute<E, String>.() -> E?) = { nextRandom() }) =
            EntitySequenceAttributeDelegate(entities, ContextScope.User, { namespace ?: dialogueNameWithoutVersion }, nextValue)

    @Deprecated("Use turnEntityList instead", replaceWith = ReplaceWith("turnEntityList"))
    inline fun <reified E: NamedEntity> turnEntityListAttribute(entities: Collection<E>, namespace: String? = null) =
            NamedEntityListAttributeDelegate(entities, ContextScope.Turn) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use sessionEntityList instead", replaceWith = ReplaceWith("sessionEntityList"))
    inline fun <reified E: NamedEntity> sessionEntityListAttribute(entities: Collection<E>, namespace: String? = null) =
            NamedEntityListAttributeDelegate(entities, ContextScope.Session) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use userEntityList instead", replaceWith = ReplaceWith("userEntityList"))
    inline fun <reified E: NamedEntity> userEntityListAttribute(entities: Collection<E>, namespace: String? = null) =
            NamedEntityListAttributeDelegate(entities, ContextScope.User) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use turnEntitySet instead", replaceWith = ReplaceWith("turnEntitySet"))
    inline fun <reified E: NamedEntity> turnEntitySetAttribute(entities: Collection<E>, namespace: String? = null) =
            NamedEntitySetAttributeDelegate(entities, ContextScope.Turn) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use sessionEntitySet instead", replaceWith = ReplaceWith("sessionEntitySet"))
    inline fun <reified E: NamedEntity> sessionEntitySetAttribute(entities: Collection<E>, namespace: String? = null) =
            NamedEntitySetAttributeDelegate(entities, ContextScope.Session) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use userEntitySet instead", replaceWith = ReplaceWith("userEntitySet"))
    inline fun <reified E: NamedEntity> userEntitySetAttribute(entities: Collection<E>, namespace: String? = null) =
            NamedEntitySetAttributeDelegate(entities, ContextScope.User) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use sessionMap instead", replaceWith = ReplaceWith("sessionMap"))
    inline fun <reified E: Any> sessionMapAttribute(entities: Map<String, E>, namespace: String? = null) =
            MapAttributeDelegate(entities, ContextScope.Session) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use turnMap instead", replaceWith = ReplaceWith("turnMap"))
    inline fun <reified E: Any> turnMapAttribute(entities: Map<String, E>, namespace: String? = null) =
            MapAttributeDelegate(entities, ContextScope.Turn) { namespace ?: dialogueNameWithoutVersion }

    @Deprecated("Use userMap instead", replaceWith = ReplaceWith("userMap"))
    inline fun <reified E: Any> userMapAttribute(entities: Map<String, E>, namespace: String? = null) =
            MapAttributeDelegate(entities, ContextScope.User) { namespace ?: dialogueNameWithoutVersion }

    fun metric(metricSpec: String) = MetricDelegate(metricSpec)

    @Deprecated("Use metric instead", replaceWith = ReplaceWith("metric"))
    fun metricValue(metricSpec: String) = MetricDelegate(metricSpec)

    inline fun <T: Any> loader(path: String): Lazy<T> = lazy {
        val typeRef = object : TypeReference<T>() {}
        loadResource(path, typeRef)
    }

    inline fun <T: Any> loader(crossinline path: (BasicDialogue<C>.() -> String)) = lazy {
        val typeRef = object : TypeReference<T>() {}
        loadResource(path.invoke(this), typeRef)
    }

    fun <T: Any> loadResource(path: String, typeRef: TypeReference<T>): T =
        when {
            path.startsWith("file:///") ->
                FileInputStream(File(path.substring(7))).use {
                    ObjectUtil.defaultMapper.readValue<T>(it, typeRef)
                }
            path.startsWith("http") ->
                URL(path).openStream().use {
                    ObjectUtil.defaultMapper.readValue<T>(it, typeRef)
                }
            path.startsWith("./") ->
                loader.loadObject(dialogueId + path.substring(1).substringBeforeLast(".json"), typeRef)
            else ->
                loader.loadObject(path.substringBeforeLast(".json"), typeRef)
        }

    inline fun <reified T: Any> tableOf(path: String, skipFirst: Boolean = true) = TableDelegate(path, T::class, skipFirst)

//    This method was not used anywhere but it won't work since communityResource.get() requires space ID which is not accessible in this class
//    fun communityAttributes(communityName: String) =
//            run.context.communityResource.get(communityName, null)?.attributes ?: Dynamic.EMPTY
    fun newResponseItem(text: String?, image: String?, audio: String?, video: String?, code: String?, background: String?, repeatable: Boolean, ttsConfig: TtsConfig?, turnId: String, nodeId: Int, dialogueNodeId: String) =
            ResponseItem(text = text?.let { evaluateTextTemplate(it) }, image = image, audio = audio, video = video, code = code, background = background, repeatable = repeatable, ttsConfig = ttsConfig, turnId = turnId, nodeId = nodeId, dialogueNodeId = dialogueNodeId)

    fun addResponseItem(text: String?, image: String? = null, audio: String? = null, video: String? = null, code: String? = null, background: String? = null, repeatable: Boolean = true, ttsConfig: TtsConfig? = null) =
            run.context.pipelineScope.addResponseItem(ResponseItem(text = text?.let { evaluateTextTemplate(it) }, image = image, audio = audio, video = video, code = code, background = background, repeatable = repeatable, ttsConfig = ttsConfig ?: this.ttsConfig))

    fun addResponseItem(text: String?, image: String? = null, audio: String? = null, video: String? = null, code: String? = null, background: String? = null, repeatable: Boolean = true) =
            run.context.pipelineScope.addResponseItem(ResponseItem(text = text?.let { evaluateTextTemplate(it) }, image = image, audio = audio, video = video, code = code, background = background, repeatable = repeatable, ttsConfig = ttsConfig))

    fun addResponseItem(text: String?, image: String? = null, audio: String? = null, video: String? = null, background: String? = null, repeatable: Boolean = true) =
            addResponseItem(text, image, audio, video, null, background, repeatable)

    /**
     * evaluate # in response text
     */
    open fun evaluateTextTemplate(text: String) =
        if (text.matches(Regex("#[\\w\\-]+"))) // just command
            text
        else
            TextExpander.expand(text).run {
                enumerate(this[Random.nextInt(size)]).
                replace(Regex("#([\\w\\.\\d]+)")) {
                    if (enumContains<ClientCommand>(it.groupValues[1])) {
                        "#" + it.groupValues[1]
                    } else {
                        var obj: Any? = this@BasicDialogue
                        var point = false
                        for (name in it.groupValues[1].split(".")) {
                            if (name.isBlank()) {
                                point = true
                                break
                            } else {
                                val prop = obj!!.javaClass.kotlin.memberProperties.firstOrNull { it.name == name }
                                obj = prop?.call(obj)
                                if (obj == null)
                                    break
                            }
                        }
                        describe(obj) + (if (point) "." else "")
                    }
                }
            }

    inline fun unsupportedLanguage(): Nothing {
        val stackTraceElement = Thread.currentThread().stackTrace[1]
        throw error("${stackTraceElement.className}.${stackTraceElement.methodName} does not support language ${language} of dialogue ${dialogueName}")
    }

    fun indent(value: Any?, separator: String = " ") = (value?.let { separator + describe(value) } ?: "")

    infix fun Number.of(subj: String) =
            if (this == 0)
                empty(plural(subj, 0))
            else
                describe(this) + " " + plural(subj, this.toInt())

    infix fun Array<*>.of(subj: String) = size of subj

    infix fun Map<*, *>.of(subj: String) = size of subj

    infix fun Collection<String>.of(subj: String) = enumerate(this, subj)

    open val relevantNodeRefs get(): List<NodeRef> =
        selectableNodes.mapNotNull {
            it.selector?.let { selector ->
                selector.beforeSelect?.invoke(run.context, getNodeState(it.id, selector.scope))
            }
        }.ifEmpty {
            error("No relevant nodes for dialogue selector")
        }

    fun prefixedNodeRefs(prefix: String) = nodeMap
        .filterKeys { it.startsWith(prefix) }.values
        .filterIsInstance<SelectableNode>()
        .mapNotNull {
            it.selector?.let { selector ->
                selector.beforeSelect?.invoke(run.context, getNodeState(it.id, selector.scope))
            }
        }


    override fun getNodeState(id: Int, scope: Scope) =
        when (scope) {
            Scope.Session -> sessionNodeStates.find { it.id == id }
                ?: NodeState(id).also { sessionNodeStates.add(it) }
            Scope.User -> userNodeStates.find { it.id == id }
                ?: NodeState(id).also { userNodeStates.add(it) }
            else -> error("Unsupported SubDialogue State Scope - $scope")
        }

    fun selectTransition(ref: NodeRef?): Transition? {
        if (ref == null) {
            return null
        }
        selectorState.nodeRefs += ref
        getNodeState(ref.id, ref.scope).let { state ->
            state.count++
            state.lastTime = DateTime.now()
            selectableNodes.find { it.id == ref.id }?.selector?.afterSelect?.invoke(run.context, state)
        }
        return Transition(node(ref.id))
    }

    fun selectTransition(selector: Selector) = selectTransition(selector.select(this, relevantNodeRefs))

    fun selectTransition() = selectTransition(RandomSelector)

}
