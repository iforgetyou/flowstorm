package ai.flowstorm.core.dialogue

import ai.flowstorm.common.AppConfig
import ai.flowstorm.core.Context
import ai.flowstorm.core.model.DialogueEvent

abstract class BasicEnglishDialogue <C : Context> : BasicDialogue<C>() {

    //Nodes
    val _goBack = GoBack(_basicId++, repeat = true)

    val _basicVersionResponse = Response(_basicId++, false, { "Server version ${AppConfig.version}, space ${AppConfig.namespace}, dialogue model $dialogueName version $version" })
        .also { _basicId++ } // Additional increment for a deleted GlobalIntent to keep the same IDs for the rest of the nodes

    val _basicVolumeUpCommand = Command(_basicId++, "volume", "up").also { _basicId++ }
    val _basicVolumeUpResponse = Response(_basicId++, false, { "volume is up" })

    val _basicVolumeDownCommand = Command(_basicId++, "volume", "down").also { _basicId++ }
    val _basicVolumeDownResponse = Response(_basicId++, false, { "volume is down" })

    val _basicLogApplicationErrorResponse1 = Response(_basicId++, { "What's the problem?" }).also { _basicId++ }
    val _basicLogApplicationErrorResponse2 = Response(_basicId++, false, { "Thanks. Let's get back." })
    val _basicLogApplicationErrorUserInput = UserInput(_basicId++, arrayOf(), arrayOf()) {
        val transition = Transition(_basicLogApplicationErrorResponse2)
        dialogueEvent = DialogueEvent(this, this@BasicEnglishDialogue, DialogueEvent.Type.UserError, input.alternatives[0].text)
        transition
    }

    val _basicLogApplicationCommentResponse1 = Response(_basicId++, { "What's the comment?" }).also { _basicId++ }
    val _basicLogApplicationCommentResponse2 = Response(_basicId++, false, { "Thanks. Let's get back" })
    val _basicLogApplicationCommentUserInput = UserInput(_basicId++, arrayOf(), arrayOf()) {
        val transition = Transition(_basicLogApplicationCommentResponse2)
        dialogueEvent = DialogueEvent(this, this@BasicEnglishDialogue, DialogueEvent.Type.UserComment, input.alternatives[0].text)
        transition
    }

    init {
        _basicVersionResponse.next = _goBack

        _basicVolumeUpCommand.next = _basicVolumeUpResponse
        _basicVolumeUpResponse.next = _goBack

        _basicVolumeDownCommand.next = _basicVolumeDownResponse
        _basicVolumeDownResponse.next = _goBack

        _basicLogApplicationErrorResponse1.next = _basicLogApplicationErrorUserInput
        _basicLogApplicationErrorResponse2.next = _goBack

        _basicLogApplicationCommentResponse1.next = _basicLogApplicationCommentUserInput
        _basicLogApplicationCommentResponse2.next = _goBack
    }
}