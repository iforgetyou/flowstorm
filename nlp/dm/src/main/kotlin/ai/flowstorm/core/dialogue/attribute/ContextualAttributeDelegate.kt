package ai.flowstorm.core.dialogue.attribute

import ai.flowstorm.core.Context
import ai.flowstorm.core.dialogue.DateTimeUnit
import ai.flowstorm.core.runtime.DialogueRuntime
import ai.flowstorm.core.type.Memorable
import ai.flowstorm.core.type.Memory
import kotlin.reflect.KClass

class ContextualAttributeDelegate<V: Any>(
        private val scope: Scope,
        clazz: KClass<*>,
        namespace: (() -> String),
        expiration: DateTimeUnit? = null,
        indexable: Boolean,
        default: (Context.() -> V)
) : AttributeDelegate<V>(clazz, namespace, expiration, indexable, default) {

    // constructors for previously built models compatibility
    constructor(scope: Scope,
                clazz: KClass<*>,
                namespace: (() -> String),
                expiration: DateTimeUnit?,
                default: (Context.() -> V)) : this(scope, clazz, namespace, expiration, false, default)
    
    constructor(scope: Scope,
                clazz: KClass<*>,
                namespace: (() -> String),
                default: (Context.() -> V)) : this(scope, clazz, namespace, null, false, default)

    @Deprecated("use standalone enum class (requires bulk rebuild)", ReplaceWith("ai.flowstorm.core.dialogue.Scope"))
    enum class Scope { Turn, Session, User, Client }

    override fun attribute(namespace: String, name: String, init: (Memorable?) -> Memorable) = with (DialogueRuntime.run.context) {
        val attributes = (when (scope) {
            Scope.Client -> getAttributes(name)
            Scope.User -> profile.attributes
            Scope.Session -> session.attributes
            else -> turn.attributes
        })[namespace]
        init(attributes[name]).also {
            attributes[name] = it
            if (it is Memory<*>)
                it.indexable = indexable

        }
    }
}
