package ai.flowstorm.nlp.dm

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.*
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.dialogue.BasicDialogue
import ai.flowstorm.core.model.*
import ai.flowstorm.core.nlp.pipeline.DialogueManager
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.core.runtime.DialogueException
import ai.flowstorm.core.runtime.DialogueRuntime.run
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.Logger
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.SetAlso
import ai.flowstorm.util.use
import javax.inject.Inject
import kotlin.math.roundToInt
import ai.flowstorm.core.model.Session.DialogueStackFrame as Frame

class DialogueManagerV2 : DialogueManager {

    data class ProcessedNode(val node: AbstractDialogue<*>.Node, var time: Long)

    companion object {
        const val MAX_TRANSITIONS = 200
    }

    @Inject
    lateinit var dialogueFactory: DialogueFactory

    private val logger by LoggerDelegate()

    override fun process(scope: PipelineScope, span: PerformanceSpan) = use(scope) {
        context?.apply {
            this@DialogueManagerV2.logger.info("Processing DM")
            val contextClassLoader = Thread.currentThread().contextClassLoader
            Thread.currentThread().contextClassLoader = DialogueClassLoader(contextClassLoader ?: this::class.java.classLoader)
            try {
                proceed(this, span)
            } finally {
                Thread.currentThread().contextClassLoader = contextClassLoader
            }
        }
    }

    fun getIntentModels(currentFrame: Frame, context: Context): List<IntentModel> {
        val node = getNode(currentFrame, context.logger)
        require(node is AbstractDialogue.Input)

        val models = mutableListOf(
            IntentModel(
                node.dialogue.buildId,
                node.dialogue.dialogueId,
                node.id
            )
        )
        if (!node.skipGlobalIntents) {
            //current global intents
            models.add(IntentModel(node.dialogue.buildId, node.dialogue.dialogueId))
            //parents global intents
            context.session.dialogueStack.distinctBy { it.id } .forEach {
                val dialogue = dialogueFactory.get(it, context.logger)
                models.add(IntentModel(dialogue.buildId, dialogue.dialogueId))
            }
        }
        return models
    }

    private fun getEntityTypes(currentFrame: Frame, context: Context) =
        getIntentsByDialogueStack(currentFrame, context).map { it.entities }.flatten().toSet()

    override fun getUserInput(session: Session, logger: Logger): AbstractDialogue<*>.UserInput? {
        val node  = session.dialogueStack.firstOrNull()?.let { getNode(it, logger) }

        return when {
            node is AbstractDialogue.UserInput -> node
            node is AbstractDialogue.Enter && node.next is AbstractDialogue.UserInput -> node.next as AbstractDialogue.UserInput
            else -> null //dialogue starts with bot response
        }
    }

    private fun getIntentFrame(models: List<IntentModel>, frame: Frame, context: Context): Frame {
        val intentNodes = getIntentsByDialogueStack(frame, context)
        val recognizedEntities = context.input.entityMap.keys.filter { context.input.entityMap[it]?.isNotEmpty() ?: false }

        val intent = context.input.intents.firstOrNull { intent ->
            val (modelId, nodeId) = intent.name.split("#")
            val model = models.first { it.id == modelId }
            val requiredEntities = intentNodes.firstOrNull {
                it.dialogue.dialogueId == model.dialogueId && it.id == nodeId.toInt()
            }?.requiredEntities ?: listOf()
            recognizedEntities.containsAll(requiredEntities)
        }?: error("No intent for the given input and recognized entities $recognizedEntities found.")

        val (modelId, nodeId) = intent.name.split("#")
        val model = models.first { it.id == modelId }
        val dialogueName = model.dialogueId

        context.logger.info("IR match (model=${model.name}, id=${model.id}, answer=$nodeId, score=${context.input.intent.score})")

        return when {
            // intent is from current dialogue
            dialogueName == frame.id -> {
                frame.copy(nodeId = nodeId.toInt())
            }
            //intent is from parent dialogue
            context.session.dialogueStack.any { it.id == dialogueName } -> {
                context.session.dialogueStack.first { it.id == dialogueName }.copy(nodeId = nodeId.toInt())
            }
            else -> error("Dialogue $dialogueName matched by IR is not on current stack.")
        }
    }

    override fun <T: Any> getPropertyFromDialogueStack(session: Session, logger: Logger, callback: ((AbstractDialogue<*>) -> T?)): T? {
        for (frame in session.dialogueStack) {
            val dialogue = dialogueFactory.get(frame, logger)
            val value = callback(dialogue)
            if (value != null)
                return value
        }
        return null
    }

    private fun getIntentsByDialogueStack(currentFrame: Frame, context: Context): List<AbstractDialogue<*>.Intent> =
        context.session.dialogueStack.map { dialogueFactory.get(it, context.logger).globalIntents }.flatten() +
                dialogueFactory.get(currentFrame, context.logger).globalIntents +
                (getNode(currentFrame, context.logger) as AbstractDialogue.Input).intents

    private fun getActionFrame(frame: Frame, context: Context): Frame {
        val node = getNode(frame, context.logger) as? AbstractDialogue.Input
        val dialogue = dialogueFactory.get(frame, context.logger)

        val actions = (node?.actions?.toList() ?: emptyList()) + dialogue.globalActions
        actions.firstOrNull { it.action == context.input.action }?.let {
            return frame.copy(nodeId = it.id)
        }

        context.session.dialogueStack.distinctBy { it.id }.forEach { f ->
            dialogueFactory.get(f, context.logger).globalActions.firstOrNull { it.action == context.input.action }?.let {
                return f.copy(nodeId = it.id)
            }
        }
        error("Action ${context.input.action} not found in dialogue")
    }


    private fun getDialogue(frame: Frame, logger: Logger) = dialogueFactory.get(frame, logger)//.apply { context?.locale = locale }

    private fun getNode(frame: Frame, logger: Logger) = getDialogue(frame, logger).node(frame.nodeId)

    /**
     * @return true if next user input requested, false if conversation ended
     */
    private fun proceed(context: Context, span: PerformanceSpan): Unit = with(context) {
        var frame: Frame by SetAlso(session.dialogueStack.pop(), context::currentFrame)
        var node: AbstractDialogue<*>.Node
        var nodeSpan: PerformanceSpan? = null
        val processedNodes = mutableListOf<ProcessedNode>()
        var prevBuildId = frame.buildId
        try {
            loop@ while (true) {
                try {
                    if (processedNodes.size >= MAX_TRANSITIONS)
                        error("Too many steps (over $MAX_TRANSITIONS) in dialogue turn (${processedNodes.size})")
                    node = getNode(frame, context.logger)
                    if (node !is AbstractDialogue.UserInput && processedNodes.find { it.node == node } != null)
                        context.logger.warn("$node is repeating in turn")
                    nodeSpan?.finish()
                    nodeSpan = span.createChild("[${node.dialogue.dialogueName}:${node.dialogue.version}] $node", mutableMapOf(
                        "node_id" to node.id.toString(),
                        "build_id" to node.dialogue.buildId,
                        "dialogue_id" to node.dialogue.dialogueId
                    ))
                    nodeSpan.start()
                    val time = currentTime()
                    val processedNode = ProcessedNode(node, 0)
                    processedNodes.add(processedNode)
                    if (node.id < 0)
                        turn.attributes[Defaults.globalNamespace].set("nodeId", node.id)
                    if (turn.inputId != null && turn.nextId == null)
                        turn.nextId = node.id
                    if (frame.buildId != prevBuildId) {
                        turn.attributes.convertToSerializable()
                        session.attributes.convertToSerializable()
                        profile.attributes.convertToSerializable()
                    }
                    prevBuildId = frame.buildId

                    when (node) {
                        is AbstractDialogue.UserInput -> {
                            if (shouldProcessUserInput(processedNodes.map { it.node }, node)) {
                                turn.inputId = node.id
                                //first user input in turn
                                val intentModels = getIntentModels(frame, context)
                                context.intentModels = intentModels
                                // Store entity types used in the current intents
                                context.entityTypes = getEntityTypes(frame, context)
                                val transition = node.process(context)
                                frame = if (transition != null) {
                                    frame.copy(nodeId = transition.node.id)
                                } else {
                                    // process the rest of the pipeline components
                                    if (!isProcessed) context.processInput()
                                    if (errors.isNotEmpty())
                                        throw DialogueException(node, errors.first().second)
                                    if (context.input.action != null) {
                                        getActionFrame(frame, context)
                                    } else {
                                        getIntentFrame(intentModels, frame, context)
                                    }
                                }
                            } else {
                                //last user input in turn
                                frame.copy(nodeId = node.id).let {
                                    turn.endFrame = it
                                    session.dialogueStack.push(it)
                                }
                                break@loop
                            }
                        }
                        is AbstractDialogue.ReInput -> {
                            val intentModels = getIntentModels(frame, context)
                            context.intentModels = intentModels
                            context.entityTypes = getEntityTypes(frame, context)
                            val transition = node.process(context)
                            frame = if (transition != null) {
                                frame.copy(nodeId = transition.node.id)
                            } else {
                                context.processReInput()
                                if (context.input.action != null) {
                                    getActionFrame(frame, context)
                                } else {
                                    getIntentFrame(intentModels, frame, context)
                                }
                            }

                        }
                        is AbstractDialogue.Function -> {
                            val transition = node.exec(context)
                            frame = frame.copy(nodeId = transition.node.id)
                        }
                        is AbstractDialogue.End -> {
                            session.dialogueStack.clear()
                            break@loop
                        }
                        is AbstractDialogue.Sleep -> {
                            frame.copy(nodeId = node.next.id).let {
                                turn.endFrame = it
                                session.dialogueStack.push(it)
                            }
                            sleepTimeout = node.timeout
                            break@loop
                        }
                        is AbstractDialogue.GoBack -> {
                            if (session.dialogueStack.isEmpty()) {
                                break@loop
                            } else {
                                frame = session.dialogueStack.pop()
                                if (node.repeat) {
                                    val index = session.turns.indexOfLast { it.endFrame == frame }
                                    for (i in index downTo 0) {
                                        val repeatable = session.turns[i].responseItems.filter { it.repeatable }
                                        if (repeatable.isNotEmpty()) {
                                            repeatable.forEach { pipelineScope.addResponseItem(it) }
                                            break
                                        }
                                    }
                                }
                            }
                        }
                        is AbstractDialogue.Exit -> {
                            while (frame.id == node.dialogue.dialogueId) {
                                if (session.dialogueStack.isEmpty()) {
                                    break@loop
                                }
                                frame = session.dialogueStack.pop()
                            }
                        }
                        is AbstractDialogue.SubDialogue -> {
                            val args = node.getConstructorArgs(context)
                            session.dialogueStack.push(frame.copy(nodeId = node.next.id))
                            val buildId = dialogueFactory.getLatestBuildId(node.dialogueId, args)
                            frame = Frame(node.dialogueId, buildId, args, 0)
                        }
                        is AbstractDialogue.TransitNode -> {
                            when (node) {
                                is AbstractDialogue.Response -> {
                                    with(node) {
                                        val text = getText(context)
                                        val nodeBackground = node.background ?: ""
                                        val dialogueBackground = nodeBackground.ifEmpty { dialogue.background ?: "" }
                                        val background = dialogueBackground.ifEmpty { getPropertyFromDialogueStack(session, context.logger) { it.background } }
                                        val ttsConfig = if (text.isNullOrBlank()) null else
                                            ttsConfig
                                                ?: dialogue.ttsConfig
                                                ?: getPropertyFromDialogueStack(session, context.logger) { it.ttsConfig }
                                                ?: TtsConfig.forLanguage(dialogue.language)
                                        pipelineScope.addResponseItem(
                                            if (dialogue is BasicDialogue) {
                                                run(context, this) {
                                                    (dialogue as BasicDialogue).newResponseItem(text, image, audio, video, node.getCode(context), background, isRepeatable, ttsConfig, input.id.toString(), id, "${dialogue.dialogueId}#$id")
                                                }
                                            } else {
                                                Response.Item(text, null, 1.0, image, audio, video, node.getCode(context), background, isRepeatable, ttsConfig, input.id.toString(), "${dialogue.dialogueId}#$id", id)
                                            }
                                        )
                                    }
                                }
                                is AbstractDialogue.Command -> {
                                    pipelineScope.addResponseItem(Response.Item(text = '#' + node.command, code = node.getCode(context), repeatable = false, nodeId = node.id, dialogueNodeId = "${node.dialogue.dialogueId}#${node.id}"))
                                }
                                is AbstractDialogue.GlobalIntent,
                                is AbstractDialogue.GlobalAction -> {
                                    if (session.turns.isNotEmpty()) //it is empty only when GlobalIntent/Action is reached in first turn(UInput right after start)
                                        session.dialogueStack.push(session.turns.last().endFrame)
                                }
                            }
                            frame = frame.copy(nodeId = node.next.id)
                        }
                    }
                    processedNode.time = currentTime() - time
                } catch (e: DialogueException) {
                    nodeSpan?.abort(e)
                    context.createDialogueEvent(e)
                    context.logger.error("${e::class.simpleName}: ${e.message}" + (e.cause?.let { " caused by ${it::class.simpleName}: ${it.message}" } ?: ""))
                    context.input.action = "error"
                    context.turn.attributes[Defaults.globalNamespace].set("serverTurnError", e.cause!!::class.simpleName ?: "Unknown")
                    try {
                        frame = getActionFrame(frame, context)
                    } catch (fe: IllegalStateException) { // action error not found, let's throw original exception
                        throw e
                    }
                }
            }
        } finally {
            nodeSpan?.finish()
            logNodes(processedNodes, logger)
            saveFlow(processedNodes.map { it.node }, context.turn)
        }
    }

    private fun shouldProcessUserInput(processedNodes: List<AbstractDialogue<*>.Node>, node: AbstractDialogue<*>.UserInput): Boolean =
            when (processedNodes.size) {
                // first user input in the turn
                1 -> processedNodes[0] == node
                // user input next to start node
                2 -> (processedNodes[0] is AbstractDialogue<*>.Enter && processedNodes[1] == node)
                else -> false
            }

    private fun logNodes(nodes: List<ProcessedNode>, logger: Logger) {
        if (nodes.isEmpty()) return
        var dialogueNodes: List<ProcessedNode>
        var rest = nodes
        do {
            dialogueNodes = rest.takeWhile { it.node.dialogue.dialogueName == rest.first().node.dialogue.dialogueName }
            logger.info("Flow >> [${dialogueNodes.first().node.dialogue.dialogueName}:${dialogueNodes.first().node.dialogue.version}](${dialogueNodes.first().node.dialogue.dialogueId}) >> " +
                    dialogueNodes.map { "${it.node} in ${it.time} ms" }.reduce { acc, s -> "$acc > $s" })
            rest = rest.drop(dialogueNodes.size)
        } while (rest.isNotEmpty())
    }

    private fun saveFlow(nodes: List<AbstractDialogue<*>.Node>, turn: Turn) {
        nodes.forEach {
            turn.flow.add(Turn.FlowNode(it.dialogue.dialogueId, it.dialogue.dialogueName, it.dialogue.version,
                                        it.stringName, it.id))
        }
    }

    override fun getExpectedPhrases(intents: Array<out AbstractDialogue<*>.Intent>): List<ExpectedPhrase> {
        val expectedPhrases = mutableListOf<ExpectedPhrase>()
        if (intents.isNotEmpty()) {
            //note: google has limit 5000 (+100k per whole ASR request), we use lower value to be more comfortable with even longer phrases
            val maxPhrasesPerIntent = 2000 / intents.size
            intents.forEach { intent ->
                if (intent.utterances.size > maxPhrasesPerIntent) {
                    val rat = intent.utterances.size / maxPhrasesPerIntent.toFloat()
                    var idx = 0.0F
                    for (i in 0 until maxPhrasesPerIntent) {
                        expectedPhrases.add(ExpectedPhrase(intent.utterances[idx.roundToInt()]))
                        idx += rat
                    }
                } else {
                    expectedPhrases.addAll(intent.utterances.map { text -> ExpectedPhrase(text) })
                }
            }
        }

        logger.info("${expectedPhrases.size} expected phrase(s) added")
        return expectedPhrases
    }
}
