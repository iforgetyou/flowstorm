package ai.flowstorm.nlp.dm

import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.model.Session.DialogueStackFrame
import ai.flowstorm.core.runtime.Loader
import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.util.Logger
import ai.flowstorm.util.LoggerDelegate
import javax.inject.Inject

class DialogueFactory {

    @Inject
    lateinit var loader: Loader
    private val logger by LoggerDelegate()
    private val dialogues: MutableMap<Triple<String?, String, PropertyMap>, AbstractDialogue<*>> = mutableMapOf()

    private fun create(id: String, buildId: String, args: PropertyMap, logger: Logger): AbstractDialogue<*> {
        "Creating dialogue instance $id $args".let {
            logger.info(it)
            this.logger.info(it)
        }
        val dialogue = loader.newObjectWithArgs<AbstractDialogue<*>>("$id/builds/$buildId/model", args)
        dialogue.loader = loader
        dialogue.validate()
        return dialogue
    }

    fun get(id: String, buildId: String, args: PropertyMap, logger: Logger): AbstractDialogue<*> {
        this.logger.debug("Getting dialogue instance $id $args")
        val triple = Triple(id, buildId, args)
        return dialogues.getOrPut(triple) {create(triple.first, triple.second, triple.third, logger)}
    }

    fun get(frame: DialogueStackFrame, logger: Logger): AbstractDialogue<*> =
            get(frame.id ?: frame.name ?: error("Missing dialogue frame identification"), frame.buildId, frame.args, logger)

    fun getLatestBuildId(id: String, args: PropertyMap) =
        loader.getMetadata("$id/model")["buildId"].also {
            logger.info("Found latest buildId=$it for dialogueId=$id")
        } ?: error("Missing buildId in metadata")
}
