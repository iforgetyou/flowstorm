group = "ai.flowstorm.nlp"
description = "Flowstorm NLP DM"

val kotlinVersion = findProperty("kotlin.version")

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-common-app"))
    implementation(project(":flowstorm-core-lib"))
    implementation(project(":flowstorm-storage-local"))
    implementation(project(":flowstorm-connector-lib"))
    implementation(project(":flowstorm-connector-reddit"))
    implementation("com.opencsv:opencsv:5.4")
    implementation("org.jetbrains.kotlin:kotlin-script-runtime:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-script-util:$kotlinVersion")
}

