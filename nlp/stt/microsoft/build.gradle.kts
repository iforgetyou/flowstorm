group = "ai.flowstorm.nlp"
description = "Flowstorm NLP STT Microsoft"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-nlp-stt-api"))
    implementation("com.microsoft.cognitiveservices.speech", "client-sdk", "1.16.0", ext = "jar")
}

