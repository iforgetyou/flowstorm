package ai.flowstorm.nlp.stt.microsoft

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStreamFactory
import ai.flowstorm.core.nlp.stt.SttStream
import com.microsoft.cognitiveservices.speech.SpeechConfig
import java.net.URI

class MicrosoftSttStreamFactory : SttStreamFactory {

    @ConfigValue("mscs.location", "westeurope")
    lateinit var location: String

    @ConfigValue("mscs.key", "")
    lateinit var key: String

    @ConfigValue("mscs.stt.host", "")
    lateinit var host: String

    override fun create(config: SttStreamConfig, callback: SttCallback, expectedPhrases: List<ExpectedPhrase>): SttStream {
        val speechConfig = if (host.isNotBlank()) {
            if (key.isNotBlank())
                SpeechConfig.fromHost(URI(host), key)
            else
                SpeechConfig.fromHost(URI(host))
        } else {
            SpeechConfig.fromSubscription(key, location)
        }
        speechConfig.requestWordLevelTimestamps()
        return MicrosoftSttStream(speechConfig, config, callback, expectedPhrases)
    }
}