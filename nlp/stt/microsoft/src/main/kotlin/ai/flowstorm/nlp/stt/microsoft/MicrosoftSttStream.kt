package ai.flowstorm.nlp.stt.microsoft

import ai.flowstorm.common.ObjectUtil.defaultMapper
import ai.flowstorm.core.*
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStream
import ai.flowstorm.util.ByteBuffer
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.withSupportedCountry
import com.fasterxml.jackson.annotation.JsonProperty
import com.microsoft.cognitiveservices.speech.*
import com.microsoft.cognitiveservices.speech.audio.AudioConfig
import com.microsoft.cognitiveservices.speech.audio.AudioInputStream
import com.microsoft.cognitiveservices.speech.audio.AudioStreamFormat

class MicrosoftSttStream(
    private val speechConfig: SpeechConfig,
    override val config: SttStreamConfig,
    callback: SttCallback,
    expectedPhrases: List<ExpectedPhrase>
) : SttStream {

    data class Result(
        @JsonProperty("Id") val id: String,
        @JsonProperty("DisplayText") val displayText: String,
        @JsonProperty("RecognitionStatus") val recognitionStatus: String,
        @JsonProperty("Offset") val offset: Long,
        @JsonProperty("Duration") val duration: Long,
        @JsonProperty("NBest") val alternatives: List<Alternative>
    ) {
        data class Alternative(
            @JsonProperty("Confidence") val confidence: Float,
            @JsonProperty("Display") val display: String,
            @JsonProperty("ITN") val itn: String,
            @JsonProperty("Lexical") val lexical: String,
            @JsonProperty("MaskedITN") val maskedItn: String,
            @JsonProperty("Words") val words: List<Word>
        )

        data class Word(
            @JsonProperty("Word") val word: String,
            @JsonProperty("Offset") val offset: Long,
            @JsonProperty("Duration") val duration: Long,
        )
    }

    override val locale = config.locale.withSupportedCountry(mapOf(
        "en" to listOf("US", "AU", "CA", "GH", "HK", "IN", "IE", "KE", "NZ", "NG", "PH", "SG", "ZA", "TZ", "GB"),
        "sp" to listOf("ES", "AR", "BO", "CL", "CO", "CR", "CU", "DO", "EC", "SV", "GQ", "GT", "HN", "MX", "NI", "PA", "PY", "PE", "PR", "UY", "US", "WE"),
        "fr" to listOf("FR", "CA", "CH"),
        "de" to listOf("DE", "AT", "CH"),
        "pt" to listOf("PT", "BR")
    ))
    override val isOpen get() = isRunning
    private val logger by LoggerDelegate()
    private val pushStream = AudioInputStream.createPushStream(
        AudioStreamFormat.getWaveFormatPCM(config.sampleRate.toLong(), config.encoding.sampleSizeInBits.toShort(), 1)
    )
    private val audioInput = AudioConfig.fromStreamInput(pushStream)
    private var isRunning = false
    private val buffer = ByteBuffer()
    private val speechRecognizer = SpeechRecognizer(speechConfig, locale.toLanguageTag(), audioInput).apply {

        val phraseList = PhraseListGrammar.fromRecognizer(this)
        expectedPhrases.forEach {
            phraseList.addPhrase(it.text)
        }

        recognizing.addEventListener { _, e: SpeechRecognitionEventArgs ->
            val input = input(e.result.text, locale, provider = config.sttConfig.provider)
            logger.debug("Interim $input")
            if (isRunning)
                callback.onRecognized(this@MicrosoftSttStream, input, false)
        }

        recognized.addEventListener { _: Any, e: SpeechRecognitionEventArgs ->
            when (e.result.reason) {
                ResultReason.RecognizedSpeech -> {
                    val json = e.result.properties.getProperty(PropertyId.SpeechServiceResponse_JsonResult)
                    with (defaultMapper.readValue(json, Result::class.java)) {
                        val input = Input().also {
                            it.locale = locale
                            it.transcript = Transcript(
                                alternatives[0].display,
                                alternatives[0].confidence,
                                config.sttConfig.provider,
                                locale
                            )
                        }
                        input.tokens += alternatives[0].words.map {
                            Input.Word(it.word,
                                startTime = it.offset.toFloat() / 10000000,
                                endTime = (it.offset + it.duration).toFloat() / 10000000
                            )
                        }
                        input.alternatives += alternatives.drop(1).map {
                            Transcript(it.display, it.confidence, config.sttConfig.provider, locale)
                        }
                        logger.debug("Final $input")
                        if (isRunning)
                            callback.onRecognized(this@MicrosoftSttStream, input, true)
                    }
                }
                ResultReason.NoMatch -> {}
                else -> logger.warn("Unhandled event result reason: ${e.result.reason}")
            }
        }

        canceled.addEventListener { _, e: SpeechRecognitionCanceledEventArgs ->
            if (e.reason == CancellationReason.Error) {
                logger.error("Error (code=${e.errorCode}, details=${e.errorDetails})")
                callback.onError(this@MicrosoftSttStream, Exception(e.errorDetails))
            } else {
                logger.warn("Canceled for reason ${e.reason}")
            }
        }

        sessionStarted.addEventListener { _, _ ->
            logger.info("Session started")
            isRunning = true
            callback.onOpen(this@MicrosoftSttStream)
        }

        sessionStopped.addEventListener { _, _ ->
            logger.info("Session stopped")
            isRunning = false
            callback.onClosed(this@MicrosoftSttStream)
        }
    }

    init {
        speechRecognizer.startContinuousRecognitionAsync().get()
    }

    override fun write(data: ByteArray, offset: Int, count: Int) = with(buffer) {
        write(data, offset, count)
        read(4096, false) { block, _ ->
            pushStream.write(block)
        }
    }

    override fun close() {
        isRunning = false
        //speechRecognizer.stopContinuousRecognitionAsync().get() //commented out because of blocking for several seconds
        speechRecognizer.close()
        speechConfig.close()
        audioInput.close()
    }

    override fun toString() = "${this::class.simpleName}(locale=$locale, config=$config)"
}