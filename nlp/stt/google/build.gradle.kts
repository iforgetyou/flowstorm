group = "ai.flowstorm.nlp"
description = "Flowstorm NLP STT Google"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-nlp-stt-api"))
    implementation("com.google.cloud:google-cloud-speech:1.22.1")
}

