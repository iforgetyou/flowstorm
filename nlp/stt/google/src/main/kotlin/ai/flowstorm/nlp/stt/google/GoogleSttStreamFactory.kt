package ai.flowstorm.nlp.stt.google

import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStreamFactory

class GoogleSttStreamFactory : SttStreamFactory {

    override fun create(config: SttStreamConfig, callback: SttCallback, expectedPhrases: List<ExpectedPhrase>) =
        GoogleSttStream(config, callback, expectedPhrases)
}