package ai.flowstorm.nlp.stt.google

import ai.flowstorm.core.ExpectedPhrase
import com.google.api.gax.rpc.ResponseObserver
import com.google.api.gax.rpc.StreamController
import ai.flowstorm.core.Input
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.Transcript
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStream
import ai.flowstorm.util.*
import com.google.cloud.speech.v1.*
import com.google.protobuf.ByteString
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread

class GoogleSttStream(
    override val config: SttStreamConfig,
    private val callback: SttCallback,
    expectedPhrases: List<ExpectedPhrase> = listOf(),
    interimResults: Boolean = true
) : SttStream, ResponseObserver<StreamingRecognizeResponse> {

    override val locale = config.locale.withSupportedCountry(mapOf(
        "en" to listOf("US", "AU", "CA", "GH", "HK", "IN", "IE", "KE", "NZ", "NG", "PK", "PH", "SG", "ZA", "TZ", "GB"),
        "sp" to listOf("ES", "AR", "BO", "CL", "CO", "CR", "DO", "EC", "SV", "GT", "HN", "MX", "NI", "PA", "PY", "PE", "PR", "US", "UY", "VE"),
        "fr" to listOf("FR", "BE", "CA", "CH"),
        "de" to listOf("DE", "AT", "CH"),
        "pt" to listOf("PT", "BR"),
        "it" to listOf("IT", "CH")
    ))
    override var isOpen = true
    private val logger by LoggerDelegate()
    private val client = SpeechClient.create()
    private val clientStream = client.streamingRecognizeCallable().splitCall(this)

    init {
        val recognitionConfig = RecognitionConfig.newBuilder().apply {
            encoding = when (config.encoding) {
                AudioEncoding.MULAW -> RecognitionConfig.AudioEncoding.MULAW
                else -> RecognitionConfig.AudioEncoding.LINEAR16
            }
            languageCode = locale.toLanguageTag()
            sampleRateHertz = config.sampleRate
            maxAlternatives = 5
            enableAutomaticPunctuation = true
            enableWordTimeOffsets = true
            addSpeechContexts(
                SpeechContext.newBuilder()
                    .addAllPhrases(expectedPhrases.filter { (it.text?.length ?: 100) < 100 }.map { it.text })
                    .build()
            )
            model = config.model.googleName
        }.build()
        val streamingRecognitionConfig =
            StreamingRecognitionConfig.newBuilder().also {
                it.config = recognitionConfig
                it.interimResults = interimResults
            }.build()
        val request = StreamingRecognizeRequest.newBuilder()
            .setStreamingConfig(streamingRecognitionConfig)
            .build() // The first request in a streaming call has to be a configuration
        clientStream.send(request)
    }

    override fun onStart(controller: StreamController) {
        callback.onOpen(this)
    }

    override fun onResponse(response: StreamingRecognizeResponse?) {
        if (!isOpen || response == null)
            return

        for (result in response.resultsList) {
            val alternatives = result.alternativesList ?: continue
            val input = Input().also {
                it.locale = locale
                it.transcript = Transcript(alternatives[0].transcript.trim(), alternatives[0].confidence, config.sttConfig.provider, locale)
            }
            input.tokens += alternatives[0].wordsList.map {
                Input.Word(it.word,
                    startTime = it.startTime.seconds.toFloat() + it.startTime.nanos.toFloat() / 1000000000,
                    endTime = it.endTime.seconds.toFloat() + it.endTime.nanos.toFloat() / 1000000000
                )
            }
            input.alternatives += alternatives.drop(1).map {
                Transcript(it.transcript.trim(), it.confidence, config.sttConfig.provider, locale)
            }
            logger.debug((if (result.isFinal) "Final" else "Interim") + " $input")
            callback.onRecognized(this, input, result.isFinal)
        }
    }

    override fun onComplete() {
    }

    override fun onError(t: Throwable) = callback.onError(this, t)

    override fun write(data: ByteArray, offset: Int, count: Int) {

        val request = StreamingRecognizeRequest.newBuilder()
            .setAudioContent(ByteString.copyFrom(data, offset, count))
            .build()
        clientStream.send(request)
    }

    override fun close() {
        if (isOpen) {
            isOpen = false
            clientStream.closeSend()
            thread {
                try {
                    client.awaitTermination(5, TimeUnit.SECONDS)
                    client.close()
                } catch (e: Throwable) {
                    logger.error("Termination", e)
                }
            }
            callback.onClosed(this)
        }
    }

    override fun toString() = "${this::class.simpleName}(locale=$locale, config=$config)"
}