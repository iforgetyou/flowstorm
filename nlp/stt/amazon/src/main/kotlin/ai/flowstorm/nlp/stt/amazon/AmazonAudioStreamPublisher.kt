package ai.flowstorm.nlp.stt.amazon

import ai.flowstorm.concurrent.sleep
import ai.flowstorm.util.ByteBuffer as Buffer
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import software.amazon.awssdk.core.SdkBytes
import software.amazon.awssdk.services.transcribestreaming.model.AudioEvent
import software.amazon.awssdk.services.transcribestreaming.model.AudioStream
import software.amazon.awssdk.services.transcribestreaming.model.TranscribeStreamingException
import java.nio.ByteBuffer
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicLong

class AmazonAudioStreamPublisher(private val buffer: Buffer) : Publisher<AudioStream> {

    private val executor = Executors.newFixedThreadPool(1)
    private val demand = AtomicLong(0)
    var isOpen = true

    override fun subscribe(subscriber: Subscriber<in AudioStream>) = subscriber.onSubscribe(
        object : Subscription {
            override fun request(n: Long) {
                if (n <= 0)
                    subscriber.onError(IllegalArgumentException("Demand must be positive"))
                demand.getAndAdd(n)
                executor.submit {
                    try {
                        do {
                            while (isOpen && buffer.count < 1024) {
                                sleep(5)
                            }
                            if (isOpen) {
                                val audioBytes = buffer.read(1024)
                                val audioBuffer = ByteBuffer.wrap(audioBytes, 0, audioBytes.size)
                                val audioEvent = AudioEvent.builder()
                                    .audioChunk(SdkBytes.fromByteBuffer(audioBuffer))
                                    .build()
                                subscriber.onNext(audioEvent)
                            } else {
                                subscriber.onComplete()
                                break
                            }
                        } while (demand.decrementAndGet() > 0)
                    } catch (e: TranscribeStreamingException) {
                        subscriber.onError(e)
                    }
                }
            }

            override fun cancel() {}
        }
    )
}