package ai.flowstorm.nlp.stt.amazon

import ai.flowstorm.core.Input
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStream
import ai.flowstorm.util.*
import software.amazon.awssdk.services.transcribestreaming.TranscribeStreamingAsyncClient
import software.amazon.awssdk.services.transcribestreaming.model.*

class AmazonSttStream(
    client: TranscribeStreamingAsyncClient,
    override val config: SttStreamConfig,
    private val callback: SttCallback,
) : SttStream {

    override val locale = config.locale.withSupportedCountry(mapOf(
        "en" to listOf("US", "AU", "GB"),
        "fr" to listOf("FR", "CA"),
        "sp" to listOf("US"),
        "pt" to listOf("BR")
    ))
    override val isOpen get() = audioStreamPublisher.isOpen
    private val logger by LoggerDelegate()
    private val buffer = ByteBuffer()
    private val audioStreamPublisher = AmazonAudioStreamPublisher(buffer)
    private val responseHandler: StartStreamTranscriptionResponseHandler get() =
        StartStreamTranscriptionResponseHandler.builder()
            .onResponse {
                logger.info("Response")
            }
            .onError { e: Throwable ->
                callback.onError(this, e)
            }
            .onComplete {
                logger.info("Completed")
            }
            .subscriber { event: TranscriptResultStream ->
                val results = (event as TranscriptEvent).transcript().results()
                if (isOpen && results.isNotEmpty()) {
                    val result = results[0]
                    val alternatives = result.alternatives()
                    val input = Input().also {
                        it.locale = locale
                        it.transcript = Input.Transcript(alternatives[0].transcript(), provider = config.sttConfig.provider, locale = locale)
                    }
                    input.tokens += alternatives[0].items().mapNotNull {
                        when (it.type()) {
                            ItemType.PRONUNCIATION -> {
                                Input.Word(
                                    it.content(),
                                    startTime = it.startTime().toFloat(),
                                    endTime = it.endTime().toFloat(),
                                    confidence = it.confidence()?.toFloat() ?: 0.0F,
                                    speakerId = it.speaker()
                                )
                            }
                            else -> null
                        }
                    }
                    input.alternatives += alternatives.drop(1).map {
                        Input.Transcript(it.transcript(), provider = config.sttConfig.provider, locale = locale)
                    }
                    logger.debug((if (result.isPartial) "Interim" else "Final") + " $input")
                    callback.onRecognized(this, input, !result.isPartial)
                }
            }
            .build()

    init {
        client.startStreamTranscription(
            StartStreamTranscriptionRequest.builder()
                .languageCode(locale.toLanguageTag())
                .mediaEncoding(MediaEncoding.PCM)
                .mediaSampleRateHertz(config.sampleRate)
                .build(),
            audioStreamPublisher,
            responseHandler
        )
        callback.onOpen(this)
    }

    override fun write(data: ByteArray, offset: Int, count: Int) {
        if (isOpen)
            buffer.write(data, offset, count)
    }

    override fun close() {
        if (isOpen) {
            audioStreamPublisher.isOpen = false
            callback.onClosed(this)
        }
    }

    override fun toString() = "${this::class.simpleName}(locale=$locale, config=$config)"
}