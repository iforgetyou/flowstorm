package ai.flowstorm.nlp.stt.amazon

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStreamFactory
import software.amazon.awssdk.auth.credentials.AwsCredentials
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.transcribestreaming.TranscribeStreamingAsyncClient

class AmazonSttStreamFactory : SttStreamFactory {

    @ConfigValue("aws.access-key", "")
    lateinit var accessKey: String

    @ConfigValue("aws.secret-key", "")
    lateinit var secretKey: String

    @ConfigValue("aws.region", "eu-central-1")
    lateinit var region: String

    private val client by lazy {
        with (TranscribeStreamingAsyncClient.builder()) {
            if (accessKey.isNotBlank() && secretKey.isNotBlank())
                credentialsProvider {
                    object : AwsCredentials {
                        override fun accessKeyId() = accessKey
                        override fun secretAccessKey() = secretKey
                    }
                }
            region(Region.of(region))
            build()
        }
    }

    override fun create(config: SttStreamConfig, callback: SttCallback, expectedPhrases: List<ExpectedPhrase>) =
        AmazonSttStream(client, config, callback)
}
