group = "ai.flowstorm.nlp"
description = "Flowstorm NLP STT Amazon"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-nlp-stt-api"))
    implementation("software.amazon.awssdk:transcribestreaming:2.17.100")
}
