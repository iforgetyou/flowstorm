package ai.flowstorm.nlp.stt

enum class SttProvider { Google, Amazon, Microsoft, Deepgram }