group = "ai.flowstorm.nlp"
description = "Flowstorm NLP STT API"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-core-lib"))
}

