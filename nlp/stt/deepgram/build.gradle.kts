group = "ai.flowstorm.nlp"
description = "Flowstorm NLP STT Deepgram"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-nlp-stt-api"))
    implementation(project(":flowstorm-common-client"))
}
