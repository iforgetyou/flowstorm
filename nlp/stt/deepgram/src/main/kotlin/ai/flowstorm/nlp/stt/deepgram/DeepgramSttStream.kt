package ai.flowstorm.nlp.stt.deepgram

import ai.flowstorm.common.transport.Socket
import ai.flowstorm.common.client.WebSocketClient
import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.Input
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStream
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.*
import java.net.URLEncoder
import kotlin.properties.Delegates

class DeepgramSttStream(
    key: String,
    override val config: SttStreamConfig,
    private val callback: SttCallback,
    expectedPhrases: List<ExpectedPhrase> = listOf(),
    alternatives: Int = 4,
    numerals: Boolean = true,
    private val openingBuffering: Boolean = true,
    private val blockSize: Int = 20000
) : SttStream {

    inner class SocketListener : Socket.Listener<DeepgramMessage, DeepgramMessage> {

        private val logger by LoggerDelegate()

        override fun onOpen(socket: Socket<DeepgramMessage>) {
            logger.debug("$socket open in ${currentTime() - openTime} ms")
            if (!openingBuffering) {
                isOpen = true
                callback.onOpen(this@DeepgramSttStream)
            }
        }

        override fun onClosed(socket: Socket<DeepgramMessage>) {
            logger.debug("$socket closed")
            isOpen = false
            callback.onClosed(this@DeepgramSttStream)
        }

        override fun onObject(socket: Socket<DeepgramMessage>, obj: DeepgramMessage) = with(obj) {
            val alternatives = channel.alternatives.sortedByDescending { it.confidence }
            if (isOpen && alternatives[0].transcript.isNotBlank()) {
                val input = Input()
                input.locale = locale
                input.transcript = Input.Transcript(alternatives[0].transcript, alternatives[0].confidence, config.sttConfig.provider, locale)
                input.tokens += alternatives[0].words.map {
                    Input.Word(it.punctuated_word ?: it.word,
                        startTime = it.start,
                        endTime = it.end,
                        confidence = it.confidence,
                        speakerId = it.speaker.toString()
                    )
                }
                input.alternatives += alternatives.drop(1).map {
                    Input.Transcript(it.transcript, it.confidence, config.sttConfig.provider, locale)
                }
                logger.debug((if (is_final) "Final" else "Interim") + " $input")
                callback.onRecognized(this@DeepgramSttStream, input, is_final)
            }
        }

        override fun onBinary(socket: Socket<DeepgramMessage>, data: ByteArray) = error("$socket binary not supported")

        override fun onFailure(socket: Socket<DeepgramMessage>, t: Throwable) = callback.onError(this@DeepgramSttStream, t)
    }

    override val locale = config.locale.withSupportedCountry(mapOf(
        "en" to listOf("US", "GB", "IN", "NZ"),
        "fr" to listOf("FR", "CA"),
        "pt" to listOf("PT", "BR")
    ))
    override var isOpen = false
    private var openTime by Delegates.notNull<Long>()
    private val buffer = ByteBuffer()
    private val socket = WebSocketClient<DeepgramMessage, DeepgramMessage>(
        StringBuilder("wss://api.deepgram.com/v1/listen?endpointing=true")
            .append("&language=").append(locale.toLanguageTag())
            .append("&model=").append(config.model.name.lowercase())
            .append("&encoding=").append(config.encoding.name.lowercase())
            .append("&sample_rate=").append(config.sampleRate)
            .append("&vad_turnoff=").append(config.sttConfig.maxPause * 100)
            .append("&interim_results=true")
            .append("&punctuate=").append(true)
            .append("&diarize=").append(true)
            .append("&numerals=").append(numerals)
            .append("&alternatives=").append(alternatives)
            .run {
                expectedPhrases.sortedByDescending { it.boost }.take(200).forEach {
                    append("&keywords=" + URLEncoder.encode(it.text, "UTF-8"))
                }
                toString()
            }
        , DeepgramMessage::class, mapOf("Authorization" to "Token $key")
    ).apply {
        listener = SocketListener()
        openTime = currentTime()
    }

    init {
        socket.open()
        if (openingBuffering) {
            this@DeepgramSttStream.isOpen = true
            callback.onOpen(this@DeepgramSttStream)
        }
    }

    override fun write(data: ByteArray, offset: Int, count: Int) {
        buffer.write(data, offset, count)
        if (socket.isConnected) {
            buffer.read(blockSize, false) { block, _ ->
                socket.write(block)
            }
        }
    }

    override fun close() {
        isOpen = false
        socket.close()
    }

    override fun toString() = "${this::class.simpleName}(locale=$locale, config=$config)"
}