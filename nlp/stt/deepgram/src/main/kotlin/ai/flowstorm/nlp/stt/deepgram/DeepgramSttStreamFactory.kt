package ai.flowstorm.nlp.stt.deepgram

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStreamFactory

class DeepgramSttStreamFactory : SttStreamFactory {

    @ConfigValue("deepgram.key")
    lateinit var key: String

    override fun create(config: SttStreamConfig, callback: SttCallback, expectedPhrases: List<ExpectedPhrase>) =
        DeepgramSttStream(key, config, callback, expectedPhrases)
}