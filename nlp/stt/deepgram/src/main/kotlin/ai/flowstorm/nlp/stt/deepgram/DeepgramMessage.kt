package ai.flowstorm.nlp.stt.deepgram

data class DeepgramMessage(
    val channel_index: List<Int>,
    val duration: Float,
    val start: Float,
    val channel: Channel,
    val metadata: Metadata,
    val is_final: Boolean,
    val speech_final: Boolean = false
) {
    data class Channel(
        val alternatives: List<Alternative>
    ) {
        data class Alternative(
            val transcript: String,
            val confidence: Float,
            val words: List<Word>
        )

        data class Word(
            val word: String,
            val start: Float,
            val end: Float,
            val confidence: Float,
            val speaker: Int = 0,
            val punctuated_word: String? = null
        )
    }

    data class Metadata(
        val request_id: String
    )
}
