import torch
from transformers import AutoModelForSequenceClassification, GPT2TokenizerFast


class DialogRPT:
    def __init__(self, model, cuda=False, max_input_tokens=-1):
        self.model = AutoModelForSequenceClassification.from_pretrained(model)
        self.tokenizer = GPT2TokenizerFast.from_pretrained(model)
        self.tokenizer.pad_token = self.tokenizer.eos_token
        self.max_input_tokens = max_input_tokens

        self.cuda = cuda
        if self.cuda:
            self.model.to('cuda')

    def score(self, context, messages):
        inputs = [context + " " + self.tokenizer.pad_token + " " + message for message in messages]
        model_input = self.tokenizer(inputs, return_tensors="pt", padding=True)
        model_input = model_input["input_ids"]

        if -1 < self.max_input_tokens < model_input.shape[0] * model_input.shape[1]:
            message_limit = int(self.max_input_tokens / len(messages))
            to_cut = int((len(model_input[0]) - message_limit) / 2)
            if to_cut == 0:
                to_cut = 1
            model_input = model_input[:, to_cut: -to_cut]

        if self.cuda:
            model_input = model_input.to('cuda')
        result = self.model(model_input, return_dict=True)
        return torch.sigmoid(result.logits).squeeze(dim=1).tolist()
