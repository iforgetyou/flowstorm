import math
import json

import argparse
from tqdm import tqdm

from training import language_models
from training.data.switchboard.switchboard import Switchboard


class GenerateExamples:

    def __init__(self):
        self.tags = list(Switchboard().tags.keys())

    def generate(self, generator_model, n_examples):
        generated = {}
        for dialogue_act in tqdm(self.tags):
            texts = []
            for _ in tqdm(range(math.ceil(n_examples / 8))):
                output = generator_model.generate_response([dialogue_act], number_of_responses=8)
                bos_token = generator_model.tokenizer.bos_token
                eos_token = generator_model.tokenizer.eos_token
                texts += [text.split(bos_token, 1)[1].split(eos_token, 1)[0].strip() for text in output]
            generated[Switchboard().tag_to_name(dialogue_act)] = texts[:n_examples]
        return generated


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--generator_model', type=str, required=True)
    parser.add_argument('--n_examples', type=int, default=100)
    args = parser.parse_args()

    generator_model = language_models.Model(args.generator_model)
    generated = GenerateExamples().generate(generator_model, args.n_examples)

    with open("generated.json", "w", encoding="utf-8") as out_file:
        json.dump(generated, out_file, indent=4)
