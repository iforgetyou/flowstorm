import argparse
import math
from functools import reduce
from operator import mul

from tqdm import tqdm

from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import matplotlib.pyplot as plt

from training import classifier, language_models
import numpy as np
from training.analysis.pair_sequences import get_da_turn_histogram
from training.data.switchboard.switchboard import Switchboard


class Evaluate:

    def __init__(self):
        '''self.tags = [("%", 0.01), ("^2", 0.004), ("^g", 0.001), ("^h", 0.003), ("^q", 0.005), ("aa", 0.05),
                     ("aap_am", 0.001), ("ad", 0.004), ("ar", 0.002), ("arp_nd", 0.001), ("b", 0.19), ("b^m", 0.003),
                     ("ba", 0.02), ("bd", 0.001), ("bf", 0.005), ("bh", 0.01), ("bk", 0.01), ("br", 0.001),
                     ("fa", 0.001), ("fc", 0.01), ("fo_o_fw_\"_by_bc", 0.01), ("fp", 0.001), ("ft", 0.001), ("h", 0.01),
                     ("na", 0.004), ("ng", 0.001), ("nn", 0.01), ("no", 0.001), ("ny", 0.01), ("oo_co_cc", 0.001),
                     ("qh", 0.002), ("qo", 0.003), ("qrr", 0.001), ("qw", 0.01), ("qw^d", 0.01), ("qy ()", 0.02),
                     ("qy^d", 0.01), ("sd", 0.36), ("sv", 0.13), ("t1", 0.001), ("t3", 0.001), ("x", 0.02)]'''
        '''self.tags = [("sd", 0.36), ("b", 0.19), ("sv", 0.13), ("aa", 0.05), ("%", 0.01), ("ba", 0.02), ("qy ()", 0.02),
                     ("fc", 0.01), ("qw", 0.01), ("nn", 0.01), ("h", 0.01), ("bh", 0.01)]'''

    def reshape(self, lst, shape):
        if len(shape) == 1:
            return lst
        n = reduce(mul, shape[1:])
        return [self.reshape(lst[i * n:(i + 1) * n], shape[1:]) for i in range(len(lst) // n)]

    def argmax(self, matrix):
        argmaxs = []
        for vector in matrix:
            argmaxs.append(max(zip(vector, range(len(vector))))[1])
        return argmaxs

    def chunks(self, lst, n):
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    def classify(self, classifier_model, texts):
        outputs = []
        for batch in self.chunks(texts, 8):
            output = classifier_model.predict(batch)
            outputs += output
        return outputs

    def test_downstream_task(self, n_test_examples):
        y_true = []
        y_pred = []

        for dialogue_act in tqdm(tags):
            y_pred_tag = []
            for _ in tqdm(range(math.ceil(n_test_examples / 8))):
                texts = generator_model.generate_response([dialogue_act], number_of_responses=8)
                bos_token = generator_model.tokenizer.bos_token
                eos_token = generator_model.tokenizer.eos_token
                texts = [text.split(bos_token, 1)[1].split(eos_token, 1)[0].strip() for text in texts]
                response = evaluate.classify(classifier_model, texts)
                tag_idxs = evaluate.argmax(response)
                result = [Switchboard().id_to_tag(idx) for idx in tag_idxs]
                y_pred_tag += result

            y_pred += y_pred_tag[:n_test_examples]
            y_true += n_test_examples * [dialogue_act]

        downstream_confusion_matrix = confusion_matrix(y_true, y_pred, labels=tags)
        return downstream_confusion_matrix

    def test_downstream_task_joined_turn(self, n_test_examples):
        length_true = []
        length_pred = []
        y_true = []
        y_pred = []
        histogram = get_da_turn_histogram(labels=True)
        dialogue_acts = histogram.keys()
        dialogue_acts = [x.split(", ") for x in dialogue_acts]
        for acts in dialogue_acts[0:100]:
            predicted = []
            for _ in tqdm(range(math.ceil(n_test_examples / 8))):
                texts = generator_model.generate_response(acts, number_of_responses=8)
                bos_token = generator_model.tokenizer.bos_token
                eos_token = generator_model.tokenizer.eos_token
                sep_token = generator_model.tokenizer.sep_token
                texts = [text.split(bos_token, 1)[1].split(eos_token, 1)[0].strip() for text in texts]
                generated_turns = [[x.strip() for x in text.split(sep_token)] for text in texts]
                length_pred += [len(turns) for turns in generated_turns]
                length_true += [len(acts)] * len(generated_turns)
                generated_turns = [turns for turns in generated_turns if len(turns) == len(acts)]
                generated_turns = [j for sub in generated_turns for j in sub]
                response = evaluate.classify(classifier_model, generated_turns)
                tag_idxs = evaluate.argmax(response)
                result = [Switchboard().id_to_tag(idx) for idx in tag_idxs]
                predicted += result

            predicted = [predicted[i:i + len(acts)] for i in range(0, len(predicted), len(acts))][:n_test_examples]
            y_pred += predicted
            y_true += len(predicted) * [acts]
        self.length_confusion_matrix(length_true, length_pred)
        self.calculate_metrics(y_true, y_pred)

    def length_confusion_matrix(self, length_true, length_pred):
        tags = list(range(1, max(length_true + length_pred) + 1))
        confusion_matrix_position = confusion_matrix(length_true, length_pred, labels=tags, normalize="true")
        fig, ax = plt.subplots()
        disp = ConfusionMatrixDisplay(confusion_matrix=confusion_matrix_position, display_labels=tags)
        plt.title('Confusion Matrix Lengths')
        disp.plot(xticks_rotation="vertical", ax=ax)
        plt.show()

    def calculate_metrics(self, y_true, y_pred):
        accuracies_turns = {}
        for true, predicted in zip(y_true, y_pred):
            true_key = ", ".join(true)
            if true_key not in accuracies_turns:
                accuracies_turns[true_key] = []
            if true == predicted:
                accuracies_turns[true_key].append(1)
            else:
                accuracies_turns[true_key].append(0)
        for key in accuracies_turns:
            accuracies_turns[key] = sum(accuracies_turns[key]) / len(accuracies_turns[key])

        print("| Turn | Acc |")
        print("|---|---|")
        for key in accuracies_turns:
            print("| {} | {:0.2f} |".format(key, accuracies_turns[key]))

        y_true_flat = [j for sub in y_true for j in sub]
        y_pred_flat = [j for sub in y_pred for j in sub]
        confusion_matrix_all = confusion_matrix(y_true_flat, y_pred_flat, labels=tags, normalize="true")
        accuracy_all = sum([1 if true == pred else 0 for true, pred in zip(y_true_flat, y_pred_flat)]) / len(
            y_true_flat)

        fig, ax = plt.subplots(figsize=(25, 25))
        disp = ConfusionMatrixDisplay(confusion_matrix=confusion_matrix_all,
                                      display_labels=list(Switchboard().tags.values()))
        disp.plot(xticks_rotation="vertical", ax=ax)
        plt.title('Confusion Matrix All')
        plt.show()

        accuracies_positions = []
        for i in range(max([len(x) for x in y_true])):
            y_true_position = [x[i] for x in y_true if len(x) > i]
            y_pred_position = [x[i] for x in y_pred if len(x) > i]
            accuracies_positions.append(
                sum([1 if true == pred else 0 for true, pred in zip(y_true_position, y_pred_position)]) / len(
                    y_true_position))
            confusion_matrix_position = confusion_matrix(y_true_position, y_pred_position, labels=tags,
                                                         normalize="true")

            fig, ax = plt.subplots(figsize=(25, 25))
            disp = ConfusionMatrixDisplay(confusion_matrix=confusion_matrix_position,
                                          display_labels=list(Switchboard().tags.values()))
            plt.title('Confusion Matrix Position {}'.format(i + 1))
            disp.plot(xticks_rotation="vertical", ax=ax)
            plt.show()

        print("| Position | Acc |")
        print("|---|---|")
        for position, acc in enumerate(accuracies_positions):
            print("| {} | {:0.2f} |".format(position, acc))
        print("| ALL | {:0.2f} |".format(accuracy_all))

    def generator_accuracy(self, downstream_confusion_matrix, classifier_confusion_matrix):
        downstream_confusion_matrix = downstream_confusion_matrix / downstream_confusion_matrix.sum()
        classifier_confusion_matrix = classifier_confusion_matrix / classifier_confusion_matrix.sum()

        upper_bounds = []
        lower_bounds = []
        downstream_tasks = []
        for i in range(len(classifier_confusion_matrix)):
            downstream_in_not_out = downstream_confusion_matrix.sum(axis=1)[i] - downstream_confusion_matrix[i][i]
            downstream_in = downstream_confusion_matrix.sum(axis=1)[i]
            downstream_in_out = downstream_confusion_matrix[i][i]

            classifier_in_not_out = classifier_confusion_matrix.sum(axis=1)[i] - classifier_confusion_matrix[i][i]
            classifier_in = classifier_confusion_matrix.sum(axis=1)[i]
            classifier_not_in_out = classifier_confusion_matrix.sum(axis=0)[i] - downstream_confusion_matrix[i][i]
            classifier_not_in = classifier_confusion_matrix.sum() - classifier_confusion_matrix.sum(axis=1)[i]

            upper_bound = (downstream_in_out / downstream_in) + (downstream_in_not_out / downstream_in) * (
                    classifier_in_not_out / classifier_in)
            lower_bound = (downstream_in_out / downstream_in) - (downstream_in_out / downstream_in) * (
                    classifier_not_in_out / classifier_not_in)
            downstream_task = downstream_in_out / downstream_in

            upper_bounds.append(upper_bound)
            lower_bounds.append(lower_bound)
            downstream_tasks.append(downstream_task)
        return lower_bounds, downstream_tasks, upper_bounds

    def print_generator_accuracy(self, lower_bound, downstream_tasks, upper_bounds):
        print("| DA | Downstream Task Acc | Lower Bound Acc | Upper Bound Acc |")
        print("|---|---|---|---|")
        for i, tag in enumerate(list(Switchboard().tags.values())):
            print("| {} | {:0.2f} | {:0.2f} | {:0.2f} |".format(tag, downstream_tasks[i], lower_bound[i],
                                                                upper_bounds[i]))

    def show_bounds(self, lower_bounds, downstream_tasks, upper_bounds):
        x = downstream_tasks
        y = [-i for i in range(len(downstream_tasks))]
        err = [[true - low for low, true in zip(lower_bounds, downstream_tasks)],
               [upp - true for upp, true in zip(upper_bounds, downstream_tasks)]]

        plt.subplots(figsize=(10, 10))
        plt.errorbar(x, y, fmt="o", xerr=err, capsize=5)
        plt.yticks(y, list(Switchboard().tags.values()))
        plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--generator_model', type=str, required=True)
    parser.add_argument('--classifier_model', type=str, required=True)
    parser.add_argument('--n_test_examples', type=int, default=100)
    parser.add_argument('--join_turns', type=bool, default=False)
    args = parser.parse_args()

    tags = list(Switchboard().tags.keys())

    evaluate = Evaluate()

    generator_model = language_models.Model(args.generator_model)
    classifier_model = classifier.Model(args.classifier_model)

    if args.join_turns:
        downstream_confusion_matrix = evaluate.test_downstream_task_joined_turn(args.n_test_examples)
    else:
        try:
            classifier_confusion_matrix = np.load('classifier_confusion_matrix.npy')
            downstream_confusion_matrix = np.load('downstream_confusion_matrix.npy')
        except:
            classifier_confusion_matrix = training.classifier.evaluate.Evaluate().evaluate(args.classifier_model)[
                "test_confusion_matrix"]
            if args.join_turns is False:
                downstream_confusion_matrix = evaluate.test_downstream_task(args.n_test_examples)
            else:
                downstream_confusion_matrix = evaluate.test_downstream_task_joined_turn(args.n_test_examples)

            np.save("classifier_confusion_matrix.npy", classifier_confusion_matrix)
            np.save("downstream_confusion_matrix.npy", downstream_confusion_matrix)

        lower_bounds, downstream_tasks, upper_bounds = evaluate.generator_accuracy(downstream_confusion_matrix,
                                                                                   classifier_confusion_matrix)
        evaluate.print_generator_accuracy(lower_bounds, downstream_tasks, upper_bounds)
        evaluate.show_bounds(lower_bounds, downstream_tasks, upper_bounds)
