import argparse

from training.language_models.model import Model

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--save_model_name', type=str, required=True)
    parser.add_argument('--context', type=bool, default=False)
    args = parser.parse_args()

    model = Model(args.save_model_name)
    context = []
    while True:
        if args.context:
            input_message = input(">> Input:")
            context.append(input_message)
        dialogue_acts = input(">> Dialogue act:").split(" ")
        results = model.generate_response(dialogue_acts, context=context[-3:], number_of_responses=1)
        results = [model.postprocess_response(result) for result in results]
        if args.context:
            context.append(results[0])
        print("Output:\n" + 100 * '-')
        for i, output in enumerate(results):
            print("{}: {}".format(i + 1, output))
