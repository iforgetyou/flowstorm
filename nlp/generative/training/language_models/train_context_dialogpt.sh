#!/bin/bash
#SBATCH --job-name=context_generation
#SBATCH --output=context-generation-dialoGPT-large.out
#SBATCH --cpus-per-task=2
#SBATCH --gres=gpu:2
#SBATCH --mem=12G
#SBATCH --time=4-00:00:00
#SBATCH --partition gpu
#SBATCH --exclude node-12,node-14,node-15,node-16,node-17
module purge
module load cuDNN
module load Anaconda3
nvidia-smi
source activate transformers
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/marekp17/.conda/envs/transformers/lib/
export PYTHONPATH=$PYTHONPATH:/home/marekp17/LanguageModelForConditionalTextGeneration
python3 train.py --project_name Context --join_turns true --epochs 15 --batch_size 4 --context true --base_model microsoft/DialoGPT-large