import torch
from transformers import GPT2Tokenizer, GPT2LMHeadModel, Trainer, TrainingArguments

from training.data.switchboard.switchboard import Switchboard


class Model:

    def __init__(self, model, cuda=False):
        # < | sd | > < | qw | > < | bos | > < | eos | > < | pad | >
        EOS_TOKEN = "<|endoftext|>"
        BOS_TOKEN = "<|bos|>"
        PAD_TOKEN = "<|pad|>"
        SEP_TOKEN = "<|sep|>"
        self.tag_tokens = ["<|" + tag + "|>" for tag in list(Switchboard().tags.keys())]

        self.tokenizer = GPT2Tokenizer.from_pretrained(model, eos_token=EOS_TOKEN, bos_token=BOS_TOKEN,
                                                       pad_token=PAD_TOKEN, sep_token=SEP_TOKEN)
        self.tokenizer.add_tokens(self.tag_tokens, special_tokens=True)

        self.model = GPT2LMHeadModel.from_pretrained(model, pad_token_id=self.tokenizer.pad_token_id,
                                                     eos_token_id=self.tokenizer.eos_token_id,
                                                     bos_token_id=self.tokenizer.bos_token_id,
                                                     sep_token_id=self.tokenizer.sep_token_id)
        self.model.resize_token_embeddings(len(self.tokenizer))

        self.cuda = cuda
        if cuda:
            self.model.to('cuda')

    def save(self, name):
        self.model.save_pretrained(name)
        self.tokenizer.save_pretrained(name)

    def train(self, project_name, run_name, train_dataset, eval_dataset, epochs=10, batch_size=8):
        training_arguments = TrainingArguments(output_dir='./' + project_name + "/" + run_name, do_train=True,
                                               do_eval=True,
                                               num_train_epochs=epochs,
                                               logging_strategy="epoch", log_level="info", save_total_limit=1,
                                               load_best_model_at_end=True,
                                               evaluation_strategy="epoch", save_strategy="epoch",
                                               per_device_train_batch_size=batch_size,
                                               per_device_eval_batch_size=batch_size)
        self.model.parallelize()
        trainer = Trainer(model=self.model, args=training_arguments, train_dataset=train_dataset,
                          eval_dataset=eval_dataset, tokenizer=self.tokenizer)
        trainer.train()

    def construct_input_string(self, dialogue_acts, context=None, turns=None, train=False):
        if context is None:
            context = []
        dialogue_acts = ["<|" + act + "|>" for act in dialogue_acts]
        for act in dialogue_acts:
            if act not in self.tag_tokens:
                print("Tag not supported: " + act)

        input_string = (" " + self.tokenizer.sep_token + " ").join(context) + " " + " ".join(
            dialogue_acts) + " " + self.tokenizer.bos_token
        if train:
            input_string += " " + (" " + self.tokenizer.sep_token + " ").join(turns) + " " + self.tokenizer.eos_token
        input_string = input_string.strip()
        return input_string

    def generate_response(self, dialogue_acts, context=None, max_length=100,
                          num_beams=3, number_of_responses=1, postprocess=False):
        if context is None:
            context = []
        inputs = torch.tensor([self.tokenizer.encode(self.construct_input_string(dialogue_acts, context=context))])
        if self.cuda:
            inputs = inputs.to('cuda')
        outputs = self.model.generate(inputs, do_sample=False, num_return_sequences=number_of_responses,
                                      max_length=max_length, num_beams=num_beams,
                                      bos_token_id=self.tokenizer.bos_token_id,
                                      eos_token_id=self.tokenizer.eos_token_id,
                                      pad_token_id=self.tokenizer.pad_token_id,
                                      sep_token_id=self.tokenizer.sep_token_id)
        results = []
        for output in outputs:
            response = self.tokenizer.decode(output, skip_special_tokens=False)
            if postprocess:
                response = self.postprocess_response(response)
            results.append(response)
        return results

    def postprocess_response(self, response):
        bos_token = self.tokenizer.bos_token
        eos_token = self.tokenizer.eos_token
        sep_token = self.tokenizer.sep_token
        response = response.split(bos_token, 1)[1].split(eos_token, 1)[0].strip()
        response = " ".join([x.strip() for x in response.split(sep_token)])
        return response
