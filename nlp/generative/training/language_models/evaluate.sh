#!/bin/bash
#SBATCH --job-name=gpt2-dialogue-act-evaluation
#SBATCH --output=dialogue-act-evaluation.out
#SBATCH --cpus-per-task=2
#SBATCH --gres=gpu:1
#SBATCH --mem=12G
#SBATCH --time=2-00:00:00
#SBATCH --partition gpu
#SBATCH --exclude node-12,node-14,node-15,node-16,node-17
module purge
module load cuDNN
module load Anaconda3
nvidia-smi
source activate transformers
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/marekp17/.conda/envs/transformers/lib/
export PYTHONPATH=$PYTHONPATH:/home/marekp17/LanguageModelForConditionalTextGeneration
python3 evaluate.py --generator_model /home/marekp17/LanguageModelForConditionalTextGeneration/language_models/models/dialogue_acts/jumping-snowball-34 --classifier_model /home/marekp17/LanguageModelForConditionalTextGeneration/classifier/models/flowing-firebrand-15 --n_test_examples 100