import argparse
import random
import wandb

from training.data.dialogue_act_dataset import DialogueActDataset
from training.data.switchboard.switchboard import Switchboard
from training.language_models.model import Model
from training.secret import wandb_login


class Train:
    def prepare_data(self, model, dataset_size_limit=-1, join_turns=False, context=False, complete=False):
        train_dataset, eval_dataset, test_dataset = Switchboard().get(dataset_size_limit)
        train_inputs = self.extract_inputs(train_dataset, join_turns, context, complete)
        eval_inputs = self.extract_inputs(eval_dataset, join_turns, context, complete)
        test_inputs = self.extract_inputs(test_dataset, join_turns, context, complete)

        train_inputs = self.construct_input_strings(train_inputs, model)
        eval_inputs = self.construct_input_strings(eval_inputs, model)
        test_inputs = self.construct_input_strings(test_inputs, model)

        train_dataset = DialogueActDataset(model.tokenizer(train_inputs, truncation=True, padding=True))
        eval_dataset = DialogueActDataset(model.tokenizer(eval_inputs, truncation=True, padding=True))
        test_dataset = DialogueActDataset(model.tokenizer(test_inputs, truncation=True, padding=True))

        return train_dataset, eval_dataset, test_dataset

    def extract_inputs(self, dataset, join_turns=False, context=False, complete=False):
        if complete:
            to_return = self.with_context(dataset, 3) + self.join_turns(dataset) + self.separate_turns(dataset)
            random.shuffle(to_return)
            return to_return
        if join_turns:
            if context:
                return self.with_context(dataset, 3)
            else:
                return self.join_turns(dataset)
        else:
            return self.separate_turns(dataset)

    def separate_turns(self, dataset):
        inputs = []
        for dialogue in dataset:
            for utterance in dialogue["utterances"]:
                inputs.append(([utterance["da_label"]], [utterance["text"]], []))
        return inputs

    def join_turns(self, dataset):
        inputs = []
        for dialogue in dataset:
            speaker = None
            joined_utterance = None
            for utterance in dialogue["utterances"]:
                if utterance["speaker"] != speaker:
                    speaker = utterance["speaker"]
                    if joined_utterance is not None:
                        inputs.append((joined_utterance["da_label"], joined_utterance["text"], []))
                    joined_utterance = {}
                    for key in utterance:
                        joined_utterance[key] = [utterance[key]]
                else:
                    for key in utterance:
                        joined_utterance[key].append(utterance[key])
        return inputs

    def with_context(self, dataset, context_length):
        inputs = []
        for dialogue in dataset:
            dialogue_inputs = []
            speaker = None
            joined_utterance = None
            for utterance in dialogue["utterances"]:
                if utterance["speaker"] != speaker:
                    speaker = utterance["speaker"]
                    if joined_utterance is not None:
                        dialogue_inputs.append((joined_utterance["da_label"], joined_utterance["text"]))
                    joined_utterance = {}
                    for key in utterance:
                        joined_utterance[key] = [utterance[key]]
                else:
                    for key in utterance:
                        joined_utterance[key].append(utterance[key])
            for i in range(len(dialogue_inputs)):
                context = [" ".join(x[1]) for x in dialogue_inputs[max(i - context_length, 0):i]]
                dialogue_inputs[i] = (dialogue_inputs[i][0], dialogue_inputs[i][1], context)
            inputs += dialogue_inputs
        return inputs

    def construct_input_strings(self, inputs, model):
        to_return = []
        for das, texts, context in inputs:
            to_return.append(model.construct_input_string(das, turns=texts, context=context, train=True))
        return to_return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--project_name', type=str, required=True)
    parser.add_argument('--join_turns', type=bool, default=False)
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--batch_size', type=int, default=8)
    parser.add_argument('--dataset_size_limit', type=int, default=-1)
    parser.add_argument('--context', type=bool, default=False)
    parser.add_argument('--complete', type=bool, default=False)
    parser.add_argument('--base_model', type=str, default="gpt2")
    args = parser.parse_args()

    wandb.login(key=wandb_login)
    wandb.init(project=args.project_name, entity='thepetrmarek')
    run_name = wandb.run.name
    random.seed(4)

    model = Model(args.base_model)
    train = Train()
    train_dataset, eval_dataset, test_dataset = train.prepare_data(model, dataset_size_limit=args.dataset_size_limit,
                                                                   join_turns=args.join_turns, context=args.context,
                                                                   complete=args.complete)
    model.train(args.project_name, run_name, train_dataset, eval_dataset, epochs=args.epochs,
                batch_size=args.batch_size)

    model.save('./' + args.project_name + "/" + run_name + "/" + "final")
