import argparse

import numpy as np

import torch
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, ConfusionMatrixDisplay
from transformers import TrainingArguments, Trainer
from torch.utils.data.dataset import Dataset

from training.classifier.model import Model
from training.data.switchboard.switchboard import Switchboard
import matplotlib.pyplot as plt


class Evaluate:
    class DialogueActDataset(torch.utils.data.Dataset):
        def __init__(self, encodings, labels):
            self.encodings = encodings
            self.labels = labels

        def __getitem__(self, idx):
            item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
            item["labels"] = self.labels[idx]
            return item

        def __len__(self):
            return len(self.encodings["input_ids"])

    def extract_inputs(self, dataset):
        inputs = []
        labels = set()
        for dialogue in dataset:
            for utterance in dialogue["utterances"]:
                inputs.append((utterance["da_label"], utterance["text"]))
                labels.add(utterance["da_label"])
        return inputs

    def evaluate(self, save_model_name, switchboard_limit=-1):
        model = Model(save_model_name)
        switchboard = Switchboard()
        _, _, test_dataset = switchboard.get(switchboard_limit)
        inputs = self.extract_inputs(test_dataset)
        test_dataset = self.DialogueActDataset(
            model.tokenizer([example[1] for example in inputs], truncation=True, padding=True),
            [switchboard.tag_to_id(example[0]) for example in inputs])

        arguments = TrainingArguments(output_dir="delete", per_device_eval_batch_size=64)
        trainer = Trainer(args=arguments, model=model.model, tokenizer=model.tokenizer,
                          compute_metrics=self.compute_metrics)
        result = trainer.evaluate(test_dataset, metric_key_prefix="test")
        return result

    def print_result(self, result):
        print("Accuracy: {:0.2f}".format(result["test_accuracy"]))
        print("| | Precision | Recall | F1-Score | Class Accuracy | Support |")
        print("|---|---|---|---|---|---|")
        for key in result:
            if type(result[key]) is dict and key != "test_confusion_matrix" and "class_accuracy" in result[key]:
                print("| {} | {:0.2f} | {:0.2f} | {:0.2f} | {:0.2f} | {} |".format(key.replace("test_", ""),
                                                                                   result[key]["precision"],
                                                                                   result[key]["recall"],
                                                                                   result[key]["f1-score"],
                                                                                   result[key]["class_accuracy"],
                                                                                   result[key]["support"]))

    def print_confusion_matrix(self, result):
        fig, ax = plt.subplots(figsize=(25, 25))
        disp = ConfusionMatrixDisplay(confusion_matrix=result["test_confusion_matrix_normalized"],
                                      display_labels=list(Switchboard().tags.values()))
        disp.plot(xticks_rotation="vertical", ax=ax)
        plt.show()

    def compute_metrics(self, eval_pred):
        logits, labels = eval_pred
        predictions = np.argmax(logits, axis=-1)
        target_names = list(Switchboard().tags.values())
        c_matrix = confusion_matrix(labels, predictions, labels=list(range(len(target_names))))
        c_matrix_normalized = confusion_matrix(labels, predictions, labels=list(range(len(target_names))),
                                               normalize="true")
        class_accuracy = c_matrix_normalized.diagonal()
        accuracy = accuracy_score(labels, predictions)
        report = classification_report(labels, predictions, output_dict=True, target_names=target_names,
                                       labels=list(range(len(target_names))))

        report["accuracy"] = accuracy
        for i, name in enumerate(target_names):
            report[name]["class_accuracy"] = class_accuracy[i]
        report["confusion_matrix"] = c_matrix
        return report


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--save_model_name', type=str, required=True)
    args = parser.parse_args()
    evaluateObj = Evaluate()
    result = evaluateObj.evaluate(args.save_model_name)
    evaluateObj.print_result(result)
    evaluateObj.print_confusion_matrix(result)
