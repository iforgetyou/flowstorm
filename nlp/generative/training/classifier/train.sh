#!/bin/bash
#SBATCH --job-name=albert-dialogue-act-classification
#SBATCH --output=albert-dialoge-act-classification.out
#SBATCH --cpus-per-task=2
#SBATCH --gres=gpu:1
#SBATCH --mem=12G
#SBATCH --time=2-00:00:00
#SBATCH --partition gpu
#SBATCH --exclude node-12,node-14,node-15,node-16,node-17
module purge
module load cuDNN
module load Anaconda3
nvidia-smi
source activate transformers
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/marekp17/.conda/envs/transformers/lib/
export PYTHONPATH=$PYTHONPATH:/home/marekp17/LanguageModelForConditionalTextGeneration
python3 train.py --project_name DialogueActsClassification --epochs 15 --batch_size 64