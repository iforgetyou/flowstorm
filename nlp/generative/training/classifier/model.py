import numpy as np
from transformers import AlbertTokenizer, AlbertForSequenceClassification, TrainingArguments, Trainer
from datasets import load_metric


class Model:

    def __init__(self, model):
        self.tokenizer = AlbertTokenizer.from_pretrained(model)
        self.model = AlbertForSequenceClassification.from_pretrained(model, num_labels=41)
        self.accuracy_metric = load_metric("accuracy")

    def train(self, project_name, run_name, train_dataset, eval_dataset, epochs=10, batch_size=8):
        training_arguments = TrainingArguments(output_dir='./' + project_name + "/" + run_name, do_train=True,
                                               do_eval=True,
                                               num_train_epochs=epochs,
                                               logging_strategy="epoch", log_level="info", save_total_limit=1,
                                               load_best_model_at_end=True,
                                               evaluation_strategy="epoch", save_strategy="epoch",
                                               per_device_train_batch_size=batch_size,
                                               per_device_eval_batch_size=batch_size)
        trainer = Trainer(model=self.model, args=training_arguments, train_dataset=train_dataset,
                          eval_dataset=eval_dataset, tokenizer=self.tokenizer,
                          compute_metrics=self.compute_metrics)
        trainer.train()

    def compute_metrics(self, eval_pred):
        logits, labels = eval_pred
        predictions = np.argmax(logits, axis=-1)
        return self.accuracy_metric.compute(predictions=predictions, references=labels)

    def save(self, name):
        self.model.save_pretrained(name)
        self.tokenizer.save_pretrained(name)

    def predict(self, sentences):
        tokens = self.tokenizer(sentences, return_tensors="pt", truncation=True, padding=True)
        logits = self.model(**tokens).logits
        return logits
