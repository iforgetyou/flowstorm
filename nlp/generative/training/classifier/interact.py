import argparse

import torch

from training.classifier.model import Model
from training.data.switchboard.switchboard import Switchboard

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--save_model_name', type=str, required=True)
    args = parser.parse_args()

    switchboard = Switchboard()
    model = Model(args.save_model_name)
    while True:
        sentence = input(">> Sentence:")
        results = model.predict(sentence)
        da_class = torch.argmax(results).item()
        tag = switchboard.id_to_tag(da_class)
        tag_name = switchboard.tag_to_name(tag)
        print("Output:\n" + 100 * '-')
        print(tag + ": " + tag_name)
