import argparse
import random
import wandb

from training.classifier.model import Model
from training.data.dialogue_act_dataset import DialogueActDataset
from training.data.switchboard.switchboard import Switchboard
from training.secret import wandb_login


class Train:
    def prepare_data(self, model, dataset_size_limit=-1):
        switchboard = Switchboard()
        train_dataset, eval_dataset, test_dataset = switchboard.get(dataset_size_limit)
        train_inputs = self.extract_inputs(train_dataset)
        eval_inputs = self.extract_inputs(eval_dataset)
        test_inputs = self.extract_inputs(test_dataset)

        train_dataset = DialogueActDataset(
            model.tokenizer([example[1] for example in train_inputs], truncation=True, padding=True),
            [switchboard.tag_to_id(example[0]) for example in train_inputs])
        eval_dataset = DialogueActDataset(
            model.tokenizer([example[1] for example in eval_inputs], truncation=True, padding=True),
            [switchboard.tag_to_id(example[0]) for example in eval_inputs])
        test_dataset = DialogueActDataset(
            model.tokenizer([example[1] for example in test_inputs], truncation=True, padding=True),
            [switchboard.tag_to_id(example[0]) for example in test_inputs])
        return train_dataset, eval_dataset, test_dataset

    def extract_inputs(self, dataset):
        inputs = []
        for dialogue in dataset:
            for utterance in dialogue["utterances"]:
                inputs.append((utterance["da_label"], utterance["text"]))
        return inputs


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--project_name', type=str, required=True)
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--batch_size', type=int, default=8)
    parser.add_argument('--dataset_size_limit', type=int, default=-1)
    args = parser.parse_args()

    wandb.login(key=wandb_login)
    wandb.init(project=args.project_name, entity='thepetrmarek')
    run_name = wandb.run.name
    random.seed(4)

    model = Model('albert-base-v2')
    train = Train()
    train_dataset, eval_dataset, test_dataset = train.prepare_data(model, dataset_size_limit=args.dataset_size_limit)
    model.train(args.project_name, run_name, train_dataset, eval_dataset, epochs=args.epochs,
                batch_size=args.batch_size)

    model.save('./' + args.project_name + "/" + run_name + "/" + "final")
