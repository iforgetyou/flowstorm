from training.data.switchboard.switchboard import Switchboard


def get_da_turn_histogram(labels=True):
    train_dataset, eval_dataset, test_dataset = Switchboard().get()
    histogram = {}
    for conversation in train_dataset:
        speaker = conversation["utterances"][0]["speaker"]
        dialogue_acts = []
        for utterance in conversation["utterances"]:
            if speaker != utterance["speaker"]:
                dialogue_acts_string = ", ".join(dialogue_acts)
                if dialogue_acts_string not in histogram:
                    histogram[dialogue_acts_string] = 0
                histogram[dialogue_acts_string] += 1

                speaker = utterance["speaker"]
                dialogue_acts = []
            if labels:
                dialogue_acts.append(utterance["da_label"])
            else:
                dialogue_acts.append(utterance["da_name"])
    histogram = {k: v for k, v in sorted(histogram.items(), key=lambda item: item[1], reverse=True)}
    return histogram


def analyze_turn():
    histogram = get_da_turn_histogram()
    print(len(histogram))
    i = 0
    print("| DAs | Count |")
    print("|---|---|")
    for el in histogram:
        print("| " + el + " | " + str(histogram[el]) + " |")
        i += 1
        if i == 100:
            break


def analyze_two_speakers():
    train_dataset, eval_dataset, test_dataset = Switchboard().get()
    histogram = {}
    for conversation in train_dataset:
        speaker = conversation["utterances"][0]["speaker"]
        pair = [["START"], []]
        for utterance in conversation["utterances"]:
            if speaker != utterance["speaker"]:
                pair_string = ", ".join(pair[0]) + " / " + ", ".join(pair[1])
                if pair_string not in histogram:
                    histogram[pair_string] = 0
                histogram[pair_string] += 1

                speaker = utterance["speaker"]
                pair = [pair[1], []]
            pair[1].append(utterance["da_name"])

        pair_string = ", ".join(pair[1]) + " / " + "END"
        if pair_string not in histogram:
            histogram[pair_string] = 0
        histogram[pair_string] += 1

    histogram = {k: v for k, v in sorted(histogram.items(), key=lambda item: item[1], reverse=True)}
    print(len(histogram))
    i = 0
    print("| DAs speaker A / DAs speaker B | Count |")
    print("|---|---|")
    for el in histogram:
        print("| " + el + " | " + str(histogram[el]) + " |")
        i += 1
        if i == 100:
            break


if __name__ == '__main__':
    analyze_turn()
    analyze_two_speakers()
