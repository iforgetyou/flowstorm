from training.data.switchboard.switchboard import Switchboard


def analyze():
    switchboard = Switchboard()
    train_dataset, eval_dataset, test_dataset = switchboard.get()
    train_labels = get_labels(train_dataset)
    eval_labels = get_labels(eval_dataset)
    test_labels = get_labels(test_dataset)

    whole_dataset_labels = join_dicts(train_labels, join_dicts(eval_labels, test_labels))

    labels_missing_in_train = find_missing_labels(train_labels, whole_dataset_labels)
    labels_missing_in_eval = find_missing_labels(eval_labels, whole_dataset_labels)
    labels_missing_in_test = find_missing_labels(test_labels, whole_dataset_labels)

    print("There are {} labels in whole dataset".format(len(train_labels)))
    print("There are {} labels in train".format(len(train_labels)))
    print("Missing labels in train are: {}".format(", ".join(labels_missing_in_train)))
    print("There are {} labels in eval".format(len(eval_labels)))
    print("Missing labels in eval are: {}".format(", ".join(labels_missing_in_eval)))
    print("There are {} labels in test".format(len(test_labels)))
    print("Missing labels in test are: {}".format(", ".join(labels_missing_in_test)))
    print_labels(switchboard.tag_to_name, whole_dataset_labels, train_labels, eval_labels, test_labels)


def get_labels(dataset):
    labels = dict()
    for dialogue in dataset:
        for utterance in dialogue["utterances"]:
            if utterance["da_label"] not in labels:
                labels[utterance["da_label"]] = 0
            labels[utterance["da_label"]] += 1
    labels = {k: v for k, v in sorted(labels.items(), key=lambda item: item[1], reverse=True)}
    return labels


def join_dicts(dict_a, dict_b):
    new_dict = dict()
    for key in dict_a:
        new_dict[key] = dict_a[key]
    for key in dict_b:
        if key not in new_dict:
            new_dict[key] = dict_b[key]
        else:
            new_dict[key] += dict_b[key]
    return new_dict


def find_missing_labels(labels, whole_dataset_labels):
    missing_labels = set()
    for label in whole_dataset_labels:
        if label not in labels:
            missing_labels.add(label)
    return missing_labels


def print_labels(tag2name, whole_dataset_labels, train_labels, eval_labels, test_labels):
    print("| Label | # Whole Dataset | # Train | # Eval | # Test |")
    print("|---|---|---|---|---|")
    for label in whole_dataset_labels:
        print("| {} ({})| {} | {} | {} | {} |".format(tag2name(label), label, whole_dataset_labels[label],
                                                      train_labels[label] if label in train_labels else 0,
                                                      eval_labels[label] if label in eval_labels else 0,
                                                      test_labels[label] if label in test_labels else 0))


if __name__ == '__main__':
    analyze()
