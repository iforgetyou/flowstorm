import json
import os
from os.path import isfile, join
from tqdm import tqdm

from training.data.switchboard import process_transcript
from training.data.switchboard.swda import Transcript
from datasets import load_dataset


class Switchboard:

    def __init__(self):
        self.tags = {
            "sd": "Statement-non-opinion",
            "b": "Acknowledge (Backchannel)",
            "sv": "Statement-opinion",
            "aa": "Agree Accept",
            "%": "Abandoned or Turn-Exit",
            "ba": "Appreciation",
            "qy ()": "Yes-No-Question",
            "ny": "Yes answers",
            "fc": "Conventional-closing",
            "qw": "Wh-Question",
            "nn": "No answers",
            "bk": "Response Acknowledgement",
            "h": "Hedge",
            "qy^d": "Declarative Yes-No-Question",
            "fo_o_fw_\"_by_bc": "Other",
            "bh": "Backchannel in question form",
            "^q": "Quotation",
            "bf": "SummarizeReformulate",
            "na": "Affirmative non-yes answers",
            "ad": "Action-directive",
            "^2": "Collaborative Completion",
            "b^m": "Repeat-phrase",
            "qo": "Open-Question",
            "qh": "Rhetorical-Questions",
            "^h": "Hold before answer Agreement",
            "ar": "Reject",
            "ng": "Negative non-no answers",
            "br": "Signal-non-understanding",
            "no": "Other answers",
            "fp": "Conventional-opening",
            "qrr": "Or-Clause",
            "arp_nd": "Dispreferred answers",
            "t3": "3rd-party-talk",
            "oo_co_cc": "Offers, Options, Commits",
            "t1": "Self-talk",
            "bd": "Downplayer",
            "aap_am": "Maybe Accept-part",
            "^g": "Tag-Question",
            "qw^d": "Declarative Wh-Question",
            "fa": "Apology",
            "ft": "Thanking"
        }

        self._tag_to_id = {tag: id for id, tag in enumerate(self.tags.keys())}
        self._id_to_tag = {id: tag for id, tag in enumerate(self.tags.keys())}

    def get(self, limit=-1):
        folder = os.path.dirname(os.path.abspath(__file__))
        cache_file_name = "switchboard_cache.json"

        train_dataset, eval_dataset, test_dataset = self.load_cache(folder, cache_file_name)
        if train_dataset is not None and eval_dataset is not None and test_dataset is not None:
            if limit != -1 and len(train_dataset) > limit:
                train_dataset = train_dataset[:limit]
            if limit != -1 and len(eval_dataset) > limit:
                eval_dataset = eval_dataset[:limit]
            if limit != -1 and len(test_dataset) > limit:
                test_dataset = test_dataset[:limit]
        else:
            train_files, eval_files, test_files = self.get_splits()
            train_dataset, eval_dataset, test_dataset = self.process_dataset(limit, folder, train_files, eval_files,
                                                                             test_files)
            train_dataset = self.dataset_to_dict(train_dataset)
            eval_dataset = self.dataset_to_dict(eval_dataset)
            test_dataset = self.dataset_to_dict(test_dataset)
            self.make_cache(train_dataset, eval_dataset, test_dataset, folder, cache_file_name)
        return train_dataset, eval_dataset, test_dataset

    def load_cache(self, folder, cache_file_name):
        if os.path.exists(folder + "/" + cache_file_name):
            with open(folder + "/" + cache_file_name, "r", encoding="utf-8") as in_file:
                print("Loading Switchboard from cache")
                complete_cache = json.load(in_file)
                return complete_cache["train"], complete_cache["eval"], complete_cache["test"]
        else:
            return None, None, None

    def make_cache(self, train_dataset, eval_dataset, test_dataset, folder, cache_file_name):
        with open(folder + "/" + cache_file_name, "w", encoding="utf-8") as out_file:
            print("Creating Switchboard cache")
            json_output = json.dumps({"train": train_dataset, "eval": eval_dataset, "test": test_dataset},
                                     default=lambda x: x.__dict__, indent=4)
            out_file.write(json_output)

    def get_splits(self):
        dataset = load_dataset("swda")
        train_files = set()
        eval_files = set()
        test_files = set()
        for turn in dataset["train"]:
            train_files.add(turn["swda_filename"])
        for turn in dataset["validation"]:
            eval_files.add(turn["swda_filename"])
        for turn in dataset["test"]:
            test_files.add(turn["swda_filename"])
        return train_files, eval_files, test_files

    def process_dataset(self, limit, folder, train_files, eval_files, test_files):
        train_dataset = []
        eval_dataset = []
        test_dataset = []
        subdirs = next(os.walk(os.path.join(folder, 'swda')))[1]
        for subdir in subdirs:
            onlyfiles = [f for f in os.listdir(folder + "/swda/" + subdir) if
                         isfile(join(folder + "/swda/" + subdir, f))]
            for file in tqdm(onlyfiles, desc=subdir):
                transcript = Transcript(folder + '/swda/' + subdir + '/' + file, folder + '/swda/swda-metadata.csv')
                # Excluded dialogue act tags i.e. x = Non-verbal
                excluded_tags = ['x']
                # Excluded characters for ignoring i.e. <laughter>
                excluded_chars = {'<', '>', '(', ')', '-', '#'}
                conversation = process_transcript(transcript, self.tag_to_name, excluded_tags=excluded_tags,
                                                  excluded_chars=excluded_chars)

                if subdir + "/" + file.replace(".csv", "") in train_files:
                    dataset = train_dataset
                elif subdir + "/" + file.replace(".csv", "") in test_files:
                    dataset = test_dataset
                else:
                    dataset = eval_dataset

                if limit != -1 and len(dataset) > limit:
                    continue
                dataset.append(conversation)
        return train_dataset, eval_dataset, test_dataset

    def dataset_to_dict(self, dataset):
        json_dict = json.dumps(dataset, default=lambda x: x.__dict__, indent=4)
        return json.loads(json_dict)

    def tag_to_name(self, tag):
        return self.tags[tag]

    def tag_to_id(self, tag):
        return self._tag_to_id[tag]

    def id_to_tag(self, id):
        return self._id_to_tag[id]


if __name__ == '__main__':
    Switchboard().get()
