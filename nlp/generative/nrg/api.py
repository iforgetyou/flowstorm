import os
from typing import Dict

import argparse
import kfserving
import sentry_sdk

from download_s3_folder import download_s3_folder
from config import CONFIG
from nrg.dialoGPT import DialoGPT


class Model(kfserving.KFModel):

    def __init__(self, name: str, model_path: str, cuda: bool, max_input_tokens: int):
        super().__init__(name)
        self.model_path = model_path
        self.cuda = cuda
        self.max_input_tokens = max_input_tokens
        self.ready = False

    def load(self) -> bool:
        self.model = DialoGPT(self.model_path, self.cuda, self.max_input_tokens)
        self.ready = True
        return True

    def predict(self, request: Dict) -> Dict:
        responses = self.model.generate_response(request)
        return {"responses": responses}


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=8080, help="Port to run on")
    parser.add_argument('--cuda', type=bool, default=False, help='CUDA support')
    parser.add_argument('--workers', type=int, default=1, help='Number of KFServing workers')
    parser.add_argument('--max_input_tokens', type=int, default=-1,
                        help="Number of tokenizer tokens to which the input will be truncated. For memory purposes. "
                             "-1 for no limit.")
    parser.add_argument('--model_path', type=str, help="Path to model on local machine. Mainly for testing.")
    args = parser.parse_args()

    sentry_sdk.init(CONFIG["sentry.dsn"], traces_sample_rate=0.0)

    if args.model_path is None:
        bucket_name = os.getenv('BUCKET_NAME')
        model_path = os.getenv('MODEL_PATH')
        if bucket_name is None:
            bucket_name = "promethist-models"
        if model_path is None:
            model_path = "neural_response_generator/active/"
        local_folder = "models/"
        download_s3_folder(bucket_name, model_path, local_folder)
        subfolders = [f for f in os.scandir(local_folder) if f.is_dir()]

        models = []
        for subfolder in subfolders:
            args.model_path = subfolder.path
            model = Model(subfolder.name, args.model_path, args.cuda, args.max_input_tokens)
            model.load()
            models.append(model)
        kfserving.KFServer(workers=args.workers, http_port=args.port).start(models)
    else:
        model = Model("NRG", args.model_path, args.cuda, args.max_input_tokens)
        model.load()
        kfserving.KFServer(workers=args.workers, http_port=args.port).start([model])
