import json
from transformers import GPT2Tokenizer, GPT2LMHeadModel


class DialoGPT:

    def __init__(self, model, cuda=False, max_input_tokens=-1):
        self.tokenizer = GPT2Tokenizer.from_pretrained(model)
        self.model = GPT2LMHeadModel.from_pretrained(model)
        self.max_input_tokens = max_input_tokens
        self.speaker_user, self.speaker_system = self.load_speakers(model)

        self.cuda = cuda
        if cuda:
            self.model.to('cuda')

    def load_speakers(self, model):
        speaker_user = None
        speaker_system = None
        try:
            with open(model + "/config.json", "r", encoding="utf-8") as config_file:
                config = json.load(config_file)
                if "speaker_user" in config:
                    speaker_user = config["speaker_user"]
                if "speaker_system" in config:
                    speaker_system = config["speaker_system"]
        except:
            pass
        return speaker_user, speaker_system

    def generate_response(self, request):
        input_string = self.construct_input_string(request)
        inputs = self.tokenizer.encode(input_string, return_tensors='pt')

        if -1 < self.max_input_tokens < len(inputs[0]):
            inputs = inputs[:, -self.max_input_tokens:]

        if "max_length" in request:
            request["max_length"] += len(inputs[0])
        else:
            request["max_length"] = self.model.config.max_length + len(inputs[0])

        if "min_length" in request:
            request["min_length"] += len(inputs[0])
        else:
            request["min_length"] = self.model.config.min_length + len(inputs[0])

        if self.cuda:
            inputs = inputs.to('cuda')
        generated = self.model.generate(input_ids=inputs, pad_token_id=self.tokenizer.eos_token_id, **request)
        generated = generated[:, inputs.shape[-1]:]
        responses = [self.tokenizer.decode(output, skip_special_tokens=True) for output in generated]
        if "response_start" in request:
            responses = ["{}{}".format(request["response_start"], x) for x in responses]
        return responses

    def construct_input_string(self, request):
        dialogue_act = "<|{}|>".format(request["dialogue_act"]) if "dialogue_act" in request else ""
        response_start = request["response_start"] if "response_start" in request else ""
        input_string = ""
        for i, utterance in enumerate(request["context"]):
            input_string += self.create_speaker_tag(len(request["context"]), i) + utterance + self.tokenizer.eos_token
        input_string += self.create_speaker_tag(len(request["context"]),
                                                len(request["context"])) + dialogue_act + response_start
        if input_string == "":
            input_string = self.tokenizer.bos_token
        return input_string

    def create_speaker_tag(self, context_length, utterance_position):
        if context_length % 2 == 0:
            if utterance_position % 2 == 0:
                return "<|{}|>".format(self.speaker_system) if self.speaker_system is not None else ""
            else:
                return "<|{}|>".format(self.speaker_user) if self.speaker_user is not None else ""
        else:
            if utterance_position % 2 == 0:
                return "<|{}|>".format(self.speaker_user) if self.speaker_user is not None else ""
            else:
                return "<|{}|>".format(self.speaker_system) if self.speaker_system is not None else ""
