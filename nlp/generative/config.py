import os
import yaml

from configparser import ConfigParser


config = ConfigParser()


def load_properties(path):
    dummy_section = 'dummy_section'
    with open(path, 'r') as f:
        config_string = f'[{dummy_section}]\n' + f.read()
    config.read_string(config_string)
    return config[dummy_section]


class Config(dict):
    """ Application configuration """

    def __init__(self, path=None):
        super().__init__()

        if os.path.exists(path):
            with open(path, 'r') as f:
                config_string = '[dummy_section]\n' + f.read()
            config = ConfigParser()
            config.read_string(config_string)

            with open(path) as in_file:
                self.update(yaml.safe_load(in_file))

    def __getitem__(self, item):
        return super().__getitem__(item)


CUR_DIR = os.path.dirname(os.path.abspath(__file__))
APP_DIR = os.path.join(CUR_DIR, os.pardir)

try:
    CONFIG = load_properties(os.path.join(APP_DIR, 'app.properties'))
    CONFIG = load_properties(os.path.join(APP_DIR, 'app.local.properties'))
except FileNotFoundError:
    CONFIG = load_properties('/app.properties')
    CONFIG = load_properties('/app.local.properties')
