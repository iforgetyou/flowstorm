group = "ai.flowstorm.nlp"
description = "Flowstorm NLP NRG Lib"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-common-app"))
    implementation(project(":flowstorm-core-lib"))
    implementation(project(":flowstorm-nlp-profanity"))
}
