package ai.flowstorm.core.dialogue

enum class DialogueAct {
    open_question,
    yes_no_question,
    command,
    opinion,
    statement,
    yes_answer,
    no_answer,
    other_answer
}