package ai.flowstorm.core.dialogue

enum class GeneratorModel {
    DialoGPT_large,
    EmpatheticDialogues,
    EmpatheticDialogues_DialogueActs,
    TheTherapyFanfic,
    CCPE_Movies,
    TheTherapyFanfic_DialogueActs,
    TopicalChat,
    TopicalChat_DialogueActs,
    Experimental_one,
    Experimental_two,
}