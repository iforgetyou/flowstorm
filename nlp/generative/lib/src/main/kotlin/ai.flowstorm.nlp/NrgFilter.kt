package ai.flowstorm.nlp

import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.lazyExpiring
import com.fasterxml.jackson.core.type.TypeReference
import java.io.IOException
import java.net.URL

class NrgFilter {
    private val logger by LoggerDelegate()

    val url = AppConfig.get("nrgFilter.sourceUrl", "https://core.flowstorm.ai/file/assets/spaces/62014f95b347bc564f2333f6")


    private val nrgFilterRules by lazyExpiring(300) {
        URL(url).openStream().use {
            try {
                ObjectUtil.defaultMapper.readValue(it, object : TypeReference<List<String>>() {})
            } catch (e: IOException) {
                logger.error("Cannot parse json file with nrg filter rules: ${e.message}. No nrg filtering will be used in nrg!")
                listOf<String>()
            }
        }
    }

    fun isToBeFiltered(text: String) = nrgFilterRules.any { "\\b${it}\\b".toRegex(RegexOption.IGNORE_CASE).containsMatchIn(text) }
}