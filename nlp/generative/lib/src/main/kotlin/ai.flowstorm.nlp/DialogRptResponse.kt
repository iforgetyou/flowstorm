package ai.flowstorm.nlp

@com.fasterxml.jackson.databind.annotation.JsonNaming(com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy::class)
data class DialogRPTResponse(val scores: List<Float>)