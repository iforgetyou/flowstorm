package ai.flowstorm.nlp

import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.core.Context
import ai.flowstorm.core.dialogue.DialogueAct
import ai.flowstorm.core.dialogue.GeneratorModel
import ai.flowstorm.core.dialogue.RankerModel
import ai.flowstorm.core.model.Turn
import ai.flowstorm.nlp.profanity.ProfanityFilter

class Nrg(private val context: Context, private val profanityFilterProvider: ProfanityFilterProvider) {

    fun interface ProfanityFilterProvider {
        fun provide(): ProfanityFilter
    }

    private val profanityFilter: ProfanityFilter get() = profanityFilterProvider.provide()

    private val nrgFilter = NrgFilter()

    fun generate(generatorModel: GeneratorModel, rankerModel: RankerModel, contextLength: Int = 3, responseStart: String? = null, dialogueAct: DialogueAct? = null, generatorParameters: MutableMap<String, Any>? = null) = with(context) {
        try {
            performanceSpan.createChild("Nrg.generate").measure({ result, time ->
                logger.info("NRG (generatorModel=$generatorModel, rankerModel=$rankerModel) ${result.name.lowercase()} in $time ms")
            }) {
                val generatorContext = constructGeneratorContext(contextLength)
                logger.info("NRG context=$generatorContext")
                execute(generatorModel, rankerModel, generatorContext, responseStart, dialogueAct, generatorParameters)
            }
        } catch (t: Throwable) {
            logger.error("NRG failed: ${t.message}")
            null
        }
    }

    fun generate(generatorModel: GeneratorModel, rankerModel: RankerModel, generatorContext: List<String>, responseStart: String? = null, dialogueAct: DialogueAct? = null, generatorParameters: MutableMap<String, Any>? = null) = with(context) {
        try {
            performanceSpan.createChild("Nrg.generate").measure({ result, time ->
                logger.info("NRG (generatorModel=$generatorModel, rankerModel=$rankerModel) ${result.name.lowercase()} in $time ms")
            }) {
                execute(generatorModel, rankerModel, generatorContext, responseStart, dialogueAct, generatorParameters)
            }
        } catch (t: Throwable) {
            logger.error("NRG failed: ${t.message}")
            null
        }
    }

    private fun execute(generatorModel: GeneratorModel, rankerModel: RankerModel, generatorContext: List<String>, responseStart: String? = null, dialogueAct: DialogueAct? = null, generatorParameters: MutableMap<String, Any>? = null): String {
        val defaultGeneratorParameters = mutableMapOf<String, Any>(
                "num_return_sequences" to 5,
                "num_beams" to 10,
                "do_sample" to false,
                "min_length" to 10,
                "max_length" to 50,
                "repetition_penalty" to 1.5,
                "length_penalty" to 2,
                "num_beam_groups" to 5,
                "diversity_penalty" to 1.0,
                "no_repeat_ngram_size" to 3
        )
        val nrgResponses = nrg(generatorContext, responseStart, dialogueAct, generatorModel, generatorParameters
                ?: defaultGeneratorParameters).responses

        val profanityFilteredNrgResponses = nrgResponses.filter { !profanityFilter.isProfane(it) }
        if (profanityFilteredNrgResponses.isEmpty() && nrgResponses.isNotEmpty()) {
            throw Exception("All generated responses were profane.")
        }

        val nrgFilterFilteredNrgResponses = profanityFilteredNrgResponses.filter { !nrgFilter.isToBeFiltered(it) }
        if (nrgFilterFilteredNrgResponses.isEmpty() && profanityFilteredNrgResponses.isNotEmpty()) {
            throw Exception("All generated responses were filtered by nrg filter.")
        }

        val polishedNrgResponses = nrgFilterFilteredNrgResponses.map { polish(it) }.filter { it.isNotEmpty() }
        if (polishedNrgResponses.isEmpty() && nrgFilterFilteredNrgResponses.isNotEmpty()) {
            throw Exception("All generated responses were empty after polishing.")
        }

        val rptResponseArgmax = dialogRpt(generatorContext.lastOrNull()
                ?: "", polishedNrgResponses, rankerModel).scores.argmax()
        return polishedNrgResponses[rptResponseArgmax]
    }

    private fun extractResponseItems(turn: Turn): String {
        val responses = mutableListOf<String?>()
        turn.responseItems.filter { it.text != null && !(it.text?:"").startsWith("#")}.forEach { x ->
            responses.add(x.text)
        }
        return responses.joinToString(separator = " ")
    }

    private fun constructGeneratorContext(contextLength: Int, useLastResponse: Boolean = false): List<String> {
        val generatorContext = mutableListOf<String>()
        if (useLastResponse) {
            generatorContext.add(extractResponseItems(context.turn))
        }
        if (!context.turn.input.transcript.text.startsWith('#')) {
            generatorContext.add(context.turn.input.transcript.text)
        }
        if (contextLength > 1) {
            val relevantTurns = context.previousTurns
            for (it in relevantTurns) {
                if (generatorContext.size < contextLength) {
                    val response = extractResponseItems(it)
                    if (response.isNotEmpty()) {
                        generatorContext.add(response)
                    }
                } else {
                    break
                }
                if (generatorContext.size < contextLength) {
                    if (!it.input.transcript.text.startsWith('#')) {
                        generatorContext.add(it.input.transcript.text)
                    }
                } else {
                    break
                }
            }
        }
        return generatorContext.reversed()
    }

    private fun nrg(context: List<String>, responseStart: String?, dialogueAct: DialogueAct?, generatorModel: GeneratorModel, generatorParameters: MutableMap<String, Any>): NRGResponse {
        generatorParameters["context"] = context
        responseStart?.let { generatorParameters["response_start"] = it }
        dialogueAct?.let {generatorParameters["dialogue_act"] = it}
        return call(ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.nrg), generatorParameters, generatorModel.toString())
    }

    private fun dialogRpt(context: String, variants: List<String>, rankerModel: RankerModel): DialogRPTResponse {
        val input = mapOf(
                "context" to context,
                "messages" to variants
        )
        return call(ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.dialogrpt), input, rankerModel.toString())
    }

    private inline fun <reified T> call(url: String, input: Map<String, Any>, model: String): T =
            ai.flowstorm.common.client.RestClient.call(
                    java.net.URL("$url/v2/models/$model/infer"),
                    responseType = T::class.java,
                    method = "POST",
                    output = input)

    private fun <T : Comparable<T>> Iterable<T>.argmax(): Int {
        return withIndex().maxByOrNull { it.value }?.index ?: -1
    }

    private fun polish(response: String): String {
        val remove = listOf<String>(":)", ":D", ":/", ":\\", ")", "(", "[", "]", "{", "}", ":")
        val replace = mapOf<String, String>("ive" to "I've", "im" to "I'm")

        var polishedResponse = response
        for (x in remove) {
            polishedResponse = polishedResponse.replace(x, "")
        }
        for ((key, value) in replace) {
            polishedResponse = ("\\b$key\\b").toRegex().replace(polishedResponse, value)
        }
        polishedResponse = ("^\\W+|[^a-zA-Z\\d.!?]+$").toRegex().replace(polishedResponse, "")
        polishedResponse = ("<\\|.*?\\|>").toRegex().replace(polishedResponse, "")
        return polishedResponse
    }
}