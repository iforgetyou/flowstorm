FROM pytorch/pytorch:1.6.0-cuda10.1-cudnn7-runtime

ADD ./dialogrpt/ /app/dialogrpt
ADD ./training/ /app/training
ADD ./download_s3_folder.py /app/download_s3_folder.py
ADD ./config.py /app/config.py
ADD ./requirements.txt /app/requirements.txt
RUN cd /app && pip install -r requirements.txt --ignore-installed

WORKDIR /app

CMD ["python3", "-m", "dialogrpt.api", "--cuda", "true", "--max_input_tokens", "400"]

# Expose port
EXPOSE 8080