group = "ai.flowstorm.nlp"
description = "Flowstorm NLP Profanity"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-lib"))
}