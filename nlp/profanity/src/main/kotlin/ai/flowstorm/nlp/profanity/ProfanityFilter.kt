package ai.flowstorm.nlp.profanity

import ai.flowstorm.common.ObjectUtil.defaultMapper
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.Input
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.lazyExpiring
import com.fasterxml.jackson.core.type.TypeReference
import java.io.IOException
import java.net.URL

class ProfanityFilter : PipelineComponent {

    private val logger by LoggerDelegate()

    @ConfigValue("profanity.sourceUrl", "https://core.flowstorm.ai/file/assets/spaces/61e589c079e5143f41f6a083")
    lateinit var url: String

    private val rules by lazyExpiring(300) {
        URL(url).openStream().use {
            try {
                defaultMapper.readValue(it, object : TypeReference<List<String>>() {})
            } catch (e: IOException) {
                logger.error("Cannot parse json file with profanities: ${e.message}. No profanity filtering will be used in turn processing!")
                listOf<String>()
            }
        }
    }

    val regexRules get() = (rules.map { "\\b${it}\\b" } + "\\b[a-z]\\*+").map { it.toRegex(RegexOption.IGNORE_CASE) }

    fun isProfane(text: String) = regexRules.any { it.containsMatchIn(text) }


    override fun process(scope: PipelineScope, span: PerformanceSpan) = with(scope) {
        if (isProfane(context.input.transcript.text)) {
            context.turn.input.classes.add(Input.Class(Input.Class.Type.Profanity, "ProfanityFilter", 1.0F, url))
            //context.input.action = PROFANITY
        }
    }

    fun bleepProfanities(text: String) = regexRules.fold(text) { censored, regex -> regex.replace(censored, BLEEP) }


    companion object {
        const val PROFANITY = "profanity"
        const val BLEEP = "BLEEP"
    }
}