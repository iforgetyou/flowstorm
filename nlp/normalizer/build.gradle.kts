group = "ai.flowstorm.nlp"
description = "Flowstorm NLP Normalization"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-lib"))
    implementation("org.junit.jupiter:junit-jupiter:5.7.0")
}