package ai.flowstorm.nlp.normalizer

import ai.flowstorm.core.Input
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.nlp.normalizer.provider.RemoteTextNormalizationRulesProvider
import ai.flowstorm.nlp.normalizer.provider.TextNormalizationRulesProvider
import javax.inject.Inject

class Normalizer : PipelineComponent {
    private val logger by LoggerDelegate()

    @Inject
    lateinit var rulesProvider: TextNormalizationRulesProvider

    fun normalize(inputString: String, language: String): String = rulesProvider.getRules(language).fold(inputString) { res, rule -> rule.normalize(res)}.trim()


    override fun process(scope: PipelineScope, span: PerformanceSpan) {
        with(scope.context) {
            val language = turn.input.locale.language
            with(turn.input){
                if (!transcript.text.startsWith("#")){
                    transcript.normalizedText = normalize(transcript.text, language)

                    alternatives.forEach { transcript: Input.Transcript ->
                        transcript.normalizedText = normalize(transcript.text, language)
                    }
                }
                logger.info("'${transcript.text}' normalized to '${transcript.normalizedText}'")
            }
        }
    }

    companion object{

        @JvmStatic
        fun main(args: Array<String>) {
            val normalizer = Normalizer()
            normalizer.rulesProvider = RemoteTextNormalizationRulesProvider().apply { urlRules = "https://core.flowstorm.ai/file/assets/spaces/621618ad9beffd49fc7e6daa" }
            println(normalizer.normalize("HELLLLLLLLLLOOO \uD83D\uDC4E", "en"))
            println(normalizer.normalize("10:00", "en"))
            println(normalizer.normalize("i’d love to :)", "en"))
            println(normalizer.normalize("are u there    :)", "en"))
            println(normalizer.normalize("iam fine :)", "en"))
            println(normalizer.normalize("iam !!!!!!!!!!!.", "en"))
        }
    }
}

