package ai.flowstorm.nlp.normalizer.provider

import ai.flowstorm.nlp.normalizer.rules.Rule

interface TextNormalizationRulesProvider {
    fun getRules(language: String): List<Rule>
}