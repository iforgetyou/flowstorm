package ai.flowstorm.nlp.normalizer.rules

data class NotSupportedCharsRule(override val pattern: String, override val value: String,
                                 override val language: String) : Rule {
    private val notSupportedCharsRegex: Regex = pattern.toRegex(option = RegexOption.IGNORE_CASE)

    override fun normalize(text: String): String = text.replace(notSupportedCharsRegex, "")
}
