package ai.flowstorm.nlp.normalizer.rules

data class RegexRule(override val pattern: String, override val value: String, override val language: String) : Rule {
    private val regex: Regex = ("\\b${pattern}\\b").toRegex(option = RegexOption.IGNORE_CASE)

    override fun normalize(text: String): String = text.replace(regex, value)

}
