package ai.flowstorm.nlp.normalizer.provider

import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.nlp.normalizer.rules.Rule
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.lazyExpiring
import com.fasterxml.jackson.core.type.TypeReference
import java.io.IOException
import java.net.URL
import java.util.*

class RemoteTextNormalizationRulesProvider : TextNormalizationRulesProvider {
    private val logger by LoggerDelegate()

    @ConfigValue("normalizer.rules")
    lateinit var urlRules: String

    private val rules by lazyExpiring(300) {
        val withoutCache = UUID.randomUUID().toString()
        try {
            URL("${urlRules}?${withoutCache}").openStream().use {
                ObjectUtil.defaultMapper.readValue(it, object : TypeReference<List<Rule>>() {})
            }
        } catch (e: IOException) {
            logger.error("Cannot parse json file with normalization rules: ${e.message}. No rules will be used in turn processing!")
            listOf<Rule>()
        }
    }

    override fun getRules(language: String): List<Rule> {
        return rules.filter { it.language == language }
    }
}