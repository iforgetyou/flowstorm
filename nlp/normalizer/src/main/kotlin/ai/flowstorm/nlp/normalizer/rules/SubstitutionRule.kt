package ai.flowstorm.nlp.normalizer.rules

data class SubstitutionRule(override val pattern: String, override val value: String, override val language: String) : Rule {
    override fun normalize(text: String): String = text.replace(pattern, " $value ")
}
