package ai.flowstorm.nlp.normalizer.provider

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.nlp.normalizer.rules.Rule
import com.fasterxml.jackson.core.type.TypeReference

class LocalTextNormalizationRulesProvider : TextNormalizationRulesProvider {
    private fun getRulesFromFile(fileName: String = "rules"): List<Rule> = try {
        ObjectUtil.defaultMapper.readValue(
            javaClass.getResource("/${fileName}.json"),
            object : TypeReference<List<Rule>>() {})
    } catch (e: IllegalArgumentException) {
        emptyList()
    }

    override fun getRules(language: String): List<Rule> {
        return getRulesFromFile().filter { it.language == language }
    }
}