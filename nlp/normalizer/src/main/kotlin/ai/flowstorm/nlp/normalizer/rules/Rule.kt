package ai.flowstorm.nlp.normalizer.rules

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = RegexRule::class, name = "regex"),
    JsonSubTypes.Type(value = RegexWithoutBoundaryRule::class, name = "regex_without_boundary"),
    JsonSubTypes.Type(value = SubstitutionRule::class, name = "substitution"),
    JsonSubTypes.Type(value = NotSupportedCharsRule::class, name = "not_supported_chars")
)
interface Rule {
    val pattern: String
    val language: String
    val value: String

    fun normalize(text: String): String
}