package ai.flowstorm.nlp.normalizer

import ai.flowstorm.common.monitoring.performance.Blank
import ai.flowstorm.core.*
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.nlp.normalizer.provider.LocalTextNormalizationRulesProvider
import ai.flowstorm.nlp.normalizer.rules.RegexRule
import ai.flowstorm.util.Locale
import ai.flowstorm.util.LoggerDelegate
import io.mockk.every
import io.mockk.mockkClass
import io.mockk.spyk
import org.junit.jupiter.api.Test

internal class NormalizerTest {

    val logger by LoggerDelegate()

    val pipelineScope = mockkClass(PipelineScope::class)
    val rulesProvider = spyk<LocalTextNormalizationRulesProvider>()
//    val rulesProvider = spyk<RemoteRulesProvider>()
    val normalizer = Normalizer()

    val testBatch = listOf(
        Pair("I dont like it","I do not like it"),
        Pair("don't like it","do not like it"),
        Pair("i'm'o forest cuz i amn't gonna have bday","I am going to forest because I am not going to have birthday"),
    )

    val testBatchAdvanced = listOf(
        Pair("looooooooooooooooooooook at me","look at me"),
        Pair("are u there???????????????","are you there"),
        Pair("are u there ???????????????","are you there"),
        Pair("yeeeeeeeessssssss or noooooooooooooooo!!","yes or no"), // not solved yet
        Pair("yeah...","yeah"),
        Pair("...",""),
        Pair(":D",""),
        Pair("yesss","yes"),
        Pair("u","you"),
        Pair("i wanted tu talk to u","I wanted tu talk to you"),
        Pair("ok čččč","ok"),
        )

    val testEmojiBatch = listOf(
        Pair("bad \uD83D\uDC8B","bad"),
        Pair("what? \uD83E\uDD52", "what cucumber"),
        Pair("ok :)","ok"),
    )

    init {
//        every { pipelineScope.log.logger } returns LoggerFactory.getLogger(this.javaClass)
        every { pipelineScope.context.turn.input.transcript } returns Transcript("")
        every { pipelineScope.context.turn.input.alternatives } returns mutableListOf(Input.Transcript("cuz I can"))
        every { pipelineScope.context.turn.input.locale } returns Locale("en")
        every { pipelineScope.context.logger } returns logger

        normalizer.rulesProvider = rulesProvider
//        every { rulesProvider.getRegexRules(any()) } returns rulesProvider.getRules("https://core.flowstorm.ai/file/assets/spaces/61e6db21e9e4ca66b39837bb")
//        every { rulesProvider.getEmojiRules(any()) } returns rulesProvider.getRules("https://core.flowstorm.ai/file/assets/spaces/61e6dafbe9e4ca66b3982366")
//        every { rulesProvider.getNotSupportedChars(any()) } returns rulesProvider.getRules("https://core.flowstorm.ai/file/assets/spaces/61e6db12e9e4ca66b3983050?")

    }

    @Test
    fun `try alternatives`() {
        pipelineScope.context.turn.input.transcript.text = testBatch.first().first
        normalizer.process(pipelineScope, Blank)
        assert(pipelineScope.context.turn.input.transcript.normalizedText == testBatch.first().second)
        assert(pipelineScope.context.turn.input.alternatives.first().normalizedText == "because I can")
    }

    @Test
    fun `try emojis`() {
        testEmojiBatch.forEach {
            pipelineScope.context.turn.input.transcript.text = it.first
            normalizer.process(pipelineScope, Blank)
            assert(pipelineScope.context.turn.input.transcript.normalizedText == it.second)
        }
    }

    @Test
    fun `try advanced`() {
        testBatchAdvanced.forEach {
            pipelineScope.context.turn.input.transcript.text = it.first
            normalizer.process(pipelineScope, Blank)
            println("'${it.first}'->'${pipelineScope.context.turn.input.transcript.normalizedText}' ?= '${it.second}'")
            assert(pipelineScope.context.turn.input.transcript.normalizedText == it.second)
        }
    }

    @Test
    fun process() {
        val rulesProvider = spyk<LocalTextNormalizationRulesProvider>()
        every { rulesProvider.getRules(any()) } returns listOf(
            RegexRule("i", "I", language = "en"),
            RegexRule("dont", "do not" ,language = "en"),
            RegexRule("don't", "do not", language = "en"),
            RegexRule("cuz", "because", language = "en"),
            RegexRule("amn't", "am not", language = "en"),
            RegexRule("gonna", "going to", language = "en"),
            RegexRule("i'm'o", "I am going to", language = "en"),
            RegexRule("I'm'o", "I am going to", language = "en"),
            RegexRule("bday", "birthday", language = "en"),
        )
        normalizer.rulesProvider = rulesProvider

        testBatch.forEach {
            pipelineScope.context.turn.input.transcript.text = it.first
            normalizer.process(pipelineScope, Blank)
            assert(pipelineScope.context.turn.input.transcript.normalizedText == it.second)
            assert(pipelineScope.context.turn.input.transcript.text == it.first)
        }
    }

}
