from typing import Optional

from common.blocks.classification.token.lstm import LstmTrainer
from common.blocks.embeddings.char import CharEmbedding, CaseEmbedding
from common.blocks.embeddings.fasttext import FastTextSW
from common.blocks.preprocessing import Tokens, Padding, Labels, Preprocessing
from common.pipeline import Pipeline
from common.pipeline.stage import Stage


def init_entity_pipeline(_id: str, lang: str = 'en', config: Optional[dict] = None) -> Pipeline:
    return Pipeline("lstm-case-char").add_stage(
        Stage("preproc")
            .add_component(prep := Preprocessing())
    ).add_stage(
        Stage('tokens-labels')
            .add_component(labels := Labels(30).with_input_from(prep))
            .add_component(tokens := Tokens().with_input_from(prep))
    ).add_stage(
        Stage('embedding')
            .add_component(emb := FastTextSW(lang=lang).with_input_from(tokens))
            .add_component(char := CharEmbedding(30, 20).with_input_from(tokens))
            .add_component(case := CaseEmbedding(30).with_input_from(tokens))
    ).add_stage(
        Stage('padding')
            .add_component(pad := Padding(30).with_input_from(emb))
    ).add_stage(
        Stage('training')
            .add_component(LstmTrainer(_id, lang, config or {}).with_input_from(pad, char, case, labels))
    )
