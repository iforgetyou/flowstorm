import logging
import base64
import numpy as np
from abc import ABC, abstractmethod
from sklearn.feature_extraction.text import TfidfVectorizer

from common.blocks.embeddings import EmbeddingFactory
from common.utils import make_logger_name

logger = logging.getLogger(make_logger_name(__name__))

EM = EmbeddingFactory()


class VectorGetter:
    @staticmethod
    def get_vectors(data):
        """
        Return list of vectors for given queries
        """

        assert 'auth_key' in data

        model = data.get('model')

        # model defaults
        model['algorithm'] = model.get('algorithm', 'FastTextSW') if "embedding" not in model else model.get("embedding", "FastTextSW")
        model['lang'] = model.get('lang', 'en')
        model['use_tfidf'] = model.get('use_tfidf', False)

        emb = EM.get_model(emb=model['algorithm'] if "embedding" not in model else model.get("embedding", "FastTextSW"), lang=model['lang'])

        payload = {intent: {"vectors": [emb.vector(sen).tolist() for sen in queries["questions"]], "questions": queries["questions"]} for intent, queries in data.get("qa", {}).items()}
        return payload
