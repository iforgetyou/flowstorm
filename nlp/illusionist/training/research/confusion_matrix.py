import json
import time
import logging

from sklearn.model_selection import KFold
import numpy as np

from common.blocks.classification.sentence.logistic_regression import LogisticRegression
from common.utils import make_logger_name

logger = logging.getLogger(make_logger_name(__name__))

n_fold = 25


class ConfusionMatrix(object):
    """
    Preprocessing of the dialog to be shown with Facets (see https://pair-code.github.io/facets/)
    """

    @staticmethod
    def generate(model_params, qa):
        X = []
        y = []
        for true_label, data in qa.items():
            for testing_query in data["questions"]:
                X.append(testing_query)
                y.append(true_label)

        X = np.array(X)
        y = np.array(y)

        similarity_data = []
        start_time = time.time()

        skf = KFold(n_splits=n_fold if n_fold < len(X) else len(X), random_state=42)
        for train_index, test_index in skf.split(X):
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]

            kfold_qa = {k: {"questions": [], "answer": k, "threshold": qa[k]["threshold"]} for k in set(y_train)}
            for label, question in zip(y_train, X_train):
                kfold_qa[label]["questions"].append(question)

            payload = {"model": model_params, 'qa': kfold_qa}
            QS = LogisticRegression(payload)

            for true_label, testing_query in zip(y_test, X_test):
                results = QS.get(testing_query, n=1)

                output = {"similarity": results[0]["confidence"],
                          "predicted_label": results[0]["answer"],
                          "closest_sentence": results[0]["hit"],
                          "true_label": true_label,
                          "given_sentence": testing_query}

                similarity_data.append(output)
            end_time = time.time()

            logger.debug("Generating confusion matrix take {}s".format(end_time - start_time))

            acc = sum([1 if x["predicted_label"] == x["true_label"] else 0 for x in similarity_data]) / float(
                len(similarity_data))

        response = {
            "oov": list(QS.emb._oov_vectors.keys()),
            "num_possible_classes": len(set(payload['qa'].keys())),
            "data": json.dumps(similarity_data),
            "accuracy": acc
        }

        return response
