# -*- coding: utf-8 -*-
"""
    training.model_manager
    ~~~~~~~~~~~~~~~~~~~~~~

    Module for managing loaded NLP models.
"""
import datetime
import importlib
import logging
from random import getrandbits

import pytz
from deepmerge import always_merger

from common.api.exceptions import ModelPermissionError, ModelNotFoundError, UnknownAlgorithm
from common.blocks.classification.base import ClassificationModelBase
from common.blocks.classification.sentence.cosine_similarity import CosineSimilarity
from common.blocks.classification.sentence.logistic_regression import LogisticRegression
from common.blocks.classification.sentence.hybrid import Hybrid
from common.config import CONFIG
from common.persistence import SerializableModel, StorageFactory, Status
from common.pipeline import Pipeline
from common.pipeline.component import Serializable
from common.utils import make_logger_name
from common.model_manager import BaseModelManager
from common.concurrency import ExecutorFactory
from training.pipeline import init_entity_pipeline

logger = logging.getLogger(make_logger_name(__name__))


class IntentModelManager(BaseModelManager):
    """ Class that holds all currently running models. """

    def __init__(self):
        super().__init__()
        logger.info("Model Manager initialization complete")

    def get_model(self, _id, auth_key, allow_global=False):
        """
        Get a model by ID.
        :param _id: model ID
        :param auth_key: authentication key
        :param allow_global: allow regular users to see global (studio created) models
        :return: model instance
        """

        model = None
        data = self.storage.get_model_data(_id)
        if data:
            logger.info('Loaded model _id: %s', _id)
            algorithm = data["model"].get("approach", data["model"].get("algorithm", ""))
            if algorithm == "logistic":
                model_cls = LogisticRegression
            elif algorithm == "hybrid":
                model_cls = Hybrid
            elif algorithm == "cosine":
                model_cls = CosineSimilarity
            else:
                raise UnknownAlgorithm(f"Unkown algorithm {data['model'].get('algorithm', data['model'].get('approach', 'NOT-SET-PROPERLY'))}")
            model = model_cls(data.get('model'), data.get('qa'), data.get('runtime'))

        if model:
            model_key = data.get('auth_key')
            if model_key == auth_key:
                return model
            raise ModelPermissionError(f'no model with id {_id} for the given api_key found')
        raise ModelNotFoundError(f'no model with id {_id} for the given api_key found')

    def create_model(self, _id, data):
        """
        Add a new model.
        :param _id: model ID
        :param data: data passed to the model for init
        :param persist: persist data
        :return: added model params
        """

        assert 'auth_key' in data

        if not _id:
            _id = self.generate_id()

        # unifying creation of all new models
        data["model"] = self._normalizing_model_params(data["model"])

        algorithm = data["model"]["algorithm"]
        if algorithm == "logistic":
            model = LogisticRegression(data['model'])
        elif algorithm == "hybrid":
            model = Hybrid(data["model"])
        elif algorithm == "cosine":
            model = CosineSimilarity(data['model'])
        else:
            raise UnknownAlgorithm(f"Unknown algorithm: {algorithm}")
        model.data['auth_key'] = data['auth_key']
        self._save_model(_id, model)

        future = ExecutorFactory.get().submit(self._init_model, _id, model, data)
        future._self.result()
        params = model.data.get('model', dict()).copy()
        params['_id'] = _id
        return params

    def update_model(self, _id, data):
        """
        Update model data.
        :param _id: model ID
        :param data: data passed to the model for init
        :return: updated model params
        """

        model = self.get_model(_id, data.get('auth_key'))
        model.data['auth_key'] = data['auth_key']
        self._save_model(_id, model)

        future = ExecutorFactory.get().submit(self._init_model, _id, model, data)
        future._self.result()
        params = data.get('model', dict()).copy()
        params['_id'] = _id
        return params

    def _save_model(self, _id: str, model: ClassificationModelBase):
        model.data['lastModified'] = datetime.datetime.now(pytz.utc)
        to_save = model.data.copy()
        if model.data['model']['initialized']:
            to_save['runtime'] = model.get_runtime_data()

        self.storage.save_model_data(_id, to_save)

    def _init_model(self, _id: str, model: ClassificationModelBase, data: dict):
        model.initialize(data)
        self._save_model(_id, model)
        self._clean_outdated_models(data['model']['name'], data['auth_key'])

    def _clean_outdated_models(self, name, auth_key, older_than_days=30):
        older_than = datetime.datetime.now() - datetime.timedelta(days=older_than_days)
        old_models = self.storage.load_persisted({'model.name': name, 'auth_key': auth_key, 'lastModified': {'$lt': older_than}})
        for model in old_models:
            logger.info(f'Deleting old model, name: {model["model"]["name"]}, _id: {model["_id"]}, last modified: {model["lastModified"]}')
            self.delete_model(model['_id'], model['auth_key'])

    @staticmethod
    def _normalizing_model_params(model):
        model["algorithm"] = model.get("approach", model.get("algorithm", "NOT-SET"))
        if "approach" in model:
            del model["approach"]

        if model["lang"] == "en" and bool(model.get("overwrite", True)):
            model["embedding"] = "UniversalSentenceEncoder"
        elif model["lang"] != "en" and bool(model.get("overwrite", True)):
            model["embedding"] = "FastTextSW"

        if "overwrite" in model:
            del model["overwrite"]

        return model

    def delete_model(self, _id, auth_key):
        """
        Delete model by ID.
        :param _id: model ID
        :param auth_key: authentication key
        :return: 0 if OK
        """

        self.storage.delete_model_data(_id, auth_key)
        return 0

    def generate_id(self):
        """
        Generate unique (not yet used) model ID.
        :return: model ID
        """

        _id = generate_md5()
        while _id in [model['_id'] for model in self.storage.load_persisted({}, {'_id': 1})]:
            _id = generate_md5()
        return _id


loader_location_annotator = "app.api.entity"
loader_location_trainer = "training.api.entity"

model_identification = "{}_{}"


class EntityModelManager(BaseModelManager):
    """ Class that holds all currently running models. """

    def __init__(self):
        super().__init__()
        self.storage = StorageFactory().get_entity_storage_from_config()
        self._fail_pending_models()

    def _fail_pending_models(self):
        for model in self.storage.get_all():
            if model.status == Status.IN_PROGRESS:
                logger.warning(f'Model {model.id} has status IN_PROGRESS during the start (possibly'
                               f' crashed training). Setting status to FAILED')
                model.status = Status.FAILED
                self.storage.save_model(model)

    def get_model(self, _model_type, _id, auth_key="XXX", language="en"):
        """
        Get a model by ID.
        :param language:
        :param _model_type:
        :param _id: model ID
        :param auth_key: authentication key TODO
        :return: model instance
        """
        language = language.split('_')[0]
        lang_id = model_identification.format(language, _id)

        class_name, model_data, mapping, config = self.storage.get_model(_id)
        if model_data:
            logger.info('Loaded model _id: %s', lang_id)
            module = importlib.import_module(loader_location_annotator)
            try:
                class_ = getattr(module, _model_type)
                return class_(_id=_id, language=language, model_data=model_data, mapping=mapping, config=config, vectorizer=self.emb)
            except Exception as e:
                logger.debug("Error :{}".format(e))
                raise KeyError(f'no model with type {_model_type} found')

    def get_model_status(self, _id, auth_key='XXX', language='en') -> Status:
        model = self.storage.get_model(_id)
        if model is None:
            raise ModelNotFoundError(f'no model with id {_id} for the given api_key found')
        return model.status

    def train_model(self, data, _id=None, space_id=None, language="en", auth_key="XXX", save=True):
        """
        Update model data.
        :param auth_key:
        :param language:
        :param _id: model ID
        :param space_id: space ID which the model belongs to
        :param data: data passed to the model for init
        :param save: whether to save model or not
        :return: updated model params
        """
        if _id is None:
            _id = generate_md5()
        space_id = None if space_id == 'null' else space_id

        train_config = always_merger.merge(CONFIG.ner["default"]["params"], data.get("params") or {})

        pipeline = init_entity_pipeline(_id, language, train_config)
        model = self.storage.get_model(_id) or SerializableModel(_id, data['name'], language, space_id, Status.IN_PROGRESS, [])
        model.name = data['name']
        model.status = Status.IN_PROGRESS
        self.storage.save_model(model)
        ExecutorFactory.get().submit(self._run_job, _id, space_id or "ROOT", language, pipeline, data)

        return _id

    def _run_job(self, _id: str, space_id: str, language: str, pipeline: Pipeline, data):
        try:
            pipeline.train([data])
            self.save_pipeline(_id, space_id, data['name'], language, pipeline)
        except Exception as e:
            self.storage.save_model(SerializableModel(_id, data['name'], language, space_id,
                                                      Status.FAILED, []))
            raise e

    def save_pipeline(self, _id: str, space_id: str, name: str, language: str, pipeline: Pipeline):
        item = SerializableModel(_id, name, language, space_id, Status.FINISHED,
                                 pipeline.get_component_list(), pipeline=pipeline.name)
        for component in item.components:
            if isinstance(component, Serializable):
                item.binaries.append(SerializableModel.Binary(f'{item.id}-{component.name}', component.serialize()))
        self.storage.save_model(item)

    def delete_model(self, _id):
        self.storage.delete_model(_id)


def generate_md5():
    """
    Generate a random md5 hash.
    :return: md5 hash string
    """

    return '%032x' % getrandbits(128)
