"""

    Default (un-categorized) API endpoints for training.
"""
import jsons
from flask import request

from training.model_manager import EntityModelManager

MM = EntityModelManager()


def train_model(_id, space_id, language, data):
    """
    Query a QA model.
    :param data:
    :param language:
    :param _id: model ID
    :param space_id: space ID which the model belongs to
    :return: json response, status code
    """

    key = request.args.get('key', "XXX")

    res = MM.train_model(_id=_id, space_id=space_id, data=data, language=language)
    return res, 200


def get_model_status(_id):
    status = MM.get_model_status(_id)
    return {'status': jsons.dump(status)}, 200


def delete_model(_id):
    """
    Query a QA model.
    :param _id: model ID
    :return: json response, status code
    """

    key = request.args.get('key', "XXX")

    res = MM.delete_model(_id=_id)
    return res, 200
