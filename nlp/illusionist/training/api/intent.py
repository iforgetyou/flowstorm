# -*- coding: utf-8 -*-
"""
    training.api.models
    ~~~~~~~~~~~~~~~~~~~

    API endpoints for QA model management.
"""
import logging

from flask import request

from common.utils import make_logger_name
from training.model_manager import IntentModelManager
from training.research.confusion_matrix import ConfusionMatrix
from training.research.get_vectors import VectorGetter


MM = IntentModelManager()
logger = logging.getLogger(make_logger_name(__name__))


### Legacy functions ###


def legacy_get_model_by_id(_id):
    return get_model_by_id(_id)


def legacy_post_model_by_id(_id, data):
    return post_model_by_id(_id, data)


def legacy_put_model_by_id(_id, data):
    return put_model_by_id(_id, data)


def legacy_delete_model_by_id(_id):
    return delete_model_by_id(_id)


def legacy_post_analysis_confusion_matrix(data):
    return post_analysis_confusion_matrix(data)


def legacy_post_sentences(data):
    return post_sentences(data)


### End of legacy functions ###


def get_model_by_id(_id):
    """
    Get QA model data by model ID.
    :param _id: model ID
    :return: json response, status code
    """

    key = request.args.get('key')

    data = MM.get_model(_id=_id, auth_key=key, allow_global=True).data.copy()
    data['auth_key'] = '<HIDDEN>'

    return data, 200


def post_model_by_id(_id, data):
    """
    Create a QA model.
    :param _id: model ID (use None to generate random)
    :param data: QA model data
    :return: json response, status code
    """

    data['auth_key'] = request.args.get('key')
    if not _id or _id.lower() == 'none':
        _id = None

    res = MM.create_model(_id=_id, data=data)

    return res, 200


def put_model_by_id(_id, data):
    """
    Update a QA model by model ID.
    :param _id: model ID
    :param data: QA model data
    :return: json response, status code
    """

    data['auth_key'] = request.args.get('key')

    res = MM.update_model(_id=_id, data=data)

    return res, 200


def delete_model_by_id(_id):
    """
    Delete a QA model by model ID.
    :param _id: model ID
    :return: json response, status code
    """

    key = request.args.get('key')

    MM.delete_model(_id=_id, auth_key=key)

    return {}, 200


def post_analysis_confusion_matrix(data):
    """
    Update a QA model by model ID.
    :param data: QA model data
    :return: json response, status code
    """

    data['auth_key'] = request.args.get('key')

    res = ConfusionMatrix.generate(data["model_params"], data["qa"])

    return res, 200


def post_sentences(data):
    """
    Return vector for given sentences
    :param data: QA model data - as for training
    :return: json response, status code
    """

    data['auth_key'] = request.args.get('key')

    res = VectorGetter.get_vectors(data)

    return res, 200
