FROM python:3.8-slim AS builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        apt-utils build-essential curl locales net-tools telnet && \
    rm -rf /var/cache/apt/* && \
    rm -rf /var/lib/apt/lists/*

# Set locale (required by pipenv and Click package)
# http://click.pocoo.org/5/python3/
# http://jaredmarkell.com/docker-and-locales/
RUN sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8

# Install core python packages and pipenv
RUN pip3 install --no-cache-dir --disable-pip-version-check pipenv setuptools wheel

# Install requirements
WORKDIR /root
COPY Pipfile Pipfile.lock ./
RUN pipenv install --system

FROM python:3.8-slim AS runtime

ENV APP=training

# Create a new user and group (to not run docker as a root user)
ENV USER app
ENV USER_HOME /home/$USER
RUN groupadd -g 999 $USER && \
    useradd -r -u 999 -g $USER $USER && \
    mkdir -p $USER_HOME && \
    chown $USER:$USER -R $USER_HOME
WORKDIR $USER_HOME

COPY --from=builder /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages
COPY --from=builder /usr/local/bin/gunicorn /usr/local/bin/gunicorn
USER $USER
COPY --chown=999 ./${APP} ./${APP}
COPY --chown=999 ./common ./common
COPY --chown=999 ./conf ./conf
COPY --chown=999 ./run.py .

EXPOSE 8080

# Start gunicorn
CMD ["gunicorn", "run:app", "--bind=:8080", "--workers=1", "--threads=10", "--timeout=300", "--graceful-timeout=600"]