from __future__ import annotations
import datetime
import os
import json
import pickle
from dataclasses import dataclass, field
from typing import List, Union, Optional

import boto3
import botocore
import logging
import decimal

import jsons
from gridfs import GridFS
from enum import Enum, auto
from pymongo import MongoClient
from pynamodb.attributes import UnicodeAttribute, MapAttribute, UTCDateTimeAttribute
from pynamodb.models import Model
from boto3.dynamodb.conditions import Key

from common.api.exceptions import ModelNotFoundError
from common.config import CONFIG
from common.pipeline import Component
from common.utils import make_logger_name, ObjectUtil

logger = logging.getLogger(make_logger_name(__name__))


class Status(Enum):
    FINISHED = auto()
    IN_PROGRESS = auto()
    FAILED = auto()
    UNKNOWN = auto()


@dataclass
class SerializableModel:
    @dataclass
    class Binary:
        id: str
        data: bytes

    id: str
    name: str
    language: str
    space_id: Optional[str]
    status: Status = Status.UNKNOWN
    components: Union[List[Component], list] = field(default_factory=lambda: [])
    binaries: List[Union[SerializableModel.Binary, str]] = field(default_factory=lambda: [])
    pipeline: Optional[str] = None
    last_modified: datetime.datetime = field(default_factory=lambda: datetime.datetime.now())


class FileStorage:
    __data_model_dir = os.path.join(os.path.dirname(__file__), os.pardir, './data')

    def load_persisted(self, query={}, projection=None):
        """
        Load saved json-file models.
        :return: list of added model params
        """

        name = query.get("model.name", "")
        time_limit = query.get("lastModified", {"$lt": datetime.datetime.now()}).get("$lt", datetime.datetime.now())
        files = os.listdir(self.__data_model_dir)
        files = [fn for fn in files if fn.endswith('.plk') and name in fn]

        models = []
        for name in files:
            path = os.path.join(self.__data_model_dir, name)
            filestamp = os.stat(path).st_mtime
            if datetime.datetime.fromtimestamp(filestamp) < time_limit:
                with open(path, "rb") as f:
                    data = pickle.load(f)
                data['_id'] = name.rstrip('.plk')
                models.append(data)

        return models

    def get_model_data(self, _id, newer_than=datetime.datetime.min):
        path = os.path.join(self.__data_model_dir, _id + '.plk')
        try:
            with open(path, "rb") as f:
                return pickle.load(f)
        except FileNotFoundError:
            return None

    def save_model_data(self, _id, data):
        """
        Save model data to a json file.
        :param _id: model ID
        :param data: model data
        """

        data['_id'] = _id
        path = os.path.join(self.__data_model_dir, f'{_id}.plk')
        with open(path, 'wb') as f:
            pickle.dump(data, f)

    def delete_model_data(self, _id, auth_key):
        """
        Delete model data file.
        :param _id: model ID
        """

        path = os.path.join(self.__data_model_dir, f'{_id}.plk')
        try:
            os.remove(path)
        except FileNotFoundError:
            raise ModelNotFoundError(f'Cannot delete model with id {_id}. No such model.')


class MongoDBStorage:
    def __init__(self, coll_name="model"):
        self.client = MongoClient(CONFIG["database.url"])
        self.db = self.client[f'{CONFIG["name"]}-{CONFIG.get("dataSuffix", CONFIG["namespace"])}']
        self.grid_fs = GridFS(self.db)
        self.collection = self.db[coll_name]

    def load_persisted(self, query={}, projection=None):
        return self.collection.find(query, projection)

    def get_model_data(self, _id, newer_than=datetime.datetime.min):
        data = self.collection.find_one({'_id': _id, 'lastModified': {'$gt': newer_than}})
        runtime = self.grid_fs.find_one({'_id': _id})
        if runtime is not None:
            data['runtime'] = pickle.loads(runtime.read())
        return data

    def save_model_data(self, _id, data):
        data['_id'] = _id
        runtime = data.pop('runtime', None)
        self.collection.update_one({'_id': _id}, {"$set": data}, upsert=True)
        g = self.grid_fs.find_one({'_id': _id})
        if g is not None:
            self.grid_fs.delete(g._id)
        if runtime is not None:
            self.grid_fs.put(pickle.dumps(runtime), _id=_id)

    def delete_model_data(self, _id, auth_key):
        r = self.collection.delete_one({'_id': _id, 'auth_key': auth_key})
        if r.deleted_count == 0:
            raise ModelNotFoundError(f'Cannot delete model with id {_id}. No such model.')
        g = self.grid_fs.find_one({'_id': _id})
        if g is not None:
            self.grid_fs.delete(g._id)


class CassandraMongoDBStorage:
    def __init__(self, coll_name="entity"):
        self.client = MongoClient(CONFIG["database.url"])
        self.db = self.client[f'{CONFIG["name"]}-{CONFIG.get("dataSuffix", CONFIG["namespace"])}']
        self.grid_fs = GridFS(self.db)
        self.collection = self.db[coll_name]

    def get_all(self, load_binary=False):
        models = []
        for model in self.collection.find({}):
            model = jsons.load(model, SerializableModel)
            if load_binary:
                model.binaries = self.get_binary_data(model.binaries)
            models.append(model)
        return models

    def get_model(self, _id) -> Optional[SerializableModel]:
        data = self.collection.find_one({'id': _id})
        if data is None:
            return None
        item = jsons.load(data, SerializableModel)
        item.binaries = self.get_binary_data(item.binaries)
        return item

    def get_by_space_id(self, space_id, language, include_root=False) -> List[SerializableModel]:
        if include_root:
            items = self.collection.find({'language': language, '$or': [{'space_id': space_id}, {'space_id': 'ROOT'}]})
        else:
            items = self.collection.find({'language': language, 'space_id': space_id})
        return [jsons.load(i, SerializableModel) for i in items]

    def get_binary_data(self, ids: List[str]) -> List[SerializableModel.Binary]:
        res = []
        for _id in ids:
            binary = self.grid_fs.find_one({'_id': _id})
            if binary is not None:
                res.append(SerializableModel.Binary(binary._id, binary.read()))
        return res

    def save_model(self, item: SerializableModel):
        item.binaries = [self.save_binary_data(binary) for binary in item.binaries]
        serialized = json.loads(jsons.dumps(item, strip_privates=True))
        self.collection.update_one({'id': item.id}, {"$set": serialized}, upsert=True)

    def save_binary_data(self, item: SerializableModel.Binary):
        g = self.grid_fs.find_one({'_id': item.id})
        if g is not None:
            self.grid_fs.delete(g._id)
        self.grid_fs.put(item.data, _id=item.id)
        return item.id

    def delete_model(self, _id):
        for binary in self.grid_fs.find(filter={'metadata.refId': _id}):
            self.grid_fs.delete(binary._id)
        self.collection.delete_one({'id': _id})


class DynamoDBS3Storage:

    def __init__(self):
        self.s3 = boto3.resource('s3')
        DynamoDBModel.create_table(read_capacity_units=1, write_capacity_units=1)

    def load_persisted(self, query={}, projection=None):
        # Fixme query is not working
        if query == {}:
            return (self._unconvert(r) for r in DynamoDBModel.scan())
        else:
            return []

    def get_model_data(self, _id, newer_than=datetime.datetime.min):
        try:
            data = self._unconvert(DynamoDBModel.get(_id))
            s3_handle = self.s3.Object(CONFIG["s3"]["bucket"], f'{CONFIG["name"]}/{_id}.pickle')
            try:
                data['runtime'] = pickle.loads(s3_handle.get()['Body'].read())
            except botocore.exceptions.ClientError:
                pass
            return data
        except DynamoDBModel.DoesNotExist:
            return None

    def save_model_data(self, _id, data):
        data['_id'] = _id
        runtime = data.pop('runtime', None)
        data['qa'] = data.get('qa') or {}

        s3_handle = self.s3.Object(CONFIG["s3"]["bucket"], f'{CONFIG["name"]}/{_id}.pickle')
        s3_handle.put(Body=pickle.dumps(runtime))

        DynamoDBModel(**data).save()

    def _unconvert(self, entry):
        entry = entry.attribute_values
        for k in entry:
            if type(entry[k]) is MapAttribute:
                entry[k] = entry[k].attribute_values
        return entry

    def delete_model_data(self, _id, auth_key):
        # Fixme check model auth_key
        try:
            DynamoDBModel.get(_id).delete()
        except DynamoDBModel.DoesNotExist:
            raise ModelNotFoundError(f'Cannot delete model with id {_id}. No such model.')


class DynamoDBModel(Model):
    class Meta:
        # FIXME do no load table name statically
        try:
            table_name = CONFIG["dynamodb"]["table"]
        except KeyError:
            table_name = ""
    _id = UnicodeAttribute(hash_key=True)
    lastModified = UTCDateTimeAttribute()
    qa = MapAttribute()
    model = MapAttribute()
    auth_key = UnicodeAttribute()


class CassandraDynamoDBS3Storage:
    def __init__(self):
        jsons.set_serializer(lambda obj, **_: decimal.Decimal(str(obj)), float)
        dynamodb = boto3.resource('dynamodb', region_name=CONFIG['dynamodb']['region'])
        self.table = dynamodb.Table('illusionist.entity')
        self.s3 = boto3.resource('s3')
        self.mapper = ObjectUtil(use_decimals=True)

    def get_model(self, _id) -> Optional[SerializableModel]:
        data = self.table.get_item(Key={'id': _id}).get('Item')
        if data is None:
            return None
        item = self.mapper.load(data, SerializableModel)
        item.binaries = self.get_binary_data(item.binaries)
        return item

    def get_by_space_id(self, space_id, language, include_root=False) -> List[SerializableModel]:
        items = self.table.query(
            IndexName='space_id',
            KeyConditionExpression=Key('space_id').eq(space_id)
        ).get('Items', [])
        return [self.mapper.load(i, SerializableModel) for i in items]

    def get_binary_data(self, ids: List[str]) -> List[SerializableModel.Binary]:
        res = []
        for _id in ids:
            s3_handle = self.s3.Object(CONFIG["s3"]["bucket"], f'{CONFIG["name"]}/entity/{_id}.pickle')
            res.append(SerializableModel.Binary(_id, s3_handle.get()['Body'].read()))
        return res

    def save_model(self, item: SerializableModel):
        item.binaries = [self.save_binary_data(binary) for binary in item.binaries]
        serialized = self.mapper.dump(item, strip_privates=True)
        self.table.put_item(Item=serialized)

    def save_binary_data(self, item: SerializableModel.Binary):
        s3_handle = self.s3.Object(CONFIG["s3"]["bucket"], f'{CONFIG["name"]}/entity/{item.id}.pickle')
        s3_handle.put(Body=item.data)
        return item.id

    def delete_model(self, _id):
        for binary in self.grid_fs.find(filter={'metadata.refId': _id}):
            self.grid_fs.delete(binary._id)
        self.table.delete_item(Key={'id': _id})


class CassandraFilesStorage:
    def __init__(self):
        pass


class StorageFactory:

    @staticmethod
    def get_storage(name):
        if name == 'MONGO':
            return MongoDBStorage()
        elif name == 'DYNAMO':
            return DynamoDBS3Storage()
        elif name == 'FILES':
            return FileStorage()
        else:
            raise KeyError(f'Unsupported storage {name}')

    @staticmethod
    def get_entity_storage(name):
        if name == 'MONGO':
            return CassandraMongoDBStorage()
        elif name == 'DYNAMO':
            return CassandraDynamoDBS3Storage()
        elif name == 'FILES':
            return CassandraFilesStorage()
        else:
            raise KeyError(f'Unsupported storage {name}')

    @staticmethod
    def get_entity_storage_from_config():
        return StorageFactory.get_entity_storage(CONFIG['storage'])

    @staticmethod
    def get_storage_from_config():
        return StorageFactory.get_storage(CONFIG['storage'])

