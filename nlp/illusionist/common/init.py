# -*- coding: utf-8 -*-

import datetime
import json
import logging
import logging.config
import sentry_sdk
import time
import os

import connexion
import werkzeug
from flask import g, request
from flask_cors import CORS
from sentry_sdk.integrations.flask import FlaskIntegration

from common.api.exceptions import handle_exception
from common.utils import DateTimeEncoder, init_swagger_host
from common.config import CONFIG
from common.concurrency import ExecutorFactory


def create_app(module_name):
    """
    Set up Flask app.
    :param testing: app for testing purposes
    :return: Flask app
    """

    cur_dir = os.path.dirname(os.path.abspath(__file__))
    spec_dir = os.path.join(cur_dir, f'../{module_name}/api')
    init_swagger_host(module_name, spec_dir)
    app = connexion.FlaskApp(__name__, specification_dir=spec_dir)
    app.add_error_handler(Exception, handle_exception)
    for error_code in werkzeug.exceptions.default_exceptions:
        app.add_error_handler(error_code, handle_exception)

    app.app.config['LOG_LEVEL'] = CONFIG.logger["level"]
    configure_logger(app.app)

    app.add_api('swagger.yaml')

    if CONFIG.get("testing"):
        return app

    # TODO: Deal with allowed origins.
    CORS(app.app, origins=CONFIG.cors["origins"],
         expose_headers=CONFIG.cors["expose_headers"],
         supports_credentials=CONFIG.cors["supports_credentials"])

    sentry_sdk.init(
        integrations=[FlaskIntegration()],
    )

    ExecutorFactory.create(app.app)

    return app


def configure_logger(app):
    """
    Set up logging.
    :param app: Flask app
    """

    @app.before_request
    def start_timer():
        g.start = time.time()

    @app.after_request
    def log_request(response):

        if request.path == '/favicon.ico' or request.path.endswith('/check') or request.path.startswith('/static'):
            return response

        if request.query_string:
            path_query = f"{request.path}?{request.query_string.decode('utf-8')}"
        else:
            path_query = request.path

        duration = round(time.time() - g.start, 3)

        log_params = {
            'method': request.method,
            'path': path_query,
            'status': response.status_code,
            'duration': duration,
            'ip': request.headers.get('X-Forwarded-For', request.remote_addr),
            'host': request.host
        }

        request_id = request.headers.get('X-Request-ID')
        if request_id:
            log_params['request_id'] = request_id

        if request.is_json and request.get_data():
            log_params['request_body'] = request.get_json().copy()
            if 'qa' in log_params['request_body']:
                log_params['request_body']['qa'] = 'HIDDEN'
            if 'data' in log_params['request_body']:
                log_params['request_body']['data'] = 'HIDDEN'

        if response.is_json and response.get_data():
            log_params['response_body'] = response.get_json()

        app.logger.info(json.dumps(log_params, cls=DateTimeEncoder))
        return response

    logging.config.dictConfig(CONFIG.logger)

    app.logger.setLevel(logging.getLevelName(CONFIG.logger["level"]))
    app.logger.debug("Logger started %s", datetime.datetime.utcnow())
