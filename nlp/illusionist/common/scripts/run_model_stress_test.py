# -*- coding: utf-8 -*-
""" Test script for application model memory """

import os
import yaml
import requests

from common.persistence import MongoDBStorage
from common.config import CONFIG


HOST = CONFIG["app"]["host"]
PORT = CONFIG["app"]["port"]

try:
    PROTOCOL = CONFIG["app"]["protocol"]
except KeyError:
    PROTOCOL = 'http://'

try:
    SERVICE_HOST = CONFIG["app"]['serviceHost']
except KeyError:
    SERVICE_HOST = str(HOST) + ':' + str(PORT)

URL = PROTOCOL + SERVICE_HOST
STUDIO_API_KEY = CONFIG["studioApiKey"]

DB = MongoDBStorage()
IDS = [model['_id'] for model in DB.load_persisted({}, {'_id': 1})]
for x in range(3):
    for _id in IDS:
        print('_id', _id, URL + '/models/' + _id + '?key=' + STUDIO_API_KEY)
        # IDS.append(_id)
        requests.get(URL + '/models/' + _id + '?key=' + STUDIO_API_KEY)
