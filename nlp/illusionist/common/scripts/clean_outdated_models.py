from common.config import CONFIG
from common.persistence import MongoDBStorage
from datetime import datetime, timedelta

if __name__ == '__main__':
    older_than = 30
    print(f'Connecting to mongo database: {CONFIG["mongodb"]["database"]}-{CONFIG.get("dsuffix", CONFIG["namespace"])}')
    storage = MongoDBStorage()
    outdated_docs = []
    total = 0
    for doc in storage.load_persisted():
        total += 1
        outdated = doc['lastModified'] < datetime.now() - timedelta(days=older_than)
        if outdated:
            outdated_docs.append({'_id': doc["_id"], 'lastModified': doc['lastModified'], 'auth_key': doc['auth_key']})
    print(f'Total number of documents: {total}')
    print(f'Number of outdated documents: {len(outdated_docs)}')

    if input('Do you want to delete all orphaned models? [yes/NO] ') == 'yes':
        print('Deleting...')
        for doc in outdated_docs:
            print(f'Deleting model {doc["_id"]}, last modified {doc["lastModified"]}')
            storage.delete_model_data(doc["_id"], doc['auth_key'])
        print('Done')
