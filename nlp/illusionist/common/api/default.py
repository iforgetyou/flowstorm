# -*- coding: utf-8 -*-
"""
    common.api.default
    ~~~~~~~~~~~~~~~~~~

    Default (un-categorized) API endpoints.
"""

from os import getpid, getenv

from flask import request
from psutil import cpu_percent, disk_usage, virtual_memory, Process

from common.config import CONFIG
from common.model_manager import BaseModelManager

MM = BaseModelManager()
process = Process(getpid())


def auth(key, required_scopes):
    """
    Auth required by connexion package.
    :param key: authentication key
    :param required_scopes: not used
    :return: authentication response
    """
    return {'sub': 'studio'}


def get_health():
    """
    Health check endpoint.
    :return: json response, status code
    """

    cpu = 1 - cpu_percent() / 100
    mem = 1 - virtual_memory().percent / 100
    disk = 1 - disk_usage('/').percent / 100
    return {
        'module name:': getenv('APP', 'app'),
        'health': min(cpu, mem, disk),
        'cpu': cpu,
        'memory': mem,
        'disk': disk,
        'application memory limit': MM.get_memory_limit(),
        'application memory used': process.memory_info().rss
    }, 200, {
        'X-Application-Ready': MM.is_ready
    }


def get_version():
    """
    Get Illusionist app package version.
    :return: json response, status code
    """
    return {'version': CONFIG["version"]}, 200


def get_models():
    """
    List params of all deployed models.
    :return: json response, status code
    """

    key = request.args.get('key')
    return MM.list_models(auth_key=key), 200
