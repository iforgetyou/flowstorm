import logging

from werkzeug.exceptions import HTTPException
from common.config import CONFIG
from common.utils import make_logger_name

logger = logging.getLogger(make_logger_name(__name__))


class ModelNotFoundError(HTTPException):
    code = 404

    def __init__(self, description: str):
        HTTPException.__init__(self, description)


class ModelPermissionError(ModelNotFoundError, PermissionError):

    def __init__(self, description: str):
        ModelNotFoundError.__init__(self, description)


class InvalidEmbeddingAlgorithm(HTTPException):
    code = 400

    def __init__(self, description: str):
        HTTPException.__init__(self, description)

class UnknownAlgorithm(HTTPException):
    code = 400

    def __init__(self, description: str):
        HTTPException.__init__(self, description)


class IncompatibleModels(HTTPException):
    code = 500

    def __init__(self, description: str):
        HTTPException.__init__(self, description)


def handle_exception(error: Exception):
    if isinstance(error, HTTPException):
        logger.error(f'{type(error).__name__}: {error.description}')
        name = ModelNotFoundError.__name__ if type(error) is ModelPermissionError else type(error).__name__
        response = f'{CONFIG["name"]}:{name}: {error.description}'
        return response, error.code
    else:
        logger.exception(error)
        response = f'{CONFIG["name"]}:{type(error).__name__}: {error}'
        return response, 500
