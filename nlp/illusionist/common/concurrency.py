from flask import Flask
from flask_executor import Executor


class ExecutorFactory:
    _executor = None

    @staticmethod
    def create(app: Flask):
        app.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = True
        ExecutorFactory._executor = Executor(app)
        app.config['EXECUTOR_TYPE'] = 'process'

    @staticmethod
    def get() -> Executor:
        if ExecutorFactory._executor is None:
            raise RuntimeError("Executor is not initialized")
        return ExecutorFactory._executor
