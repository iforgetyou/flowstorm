# -*- coding: utf-8 -*-
"""
    common.data_download
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    Tools for downloading models and other large files.

    @author: tomas.brich@seznam.cz
"""

import logging
import os

import requests

from common.utils import make_logger_name
from common.config import CONFIG

logger = logging.getLogger(make_logger_name(__name__))

DATA_DIR = os.path.join(os.path.dirname(__file__), os.pardir, './data')

def ensure_model_file(name, path=None):
    """
    Ensure that model file is present (try to download if not).
    :param name: name of the model file
    :param path: where to look for the file
    :return: path to the file
    """

    dirs = [DATA_DIR] if path is None else [path]
    for p in dirs:
        filename = os.path.join(p, name)
        if os.path.isfile(filename):
            logger.debug('Model found in %s.', filename)
            return os.path.realpath(filename)

    return download_model_file(name, path=DATA_DIR)


def download_model_file(name, path=DATA_DIR):
    """
    Download model file from the Google Cloud Storage server.
    :param name: model file name
    :param path: where to download the model (default project data folder)
    :return: path to the downloaded file
    """

    d_path = os.path.realpath(os.path.join(path, name))
    if not os.path.exists(path):
        os.makedirs(path)

    logger.debug('Trying to download %s from %s.', name, CONFIG["embDownloadUrl"])
    res = requests.get(CONFIG["embDownloadUrl"] + name)

    if res.status_code == 200:
        with open(d_path, 'wb') as f:
            f.write(res.content)
        logger.debug('Downloaded %s.', d_path)
        return d_path

    if res.status_code == 404:
        logger.debug('Model %s not found on the server.', name)
    else:
        logger.debug('Response status %d while trying to download %s.', res.status_code, name)
    return None
