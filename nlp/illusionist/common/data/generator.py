import math

import numpy as np
from tensorflow.python.keras.utils.data_utils import Sequence


class BatchGenerator(Sequence):
    def __init__(self, x_generators, y, batch_size):
        self.x_generators = x_generators
        self.initialized_generators = None
        self.y = y
        self.y_iter = None
        self.batch_size = batch_size
        self.on_epoch_end()

    def __getitem__(self, index):
        batches = [[] for _ in self.initialized_generators]
        y_batch = []
        for y in self.y_iter:
            y_batch.append(y)
            for batch, gen in zip(batches, self.initialized_generators):
                batch.append(next(gen))
            if len(y_batch) >= self.batch_size:
                return [np.asarray(b) for b in batches], np.asarray(y_batch)
        return [np.asarray(b) for b in batches], np.asarray(y_batch)

    def __len__(self):
        self.on_epoch_end()  # len is called once per epoch
        return int(math.ceil(len(self.y) / self.batch_size))

    def on_epoch_end(self):
        self.initialized_generators = [gen() for gen in self.x_generators]
        self.y_iter = iter(self.y)

