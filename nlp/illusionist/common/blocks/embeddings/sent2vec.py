# -*- coding: utf-8 -*-
"""
    common.embeddings.sent2vec
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Sent2Vec embedding algorithm wrappers.
"""


# default (English) Sent2Vec model
from common.blocks.embeddings.base import CompressedEmbeddingBase

DEFAULT_MODEL = 'twitter_50k.pickle'


class Sent2Vec(CompressedEmbeddingBase):
    """
    Compressed Sent2Vec embedding algorithm.
    :param path: where to look for model file
    :param model: model file (model.pickle)
    """

    def __init__(self, model_data):
        super().__init__('Sent2Vec', model_data)
