# -*- coding: utf-8 -*-
"""
    common.embeddings.__init__
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Class for generating model embedding algorithms.
"""
import pickle

from common.api.exceptions import InvalidEmbeddingAlgorithm
from common.blocks.embeddings.sent2vec import Sent2Vec
from common.blocks.embeddings.universal_sentence_encoder import UniversalSentenceEncoder
from common.singleton import Singleton

import common.blocks.embeddings.fasttext
from common.data_download import ensure_model_file


class EmbeddingFactory(metaclass=Singleton):
    """
    Get an embedding model instance.

    Supported embedding algorithms (<emb>):
    - FastText (without sub-word information)
    - FastTextSW (using sub-word information)
    - Sent2Vec (English only!)
    """

    def __init__(self):
        self._embeddings = {
            'FastText': {},
            'FastTextSW': {},
            'Sent2Vec': {},
            "UniversalSentenceEncoder": {}
        }

    def get_model(self, emb, lang='en'):
        """
        Get or generate embedding model instance for algorithm name and language.

        Supported languages (<lang>):
        - en (English - default)
        - cs (Czech)
        - es (Spanish)

        :param emb: embedding algorithm name
        :param lang: model language
        :return: embedding model instance
        """
        if emb not in self._embeddings:
            raise InvalidEmbeddingAlgorithm(f'Wrong algorithm {emb}')

        algorithm = self.model_creator(emb, lang)
        cls = algorithm['class']
        return cls(**algorithm['args'])

    def get_model_args(self, emb: str, lang='en'):
        if emb not in self._embeddings:
            raise InvalidEmbeddingAlgorithm(f'Wrong algorithm {emb}')

        return self.model_creator(emb, lang)['args']

    def model_creator(self, emb, lang):
        """
        Generate algorithm.

        :param emb: embedding algorithm
        :param lang: model language
        :return: algorithm instance
        """

        if lang not in self._embeddings[emb]:
            if emb == 'FastText':
                self._embeddings[emb][lang] = {
                    'class': common.blocks.embeddings.fasttext.FastText,
                    'args': {'model_data': self.load_model_data(f'wiki_{lang}_50k.pickle')}
                }
            elif emb == 'FastTextSW':
                self._embeddings[emb][lang] = {
                    'class': common.blocks.embeddings.fasttext.FastTextSW,
                    'args': {
                        'model_data': self.load_model_data(f'wiki_{lang}_50k.pickle'),
                        'sw_data': self.load_model_data(f'wiki_{lang}_sw_100k.pickle'),
                        "lang": lang
                    }
                }
            elif emb == 'Sent2Vec':
                self._embeddings[emb][lang] = {
                    'class': Sent2Vec,
                    'args': {'model_data': self.load_model_data(f'twitter_50k.pickle')}
                }
            elif emb == 'UniversalSentenceEncoder':
                if lang != "en":
                    raise InvalidEmbeddingAlgorithm(f'Use is not supported for language {lang}')
                self._embeddings[emb][lang] = {
                    'class': UniversalSentenceEncoder,
                    'args': {}
                }

        return self._embeddings[emb][lang]

    def load_model_data(self, model, path=None):
        model_path = ensure_model_file(model, path)
        with open(model_path, 'rb') as f:
            data = pickle.load(f)
        return data
