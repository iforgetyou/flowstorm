import numpy as np

from common.blocks.embeddings.base import EmbeddingBase


class CharEmbedding(EmbeddingBase):
    def __init__(self, max_sequence_length, max_char_length):
        self.char2idx = {"PADDING": 0, "UNKNOWN": 1}
        for c in "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZé.,-_()[]{" \
                 "}!?:;#'\"/\\%`&=*+@^~|<>ěščřžýáíéůúĚŠČŘŽÝÁÍÉŮÚ":
            self.char2idx[c] = len(self.char2idx)
        super(CharEmbedding, self).__init__('char', (len(self.char2idx, )))
        self.max_sequence_length = max_sequence_length
        self.max_char_length = max_char_length
        self.output_dim = (len(self.char2idx, ))

    def vector_preprocessed(self, sentence, average=True):
        """
        Transform a tokenized sequence [[w1, w2, ...], [...], ...] into
        array of shape (# examples, max_sequence_length, max_char_length)
        :param tokenized_sequences:
        :return: array of shape (# examples, max_sequence_length, max_char_length)
        """
        chars = []

        sentence_chars = []
        for i in range(self.max_sequence_length):
            word_chars = []
            for j in range(self.max_char_length):
                try:
                    word_chars.append(self.char2idx.get(sentence[i][j], self.char2idx.get("UNKNOWN")))
                except IndexError as e:
                    word_chars.append(self.char2idx.get("PADDING"))  # padding
            sentence_chars.append(np.array(word_chars))
        return np.array(sentence_chars)


class CaseEmbedding(EmbeddingBase):
    def __init__(self, max_sequence_length):
        self.case2idx = {'numeric': 0, 'allLower': 1, 'allUpper': 2, 'initialUpper': 3, 'other': 4, 'mainly_numeric': 5,
                         'contains_digit': 6, 'PADDING_TOKEN': 7}
        super(CaseEmbedding, self).__init__('case', (len(self.case2idx, )))
        self.max_sequence_length = max_sequence_length

        self.output_dim = (len(self.case2idx, ))

    def vector_preprocessed(self, sentence, average=True):
        sentence_case = []
        for i in range(self.max_sequence_length):
            try:
                word = sentence[i]
            except:
                sentence_case.append(self.case2idx["PADDING_TOKEN"])
                continue

            casing = 'other'

            num_digits = 0
            for char in word:
                if char.isdigit():
                    num_digits += 1

            digit_fraction = num_digits / float(len(word))

            if word.isdigit():  # Is a digit
                casing = 'numeric'
            elif digit_fraction > 0.5:
                casing = 'mainly_numeric'
            elif word.islower():  # All lower case
                casing = 'allLower'
            elif word.isupper():  # All upper case
                casing = 'allUpper'
            elif word[0].isupper():  # is a title, initial char upper, then all lower
                casing = 'initialUpper'
            elif num_digits > 0:
                casing = 'contains_digit'

            sentence_case.append(self.case2idx[casing])
        return np.array(sentence_case)
