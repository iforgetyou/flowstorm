# -*- coding: utf-8 -*-
import logging

import gevent
import numpy as np

import tritonclient.http as httpclient

from common.blocks.embeddings.base import EmbeddingBase, generate_batches
from common.utils import make_logger_name
from common.config import CONFIG

logger = logging.getLogger(make_logger_name(__name__))


class UniversalSentenceEncoder(EmbeddingBase):
    """
    """

    def __init__(self):
        super().__init__(alg='UniversalSentenceEncoder', dim=512)
        self.model_name = "use-large4"
        self.client = httpclient.InferenceServerClient(url=CONFIG["triton.url"].replace('https://', '').replace('http://', ''),
                                                       ssl=CONFIG["triton.url"].startswith('https://'),
                                                       ssl_context_factory=gevent.ssl.create_default_context,
                                                       verbose=False)

    def vector_batch(self, batch: list, normalize=True):
        result = []
        for batch_slice in generate_batches(batch, 512):
            sentence = np.array(batch_slice, dtype=object).reshape(-1)

            inputs, outputs = [], []

            inputs.append(httpclient.InferInput('inputs', [len(sentence)], "BYTES"))
            inputs[0].set_data_from_numpy(sentence, binary_data=True)

            outputs.append(httpclient.InferRequestedOutput('outputs', binary_data=True))
            if len(result):
                result = np.append(result, self.client.infer(model_name=self.model_name, inputs=inputs, outputs=outputs).as_numpy('outputs'), axis=0)
            else:
                result = self.client.infer(model_name=self.model_name, inputs=inputs, outputs=outputs).as_numpy('outputs')
        return result.reshape(-1, self.dim)

    def vector(self, sentence: str, normalize=True):
        return self.vector_batch([sentence])[0]

    def vector_preprocessed(self, sentence, average=True):
        return self.vector_batch([sentence])[0]

    def process(self, data):
        return self.vector_batch([' '.join(sentence) for sentence in data])


if __name__ == '__main__':
    model_name = "use-large4"
    sentences = ["I would like to know where is prague?", "whatt are you doing"]

    client = httpclient.InferenceServerClient(url="triton-preview.flowstorm.ai",
                                              ssl=True,
                                              ssl_context_factory=gevent.ssl.create_default_context,
                                              verbose=True)

    inputs = []
    outputs = []

    sentence = np.array(sentences, dtype=object).reshape(-1)

    inputs.append(httpclient.InferInput('inputs', [len(sentence)], "BYTES"))
    inputs[0].set_data_from_numpy(sentence, binary_data=True)

    outputs.append(httpclient.InferRequestedOutput('outputs', binary_data=True))

    results = client.infer(model_name=model_name,
                           inputs=inputs,
                           outputs=outputs)
    print(results.as_numpy('outputs').shape)

    use = UniversalSentenceEncoder()
    print(use.vector(sentences[0]).shape)
