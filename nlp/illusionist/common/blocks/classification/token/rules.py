import logging

import numpy as np
from sklearn.metrics import precision_recall_fscore_support, classification_report

from common.blocks.classification.base import SequenceAbstractTrainer
from common.utils import make_logger_name

logger = logging.getLogger(make_logger_name(__name__))


class StringMatchingTrainer(SequenceAbstractTrainer):
    def __init__(self, _id, language, vectorizer, config):
        super().__init__(_id=_id, language=language, config=config, vectorizer=vectorizer)
        self.gazetteer = {}

    def save_model(self, persistence):
        pass

    def _train(self, train_json, valid_json):
        self.gazetteer = self.extract_entities(train_json)
        return [], {}

    def _evaluate(self, test_json, debug):
        y_true, y_pred = [], []
        for sentence in test_json:
            p = np.full(len(sentence), 'O', dtype=object)
            y_true.extend(token['tag'] for token in sentence)
            for cls, examples in self.gazetteer.items():
                for entity in examples:
                    for i in range(len(sentence) - len(entity)):
                        if tuple(t['word'] for t in sentence[i:i+len(entity)]) == entity:
                            p[i] = 'B-' + cls
                            p[i+1:i+len(entity)] = 'I-' + cls
            y_pred.extend(p)

        precision, recall, f1, support = precision_recall_fscore_support(y_true, y_pred, labels=list(set(y_pred)))
        print(classification_report(y_true, y_pred, labels=list(set(y_pred))))
        return precision[0], recall[0], f1[0], support[0]

    def extract_entities(self, data):
        gazetteer = {}
        for sentence in data:
            for token in sentence:
                cls = token['tag'][2:] if token['tag'] != 'O' else ''
                if token['tag'].startswith('B-'):
                    if cls not in gazetteer:
                        gazetteer[cls] = []
                    gazetteer[cls].append([token['word']])
                elif token['tag'].startswith('I'):
                    gazetteer[cls][-1].append(token['word'])
        for cls, entities in gazetteer.items():
            gazetteer[cls] = set([tuple(e) for e in entities])
        return gazetteer


if __name__ == '__main__':
    import json
    with open('train_data/conll2003.json') as f:
        data = json.load(f)
    trainer = StringMatchingTrainer('text', 'en', None, {'mlflow': {'url': None, 'projectName': 'test'}})
    trainer.train(data['train']['data'], data['valid']['data'], data['test']['data'])
