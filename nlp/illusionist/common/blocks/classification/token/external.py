import logging

import spacy

from common.blocks.classification.base import SequenceAbstractTrainer
from common.pipeline import Serializable, Pipeline, Stage
from common.twitter_tokenizer import TweetTokenizer
from common.utils import make_logger_name
from common.utils.nlp import tokenize

logger = logging.getLogger(make_logger_name(__name__))


class Spacy(SequenceAbstractTrainer, Serializable):

    def __init__(self, _id, language, config):
        super().__init__(_id=_id, language=language, config=config)
        self._tokenizer = TweetTokenizer()
        self._tokenize = lambda s: tokenize(self._tokenizer, s).split()

    def _train(self, train_json, valid_json):
        model_name = "en_core_web_sm"
        if not spacy.util.is_package(model_name):
            logger.info(f'Downloading model {model_name}')
            spacy.cli.download(model_name)

        self._model = spacy.load(model_name, disable=["tagger", "parser"])
        return [], {}

    def _evaluate(self, test_json, debug):
        pass

    def process(self, data):
        return self.predict(data)

    def predict(self, data, **kwargs):
        if 'tokens' in data:
            doc = spacy.tokens.Doc(self._model.vocab, [token["text"] for token in data['tokens']])
        else:
            doc = spacy.tokens.Doc(self._model.vocab, self._tokenize(data['transcript']['text']))
        for name, proc in self._model.pipeline:
            doc = proc(doc)

        result = []
        for token in doc:
            token_obj = {'text': token.text, 'type': 'Word'}
            cls = token.ent_iob_ + ('-' + token.ent_type_ if token.ent_iob_ != 'O' else '')
            if cls != 'O':
                token_obj['classes'] = [{'model_id': kwargs.get('model_id'), 'name': cls,
                                         'score': '0.5', 'type': 'Entity'}]
            result.append(token_obj)
        return {'tokens': result}

    def serialize(self) -> bytes:
        return self._model.to_bytes()

    def deserialize(self, data: bytes):
        self._model = spacy.blank(self.language)
        ner = self._model.create_pipe('ner')
        self._model.add_pipe(ner)
        self._model.from_bytes(data)


if __name__ == '__main__':
    from training.model_manager import EntityModelManager
    pipeline = Pipeline('spacy').add_stage(Stage('spacy').add_component(Spacy('core', 'en', {})))
    pipeline.train({})
    mm = EntityModelManager()
    mm.save_pipeline('core_web_sm_en', 'spacy', 'en', pipeline)
