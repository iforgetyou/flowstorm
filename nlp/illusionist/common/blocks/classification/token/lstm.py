# -*- coding: utf-8 -*-

import pickle

import numpy as np
import logging

from tensorflow.keras.models import Model
from tensorflow.keras.layers import TimeDistributed, Conv1D, Dense, Embedding, Input, Dropout, LSTM, Bidirectional, \
    MaxPooling1D, Flatten, concatenate
from tensorflow.keras.initializers import RandomUniform
from sklearn.metrics import precision_recall_fscore_support, classification_report

from common.blocks.classification.base import SequenceAbstractTrainer
from common.blocks.classification.utils import F1Metrics, masked_acc
from common.blocks.embeddings.base import EmbeddingBase
from common.blocks.embeddings.char import CharEmbedding, CaseEmbedding
from common.blocks.preprocessing import Labels
from common.pipeline.component import Serializable
from common.utils import make_logger_name
from common.data.generator import BatchGenerator

logger = logging.getLogger(make_logger_name(__name__))


class LstmTrainer(SequenceAbstractTrainer, Serializable):

    def __init__(self, _id, language, config):
        super(LstmTrainer, self).__init__(_id=_id, language=language, config=config)
        self.max_sequence_length = self.get_config_value("maxSequenceLength")
        self.max_char_length = self.get_config_value("maxCharLength")

        self.dropout_rate = self.get_config_value("droupout")
        self.recurrent_dropout_rate = self.get_config_value("recurrentDroupout")
        self.lstm_size = self.get_config_value("lstmSize")
        self.optimizer = self.get_config_value("optimizer")

    @property
    def _word_emb(self) -> EmbeddingBase:
        return self._input_components[0]

    @property
    def _char_emb(self) -> CharEmbedding:
        return self._input_components[1]

    @property
    def _case_emb(self) -> CaseEmbedding:
        return self._input_components[2]

    @property
    def _labels(self) -> Labels:
        return self._input_components[3]

    def _build_model(self):
        """Model layers"""
        # Loading parameters
        max_sequence_length = self.get_config_value("maxSequenceLength")
        max_char_length = self.get_config_value("maxCharLength")
        embedding_dim = self._word_emb.output_dim
        dropout_rate = self.get_config_value("dropout")
        recurrent_dropout_rate = self.get_config_value("recurrentDropout")
        lstm_size = self.get_config_value("lstmSize")
        optimizer = self.get_config_value("optimizer")
        n_labels = self._labels.output_dim
        n_chars = self._char_emb.output_dim

        # Define model --------------------------------------------
        # character input
        character_input = Input(shape=(max_sequence_length, max_char_length), name="Character_input")
        embed_char_out = TimeDistributed(
            Embedding(n_chars, 30, embeddings_initializer=RandomUniform(minval=-0.5, maxval=0.5)),
            name="Character_embedding")(
            character_input)

        dropout = Dropout(dropout_rate)(embed_char_out)

        # CNN over characters
        conv1d_out = TimeDistributed(
            Conv1D(kernel_size=3, filters=30, padding='same', activation='tanh', strides=1),
            name="Convolution")(dropout)
        maxpool_out = TimeDistributed(MaxPooling1D(max_char_length), name="Maxpool")(conv1d_out)
        char = TimeDistributed(Flatten(), name="Flatten")(maxpool_out)
        char = Dropout(dropout_rate)(char)

        # word-level input
        words_input = Input(shape=(max_sequence_length, embedding_dim), dtype='float32', name="Word_embedding")

        # # case-info input
        casing_input = Input(shape=(None,), dtype='int32', name='casing_input')
        casing = Embedding(output_dim=self._case_emb.output_dim, input_dim=self._case_emb.output_dim,
                           weights=[np.identity(self._case_emb.output_dim, dtype='float32')],
                           trainable=False)(casing_input)

        # concat & BiLSTM
        output = concatenate([words_input, casing, char])
        output = Bidirectional(LSTM(lstm_size,
                                    return_sequences=True,
                                    dropout=dropout_rate,  # on input to each LSTM block
                                    recurrent_dropout=recurrent_dropout_rate  # on recurrent input signal
                                    ), name="BiLSTM")(output)
        output = TimeDistributed(Dense(n_labels, activation='softmax'), name="Softmax_layer")(output)

        # set up model
        model = Model(inputs=[words_input, character_input, casing_input], outputs=[output])

        model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer, metrics=["acc", masked_acc])

        model.summary()

        return model

    def process(self, data):
        self.predict(data)

    def predict(self, data, **kwargs):
        return self._model.predict([np.asarray(list(gen())) for gen in data])

    def _train(self, data, **kwargs):

        self._model = self._build_model()

        train_data = [feature[0] for feature in data]
        valid_data = [feature[1] for feature in data if len(feature) > 1]
        data_generator = BatchGenerator(train_data[:-1], train_data[-1], self.get_config_value("batchSize"))

        # if valid json is not set then the validation_split is used
        if not valid_data:
            history = self._model.fit(data_generator,
                                      epochs=self.get_config_value("epochs"),
                                      shuffle=True,
                                      verbose=1)
        else:
            x_valid_word, x_valid_char, x_valid_case, y_valid = valid_data
            valid_data_generator = BatchGenerator(valid_data[:-1], valid_data[-1], len(valid_data[-1]))

            f1score = F1Metrics(self._labels.idx2label, pad_value=0,
                                validation_data=valid_data_generator)

            history = self._model.fit(data_generator,
                                      validation_data=valid_data_generator,
                                      epochs=self.get_config_value("epochs"),
                                      verbose=1,
                                      shuffle=True,
                                      callbacks=[f1score] + kwargs.get("callbacks", []))

        return history

    def _evaluate(self, test_json, debug=False):
        x_test_word, x_test_char, x_test_case, y_test = test_json

        y_pred = self._model.predict_on_batch([x_test_word, x_test_char, x_test_case])

        y_true = []
        # reduce dimension.
        y_pred_idx = np.argmax(y_pred, -1)
        y_pred_processed = []

        for si, sentence in enumerate(y_test):
            for ti, tag in enumerate(sentence):
                if tag != self._labels.label2idx["PAD"]:
                    y_true.append(tag)
                    y_pred_processed.append(y_pred_idx[si][ti])

        y_true = [self._labels.idx2label[l] for l in y_true]
        y_pred = [self._labels.idx2label[l] for l in y_pred_processed]

        # To use them for conlleval.py
        if debug:
            return y_true, y_pred

        precision, recall, f1, support = precision_recall_fscore_support(y_true, y_pred, labels=list(set(y_pred)))
        logger.debug(classification_report(y_true, y_pred, labels=list(set(y_pred))))
        return precision[0], recall[0], f1[0], support[0]

    def serialize(self) -> bytes:
        return pickle.dumps({'weights': self._model.get_weights(), 'config': self._model.get_config()})

    def deserialize(self, data: bytes):
        weights, config = map(pickle.loads(data).get, ['weights', 'config'])
        self._model = Model.from_config(config)
        self._model.set_weights(weights)
