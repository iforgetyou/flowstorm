# -*- coding: utf-8 -*-
"""
    common.qa
    ~~~~~~~~~

    Question-answering algorithms.
"""

import logging
import json

import numpy as np

from common.blocks.classification.base import ClassificationModelBase
from common.utils import make_logger_name, get_out_of_scope_label

logger = logging.getLogger(make_logger_name(__name__))

# Not used anymore

class CosineSimilarity(ClassificationModelBase):
    """
    Question-answering by semantic query-matching.

    Data:
    {
      "model": {
        "name": "<human-friendly model name>",
        "algorithm": "Sent2Vec",
        "lang": "en",
        "use_tfidf": False,
        "model": "cosine/logistic"
      },
      "qa": {
        "id1": {
          "questions": ["first question", "second question"],
          "answer": "answer to the questions"
        },
        "id2": {...}
      }
    }

    :param data: QA data (see above)
    """

    def after_initialization(self, data):
        # filtering out of domain examples
        if get_out_of_scope_label() in self.answers:
            idx_ood = self.answers.index(get_out_of_scope_label())
            idx_to_preserve = [idx for idx, label in enumerate(self.y) if label != idx_ood]
            self.X = self.X[idx_to_preserve]
            self.y = [self.y[idx] for idx in idx_to_preserve]
            self.questions = [self.questions[idx] for idx in idx_to_preserve]

            if get_out_of_scope_label() in self.answers: self.answers.remove(get_out_of_scope_label())
            if get_out_of_scope_label() in self.data.get("qa", {}): del self.data["qa"][get_out_of_scope_label()]

    def get_runtime_data(self):
        return {'X': self.X, 'y': self.y, 'questions': self.questions, 'answers': self.answers,
                'oov_vectors': self.emb._oov_vectors, 'thresholds': self.thresholds,
                'tfidf_idf': list(self.TFIDF.idf_) if self.TFIDF else None,
                'tfidf_vocabulary': json.dumps(self.TFIDF.vocabulary_ if self.TFIDF else {})}

    def load_runtime_data(self, metadata, runtime):
        tfidf_vocabulary = json.loads(runtime['tfidf_vocabulary']) if metadata['use_tfidf'] else None
        tfidf_idf = np.array(runtime['tfidf_idf']) if metadata['use_tfidf'] else None

        self.init_embedding_model(metadata, tfidf_idf=tfidf_idf, tfidf_vocabulary=tfidf_vocabulary)

        self.X = runtime['X']
        self.y = runtime['y']
        self.questions = runtime['questions']
        self.answers = runtime['answers']
        self.emb._oov_vectors = runtime.get('oov_vectors', {})
        self.thresholds = runtime.get('thresholds', [0.0] * len(self.y) if self.y else None)

    def filter(self, X, y, questions, filtered_answers, allow=False):
        filtered_answers = np.asarray(filtered_answers)
        all_answers = np.asarray(self.answers)

        indices = np.argwhere(np.isin(all_answers, filtered_answers) == allow)

        mask = np.isin(y, indices.flatten())
        return X[mask], y[mask], questions[mask]

    def get(self, vec, denied_answers=None, allowed_answers=None, n=1, use_threshold=True):
        """
        Get answer for a query.
        :param query: input query
        :param denied_answers: answers excluded form the result
        :param allowed_answers: only listed answers may be returned
        :param n: get top <n> results
        :param use_threshold: only results with similarity higher than threshold are returned, default True
        :return: [{answer: string, confidence: float, hit: string}]
        """

        if self.X is None:
            logger.debug('QA model not yet initialized!')
            return []

        if type(vec) == str: # mainly because of tests
            vec = self.emb.vector(vec) if self.TFIDF is None else self.tfidf_vector(vec)

        X, y, questions = self.X, np.asarray(self.y), np.asarray(self.questions)
        if denied_answers:
            X, y, questions = self.filter(X, y, questions, denied_answers, False)
        if allowed_answers:
            X, y, questions = self.filter(X, y, questions, allowed_answers, True)

        sim = np.dot(X, vec)

        ids = reversed(np.argsort(sim))
        output = []
        best_classes = set()
        for idx in ids:
            confidence = np.exp(sim[idx] - 1)
            if (not use_threshold or confidence > self.thresholds[y[idx]]) and y[idx] not in best_classes:
                output.append({
                    'answer': self.answers[y[idx]],
                    'similarity': confidence,
                    'hit': questions[idx]
                })
                best_classes.add(y[idx])
            if 0 < n <= len(output):
                break

        return output
