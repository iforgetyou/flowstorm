# -*- coding: utf-8 -*-
"""
    common.qa
    ~~~~~~~~~

    Question-answering algorithms.
"""

import logging
import pickle
import json

import numpy as np
from sklearn.linear_model import LogisticRegression as LogisticRegressionModel
from scipy.special import expit, softmax

from common.blocks.classification.base import ClassificationModelBase
from common.blocks.embeddings import EmbeddingFactory
from common.utils import make_logger_name, get_out_of_scope_label

logger = logging.getLogger(make_logger_name(__name__))
EM = EmbeddingFactory()

# To have reproducible results
from numpy.random import seed
from scipy.stats import norm as dist_model

seed(42)


class LogisticRegression(ClassificationModelBase):
    """
    Question-answering by semantic query-matching.

    Data:
    {
      "model": {
        "name": "<human-friendly model name>",
        "algorithm": "Sent2Vec",
        "lang": "en",
        "use_tfidf": False,
        "model": "cosine/logistic"
      },
      "qa": {
        "id1": {
          "questions": ["first question", "second question"],
          "answer": "answer to the questions"
        },
        "id2": {...}
      }
    }

    :param data: QA data (see above)
    """

    def __init__(self, metadata=None, qa_data=None, runtime=None):
        self.idx2label = {}
        self.label2idx = {}
        self.model = None
        self.gaussians = {}
        super().__init__(metadata, qa_data, runtime)

    # fit a gaussian model
    @staticmethod
    def fit_gaussian(prob_pos_X):
        prob_pos = [p for p in prob_pos_X] + [2 - p for p in prob_pos_X]
        pos_mu, pos_std = dist_model.fit(prob_pos)
        return pos_mu, pos_std

    def after_initialization(self, data):
        self.filter_duplicates(get_out_of_scope_label())

        self.idx2label = {}
        for idx in np.unique(self.y):
            self.idx2label[idx] = self.answers[idx]
        self.label2idx = {v: k for k, v in self.idx2label.items()}

        if len(self.idx2label) == 1:
            self.model = LogisticRegression.DummyModel(list(self.idx2label.keys())[0])
            return
        elif len(self.idx2label) == 0:
            self.model = LogisticRegression.DummyModel(get_out_of_scope_label())
            return


        self.y = np.array(self.y).reshape(-1)

        additional_feature = []
        for idx, (vector, text) in enumerate(zip(self.X, self.questions)):
            additional_feature.append(self.adding_features(text))

        self.model = LogisticRegressionModel(max_iter=3000, C=1000,
                                             class_weight="balanced",
                                             multi_class="ovr", n_jobs=1)

        self.model.fit(np.append(self.X, additional_feature, axis=1), self.y)


        # workaround in a case of two classes
        if len(self.idx2label) == 2:
            X = [x for x, y in zip(np.append(self.X, additional_feature, axis=1), self.y) if y == 0]
            prediction = 1 - expit(np.dot(X, self.model.coef_[0]) + self.model.intercept_[0])

            pos_mu, pos_std = LogisticRegression.fit_gaussian(prediction)
            self.gaussians[0] = {"pos_mu": pos_mu, "pos_std": pos_std}

            X = [x for x, y in zip(np.append(self.X, additional_feature, axis=1), self.y) if y == 1]
            prediction = expit(np.dot(X, self.model.coef_[0]) + self.model.intercept_[0])

            pos_mu, pos_std = LogisticRegression.fit_gaussian(prediction)
            self.gaussians[1] = {"pos_mu": pos_mu, "pos_std": pos_std}
        else:
            for idx in self.idx2label.keys():
                X = [x for x, y in zip(np.append(self.X, additional_feature, axis=1), self.y) if y == idx]

                prediction = expit(np.dot(X, self.model.coef_[idx]) + self.model.intercept_[idx])

                pos_mu, pos_std = LogisticRegression.fit_gaussian(prediction)
                self.gaussians[idx] = {"pos_mu": pos_mu, "pos_std": pos_std}

    def get_runtime_data(self):
        return {'model': pickle.dumps(self.model), "idx2label": self.idx2label, 'questions': self.questions,
                'gaussians': pickle.dumps(self.gaussians),
                'y': self.y,
                'oov_vectors': self.emb._oov_vectors, 'thresholds': self.thresholds,
                'tfidf_idf': list(self.TFIDF.idf_) if self.TFIDF else None,
                'tfidf_vocabulary': json.dumps(self.TFIDF.vocabulary_ if self.TFIDF else {})                }

    def load_runtime_data(self, metadata, runtime):
        tfidf_vocabulary = json.loads(runtime['tfidf_vocabulary']) if metadata['use_tfidf'] else None
        tfidf_idf = np.array(runtime['tfidf_idf']) if metadata['use_tfidf'] else None

        self.init_embedding_model(metadata, tfidf_idf=tfidf_idf, tfidf_vocabulary=tfidf_vocabulary)

        self.idx2label = runtime['idx2label']
        self.y = runtime["y"]
        self.questions = runtime["questions"]
        self.model = pickle.loads(runtime['model'])
        self.gaussians = pickle.loads(runtime['gaussians'])

        self.emb._oov_vectors = runtime.get('oov_vectors', {})
        self.thresholds = runtime.get('thresholds', [0.0] * len(self.idx2label) if self.idx2label else None)

    def adding_features(self, text: str):
        return [self.is_negation_in_sentence(text)]

    def get(self, query, denied_answers=None, allowed_answers=None, n=1, use_threshold=True):
        """
        Get answer for a query.
        :param query: input query
        :param denied_answers: answers excluded form the result
        :param allowed_answers: only listed answers may be returned
        :param n: get top <n> results
        :param use_threshold: only results with similarity higher than threshold are returned, default True
        :return: [{answer: string, confidence: float, hit: string}]
        """
        if self.model is None:
            logger.debug('QA model not yet initialized!')
            return []

        output = []
        best_classes = set()

        if type(query) == str: # mainly because of tests
            query = query.lower()

            for idx, question in enumerate(self.questions):
                if query == question:
                    output.append({
                        'answer': self.idx2label[self.y[idx]],
                        'confidence': 1.0,
                        'hit': question
                    })
                    best_classes.add(self.idx2label[self.y[idx]])
                    break

            vec = self.emb.vector(query) if self.TFIDF is None else self.tfidf_vector(query)
        vec = np.append(vec, self.adding_features(query), axis=0).reshape(1, -1)

        # access to sigmoids
        # workaround in a case of two classes
        if len(self.idx2label) <= 1:
            output.append({
                'answer': self.idx2label[self.model.predict_proba(None)],
                'confidence': 1.0,
                'hit': ""
            })
            return output
        elif len(self.idx2label) == 2:
            prediction = []
            for idx in self.idx2label.keys():
                prediction.append((1 - idx) - expit(np.dot(vec, self.model.coef_.T) + self.model.intercept_))
        else:
            prediction = expit(np.dot(vec, self.model.coef_.T) + self.model.intercept_)
        prediction = np.array(prediction).reshape(-1)
        # access to standard deviation of predictions
        mu_stds = [gaussian["pos_std"] for gaussian in self.gaussians.values()]

        scale = 3

        ids = list(reversed(np.argsort(prediction)))
        threshold = min(0.9, 1 - scale * mu_stds[ids[0]])
        np.set_printoptions(suppress=True)
        # for i in ids:
        #     print(f"hit:{self.idx2label[i]} with confidence {prediction[i]}")
        # print()
        if prediction[ids[0]] < threshold:  # not confident about the prediction
            output.append({
                'answer': get_out_of_scope_label(),
                'confidence': threshold,
                'hit': ""
            })
            best_classes.add(get_out_of_scope_label())

        for idx in ids:
            confidence = prediction[idx]
            if (not use_threshold or confidence > self.thresholds[idx]) and self.idx2label[idx] not in best_classes:
                output.append({
                    'answer': self.idx2label[idx],
                    'confidence': confidence,
                    'hit': ""
                })
                best_classes.add(self.idx2label[idx])
            if 0 < n <= len(output):
                break

        return output

    class DummyModel:

        def __init__(self, intent):
            self.intent = intent

        def predict_proba(self, vec):
            return self.intent

    def is_negation_in_sentence(self, sentence: str):
        if any([1 for x in self.negation if x.lower() in sentence.lower()]):
            return 1
        return 0

    negation = [
        "no",
        "not",
        "none",
        "nobody",
        "nothing",
        "neither",
        "nowhere",
        "never",
        "negative",
        "hardly",
        "scarcely",
        "barely"
    ]
