from abc import ABC, abstractmethod


class ClassificationModel(ABC):

    @abstractmethod
    def get_binary(self):
        pass

    @abstractmethod
    def load_binary(self, binary_file):
        pass
