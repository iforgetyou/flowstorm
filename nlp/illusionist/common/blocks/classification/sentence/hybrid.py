# -*- coding: utf-8 -*-
"""
    common.qa
    ~~~~~~~~~

    Question-answering algorithms.
"""
import json
import logging

import numpy as np

from common.blocks.classification.base import ClassificationModelBase
from common.blocks.embeddings import EmbeddingFactory
from common.utils import make_logger_name, get_out_of_scope_label

logger = logging.getLogger(make_logger_name(__name__))
EM = EmbeddingFactory()

from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression as LogisticRegressionModel
from numpy.random import seed
import pickle

# To have reproducible results
seed(42)


class Hybrid(ClassificationModelBase):
    """
    Question-answering by semantic query-matching.

    Data:
    {
      "model": {
        "name": "<human-friendly model name>",
        "algorithm": "Sent2Vec",
        "lang": "en",
        "use_tfidf": False,
        "model": "cosine/logistic"
      },
      "qa": {
        "id1": {
          "questions": ["first question", "second question"],
          "answer": "answer to the questions"
        },
        "id2": {...}
      }
    }

    :param data: QA data (see above)
    """

    def __init__(self, metadata=None, qa_data=None, runtime=None):
        self.model = None
        self.ood_thresholds = None
        self.use_implicit_threshold = 0.50
        self.fasttext_implicit_threshold = 0.60
        super().__init__(metadata, qa_data, runtime)

    def after_initialization(self, data):
        # training of model
        self.y = np.array(self.y).reshape(-1)

        # remove when OOD class will be valid in Admin
        if get_out_of_scope_label() in self.answers:
            self.questions = np.array(self.questions)[self.y != self.answers.index(get_out_of_scope_label())].tolist()
            self.X = self.X[self.y != self.answers.index(get_out_of_scope_label())]
            self.y = self.y[self.y != self.answers.index(get_out_of_scope_label())]
            self.answers.remove(get_out_of_scope_label())

        if len(self.answers) == 1:
            self.model = Hybrid.DummyModel()
            return

        logistic_regression = LogisticRegressionModel(max_iter=850,
                                                      C=0.1,
                                                      penalty='l2',
                                                      random_state=42,
                                                      solver="liblinear",
                                                      class_weight="balanced",
                                                      dual=True,
                                                      fit_intercept=True,
                                                      multi_class="auto")

        try:
            param_grid = {'C': [0.01, 1, 100]}
            clf = GridSearchCV(
                estimator=logistic_regression,
                param_grid=param_grid,
                cv=3,
                refit=True,
                # verbose=3
                verbose=0)

            clf.fit(self.X, self.y)
            logger.info(clf.best_params_)
            self.model = clf.best_estimator_

        # not enough examples to do GridSearchCV with CV=3
        except ValueError:
            logistic_regression.fit(self.X, self.y)
            self.model = logistic_regression

        if self.data.get('model').get("embedding", "UniversalSentenceEncoder") == "UniversalSentenceEncoder":
            self.ood_thresholds = [np.exp(self.use_implicit_threshold - 1)] * len(self.answers)
        else:
            self.ood_thresholds = [np.exp(self.fasttext_implicit_threshold - 1)] * len(self.answers)

    def get_runtime_data(self):
        # dummy placeholder - not used
        self.ood_thresholds = [self.use_implicit_threshold] * len(self.answers)

        return {'model': pickle.dumps(self.model),
                'tfidf_idf': list(self.TFIDF.idf_) if self.TFIDF else None,
                'tfidf_vocabulary': json.dumps(self.TFIDF.vocabulary_ if self.TFIDF else {}),
                'X': self.X,
                'y': self.y,
                'questions': self.questions,
                'answers': self.answers,
                'oov_vectors': self.emb._oov_vectors,
                'ood_thresholds': self.ood_thresholds,
                'thresholds': self.thresholds}

    def load_runtime_data(self, metadata, runtime):
        tfidf_vocabulary = json.loads(runtime['tfidf_vocabulary']) if metadata['use_tfidf'] else None
        tfidf_idf = np.array(runtime['tfidf_idf']) if metadata['use_tfidf'] else None

        self.init_embedding_model(metadata, tfidf_idf=tfidf_idf, tfidf_vocabulary=tfidf_vocabulary)
        self.model = pickle.loads(runtime['model'])

        self.y = runtime['y']
        self.X = runtime['X']
        self.questions = runtime['questions']
        self.answers = runtime['answers']
        self.emb._oov_vectors = runtime.get('oov_vectors', {})
        self.thresholds = runtime.get('thresholds', [0.0] * len(self.answers) if self.answers is not None else None)
        if metadata.get("embedding", "UniversalSentenceEncoder") == "UniversalSentenceEncoder":
            self.ood_thresholds = [self.use_implicit_threshold] * len(self.answers)
        else:
            self.ood_thresholds = [self.fasttext_implicit_threshold] * len(self.answers)

    def get(self, vec, denied_answers=None, allowed_answers=None, n=1, use_threshold=True):
        """
        Get answer for a query.
        :param vec: input query
        :param denied_answers: answers excluded form the result
        :param allowed_answers: only listed answers may be returned
        :param n: get top <n> results
        :param use_threshold: only results with similarity higher than threshold are returned, default True
        :return: [{answer: string, confidence: float, hit: string}]
        """

        if self.X is None:
            logger.debug('QA model not yet initialized!')
            return []

        output = []

        # mainly because of tests
        if type(vec) == str:
            vec = self.emb.vector(vec) if self.TFIDF is None else self.tfidf_vector(vec)

        sim = np.dot(self.X, vec)
        sim = np.exp(sim - 1)

        if isinstance(self.model, Hybrid.DummyModel):
            model_prediction = [1.0]
        else:
            model_prediction = self.model.predict_proba(vec.reshape(1, -1)).reshape(-1)

        sorted_idx_model_prediction = reversed(np.argsort(model_prediction))

        similarity_per_level_raw = np.max(sim)
        similarity_per_level = (similarity_per_level_raw - np.min(self.ood_thresholds)) / (1 - np.min(self.ood_thresholds))

        for idx_model_prediction in sorted_idx_model_prediction:
            classification_confidence = model_prediction[idx_model_prediction]
            max_similarity_for_intent = float(np.max(sim[self.y == idx_model_prediction]))
            studio_threshold = float(self.thresholds[idx_model_prediction])
            implicit_threshold = float(self.ood_thresholds[idx_model_prediction])

            threshold_raw = implicit_threshold + (1 - implicit_threshold) * studio_threshold
            similarity_per_intent_raw = max_similarity_for_intent
            similarity_per_intent = (similarity_per_intent_raw - implicit_threshold) / (1 - implicit_threshold)

            output.append({
                'answer': self.answers[idx_model_prediction],
                'hit': f"COSINE SIMILARITY: {max_similarity_for_intent}",

                'similarity_per_level': similarity_per_level,  # studio threshold
                'similarity_per_level_raw': similarity_per_level_raw,  # cosine
                'similarity_per_intent': similarity_per_intent,  # studio threshold
                'similarity_per_intent_raw': similarity_per_intent_raw,  # cosine
                'classification_confidence': classification_confidence,

                'threshold': self.thresholds[idx_model_prediction],  # studio threshold
                'threshold_raw': threshold_raw,  # cosine
            })

            if 0 < n <= len(output):
                break

        return output

    class DummyModel:
        def predict_proba(self, vec=None):
            return np.array([1.0])

        def predict(self, vec=None):
            return np.array([1.0])
