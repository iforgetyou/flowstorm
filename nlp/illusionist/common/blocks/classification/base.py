# -*- coding: utf-8 -*-
"""
    common.qa
    ~~~~~~~~~

    Question-answering algorithms.
"""
import json
import logging
from typing import Tuple

import numpy as np
from abc import ABC, abstractmethod
from sklearn.feature_extraction.text import TfidfVectorizer

from common.blocks.embeddings import EmbeddingFactory
from common.pipeline import Component
from common.utils import make_logger_name, get_out_of_scope_label
from common.utils.nlp import tokenize
from common.twitter_tokenizer import TweetTokenizer

logger = logging.getLogger(make_logger_name(__name__))
EM = EmbeddingFactory()


class ClassificationModelBase(ABC):
    """
    Question-answering by semantic query-matching.

    Data:
    {
      "model": {
        "name": "<human-friendly model name>",
        "algorithm": "Sent2Vec",
        "lang": "en",
        "use_tfidf": False
        "model": "cosine/logistic"
      },
      "qa": {
        "id1": {
          "questions": ["first question", "second question"],
          "answer": "answer to the questions"
        },
        "id2": {...}
      }
    }

    :param data: QA data (see above)
    """

    def __init__(self, metadata=None, qa_data=None, runtime=None):
        self.X = None
        self.y = None
        self.questions = None
        self.answers = None
        self.thresholds = None

        self.emb = None
        self.tokenizer = None
        self.TFIDF = None

        self.data = {'model': metadata, 'qa': qa_data}
        self.data['model']['initialized'] = False
        if runtime:
            self.load_runtime_data(metadata, runtime)
            self.data['model']['initialized'] = True
        elif qa_data:
            logger.info('No runtime data. Initializing model...')
            self.initialize(qa_data)
        else:
            self.init_embedding_model(self.data.get('model'))

    def after_initialization(self, data):
        pass

    def filter_duplicates(self, ood_label):
        # check for duplicated in context/local/global intents - remove with this priority in mind
        idx_to_delete = []

        if ood_label in self.answers:
            idx_ood = self.answers.index(ood_label)
        else:
            idx_ood = ood_label

        ood_sentences = [(idx, sentence) for idx, sentence in enumerate(self.questions) if self.y[idx] == idx_ood]
        in_sentences = set([sentence for idx, sentence in enumerate(self.questions) if self.y[idx] != idx_ood])

        for idx, sentence in ood_sentences:
            if sentence in in_sentences:
                idx_to_delete.append(idx)

        self.questions = np.delete(self.questions, idx_to_delete, axis=0)
        self.y = np.delete(self.y, idx_to_delete, axis=0)
        self.X = np.delete(self.X, idx_to_delete, axis=0)

    def initialize(self, data):
        """
        Initialize embedding model and prepare data.
        :param data: QA data (see __init__)
        """

        self.init_embedding_model(data.get('model'))
        self.data = data

        if not data['qa']:
            self.X, self.y, self.questions, self.answers = None, None, None, None
            return

        questions, answers, y, thresholds = [], [], [], []
        for idx, content in enumerate(data['qa'].values()):
            if not content['questions']:
                content['questions'].append(get_out_of_scope_label())
            q = [x.lower() for x in content['questions']]
            questions += q
            y += [idx] * len(q)
            answers.append(content['answer'])
            thresholds.append(content.get('threshold', 0.0))

        if self.TFIDF is None:
            self.X = self.emb.vector_batch(questions)
        else:
            self.TFIDF.fit(questions)
            self.X = np.asarray([self.tfidf_vector(q) for q in questions])

        self.y, self.questions, self.answers, self.thresholds = y, questions, answers, thresholds
        self.after_initialization(data)
        self.data['model']['initialized'] = True

    def init_embedding_model(self, model, tfidf_idf=None, tfidf_vocabulary=None):
        """
        Initialize NLP models from data.
        :param model: <model> from QA data (see __init__)
        :param tfidf_idf: Inverse document frequency
        :param tfidf_vocabulary: TFIDF vocabulary
        """

        # model defaults
        model['name'] = model.get('name', '<UNNAMED>')
        model['embedding'] = model.get("embedding", "UniversalSentenceEncoder")
        model['algorithm'] = model.get("algorithm", "hybrid")
        model['lang'] = model.get('lang', 'en')
        model['use_tfidf'] = model.get('use_tfidf', False)

        if model['use_tfidf']:
            self.tokenizer = TweetTokenizer()
            self.TFIDF = TfidfVectorizer(analyzer='word',
                                         tokenizer=self.tokenize,
                                         lowercase=True,
                                         ngram_range=(1, 1),
                                         decode_error="ignore")
            if tfidf_idf is not None and tfidf_vocabulary is not None:
                self.TFIDF.idf_ = tfidf_idf
                self.TFIDF.vocabulary_ = tfidf_vocabulary
        else:
            self.TFIDF = None
        self.emb = EM.get_model(emb=model['algorithm'] if "embedding" not in model else model.get("embedding", "FastTextSW"), lang=model['lang'])

    def exact_match(self, query):
        response = {
            'similarity_per_level': 1.01,  # studio threshold
            'similarity_per_level_raw': 1.01,  # cosine
            'similarity_per_intent': 1.01,  # studio threshold
            'similarity_per_intent_raw': 1.01,  # cosine
            'classification_confidence': 1.01,

            "hit": "EXACT MATCH "
        }
        alternatives = [query['transcript']['text']] + [query['transcript']['normalizedText']] + [al['text'] for al in query['alternatives']] + [
            al['normalizedText'] for al in query['alternatives']]
        for text in alternatives:
            for cls, intent in self.data['qa'].items():
                if text.lower() in (ex.lower() for ex in intent['questions']):
                    response['threshold'] = intent['threshold']  # studio threshold
                    response['threshold_raw'] = intent['threshold']  # studio threshold - not correct value
                    response['answer'] = intent['answer']
                    response['hit'] += f'({text})'
                    return response
        return None

    def filter_negatives(self, responses, query):
        filtered_response = {
            'similarity_per_level': -1,  # studio threshold
            'similarity_per_level_raw': -1,  # cosine
            'similarity_per_intent': -1,  # studio threshold
            'similarity_per_intent_raw': -1,  # cosine
            'classification_confidence': -1,

            "hit": "NEGATIVE MATCH "
        }
        alternatives = [query['transcript']['text']] + [query['transcript']['normalizedText']] + [al['text'] for al in query['alternatives']] + [
            al['normalizedText'] for al in query['alternatives']]
        for text in alternatives:
            for cls, intent in self.data['qa'].items():
                if text.lower() in (ex.lower() for ex in intent.get('negativeQuestions', [])):
                    try:
                        idx = next(i for i, v in enumerate(responses) if v['answer'] == intent['answer'])
                        responses[idx].update(filtered_response)
                        responses[idx]['hit'] += f'({text})'
                    except StopIteration:
                        continue
        return responses

    def tfidf_vector(self, sentence):
        """
        Transform sentence into vector form using TF-IDF weights.
        :param sentence: input sentence
        :return: weighted sentence vector
        """

        tfidf = self.TFIDF.transform([sentence])
        names = self.TFIDF.get_feature_names()
        max_tf = np.max(tfidf[0])
        max_tf = max_tf if max_tf > 0 else 1

        vec = np.zeros((self.emb.dim,))
        tokens = self.tokenize(sentence)
        for token in tokens:
            try:
                tmp = tfidf[0, names.index(token)]
            except ValueError:
                tmp = max_tf
            vec += tmp * self.emb.vector(token)

        vec /= np.linalg.norm(vec)
        return vec

    def tokenize(self, sentence):
        """
        Tokenize sentence into words.
        Used for TF-IDF word tokenizer that requires list of tokens.
        :param sentence: input sentence
        :return: list of word tokens
        """

        tokens = tokenize(self.tokenizer, sentence, to_lower=True)
        return tokens.split(' ')

    def get_vector(self, query):
        return self.emb.vector(query.lower()) if self.TFIDF is None else self.tfidf_vector(query.lower())

    @abstractmethod
    def get_runtime_data(self):
        pass

    @abstractmethod
    def load_runtime_data(self, metadata, runtime):
        pass

    @abstractmethod
    def get(self, query, denied_answers=None, allowed_answers=None, n=1, use_threshold=True):
        """
        Get answer for a query.
        :param query: input query
        :param denied_answers: answers excluded form the result
        :param allowed_answers: only listed answers may be returned
        :param n: get top <n> results
        :param use_threshold: only results with similarity higher than threshold are returned, default True
        :return: [{answer: string, confidence: float, hit: string}]
        """
        pass


class AbstractNLPModelWrapper(Component, ABC):
    def __init__(self, _id, language, config):
        super(AbstractNLPModelWrapper, self).__init__()
        self._id = _id
        self.language = language
        self.config = config

        self._model = None

    def get_config_value(self, key):
        value = self.config.get(key, None)
        if value is None:
            logger.debug("Key {} is not present in CONFIG".format(key))
        return value


class SequenceAbstractTrainer(AbstractNLPModelWrapper):

    def __init__(self, _id, language, config):
        super(SequenceAbstractTrainer, self).__init__(_id=_id, language=language, config=config)

    @abstractmethod
    def _train(self, data, **kwargs):
        """
        :param train_json:
        :return: history, params
        """
        raise NotImplementedError("You need to override this method.")

    def train(self, data, **kwargs):
        """

        :param test_data:
        :param valid_data:
        :param train_data:
        :return:
        """
        history = self._train(data, **kwargs)

        test_data = [feature[2] for feature in data if len(feature) > 2]
        if test_data:
            precision, recall, f1, support = self._evaluate(test_json=test_data, debug=False)

    def _evaluate(self, test_json, debug) -> Tuple[float, float, float, int]:
        """

        :param test_json:
        :return: precision, recall, f1, support
        """
        raise NotImplementedError("You need to override this method.")
