import numpy as np
from sklearn.metrics import precision_recall_fscore_support, classification_report
from tensorflow.python.keras.callbacks import Callback
import tensorflow.python.keras.backend as K


def masked_acc(y_true, y_pred):
    mask = K.not_equal(y_true, 0)
    y_cls = K.cast(K.argmax(y_pred, axis=-1), K.floatx())
    return K.cast(K.equal(y_true[mask],
                          y_cls[mask]),
                  K.floatx())


class F1Metrics(Callback):

    def __init__(self, id2label, pad_value=0, validation_data=None):
        """
        Args:
            id2label (dict): id to label mapping.
            (e.g. {1: 'B-LOC', 2: 'I-LOC'})
            pad_value (int): padding value.
        """
        super(F1Metrics, self).__init__()
        self.id2label = id2label
        self.pad_value = pad_value
        self.validation_data = [d for d in validation_data][0]
        self.is_fit = validation_data is None

    def convert_idx_to_name(self, y):
        """Convert label index to name.
        Args:
            y (list): label index list.
        Returns:
            y: label name list.
        Examples:
        """
        return [self.id2label[l] for l in y]

    def predict(self, X, y):
        """Predict sequences.
        Args:
            X (list): input data.
            y (list): tags.
        Returns:
            y_true: true sequences.
            y_pred: predicted sequences.
        """
        y_pred = self.model.predict_on_batch(X)

        # reduce dimension.
        y_true = []
        y_pred_idx = np.argmax(y_pred, -1)
        y_pred_processed = []

        for si, sentence in enumerate(y):
            for ti, tag in enumerate(sentence):
                if tag != self.pad_value:
                    y_true.append(tag)
                    y_pred_processed.append(y_pred_idx[si][ti])

        y_true = self.convert_idx_to_name(y_true)
        y_pred = self.convert_idx_to_name(y_pred_processed)

        return y_true, y_pred

    def score(self, y_true, y_pred):
        """Calculate f1 score.
        Args:
            y_true (list): true sequences.
            y_pred (list): predicted sequences.
        Returns:
            score: f1 score.
        """
        labels = list(set(y_pred) - {'PAD'})
        _, _, f1_micro, _ = precision_recall_fscore_support(y_true, y_pred, labels=labels, average='micro')
        _, _, f1_macro, _ = precision_recall_fscore_support(y_true, y_pred, labels=labels, average='macro')
        _, _, f1_weighted, _ = precision_recall_fscore_support(y_true, y_pred, labels=labels, average='weighted')
        print(classification_report(y_true, y_pred, labels=labels))
        return f1_micro, f1_macro, f1_weighted

    def on_epoch_end(self, epoch, logs={}):
        if self.is_fit:
            self.on_epoch_end_fit(epoch, logs)
        else:
            self.on_epoch_end_fit_generator(epoch, logs)

    def on_epoch_end_fit(self, epoch, logs={}):
        X = self.validation_data[0]
        y = self.validation_data[1]
        y_true, y_pred = self.predict(X, y)
        score = self.score(y_true, y_pred)
        logs['f1_micro'], logs['f1_macro'], logs['f1_weighted'] = score

    def on_epoch_end_fit_generator(self, epoch, logs={}):
        y_true = []
        y_pred = []
        # for X, y in self.validation_data:
        y_true_batch, y_pred_batch = self.predict(self.validation_data[0], self.validation_data[1])
        y_true.extend(y_true_batch)
        y_pred.extend(y_pred_batch)
        score = self.score(y_true, y_pred)
        logs['f1_micro'], logs['f1_macro'], logs['f1_weighted'] = score
