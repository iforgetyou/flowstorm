import random
import re

import numpy as np
from keras_preprocessing.sequence import pad_sequences

from common.pipeline import Component
from common.twitter_tokenizer import TweetTokenizer
from common.utils.nlp import expand_text, tokenize


class Preprocessing(Component):
    def __init__(self):
        super(Preprocessing, self).__init__()
        self._tokenizer = TweetTokenizer()
        self._tokenize = lambda s: tokenize(self._tokenizer, s).split()

    def _fill_values(self, contexts, values):
        filled_contexts = []
        for context in contexts:
            pattern = re.compile(r"(?:[^\]]|^)\{([^\{\}]*)\}")
            if (match := pattern.search(context)) is None:
                filled_contexts.append(context)
                continue
            for value in values[match.group(1)]:
                filled = re.sub(rf'(?<!\])\{{{match.group(1)}\}}', f'[{value}]{{{match.group(1)}}}', context, count=1)
                filled_contexts.extend(self._fill_values([filled], values))
        return filled_contexts

    def process(self, data):
        contexts = [e for s in data['data'] for e in expand_text(s)]
        filled = self._fill_values(contexts, data['valueSamples']) if 'valueSamples' in data else contexts
        tokenized_data = []
        for sample in filled:
            pattern = re.compile(r"\[([^\[\]]*)\]\{([^\{\}]*)\}")
            matches = pattern.finditer(sample)

            pos = 0
            tokens = []
            for m in matches:
                tokens.extend((t, 'O') for t in self._tokenize(sample[pos: max(0, m.start() - 1)]))
                class_name = m.group(2)
                tokens.extend((t, f"B-{class_name}" if (i == 0) else f"I-{class_name}") for i, t in enumerate(self._tokenize(m.group(1))))
                pos = m.end() + 1

            tokens.extend((t, 'O') for t in self._tokenize(sample[pos:]))
            tokenized_data.append(tokens)
        random.shuffle(tokenized_data)
        return tokenized_data


class Padding(Component):
    def __init__(self, max_sequence_length: int):
        super(Padding, self).__init__(self.__class__.__name__)
        self.max_sequence_length = max_sequence_length

    @property
    def output_dim(self):
        for component in self._input_components:
            return component.output_dim
        return None

    def process(self, data):
        def generator():
            for words in data():
                sentence_features = np.zeros((self.max_sequence_length, self.output_dim))
                for i, word in enumerate(words):
                    if i >= self.max_sequence_length:
                        break
                    sentence_features[i, :] = words[i]
                yield sentence_features
        return generator


class Tokens(Component):
    def __init__(self):
        super(Tokens, self).__init__()
        self._tokenizer = TweetTokenizer()
        self._tokenize = lambda s: tokenize(self._tokenizer, s).split()

    def train(self, data, **kwargs):
        return [[[word[0] for word in sample] for sample in split] for split in data]

    def process(self, data):
        return self.predict(data)

    def predict(self, data, **kwargs):
        if 'tokens' in data:
            return [[word['text'] for word in data['tokens']]]
        else:
            return [self._tokenize(data['transcript']['text'])]


class Labels(Component):
    output_dim: int = 0
    _label2idx: dict = {}
    idx2label: list = []

    def __init__(self, max_sequence_length: int):
        super(Labels, self).__init__()
        self.max_sequence_length = max_sequence_length
        self.threshold = 0.5

    @property
    def label2idx(self):
        return self._label2idx

    @label2idx.setter
    def label2idx(self, label_dict):
        self._label2idx = label_dict
        self.idx2label = [''] * len(label_dict)
        for cls, idx in self.label2idx.items():
            self.idx2label[idx] = cls

    def train(self, data, **kwargs):
        first = True
        padded_splits = []
        for split in data:
            labels = [[y[1] for y in x] for x in split]

            # Mapping dictionary for labels
            if first:
                first = False
                label2idx = {l: i + 2 for i, l in enumerate(set([item for sublist in labels for item in sublist]))}
                label2idx["UNK"] = 1
                label2idx["PAD"] = 0
                self.output_dim = len(label2idx)
                self.label2idx = label2idx

            y = [[self.label2idx[tag] if tag in self.label2idx else self.label2idx["UNK"] for tag in example] for
                 example in labels]
            padded_splits.append(pad_sequences(maxlen=self.max_sequence_length, sequences=y, value=self.label2idx["PAD"],
                                   padding='post',
                                   truncating='post'))
        return padded_splits

    def process(self, data):
        self.train(data)

    def predict(self, data, **kwargs):
        res = []
        samples, predictions = data
        for tokens, prediction in zip(samples, predictions):
            result_tokens = []
            for token, probs in zip(tokens, prediction):
                token_obj = {'text': token, 'type': 'Word', 'classes': []}
                filtered_idx = self._filter_results(probs, kwargs['entity_types'])
                best_idx = np.argmax(probs, axis=-1)
                if probs[filtered_idx] > self.threshold:
                    token_obj['classes'].extend(self._create_classes(filtered_idx, f"{kwargs['model_id']}-filtered", probs))
                if best_idx != filtered_idx and probs[best_idx] > self.threshold:
                    token_obj['classes'].extend(self._create_classes(best_idx, f"{kwargs['model_id']}", probs))

                result_tokens.append(token_obj)
            res.append({'tokens': result_tokens})
        if len(res) == 1:
            return res[0]
        else:
            return res

    def _create_classes(self, idx, model_id, probs):
        cls = self.idx2label[idx]
        if cls != 'O' and cls != 'PAD':
            return [{'model_id': model_id, 'name': cls,
                    'score': str(probs[idx]), 'type': 'Entity'}]
        else:
            return []

    def _filter_results(self, probs, entity_types):
        sorted_indices = np.argsort(-probs, axis=-1)
        for idx in sorted_indices:
            label = self.idx2label[idx].replace('B-', '').replace('I-', '')
            if label == 'O' or not entity_types or label in entity_types:
                return idx
        return -1
