# -*- coding: utf-8 -*-
"""
    common.singleton
    ~~~~~~~~~~~~~~~~

    Singleton pattern that is used to instantiate a class only once per module
    with thread lock via metaclass pattern.
"""

from threading import Lock

LOCK = Lock()

class Singleton(type):
    """ Decorator for a class to make a singleton out of it. """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            with LOCK:
                if cls not in cls._instances:
                    cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
