from __future__ import annotations

from abc import ABC, abstractmethod
from datetime import datetime
from typing import List, Optional, Dict, Tuple

import jsons

from common.pipeline.component import Component, ComponentDefinition, Serializable
from common.pipeline.stage import StageDefinition, Stage


class ScikitCompat(ABC):
    """
    Interface layer for the Scikit and Keras compatibility.
    Inspired by HuggingFace Transformers pipeline
    """

    @abstractmethod
    def transform(self, data):
        raise NotImplementedError()

    @abstractmethod
    def predict(self, data):
        raise NotImplementedError()


class Pipeline:
    def __init__(self, name: str, stages: Optional[List[Stage]] = None):
        self.name = name
        self.stages = stages or []
        self.id = ''
        self.output: Optional[Component] = None

    def add_stage(self, stage: Stage):
        self.stages.append(stage)
        return self

    def predict(self, data, **kwargs):
        data_bus = {'inputs': data}
        for stage in self.stages:
            data_bus.update(stage.predict(data_bus, **kwargs))
        output_id = self.stages[-1].components[-1].__hash__() if self.output is None else self.output.__hash__()
        return data_bus[output_id]

    def train(self, data, **kwargs):
        data_bus = {'inputs': data}
        for stage in self.stages:
            data_bus.update(stage.train(data_bus, **kwargs))
        output_id = self.stages[-1].components[-1].__hash__() if self.output is None else self.output.__hash__()
        return data_bus[output_id]

    def get_stage_by_name(self, name: str) -> Optional[Stage]:
        for stage in self.stages:
            if stage.name == name:
                return stage
        return None

    def to_definition(self) -> PipelineDefinition:
        definition = PipelineDefinition(self.name)
        for stage in self.stages:
            stage_definition = StageDefinition(stage.name)
            for component in stage.components:
                component_definition = ComponentDefinition(type(component))
                inputs = []
                for input_component in component._input_components:
                    inputs.append((self.find_component_stage(input_component).name, input_component.name))
                component_definition.with_input_from(*inputs)
                stage_definition.add_component_definition(component_definition)
            definition.add_stage_definition(stage_definition)

        definition.output = (self.find_component_stage(self.output).name, self.output.name)
        return definition

    def find_component_stage(self, component: Component) -> Stage:
        for stage in self.stages:
            if component in stage.components:
                return stage
        return None

    def get_component_list(self):
        return [component for stage in self.stages for component in stage.components]


class PipelineDefinition:

    def __init__(self, name: str):
        self.name = name
        self.defined_stages: List[StageDefinition] = []
        self.output: Optional[Tuple[str, str]] = None

    def add_stage_definition(self, stage_definition: StageDefinition):
        self.defined_stages.append(stage_definition)
        return self

    def with_output_from(self, output: Tuple[str, str]):
        self.output = output
        return self

    def init_with_components(self, serialized_components: List[dict], binaries: Dict[str, bytes]) -> Pipeline:
        pipeline = Pipeline(self.name)
        component_dict = {c['name']: c for c in serialized_components}
        for stage_definition in self.defined_stages:
            stage = Stage(stage_definition.name)
            for component_definition in stage_definition.defined_components:
                component = jsons.load(component_dict[component_definition.cls.__name__], component_definition.cls)
                if component.name in binaries and isinstance(component, Serializable):
                    component.deserialize(binaries[component.name])

                inputs = []
                for stage_name, comp_name in component_definition.input_components:
                    inputs.append(pipeline.get_stage_by_name(stage_name).get_component_by_name(comp_name))
                component.with_input_from(*inputs) if (len(inputs)) > 0 else None
                stage.add_component(component)

            pipeline.add_stage(stage)
        if self.output:
            pipeline.output = pipeline.get_stage_by_name(self.output[0]).get_component_by_name(self.output[1])
        return pipeline




