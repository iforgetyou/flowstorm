from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Tuple, Optional, List


class ComponentDefinition:
    def __init__(self, cls: type):
        self.cls = cls
        self.input_components: List[Tuple[str, str]] = []

    def with_input_from(self, *components: Tuple[str, str]) -> ComponentDefinition:
        self.input_components = components
        return self


class Component(ABC):
    _input_components: Tuple[Component]

    def __init__(self, name: Optional[str] = None,
                 input_components: Tuple[Component] = ()):
        self.name: str = name or self.__class__.__name__
        self._input_components = input_components

    @abstractmethod
    def process(self, data):
        raise NotImplementedError()

    def predict(self, data, **kwargs):
        return self.process(data)

    def train(self, data, **kwargs):
        return [self.process(split) for split in data]

    def with_input_from(self, *components: Component) -> Component:
        self._input_components = components
        return self


class Serializable(ABC):

    @abstractmethod
    def serialize(self) -> bytes:
        raise NotImplementedError()

    @abstractmethod
    def deserialize(self, data: bytes):
        raise NotImplementedError()
