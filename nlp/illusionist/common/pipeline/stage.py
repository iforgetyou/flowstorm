from typing import List, Optional

from common.pipeline.component import ComponentDefinition, Component


class StageDefinition:

    def __init__(self, name: str):
        self.name = name
        self.defined_components: List[ComponentDefinition] = []

    def add_component_definition(self, component_definition: ComponentDefinition):
        self.defined_components.append(component_definition)
        return self


class Stage:

    def __init__(self, name: str, components: Optional[List[Component]] = None):
        self.name = name
        self.components = components or []

    def add_component(self, component: Component):
        self.components.append(component)
        return self

    def train(self, data, **kwargs):
        output = {}
        for component in self.components:
            output[component.__hash__()] = component.train(self._extract_input(data, component), **kwargs)
        return output

    def predict(self, data, **kwargs):
        output = {}
        for component in self.components:
            output[component.__hash__()] = component.predict(self._extract_input(data, component), **kwargs)
        return output

    def get_component_by_name(self, name: str) -> Optional[Component]:
        for component in self.components:
            if component.name == name:
                return component
        return None

    def _extract_input(self, data_bus: dict, component: Component):
        if component._input_components is None or len(component._input_components) == 0:
            return data_bus['inputs']
        elif len(component._input_components) == 1:
            return data_bus[component._input_components[0].__hash__()]
        else:
            return tuple(data_bus[c.__hash__()] for c in component._input_components)