# -*- coding: utf-8 -*-
"""
    common.model_manager
    ~~~~~~~~~~~~~~~~~~~~

    Module for managing loaded NLP models.

    @author: tomas.brich@seznam.cz
"""
import logging

from common.persistence import MongoDBStorage, FileStorage, DynamoDBS3Storage, StorageFactory
from common.singleton import Singleton
from common.utils import make_logger_name, get_memory_limit

logger = logging.getLogger(make_logger_name(__name__))


class BaseModelManager(metaclass=Singleton):
    """ Class that holds all currently running models. """

    def __init__(self):
        self.is_ready = True
        self._memory_limit = get_memory_limit(logger)
        self.storage = StorageFactory.get_storage_from_config()

    def get_memory_limit(self):
        """
        Return model manager memory limit.
        :return: memory limit <int>
        """
        return self._memory_limit

    def list_models(self, auth_key):
        """
        Lists parameters of all running models (for given key).
        :param auth_key: authentication key
        :return: list of model param dicts
        """

        params = []
        for model in self.storage.load_persisted():
            model_key = model.get('auth_key')
            if model_key == auth_key:
                tmp = model.get('model', dict()).copy()
                tmp['_id'] = model.get('_id')
                params.append(tmp)
        return params
