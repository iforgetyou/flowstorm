import decimal
import importlib
import json
import datetime

from os import getenv, path
from typing import TypeVar, Type, Optional

import jsons

from common.config import CONFIG
from common.singleton import Singleton


def get_out_of_scope_label():
    return "outofdomain"


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.strftime("%Y-%m-%dT%H:%M:%S%z")


def make_logger_name(file_name, logger_name='flask.app'):
    """ Join the default logger name with a module name. """
    return logger_name + "." + file_name


def get_memory_limit(logger):
    """
    Get current memor limit via docker setting or configuration value.
    :param logger: logger
    :return: current memory limit <int>
    """
    try:
        with open("/sys/fs/cgroup/memory/memory.limit_in_bytes") as memory:
            memory_value = memory.read()
            memory_limit = int(int(memory_value) * 0.7)
            logger.info("Container memory value: %s", memory_value)
            logger.info("Container memory limit: %s", memory_limit)
    except FileNotFoundError:
        memory_limit = int(CONFIG["memoryLimit"])
        logger.info("Memory limit from application configuration: %s", memory_limit)
    return memory_limit


def init_swagger_host(module_name, spec_dir):
    """
    Replace swagger service host via configuration.
    :param module_name: module name
    :param spec_dir: swagger specification directory
    """

    default_host = str('localhost:' + str(CONFIG.get(f"{module_name}.port", 8080)))
    try:
        service_host = CONFIG['serviceHost']
    except KeyError:
        service_host = default_host

    # change service host in swagger.yaml
    if service_host != default_host:
        swagger_path = path.join(spec_dir, 'swagger.yaml')
        with open(swagger_path) as swagger:
            data = swagger.read()
        with open(swagger_path, 'w') as swagger:
            swagger.write(data.replace(default_host, service_host))


class ObjectUtil:
    T = TypeVar('T')

    def __init__(self, use_decimals: bool = False):
        self.fork = jsons.fork()
        self.use_decimals = use_decimals
        jsons.set_serializer(lambda obj, **_: decimal.Decimal(str(obj)), float, fork_inst=self.fork)

    @staticmethod
    def _replace_decimals(obj):
        """
        Convert all whole number decimals in `obj` to integers
        """
        if isinstance(obj, list):
            return [ObjectUtil._replace_decimals(i) for i in obj]
        elif isinstance(obj, dict):
            return {k: ObjectUtil._replace_decimals(v) for k, v in obj.items()}
        elif isinstance(obj, decimal.Decimal):
            return int(obj) if obj % 1 == 0 else float(obj)
        else:
            return obj

    def load(self, json_obj: object, cls: Optional[Type[T]] = None, *args, **kwargs) -> T:
        if self.use_decimals:
            json_obj = ObjectUtil._replace_decimals(json_obj)
        return jsons.load(json_obj, cls, *args, fork_inst=self.fork, **kwargs)

    def loads(self, json_obj: object, cls: Optional[Type[T]] = None, *args, **kwargs) -> T:
        if self.use_decimals:
            json_obj = ObjectUtil._replace_decimals(json_obj)
        return jsons.loads(json_obj, cls, *args, fork_inst=self.fork, **kwargs)

    def dump(self, obj: object, *args, **kwargs) -> object:
        return jsons.dump(obj, *args, fork_inst=self.fork, **kwargs)

    def dumps(self, obj: object, *args, **kwargs) -> str:
        return jsons.dumps(obj, *args, fork_inst=self.fork, **kwargs)


class DictBackend:
    def encode(self, value: dict, indent=None, separators=None) -> dict:
        module_name = '.'.join(value['py/object'].split('.')[:-1])
        class_name = value['py/object'].split('.')[-1]
        module = importlib.import_module(module_name)
        cls = getattr(module, class_name)
        try:
            for field in cls._ignored_fields:
                del value[field]
        except AttributeError:
            pass
        return value

    def decode(self, value: dict) -> dict:
        return value
