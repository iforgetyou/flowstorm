import re
import sys
from collections import namedtuple


def format_brackets(token):
    """
    Formats bracket tokens.
    :param token: string token
    :return: formatted token
    """

    if token == '-LRB-':
        token = '('
    elif token == '-RRB-':
        token = ')'
    elif token == '-RSB-':
        token = ']'
    elif token == '-LSB-':
        token = '['
    elif token == '-LCB-':
        token = '{'
    elif token == '-RCB-':
        token = '}'
    return token


def tokenize(tokenizer, sentence, to_lower=True):
    """
    Tokenize sentence.
    :param tokenizer: a tokenizer implementing the NLTK tokenizer interface
    :param sentence: a string to be tokenized
    :param to_lower: lowercase input
    :return: tokenized string
    """

    sentence = sentence.strip()
    sentence = ' '.join([format_brackets(x) for x in tokenizer.tokenize(sentence)])
    if to_lower:
        sentence = sentence.lower()
    sentence = re.sub(r"((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))", "<url>", sentence)
    sentence = re.sub(r"(@[^\s]+)", "<user>", sentence)
    sentence = re.sub(r'\{ ([^\{\}]+) \}', r'{\1}', sentence)  # entity tokens
    filter(lambda word: r" " not in word, sentence)
    return sentence


def get_subwords(word, nmin=3, nmax=6):
    """
    Get a list of n-grams in <word> with n between <nmin> and <nmax>.
    :param word: input word
    :param nmin: lower bound on n
    :param nmax: upper bound on n
    :return: list of n-grams in <word>
    """

    word = f'<{word}>'
    return [word[i:j] for i in range(len(word))
            for j in range(i + nmin, 1 + min(i + nmax, len(word)))]


Expansion = namedtuple('Expansion', 'texts start end')


def expand_text(text: str):
    if '(' not in text and ')' not in text:
        return [text]
    start, group_start, stack = 0, 0, 0
    expansions, current_group = [], []
    for i, c in enumerate(text):
        if c == '(':
            if stack == 0:
                start = i + 1
                group_start = i + 1
                current_group = []
            stack += 1
        elif c == '|' and stack == 1:
            current_group.extend(expand_text(text[start: i]))
            start = i + 1
        elif c == ')':
            stack -= 1
            if stack == 0:
                current_group.extend(expand_text(text[start: i]))
                expansions.append(Expansion(texts=current_group, start=group_start - 1, end=i + 1))
        if stack < 0:
            raise ValueError("Missing opening parenthesis or too many closing ones.")

    if stack > 0:
        raise ValueError("Missing closing parenthesis or too many opening ones.")
    return replace(text, expansions)


def replace(text, expansions):
    def _replace(texts, expansions, i):
        if i < 0:
            return texts
        result = []
        for text in texts:
            for replacement in expansions[i].texts:
                res = text[:expansions[i].start] + replacement + text[expansions[i].end:]
                res = re.sub(' +', ' ', res.strip())
                if res != '':
                    result.append(res)

        return _replace(result, expansions, i - 1)

    return _replace([text], expansions, len(expansions) - 1)


if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        for s in f:
            for line in expand_text(s.strip()):
                print(line)
