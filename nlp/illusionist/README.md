# Illusionist QA

[![pipeline status](https://gitlab.promethist.ai/services/illusionist/badges/develop/pipeline.svg)](https://gitlab.promethist.ai/services/illusionist/commits/develop)
[![coverage report](https://gitlab.promethist.ai/services/illusionist/badges/develop/coverage.svg)](https://gitlab.promethist.ai/services/illusionist/commits/develop)

Simple question-answering application using word embeddings.

## Deployment

Build and run the app docker:
```bash
cd app
./docker_build.sh
./docker_run.sh
```

Note: The volume in `docker_run.sh` is used for model persistence.
You might have to set write permissions to all for the linked folder.
The image can be run without the volume if persistence is not required.

## Usage

1. Set up a QA model (you can set up as many models as you want):
- Open the Swagger UI (`http://localhost:8080/ui/`).
- Create a model using the `POST /models/{_id}` endpoint.
`_id` is a unique model identifier, which is later used in most other endpoints.
The endpoint requires QA data as payload in the following format:

```json
{
  "model": {
    "name": "<user-friendly model name>",
    "algorithm": "Sent2Vec",
    "lang": "en",
    "use_tfidf": false
  },
  "qa": {
    "id1": {
      "questions": ["first question", "second question"],
      "answer": "first answer"
    },
    "id2": {}
  }
}
```

- Supported algorithms: `Sent2Vec` (English only!), `FastText`, `FastTextSW`.
- Supported languages: `en` (English), `cs` (Czech), `es` (Spanish).
- `use_tfidf` parameter turns on TF-IDF weighting of embedding vectors.
- Alternatively, a model can be created by uploading the data as a json file, using
the `POST /models/{_id}/json` endpoint.

2. Query a QA model:
- You can use the `POST /query/{_id}` endpoint to query a QA model.
- Alternatively, go to `http://localhost:8080/illusionist/{_id}`,
where you can easily do the same with a simple user-interface.

If you started the docker image using a volume, the deployed models are saved to disk
as json files. You can manage the models using the `/models/` endpoints. On a new start
of the image, the models are automatically loaded. You can therefore also create, delete,
or modify the models directly in the `data/models` folder (but note that the docker image
will have to be restarted to reflect such changes).

## How to (re)deploy API into Google Cloud
```
gcloud endpoints services deploy app/swagger.yaml
```
