from app.classification.annotator import MultiPipelineAnnotator
from common.blocks.classification.token.external import Spacy
from common.blocks.classification.token.lstm import LstmTrainer
from common.blocks.embeddings.char import CharEmbedding, CaseEmbedding
from common.blocks.embeddings.fasttext import FastTextSW
from common.blocks.preprocessing import Tokens, Padding, Labels
from common.pipeline import PipelineDefinition, StageDefinition, ComponentDefinition


class EntityPipelineFactory:

    @staticmethod
    def pipeline_definition(name=None):
        if name == 'multi':
            return PipelineDefinition('multi').add_stage_definition(StageDefinition('multi').add_component_definition(ComponentDefinition(MultiPipelineAnnotator)))
        if name == 'spacy':
            return PipelineDefinition('spacy').add_stage_definition(StageDefinition('spacy').add_component_definition(ComponentDefinition(Spacy)))
        else:
            return PipelineDefinition("pipeline").add_stage_definition(
                StageDefinition("preproc")
                    .add_component_definition(ComponentDefinition(Tokens))
            ).add_stage_definition(
                StageDefinition('embedding')
                    .add_component_definition(ComponentDefinition(FastTextSW).with_input_from(('preproc', 'Tokens')))
                    .add_component_definition(ComponentDefinition(CharEmbedding).with_input_from(('preproc', 'Tokens')))
                    .add_component_definition(ComponentDefinition(CaseEmbedding).with_input_from(('preproc', 'Tokens')))
            ).add_stage_definition(
                StageDefinition('padding')
                    .add_component_definition(ComponentDefinition(Padding).with_input_from(('embedding', 'FastTextSW')))
            ).add_stage_definition(
                StageDefinition('training')
                    .add_component_definition(ComponentDefinition(LstmTrainer).with_input_from(('padding', 'Padding'),
                                                                                               ('embedding', 'CharEmbedding'),
                                                                                               ('embedding', 'CaseEmbedding')))
            ).add_stage_definition(
                StageDefinition('postproc')
                    .add_component_definition(ComponentDefinition(Labels).with_input_from(('preproc', 'Tokens'),
                                                                                          ('training', 'LstmTrainer')))
            ).with_output_from(('postproc', 'Labels'))