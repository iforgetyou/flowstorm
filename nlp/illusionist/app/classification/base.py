import logging
from abc import abstractmethod

from common.blocks.classification.base import AbstractNLPModelWrapper
from common.utils import make_logger_name

logger = logging.getLogger(make_logger_name(__name__))


class SequenceAbstractAnnotator(AbstractNLPModelWrapper):

    def __init__(self, _id, language, model_data, mapping, config, vectorizer):
        super().__init__(_id, language, config, vectorizer)
        self.model_data = model_data
        self.mapping = mapping

    def process(self, data):
        return self.annotate(data)

    def annotate(self, query):
        logger.debug("Query: {}".format(query))
        if "tokens" not in query and "text" in query:
            query["tokens"] = [{"text": t, "type": "word"} for t in query["text"].split(" ")]
        elif "tokens" not in query:
            raise AttributeError("Query tokens or a text must be specified")
        prediction = self._annotate(query["tokens"])

        if len(prediction) < len(query['tokens']):
            logger.warning('Maximum sequence length (%d) is lower than length of the input sequence (%d). The rest of '
                           'the sequence is ignored.', self.config['maxSequenceLength'], len(query['tokens']))

        for pred, token in zip(prediction, query['tokens']):
            if pred['class'] == "O":
                continue

            if "classes" not in token:
                token["classes"] = []

            if not any([1 if x["type"] == self.get_annotation_name() and x["model_id"] == self._id else 0 for x in token["classes"]]):
                token["classes"].append({"type": self.get_annotation_name(),
                                         "model_id": self._id,
                                         "name": pred["class"],
                                         "score": str(pred["confidence"])})
            else:
                raise RuntimeError("The query has been already annotated by type {}.".format(self.get_annotation_name()))

        return query

    @abstractmethod
    def get_annotation_name(self):
        raise NotImplementedError("You need to override this method.")

    @abstractmethod
    def _annotate(self, query):
        """
        :param query:
        :return: prediction (after idx2label)
        """
        raise NotImplementedError("You need to override this method.")
