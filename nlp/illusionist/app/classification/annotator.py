import app.model_manager
from typing import List

from common.pipeline import Component, Pipeline, Stage


class MultiPipelineAnnotator(Component):

    def __init__(self, pipeline_ids: List[str]):
        super().__init__()
        self.pipeline_ids = pipeline_ids
        self._pipelines = [app.model_manager.EntityModelManager().get_model(_id) for _id in pipeline_ids]

    def process(self, data):
        self.predict(data)

    def predict(self, data, **kwargs):
        outputs = []
        for pipeline in self._pipelines:
            outputs.append(pipeline.predict(data, model_id=pipeline.id, entity_types=data.get('entity_types')))
        return self._merge(outputs)

    def _merge(self, outputs):
        res = {'tokens': []}
        for output in outputs:
            for i, token in enumerate(output['tokens']):
                if len(res['tokens']) <= i:
                    res['tokens'].append(token)
                elif 'classes' in token:
                    if 'classes' not in res['tokens'][i]:
                        res['tokens'][i]['classes'] = []
                    res['tokens'][i]['classes'].extend(token['classes'])
        return res


if __name__ == '__main__':
    import training.model_manager
    multi = Pipeline('multi').add_stage(Stage('multi').add_component(MultiPipelineAnnotator(['core_web_sm_en', '6030c5eced92130b945be3a6'])))
    mm = training.model_manager.EntityModelManager()
    mm.save_pipeline('default_en', 'multi', 'en', multi)
