group = "ai.flowstorm.nlp"
description = "Flowstorm NLP Illusionist App"

tasks.register<Exec>("build") {
    commandLine = listOf("pip3", "install", "gunicorn", "flask")
}

tasks.register<Exec>("run") {
    commandLine = listOf("python3", "app.py")
}