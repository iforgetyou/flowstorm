import logging
from flask import request

from app.model_manager import IntentModelManager
from common.utils import make_logger_name

MM = IntentModelManager()
logger = logging.getLogger(make_logger_name(__name__))


### Legacy functions ###


def legacy_get_model_by_id(_id):
    return get_model_by_id(_id)


def legacy_post_query(_id, query, denied_answers=None, allowed_answers=None, n=1, use_threshold=True):
    return post_query(_id, query, denied_answers, allowed_answers, n, use_threshold)


def legacy_post_query_multi_model(body):
    return post_query_multi_model(body)


### End of legacy functions ###


def get_model_by_id(_id):
    """
    Get QA model data by model ID.
    :param _id: model ID
    :return: json response, status code
    """

    key = request.args.get('key')

    data = MM.get_model(_id=_id, auth_key=key, allow_global=True).data.copy()
    data['auth_key'] = '<HIDDEN>'

    return data, 200


def post_query(_id, query, denied_answers=None, allowed_answers=None, n=1, use_threshold=True):
    """
    Query a QA model.
    :param _id: model ID
    :param query: user's query
    :param denied_answers: answers excluded form the result
    :param allowed_answers: only listed answers may be returned
    :param n: number of results to return
    :param use_threshold: only results with similarity higher than threshold are returned, default True
    :return: json response, status code
    """

    key = request.args.get('key')

    res = MM.query_model(_id=_id, auth_key=key, input_to_model=query, denied_answers=denied_answers,
                         allowed_answers=allowed_answers, n=n, use_threshold=use_threshold)

    return res, 200


def post_query_multi_model(body):
    """
    Query multiple QA models.
    :param body: request body
    :return: json response, status code
    """

    key = request.args.get('key')

    res = MM.query_models(turn_id=body.get("turn_id"), models=body.get("models"), auth_key=key, query=body.get('query'),
                          denied_answers=body.get('denied_answers'), allowed_answers=body.get('allowed_answers'),
                          n=body.get('n', 1), use_threshold=body.get('use_threshold', True))

    return res, 200
