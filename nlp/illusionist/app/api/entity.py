from flask import request

from app.model_manager import EntityModelManager

MM = EntityModelManager()


def post_query(_id, query, language):
    """
    Query a QA model.
    :param tokenized:
    :param language:
    :param _id: model ID
    :param query: user's query
    :return: json response, status code
    """

    key = request.args.get('key', "XXX")

    res = MM.query_model(_id=_id, auth_key=key, query=query, language=language)
    return res, 200


def post_query_by_space_id(space_id, query, language, include_root, first_turn):
    """
    Query multiple QA models.
    :param body: request body
    :return: json response, status code
    """
    key = request.args.get('key', "XXX")
    res = MM.query_by_space_id(space_id, key, query, include_root, first_turn, language)
    return res, 200


def invalidate_model(_id, language="en"):
    """
    Query a QA model.
    :param language:
    :param _id: model ID
    :return: json response, status code
    """

    key = request.args.get('key', "XXX")

    res = MM.invalidate_model(_id=_id, auth_key=key, language=language)
    return res, 200

