# -*- coding: utf-8 -*-
"""
    app.model_manager
    ~~~~~~~~~~~~~~~~~

    Module for managing loaded NLP models.
"""
import logging

from os import getpid
import gc
from typing import Dict, List
import json
import psutil
from datetime import datetime

from elasticsearch import Elasticsearch

from app.classification.annotator import MultiPipelineAnnotator
from app.pipeline import EntityPipelineFactory
from common.api.exceptions import ModelPermissionError, ModelNotFoundError, IncompatibleModels, UnknownAlgorithm
from common.blocks.classification.sentence.cosine_similarity import CosineSimilarity
from common.blocks.classification.sentence.logistic_regression import LogisticRegression
from common.blocks.classification.sentence.hybrid import Hybrid
from common.persistence import StorageFactory, Status
from common.pipeline import Pipeline, Component
from common.utils import make_logger_name, get_out_of_scope_label
from common.model_manager import BaseModelManager
from common.concurrency import ExecutorFactory

from common.config import CONFIG

logger = logging.getLogger(make_logger_name(__name__))
process = psutil.Process(getpid())


class IntentModelManager(BaseModelManager):
    """ Class that holds all currently running models. """

    def __init__(self):
        super().__init__()
        self.models = {}  # dict {<_id>: <model_instance>}
        self.models_ids = []
        self.logging_to_elastic = "es.scheme" in CONFIG and "es.user" in CONFIG and "es.password" in CONFIG
        if self.logging_to_elastic:
            self.es = Elasticsearch(f'{CONFIG["es.scheme"]}://{CONFIG["es.host"]}:{CONFIG["es.port"]}', http_auth=(CONFIG["es.user"], CONFIG["es.password"]))

        logger.info("Model Manager initialization complete")

    def check_memory_limit(self, _id):
        """ Check global memory objects and run cleaning with lock. """

        logger.info(f'Check memory limit. {len(self.models)} models in cache.')

        while self._memory_limit < process.memory_info().rss and len(self.models_ids) > 1:
            logger.info(f'Used {process.memory_info().rss} B out of {self._memory_limit} B of memory')
            model_id = self.models_ids[0]
            if model_id != _id:
                logger.info('Remove model from application memory with id: %s', model_id)
                try:
                    del self.models[model_id]
                except KeyError:
                    logger.error(f'Unable to remove model {model_id} from cache. Model not found in cache.')
                del self.models_ids[0]
            else:
                self.models_ids.append(self.models_ids.pop(self.models_ids.index(model_id)))
            gc.collect()

    def get_model(self, _id, auth_key, allow_global=False):
        """
        Get a model by ID.
        :param _id: model ID
        :param auth_key: authentication key
        :param allow_global: allow regular users to see global (studio created) models
        :return: model instance
        """

        model = self.models.get(_id)

        if not model:
            data = self.storage.get_model_data(_id)
            if data:
                logger.info('Loaded model _id: %s', _id)
                model = self.load_to_cache(_id, data)
                model.data['auth_key'] = auth_key

        if model:
            model_key = model.data.get('auth_key')
            if model_key == auth_key:
                return model
            raise ModelPermissionError(f'no model with id {_id} for the given api_key found')
        raise ModelNotFoundError(f'no model with id {_id} for the given api_key found')

    def load_to_cache(self, _id, data):
        """
        Create model object and save to application cache.
        :param _id: model identifier
        :param data: model raw data
        :return: model
        """
        if data["model"].get("approach", data["model"].get("algorithm", "")) == "logistic":
            model = LogisticRegression(data.get('model'), data.get('qa'), data.get('runtime'))
        elif data["model"].get("approach", data["model"].get("algorithm", "")) == "hybrid":
            model = Hybrid(data.get('model'), data.get('qa'), data.get('runtime'))
        elif data["model"].get("approach", data["model"].get("algorithm", "")) == "cosine":
            model = CosineSimilarity(data.get('model'), data.get('qa'), data.get('runtime'))
        else:
            raise UnknownAlgorithm(f"Unkown algorithm {data['model'].get('algorithm', data['model'].get('approach', 'ALGORITHM-NOT-SET-PROPERLY'))}")

        self.models[_id] = model
        if _id not in self.models_ids:
            self.models_ids.append(_id)
        else:
            self.models_ids.append(self.models_ids.pop(self.models_ids.index(_id)))

        self.check_memory_limit(_id)
        return model

    def query_model(self, _id, auth_key, query, n=1, use_threshold=True, **kwargs):
        """
        Query a model by ID.
        :param _id: model ID
        :param auth_key: authentication key
        :param query: query dict or string
        :param n: get top <n> results
        :param use_threshold: only results with similarity higher than threshold are returned, default True
        :return: query response (model ID saved in the response as <_id>)
        """
        model = self.get_model(_id, auth_key, allow_global=True)
        if "model_type" in kwargs and kwargs.get('model_type') != type(model):
            raise IncompatibleModels(f"The model with {_id} has type {type(model)}, which is not the same type as "
                                     f"base model {kwargs.get('model_type')}. Please, rebuild models.")

        exact_match = model.exact_match(query)
        response = model.get(query if type(query) is not dict else query['transcript']['vector'],
                             kwargs.get('denied_answers'), kwargs.get('allowed_answers'), n, use_threshold)
        response = [r for r in response if exact_match is None or r['answer'] != exact_match['answer']]
        if exact_match is not None:
            response = [exact_match] + response
        response = model.filter_negatives(response, query)
        for r in response:
            r['_id'] = _id
        return response

    def query_models(self, turn_id, models, auth_key, query, n=1, use_threshold=True, **kwargs):
        """
        Query multiple models by a list of IDs.
        :param models: list of model (id, name)
        :param auth_key: authentication key
        :param query: input query
        :param n: get top <n> results from each model
        :param use_threshold: only results with similarity higher than threshold are returned, default True
        :return: aggregated query response
        """
        _ids = [x["id"] for x in models]
        id2name = {x["id"]: x["name"].split("#")[0] for x in models}
        input_id = models[0]["name"]
        local_id = models[0]["id"]

        if type(query) == str:
            query = {'transcript': {"text": query, 'normalizedText': query, 'confidence': 1.0}, 'alternatives': []}

        if len(query['transcript']['normalizedText']) == 0:
            responses = [{
                '_id': _ids[0],
                'answer': get_out_of_scope_label(),
                'confidence': 1.0,
                'hit': "EMPTY QUERY OR GARBAGE",
            }]
            return responses

        base_model = self.get_model(_ids[0], auth_key, allow_global=True)
        base_model_type = type(base_model)

        denied_answers = [None] * len(_ids) if not kwargs.get('denied_answers') else kwargs['denied_answers']
        allowed_answers = [None] * len(_ids) if not kwargs.get('allowed_answers') else kwargs['allowed_answers']

        query['transcript']['vector'] = base_model.get_vector(query['transcript']['normalizedText'])
        if base_model_type == LogisticRegression:
            found = False
            grouped_response = []
            for num_model, (denied, allowed, _id) in enumerate(zip(denied_answers, allowed_answers, _ids)):
                response = self.query_model(_id, auth_key, query['transcript']['normalizedText'], n, use_threshold,
                                            denied_answers=denied, allowed_answers=allowed, model_type=base_model_type)
                response = sorted(response, key=lambda r: r['confidence'], reverse=True)

                if len(response) > 0 and response[0]["answer"] != get_out_of_scope_label() and not found:
                    found = True
                    response = list(filter(lambda x: x["answer"] != get_out_of_scope_label(), response))
                    grouped_response = response + grouped_response
                    continue

                if (num_model < len(_ids) - 1) or found:
                    response = list(filter(lambda x: x["answer"] != get_out_of_scope_label(), response))

                if found:
                    grouped_response = grouped_response + response
                else:
                    grouped_response = response + grouped_response

            grouped_response = list(sorted(grouped_response, key=lambda x: x["confidence"] != 1.0))

            return grouped_response[:n] if n > 0 else grouped_response

        elif base_model_type == CosineSimilarity or base_model_type == Hybrid:
            responses = [self.query_model(_id, auth_key, query, n, use_threshold,
                                          denied_answers=denied, allowed_answers=allowed, model_type=base_model_type)
                         for denied, allowed, _id in zip(denied_answers, allowed_answers, _ids)]
            responses = [r for el in responses for r in el]
            responses = sorted(responses, key=lambda r: (r['similarity_per_intent_raw'] > r["threshold_raw"],
                                                         r['similarity_per_level_raw'],
                                                         r['classification_confidence']), reverse=True)

            # logging to elastic search here

        # deleting added values of response dictionary to standardize format
        if base_model_type == Hybrid:
            idx_ood = 0
            for idx, r in enumerate(responses):
                if r['similarity_per_intent_raw'] < r["threshold_raw"]:
                    idx_ood = idx
                    break
                idx_ood = len(responses)
            responses = responses[0:idx_ood] + [{
                            '_id': _ids[0],
                            'answer': get_out_of_scope_label(),
                            'similarity_per_level': 0.0,  # studio threshold
                            'similarity_per_level_raw': 0.0,  # cosine
                            'similarity_per_intent': 0.0,  # studio threshold
                            'similarity_per_intent_raw': 0.0,  # cosine
                            'classification_confidence': 0.0,

                            'threshold': 0.0,  # studio threshold
                            'threshold_raw': 0.0,  # cosine
                            'hit': "HIGH THRESHOLDS OR LOW SIMILARITY" if idx_ood == 0 else ""
                        }] + list(filter(lambda key: key["_id"] == _ids[0],responses[idx_ood:]))

            if self.logging_to_elastic:
                for rank, r in enumerate(responses):
                    r["rank"] = str(rank)
                    r["transcript"] = query["transcript"]["normalizedText"]
                    r["timestamp"] = datetime.now().isoformat()
                    r["input_id"] = input_id
                    r["intent_id"] = id2name[r["_id"]] + f"#{r['answer']}"
                    r["intent_model_id"] = r["_id"]
                    r["turn_id"] = turn_id
                    r["level"] = "OOD" if r["answer"] == get_out_of_scope_label() else "LOCAL" if r["_id"] == local_id else "GLOBAL"

            responses_raw = []
            for response in responses:
                # clean up in response object
                responses_raw.append({
                    "confidence": response["similarity_per_intent"],
                    "answer": response["answer"],
                    "_id": response["_id"],
                    "hit": response["hit"],
                })

            if self.logging_to_elastic:
                index = "illusionist-intent-index-" + datetime.now().isoformat()[:7].replace("-", ".")
                ExecutorFactory.get().submit(self.logging_to_elastic_search, index, responses)

            responses = responses_raw

        elif base_model_type == CosineSimilarity:
            for response in responses:
                # clean up in response object
                response["confidence"] = response["similarity"]
                del response["similarity"]

        return responses[:n] if n > 0 else responses

    def logging_to_elastic_search(self, index, responses):
        logging.getLogger().setLevel(logging.CRITICAL)
        for r in responses:
            del r["_id"]
            self.es.index(index=index, document=r)
        logging.getLogger().setLevel(logging.INFO)
        logging.info(f" {len(responses)} documents saved into ElasticSearch")


class EntityModelManager(BaseModelManager):
    """ Class that holds all currently running models. """

    def __init__(self):
        super().__init__()
        self.pipelines: Dict[str, Pipeline] = {}
        self.pipelines_ids: List[str] = []
        self.space_cache: Dict[str, Component] = {}
        self.storage = StorageFactory().get_entity_storage_from_config()

    def check_memory_limit(self, _id):
        """ Check global memory objects and run cleaning with lock. """

        logger.info('Check memory limit')

        while self._memory_limit < process.memory_info().rss and len(self.pipelines_ids) > 1:
            model_id = self.pipelines_ids[0]
            if model_id != _id:
                logger.info('Remove model from application memory with id: %s', model_id)
                del self.pipelines[model_id]
                del self.pipelines_ids[0]
            else:
                self.pipelines_ids.append(self.pipelines_ids.pop(self.pipelines_ids.index(model_id)))
            gc.collect()

    def get_model(self, _id, auth_key="XXX"):
        """
        Get a model by ID.
        :param language:
        :param _id: model ID
        :param auth_key: authentication key TODO
        :return: model instance
        """

        pipeline = self.pipelines.get(_id)

        if not pipeline:
            item = self.storage.get_model(_id)
            binaries = {it.id.split('-')[-1]: it.data for it in item.binaries}
            pipeline = EntityPipelineFactory.pipeline_definition(item.pipeline).init_with_components(item.components, binaries)
            pipeline.last_modified = item.last_modified
            logger.info('Loaded entity pipeline _id: %s', _id)
            pipeline.id = _id

        if pipeline:
            self.pipelines[_id] = pipeline
            return pipeline

        raise KeyError(f'no model with id {_id} found')

    def query_model(self, _id, auth_key, query, language="en"):
        """
        Query a model by ID.
        :param language:
        :param _id: model ID
        :param auth_key: authentication key
        :param query: input query
        :return: query response (model ID saved in the response as <_id>)
        """

        response = self.get_model(_id, auth_key).predict(query, model_id=_id)
        return response

    def query_by_space_id(self, space_id, auth_key, query, include_root=False, first_turn=False, language="en"):
        """
        Query a model by ID.
        :param first_turn: whether the request is created by the first turn in a session
        :param include_root:  whether to use global (ROOT) models
        :param language:
        :param space_id: space ID which the models belongs to
        :param auth_key: authentication key
        :param query: input query
        :return: query response (model ID saved in the response as <_id>)
        """
        cache_key = language + '_' + space_id + ('+ROOT' if include_root else '')
        if first_turn or cache_key not in self.space_cache:
            model_items = self.storage.get_by_space_id(space_id, language, include_root)
            for item in model_items:
                if item.status != Status.FINISHED:
                    logger.warning(f'Model {item.id} has {item.status}. Skipping.')
                    continue
                pipeline = self.pipelines.get(item.id)
                if not pipeline or pipeline.last_modified < item.last_modified:
                    binaries = {it.id.split('-')[-1]: it.data for it in self.storage.get_binary_data(item.binaries)}
                    pipeline = EntityPipelineFactory.pipeline_definition(item.pipeline).init_with_components(item.components, binaries)
                    pipeline.last_modified = item.last_modified
                    logger.info('Loaded entity pipeline _id: %s', item.id)
                    pipeline.id = item.id
                    self.pipelines[item.id] = pipeline
            self.space_cache[cache_key] = MultiPipelineAnnotator([it.id for it in model_items])

        return self.space_cache[cache_key].predict(query)

    def invalidate_model(self, _id, auth_key="XXX", language="en"):
        """
        After retraining new model, the train modul should send a request to invalidate cached model
        :param _id:
        :param auth_key:
        :param language:
        :return:
        """
        self.pipelines.pop(_id, None)
