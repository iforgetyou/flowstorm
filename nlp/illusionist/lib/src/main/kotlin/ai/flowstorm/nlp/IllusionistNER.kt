package ai.flowstorm.nlp

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.Input
import ai.flowstorm.core.InputEntity
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.util.Logger
import ai.flowstorm.util.use
import com.fasterxml.jackson.annotation.JsonProperty
import javax.ws.rs.client.Entity
import javax.ws.rs.core.GenericType

class IllusionistNER : IllusionistComponent() {

    override fun process(scope: PipelineScope, span: PerformanceSpan) = use(scope) {
        val request = Request(context.input.alternatives, context.input.transcript,
                              context.input.tokens, context.entityTypes)
        val response = webTarget.path("/entity/query/space/${context.session.space_id}")
                .queryParam("language", (context.session.locale ?: context.input.locale).toString()).queryParam("include_root", true)
                .queryParam("first_turn", context.session.turns.size <= 1)
                .request().post(Entity.json(request), object : GenericType<Response>() {})
        val rawTokens = context.turn.input.tokens.toList()
        context.turn.input.tokens.clear()
        context.turn.input.tokens.addAll(mergeTokens(rawTokens, response.tokens, context.logger))
        val entities = InputEntity.fromAnnotation(context.turn.input.words)
        synchronized(context.input.entityMap) {
            entities.forEach { (k, v) ->
                context.input.entityMap.getOrPut(k) { mutableListOf() }.apply {
                    v.forEach { it.text = it.text.replace(Regex("[.,!?]$"), "").trim() }
                    addAll(v)
                    sortBy { -it.confidence }
                }
            }
        }
    }

    private fun mergeTokens(rawTokens: List<Input.Token>, annotatedTokens: List<Input.Token>, logger: Logger): List<Input.Token> {
        val res = mutableListOf<Input.Token>()
        rawTokens.zip(annotatedTokens) { t1, t2 ->
            if (t1.text != t2.text) {
                logger.warn("Token text mismatch after NER processing (${t1.text} != ${t2.text})")
            }
            res.add(t2)
        }
        if (rawTokens.size != annotatedTokens.size) {
            logger.warn("Tokens length mismatch after NER processing (${rawTokens.size} != ${annotatedTokens.size})")
        }
        res.addAll(rawTokens.drop(annotatedTokens.size))
        return res
    }

    private class Request(val alternatives: List<Input.Transcript>,
                          val transcript: Input.Transcript,
                          val tokens: List<Input.Token>,
                          @field:JsonProperty("entity_types")
                          val entityTypes: Set<String>)

    data class Response(val tokens: List<Input.Token>)
}
