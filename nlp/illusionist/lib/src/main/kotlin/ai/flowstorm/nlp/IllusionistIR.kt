package ai.flowstorm.nlp

import ai.flowstorm.core.Context
import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import com.fasterxml.jackson.annotation.JsonProperty
import ai.flowstorm.core.Input
import ai.flowstorm.core.InputEntity
import ai.flowstorm.core.Transcript
import ai.flowstorm.core.model.Model
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.LoggerDelegate
import javax.inject.Inject
import javax.ws.rs.client.Entity
import javax.ws.rs.core.GenericType

class IllusionistIR : IllusionistComponent() {

    @Inject
    lateinit var ner: IllusionistNER
    @Inject
    lateinit var simpleNer: SimpleNER
    @Inject
    lateinit var duckling: Duckling

    private val logger by LoggerDelegate()

    override fun process(scope: PipelineScope, span: PerformanceSpan) = with(scope) {
        val models = context.intentModels
        if (models.isEmpty()) {
            logger.info("IllusionistIR - nothing to do")
        } else {
            // Wait for NER results only if there are entities to mask
            if (context.entityTypes.isNotEmpty()) {
                val startTime = currentTime()
                wait(duckling, ner, simpleNer)
                context.logger.info("IllusionistIR - waited for NER components for ${currentTime() - startTime} ms")
            }

            val request = Request(constructQuery(context), models = models, turnId = context.turn._id.toString())
            val responses = webTarget.path("/intent/query/multi_model").request()
                .post(Entity.json(request), object : GenericType<List<Response>>() {})

            if (responses[0].answer == OUT_OF_DOMAIN_ACTION) {
                context.input.action = OUT_OF_DOMAIN_ACTION
            }

            for (response in responses) {
                if (response.answer != OUT_OF_DOMAIN_ACTION) {
                    val name = response._id + "#" + response.answer

                    context.turn.input.classes.add(Input.Class(Input.Class.Type.Intent, name, response.confidence))
                }
            }

            context.logger.info("IllusionistIR with models $models")
        }
    }

    private fun constructQuery(context: Context): Query {
        // The ER needs to be triggered before IR
        val entities = context.input.entityMap.values.flatten()
        val defaultProvider = context.input.transcript.provider

        val originalTranscript = context.input.transcript.copy()
        val maskedTranscript = context.input.transcript.let { it.copy(
            text = it.text.replaceEntities(entities, context.entityTypes),
            normalizedText = it.normalizedText.replaceEntities(entities, context.entityTypes)
        ) }

        val originalAlternatives = context.input.alternatives.toMutableList()
        val maskedAlternatives = context.input.alternatives
            .map { it.copy(
                text = it.text.replaceEntities(entities, context.entityTypes),
                normalizedText = it.normalizedText.replaceEntities(entities, context.entityTypes)
            ) }.union(originalAlternatives).distinctBy { it.text }.filter { defaultProvider == null || it.provider == defaultProvider}

        context.pipelineScope.log.logger.info("IllusionistIR - Masked transcript: '${maskedTranscript}', original transcript: '${originalTranscript}' and alternatives: '${maskedAlternatives.joinToString(",")}'")
        return Query(
            transcript = maskedTranscript,
            alternatives = if (originalTranscript.text != maskedTranscript.text) { maskedAlternatives.union(listOf(originalTranscript)) } else { maskedAlternatives}
                .filter { defaultProvider == null || it.provider == defaultProvider}
                .sortedBy { it.confidence }.reversed()
        )
    }

    private fun String.replaceEntities(entities: List<InputEntity>, entityTypes: Set<String>): String {
        var text = this
        entities.forEach {
            val entityText = it.text.replace(Regex("[.!?:;]$"), "").trim()
            if (entityTypes.contains(it.className)) text = text.replace(entityText, "{${it.className}}", ignoreCase = true)
        } // Better to replace by index
        return text
    }

    data class Response(
            val _id: String,
            val answer: String,
            val confidence: Float,
            val hit: String
    )

    data class Request(
            val query: Query,
            val models: List<Model>,
            @field:JsonProperty("turn_id")
            val turnId: String,
            @field:JsonProperty("denied_answers")
            val deniedAnswers: List<List<String?>?> = listOf(),
            @field:JsonProperty("allowed_answers")
            val allowedAnswers: List<List<String?>?> = listOf(),
            @field:JsonProperty("use_threshold")
            val useThreshold: Boolean = true,
            val n: Int = 0 //number of maximum results, ordered by confidence, default 0 == no limit
    )

    data class Query(
        val transcript: Transcript,
        val alternatives: List<Transcript> = listOf()
    )

    companion object {
        const val OUT_OF_DOMAIN_ACTION = "outofdomain"
    }
}
