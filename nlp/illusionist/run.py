# -*- coding: utf-8 -*-
"""
    run_app
    ~~~~~~~

    This file implements Flask server for the app application.
"""

from os import getenv
from common.config import CONFIG
from common.init import create_app


MODULE_NAME = getenv('APP', 'app')

application = create_app(MODULE_NAME)
app = application.app

if __name__ == '__main__':
    app.run(host='localhost', port=CONFIG.get(f"{MODULE_NAME}.port", 8080),
            debug=CONFIG["debug"] == 'true')
