# -*- coding: utf-8 -*-
"""
    tests.test_endpoints
    ~~~~~~~~~~~~~~~~~~~~

    Test Flask app endpoints.
"""

import json
import pytest

from common.api.exceptions import ModelNotFoundError
from common.config import CONFIG
from training.model_manager import IntentModelManager

MM = IntentModelManager()


def test_create_model(test_training, qa_data_hybrid):
    """
    Sets up a testing model.
    Required for the following tests!
    """

    # create model (POST /models/{_id})
    response = test_training.post('/training/models/test?key=test', data=json.dumps(qa_data_hybrid),
                                  content_type='application/json')
    assert response.status_code == 200

    # check data (GET /models/{_id})
    response = test_training.get('/training/models/test?key=test')
    qa_data_hybrid['auth_key'] = '<HIDDEN>'
    assert response.status_code == 200
    assert response.json["model"]["embedding"] == "UniversalSentenceEncoder"

    # check all models (GET /models/)
    qa_data_hybrid['model']['_id'] = 'test'
    response = test_training.get('/training/models?key=test')
    assert response.status_code == 200

    # test wrong id
    response = test_training.get('/training/models/nope?key=test')
    assert response.status_code == 404

    # test wrong auth key
    response = test_training.get('/training/models/test?key=nope')
    assert response.status_code == 404


def test_query(test_app, similar_query, ood_query):
    """ Test query endpoint """

    # test n > 1
    response = test_app.post('/query/test?key=test&query=' + similar_query['q'] + '&n=2&use_threshold=false')
    response = response.json
    assert len(response) == 1

    response = test_app.post('/query/multi_model?key=test',
                             json={"_ids": ["test"], "query": similar_query["q"], "denied_answers": [],
                                   "allowed_answers": [],
                                   "use_threshold": True,
                                   "n": 0})
    response = response.json
    assert len(response) == 2
    assert "confidence" in response[0]


    response = test_app.post('/query/multi_model?key=test',
                             json={"_ids": ["test"], "query": ood_query["q"], "denied_answers": [],
                                   "allowed_answers": [],
                                   "use_threshold": True,
                                   "n": 0})
    response = response.json
    assert len(response) == 1
    assert response[0]["answer"] == ood_query["a"]

    # test wrong id
    response = test_app.post('/query/nope?key=test&query=' + similar_query['q'])
    assert response.status_code == 404

    # test wrong auth key
    response = test_app.post('/query/test?key=nope&query=' + similar_query['q'])
    assert response.status_code == 404


def test_version(test_app):
    """ Test app version """

    response = test_app.get('/version?key=test')
    assert response.status_code == 200
    assert CONFIG["version"] == response.json.get('version')


def test_update_delete_model(test_training, qa_data_hybrid):
    """ Test update and delete QA model """

    # update QA model with new data
    qa_data_hybrid['qa'] = {'id1': {'questions': ['new question'], 'answer': 'new answer'}}
    response = test_training.put('/training/models/test?key=test', data=json.dumps(qa_data_hybrid),
                                 content_type='application/json')
    assert response.status_code == 200
    updated = MM.get_model('test', auth_key='test').data
    # del updated['lastModified']
    # assert qa_data_hybrid == updated

    # test wrong id
    response = test_training.put('/training/models/nope?key=test', data=json.dumps(qa_data_hybrid),
                                 content_type='application/json')
    assert response.status_code == 404

    # test wrong auth key
    response = test_training.put('/training/models/test?key=nope', data=json.dumps(qa_data_hybrid),
                                 content_type='application/json')
    assert response.status_code == 404

    # delete model
    response = test_training.delete('/training/models/test?key=test')
    assert response.status_code == 200
    with pytest.raises(ModelNotFoundError):
        MM.get_model('test', auth_key='test')

    # test wrong id
    response = test_training.delete('/training/models/nope?key=test')
    assert response.status_code == 404

    # test wrong auth key
    response = test_training.delete('/training/models/test?key=nope')
    assert response.status_code == 404
