# -*- coding: utf-8 -*-
"""
    tests.test_utils
    ~~~~~~~~~~~~~~~~

    Test utility functions.

    @author: tomas.brich@seznam.cz
"""

from common.utils.nlp import tokenize, get_subwords
from common.twitter_tokenizer import TweetTokenizer

tokenizer = TweetTokenizer()


def test_tokenize():
    """ Test tokenize """

    sentence = 'Prague is the capital of Czechia (i.e. Czech Republic).'
    result = tokenize(tokenizer, sentence, to_lower=True)
    assert 'prague is the capital of czechia ( i . e . czech republic ) .' == result


def test_subwords():
    """ Test sub-word decomposition """

    word = 'window'
    sw = get_subwords(word, nmin=3, nmax=4)

    assert {'<wi', 'win', 'ind', 'ndo', 'dow', 'ow>',
            '<win', 'wind', 'indo', 'ndow', 'dow>'} == set(sw)
