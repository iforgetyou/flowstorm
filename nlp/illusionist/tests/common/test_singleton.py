# -*- coding: utf-8 -*-
"""
    tests.test_singleton
    ~~~~~~~~~~~~~~~~~~~~

    Test singleton module.
"""

from common.singleton import Singleton


def test_singleton():
    """ Test singleton without params """

    class TestSingleton(metaclass=Singleton):
        pass

    c1 = TestSingleton()
    c2 = TestSingleton()

    assert id(c1) == id(c2)
