# -*- coding: utf-8 -*-
"""
    tests.embeddings.test_base
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Test embeddings base module.

    @author: tomas.brich@seznam.cz
"""

import pytest

from common.blocks.embeddings import base


def test_embedding_base():
    """ Test EmbeddingBase is abstract """

    with pytest.raises(TypeError):
        base.EmbeddingBase()
