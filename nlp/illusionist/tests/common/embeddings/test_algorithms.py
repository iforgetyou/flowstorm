# -*- coding: utf-8 -*-
"""
    tests.embeddings.algorithms
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Test embedding algorithms.

    @author: tomas.brich@seznam.cz
"""

import numpy as np

from common.blocks.embeddings import EmbeddingFactory

EM = EmbeddingFactory()
TESTED = [EM.get_model('FastText', 'en'),
          EM.get_model('FastTextSW', 'en'),
          EM.get_model('Sent2Vec', 'en')]


def test_word_equality():
    """ Test that two same words have similarity ~ 1 """

    for model in TESTED:
        sim = np.dot(model.vector('car'), model.vector('car'))
        assert 0.99 < sim, model.alg


def test_sentence_similarity():
    """ Test sentence similarity """

    for model in TESTED:
        v1 = model.vector('A girl is playing the guitar.')
        v2 = model.vector('A girl is listening to a guitar.')
        sim = np.dot(v1, v2)
        assert 0.5 < sim, model.alg


def test_oov():
    """ Test the ability to create OOV vectors """

    for model in TESTED:
        # test that non-zero vector is created
        sim = np.dot(model.vector('ThisIsOOVWord'), model.vector('ThisIsAnotherOOVWord'))
        assert 0.0001 < abs(sim), model.alg

        # test that the same vector is created for same OOV words
        sim = np.dot(model.vector('ThisIsOOVWord'), model.vector('ThisIsOOVWord'))
        assert 0.99 < sim, model.alg
