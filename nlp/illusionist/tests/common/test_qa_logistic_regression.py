# -*- coding: utf-8 -*-
"""
    tests.test_qa
    ~~~~~~~~~~~~~

    Test question-answering module.
"""

import numpy as np

from common.blocks.classification.sentence.logistic_regression import LogisticRegression

QS = LogisticRegression(metadata={})


def test_initialize(qa_data_logistic_regression):
    """
    Test QuestionSimilarity model initialization.
    Required for the following tests!
    """

    assert QS.X is None

    q_len = 0
    for qa in qa_data_logistic_regression['qa'].values():
        q_len += len(qa['questions'])

    QS.initialize(qa_data_logistic_regression)
    assert (q_len, QS.emb.dim) == QS.X.shape
    assert qa_data_logistic_regression == QS.data


def test_get_query(qa_data_logistic_regression, similar_query):
    """ Test answer for queries (QuestionSimilarity.get) """

    # test exact match
    for qa in qa_data_logistic_regression['qa'].values():
        response = QS.get(qa['questions'][0])[0]
        assert qa['answer'] == response['answer']
        assert 0.99 < response['confidence']

    # test similarity
    response = QS.get(similar_query['q'])[0]
    assert similar_query['a'] == response['answer']
    assert 0.5 < response['confidence']

    # test n > 1
    data_len = len(QS.data["qa"])
    response = QS.get(similar_query['q'], n=3, use_threshold=False)
    assert data_len if data_len < 3 else 3 == len(response)


def test_threshold(similar_query):
    data_len = len(QS.data["qa"])
    response = QS.get(similar_query['q'], n=5, use_threshold=False)
    assert data_len if data_len < 5 else 5 == len(response)
    response = QS.get(similar_query['q'], n=5, use_threshold=True)
    assert 1 == len(response)


def test_tfidf_model(qa_data_logistic_regression, similar_query):
    """ Test TF-IDF model """

    qa_data_logistic_regression['model']['use_tfidf'] = True
    model = LogisticRegression(qa_data=qa_data_logistic_regression, metadata={})

    query = similar_query['q']
    vec1 = model.emb.vector(query)
    vec2 = model.tfidf_vector(query)
    assert not np.array_equal(vec1, vec2)

    sample = 'This is just a simple test.'
    tokens = model.tokenize(sample)
    assert ['this', 'is', 'just', 'a', 'simple', 'test', '.'] == tokens
