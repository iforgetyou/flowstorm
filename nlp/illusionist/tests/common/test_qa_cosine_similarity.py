# -*- coding: utf-8 -*-
"""
    tests.test_qa
    ~~~~~~~~~~~~~

    Test question-answering module.
"""

import numpy as np

from common.blocks.classification.sentence.cosine_similarity import CosineSimilarity

QS = CosineSimilarity(metadata={})


def test_initialize(qa_data_cosine_similarity):
    """
    Test QuestionSimilarity model initialization.
    Required for the following tests!
    """

    assert QS.X is None

    q_len = 0
    for qa in qa_data_cosine_similarity['qa'].values():
        q_len += len(qa['questions'])

    QS.initialize(qa_data_cosine_similarity)
    assert (q_len, QS.emb.dim) == QS.X.shape
    assert qa_data_cosine_similarity == QS.data


def test_get_query(qa_data_cosine_similarity, similar_query):
    """ Test answer for queries (QuestionSimilarity.get) """

    # test exact match
    for qa in qa_data_cosine_similarity['qa'].values():
        response = QS.get(QS.get_vector(qa['questions'][0]))[0]
        assert qa['answer'] == response['answer']
        assert qa['questions'][0].lower() == response['hit']
        assert 0.99 < response['similarity']

    # test similarity
    response = QS.get(QS.get_vector(similar_query['q']))[0]
    assert similar_query['a'] == response['answer']
    assert 0.5 < response['similarity']

    # test n > 1
    data_len = len(QS.data['qa'])
    response = QS.get(QS.get_vector(similar_query['q']), n=3, use_threshold=False)
    assert data_len if data_len < 3 else 3 == len(response)


def test_threshold(similar_query):
    data_len = len(QS.data['qa'])
    response = QS.get(QS.get_vector(similar_query['q']), n=5, use_threshold=False)
    assert data_len if data_len < 5 else 5 == len(response)
    response = QS.get(QS.get_vector(similar_query['q']), n=5, use_threshold=True)
    assert 1 == len(response)


def test_tfidf_model(qa_data_cosine_similarity, similar_query):
    """ Test TF-IDF model """

    qa_data_cosine_similarity['model']['use_tfidf'] = True
    model = CosineSimilarity(qa_data=qa_data_cosine_similarity, metadata={})

    query = similar_query['q']
    vec1 = model.emb.vector(query)
    vec2 = model.tfidf_vector(query)
    assert not np.array_equal(vec1, vec2)

    sample = 'This is just a simple test.'
    tokens = model.tokenize(sample)
    assert ['this', 'is', 'just', 'a', 'simple', 'test', '.'] == tokens
