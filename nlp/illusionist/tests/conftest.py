# -*- coding: utf-8 -*-
"""
    tests.conftest
    ~~~~~~~~~~~~~~

    PyTest configuration.
"""

import pytest

from common.init import create_app


@pytest.fixture(scope='module')
def test_app():
    """ Test client app """

    app = create_app('app')
    with app.app.test_client() as c:
        yield c


@pytest.fixture(scope='module')
def test_training():
    """ Test client training """

    app = create_app('training')
    with app.app.test_client() as c:
        yield c


@pytest.fixture
def similar_query():
    """ Query similar to a question in data """

    yield {
        'q': 'What is the very first answer?',
        'a': 'This is the first answer.'
    }


@pytest.fixture
def ood_query():
    """ Query similar to a question in data """

    yield {
        'q': 'I like books',
        'a': 'outofdomain'
    }


@pytest.fixture
def qa_data_hybrid():
    """ Test QA data """

    yield {
        '_id': 'test',
        'auth_key': 'test',
        'model': {
            'name': 'test',
            'algorithm': 'UniversalSentenceEncoder',
            'lang': 'en',
            'use_tfidf': False,
            'approach': "hybrid"
        },
        'qa': {
            'id1': {
                'questions': [
                    'What is a answer?',
                    'What\'s the first answer?',
                    'Are you sure?',
                    'Do you really think so?'
                ],
                'answer': 'This is the first answer.',
                'threshold': 0.8
            },
            'id2': {
                'questions': [
                    'Can you tell me something else?',
                    'Tell me more?',
                    "More information?"
                ],
                'answer': 'Something completely different.',
                'threshold': 0.8
            }
        }
    }


@pytest.fixture
def qa_data_logistic_regression():
    """ Test QA data """

    yield {
        '_id': 'test',
        'auth_key': 'test',
        'model': {
            'name': 'test',
            'algorithm': 'FastTextSW',
            'lang': 'en',
            'use_tfidf': False,
            'approach': "logistic"
        },
        'qa': {
            'id1': {
                'questions': [
                    'What is a answer?',
                    'What\'s the first answer?',
                    'Are you sure?',
                    'Do you really think so?'
                ],
                'answer': 'This is the first answer.',
                'threshold': 0.8
            },
            'id2': {
                'questions': [
                    'Can you tell me something else?',
                    'Tell me more?',
                    "More information?"
                ],
                'answer': 'Something completely different.',
                'threshold': 0.8
            }
        }
    }


@pytest.fixture
def qa_data_cosine_similarity():
    """ Test QA data """

    yield {
        '_id': 'test',
        'auth_key': 'test',
        'model': {
            'name': 'test',
            'algorithm': 'FastTextSW',
            'lang': 'en',
            'use_tfidf': False,
            'approach': "hybrid"
        },
        'qa': {
            'id1': {
                'questions': [
                    'What is a answer?',
                    'What\'s the first answer?',
                    'Are you sure?',
                    'Do you really think so?'
                ],
                'answer': 'This is the first answer.',
                'threshold': 0.8
            },
            'id2': {
                'questions': [
                    'Can you tell me something else?',
                    'Tell me more?',
                    "More information?"
                ],
                'answer': 'Something completely different.',
                'threshold': 0.8
            }
        }
    }


@pytest.fixture
def model_manager_data():
    """ Test QA data """

    yield {"local_model": {
        '_id': 'local_model',
        'auth_key': 'test',
        "model": {
            "name": "test-mode",
            "lang": "en",
            "algorithm": "FastTextSW",
            "approach": "logistic",
            "use_tfidf": False
        },
        "qa": {
            "Intent4": {
                "questions": [
                    "yes",
                    "ok",
                    "sure",
                    "continue"
                ],
                "answer": "-4",
                "threshold": 0.0
            },
            "Intent5": {
                "questions": [
                    "no",
                    "bad idea",
                    "do not talk anymore",
                    "no way"
                ],
                "answer": "-5",
                "threshold": 0.0
            },
            "Intent6": {
                "questions": [
                    "i like your mum",
                    "i like you"
                ],
                "answer": "-6",
                "threshold": 0.0
            },
            "Intent7": {
                "questions": [
                    "i don't know"
                ],
                "answer": "-7",
                "threshold": 0.0
            }
        }
    }, "global_model": {
        '_id': 'global_model',
        'auth_key': 'test',
        "model": {
            "name": "test-mode",
            "lang": "en",
            "algorithm": "FastTextSW",
            "approach": "logistic",
            "use_tfidf": False
        },
        "qa": {
            "Intent8": {
                "questions": [
                    "stop",
                ],
                "answer": "-8",
                "threshold": 0.0
            },
            "Intent9": {
                "questions": [
                    "volume up",
                    "i can't hear you"
                ],
                "answer": "-9",
                "threshold": 0.0
            },
            "Intent610": {
                "questions": [
                    "please repeat",
                    "repeat that",
                    "sorry, i didn't understand"
                ],
                "answer": "-10",
                "threshold": 0.0
            },
        }
    },
        "query1": {
            "query": "i do not know",
            "_ids": [
                "local_model",
                "global_model"
            ],
            "denied_answers": [],
            "allowed_answers": [],
            "use_threshold": True,
            "n": 0
        },
        "query2": {
            "query": "what the weather?",
            "_ids": [
                "local_model",
                "global_model"
            ],
            "denied_answers": [],
            "allowed_answers": [],
            "use_threshold": True,
            "n": 0
        }
    }
