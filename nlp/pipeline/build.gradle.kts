group = "ai.flowstorm.nlp"
description = "Flowstorm NLP Pipeline"

val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-common-app"))
    api(project(":flowstorm-core-lib"))
    api(project(":flowstorm-core-api"))
    api(project(":flowstorm-nlp-dm"))
    api(project(":flowstorm-nlp-stt-api"))
    api(project(":flowstorm-nlp-tts-api"))
    implementation(project(":flowstorm-nlp-profanity"))
    implementation(project(":flowstorm-storage-local"))
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
}
