package ai.flowstorm.nlp.pipeline

import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.common.monitoring.performance.measure
import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.common.monitoring.performance.PerformanceTransaction
import ai.flowstorm.concurrent.call
import ai.flowstorm.concurrent.waitUntil
import ai.flowstorm.core.*
import ai.flowstorm.core.model.*
import ai.flowstorm.core.model.metrics.Metric
import ai.flowstorm.core.nlp.pipeline.*
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.pipeline.StreamInput
import ai.flowstorm.core.nlp.stt.SttStreamFactory
import ai.flowstorm.core.nlp.stt.SttStream
import ai.flowstorm.core.repository.DialogueEventRepository
import ai.flowstorm.core.repository.ProfileRepository
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.core.type.beautify
import ai.flowstorm.core.type.toLocation
import ai.flowstorm.io.WritableStream
import ai.flowstorm.nlp.profanity.ProfanityFilter
import ai.flowstorm.nlp.stt.AnySttStreamFactory
import ai.flowstorm.nlp.tts.TtsProcessor
import ai.flowstorm.time.currentTime
import ai.flowstorm.time.zoneId
import ai.flowstorm.util.*
import kotlinx.datetime.Clock
import org.glassfish.hk2.api.ServiceLocator
import org.litote.kmongo.toId
import javax.inject.Inject
import kotlin.concurrent.thread

class PipelineScopeV2(
    private val pipeline: Pipeline,
    private val pipelineConfig: PipelineConfig,
    private val pipelineHandler: PipelineHandler
) : PipelineScope {

    class PipelineComponentThread(val component: PipelineComponent, val thread: Thread)

    inner class PipelineSttCallback(private val span: PerformanceSpan) : SttCallback {

        private var textRecognized = 0
        private var recognitionSpan: PerformanceSpan? = null

        override fun onRecognized(stream: SttStream, input: Input, isFinal: Boolean) {

            streamInput.allRecognizedInputs += input
            recognitionSpan?.apply {
                tags.putAll(mutableMapOf(
                    "text" to input.transcript.text,
                    "isFinal" to isFinal.toString()
                ))
                finish()
            }
            if (isFinal) {
                with(streamInput) {
                    alternatives.addAll(input.alternatives)
                    log.logger.info("${stream.streamName} recognized final text \"${input.transcript.text}\", alternatives${
                        alternatives.groupBy { it.provider }.map { "${it.key}:${it.value.size}" }} = $alternatives")
                }
                if (pipelineConfig.sttMode == SttMode.SingleUtterance) {
                    closeStream(stream)
                }
            } else {
                recognitionSpan = span.createChild("recognition")
                recognitionSpan?.start()
                if (textRecognized == 0) {
                    log.logger.info("${stream.streamName} recognized first text \"${input.transcript.text}\"")
                    textRecognized++
                }
            }
            if (input.transcript.provider == sttStreamConfig.sttConfig.provider && sttStreamConfig.locale.similar(input.locale)) {
                // result from requested provider and language
                streamInput.updatedTime = currentTime()
                streamInput.textRecognized++
                if (isFinal) {
                    if (pipelineConfig.sttMode == SttMode.SingleUtterance) {
                        // wait until all STT streams are closed
                        waitUntil(5000) { writableStreams.filterIsInstance<SttStream>().isEmpty() }
                    }
                    pipelineHandler.onRecognizedInput(input, true)
                    if (!isProcessingInput) {
                        streamInput.transcript = input.transcript
                        streamInput.locale = input.locale
                        streamInput.tokens = input.tokens
                        process(streamInput, streamInput.performanceTransaction.trace)
                    } else {
                        log.logger.warn("${stream.streamName} cannot process final stream input because input is already being processed")
                    }
                } else {
                    pipelineHandler.onRecognizedInput(input, false)
                }
            }
        }

        override fun onOpen(stream: SttStream) {
            log.logger.info("${stream.streamName} open")
            span.start()
        }

        override fun onClosed(stream: SttStream) {
            log.logger.info("${stream.streamName} closed")
            span.finish()
        }

        override fun onError(stream: SttStream, error: Throwable) {
            processError(stream, streamLogs.joinToString("\n"), error)
            recognitionSpan?.abort()
            span.abort(error)
            //pipelineHandler.onRecognitionError(error, stream.streamName)
        }
    }

    @Inject
    private lateinit var dialogueManager: DialogueManager

    @Inject
    private lateinit var profanityFilter: ProfanityFilter

    @Inject
    private lateinit var dialogueEventRepository: DialogueEventRepository

    @Inject
    private lateinit var sttStreamFactory: AnySttStreamFactory

    @Inject
    private lateinit var ttsProcessor: TtsProcessor

    @Inject
    private lateinit var profileRepository: ProfileRepository

    @Inject
    private lateinit var sessionFactory: SessionFactory

    @Inject
    private lateinit var contextPersister: ContextPersister

    @Inject
    private lateinit var monitoring: Monitoring

    @Inject
    private lateinit var di: ServiceLocator

    override val log = RunLog(id = pipelineConfig.sessionId)
    override val config = pipelineConfig
    override val session by lazy {
        measure({ result, time ->
            log.logger.info("Session loading ${result.name.lowercase()} in $time ms")
        }) {
            sessionFactory.initSession(pipelineConfig, turnNodeId)
        }
    }
    override val profile by lazy {
        measure({ result, time ->
            log.logger.info("Profile loading ${result.name.lowercase()} in $time ms")
        }) {
            profileRepository.findBy(session.user._id, session.space_id)
                ?: Profile(user_id = session.user._id, space_id = session.space_id).also {
                    profileRepository.create(it)
                }
        }
    }
    override val locale: Locale get() =
        dialogueManager.getPropertyFromDialogueStack(session, log.logger) { it.locale }?.let { locale ->
            if (pipelineConfig.locale.language == locale.language && locale.country == "")
                pipelineConfig.locale
            else
                locale
        } ?: pipelineConfig.locale
    override var isProcessingInput = false
    override val isStreamingInput get() = writableStreams/*.filterIsInstance<SttStream>()*/.isNotEmpty()
    override lateinit var turn: Turn
    override lateinit var context: Context
    override lateinit var streamInput: StreamInput
    private lateinit var sttStreamConfig: SttStreamConfig
    private var streamLogs = mutableListOf<String>()
    private var turnNodeId: Int? = null
    private val currentSttStreamConfig: SttStreamConfig get() {
        val sttConfig = userInput?.sttConfig
            ?: dialogueManager.getPropertyFromDialogueStack(session, log.logger) { it.sttConfig }
            ?: SttConfig()
        return SttStreamConfig(locale, pipelineConfig.sttSampleRate, pipelineConfig.sttEncoding, pipelineConfig.sttMode, pipelineConfig.sttModel, sttConfig)
    }
    private val userInput get() = dialogueManager.getUserInput(session, log.logger)
    private val componentThreads = mutableListOf<PipelineComponentThread>()
    private val writableStreams = mutableListOf<WritableStream>()
    private lateinit var performanceTransaction: PerformanceTransaction

    override fun closeStreams(type: PipelineStreamType) {
        writableStreams.filter {
            (type == PipelineStreamType.Any) ||
                    (type == PipelineStreamType.Stt && it is SttStream)
        }.forEach { stream ->
            closeStream(stream)
        }
    }

    private fun closeStream(stream: WritableStream) {
        writableStreams.remove(stream)
        log.logger.info("${stream.streamName} removed, ${writableStreams.size} stream(s) left")
        stream.close()
    }

    override fun close() {
        closeStreams()
    }

    private fun newStreamInput(trace: String?) {
        streamLogs.clear()
        streamInput = StreamInput(createPerformanceTransaction("PipelineV2.stream", "stream")).also {
            it.locale = locale
            it.zoneId = zoneId(pipelineConfig.timeZone)
            if (::streamInput.isInitialized && streamInput.updatedTime > it.updatedTime)
                it.updatedTime = streamInput.updatedTime
            it.performanceTransaction.start(trace)
        }
        log.logger.info("New stream input(id=${streamInput.id})")
    }

    override fun openStream(type: PipelineStreamType, trace: String?) {
        newStreamInput(trace)
        pipeline.openStream.process(this, type)
    }

    override fun write(data: ByteArray, offset: Int, count: Int) {
        val maxSilence =
            if (::sttStreamConfig.isInitialized) sttStreamConfig.sttConfig.maxSilence.let {
                if (it > 1000)
                    it
                else
                    it * 1000
            } else 5000
        val currentTime = currentTime()
        if (!isProcessingInput && streamInput.updatedTime + maxSilence < currentTime) {
            // silence timeout has occurred yet still not the final input received
            if (pipelineConfig.sttMode == SttMode.SingleUtterance) {
                closeStreams(PipelineStreamType.Stt)
            }
            val bestRecognizedInput = streamInput.getBestRecognizedInput()
            if (bestRecognizedInput != null && !streamInput.isTranscriptInitialized()) {
                // in case we have any interim alternative transcript we are going to use the most confident one as the final
                streamInput.transcript = bestRecognizedInput.transcript
                streamInput.locale = bestRecognizedInput.locale
                streamInput.tokens = bestRecognizedInput.tokens
                pipelineHandler.onRecognizedInput(streamInput, true)
                log.logger.warn("Using best interim recognized input as final after ${currentTime - streamInput.updatedTime} ms of silence")
                process(streamInput, streamInput.performanceTransaction.trace)
            } else if (streamInput.silence()) { // input transcript set to #silence action
                pipelineHandler.onRecognizedInput(streamInput, true)
                log.logger.info("Processing silence after ${currentTime - streamInput.updatedTime} ms")
                process(streamInput, streamInput.performanceTransaction.trace)
            }
        } else {
            streamInput.byteCount += count
            val nominalSpeed =
                if (::sttStreamConfig.isInitialized)
                    sttStreamConfig.sampleRate * (sttStreamConfig.encoding.sampleSizeInBits / 8) / 1024.0
                else null
            val averageSpeed = streamInput.byteCount / ((currentTime() - streamInput.startTime) / 1000.0) / 1024
            val streamLog = "Writing $count bytes to stream(s) with average speed ${"%.2f".format(averageSpeed)} KB/sec = ${if (nominalSpeed != null) "%.2f".format(averageSpeed / nominalSpeed * 100) else "-"}% of nominal speed ${"%.2f".format(nominalSpeed)} KB/sec"
            streamLogs += "${Clock.System.now()} $streamLog"
            if (nominalSpeed == null || averageSpeed / nominalSpeed < 0.8)
                log.logger.warn(streamLog)
            writableStreams.toList().forEach {
                it.write(data, offset, count)
            }
        }
    }

    private fun createPerformanceTransaction(name: String, operation: String, extra: Map<String, String> = mapOf()) =
        monitoring.createPerformanceTransaction(
            name,
            operation,
            session.user.toMonitoringUser(),
            session.toTags {
                it.putAll(extra)
            }
        )

    private val Input.streamPerformanceTransaction get() = if (this is StreamInput)
        performanceTransaction
    else
        null

    override fun process(input: Input, trace: String?) {
        if (isProcessingInput) {
            processError(this, null, IllegalStateException("Input is still being processed"))
        } else {
            val tags = mutableMapOf(
                "transcript.text" to input.transcript.text,
                "transcript.confidence" to input.transcript.confidence.toString(),
                "transcript.provider" to (input.transcript.provider ?: "Unknown")
            )
            turnNodeId = input.attributes["turnNodeId"] as Int?
            performanceTransaction = createPerformanceTransaction("PipelineV2.process", "process", tags)
            performanceTransaction.measure(trace) {
                val startTime = currentTime()
                try {
                    isProcessingInput = true
                    log.logger.info(input.toString())
                    performanceTransaction.createChild("get:Context").measure({ result, time ->
                        log.logger.info("Context getting ${result.name.lowercase()} in $time ms")
                    }) {
                        updateAutomaticMetrics(session)
                        context = di.getService(Context::class.java)
                        context.pipelineScope = this
                        context.performanceSpan = performanceTransaction
                        context.performanceTrace = trace
                        val dialogueId = session.dialogueStack.peek()?.id?.toId() ?: session.application.dialogue_id
                        turn = Turn(input.id, session.user._id, session._id, dialogueId, session.application._id, locale, input)
                        performanceTransaction.tags["turn_id"] = turn._id.toString()
                        input.streamPerformanceTransaction?.tags?.put("turn_id", turn._id.toString())
                        input.processAttributes()
                    }
                    process(dialogueManager)
                    performanceTransaction.createChild("persist:Context").measure({ result, time ->
                        log.logger.info("Context persistence ${result.name.lowercase()} in $time ms")
                    }) {
                        contextPersister.persist(context)
                    }
                } catch (error: Throwable) {
                    input.streamPerformanceTransaction?.abort(error)
                    processError(this, null, error, true)
                } finally {
                    input.streamPerformanceTransaction?.finish()
                    log.logger.info(
                        (if (input is StreamInput) "Stream input recognized after ${startTime - streamInput.createdTime} ms," else "Input") +
                                " processed in ${currentTime() - startTime} ms"
                    )
                    if (pipelineConfig.sttMode == SttMode.Continuous) {
                        if (currentSttStreamConfig != sttStreamConfig) {
                            log.logger.info("Reopening STT streams due to config change")
                            closeStreams(PipelineStreamType.Stt)
                            openStream(PipelineStreamType.Stt)
                        } else {
                            newStreamInput(null)
                        }
                    } else {
                        closeStreams()
                    }
                    pipelineHandler.onResponseDone(log.entries, session.isEnded, context.sleepTimeout, input.id.toString())
                    log.entries.clear()
                    isProcessingInput = false
                }
            }
        }
    }

    override fun processInput(reInput: Boolean) {
        if (reInput) {
            context.input.action = null
            context.input.classes.removeIf { it.type == Input.Class.Type.Intent }
            pipeline.processReInput
        } else {
            pipeline.processInput
        }.process(this, performanceTransaction)
        wait()
    }

    override fun process(component: PipelineComponent) {
        try {
            val span = performanceTransaction.createChild(component::class.simpleName ?: "Unknown")
            span.measure({ result, time ->
                log.logger.info("${component::class.simpleName} ${result.name.lowercase()} in $time ms")
            }) {
                call {
                    component.process(this, span)
                }
            }
        } catch (error: Throwable) {
            processError(component, null, error)
        }
    }

    private fun processError(component: Any, text: String?, error: Throwable, isFinal: Boolean = false) {
        val message = "${component::class.simpleName} failed: ${error.message}"
        error.printStackTrace()
        log.logger.error(message, error)
        monitoring.capture(text, error, session.toTags {
            if (::turn.isInitialized)
                it["turn_id"] = turn._id.toString()
            it["component"] = component::class.qualifiedName ?: "?"
        })
        if (::context.isInitialized) {
            context.errors += component to error
        }
        if (isFinal)
            isProcessingInput = false
        if (!isFinal)
            pipelineHandler.onProcessError(component, error)
    }

    override fun launch(component: PipelineComponent) {
        componentThreads += PipelineComponentThread(component, thread { process(component) })
    }

    override fun wait(vararg components: PipelineComponent) {
        val threads = if (components.isEmpty())
            componentThreads
        else
            componentThreads.filter { components.contains(it.component) }
        threads.forEach { it.thread.join() }
    }

    override fun stream(stream: WritableStream) {
        writableStreams += stream
        log.logger.info("$stream added, ${writableStreams.size} stream(s) in total")
    }

    override fun stream(streamService: PipelineStreamService) = stream(streamService.createStream(this))

    override fun sttStream(sttServiceFactory: SttStreamFactory?, locale: Locale?) {
        sttStreamConfig = currentSttStreamConfig
        val sttStream = (sttServiceFactory ?: sttStreamFactory).create(
            if (locale != null)
                sttStreamConfig.copy(locale = locale)
            else
                sttStreamConfig,
            PipelineSttCallback(streamInput.performanceTransaction.createChild("sttStream:${sttStreamConfig.sttConfig.provider}")),
            (userInput?.let { userInput ->
                mutableListOf<ExpectedPhrase>().apply {
                    addAll(dialogueManager.getExpectedPhrases(userInput.intents))
                    addAll(userInput.expectedPhrases)
                }
            } ?: listOf()).also { expectedPhrases ->
                log.logger.info("Using ${expectedPhrases.size} expected phrases: ${expectedPhrases.take(10).map { it.text}.joinToString(", ")}...${expectedPhrases.takeLast(10).map { it.text}.joinToString(", ")}")
            }
        )
        stream(sttStream)
    }

    override fun ttsProcess(item: Response.Item) =
        if (item.audio == null && (!item.text.isNullOrBlank() || !item.ssml.isNullOrBlank()) && item.text?.startsWith('#') != true) {
            performanceTransaction.createChild("ttsProcess:${item.ttsConfig?.provider ?: "Unknown"}", mutableMapOf(
                "item.text" to item.text(),
                "item.nodeId" to item.nodeId.toString()
            )).measure {
                ttsProcessor.process(context, log.logger, item)
            }
        } else listOf(item)

    override fun createDialogueEvent(e: Throwable) = context.run {
        dialogueEvent = DialogueEvent(
            datetime = Clock.System.now(),
            type = DialogueEvent.Type.ServerError,
            user = user,
            sessionId = session.sessionId,
            properties = session.properties,
            applicationName = application.name,
            dialogue_id = application.dialogue_id,
            //TODO Replace with actual node ID after node sequence is added in Context
            nodeId = 0,
            text = DialogueEvent.toText(e),
            space_id = session.space_id,
        ).also {
            dialogueEventRepository.create(it)
            contextPersister.indexer.save(it)
        }
    }

    override fun addResponseItem(item: Response.Item) {
        var itemText = if (item.text?.startsWith('#') == true) item.text else item.text?.let {
            it.beautify()
        }
        //val item = Response.Item(plainText, if (text != plainText) text else null, 1.0, image, audio, video, code, background, ttsConfig, repeatable)
        var itemImage: String? = item.image
        var itemVideo: String? = item.video
        var itemSsml = if (item.text != itemText) item.text else null
        itemSsml = itemSsml?.replace(Regex("<image.*?src=\"(.*?)\"[^\\>]+>")) {
            itemImage = it.groupValues[1]
            ""
        }
        itemSsml = itemSsml?.replace(Regex("<video.*?src=\"(.*?)\"[^\\>]+>")) {
            itemVideo = it.groupValues[1]
            ""
        }
        if (itemText != null && profanityFilter.isProfane(itemText)) {
            // Handle profanities here
            //itemText = profanityFilter.bleepProfanities(itemText)
            //itemSsml = null
        }
        item.copy(text = itemText, ssml = itemSsml, confidence = item.confidence, image = itemImage, video = itemVideo).let { item ->
            context.turn.responseItems.add(item)
            pipeline.processResponseItem.process(this, item).forEach {
                pipelineHandler.onResponseItem(it)
            }
        }
    }

    override fun addClientLogEntries(entries: List<Log.Entry>) {
        session.clientLogEntries.addAll(entries)
        if (session.isEnded || (::context.isInitialized && context.errors.isNotEmpty())) {
            try {
                contextPersister.persist(session)
            } catch (e: Throwable) {
                log.logger.error("Cannot persist client log entries to session", e)
            }
        }
    }

    private fun updateAutomaticMetrics(session: Session) {
        with(session.metrics) {
            find { it.name == "count" && it.namespace == "session" }
                ?: add(Metric("session", "count", 1))
            find { it.name == "turns" && it.namespace == "session" }?.increment()
                ?: add(Metric("session", "turns", 1))
        }
    }

    private fun Input.processAttributes() {
        attributes.forEach {
            val name = if (it.key == "clientLocation")
                "clientUserLocation"
            else
                it.key
            val value = when (name) {
                "clientUserLocation" ->
                    (it.value as String).toLocation().apply {
                        session.location = this
                    }
                "clientTemperature", "clientAmbientLight", "clientSpatialMotion" ->
                    it.value.toString().toDouble() // can be integer or double
                else -> {
                    if ((it.value is String) && ((it.value as String).startsWith("lat=") || (it.value as String).startsWith("lng=")))
                        (it.value as String).toLocation()
                    else if (it.value is Map<*, *>)
                        Dynamic(it.value as Map<String, Any>)
                    else
                        it.value // type by JSON
                }
            }
            context.getAttributes(name)[Defaults.globalNamespace].set(name, value)
        }
    }
}