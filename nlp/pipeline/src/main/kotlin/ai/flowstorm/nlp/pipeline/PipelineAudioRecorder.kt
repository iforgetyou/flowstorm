package ai.flowstorm.nlp.pipeline

import ai.flowstorm.common.transport.DataType
import ai.flowstorm.core.SttMode
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.core.nlp.pipeline.PipelineStreamService
import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.util.DataConverter
import javax.inject.Inject

class PipelineAudioRecorder : PipelineStreamService {

    @Inject
    lateinit var fileStorage: FileStorage

    override fun createStream(scope: PipelineScope) = with(scope) {
        with(config) {
            val path = "record/${session._id}" + (if (sttMode == SttMode.SingleUtterance) "/${streamInput.id}" else "") + ".wav"
            fileStorage.getWritableStream(path, DataType.WAV.contentType, listOf()).apply {
                val header = DataConverter.wavHeader(317520000, sttSampleRate, 1, sttEncoding.sampleSizeInBits, sttEncoding)
                write(header, 0, header.size)
            }
        }
    }
}
