package ai.flowstorm.nlp

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.Input
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.dialogue.tokenize
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.use

class InternalTokenizer : PipelineComponent {
    companion object {
        const val PUNCTUATION = ",.:;?![]"
    }

    private val logger by LoggerDelegate()

    override fun process(scope: PipelineScope, span: PerformanceSpan) = use(scope) {
        with(context.input) {
            if (tokens.isEmpty()) {
                // tokenization has not been done yet (ASR skipped?)
                tokens.addAll(transcript.text.tokenize(keepPunctuation = true))
            } else {
                tokens = tokens.flatMap { stripPunctuation(it as Input.Word) }.toMutableList()
            }
            logger.info("Tokens $tokens")
            alternatives.add(0, transcript)
        }
    }

    private fun stripPunctuation(token: Input.Word) = when {
        PUNCTUATION.any { token.text.startsWith(it) } -> {
            listOf(Input.Punctuation(token.text.substring(0, 1)), token.copy(text=token.text.substring(1)))
        }
        PUNCTUATION.any { token.text.endsWith(it) } -> {
            val length = token.text.length
            listOf(token.copy(text=token.text.substring(0, length-1)), Input.Punctuation(token.text.substring(length-1, length)))
        }
        else -> listOf(token)
    }
}
