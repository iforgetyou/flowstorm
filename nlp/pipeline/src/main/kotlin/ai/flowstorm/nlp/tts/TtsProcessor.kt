package ai.flowstorm.nlp.tts

import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.core.Context
import ai.flowstorm.core.Response
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.core.util.AudioUtil
import ai.flowstorm.nlp.pipeline.toMonitoringUser
import ai.flowstorm.nlp.pipeline.toTags
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.Logger
import org.glassfish.hk2.api.IterableProvider
import java.io.IOException
import javax.inject.Inject
import kotlin.system.measureTimeMillis

class TtsProcessor {

    @ConfigValue("tts.no-cache", "false")
    var ttsNoCache = false

    @Inject
    private lateinit var boundServices: IterableProvider<TtsService>

    @Inject
    private lateinit var fileStorage: FileStorage

    @Inject
    private lateinit var monitoring: Monitoring

    private fun get(name: String) = boundServices.find { it.name == name } ?: error("Unknown TTS service: $name")

    fun process(
        context: Context,
        logger: Logger,
        item: Response.Item,
        speakingRate: Double = 1.0,
        speakingPitch: Double = 0.0,
        speakingVolumeGain: Double = 1.0
    ): List<Response.Item> {
        var ttsConfig = item.ttsConfig ?: TtsConfig.forLanguage("en")
        val items = mutableListOf(item)

        if (ttsConfig.provider == "Amazon") {
            // Amazon Polly synthesis - strip <audio> tag and create audio item
            item.ssml = item.ssml?.replace(Regex("<audio.*?src=\"(.*?)\"[^\\>]+>")) {
                if (item.text!!.isBlank())
                    item.audio = it.groupValues[1]
                else
                    items.add(Response.Item(audio = it.groupValues[1], turnId = item.turnId, nodeId = item.nodeId))
                ""
            }
        }

        // set voice by <voice> tag
        item.ssml = item.ssml?.replace(Regex("<voice.*?name=\"(.*?)\">(.*)</voice>")) {
            val name = it.groupValues[1]
            TtsConfig.Voice.values().find { voice -> name == voice.name || name == voice.config.name } ?.let {
                ttsConfig = it.config
            }
            it.groupValues[2]
        }

        // set style by <voice> tag
        var ttsStyle = ""
        item.ssml = item.ssml?.replace(Regex("<voice.*?style=\"(.*?)\">(.*)</voice>")) {
            ttsStyle = it.groupValues[1]
            it.groupValues[2]
        }

        val request =
            TtsRequest(
                ttsConfig,
                ((if (item.ssml != null) item.ssml else item.text) ?: "").replace(Regex("#(\\w+)")) {
                    // command processing
                    when (it.groupValues[1]) {
                        //TO BE REMOVED version command is now handled by a global intent in Basic Dialogue classes for each specific localization
                        "version" -> {
                            item.text = "Server version ${AppConfig.version}, environment ${AppConfig.namespace}."
                            item.ttsConfig = TtsConfig.Voice.Audrey.config
                            item.text!!
                        }
                        else -> ""
                    }
                },
                item.ssml != null,
                ttsStyle,
                speakingRate = speakingRate,
                speakingPitch = speakingPitch,
                speakingVolumeGain = speakingVolumeGain
            )
        if (request.text.isNotBlank()) {

            val path = process(context, request, context.pipelineScope.config.ttsFileType, logger)

            // set item audio property
            item.audio = ServiceUrlResolver.getEndpointUrl(
                ServiceUrlResolver.Service.core,
                if (ServiceUrlResolver.detectedRunMode != ServiceUrlResolver.RunMode.local)
                    ServiceUrlResolver.RunMode.dist
                else ServiceUrlResolver.RunMode.detect
            ) + "/file/$path"
        }

        return items
    }

    fun process(context: Context?, request: TtsRequest, fileType: AudioFileType, logger: Logger? = null): String {
        val path = "tts/${request.code}.$fileType"
        var name = "TtsCache"
        val time = measureTimeMillis {
            val performTts = ttsNoCache || try {
                val storageFile = fileStorage.getFile(path)
                storageFile.updateTime + 1000L * 60 * 60 * 24 * 30 < currentTime() // has expired
            } catch (e: FileStorage.NotFoundException) {
                true
            }
            if (performTts) {
                if (request.isSsml && !request.text.startsWith("<speak>"))
                    request.text = "<speak>${request.text}</speak>"
                val ttsService = get(request.config.provider)
                name = "${ttsService::class.simpleName}"
                monitoring.createPerformanceTransaction(
                    "TtsProcessor.process:${request.config.provider}",
                    "speak",
                    context?.session?.user?.toMonitoringUser(),
                    (context?.session?.toTags()?: mutableMapOf()).also {
                        with (request) {
                            it["text"] = text
                            it["file.type"] = fileType.name
                            it["file.contentType"] = fileType.contentType
                            it["ttsConfig.locale"] = config.locale.toString()
                            it["ttsConfig.gender"] = config.gender.name
                            it["ttsConfig.name"] = config.name
                            if (config.engine != null)
                                it["ttsConfig.engine"] = config.engine!!
                        }
                    }
                ).measure(context?.performanceTrace) {
                    var data = ttsService.speak(request)
                    if (fileType != AudioFileType.mp3)
                        data = AudioUtil.convert(data, fileType, AudioFileType.mp3, request.code)
                    try {
                        fileStorage.writeFile(
                            path,
                            fileType.contentType,
                            listOf("text:${request.text}"),
                            data.inputStream()
                        )
                    } catch (e: Throwable) {
                        throw IOException(e.message, e)
                    }
                }
            }
        }
        logger?.info("$name processed speech \"${request.text}\" in $time ms (path=$path, request=$request)")
        return path
    }
}