package ai.flowstorm.nlp

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.core.type.DucklingEntity
import org.glassfish.hk2.api.IterableProvider
import java.util.*
import javax.inject.Inject
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Form
import javax.ws.rs.core.GenericType

class Duckling: PipelineComponent {

    @Inject
    lateinit var webTargets: IterableProvider<WebTarget>

    val webTarget: WebTarget get() = webTargets.named("duckling").get()

    override fun process(scope: PipelineScope, span: PerformanceSpan) = with(scope) {
        val response = webTarget.request().post(
                Entity.form(Form()
                        .param("locale", getValidLocale(context.session.locale ?: context.input.locale))
                        .param("tz", context.input.zoneId.id)
                        .param("text", context.input.transcript.text)), object : GenericType<List<DucklingEntity>>() {})
        synchronized(context.turn.input.entityMap) {
            response.forEach { entity ->
                context.turn.input.entityMap.getOrPut(entity.className) { mutableListOf() }.apply {
                    add(entity)
                    sortBy { -it.confidence }
                }
            }
        }
    }

    private fun getValidLocale(locale: Locale): String {
        return when (locale.toLanguageTag()){
            "cs" -> "cs_CZ"
            "en" -> "en_US"
            else -> locale.toLanguageTag()
        }
    }
}
