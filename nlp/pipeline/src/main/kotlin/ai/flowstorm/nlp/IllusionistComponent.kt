package ai.flowstorm.nlp

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import org.glassfish.hk2.api.IterableProvider
import javax.inject.Inject
import javax.ws.rs.client.WebTarget

abstract class IllusionistComponent : PipelineComponent {

    @ConfigValue("illusionist.apiKey")
    lateinit var key: String

    @Inject
    lateinit var webTargets: IterableProvider<WebTarget>

    protected val webTarget: WebTarget get() = webTargets.named("illusionist").get()
}