package ai.flowstorm.nlp.pipeline

import ai.flowstorm.core.nlp.pipeline.*
import org.glassfish.hk2.api.ServiceLocator
import javax.inject.Inject

abstract class PipelineV2 : Pipeline {

    @Inject
    private lateinit var di: ServiceLocator

    override fun createScope(
        pipelineConfig: PipelineConfig,
        pipelineHandler: PipelineHandler
    ) = PipelineScopeV2(this, pipelineConfig, pipelineHandler).also {
        di.inject(it)
    }
}
