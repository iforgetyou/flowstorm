package ai.flowstorm.nlp

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.nlp.pipeline.PipelineScope

class ActionResolver : PipelineComponent {

    override fun process(scope: PipelineScope, span: PerformanceSpan) = with(scope) {
        var text = context.input.transcript.text
        if (text.startsWith('#'))
            context.input.action = text.substring(1)
    }
}
