package ai.flowstorm.nlp.pipeline

import ai.flowstorm.common.monitoring.User as MonitoringUser
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.model.User

fun Session.toTags(block: ((MutableMap<String, String>) -> Unit)? = null): MutableMap<String, String> =
    mutableMapOf(
        "sessionId" to sessionId,
        "application_name" to application.name,
        "dialogue_id" to application.dialogue_id.toString(),
        "client_type" to clientType,
        "session_id" to _id.toString(),
        "user_id" to user._id.toString()
    ).also {
        block?.invoke(it)
    }

fun User.toMonitoringUser() = MonitoringUser(
    _id.toString(),
    username,
    mapOf("name" to name, "surname" to surname)
)