package ai.flowstorm.nlp

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.InputEntity
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.core.type.Memory
import ai.flowstorm.nlp.dm.DialogueFactory
import ai.flowstorm.util.use
import javax.inject.Inject
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

class SimpleNER: PipelineComponent {

    @Inject
    lateinit var dialogueFactory: DialogueFactory

    override fun process(scope: PipelineScope, span: PerformanceSpan) = use(scope) {
        val dialogue = context.currentFrame?.let { dialogueFactory.get(it, context.logger) }
        val property = dialogue?.let { it::class.memberProperties.firstOrNull { it.name == "gazetteer" } as? KProperty1<AbstractDialogue<*>, Map<String, List<String>>> }
        property?.let {
            context.logger.info("Found gazetteer in the dialogue. Processing defined entities.")
            val gazetteer = it.get(dialogue)
            synchronized(context.input.entityMap) {
                entitiesFromList(gazetteer, context.input.transcript.text.lowercase()).forEach { (k, v) ->
                    context.input.entityMap.getOrPut(k) { mutableListOf() }.apply {
                        addAll(v)
                        sortBy { -it.confidence }
                    }
                }
            }
        }
    }

    private fun entitiesFromList(gazetteer: Map<String, List<String>>, input: String) =
        gazetteer.mapValues { (cls, samples) ->
            samples.filter { input.contains(it) }.map { InputEntity(cls, it, 1.0F, "gazetteer") }
        }.filter { (_, samples) -> samples.isNotEmpty() }
}
