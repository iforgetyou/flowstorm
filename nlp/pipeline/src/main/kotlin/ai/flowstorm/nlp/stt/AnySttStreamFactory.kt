package ai.flowstorm.nlp.stt

import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStreamFactory
import org.glassfish.hk2.api.IterableProvider
import javax.inject.Inject

class AnySttStreamFactory : SttStreamFactory {

    @Inject
    private lateinit var sttStreamFactories: IterableProvider<SttStreamFactory>

    override fun create(
        config: SttStreamConfig,
        callback: SttCallback,
        expectedPhrases: List<ExpectedPhrase>
    ) = sttStreamFactories.find {
        it::class.simpleName?.substringBefore("Stt") == config.sttConfig.provider
    }?.create(config, callback, expectedPhrases) ?: error("Unknown STT provider: ${config.sttConfig.provider}")
}