package ai.flowstorm.nlp.pipeline

import ai.flowstorm.common.AppConfig
import ai.flowstorm.core.model.Application
import ai.flowstorm.core.model.DialogueStack
import ai.flowstorm.core.nlp.pipeline.PipelineConfig
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.repository.SessionRepository
import ai.flowstorm.core.resources.ContentDistributionResource
import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.nlp.dm.DialogueFactory
import ai.flowstorm.util.Log
import ai.flowstorm.util.LoggerDelegate
import java.util.*
import javax.inject.Inject

class SessionFactory {

    @Inject
    lateinit var contentDistributionResource: ContentDistributionResource

    @Inject
    lateinit var sessionRepository: SessionRepository

    @Inject
    lateinit var dialogueFactory: DialogueFactory

    private val logger by LoggerDelegate()

    fun initSession(pipelineConfig: PipelineConfig, turnNodeId: Int?): Session = with(pipelineConfig) {
        val storedSession = sessionRepository.findBy(sessionId)
        return if (storedSession != null) {
            logger.info("Session $sessionId restored")
            storedSession
        } else {
            val contentResponse = contentDistributionResource.resolve(
                ContentDistributionResource.ContentRequest(deviceId, token, key, locale.language)
            )
            Session(
                sessionId = sessionId,
                //initiationId = initiationId,
                device = contentResponse.device,
                user = contentResponse.user,
                test = contentResponse.test,
                hasConsent = contentResponse.hasConsent,//TODO for IVRs apps (Alexa, Google) set to true as consumer should read its terms during installation
                application = contentResponse.application,
                locale = contentResponse.locale,
                properties = Dynamic(contentResponse.sessionProperties),
                space_id = contentResponse.space._id,
                dialogueStack = initStack(contentResponse.application, turnNodeId),
            ).also {
                sessionRepository.create(it)
                logger.info("Session $sessionId created")
            }
        }
    }

    fun storeLogEntries(sessionId: String, entries: List<Log.Entry>) {
        sessionRepository.findBy(sessionId)?.let {
            it.clientLogEntries.addAll(entries)
            sessionRepository.update(it)
        }
    }

    private fun initStack(application: Application, turnNodeId: Int?): DialogueStack = with(application) {
        val nodeId = turnNodeId ?: 0
        val buildId = dialogueFactory.getLatestBuildId(dialogue_id.toString(), properties)
        val frame = Session.DialogueStackFrame(dialogue_id.toString(), buildId, properties, nodeId)

        LinkedList(listOfNotNull(frame, getImplicitGlobalsDialogueFrame(this, frame)))
    }

    private fun getImplicitGlobalsDialogueFrame(
        application: Application,
        firstFrame: Session.DialogueStackFrame
    ): Session.DialogueStackFrame? = with(application) {
        val language = dialogueFactory.get(firstFrame, logger).language
        AppConfig.getOrNull("idMap.implicitGlobalsDialogue.$language")?.let {
            try {
                // Dialogue containing implicit global intents and actions
                return Session.DialogueStackFrame(
                    it,
                    dialogueFactory.getLatestBuildId(it, properties),
                    properties,
                    0
                )

            } catch (e: FileStorage.NotFoundException) {
                logger.error("Dialogue with implicit actions and intents for locale '$language' not found.")
            }

        } ?: logger.error("No ID of implicit dialogue specified for locale '$language'.")
        null
    }
}
