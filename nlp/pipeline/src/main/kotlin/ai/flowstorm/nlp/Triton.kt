package ai.flowstorm.nlp

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.model.Sentiment
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.use
import org.glassfish.hk2.api.IterableProvider
import javax.inject.Inject
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType

class Triton : PipelineComponent {

    @Inject
    lateinit var webTargets: IterableProvider<WebTarget>

    private val webTarget: WebTarget get() = webTargets.named("triton").get()

    private val logger by LoggerDelegate()

    private val idx2Sentiment = mapOf(
            0 to Sentiment.NEGATIVE,
            1 to Sentiment.NEUTRAL,
            2 to Sentiment.POSITIVE
    )

    override fun process(scope: PipelineScope, span: PerformanceSpan) = use(scope) {
        val text = context.input.transcript.text
        val input = Input(listOf(text))
        val request = Request(listOf(input))

        val response = webTarget.path("/sentiment/infer").request().post(Entity.json(request), object : GenericType<Response>() {})
        val sentimentClasses = mutableListOf<ai.flowstorm.core.Input.Class>()
        response.outputs[0].data.forEachIndexed { i, score ->
            sentimentClasses.add(ai.flowstorm.core.Input.Class(ai.flowstorm.core.Input.Class.Type.Sentiment,
                    (idx2Sentiment[i] ?: Sentiment.UNKNOWN).toString(), score))
        }
        context.turn.input.classes.addAll(sentimentClasses.sortedWith(compareByDescending(ai.flowstorm.core.Input.Class::score)))
    }

    data class Request(val inputs: List<Input>)

    data class Input(val data: List<String>) {
        val name: String = "text"
        val datatype: String = "BYTES"
        val shape: List<Int> = listOf(1, 1)
    }

    data class Response(val outputs: List<Output>)

    data class Output(val data: List<Float>)
}
