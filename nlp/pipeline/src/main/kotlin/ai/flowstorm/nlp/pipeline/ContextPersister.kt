package ai.flowstorm.nlp.pipeline

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.core.Context
import ai.flowstorm.core.indexer.RuntimeDataIndexer
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.repository.CommunityRepository
import ai.flowstorm.core.repository.ProfileRepository
import ai.flowstorm.core.repository.SessionRepository
import ai.flowstorm.core.repository.TurnRepository
import kotlinx.datetime.Clock
import javax.inject.Inject

class ContextPersister(
    @ConfigValue("indexer.captureErrors", "false")
    val captureIndexerErrors: Boolean = false
) {

    @Inject
    lateinit var profileRepository: ProfileRepository

    @Inject
    lateinit var communityRepository: CommunityRepository

    @Inject
    lateinit var sessionRepository: SessionRepository

    @Inject
    lateinit var turnRepository: TurnRepository

    @Inject
    lateinit var monitoring: Monitoring

    @Inject
    lateinit var indexer: RuntimeDataIndexer

    fun persist(session: Session) = sessionRepository.update(session)
    fun persist(context: Context) = with(context) {
        turn.log.addAll(pipelineScope.log.entries)
        turn.duration = Clock.System.now().toEpochMilliseconds() - turn.datetime.toEpochMilliseconds()
        session.datetime = Clock.System.now()
        try {
            val newTurn = turnRepository.create(turn)
            session.turns += newTurn
            sessionRepository.update(session)
            profileRepository.update(profile, true)
            communities.values.forEach {
                communityRepository.update(it)
            }
        } catch (e: Throwable) {
            monitoring.capture(null, e, session.toTags {
                it["turn_id"] = turn._id.toString()
            })
            throw e
        }

        try {
            indexer.save(session, turn, profile)
        } catch (e: Throwable) {
            if (captureIndexerErrors) {
                monitoring.capture(null, e, session.toTags {
                    it["turn_id"] = turn._id.toString()
                })
            }
        }
    }
}
