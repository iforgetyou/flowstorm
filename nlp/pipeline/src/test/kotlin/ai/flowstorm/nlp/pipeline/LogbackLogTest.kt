package ai.flowstorm.nlp.pipeline

import ai.flowstorm.util.Log
import ai.flowstorm.util.RunLog
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class LogbackLogTest {

    @Test
    fun `test logger`() {
        val log = RunLog("TestLog")

        log.logger.info("Log something")

        with(log.entries.first()) {
            assertEquals(Log.Entry.Level.INFO, level)
            assertEquals("Log something", text)
        }
    }
}