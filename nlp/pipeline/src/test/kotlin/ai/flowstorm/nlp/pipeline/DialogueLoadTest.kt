package ai.flowstorm.nlp.pipeline

import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.model.*
import ai.flowstorm.core.runtime.FileResourceLoader
import ai.flowstorm.storage.local.LocalFileStorage
import org.slf4j.LoggerFactory
import java.io.File

object DialogueLoadTest {

    @JvmStatic
    fun main(args: Array<String>) {
        val logger = LoggerFactory.getLogger("dialogue-model-load-test")
        val localFileStorage = LocalFileStorage(File("test"))
        val loader = FileResourceLoader( "dialogue").apply { fileStorage = localFileStorage }
        val dialogueId = "dialogue1"
        val dialogue = loader.newObject<AbstractDialogue<*>>("$dialogueId/model", "ble", 1, false)
        dialogue.loader = loader

        dialogue.validate()
        println(dialogue.describe())
        val user = User(username = "tester@flowstorm.ai", name = "Tester", surname = "Tester", nickname = "Tester")
        val space =
            SpaceImpl(name="Test Space")
        /*
        val context = Context(
            object : Pipeline {
                override val components = LinkedList<PipelineComponent>()
                override fun process(context: Context): Context = components.pop().process(context)
            },
            Profile(user_id = user._id, space_id = space._id),
            Session(
                datetime = Clock.System.now(),
                sessionId = "T-E-S-T",
                device = Device(deviceId = "test", description = ""),
                    user = user,
                        application = Application(name = "test", dialogue_id = ObjectId(dialogueId).toId()), locale = Defaults.locale
                ),
                Turn(input = Input(Transcript("some message"))),
                dialogue.locale
        )

        val func = dialogue.functions.first()
        println("calling $func:")
        println(func.exec(context))

        dialogue.subDialogues.first().apply {
            val dialogueArgs = getConstructorArgs(context)
            val subDialogue = loader.newObjectWithArgs<AbstractDialogue>("${this.dialogueId}/model", dialogueArgs)
            println("subDialogue: $subDialogue")
            println(subDialogue.describe())
        }

         */
    }
}