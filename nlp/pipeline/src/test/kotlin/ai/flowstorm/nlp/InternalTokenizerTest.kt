package ai.flowstorm.nlp

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.Input
import ai.flowstorm.core.input
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import io.mockk.every
import io.mockk.mockkClass
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InternalTokenizerTest {
    val tokenizer = InternalTokenizer()
    val scope = mockkClass(PipelineScope::class)
    val span = mockkClass(PerformanceSpan::class)

    @Test
    fun punctuationAfterToken() {
        val input = input("What's the weather in Prague?").apply {
            tokens += Input.Word("What's")
            tokens += Input.Word("the")
            tokens += Input.Word("weather")
            tokens += Input.Word("in")
            tokens += Input.Word("Prague?")
        }
        every { scope.context.input } returns input
        tokenizer.process(scope, span)
        assertEquals(6, input.tokens.size)
        assertEquals("Prague", input.tokens[input.tokens.size - 2].text)
        assertEquals("?", input.tokens.last().text)
    }

    @Test
    fun punctuationBeforeToken() {
        val input = input("What's the weather in Prague?").apply {
            tokens += Input.Word("That's")
            tokens += Input.Word("right")
            tokens += Input.Word(",you")
            tokens += Input.Word("know")
        }
        every { scope.context.input } returns input
        tokenizer.process(scope, span)
        assertEquals(5, input.tokens.size)
        assertEquals("you", input.tokens[input.tokens.size - 2].text)
        assertEquals(",", input.tokens[input.tokens.size - 3].text)
    }
}