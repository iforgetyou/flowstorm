group = "ai.flowstorm.nlp"
description = "Flowstorm NLP TTS Microsoft"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-nlp-tts-api"))
    implementation(project(":flowstorm-common-client"))
}

