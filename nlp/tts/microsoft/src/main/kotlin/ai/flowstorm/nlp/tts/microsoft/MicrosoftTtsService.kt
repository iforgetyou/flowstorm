package ai.flowstorm.nlp.tts.microsoft

import ai.flowstorm.common.client.RestClient
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.nlp.tts.TtsRequest
import ai.flowstorm.nlp.tts.TtsService
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.LoggerDelegate
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import java.io.*
import javax.ws.rs.client.Entity
import javax.ws.rs.client.Invocation
import javax.ws.rs.core.Response
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

class MicrosoftTtsService : TtsService {

    object XmlTransformer {

        private val xmlBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        private val xmlTransformer = TransformerFactory.newInstance().newTransformer()
        init {
            xmlTransformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes")
        }

        fun transform(input: String, callback: ((Document) -> Unit)): String {
            val xml = xmlBuilder.parse(ByteArrayInputStream(input.toByteArray()))
            callback(xml)
            val buf = StringWriter()
            xmlTransformer.transform(DOMSource(xml), StreamResult(buf))
            return buf.toString()
        }
    }

    override val name = "Microsoft"

    @ConfigValue("mscs.location", "")
    lateinit var location: String

    @ConfigValue("mscs.key")
    lateinit var key: String

    @ConfigValue("mscs.tts.host", "")
    lateinit var host: String

    private val restAuthUrl by lazy {
        "https://$location.api.cognitive.microsoft.com"
    }

    private val restUrl by lazy {
        host.ifBlank { "https://$location.tts.speech.microsoft.com"  }
    }
    var token = ""
        get() {
            if (host.isBlank()) {
                val now = currentTime()
                if (tokenIssued + 600 * 1000 < now) {
                    field = call("$restAuthUrl/sts/v1.0/issueToken") {
                        header("Ocp-Apim-Subscription-Key", key)
                                .post(Entity.text(""))
                    }.readEntity(String::class.java)
                    tokenIssued = now
                }
            }
            return field
        }

    private var tokenIssued = 0L
    private val logger by LoggerDelegate()

    fun call(url: String, builder: (Invocation.Builder.() -> Response)): Response {
        val target = RestClient.webTarget(url)
        val res = builder(target.request())
        if (res.status != 200) {
            logger.error(res.readEntity(String::class.java))
            error(res.statusInfo.reasonPhrase)
        }
        return res
    }

    private fun synthesize(ssml: String): ByteArray {
        call("$restUrl/cognitiveservices/v1") {
            if (token.isNotBlank())
                header("Authorization", "Bearer $token")
            header("Content-Type", "application/ssml+xml")
            header("X-Microsoft-OutputFormat", "audio-16khz-64kbitrate-mono-mp3")
            post(Entity.text(ssml))
        }.readEntity(InputStream::class.java).use {
            val buf = ByteArrayOutputStream()
            it.copyTo(buf)
            return buf.toByteArray()
        }
    }

    private fun Element.moveNodes(targetElement: Element) {
        while (hasChildNodes()) {
            val child = firstChild
            removeChild(child)
            targetElement.appendChild(child)
        }
    }

    override fun speak(ttsRequest: TtsRequest): ByteArray {
        // we need to transform SSML to match Microsoft requirements
        val ssml = XmlTransformer.transform(if (ttsRequest.isSsml) ttsRequest.text else "<speak>${ttsRequest.text}</speak>") { doc ->
            val speakElement = doc.documentElement
            speakElement.setAttribute("version", "1.0")
            speakElement.setAttribute("xmlns", "https://www.w3.org/2001/10/synthesis")
            speakElement.setAttribute("xmlns:mstts", "https://www.w3.org/2001/mstts")
            speakElement.setAttribute("xml:lang", ttsRequest.config.locale.toLanguageTag())

            // add voice element if there is not
            val voiceElement = if (speakElement.getElementsByTagName("voice").length == 0) {
                doc.createElement("voice").also {
                    it.setAttribute("name", ttsRequest.config.name)
                    speakElement.moveNodes(it)
                    speakElement.appendChild(it)
                }
            } else {
                speakElement.getElementsByTagName("voice").item(0) as Element
            }

            // add ssml element if there is not
            if (voiceElement.getElementsByTagName("ssml").length == 0) {
                doc.createElement("ssml").also {
                    voiceElement.moveNodes(it)
                    voiceElement.appendChild(it)
                }
            }
        }
        return synthesize(ssml)
    }
}
