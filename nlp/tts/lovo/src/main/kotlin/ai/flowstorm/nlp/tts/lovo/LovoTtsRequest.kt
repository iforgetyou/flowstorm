package ai.flowstorm.nlp.tts.lovo

data class LovoTtsRequest(
    val text: String,
    val speaker_id: String,
    val emphasis: String = "[]",
    val speed: Int = 1,
    val pause: Int = 0,
    val encoding: String = "mp3"
)