package ai.flowstorm.nlp.tts.lovo

import ai.flowstorm.common.client.RestClient
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.nlp.tts.TtsRequest
import ai.flowstorm.nlp.tts.TtsService
import java.io.ByteArrayOutputStream
import java.net.URL

class LovoTtsService : TtsService {

    override val name = "Lovo"

    @ConfigValue("lovo.apiKey")
    lateinit var apiKey: String

    override fun speak(ttsRequest: TtsRequest): ByteArray {
        val buf = ByteArrayOutputStream()
        RestClient.call(URL("https://api.lovo.ai/v1/conversion"), "POST",
            mapOf(
                "apiKey" to apiKey,
                "Content-Type" to "application/json"
            ), LovoTtsRequest(
                ttsRequest.text.replace(Regex("\\<.*?\\>"), ""), // temporary solution - just stripping SSML
                ttsRequest.config.name
            )
        ).run {
            inputStream.copyTo(buf)
        }
        return buf.toByteArray()
    }
}