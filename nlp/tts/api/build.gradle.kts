group = "ai.flowstorm.nlp"
description = "Flowstorm NLP TTS API"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-core-lib"))
}

