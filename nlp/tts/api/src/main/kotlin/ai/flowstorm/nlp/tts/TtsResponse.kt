package ai.flowstorm.nlp.tts

data class TtsResponse(val path: String, val type: String)
