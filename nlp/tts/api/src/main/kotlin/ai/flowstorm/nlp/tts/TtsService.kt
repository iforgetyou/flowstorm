package ai.flowstorm.nlp.tts

interface TtsService {

    val name: String

    fun speak(ttsRequest: TtsRequest): ByteArray
}