package ai.flowstorm.nlp.tts.amazon

import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.polly.AmazonPollyClient
import com.amazonaws.services.polly.model.OutputFormat
import com.amazonaws.services.polly.model.SynthesizeSpeechRequest
import com.amazonaws.services.polly.model.TextType
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.nlp.tts.TtsRequest
import ai.flowstorm.nlp.tts.TtsService
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import java.io.ByteArrayOutputStream

@Suppress("DEPRECATION")
class AmazonTtsService: TtsService {

    @ConfigValue("aws.access-key")
    lateinit var accessKey: String

    @ConfigValue("aws.secret-key")
    lateinit var secretKey: String

    override val name = "Amazon"
    //val region = Region.getRegion(Regions.EU_WEST_1)
    private val client get() =
        AmazonPollyClient(
            if (accessKey.isNotBlank() && secretKey.isNotBlank())
                AWSStaticCredentialsProvider(BasicAWSCredentials(accessKey, secretKey))
            else
                DefaultAWSCredentialsProviderChain(),
           ClientConfiguration()
        )

    override fun speak(ttsRequest: TtsRequest): ByteArray {
        val buf = ByteArrayOutputStream()
        val request = SynthesizeSpeechRequest()
                .withText(ttsRequest.text)
                .withTextType(if (ttsRequest.isSsml) TextType.Ssml else TextType.Text)
                .withVoiceId(ttsRequest.config.name)
                .withOutputFormat(OutputFormat.Mp3)
        ttsRequest.config.engine?.let {
            request.engine = it
        }
        val result = client.synthesizeSpeech(request)
        result.audioStream.copyTo(buf)
        buf.close()
        return buf.toByteArray()
    }
}