group = "ai.flowstorm.nlp"
description = "Flowstorm NLP TTS Amazon"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-nlp-tts-api"))
    implementation("com.amazonaws:aws-java-sdk-polly:1.12.17") {
        exclude("com.fasterxml.jackson.core", "jackson-core")
        exclude("com.fasterxml.jackson.core", "jackson-databind")
        exclude("com.fasterxml.jackson.dataformat", "jackson-dataformat-cbor")
    }
}

