group = "ai.flowstorm.nlp"
description = "Flowstorm NLP TTS Google"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-nlp-tts-api"))
    implementation("com.google.cloud:google-cloud-texttospeech:0.117.0-beta")
}

