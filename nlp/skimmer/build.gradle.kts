group = "ai.flowstorm.nlp"
description = "Flowstorm NLP Skimmer"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-lib"))
}