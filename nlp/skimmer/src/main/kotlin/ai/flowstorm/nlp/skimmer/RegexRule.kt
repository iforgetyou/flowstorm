package ai.flowstorm.nlp.skimmer

data class RegexRule(val pattern: String, override val path: String, override val value: Any) : Rule {
    val regex: Regex by lazy {
        (".*${pattern.lowercase()}.*").toRegex()
    }
}