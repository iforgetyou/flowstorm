package ai.flowstorm.nlp.skimmer

import ai.flowstorm.common.ObjectUtil.defaultMapper
import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.Context
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.core.type.Memory
import ai.flowstorm.util.LoggerDelegate
import com.fasterxml.jackson.core.type.TypeReference
import java.io.IOException
import java.net.URL

/**
 * <pre>
 *     // example of skimmer config
 *     [
 *       {
 *         "type": "regex",
 *         "pattern": "...",
 *         "path": "userProfile.user.isSad",
 *         "value": true
 *       },
 *       {
 *         "type": "regex",
 *         "pattern": "...",
 *         "path": "session.emotions.sadness",
 *         "value": true
 *       }
 *     ]
 *
 *     // example of (mixin) init code
 *     val user by globalUser { UserProfile() }
 *     val emotions by globalSession { Emotions() }
 * </pre>
 */
class Skimmer : PipelineComponent {

    private val logger by LoggerDelegate()

    private fun getRules(url: String) = URL(url).openStream().use {
        try {
            defaultMapper.readValue(it, object : TypeReference<List<Rule>>() {})
        } catch (e: IOException) {
            logger.error("Cannot parse json file with rules: ${e.message}. No rules will be used in turn processing!")
            null
        }
    }

    private val groupName = "groupName"
    private val groupNameRegex = ("\\?<(?<$groupName>.+)>").toRegex()

    override fun process(scope: PipelineScope, span: PerformanceSpan) {
        with(scope.context) {

            val text = turn.input.transcript.text.lowercase()
            session.application.skimmerConfigUrl?.let { getRules(it) }?.forEach { rule ->

                val path = rule.path.split(".")
                val attributes = when (path[0]) {
                    Context::turn.name -> turn.attributes
                    Context::session.name -> session.attributes
                    Context::profile.name -> profile.attributes
                    "user" -> profile.attributes
                    else -> error("Unknown context scope: ${path[0]}")
                }
                attributes[path[1]][path[2]]?.let {
                    (it as? Memory<Any>)?.let { obj ->
                        (when (rule) {
                            is RegexRule -> processRegexRule(rule, text)
                            else -> error("Unexpected type of rule $rule")
                        })?.let { value ->
                            logger.info("Match for $rule")
                            val serializableObj = obj.convert()
                            if (path.size > 3) {
                                var prop = (serializableObj.value as MutableMap<String, Any>)
                                for (x in path.subList(3, path.size - 1)) {
                                    if (!prop.containsKey(x)) {
                                        prop[x] = mutableMapOf<String, Any>()
                                    }
                                    prop = prop[x] as MutableMap<String, Any>
                                }
                                prop[path.last()] = value
                            } else {
                                serializableObj.value = value
                            }
                            attributes[path[1]][path[2]] = serializableObj
                            logger.info("Saved value '$value' to ${rule.path}")
                        }
                    } ?: logger.warn("Object ${path[0]}.${path[1]}.${path[2]} not found in context")
                }
            }
        }
    }

    private fun processRegexRule(rule: RegexRule, text: String) =
        rule.regex.matchEntire(text)?.let { match ->
            if (rule.value is String) {
                // value of regex group
                val groupNameFinding = groupNameRegex.matchEntire(rule.value as String)
                if (groupNameFinding != null) {
                    val groupName = groupNameFinding.groups[groupName]?.value ?: ""
                    try {
                        match.groups[groupName]?.value ?: rule.value
                    } catch (e: Exception) {
                        logger.error("Group name $groupName not found in regex ${rule.regex}")
                    }
                }

                // plane string
                else rule.value
            } else rule.value
        }
}
