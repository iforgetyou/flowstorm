package ai.flowstorm.connector.mailgun

import okhttp3.RequestBody

class Attachment(val fileName: String, val requestBody: RequestBody)