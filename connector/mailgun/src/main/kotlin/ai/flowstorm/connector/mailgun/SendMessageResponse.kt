package ai.flowstorm.connector.mailgun

data class SendMessageResponse(val id: String? = null, val message: String? = null)