group = "ai.flowstorm.connector"
description = "Flowstorm Connector Mailgun"

val okHttpVersion = findProperty("okhttp.version")

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-common-lib"))
    api("com.squareup.okhttp3:okhttp:$okHttpVersion")
}

