group = "ai.flowstorm.connector"
description = "Flowstorm Connector Lib"

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-core-lib"))
    api(project(":flowstorm-common-client"))
}

