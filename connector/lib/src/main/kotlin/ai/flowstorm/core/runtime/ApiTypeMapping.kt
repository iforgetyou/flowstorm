package ai.flowstorm.core.runtime

typealias Thing = ai.flowstorm.core.runtime.model.Thing
typealias Movie = ai.flowstorm.core.runtime.model.Movie
typealias Actor = ai.flowstorm.core.runtime.model.Actor
typealias Track = ai.flowstorm.core.runtime.model.Track
typealias Artist = ai.flowstorm.core.runtime.model.Artist
