package ai.flowstorm.core.runtime.model

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
open class Actor(
        name: String = "",
        type: String = "Actor",
        val gender: Int = -1,
        val id: Int = -1,
        val knownForDepartment: String = ""): Thing(name, type) {


    open class Credit(
        val movie: Movie, val character: String,
    )

    //FIXIT - cannot use static access to connector
//    @get:JsonIgnore
//    val credits: List<Credit> by lazy {
//        TmdbConnectorV2.tmdb.personCredits(name)
//    }
}
