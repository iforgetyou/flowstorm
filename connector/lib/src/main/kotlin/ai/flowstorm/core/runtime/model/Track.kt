package ai.flowstorm.core.runtime.model

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
open class Track(
        name: String = "",
        type: String = "Track",
        val artist: String = "",
        @JsonAlias("mbid")
        val id: String = "",
        val listeners: Long = -1,
): Thing(name, "Track")