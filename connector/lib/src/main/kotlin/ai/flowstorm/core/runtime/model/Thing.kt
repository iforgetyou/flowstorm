package ai.flowstorm.core.runtime.model

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
open class Thing(val name: String, val type:String = "Generic") {
}

