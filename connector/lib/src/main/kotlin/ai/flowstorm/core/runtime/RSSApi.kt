package ai.flowstorm.core.runtime

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

class RSSApi(private val url: String) {

    @JacksonXmlRootElement(localName = "rss")
    data class RSS(val channel: Channel)
    data class Channel(val title: String, @JsonProperty("item") @JacksonXmlElementWrapper(useWrapping = false) val items: List<Item>,
                       val description: String, val generator: String, val link: String)
    data class Item(val title: String, var description: String, val link: String)

    fun getFeed(stripHtmlTags: Boolean = true): RSS =
        if (stripHtmlTags) {
            Api.get<RSS>(url).apply {
                channel.items.forEach {
                    it.description = it.description.replace(Regex("\\<.*?\\>"), "")
                }
            }
        } else {
            Api.get(url)
        }
}