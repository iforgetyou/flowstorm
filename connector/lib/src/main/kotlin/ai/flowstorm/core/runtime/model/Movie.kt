package ai.flowstorm.core.runtime.model
import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
open class Movie(
        @JsonAlias("name", "title")
        name: String = "",
        type: String = "Movie",
        val genreIds: ArrayList<Int> = arrayListOf(),
        val id: Int = -1,
        val mediaType: String = "",
        val overview: String = "",
        @JsonAlias("release_date", "first_air_date", "firstAirDate")
        val releaseDate: String = "",
        val voteAverage: Double = -1.0,
        val popularity: Double = -1.0
): Thing(name, "Movie")
{
    open class Crewmember(
            name: String, type: String,  gender: Int, id: Int, known_for_department: String, var job: String,
    ): Actor(name, type, gender, id, known_for_department) {
        constructor(actor: Actor, job: String): this(actor.name, "Crew", actor.gender, actor.id, actor.knownForDepartment, job) {

        }
    }
    open class Castmember(
            name: String, type: String, gender: Int, id: Int, knownForDepartment: String, var character: String,
    ): Actor(name, type, gender, id, knownForDepartment) {
        constructor(actor: Actor, character: String): this(actor.name, actor.type, actor.gender, actor.id, actor.knownForDepartment, character)
    }

    //FIXME - cannot use static access to connectors
//    @get:JsonIgnore
//    val cast: List<Castmember> by lazy {
//        TmdbConnectorV2.tmdb.castCredits(name)
//    }
//
//    @get:JsonIgnore
//    val crew: List<Crewmember> by lazy {
//        TmdbConnectorV2.tmdb.crewCredits(name)
//    }

}