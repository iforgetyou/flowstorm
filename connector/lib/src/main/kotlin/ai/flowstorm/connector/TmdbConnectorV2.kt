package ai.flowstorm.connector

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.runtime.Api
import ai.flowstorm.core.runtime.model.Actor
import ai.flowstorm.core.runtime.model.Movie
import ai.flowstorm.core.type.DateTime
import ai.flowstorm.core.type.Dynamic
import java.time.format.DateTimeFormatter
import com.fasterxml.jackson.module.kotlin.convertValue
import javax.inject.Inject

class TmdbConnectorV2 {

    enum class SearchType {
        MOVIE, TV, PERSON, MULTI;

        override fun toString(): String {
            return super.toString().lowercase()
        }
    }

    @ConfigValue("tmdb.key", "")
    private lateinit var apiKey: String

    private val target
        get() = Api.target("https://api.themoviedb.org/3")
                .queryParam("api_key", apiKey)
                .queryParam("language", "en-Us")

    fun search(query: String, type: SearchType = SearchType.MULTI): List<Movie> =
            ObjectUtil.defaultMapper.convertValue(
                Api.get<Dynamic>(target.path("/search/$type")
                .queryParam("query", query)
                .queryParam("include_adult", false)).list("results"))

    fun searchPerson(query: String, type: SearchType = SearchType.PERSON): List<Actor> =
            ObjectUtil.defaultMapper.convertValue(
                Api.get<Dynamic>(target.path("/search/$type")
                .queryParam("query", query)
                .queryParam("include_adult", false)).list("results"))


    private fun credits(movieName: String): Dynamic {
        val movie = search(movieName).getOrElse(0) { return Dynamic.EMPTY }
        return Api.get(target.path("/${movie.mediaType}/${movie.id}/credits"))
    }

    fun personCredits(personName: String): List<Actor.Credit> {
        val person = searchPerson(personName).getOrElse(0) { return listOf() }
        val movies = Api.get<Dynamic>(target.path("/person/${person.id}/combined_credits")).list("cast")
        return movies.map { Actor.Credit(ObjectUtil.defaultMapper.convertValue(it), it["character"] as String) }
    }

    fun castCredits(personName: String): List<Movie.Castmember> {
        val credits = credits(personName)
        val cast = credits.list("cast")
        return cast.map { Movie.Castmember(ObjectUtil.defaultMapper.convertValue(it), it["character"] as String) }.toList()
    }

    fun crewCredits(personName: String): List<Movie.Crewmember> {
        val credits = credits(personName)
        val crew = credits.list("crew")
        return crew.map { Movie.Crewmember(ObjectUtil.defaultMapper.convertValue(it), it["job"] as String) }.toList()
    }

    fun upcomingMovies(region: String = "US"): List<Movie> {
        return ObjectUtil.defaultMapper.convertValue(
            Api.get<Dynamic>(target.path("/movie/upcoming")
                .queryParam("include_adult", false)
                .queryParam("sort_by", "popularity.desc")
                .queryParam("region", region)).list("results"))
    }

    fun popularMovies(releasedAfter: DateTime, releasedBefore: DateTime = DateTime.now(), region: String = "US"): List<Movie> {
        return ObjectUtil.defaultMapper.convertValue(
            Api.get<Dynamic>(target.path("/discover/movie")
                .queryParam("region", region)
                .queryParam("sort_by", "popularity.desc")
                .queryParam("primary_release_date.gte", releasedAfter.format(DateTimeFormatter.ISO_LOCAL_DATE))
                .queryParam("primary_release_date.lte", releasedBefore.format(DateTimeFormatter.ISO_LOCAL_DATE))
                .queryParam("include_adult", false)).list("results"))
    }
}
