package ai.flowstorm.connector

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.runtime.Api
import ai.flowstorm.core.runtime.model.Artist
import ai.flowstorm.core.runtime.model.Track
import ai.flowstorm.core.type.Dynamic
import com.fasterxml.jackson.module.kotlin.convertValue

class LastFMConnector {

    @ConfigValue("lastfm.key", "")
    private lateinit var apiKey: String

    private val target
        get() = Api.target("http://ws.audioscrobbler.com/2.0")
                .queryParam("api_key", apiKey)
                .queryParam("format", "json")

    fun artistSearch(query: String): List<Artist> =
            ObjectUtil.defaultMapper.convertValue(
                Api.get<Dynamic>(
                    target.path("/")
                        .queryParam("artist", query)
                        .queryParam("method", "artist.search")
                )
                .list("results.artistmatches.artist"))

    fun trackSearch(query: String, artist: String = ""): List<Track> =
            ObjectUtil.defaultMapper.convertValue(
                Api.get<Dynamic>(
                    target.path("/")
                        .queryParam("track", query)
                        .queryParam("artist", artist)
                        .queryParam("method", "track.search")
                )
                .list("results.trackmatches.track"))
}