package ai.flowstorm.connector

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.runtime.Api
import ai.flowstorm.core.type.DateTime
import ai.flowstorm.core.type.Dynamic
import java.time.format.DateTimeFormatter

class TmdbConnector {

    enum class SearchType {
        MOVIE, TV, PERSON, MULTI;

        override fun toString(): String {
            return super.toString().lowercase()
        }
    }

    @ConfigValue("tmdb.key", "")
    private lateinit var apiKey: String

    private val target
        get() = Api.target("https://api.themoviedb.org/3")
                .queryParam("api_key", apiKey)
                .queryParam("language", "en-Us")

    fun search(query: String, type: SearchType = SearchType.MULTI)
        = Api.get<Dynamic>(target.path("/search/$type")
                .queryParam("query", query)
                .queryParam("include_adult", false)).list("results")

    fun details(movieName: String, type: SearchType? = null): Dynamic {
        if (type == SearchType.MULTI) {
            throw IllegalArgumentException("Details search is not supported for type MULTI.")
        }
        val movie = search(movieName).getOrElse(0) { return Dynamic.EMPTY }
        val typeString: String = type?.toString() ?: movie["media_type"].toString()
        return Api.get(target.path("/${typeString}/${movie["id"]}"))
    }

    fun credits(movieName: String): Dynamic {
        val movie = search(movieName).getOrElse(0) { return Dynamic.EMPTY }
        return Api.get(target.path("/${movie["media_type"]}/${movie["id"]}/credits"))
    }

    fun personCredits(personName: String): Dynamic {
        val person = search(personName, type = SearchType.PERSON)[0]
        return Api.get(target.path("/person/${person["id"]}/combined_credits"))
    }

    fun popularMovies(releasedAfter: DateTime, releasedBefore: DateTime = DateTime.now(), region: String = "US")
        = Api.get<Dynamic>(target.path("/discover/movie")
                .queryParam("region", region)
                .queryParam("sort_by", "popularity.desc")
                .queryParam("primary_release_date.gte", releasedAfter.format(DateTimeFormatter.ISO_LOCAL_DATE))
                .queryParam("primary_release_date.lte", releasedBefore.format(DateTimeFormatter.ISO_LOCAL_DATE))
                .queryParam("include_adult", false)).list("results")

    fun movieTitle(id: Int): String = Api.get<Dynamic>(target.path("/movie/$id").queryParam("include_adult", false))("title") as String

}
