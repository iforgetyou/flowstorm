package ai.flowstorm.connector

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.runtime.Api

class SangixConnector {

    data class Patient(val id: Int, val name: String? = "", val surname: String? = "", val email: String? = "", val phone: String? = "",
                       val phone2: String? = "", val phone3: String? = "", var apptList: List<Appointment>? = listOf()) {
    }

    data class Appointment(val apptId: String, val timestamp: String, val length: Int? = 0, val referenceNumber: String? = "") {
    }

    @ConfigValue("sangix.url", "")
    private lateinit var url: String

    @ConfigValue("sangix.credentials", "")
    private lateinit var credentials: String

    private val target get() = Api.target(url)
    private val apiKey get() = java.util.Base64.getEncoder().encodeToString(credentials.toByteArray())
    private val headers get() = mapOf("Authorization" to "Basic ${apiKey}", "Accept" to "application/json")

    fun getPatient(birthdate: String, phone: String)
            = Api.get<Patient>(target.path("/patient")
            .queryParam("birthdate", birthdate)
            .queryParam("phone", phone), headers)

    fun getAppointments(from: String, to: String, limit: Int)
            = Api.get<Map<String, List<Any>>>(target.path("/appointments")
            .queryParam("from", from)
            .queryParam("to", to)
            .queryParam("limit", limit), headers)["apptList"]

    fun getAppointments(around: String, limit: Int)
            = Api.get<Map<String, List<Any>>>(target.path("/appointments")
            .queryParam("around", around)
            .queryParam("limit", limit), headers)["apptList"]

    fun cancelAppointment(appt: String)
            = Api.delete<String>(target.path("/appointment/${appt}"), headers)

    fun bookAppointment(appt: String, user: Int)
            = Api.post<String>(target.path("/appointment/${appt}/${user}"),"", headers)

}
