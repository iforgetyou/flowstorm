package ai.flowstorm.connector.reddit

import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.client.RestClient
import ai.flowstorm.core.type.Dynamic
import java.net.URI
import java.net.URL
import java.net.URLEncoder
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.nio.charset.StandardCharsets
import java.time.Duration
import java.util.*
import kotlinx.datetime.*
import kotlinx.datetime.TimeZone
import javax.ws.rs.core.HttpHeaders

class Reddit {

    fun getFunFacts(accessToken: String): MutableList<Trivia> {
        val url = "https://oauth.reddit.com/r/todayilearned/new.json?limit=200"
        // Create authorization header
        val authorizationHeader = "Bearer $accessToken"
        val response = RestClient.call(url = URL(url), responseType = Dynamic::class.java, method = "GET", headers = mapOf(HttpHeaders.AUTHORIZATION to authorizationHeader, HttpHeaders.USER_AGENT to "trivia by flowstormer", HttpHeaders.CONTENT_TYPE to "application/json"))
        val triviaList = mutableListOf<Trivia>()
        for (sr in (response.get("data") as Dynamic).list("children")) {
            val trv = sr.let {
                Trivia(
                        title = normalizeText(((it.get("data") as Dynamic).get("title") as String)),
                        source = (it.get("data") as Dynamic).get("subreddit") as String,
                        score = (it.get("data") as Dynamic).get("score") as Int,
                        permalink = (it.get("data") as Dynamic).get("permalink") as String,
                        created = Instant.fromEpochSeconds(((it.get("data") as Dynamic).get("created_utc") as Double).toLong()),
                        fullname = (it.get("data") as Dynamic).get("name") as String
                )
            }
            if (Clock.System.now().minus(1, DateTimeUnit.DAY, TimeZone.currentSystemDefault()) <= trv.created) {
                triviaList.add(trv)
            }
        }
        return triviaList

    }

    fun normalizeText(text: String):String {
        return text.trim()
                .removePrefix("Today I learned")
                .removePrefix("TIL")
                .trim()
                .replace("^[^a-zA-Z\\d]*".toRegex(), "")
                .removePrefix("that")
                .removePrefix("of")
                .removePrefix("about")
                .replace("^[^a-zA-Z\\d]*".toRegex(), "")
                .replace("[^a-zA-Z\\d]*$".toRegex(), "")
                .replaceFirstChar { it.uppercase() }
                .let { if (!it.endsWith(".")) it.plus(".") else it }
    }

    fun getAccessToken(): String {
        val httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .connectTimeout(Duration.ofSeconds(15))
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build()

        // form parameters
        val data: MutableMap<Any, Any> = HashMap()
        data["grant_type"] = "password"
        data["username"] = AppConfig["reddit.name"]
        data["password"] = AppConfig["reddit.password"]

        val plainCredentials = AppConfig["reddit.app"] + ":" + AppConfig["reddit.secret"]
        val base64Credentials = String(Base64.getEncoder().encode(plainCredentials.toByteArray()))
        // Create authorization header
        val authorizationHeader = "Basic $base64Credentials"

        val request = HttpRequest.newBuilder()
                .POST(ofFormData(data))
                .uri(URI.create("https://www.reddit.com/api/v1/access_token"))
                .setHeader(HttpHeaders.AUTHORIZATION, authorizationHeader)
                .setHeader("User-Agent", "trivia v 1.0 by /u/flowstormer")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build()

        val response = httpClient.send(request, HttpResponse.BodyHandlers.ofString())

        val body = ObjectUtil.defaultMapper.readValue(response.body(), Dynamic::class.java)
        return body.get("access_token") as String
    }

    fun ofFormData(data: Map<Any, Any>): HttpRequest.BodyPublisher? {
        val result = StringBuilder()
        for ((key, value) in data) {
            if (result.isNotEmpty()) {
                result.append("&")
            }
            val encodedName = URLEncoder.encode(key.toString(), StandardCharsets.UTF_8)
            val encodedValue = URLEncoder.encode(value.toString(), StandardCharsets.UTF_8)
            result.append(encodedName)
            if (encodedValue != null) {
                result.append("=")
                result.append(encodedValue)
            }
        }
        return HttpRequest.BodyPublishers.ofString(result.toString())
    }

}