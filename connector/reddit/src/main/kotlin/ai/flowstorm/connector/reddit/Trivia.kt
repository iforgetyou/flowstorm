package ai.flowstorm.connector.reddit

import ai.flowstorm.common.model.Entity
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId

data class Trivia(
    val title: String,
    val source: String = "Unknown",
    val score: Int = 0,
    val permalink: String = "",
    val created: Instant = Clock.System.now(),
    val fullname: String = "",
    override val _id: Id<Trivia> = newId(),
) : Entity