package ai.flowstorm.connector.reddit.job

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.indexer.Elastic
import ai.flowstorm.common.jackson.IgnoreIdMixin
import ai.flowstorm.common.model.Entity
import ai.flowstorm.connector.reddit.Reddit
import ai.flowstorm.core.job.AbstractJob
import ai.flowstorm.core.job.ScheduleAt
import javax.inject.Inject

@ScheduleAt(hours = 3, minutes = 0)
class RedditJob : AbstractJob() {
    @Inject
    private lateinit var elastic: Elastic

    override fun execute() {
        val at = Reddit().getAccessToken()
        val triv = Reddit().getFunFacts(at)
        for (t in triv) {
            elastic.save(jsonWriter, t)
        }
        logger.info("${triv.size} fun fact(s) stored to elastic")
    }

    companion object {
        private val jsonWriter = ObjectUtil.createMapper()
            .addMixIn(Entity::class.java, IgnoreIdMixin::class.java)
    }
}
