package ai.flowstorm.connector.reddit

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.config.Config
import ai.flowstorm.common.indexer.Elastic
import ai.flowstorm.common.jackson.IgnoreIdMixin
import ai.flowstorm.common.model.Entity
import com.fasterxml.jackson.module.kotlin.convertValue
import javax.inject.Inject

class RedditConnector{
    @Inject
    private lateinit var elastic: Elastic

    @Inject
    lateinit var config: Config

    fun getTrivia(): List<Trivia> {
        return elastic.matchAllQuery("${Trivia::class.simpleName!!}-${config.namespace}*".lowercase()).hits.map { jsonWriter.convertValue(it.sourceAsMap) }
    }

    fun getTrivia(queryString: String): List<Trivia> {
        return elastic.matchQuery("${Trivia::class.simpleName!!}-${config.namespace}*".lowercase(), queryString).hits.map { jsonWriter.convertValue(it.sourceAsMap) }
    }

    companion object {
        private val jsonWriter = ObjectUtil.createMapper()
            .addMixIn(Entity::class.java, IgnoreIdMixin::class.java)
    }
}
