group = "ai.flowstorm.connector"
description = "Flowstorm Connector Reddit"
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation(project(":flowstorm-core-api"))
    implementation(project(":flowstorm-connector-lib"))
    implementation(project(":flowstorm-core-lib"))
    implementation(project(":flowstorm-common-client"))
    implementation(project(":flowstorm-common-app"))
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
}