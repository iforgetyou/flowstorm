import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.credentials.HttpHeaderCredentials
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.tasks.TaskCollection
import org.gradle.authentication.http.HttpHeaderAuthentication
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.credentials
import org.gradle.kotlin.dsl.filter
import org.gradle.kotlin.dsl.get
import org.gradle.language.jvm.tasks.ProcessResources
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget
import java.io.File
import java.net.URI
import java.util.concurrent.TimeUnit

fun Project.runCommand(
    cmd: String,
    workingDir: File = project.projectDir,
    timeoutAmount: Long = 60,
    timeoutUnit: TimeUnit = TimeUnit.SECONDS
): String = ProcessBuilder(cmd.split("\\s(?=(?:[^'\"`]*(['\"`])[^'\"`]*\\1)*[^'\"`]*$)".toRegex()))
    .directory(workingDir)
    .redirectOutput(ProcessBuilder.Redirect.PIPE)
    .redirectError(ProcessBuilder.Redirect.PIPE)
    .start()
    .apply {
        waitFor(timeoutAmount, timeoutUnit)
    }
    .run {
        val errorText = errorStream.bufferedReader().readText().trim()
        if (errorText.isNotEmpty()) {
            error(errorText)
        }
        inputStream.bufferedReader().readText().trim()
    }

fun Project.withGitProperties() {
    var value = findProperty("git.commit") as String
    if (value == "unknown") {
        value = runCommand("git rev-parse --short=8 HEAD", rootDir)
        setProperty("git.commit", value)
    }
}

fun KotlinMultiplatformExtension.nativeTargets(mobile: Boolean = false, desktop: Boolean = false): KotlinNativeTarget? {
    if (mobile) {
        iosArm64()
        iosX64()
    }
    return if (desktop) {
        val osName = System.getProperty("os.name")
        val osArch = System.getProperty("os.arch")
        when {
            osName == "Mac OS X" -> /*when (osArch) { // uncomment when macosArm64 will be available
            "x86_64" -> */macosX64("native")
            /*"arm64" -> macosArm64("native")
            else -> null
        }*/
            osName == "Linux" -> when (osArch) {
                "x86_64", "amd64" -> linuxX64("native")
                "arm64" -> linuxArm64("native")
                else -> null
            }
            osName.startsWith("Windows") -> when (osArch) {
                "x86_64", "amd64" -> mingwX64("native")
                "x86" -> mingwX86("native")
                else -> null
            }
            else -> null
        }/*?.apply {
        binaries {
            executable {
                entryPoint = "ai.promethist.sandbox.main"
            }
        }
    }*/ ?: throw GradleException("Host OS $osName ($osArch) is not supported in Kotlin/Nativet")
    } else null
}

fun KotlinMultiplatformExtension.nativeSourceSets(sharedSourceSet: KotlinSourceSet, mobile: Boolean = false) {
    if (mobile) {
        sourceSets["iosArm64Main"].dependsOn(sharedSourceSet)
        sourceSets["iosX64Main"].dependsOn(sharedSourceSet)
    }
}

fun jacksonDependencies(jacksonVersion: String) = listOf(
    "com.fasterxml.jackson.core:jackson-core:$jacksonVersion",
    "com.fasterxml.jackson.core:jackson-databind:$jacksonVersion",
    "com.fasterxml.jackson.core:jackson-annotations:$jacksonVersion",
    "com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion",
    "com.fasterxml.jackson.dataformat:jackson-dataformat-xml:$jacksonVersion",
    "com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:$jacksonVersion",
    "com.fasterxml.jackson.jaxrs:jackson-jaxrs-xml-provider:$jacksonVersion",
    "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacksonVersion"
)

fun RepositoryHandler.mavenFlowstorm(uri: URI, auth: String) {
    maven {
        url = uri
        when (auth) {
            "gitlab" -> {
                credentials(HttpHeaderCredentials::class) {
                    System.getenv("GITLAB_PRIVATE_TOKEN")?.let {
                        name = "Private-Token"
                        value = it
                    } ?: System.getenv("GITLAB_DEPLOY_TOKEN")?.let {
                        name = "Deploy-Token"
                        value = it
                    } ?: System.getenv("CI_JOB_TOKEN")?.let {
                        name = "Job-Token"
                        value = it
                    }
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}

fun Project.filterResources(tasks: TaskCollection<ProcessResources>) = tasks.forEach {
    filterResources(it)
}

fun Project.filterResources(processResources: ProcessResources) = with (processResources) {
    filesMatching("**/*.properties") {
        filter<ReplaceTokens>("tokens" to properties.filter { it.value is String })
    }
    duplicatesStrategy = DuplicatesStrategy.WARN
}