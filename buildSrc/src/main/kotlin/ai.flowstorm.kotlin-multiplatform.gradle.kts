plugins {
    kotlin("multiplatform")
    id("maven-publish")
    id("java-library")
    id("org.jetbrains.dokka")
}

val kotlinVersion = findProperty("kotlin.version") as String
val jacksonVersion = findProperty("jackson.version") as String

withGitProperties()

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
        withJava()
    }

    sourceSets {
        val commonMain by getting
        val jvmMain by getting {
            dependencies {
                jacksonDependencies(jacksonVersion).forEach { dependency ->
                    api(dependency)
                }
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-test:$kotlinVersion")
                implementation("org.assertj:assertj-core:3.12.2")
                implementation("org.junit.jupiter:junit-jupiter:5.6.0")
                implementation("io.mockk:mockk:1.10.2") {
                    exclude("org.jetbrains.kotlin", "kotlin-reflect")
                }
            }
        }
    }
}

repositories {
    mavenLocal()
    mavenCentral()
    maven { url = uri("https://gitlab.com/api/v4/groups/5074098/-/packages/maven") }
    maven { url = uri("https://csspeechstorage.blob.core.windows.net/maven/") }
    maven { url = uri("https://plugins.gradle.org/m2/") }
    maven { url = uri("https://jitpack.io") }
}

publishing {
    repositories {
        mavenFlowstorm(
            uri(findProperty("maven.repository.uri") as String),
            findProperty("maven.repository.auth") as String
        )
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

filterResources(tasks.getByName<ProcessResources>("jvmProcessResources"))

tasks.getByName<ProcessResources>("jvmTestProcessResources") {
    duplicatesStrategy = DuplicatesStrategy.WARN
}
