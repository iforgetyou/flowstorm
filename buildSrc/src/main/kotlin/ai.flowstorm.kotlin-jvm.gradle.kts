import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "ai.flowstorm.core"

val kotlinVersion = findProperty("kotlin.version") as String
val jacksonVersion = findProperty("jackson.version") as String

withGitProperties()

plugins {
    kotlin("jvm")
    id("maven-publish")
    id("org.jetbrains.dokka")
}

repositories {
    mavenLocal()
    mavenCentral()
    maven { url = uri("https://gitlab.com/api/v4/groups/5074098/-/packages/maven") }
    maven { url = uri("https://repo.maven.apache.org/maven2/") }
    maven { url = uri("https://csspeechstorage.blob.core.windows.net/maven/") }
    maven { url = uri("https://jitpack.io") }
}

dependencies {
    api("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
    jacksonDependencies(jacksonVersion).forEach { dependency ->
        api(dependency)
    }
    testImplementation("org.jetbrains.kotlin:kotlin-test:$kotlinVersion")
    testImplementation("org.assertj:assertj-core:3.12.2")
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.0")
    testImplementation("io.mockk:mockk:1.10.2") {
        exclude("org.jetbrains.kotlin", "kotlin-reflect")
    }
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar.get())
        }
    }
    repositories {
        mavenFlowstorm(
            uri(findProperty("maven.repository.uri") as String),
            findProperty("maven.repository.auth") as String
        )
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
}

filterResources(tasks.withType())
