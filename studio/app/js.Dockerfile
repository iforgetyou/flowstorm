FROM registry.gitlab.com/promethistai/system/builder AS builder

COPY ./build/libs libs
RUN cd libs && unzip flowstorm-studio-app-js.zip && rm *.zip

FROM registry.gitlab.com/promethistai/system/nginx
COPY --from=builder libs/ /var/www/html