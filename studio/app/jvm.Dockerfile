FROM registry.gitlab.com/promethistai/system/java/openjdk11-jre

RUN apk add --no-cache bash && rm -rf /var/cache/apk/*

COPY build/distributions/ /tmp

RUN tar xvf /tmp/*.tar --strip-components 1 && rm /tmp/*.tar

CMD ["sh", "-c", "bin/flowstorm-studio-app $JAVA_OPTIONS $APP_OPTIONS"]