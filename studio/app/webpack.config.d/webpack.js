webpack = require('webpack')

config.resolve.modules.push("studio/app/build/processedResources/js/main");

config.resolve.fallback = {
    "util": require.resolve("util/"),
    "crypto": false
}

config.plugins.push(
    new webpack.ProvidePlugin({
        process: 'process/browser',
    }),
)
