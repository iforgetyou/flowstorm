import org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsRootPlugin
import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpack
import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpackConfig
import org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsRootExtension

group = "ai.flowstorm.studio"
description = "Flowstorm Studio App"

val isProduction = findProperty("production") == "true"
val kotlinVersion = findProperty("kotlin.version") as String
val kvisionVersion = findProperty("kvision.version") as String
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String
val promethistaiServicesVersion = findProperty("promethistai.services.version") as String
val webDir = file("src/jsMain/web")

buildscript {
    extra.set("production", findProperty("production") == "true")
}

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
    id("kotlinx-serialization") version "1.5.21"
    application
}

kotlin {
    js {
        compilations.all {
            kotlinOptions {
                moduleKind = "umd"
                sourceMap = !isProduction
                if (!isProduction) {
                    sourceMapEmbedSources = "always"
                }
            }
        }
        browser {
            runTask {
                sourceMaps = true
                devServer = KotlinWebpackConfig.DevServer(
                    open = false,
                    port = 3000,
                    static = mutableListOf("$buildDir/processedResources/js/main")
                )
            }
            testTask {
                useKarma {
                    useChromeHeadless()
                }
            }
        }
        binaries.executable()
    }

    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(project(":flowstorm-studio-api"))
                implementation(project(":flowstorm-core-api"))
                implementation(project(":flowstorm-common-app"))
                implementation("com.twilio.sdk:twilio:7.55.1")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.0")
            }
        }
        val jsMain by getting {
            resources.srcDir(webDir)
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation("ai.promethist.services:promethistai-services-app-js:$promethistaiServicesVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
                implementation("com.benasher44:uuid:0.1.0")

                // npm
                implementation(npm("react-awesome-button", "6.5.1"))
                implementation(npm("auth0-js", "9.14.0"))
                implementation(npm("auth0-lock", "11.27.2"))
                implementation(npm("jwt-decode", "2.2.0"))
                implementation(npm("finderjs", "1.3.0"))
                implementation(npm("showdown", "1.9.1"))
                implementation(npm("js-cookie", "2.2.1"))
                implementation(npm("util", "0.12.3"))
                implementation(npm("process", "0.11.10"))
                if (findProperty("useLocalNpm") == "true") {
                    implementation(npm("@flowstorm/bot-ui", File("$projectDir/../../client/web/lib/ui/dist")))
                    implementation(npm("@flowstorm/bot-service", File("$projectDir/../../client/web/lib/service/dist")))
                } else {
                    implementation(npm("@flowstorm/bot-ui", "2.79.0"))
                    implementation(npm("@flowstorm/bot-service", "2.79.0"))
                }
                implementation(npm("@fortawesome/fontawesome-pro", "5.15.1"))
                implementation(npm("handsontable", "8.4.0"))

                // implementation(npm(projectDir.resolve("src/jsMain/resources/css")))
                implementation(npm("sass", "1.32.4"))
                implementation(devNpm("sass-loader", "^7.1.0"))
                implementation(devNpm("css-loader", "^4.2.0"))
                implementation(devNpm("style-loader","^1.2.0"))
                implementation(devNpm("bootstrap","^4.6.0"))

                // we don't actually need this dependency, its required by 'io.kvision:kvision-bootstrap-upload'
                // it's declared here to force version 5.2.8 later version causes build faliure
                implementation(npm("bootstrap-fileinput", "5.2.8"))

                // kvision
                implementation("io.kvision:kvision:$kvisionVersion")
                implementation("io.kvision:kvision-bootstrap:$kvisionVersion")
                implementation("io.kvision:kvision-bootstrap-datetime:$kvisionVersion")
                implementation("io.kvision:kvision-bootstrap-select:$kvisionVersion")
                implementation("io.kvision:kvision-bootstrap-spinner:$kvisionVersion")
                implementation("io.kvision:kvision-bootstrap-upload:$kvisionVersion")
                implementation("io.kvision:kvision-bootstrap-dialog:$kvisionVersion")
                implementation("io.kvision:kvision-bootstrap-typeahead:$kvisionVersion")
                implementation("io.kvision:kvision-i18n:$kvisionVersion")
                implementation("io.kvision:kvision-richtext:$kvisionVersion")
                implementation("io.kvision:kvision-handlebars:$kvisionVersion")
                implementation("io.kvision:kvision-datacontainer:$kvisionVersion")
                implementation("io.kvision:kvision-chart:$kvisionVersion")
                implementation("io.kvision:kvision-tabulator:$kvisionVersion")
                implementation("io.kvision:kvision-pace:$kvisionVersion")
                implementation("io.kvision:kvision-toast:$kvisionVersion")
                implementation("io.kvision:kvision-react:$kvisionVersion")
                implementation("io.kvision:kvision-moment:$kvisionVersion")
                implementation("io.kvision:kvision-routing-navigo:$kvisionVersion")
                implementation("io.kvision:kvision-event-flow:$kvisionVersion")
            }
        }
        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
                implementation("io.kvision:kvision-testutils:$kvisionVersion:tests")
            }
        }
    }
}

application {
    mainClass.set("ai.flowstorm.studio.Application")
    applicationDefaultJvmArgs = listOf("-Dai.flowstorm.common.server.port=8089")
}

tasks.getByName<Jar>("jvmJar") {
    archiveAppendix.set("") // discard jvm appendix to make distribution bundle work
}

tasks.register<Copy>("sass") {
    from("src/jsMain/resources/css")
    into("build/js/node_modules/css")
}

tasks["jsProcessResources"].dependsOn(tasks["sass"])

tasks.register("prepareKotlinBuildScriptModel") {
    //nothing - workaround to fix build error
}

fun getNodeJsBinaryExecutable(): String {
    val nodeDir = NodeJsRootPlugin.apply(rootProject).nodeJsSetupTaskProvider.get().destination
    val isWindows = System.getProperty("os.name").toLowerCase().contains("windows")
    val nodeBinDir = if (isWindows) nodeDir else nodeDir.resolve("bin")
    val command = NodeJsRootPlugin.apply(rootProject).nodeCommand
    val finalCommand = if (isWindows && command == "node") "node.exe" else command
    return nodeBinDir.resolve(finalCommand).absolutePath
}

tasks {
    create("generatePotFile", Exec::class) {
        dependsOn("compileKotlinJs")
        executable = getNodeJsBinaryExecutable()
        args("${rootProject.buildDir}/js/node_modules/gettext-extract/bin/gettext-extract")
        inputs.files(kotlin.sourceSets["jsMain"].kotlin.files)
        outputs.file("$projectDir/src/jsMain/resources/i18n/messages.pot")
    }
}

afterEvaluate {
    tasks {
        getByName("jsProcessResources", Copy::class) {
            dependsOn("compileKotlinJs")
            exclude("**/*.pot")
            doLast("Convert PO to JSON") {
                destinationDir.walkTopDown().filter {
                    it.isFile && it.extension == "po"
                }.forEach {
                    exec {
                        executable = getNodeJsBinaryExecutable()
                        args(
                            "${rootProject.buildDir}/js/node_modules/gettext.js/bin/po2json",
                            it.absolutePath,
                            "${it.parent}/${it.nameWithoutExtension}.json"
                        )
                        println("Converted ${it.name} to ${it.nameWithoutExtension}.json")
                    }
                    it.delete()
                }
            }
        }

        create("zip", Zip::class) {
            dependsOn("jsBrowserProductionWebpack")
            archiveFileName.set("flowstorm-studio-app-js.zip")
            group = "package"
            destinationDirectory.set(file("$buildDir/libs"))
            val distribution =
                project.tasks.getByName("jsBrowserProductionWebpack", KotlinWebpack::class).destinationDirectory!!
            from(distribution) {
                include("*.*")
            }
            from(webDir)
            duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            inputs.files(distribution, webDir)
            outputs.file(archiveFile)
        }
    }
}

/**
 * Workaround for issue KT-48273 (https://youtrack.jetbrains.com/issue/KT-48273)
 *
 * Task jsBrowserRun was failing with:
 * > [webpack-cli] Invalid configuration object. Object has been initialized using a configuration object that does not match the API schema.
 * - configuration has an unknown property '_assetEmittingWrittenFiles'
 */
rootProject.plugins.withType(NodeJsRootPlugin::class.java) {
    rootProject.the<NodeJsRootExtension>().versions.webpackDevServer.version =
        "4.9.2"
}

/**
 * Workaround for issue KT-49124 (https://youtrack.jetbrains.com/issue/KT-49124)
 *
 * Task jsBrowserRun was failing with:
 * [webpack-cli] Unable to load '@webpack-cli/serve' command
 * [webpack-cli] TypeError: options.forEach is not a function
 * [webpack-cli] TypeError: options.forEach is not a function
 */

rootProject.extensions.configure<NodeJsRootExtension> {
    versions.webpackCli.version = "4.10.0"
}
