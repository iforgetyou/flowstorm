package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.Turn
import ai.flowstorm.studio.resources.space.SessionResource
import kotlinx.coroutines.launch
import ai.flowstorm.common.BotLogic
import ai.flowstorm.common.id.StringId

class LastTurnPanel : JsonPanel<Turn>(Turn.serializer()) {

    override fun reload() {
        if (!lock && BotLogic.sessionId != null) {
            mainScope.launch {
                val session = SessionResource.last.load(StringId(BotLogic.sessionId!!))
                if (session.turns.isNotEmpty())
                    setData(session.turns[session.turns.size - 1])
            }
        }
    }
}
