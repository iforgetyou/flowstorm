package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.*
import ai.flowstorm.studio.ui.data.ReportForm
import ai.flowstorm.studio.ui.data.ReportViewer
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.ui.CardPanel
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import io.kvision.panel.hPanel
import io.kvision.utils.px
import ai.flowstorm.studio.resources.space.ApplicationResource as OrgApplicationModel
import ai.flowstorm.studio.resources.space.UserResource as OrgUserModel
import ai.flowstorm.studio.resources.space.report.MetricResource as OrgMetricModel
import ai.flowstorm.studio.resources.space.report.NamespaceResource as OrgNamespaceModel
import ai.flowstorm.studio.resources.space.report.ReportResource as OrgReportModel

open class ReportPanel(val singleSpace: Boolean = true) : CardPanel("Metrics Reports"), Reloadable {

    val mainScope = MainScope()
    val reportViewer = ReportViewer(reportModel = if (singleSpace) OrgReportModel else ReportResource)
    val reportForm = ReportForm(singleSpace)

    init {
        addPanelHeader()

        reportForm.width = 320.px
        hPanel(classes = setOf("content-panel")) {
            add(reportForm)
            add(reportViewer, grow = 1)
        }
    }

    lateinit var subscriber: () -> Unit

    override fun reload() {
        if (!::subscriber.isInitialized) {
            reportForm.reportSettings.subscribe {
                reportViewer.reportSettings = it
                reportViewer.reload()
            }
        }

        mainScope.launch {

            if (singleSpace) {
                reportForm.updateOptions(
                        OrgUserModel.load(),
                        OrgMetricModel.load(),
                        OrgNamespaceModel.load(),
                        OrgApplicationModel.load()
                )
            } else {
                reportForm.updateOptions(
                        UserResource.load(),
                        MetricResource.load(),
                        NamespaceResource.load(),
                        ApplicationResource.load(),
                        SpaceResource.load(),
                )
            }

            reportForm.reload()
            reportViewer.reload()
        }
    }
}