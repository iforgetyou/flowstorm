package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Group
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import io.kvision.core.onEvent
import io.kvision.form.text.Text
import io.kvision.i18n.I18n
import io.kvision.i18n.I18n.tr

class GroupEditor(graphEditor: GraphEditor) : NodeEditor<Group>(graphEditor), EditableCode {

    override val codeEditor = CodeEditor(classes = setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.code != self.value) {
                    node?.code = self.value
                    graphEditor.commit(I18n.gettext("Group code changed") + " [#${node!!.id}]", "group[${node!!.id}].code")
                }
            }
        }
    }

    override val form = object : NodeForm(Group.serializer()) {
        init {
            add(
                Group::label,
                Text(label = tr("Label")),
            )
            add(Group::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { I18n.gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
        }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Code"), codeEditor, Icon.Function.def)
        }
    }

    override fun load(node: Group) {
        super.load(node)
        form.load(node)
        codeEditor.set(node.code)
    }
}