package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import kotlinx.serialization.Serializable

@Serializable
data class SearchResult(
    val id: Id,
    val entity: String,
    val name: String,
    val score: Float,
    val highlights: List<HighLight> = listOf(),
    val data: Map<String, String> = mapOf(),
)

@Serializable
data class HighLight(
    val score: Float,
    val path: String,
    val texts: List<HighLightText>
)

@Serializable
data class HighLightText(
    val value: String,
    val type: String,
)
