package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Terms
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.form.CodeFormControl
import io.kvision.form.FormHorizontalRatio
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class TermsForm(model: EntityResource<Terms>) : EntityForm<Terms>(model, FormHorizontalRatio.RATIO_2) {

    private val nameInput = Text(label = tr("Name"))

    init {
        add(
                Terms::name,
                nameInput,
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Terms::title,
                Text(label = tr("Title")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Terms::text,
                CodeFormControl(label = tr("Text"), mode = "markdown")
        )
        add(
                Terms::newVersion,
                CheckBox(label = tr("New Version"))
        )
    }

    override fun load(entity: Terms): Terms {
        nameInput.readonly = entity.name.isNotBlank()
        return super.load(entity)
    }

    override suspend fun createOrUpdate(entity: Terms) {
        val e = if (entity.newVersion)
            entity.copy(_id = newId())
        else entity
        super.createOrUpdate(e)
        load(e)
    }
}