package ai.flowstorm.studio.ui.form

import ai.flowstorm.studio.resources.space.VoiceResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.SelectId
import io.kvision.i18n.tr

class VoiceId : SelectId(label = tr("Voice")) {

    val mainScope = MainScope()

    fun load(language: String?) = mainScope.launch {
        val voices = VoiceResource.loadForLanguage(language)
        options = UiHelpers.options(voices, true)
        console.log("voices loaded", voices)
    }
}