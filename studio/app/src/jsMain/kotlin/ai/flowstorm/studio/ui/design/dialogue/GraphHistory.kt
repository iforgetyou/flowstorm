package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.Graph
import io.kvision.i18n.gettext
import io.kvision.state.ObservableValue
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.js.Date

class GraphHistory {
    val items = mutableListOf<HistoryItem>()
    var rev: Int = 0
        set(value) {
            field = value
            update()
        }
    val currentItem get() = items.first { it.rev == rev }

    val canUndo = ObservableValue<Boolean>(false)
    val canRedo = ObservableValue<Boolean>(false)

    fun commit(graph: Graph, note: String, squashId: String?) {
        items.removeAll(items.takeLastWhile { it.rev > rev })
        var newRev: Int
        if (squashId != null && currentItem.squasId == squashId) {
            items.removeLast()
            newRev = rev
        } else {
            newRev = rev + 1
        }
        items.add(HistoryItem(graph.clone(), note, rev = newRev, squasId = squashId))

        rev = newRev
        while (items.size > MAX_ITEMS) items.removeFirst()
    }

    fun restore(rev: Int) {
        val item = items.first { it.rev == rev }
        commit(item.graph, gettext("Restored revision") + " $rev", null)
    }

    fun set(rev: Int) {
        this.rev = rev
    }

    fun undo() = set(rev - 1)
    fun redo() = set(rev + 1)

    fun current(): Graph = currentItem.graph

    private fun update() {
        canRedo.value = items.last().rev > rev
        canUndo.value = items.any { 0 < it.rev && it.rev < rev }
    }

    data class HistoryItem(val graph: Graph, val note: String = "", val time: Instant = Clock.System.now(), val rev: Int = 0, val squasId: String? = null)

    companion object {
        const val MAX_ITEMS = 50
    }
}