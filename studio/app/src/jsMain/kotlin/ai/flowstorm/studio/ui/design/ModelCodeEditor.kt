package ai.flowstorm.studio.ui.design

import ai.flowstorm.common.ui.CodeEditor

class ModelCodeEditor(mode: String = "kotlin") : CodeEditor(mode, setOf("panel-view-nav1"))