package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Namespace
import ai.flowstorm.common.resources.EntityResource

object NamespaceResource : EntityResource<Namespace>(serializer = Namespace.serializer()) {

    override val resourceUri: String
        get() = "/reports/namespaces"

}
