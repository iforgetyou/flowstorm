package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.Graph.Node
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import io.kvision.core.onEvent
import io.kvision.panel.SimplePanel

class DialogueMixinPanel(val graphEditor: GraphEditor, nodeProvider: ObjectProvider<Node>) : NodeListPanel(nodeProvider) {

    override fun row(item: Node, index: Int, list: MutableList<Node>): SimplePanel =
            super.row(item, index, list).apply {
                onEvent {
                    dragstart = { dragStart(item) }
                }
            }

    private fun dragStart(node: Node) {
        graphEditor.draggedNode = Node.copy(node)
    }
}