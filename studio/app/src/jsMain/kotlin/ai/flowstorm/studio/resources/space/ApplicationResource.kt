package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Application
import ai.flowstorm.common.resources.EntityResource
import io.kvision.state.subFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object ApplicationResource : EntityResource<Application>(serializer = Application.serializer()) {

    init {
        AppState.subFlow { it.spaceId }.onEach { clear() }.launchIn(App.scope)
    }

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/applications"
}
