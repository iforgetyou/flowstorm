package ai.flowstorm.studio.model

import ai.flowstorm.studio.model.analytics.MetricValue
import ai.flowstorm.studio.util.toDate
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.core.type.Location
import kotlinx.serialization.*
import io.kvision.i18n.gettext
import io.kvision.moment.Moment
import kotlinx.datetime.*
import kotlinx.serialization.json.JsonElement

@Serializable
data class Session(
    override val _id: Id = newId(),
    var datetime: Instant,
    var sessionId: String,
    var test: Boolean = false,
    var location: Location? = null,
    var clientType: String? = null,
    var initiationId: String? = null,
    var metrics: List<MetricValue> = listOf(),
    var turns: List<Turn> = listOf(),
    var user: User,
    var application: Application? = null,
    val space_id: Id,
    val attributes: JsonElement,
    val properties: JsonElement,
    val clientLogEntries: List<LogEntry> = listOf()
) : AbstractEntity() {

    val utterances: List<Utterance> get() =
        mutableListOf<Utterance>().apply {
            turns.forEach { turn ->
                add(Utterance(turn, Utterance.Persona.User))
                add(Utterance(turn, Utterance.Persona.Bot))
            }
        }

    override fun match(input: String): Boolean {
        val lowerInput = input.lowercase()
        return lowerInput in ("${user.name ?: ""} ${user.surname ?: ""}").lowercase()
            || lowerInput in (application?.name ?: "").lowercase()
            || lowerInput in (clientType ?: "").lowercase()
    }

    override val label: String
        get() = sessionId

    val turnCount by turns::size

    val locationAsString = location?.toString() ?: ""
    val applicationName: String? = application?.name
    val dialogue_id: Id? = application?.dialogue_id

    /*
     For unknown reason(bug in serializer?) this can't be inlined and initialization is in init block.
     Serializer throws exception "this.user is undefined"
     */
    var userName = ""
    var stringUser = ""
    var stringTime: String? = null

    init {
        stringTime = datetime.let { Moment(it.toDate()).format(gettext("YYYY/MM/DD HH:mm"))} as? String
        userName = user.name + " " + user.surname
        stringUser = User.asString(user.name, user.surname, user.username)
    }

    @Serializable
    data class DialogueStackFrame(val id: String? = null, val nodeId: Int, val name: String? = null)

}




