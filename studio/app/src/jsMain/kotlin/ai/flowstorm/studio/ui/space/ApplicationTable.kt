package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.resources.space.ApplicationResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.core.Component
import io.kvision.i18n.I18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

class ApplicationTable: EntityTable<Application>(
        caption = "Application",
        model = ApplicationResource,
        tabulatorOptions = TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), Application::name.name)
                )
        ),
        entityComponentFactory = { ApplicationRunnableEditor() },
        entityFactory = { Application(space_id = AppState.state.spaceId!!) },
        deleteEnabled = false,
        splitMode = true,
        tabWidth = 240.px,
        saveEnabled = false,
        className = "content-panel"
) {
    override fun showEntityComponent(entityComponent: Component) {
        super.showEntityComponent(entityComponent)
        // (entityComponent as ApplicationRunnableEditor).updateMainPanelHeight()
    }
}
