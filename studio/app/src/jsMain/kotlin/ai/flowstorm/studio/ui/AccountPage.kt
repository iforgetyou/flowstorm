package ai.flowstorm.studio.ui

import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Icon
import io.kvision.html.*
import io.kvision.panel.hPanel
import io.kvision.panel.simplePanel

class AccountPage : PagePanel() {
    init {
        body.apply {
            cardPanel("My Account", icon = Icon.Account, className = "body-panel") {
                addPanelHeader()
                hPanel(className = "content-panel") {
                    simplePanel {
                        margin = UiHelpers.spacing
                        p("Account management will come soon. Enjoy the free ride for now ;-)")
                    }
                }
            }
        }
    }
}