package ai.flowstorm.studio.ui

import ai.flowstorm.studio.model.File
import ai.flowstorm.studio.resources.space.FileAssetResource
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.TableEditor
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.form.upload.Upload
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.panel.simplePanel
import io.kvision.utils.getDataWithFileContent
import io.kvision.utils.px

class FileAssetForm() : EntityForm<File>(FileAssetResource) {

    private val preview: SimplePanel
    private val link: Link
    private var previewDiv: SimplePanel = Div()
    private val csvMimeTypes = listOf("text/csv", "application/vnd.ms-excel")

    private fun isCSV(type: String): Boolean {
        return csvMimeTypes.contains(type)
    }

    init {
        add(
            File::name,
            Text(label = tr("Name")),
            required = true,
            requiredMessage = tr("Required field")
        )
        add(
            File::description,
            TextArea(label = tr("Description"))
        )
        link = link("")
        preview = simplePanel()
        add(File::upload, Upload("/", label = tr("File")).apply {
            showUpload = false
            showCancel = false
            dropZoneEnabled = false
            allowedFileExtensions = setOf("jpg", "jpeg", "png", "mp3", "mp4", "json", "gif", "csv", "m4a", "wav")
        })
    }

    override fun load(entity: File) = super.load(entity).also {
        entity.source?.let {
            link.label = it
            link.url = it
            link.target = "_blank"
        }
        preview.removeAll()
        if (entity.source != null) {
            if (isCSV(entity.type)) {
                previewDiv = TableEditor(entity.source, entity.type)
            } else {
                previewDiv = FileAssetPreview(entity, classes = setOf("asset-preview")) {
                    if (entity.type.startsWith("image/")) {
                        height = 480.px
                    }
                }
            }
            preview.add(previewDiv)
        }
    }
    override suspend fun createOrUpdate(entity: File) {
        val data = getDataWithFileContent()

        // If CSV, load the changes from editor
        if (isCSV(entity.type) && data.upload == null) {
            val tableAssetData = (previewDiv as TableEditor).hot.getData() as Array<Array<String?>>
            val delimiter = (previewDiv as TableEditor).delimiter
            val stringAssetData = tableAssetData.joinToString("\n") { line ->
                line.joinToString(delimiter) {
                    val elem = it ?: ""
                    if (elem.contains(delimiter)) "\"$elem\"" else elem
                }
            }
            val encoded = js("window.btoa(unescape(encodeURIComponent(stringAssetData)));")
            entity.source = "data:${entity.type};base64,${encoded}"
        }

        // If there are files in the upload widget, load them to the asset
        data.upload?.firstOrNull()?.let { file ->
            console.log("Asset file ${file.name}, size=${file.size}, content.length=${file.content?.length}")
            entity.source = file.content ?: ""
        }
        super.createOrUpdate(entity)
    }
}
