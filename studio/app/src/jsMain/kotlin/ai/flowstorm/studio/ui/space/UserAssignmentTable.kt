package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.resources.space.UserAssignmentResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.modal.ModalSize
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions

class UserAssignmentTable: EntityTable<Space.UserAssignment>(
        "User Assignment",
        UserAssignmentResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Username"), Space.UserAssignment::_user.name + "." + User::username.name),
                        ColumnDefinition(tr("Name"), Space.UserAssignment::_user.name + "." + User::name.name),
                        ColumnDefinition(tr("Surname"), Space.UserAssignment::_user.name + "." + User::surname.name),
                        ColumnDefinition(tr("Role"), Space.UserAssignment::roleName.name),
                        ColumnDefinition(tr("Note"), Space.UserAssignment::note.name)
                )
        ),
        { UserAssignmentEditor() },
        { Space.UserAssignment(_user = User()) },
        { UserAssignmentCreator() },
        modalSize = ModalSize.LARGE,
        className = "content-panel",
        seekEnabled = false,
)