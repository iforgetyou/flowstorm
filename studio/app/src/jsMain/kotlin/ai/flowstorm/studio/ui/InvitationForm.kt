package ai.flowstorm.studio.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.studio.model.Space
import ai.flowstorm.common.resources.EntityResource
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.form.text.TextInputType
import io.kvision.i18n.tr

open class InvitationForm(model: EntityResource<Invitation>): EntityForm<Invitation>(model) {
    init {
        add(
                Invitation::username,
                Text(label = tr("Username"), type = TextInputType.EMAIL),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Invitation::role,
                Select(label = tr("Role"), options = UiHelpers.options(Space.Role.values().filter { Space.Role.isAllowed(it, AppState.state.selectedRole!!.role) }.toTypedArray())),
                required = true,
                requiredMessage = tr("Required field")
        )
    }

    override suspend fun save() {
        entity!!.space_id = AppState.state.spaceId!!
        super.save()
    }
}
