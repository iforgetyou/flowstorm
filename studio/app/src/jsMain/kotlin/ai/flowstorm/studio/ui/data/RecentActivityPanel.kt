package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.resources.space.ActivityResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.Query
import io.kvision.html.ul
import io.kvision.html.li
import io.kvision.i18n.gettext
import io.kvision.panel.SimplePanel
import kotlin.math.min

class RecentActivityPanel(limit: Int = 10) : SimplePanel(), Reloadable {

    private val mainScope = MainScope()
    private val query = Query(limit = limit, filters = mutableListOf(Query.Filter("type", Query.Operator.ne, "Used")))

    override fun reload() {
        mainScope.launch {
            with (ActivityResource) {
                reload(query)
                removeAll()
                ul {
                    for (it in entities.take(10)) {
                        li("<i>${it.datetimeString}:</i> <b>${it.username}</b> ${ gettext(it.type.lowercase())} ${convertEntityName(it.name)} <b>${createLink(it.name, it.refName, it.ref_id)}</b>", rich = true)
                    }
                }
            }
        }
    }

    private fun convertEntityName(entityName: String): String =
        when(entityName) {
            "DialogueSource" -> "the dialogue"
            "DialogueMixin" -> "the mixin"
            "DialogueSnippet" -> "the snippet"
            else -> "the ${ entityName.split(Regex("(?<=.)(?=\\p{Lu})")).joinToString(" ") { it.lowercase() }}"
        }

    private fun createLink(type: String, name: String, id: Id): String =
        if (type == "DialogueSource") {
            "<a href=\"#!/space/dialogue/${id.value}\">${name}</a>"
        } else {
            name
        }
}