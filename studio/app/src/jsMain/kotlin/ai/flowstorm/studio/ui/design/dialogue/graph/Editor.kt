package ai.flowstorm.studio.ui.design.dialogue.graph

import io.kvision.core.Component

interface Editor : Component {
    fun load(item: Any)
}