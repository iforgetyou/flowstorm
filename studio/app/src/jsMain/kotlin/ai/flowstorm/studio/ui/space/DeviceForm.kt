package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.ui.form.UserId
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import io.kvision.form.text.Password
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.i18n.tr

class DeviceForm(model: EntityResource<Device>) : EntityForm<Device>(model) {

    init {
        add(
                Device::deviceId,
                Text(label = tr("Device ID")),
                required = true,
                requiredMessage = tr("Required field")
        ) {
            (it.value?.length ?: 0) > 0
        }
        add(
                Device::deviceToken,
                Password(label = tr("Device Token")).apply {
                    input.setAttribute("autocomplete", "new-password")
                }
        )
        add(
                Device::user_id,
                UserId(tr("Assigned User"))
        )
        add(
                Device::description,
                Text(label = tr("Description")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Device::location,
                Text(label = tr("Location")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Device::configuration,
                TextArea(label = tr("Configuration"), rows = 6)
        )
    }

    override fun load(entity: Device): Device {
        form.fields["deviceId"]!!.disabled = (entity.deviceId != null)
        return super.load(entity)
    }
}