package ai.flowstorm.studio.util

import ai.flowstorm.studio.App
import kotlinx.datetime.*
import kotlin.js.Date

fun Instant.toDate(timeZone: TimeZone = App.timeZone): Date = toLocalDateTime(timeZone).let {
    Date(
        it.year,
        it.monthNumber - 1,
        it.dayOfMonth,
        it.hour,
        it.minute,
        it.second
    )
}

fun Date.toInstant(timeZone: TimeZone = App.timeZone): Instant =
    LocalDateTime(getFullYear(), getMonth() + 1, getDate(), getHours(), getMinutes(), getSeconds()).toInstant(timeZone)

fun Date.dayStart(): Date = Date(getFullYear(), getMonth(), getDate(), 0, 0, 0)
fun Date.dayEnd(): Date = Date(getFullYear(), getMonth(), getDate(), 23, 59, 59)

fun nowLocal(timeZone: TimeZone = App.timeZone): LocalDateTime = now().toLocalDateTime(timeZone)
fun now(): Instant = Clock.System.now()