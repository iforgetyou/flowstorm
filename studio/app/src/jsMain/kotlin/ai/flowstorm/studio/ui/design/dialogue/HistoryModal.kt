package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.util.toDate
import kotlinx.coroutines.Job
import io.kvision.core.Color
import io.kvision.core.onEvent
import io.kvision.html.ButtonStyle
import io.kvision.html.Span
import io.kvision.html.button
import io.kvision.html.span
import io.kvision.i18n.gettext
import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.moment.Moment
import io.kvision.table.cell
import io.kvision.table.row
import io.kvision.table.table

class HistoryModal(val editor: GraphEditor) : Modal(tr("History"), size = ModalSize.LARGE, scrollable = true) {

    private val saveClick = Job()
    private val validationCheck = Span {
        color = Color("black")
    }

    init {
        closeIcon.onEvent {
            click = { cancel() }
        }
        table {
            editor.history.items.reversed().forEach{ item ->
                row {
                    cell(item.rev.toString())
                    cell(Moment(item.time.toDate()).fromNow() as String)
                    cell(item.note)
                    cell {
                        if (item.rev == editor.history.rev) span(gettext("current state")) else {
                            button(tr("Revert"), style = ButtonStyle.SECONDARY, classes = setOf("btn-sm")).onClick {
                                editor.restore(item.rev)
                                cancel()
                            }
                            button(tr("Load"), style = ButtonStyle.SECONDARY, classes = setOf("btn-sm")).onClick {
                                editor.openRevision(item.rev)
                                cancel()
                            }
                        }
                    }
                }
            }
        }
    }

    fun open() {
        show()
    }

    fun cancel() {
        hide()
    }
}