package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class Profile(
    override val _id: Id,
    val user_id: Id,
    val space_id: Id,
    val attributes: JsonElement
) : AbstractEntity() {
    override val label get() = id.value
}
