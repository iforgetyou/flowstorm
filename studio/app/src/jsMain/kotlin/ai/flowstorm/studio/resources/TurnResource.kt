package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Turn
import ai.flowstorm.common.resources.EntityResource
import io.kvision.state.ObservableList

class TurnResource(entities: ObservableList<Turn>) : EntityResource<Turn>(serializer = Turn.serializer(), entities = entities)