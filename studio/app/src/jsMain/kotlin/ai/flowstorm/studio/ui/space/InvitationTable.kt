package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.studio.resources.space.InvitationResource
import ai.flowstorm.studio.ui.InvitationEditor
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions

class InvitationTable : EntityTable<Invitation>(
        "User Invitation",
        InvitationResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Username"), Invitation::username.name),
                        ColumnDefinition(tr("Role"), Invitation::role.name)
                )
        ),
        { InvitationEditor() },
        { Invitation() },
        { InvitationCreator() },
        saveEnabled = false,
        deleteEnabled = false,
        className = "content-panel"
)