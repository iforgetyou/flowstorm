package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Turn
import ai.flowstorm.studio.resources.TurnResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.modal.ModalSize
import io.kvision.state.ObservableList
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Formatter
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.obj
import ai.flowstorm.common.setProperty


class TurnTable(turns: ObservableList<Turn>) : EntityTable<Turn>(
        "Turn",
        TurnResource(turns),
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Time"), Turn::timeAsString.name, width = "13%"),
                        ColumnDefinition(tr("Duration"), Turn::duration.name, width = "5%"),
                        ColumnDefinition(tr("Transcript"), Turn::transcript.name, width = "12%", formatter = Formatter.TEXTAREA),
                        ColumnDefinition(tr("Response items"), Turn::responseText.name, width = "50%", formatter = Formatter.TEXTAREA),
                                // TODO Formatter.HTML
                                // TODO Once a way to display images correctly in the table without hacks is found,
                                // TODO add images and audio
                                // , variableHeight = true, formatterParams = js("{height:\"100%\"}")),
                        ColumnDefinition(tr("End frame"), Turn::endFrameName.name, width = "20%")
                        //,
                        // Workaround for cells not displaying in full height
                        // ColumnDefinition(tr(" "), "image", width = "0%", formatter = Formatter.IMAGE,
                        //      formatterParams = js("{height:\"1px\",width:\"1px\"}"), visible = false)
                )
        ),
        { TurnViewer(TurnResource(turns)) },
        deleteEnabled = false,
        saveEnabled = false,
        modalSize = ModalSize.XLARGE,
        hideHeader = true,
        dataExtractor = {
                obj {
                        setProperty(this, it::timeAsString)
                        setProperty(this, it::duration)
                        setProperty(this, it::transcript)
                        setProperty(this, it::responseText)
                        setProperty(this, it::endFrameName)
                }
        },
)
