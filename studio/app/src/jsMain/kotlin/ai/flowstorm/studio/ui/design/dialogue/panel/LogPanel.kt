package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.LogEntry
import com.github.snabbdom.VNode
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ai.flowstorm.common.Lockable
import org.w3c.dom.HTMLElement
import io.kvision.core.Overflow
import io.kvision.core.onEvent
import io.kvision.data.dataContainer
import io.kvision.html.TAG
import io.kvision.html.Tag
import io.kvision.html.span
import io.kvision.panel.SimplePanel
import io.kvision.state.observableListOf

open class LogPanel : SimplePanel(classes = setOf("panel-view-nav1")), Lockable {
    data class LogLine(val text: String, val rich: Boolean, val type: String = "dark")

    var lines: MutableList<LogLine> = observableListOf()
    override var lock = false

    var lastScrollTop: Double = 0.0

    companion object {
        fun convertToClickable(logEntry: LogEntry, type: String): LogLine{
            var logText = "+${logEntry.relativeTime}:${logEntry.level}[${logEntry.text}]"
            var isRich = false
            logEntry.text.match(".*Flow >> (\\[(.*)\\]\\((.*)\\)).*")?.let { regexMatch ->
                if (regexMatch.isNotEmpty()) {
                    isRich = true
                    logText = logText.replace(regexMatch[1], "<a href=\"#!/space/dialogue/${regexMatch[3]}\">${regexMatch[2]}</a>")
                }
            }
            return LogLine(logText, isRich, type)
        }
    }

    init {
        onEvent {
            scroll = {
                lastScrollTop = (getElement() as HTMLElement).scrollTop
            }
        }

        overflow = Overflow.SCROLL
        add(
                Tag(TAG.PRE).apply {
                    add(dataContainer(lines, { element, _, _ ->
                        span(element.text, classes = setOf("text-${element.type}"), rich = element.rich)
                    }))
                }
        )
    }

    fun log(texts: List<String>, type: String = "dark"): LogPanel {
        return log(texts.map { LogLine(it, false, type) })
    }

    fun log(logLines: List<LogLine>): LogPanel{
        lines.addAll(logLines)
        onLog()
        scrollToBottom()
        return this
    }

    fun log(text: String, type: String = "dark"): LogPanel {
        return log(listOf(text), type)
    }

    fun clearLog() {
        lines.clear()
    }

    fun scrollToBottom() {
        if (!lock) {
            val element = getElement()
            if (element != null) {
                with(element as HTMLElement) {
                    scrollTop = scrollHeight.toDouble()
                }
            }
        }
    }

    private fun scrollTo(position: Double) {
        (getElement() as HTMLElement).scrollTop = position
    }

    open fun onLog() {}

    fun convertToLink(url: String) = "<a href=\"$url\" target=\"_blank\" title=\"$url\">${url.substringAfterLast("/")}</a>"

    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        MainScope().launch {
            delay(50)
            scrollTo(lastScrollTop)
        }
    }
}