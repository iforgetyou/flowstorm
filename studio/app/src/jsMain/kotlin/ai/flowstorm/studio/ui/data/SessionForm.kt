package ai.flowstorm.studio.ui.data

import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.LocationModal
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.core.type.toLocation
import ai.flowstorm.studio.model.Session
import ai.flowstorm.studio.ui.form.DialogueId
import ai.flowstorm.studio.ui.form.EntityId
import io.kvision.core.onEvent
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class SessionForm :
        Form<Session>(Session.serializer()) {

    init {
        marginTop = UiHelpers.spacing
        marginRight = UiHelpers.spacing
        add(
                Session::_id,
                EntityId(label = tr("Object ID"))
        )
        add(
                Session::sessionId,
                Text(label = tr("Session ID"))
        )
        add(
                Session::initiationId,
                Text(label = tr("Initiation ID"))
        )
        add(
                Session::stringTime,
                Text(label = tr("Time"))
        )
        add(
                Session::test,
                CheckBox(label = tr("Test"))
        )
        add(
            Session::userName,
            Text(label = tr("User"))
        )

        add(
                Session::locationAsString,
                Text(label = tr("Location")).apply {
                    onEvent {
                        click = {
                            value?.let {
                                LocationModal(it.toLocation()).show()
                            }
                        }
                    }
                }
        )
        add(
                Session::clientType,
                Text(label = tr("Client Type"))
        )
        add(
                Session::applicationName,
                Text(label = tr("Application"))
        )
        add(
                Session::dialogue_id,
                DialogueId(label = tr("Dialogue"))
        )
    }

    fun load(session: Session): SessionForm {
        disableAll()
        setData(session)
        return this
    }
}
