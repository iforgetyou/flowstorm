package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.SubDialogue
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.studio.ui.form.DialogueId
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import io.kvision.core.onEvent
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class SubDialogueEditor(graphEditor: GraphEditor) : NodeEditor<SubDialogue>(graphEditor), EditableCode {

    override val codeEditor = CodeEditor(classes = setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.code != self.value) {
                    node?.code = self.value
                    graphEditor.commit(gettext("SubDialogue code changed") + " [#${node!!.id}]", "subdialogue[${node!!.id}].code")
                }
            }
        }
    }
    val rcodeEditor = CodeEditor(classes = setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.rcode != self.value) {
                    node?.rcode = self.value
                    graphEditor.commit(gettext("SubDialogue R-code changed") + " [#${node!!.id}]", "subdialogue[${node!!.id}].rcode")
                }
            }
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
            rcodeEditor.readOnly = value
        }

    override val form = object : NodeForm(SubDialogue.serializer()) {
        init {
            add(
                    SubDialogue::label,
                    Text(label = tr("Label")),
            )
            add(SubDialogue::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
            add(SubDialogue::sourceId, DialogueId(label = tr("Dialogue")).apply {
                onChange { it?.let { save() } }
            })
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Code"), codeEditor, Icon.Function.def)
            addTab(tr("R-Code"), rcodeEditor, Icon.Function.def)
            activeIndex = 0
        }

        override fun tabInit(index: Int) = when (index) {
            1 -> codeEditor.focus()
            else -> super.tabInit(index)
        }
    }

    override fun load(node: SubDialogue) {
        super.load(node)
        form.load(node)
        codeEditor.set(node.code)
        rcodeEditor.set(node.rcode)
    }
}