package ai.flowstorm.studio.model

import kotlinx.serialization.Serializable

@Serializable
data class Input(
        val locale: String = "en",
        val zoneId: String = "Europe/Prague",
        var transcript: Transcript = Transcript(""),
        val alternatives: List<Transcript> = listOf(),
        val classes: List<Class> = listOf(),
        val tokens: List<Token> = listOf()) {

    @Serializable
    data class Transcript(val text: String, val confidence: Float = 1.0F, val provider: String? = null)
    @Serializable
    data class Class(val type: String, val name: String, val score: Float = 1.0F, val model_id: String = "")
    @Serializable
    open class Token(open val text: String) {
        override fun equals(other: Any?): Boolean = text.equals(other)
        override fun hashCode(): Int = text.hashCode()
    }

    val intents get() = classes.filter { it.type == "Intent" }

    var action: String? = null
}
