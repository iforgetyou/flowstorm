package ai.flowstorm.studio.model

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.SpaceResource
import kotlinx.serialization.Serializable
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.core.model.TtsConfig

@Serializable
data class Voice(
    override val _id: Id = newId(),
    var name: String? = null,
    val ttsConfig: TtsConfig = TtsConfig(),
    val space_id: Id = AppState.state.spaceId!!,
    var description: String = ""
) : AbstractEntity() {
    override val label
        get() = SpaceResource.get(space_id).let {
            "[${it.name}]$name"
        }

}
