package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.space.ProfileResource
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.studio.model.Profile

class ProfileForm : EntityForm<Profile>(ProfileResource)
