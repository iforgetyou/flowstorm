package ai.flowstorm.studio.model

import ai.flowstorm.studio.util.toDate
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.id.newId
import ai.flowstorm.core.model.TtsConfig
import kotlinx.serialization.Serializable
import io.kvision.i18n.gettext
import io.kvision.moment.Moment
import kotlinx.datetime.Instant
import kotlinx.serialization.json.JsonElement

@Serializable
data class Turn(
    override val _id: Id = newId(),
    var user_id: Id? = null,
    var session_id: Id? = null,
    var dialogue_id: Id? = null,
    var application_id: Id? = null,
    val datetime: Instant,
    val locale: String? = null,
    var input: Input,
    val inputId: Int? = null,
    val nextId: Int? = null,
    val request: Request? = null,
    val responseItems: List<Item>,
    val endFrame: Session.DialogueStackFrame?,
    val duration: Long? = null,
    val log: List<LogEntry>,
    val votes: MutableMap<String, Int> = mutableMapOf(),
    val attributes: JsonElement
): AbstractEntity() {

    @Serializable
    data class Item (
        /**
         * Message text. Can contain specific XML tags (which can be interpered or stripped by channel).
         */
        var text: String? = null,

        /**
         * Ssml text - Google based by default. Can contain specific XML tags
         */
        var ssml: String? = null,

        /**
         * Confidence score. Client usually does not set (if there is human behind ;-)
         */
        var confidence: Double? = 1.0,

        /**
         * Resource links.
         */
        var image: String? = null,
        var video: String? = null,
        var audio: String? = null,
        var ttsConfig: TtsConfig? = null,
        val turnId: String? = null,
        val nodeId: Int? = null,
        val dialogueNodeId: String? = null,
    )

    @Serializable
    data class Request(
            val attributes: JsonElement? = null
    )

    override val label get() = "turn"
    override val id by ::turnId


    /*
    For unknown reason(bug in serializer?) this can't be inlined and initialization is in init block.
    Serializer throws exception "this.user is undefined"
    */
    var transcript: String = ""
    var timeAsString: String? = ""

    init {
        transcript = input.transcript.text
        timeAsString = Moment(datetime.toDate()).format(gettext("HH:mm:ss.SSS")) as String
    }

    val turnId: Id get() = StringId(timeAsString.toString().replace(":", ""))

    val responseText: String get() = responseItems.filter { item -> item.text?.isNotBlank() ?: false } .map { it.text }.joinToString(" ")
    val response: String get() = responseItems.joinToString("<br>") { item ->

        val voice = (if (item.ssml!= null || item.text?.isNotBlank() == true) item.ttsConfig?.name?:"" else "")
            .let { if(it.isNotEmpty()) "<i>($it)</i>&nbsp;" else it }

        voice + when {
            item.ssml != null -> {
                var result = item.ssml!!
                if ("<audio " in result)
                    result = result.replace(
                        "<audio ", "<br><audio controls style=\"width:100%;\" preload=\"auto\">\n     <source "
                    ).replace("/>", " preload=\"none\">\nYour browser does not support the audio element.\n</audio> ")
                result = result.replace("<s>", "")
                result = result.replace("</s>", "")
                if ("<sub alias=\"" in result) {
                    result = result.replace("<sub alias=\"", "[")
                        .replace("</sub>", "")
                        .replace("\">", "]")
                }
                highlightWithColor(result, votes[item.dialogueNodeId])
            }
            item.text?.isNotBlank() == true -> highlightWithColor(item.text!!, votes[item.dialogueNodeId])
            item.audio != null -> "<br><audio controls style=\"width:100%;\" preload=\"auto\">\n     <source src=\"${item.audio}\" preload=\"none\">\n" +
                    "    Your browser does not support the audio element.\n" +
                    "</audio> "

            item.image != null -> "<img src=\"${item.image}\" style=\"width:100%;\"/>"
            else -> ""
        }
    }

    // Workaround for images displaying incorrectly
    val image: String?
        get() {
            val images = responseItems.filter { item -> item.image != null }
            if (images.isEmpty()){
                return null
            } else {
                return images[0].image
            }
        }
    val endFrameName: String = if (endFrame != null) "${endFrame.name ?: endFrame.id}#${endFrame.nodeId}" else ""

    private fun highlightWithColor(text: String, rating: Int?): String {
        val style = when (rating){
            -3 -> "color:orange" // offensive
            -2 -> "color:yellow" // meaningless
            -1 -> "color:red" // dislike
            1 -> "color:green" // like
            2 -> "color:blue" // funny
            3 -> "color:pink" // love
            else -> ""
        }
        return "<span style=\"$style\">$text</span>"
    }
}
