package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Link
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import kotlinx.serialization.Serializable
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.ReadOnly
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.core.onEvent
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.tr
import io.kvision.panel.TabPanel

class LinkEditor(val graphEditor: GraphEditor) : TabPanel(), Editor, ReadOnly {

    lateinit var link: Link

    @Serializable
    data class FormData(val name: String)

    val form = object : Form<FormData>(FormData.serializer()) {
        init {
            marginTop = UiHelpers.spacing
            marginRight = UiHelpers.spacing
            add(FormData::name, Text(label = tr("Name")), required = true)
            onEvent {
                change = {
                    save()
                }
            }
        }
    }

    init {
        addTab(tr("Properties"), form, Icon.Properties.def)
    }

    override var readOnly: Boolean = true
        set(value) {
            form.disabled = value
            field = value
        }

    override fun load(item: Any) {
        load(item as Link)
    }

    fun load(link: Link) {
        this.link = link
        form.setData(FormData(link.name))
    }

    fun save() {
        val data = form.getData()
        link.name = data.name
        graphEditor.diagramPanel.diagram.setLinkProperty(link.from, link.to, Link::name.name, data.name)
    }
}