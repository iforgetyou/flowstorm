package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object InvitationResource : EntityResource<Invitation>(serializer = Invitation.serializer()) {

    init {
        AppState.sub { it.spaceId }.onEach { clear() }.launchIn(App.scope)
    }

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/invitations"

    suspend fun resend(invitation: Invitation?) = remoteCall(
            uri = "/${invitation!!._id}",
            method = HttpMethod.POST,
            deserializer = serializer
    )
}
