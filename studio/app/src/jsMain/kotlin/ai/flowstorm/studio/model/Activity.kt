package ai.flowstorm.studio.model

import ai.flowstorm.studio.util.toDate
import kotlinx.serialization.Serializable
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import io.kvision.moment.Moment
import kotlinx.datetime.Instant

@Serializable
data class Activity(
    override val _id: Id = newId(),
    val datetime: Instant,
    val type: String,
    val name: String,
    val ref_id: Id,
    val refName: String,
    val user_id: Id,
    val username: String,
    var space_id: Id
) : AbstractEntity() {
    override val label by this::name
    var datetimeString: String = datetime.let { Moment(it.toDate()).fromNow() as String }
}