package ai.flowstorm.studio.ui

import ai.flowstorm.common.ui.UiHelpers
import io.kvision.modal.Modal

class CurrentSpace : SpaceEditor() {

    override suspend fun save() {
        super.save()
        UiHelpers.parent<Modal>(this@CurrentSpace)?.hide()
    }
}