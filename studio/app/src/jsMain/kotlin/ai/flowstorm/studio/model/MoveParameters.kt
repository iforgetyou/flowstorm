package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import kotlinx.serialization.Serializable

@Serializable
data class MoveParameters(val spaceId: Id)