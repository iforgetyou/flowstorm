package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.serialization.Serializable
import io.kvision.i18n.gettext

@Serializable
data class Initiation(
        override val _id: Id = newId(),
        var name: String = "",
        //FIXME (why JSON serialization produces this.datetime is undefined error??)
        //@Serializable(with = DateSerializer::class)
        //var datetime: Date = Date(),
        val deviceType: String? = null,
        val space_id: Id? = null,
        val application_id: Id? = null,
        val users_id: List<Id> = listOf(),
        val portUrl: String? = null
) : AbstractEntity() {
    override val label by this::name
    val stringTime: String = "?"//Moment(datetime.toString()).format(gettext("YYYY/MM/DD HH:mm"))
    var deviceTypeTranslated: String? = null
        get() = gettext(deviceType ?: "")
}