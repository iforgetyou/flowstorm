package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Function
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.ListIdSelect
import io.kvision.core.onEvent
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class FunctionEditor(graphEditor: GraphEditor) : NodeEditor<Function>(graphEditor), EditableCode {

    override val codeEditor = CodeEditor(classes = setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.code != self.value) {
                    node?.code = self.value
                    graphEditor.commit(gettext("Function code changed") + " [#${node!!.id}]", "function[${node!!.id}].code")
                }
            }
        }
    }
    override val form = object : NodeForm(Function.serializer()) {
        init {
            add(
                    Function::label,
                    Text(label = tr("Label")),
            )
            add(
                    Function::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
            add(
                    Function::mixins_id,
                    ListIdSelect(label = tr("Mixins")).apply {
                        mainScope.launch {
                            options = UiHelpers.options(DialogueMixinResource.load("Function"))
                        }
                        onEvent {
                            change = {
                                mainScope.launch {
                                    codeEditor.set(codeEditor.value, this@apply.getValue() ?: listOf())
                                }
                            }
                        }
                    }
            )
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
        }
    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Code"), codeEditor, Icon.Function.def)
            activeIndex = 1
        }

        override fun tabInit(index: Int) = when (index) {
            1 -> codeEditor.focus()
            else -> super.tabInit(index)
        }
    }

    override fun load(node: Function) {
        super.load(node)
        form.load(node)
        codeEditor.set(node.code, node.mixins_id)
    }
}