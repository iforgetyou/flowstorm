package ai.flowstorm.studio.ui

import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.state.RootState.Action.SetSpaceUrls
import ai.flowstorm.common.resources.EntityResource
import com.github.snabbdom._set
import io.kvision.form.text.Password
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.i18n.tr

class SpaceForm(model: EntityResource<Space>): EntityForm<Space>(model) {
    init {
        add(
                Space::name,
                Text(label = tr("Name")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Space::deviceConfiguration,
                TextArea(label = tr("Device Configuration"), rows = 6)
        )
        add(
                Space::coreUrl,
                Text(label = tr("Core URL")).apply {
                    placeholder = App.config.coreUrl
                }
        )
        add(
                Space::coreKey,
                Password(label = tr("Core Key")).apply {
                    input.setAttribute("autocomplete", "new-password")
                }
        )
        add(
                Space::twilioAccountID,
                Text(label = tr("Twilio Account ID"))
        )
        add(
                Space::twilioAuthToken,
                Password(label = tr("Twilio Auth Token")).apply {
                    input.setAttribute("autocomplete", "new-password")
                }
        )
    }

    override suspend fun save() {
        if (this@SpaceForm.form.fields["coreUrl"]?.getValue() == "") {
            this@SpaceForm.form.fields["coreUrl"]?.setValue(null)
        }
        isValid = validate()
        if (isValid) {
            form.fields.forEach { entity!!._set(it.key, it.value.getValue())}
            console.log("SpaceForm.save", entity)
            createOrUpdate(entity!!)
        }

        val newCoreUrl = this@SpaceForm.form.fields["coreUrl"]?.getValue() as String? ?: App.config.coreUrl
        val newFileUrl = "$newCoreUrl/file"
        AppState.dispatch(SetSpaceUrls(newCoreUrl, newFileUrl))
    }
}
