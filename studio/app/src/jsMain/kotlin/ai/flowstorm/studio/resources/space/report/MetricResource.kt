package ai.flowstorm.studio.resources.space.report

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Metric
import ai.flowstorm.common.resources.EntityResource

object MetricResource : EntityResource<Metric>(serializer = Metric.serializer()) {

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/reports/metrics"

}
