package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.DialogueMixin
import ai.flowstorm.common.ui.EntityEditor
import ai.flowstorm.common.id.Id

class DialogueMixinEditor : EntityEditor<DialogueMixin>(DialogueMixinForm(), serializer = DialogueMixin.serializer()) {

    override fun load(id: Id) = super.load(id).also {
        if (it.space_id != AppState.state.spaceId!!)
            disableAll()
    }
}
