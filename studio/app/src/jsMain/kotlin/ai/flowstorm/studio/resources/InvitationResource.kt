package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod

object InvitationResource : EntityResource<Invitation>(resourceUri = "/invitations", serializer = Invitation.serializer()) {

    suspend fun accept(invitationId: Id) {
        remoteCall<Invitation>("/${invitationId.value}/accept", method = HttpMethod.PUT)
    }
}