package ai.flowstorm.studio.ui.design.dialogue

object DialogueName {
    fun isValidName(name: String): Boolean = name.matches(Regex("([\\w\\-]+)/([\\w\\-]+)"))
}