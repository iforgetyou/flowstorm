package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.Session
import ai.flowstorm.studio.resources.space.SessionResource
import ai.flowstorm.studio.ui.RunnableComponent
import ai.flowstorm.studio.ui.data.SessionViewer
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.modal.ModalSize
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions

class SessionPanel(val runnableComponent: RunnableComponent) : EntityTable<Session>(
        "Session",
        SessionResource.last,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("ID"), "sessionId", width = "60%"),
                        ColumnDefinition(tr("Time"), "stringTime", width = "25%"),
                        ColumnDefinition(tr("Turns"), "turnCount", width = "15%")
                )
        ),
        { SessionViewer(SessionResource.last) },
        seekEnabled = false,
        modalSize = ModalSize.XLARGE,
        hideHeader = true,
        className = "panel-view-nav1"
) {
    override fun reload() {
        val key = runnableComponent.key
        if (key != null) {
            query = Query(
                    mutableListOf(
                            Query.Filter("user._id", Query.Operator.`in`, UserService.context.user._id.value),
                            Query.Filter("test", Query.Operator.eq, true)
                    ).apply {
                        if (key.startsWith('@'))
                            add(Query.Filter("application.dialogue_id", Query.Operator.eq, StringId(key.substring(1)).value))
                        else
                            add(Query.Filter("application._id", Query.Operator.eq, StringId(key).value))
                    }
            )
            super.reload()
        } else {
            super.reload(true)
        }
    }
}
