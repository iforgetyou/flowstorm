package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import kotlinx.serialization.Serializable

@Serializable
data class UserSpaceRole(
        val spaceId: Id,
        val name: String,
        val role: Space.Role
)
