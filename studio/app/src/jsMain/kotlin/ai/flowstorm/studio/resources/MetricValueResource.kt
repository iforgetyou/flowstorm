package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.analytics.MetricValue
import ai.flowstorm.common.resources.EntityResource
import io.kvision.state.ObservableList

class MetricValueResource(entities: ObservableList<MetricValue>) : EntityResource<MetricValue>(serializer = MetricValue.serializer(), entities = entities)