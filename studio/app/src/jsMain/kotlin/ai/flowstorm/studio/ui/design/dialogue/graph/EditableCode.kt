package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.common.ui.CodeEditor

interface EditableCode {

    val codeEditor: CodeEditor
}