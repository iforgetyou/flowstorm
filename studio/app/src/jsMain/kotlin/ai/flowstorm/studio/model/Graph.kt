package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.Cloneable
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import io.kvision.i18n.gettext
import kotlin.math.abs

@Serializable
data class Graph(
    val intents: MutableList<Intent> = mutableListOf(),
    val globalIntents: MutableList<GlobalIntent> = mutableListOf(),
    val responses: MutableList<Response> = mutableListOf(),
    val subDialogues: MutableList<SubDialogue> = mutableListOf(),
    val goBacks: MutableList<GoBack> = mutableListOf(),
    val functions: MutableList<Function> = mutableListOf(),
    val userInputs: MutableList<UserInput> = mutableListOf(),
    val reInputs: MutableList<ReInput> = mutableListOf(),
    val startDialogues: MutableList<Enter> = mutableListOf(),
    val stopDialogues: MutableList<Exit> = mutableListOf(),
    val stopSessions: MutableList<End> = mutableListOf(),
    val sleeps: MutableList<Sleep> = mutableListOf(),
    val speeches: MutableList<Speech> = mutableListOf(),
    val sounds: MutableList<Sound> = mutableListOf(),
    val images: MutableList<Image> = mutableListOf(),
    val commands: MutableList<Command> = mutableListOf(),
    val actions: MutableList<Action> = mutableListOf(),
    val globalActions: MutableList<GlobalAction> = mutableListOf(),

    val groups: MutableList<Group> = mutableListOf(),

    val links: MutableList<Link> = mutableListOf()
) : Cloneable<Graph> {

    private val nodeLists: List<MutableList<out Node>>
        get() = listOf(
            intents, globalIntents, responses, subDialogues, goBacks, functions, userInputs, reInputs, startDialogues,
            stopDialogues, stopSessions, sleeps, speeches, sounds, images, commands, actions, globalActions, groups
        )

    val nodes get() = nodeLists.reduce { acc, list -> (acc + list) as MutableList<out Node> }

    val size get() = nodes.size + links.size

    override fun clone() = Cloneable.clone(this)

    fun getNodeList(type: String): MutableList<out Node> {
        return when (type) {
            Intent::class.simpleName -> intents
            Action::class.simpleName -> actions
            GlobalAction::class.simpleName -> globalActions
            Function::class.simpleName -> functions
            UserInput::class.simpleName -> userInputs
            ReInput::class.simpleName -> reInputs
            GlobalIntent::class.simpleName -> globalIntents
            Response::class.simpleName -> responses
            GoBack::class.simpleName -> goBacks
            Sleep::class.simpleName -> sleeps
            SubDialogue::class.simpleName -> subDialogues
            Enter::class.simpleName -> startDialogues
            Exit::class.simpleName -> stopDialogues
            End::class.simpleName -> stopSessions
            Speech::class.simpleName -> speeches
            Sound::class.simpleName -> sounds
            Image::class.simpleName -> images
            Command::class.simpleName -> commands
            Group::class.simpleName -> groups
            else -> error("Unknown type")
        }
    }

    fun addNode(node: Node) {
        if (node.isNew()) {
            node.id = newKey()
            node.name = newName(node)
        }

        when (node) {
            is Intent -> intents.add(node)
            is Action -> actions.add(node)
            is GlobalAction -> globalActions.add(node)
            is Function -> functions.add(node)
            is UserInput -> userInputs.add(node)
            is ReInput -> reInputs.add(node)
            is GlobalIntent -> globalIntents.add(node)
            is Response -> responses.add(node)
            is GoBack -> goBacks.add(node)
            is Sleep -> sleeps.add(node)
            is SubDialogue -> subDialogues.add(node)
            is Enter -> startDialogues.add(node)
            is Exit -> stopDialogues.add(node)
            is End -> stopSessions.add(node)
            is Speech -> speeches.add(node)
            is Sound -> sounds.add(node)
            is Image -> images.add(node)
            is Command -> commands.add(node)
            is Group -> groups.add(node)
            else -> error("Unknown type")
        }
    }

    fun newName(node: Node): String =
            if (node.name.isEmpty() || nodes.any { it.name == node.name }) {
                var generatedName = node::class.simpleName + abs(node.id)
                var counter = 0
                while (nodes.any { it.name == generatedName }) {
                    generatedName = "${node::class.simpleName + abs(node.id)}$counter"
                    counter += 1
                }
                generatedName
            } else {
                node.name
            }

    fun removeNode(id: Int) {
        val node = nodes.first { it.id == id }
        getNodeList(node::class.simpleName!!).removeAll { it.id == id }
    }

    fun merge(graph: Graph): MutableMap<Int, Int> {
        val nodeIdMap = mutableMapOf<Int, Int>()
        graph.nodes.forEach {
            val newNode = Node.copy(it).apply {
                id = Node.NEW_ID
                name = ""
            }
            addNode(newNode)
            nodeIdMap[it.id] = newNode.id
        }

        graph.nodes.forEach { oldNode ->
            nodes.first { it.id == nodeIdMap[oldNode.id] }.apply {
                groupId = nodeIdMap[groupId]
            }
        }

        graph.links.forEach {
            val from = nodeIdMap[it.from] ?: return@forEach
            val to = nodeIdMap[it.to] ?: return@forEach

            val newLink = it.copy(from = from, to = to)
            addLink(newLink)
        }
        return nodeIdMap
    }

    fun delete(graph: Graph) {
        graph.nodes.forEach { node ->
            removeNode(node.id)
            links.removeAll { link -> link.from == node.id || link.to == node.id }
        }
        links.removeAll(graph.links)
    }

    fun clear() {
        nodeLists.forEach { it.clear() }
        links.clear()
    }

    fun replace(graph: Graph) {
        clear()
        graph.nodes.forEach {
            addNode(Node.copy(it))
        }
        graph.links.forEach {
            addLink(it.copy())
        }
    }

    fun addLink(link: Link) = links.add(link)

    data class Point(val x: Double, val y: Double)

    interface Node {
        var id: Int
        var name: String
        var loc: String?
        var comment: String
        var label: String
        var groupId: Int?

        fun isNew(): Boolean

        val point: Point
            get() = loc!!.split(" ").let { Point(it[0].toDouble(), it[1].toDouble()) }

        companion object {
            const val NEW_ID = 0

            fun copy(node: Node): Node {
                return when (node) {
                    is Intent -> node.copy()
                    is Action -> node.copy()
                    is GlobalAction -> node.copy()
                    is Function -> node.copy()
                    is UserInput -> node.copy()
                    is ReInput -> node.copy()
                    is GlobalIntent -> node.copy()
                    is Response -> node.copy()
                    is GoBack -> node.copy()
                    is Sleep -> node.copy()
                    is SubDialogue -> node.copy()
                    is Enter -> node.copy()
                    is Exit -> node.copy()
                    is End -> node.copy()
                    is Speech -> node.copy()
                    is Sound -> node.copy()
                    is Image -> node.copy()
                    is Command -> node.copy()
                    is Group -> node.copy()
                    else -> error("Unknown type")
                }
            }

            fun serializer(node:Node): KSerializer<Node> = when (node) {
                is Intent -> Intent.serializer()
                is Action -> Action.serializer()
                is GlobalAction -> GlobalAction.serializer()
                is Function -> Function.serializer()
                is UserInput -> UserInput.serializer()
                is ReInput -> ReInput.serializer()
                is GlobalIntent -> GlobalIntent.serializer()
                is Response -> Response.serializer()
                is GoBack -> GoBack.serializer()
                is Sleep -> Sleep.serializer()
                is SubDialogue -> SubDialogue.serializer()
                is Enter -> Enter.serializer()
                is Exit -> Exit.serializer()
                is End -> End.serializer()
                is Speech -> Speech.serializer()
                is Sound -> Sound.serializer()
                is Image -> Image.serializer()
                is Command -> Command.serializer()
                is Group -> Group.serializer()
                else -> error("Unknown type")
            } as KSerializer<Node>
        }
    }

    abstract class AbstractNode : Node {
        override var label: String = ""
            get() = if (field.isEmpty()) name else field

        override fun isNew() = (id == Node.NEW_ID)
    }

    @Serializable
    data class Group(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Group"),
        override var groupId: Int? = null,
        var code: String = "",
    ) : AbstractNode()

    @Serializable
    data class Intent(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Intent"),
        override var groupId: Int? = null,
        var utterances: List<String> = listOf(),
        var negativeUtterances: List<String> = listOf(),
        val mixins_id: List<Id> = listOf(),
        var threshold: Float = 0F,
        val entities: String = ""
    ) : AbstractNode()

    @Serializable
    data class GlobalIntent(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Global Intent"),
        override var groupId: Int? = null,
        var utterances: List<String> = listOf(),
        var negativeUtterances: List<String> = listOf(),
        val mixins_id: List<Id> = listOf(),
        var threshold: Float = 0.5F,
        val entities: String = ""
    ) : AbstractNode()

    @Serializable
    data class Response(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Response"),
        override var groupId: Int? = null,
        var responses: List<String> = listOf(),
        var repeatable: Boolean = true
    ) : AbstractNode()

    @Serializable
    data class Function(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Function"),
        override var groupId: Int? = null,
        val mixins_id: List<Id> = listOf(),
        var code: String = ""
    ) : AbstractNode()

    @Serializable
    data class Command(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
        val mixin_id: Id? = null,
        var command: String = "",
        var code: String = ""
    ) : AbstractNode() {
        override var label: String = gettext("Command")
            get() = if (command.isNotEmpty()) "#$command" else field
    }

    @Serializable
    data class SubDialogue(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Subdialogue"),
        override var groupId: Int? = null,
        var sourceId: Id? = null,
        var code: String = "",
        var rcode: String = ""
    ) : AbstractNode()

    @Serializable
    data class GoBack(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
        var repeat: Boolean = false
    ) : AbstractNode() {
        init {
            label = gettext("Go Back")
        }
    }

    @Serializable
    data class Sleep(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
        var timeout: Int = 60
    ) : AbstractNode() {
        init {
            label = gettext("Sleep")
        }
    }

    interface Input {
        var skipGlobalIntents: Boolean
    }

    @Serializable
    data class UserInput(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("User Input"),
        override var groupId: Int? = null,
        val mixins_id: List<Id> = listOf(),
        override var skipGlobalIntents: Boolean = false,
        var expectedPhrases: String = "",
        var speechRecognizer_id: Id? = null,
        var code: String = ""
    ) : Input, AbstractNode()

    @Serializable
    data class ReInput(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Re-Input"),
        override var groupId: Int? = null,
        override var skipGlobalIntents: Boolean = false,
        var code: String = ""
    ) : Input, AbstractNode()

    @Serializable
    data class Enter(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
    ) : AbstractNode() {
        init {
            label = gettext("Enter")
        }
    }

    @Serializable
    data class Exit(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
    ) : AbstractNode() {
        init {
            label = gettext("Exit")
        }
    }

    @Serializable
    data class End(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
    ) : AbstractNode() {
        init {
            label = gettext("End")
        }
    }

    @Serializable
    data class Speech(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Speech"),
        override var groupId: Int? = null,
        var responses: List<String> = listOf(),
        val mixins_id: List<Id> = listOf(),
        var voice_id: Id? = null,
        var code: String? = null,
        var background: String? = null,
        var repeatable: Boolean = true
    ) : AbstractNode()

    @Serializable
    data class Sound(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Sound"),
        override var groupId: Int? = null,
        var asset_id: Id? = null,
        var source: String? = null,
        var repeatable: Boolean = true,
        @Transient val asset: File? = null
    ) : AbstractNode()

    @Serializable
    data class Image(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var label: String = gettext("Image"),
        override var groupId: Int? = null,
        var asset_id: Id? = null,
        var source: String? = null,
        @Transient val asset: File? = null
    ) : AbstractNode()

    @Serializable
    data class Action(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
        var action: String = ""
    ) : AbstractNode() {
        override var label: String = gettext("Action")
            get() = if (action.isNotEmpty()) "#$action" else field
    }

    @Serializable
    data class GlobalAction(
        override var id: Int = Node.NEW_ID,
        override var name: String = "",
        override var loc: String? = null,
        override var comment: String = "",
        override var groupId: Int? = null,
        var action: String = "",
    ) : AbstractNode() {
        override var label: String = gettext("Global Action")
            get() = if (action.isNotEmpty()) "#$action" else field
    }

    @Serializable
    data class Link(
            var name: String,
            var from: Int,
            var to: Int,
            var fromPort: String,
            var toPort: String,
            var points: MutableList<String> = mutableListOf()
    )

    fun newKey(): Int = (nodes.minByOrNull { it.id }?.id ?: 0) - 1

    fun isEmpty() = size == 0

    fun move(offsetX: Double, offsetY: Double) {
        nodes.filterNot { it is Group }.forEach {
            var (x, y) = it.loc!!.split(" ").map { it.toDouble() }
            x += offsetX
            y += offsetY

            it.loc = "$x $y"
        }
    }
}
