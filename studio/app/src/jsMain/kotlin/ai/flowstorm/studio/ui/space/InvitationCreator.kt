package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.studio.resources.space.InvitationResource
import ai.flowstorm.studio.ui.InvitationForm
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.AppPanel
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.core.TextAlign
import io.kvision.html.ButtonStyle
import io.kvision.html.button
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Alert
import io.kvision.modal.Modal
import io.kvision.panel.simplePanel
import io.kvision.utils.perc

class InvitationCreator : InvitationForm(InvitationResource) {
    init {
        simplePanel {
            width = 100.perc
            textAlign = TextAlign.RIGHT
            button(tr("Create"), style = ButtonStyle.PRIMARY).onClick {
                mainScope.launch {
                    if (validate()) {
                        val invitation = Invitation(space_id = AppState.state.spaceId!!)
                        save(invitation)

                        InvitationResource.create(invitation).let {
                            Alert.show(tr("User Invitation"),
                                    gettext("Invitation") + " #${it.id.value} " + gettext("has been sent to user") + " ${it.username}.")
                            UiHelpers.parent<Modal>(this@InvitationCreator)?.hide()
                            AppPanel.instance.reload()
                        }
                    }
                }
            }
        }
    }
}
