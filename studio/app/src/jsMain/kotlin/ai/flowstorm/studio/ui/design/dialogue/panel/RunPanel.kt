package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.ui.RunnableComponent
import ai.flowstorm.common.Lockable
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Hints.hint
import io.kvision.core.Component
import io.kvision.core.Placement
import io.kvision.core.onEvent
import io.kvision.i18n.I18n.tr
import io.kvision.i18n.gettext
import io.kvision.panel.SplitPanel
import io.kvision.panel.TabPanel
import io.kvision.panel.simplePanel
import io.kvision.panel.tabPanel
import io.kvision.toolbar.buttonGroup
import io.kvision.toolbar.toolbar
import io.kvision.utils.perc

class RunPanel(
        parent: RunnableComponent,
        val botFocus: Boolean = true,
        val tabInit: (TabPanel.(RunPanel) -> Unit)? = null) : SplitPanel(classes = setOf("run-panel")), ShortcutProvider {

    lateinit var tabPanel: TabPanel
    val runLogPanel = LogPanel()
    val clientPanel = ClientPanel()
    val botPanel = BotPanel(parent, this, clientPanel)
    val lastTurnPanel = LastTurnPanel()
    val lastSessionPanel = LastSessionPanel()
    val profilePanel = ProfilePanel()
    val communityPanel = CommunityPanel()

    override val shortcuts: List<Shortcut> = listOf(
            Shortcut(tr("Reload run panel"), "R") { reloadPanel() },
    )
        get() = field + botPanel.shortcuts

    val runViewButton = ToolbarButton(Icon.Expand, gettext("Toggle Run View"), "0") {
        hint("ToggleRunViewButton", Icon.Expand, index = 221)
        onClick {
            parent.toggleRunPanelView()
        }
    }
    private val lockButton = ToolbarButton(Icon.Lock, gettext("Lock Panel")) {
        hint("LockPanelButton", Icon.Lock, index = 222)
        onClick {
            val activeChild = tabPanel.activeTab?.component
            if (activeChild is Lockable) {
                activeChild.lock = !activeChild.lock
                this@ToolbarButton.active = activeChild.lock
            }
        }
    }
    private val reloadButton = ToolbarButton(Icon.Reload, gettext("Reload"), disabled = true) {
        hint("ReloadButton", Icon.Reload, index = 223)
        onClick {
            reloadPanel()
        }
    }
    private val clearButton = ToolbarButton(Icon.Clear, gettext("Clear Log")) {
        hint("ClearLogButton", Icon.Clear, Placement.RIGHT, index = 224)
        onClick {
            val activeChild = tabPanel.activeTab?.component
            if (activeChild is LogPanel)
                activeChild.clearLog()

        }
    }
    private val scrollToBottomButton = ToolbarButton(Icon.Bottom, gettext("Scroll to Bottom")) {
        hint("ScrollToBottomButton", Icon.Bottom, Placement.RIGHT, index = 225)
        onClick {
            val activeChild = tabPanel.activeTab?.component
            if (activeChild is LogPanel)
                activeChild.scrollToBottom()

        }
    }

    init {
        width = 100.perc
        simplePanel(classes = setOf("run-panel-holder")) {
            width = 65.perc
            // panel toolbar
            simplePanel(classes = setOf("panel-view", "panel-toolbar")) {
                toolbar(vertical = true) {
                    buttonGroup {
                        add(runViewButton)
                        add(lockButton)
                        add(reloadButton)
                        add(clearButton)
                        add(scrollToBottomButton)
                    }
                }
            }
            // tab panel
            tabPanel = tabPanel(classes = setOf("panel-view"), scrollableTabs = true) {
                tabInit?.invoke(this, this@RunPanel)
                tab(tr("Run log"), runLogPanel, Icon.Run.def) {
                    hint("RunTab", Icon.Run, Placement.TOP, index = 227)
                }
                tab(tr("Last Turn"), lastTurnPanel, Icon.Turn.def) {
                    hint("LastTurnTab", Icon.Turn, Placement.TOP, index = 228)
                }
                tab(tr("Last Session"), lastSessionPanel, Icon.Session.def) {
                    hint("LastSessionTab", Icon.Session, Placement.TOP, index = 229)
                }
                tab(tr("Profile"), profilePanel, Icon.User.def) {
                    hint("ProfileTab", Icon.User, Placement.TOP, index = 230)
                }
                tab(tr("Communities"), communityPanel, Icon.Community.def) {
                    hint("CommunitiesTab", Icon.Community, Placement.TOP, index = 232)
                }
            }.apply {
                onEvent {
                    tabChange = {
                        val activeChild = tabPanel.activeTab?.component
                        if (activeChild is LogPanel)
                            activeChild.scrollToBottom()
                        if (activeChild is Reloadable)
                            activeChild.reload()
                        if (activeChild is Lockable) {
                            lockButton.disabled = false
                            lockButton.active = activeChild.lock
                        } else {
                            lockButton.disable()
                        }
                        clearButton.disabled = activeChild !is LogPanel
                        scrollToBottomButton.disabled = activeChild !is LogPanel
                        reloadButton.disabled = activeChild !is Reloadable
                    }
                }
            }
        }
        // bot panel
        add(botPanel)
    }

    fun reloadPanel() {
        val activeChild = tabPanel.activeTab?.component
        if (activeChild is Reloadable)
            activeChild.reload()
    }

    fun switchToTab(component: Component) {
        tabPanel.activeTab = tabPanel.findTabWithComponent(component) ?: error("Tab with component not found!")
    }
}
