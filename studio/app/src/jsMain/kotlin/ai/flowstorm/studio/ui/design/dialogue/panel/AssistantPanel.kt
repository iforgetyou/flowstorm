package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.App
import ai.flowstorm.common.BotLogic
import com.github.snabbdom.VNode
import io.kvision.core.*
import io.kvision.i18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.panel.TabPanel
import io.kvision.panel.Tab

object AssistantPanel : SimplePanel(classes = setOf("wallpaper")) {
    class AssistantTab(caption: String, component: Component): Tab(label = caption, child = component){
        init {
            addCssClass("bot-tab")
            addCssClass("bot-tab-main")
            link.addCssClass("bot-tab")
            link.addCssClass("bot-tab-link")
            onClick {
                BotLogic.stop()
            }
        }
    }

    val assistantPanel = ChatPanel("assistantDiv", App.config.assistantKey)
    val dialoguePanel = ChatPanel("botDiv", "")
    var route = ""
    var tabPanel: TabPanel = TabPanel().apply {
        dialoguePanel.setControls(false)
        add(AssistantTab(tr("TEST"), dialoguePanel))
        add(AssistantTab(tr("ASSISTANT"), assistantPanel))
        activeIndex = 1
    }

    init {
        add(tabPanel)
    }
}
