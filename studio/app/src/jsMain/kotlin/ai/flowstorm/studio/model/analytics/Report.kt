package ai.flowstorm.studio.model.analytics

import kotlinx.serialization.Serializable

@Serializable
data class Report(val columns: Array<String>, var dataSets: Array<DataSet>) {

    enum class Granularity { /*MINUTE,*/ HOUR, DAY, /*WEEK,*/ MONTH }
    enum class Aggregation { USER, NAMESPACE, METRIC, APPLICATION, SPACE }

    @Serializable
    data class DataSet(val label: String, val data: Array<Int>) {
        // following properties required by tabulator field definition which seems not to support array access
        var x0: Int get() = data[0]; set(value) { data[0] = value }
        var x1: Int get() = data[1]; set(value) { data[1] = value }
        var x2: Int get() = data[2]; set(value) { data[2] = value }
        var x3: Int get() = data[3]; set(value) { data[3] = value }
        var x4: Int get() = data[4]; set(value) { data[4] = value }
        var x5: Int get() = data[5]; set(value) { data[5] = value }
        var x6: Int get() = data[6]; set(value) { data[6] = value }
        var x7: Int get() = data[7]; set(value) { data[7] = value }
        var x8: Int get() = data[8]; set(value) { data[8] = value }
        var x9: Int get() = data[9]; set(value) { data[9] = value }
        var x10: Int get() = data[10]; set(value) { data[10] = value }
        var x11: Int get() = data[11]; set(value) { data[11] = value }
        var x12: Int get() = data[12]; set(value) { data[12] = value }
        var x13: Int get() = data[13]; set(value) { data[13] = value }
        var x14: Int get() = data[14]; set(value) { data[14] = value }
        var x15: Int get() = data[15]; set(value) { data[15] = value }
        var x16: Int get() = data[16]; set(value) { data[16] = value }
        var x17: Int get() = data[17]; set(value) { data[17] = value }
        var x18: Int get() = data[18]; set(value) { data[18] = value }
        var x19: Int get() = data[19]; set(value) { data[19] = value }
        var x20: Int get() = data[20]; set(value) { data[20] = value }
        var x21: Int get() = data[21]; set(value) { data[21] = value }
        var x22: Int get() = data[22]; set(value) { data[22] = value }
        var x23: Int get() = data[23]; set(value) { data[23] = value }
        var x24: Int get() = data[24]; set(value) { data[24] = value }
        var x25: Int get() = data[25]; set(value) { data[25] = value }
        var x26: Int get() = data[26]; set(value) { data[26] = value }
        var x27: Int get() = data[27]; set(value) { data[27] = value }
        var x28: Int get() = data[28]; set(value) { data[28] = value }
        var x29: Int get() = data[29]; set(value) { data[29] = value }
    }

}