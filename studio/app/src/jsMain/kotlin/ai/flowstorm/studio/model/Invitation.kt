package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.serialization.Serializable

@Serializable
data class Invitation(
        override val _id: Id = newId(),
        var space_id: Id? = null,
        var username: String? = null,
        var role: String? = null
): AbstractEntity() {
    override val label: String
        get() = if (isNew) "" else id.value
}