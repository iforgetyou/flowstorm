package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Turn
import ai.flowstorm.studio.model.Utterance
import ai.flowstorm.studio.resources.TurnResource
import ai.flowstorm.studio.resources.UtteranceResource
import ai.flowstorm.common.setProperty
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.modal.ModalSize
import io.kvision.state.ObservableList
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Formatter
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.obj

class UtteranceTable(utterances: ObservableList<Utterance>, turns: ObservableList<Turn>) : EntityTable<Utterance>(
        "Utterance",
        UtteranceResource(utterances),
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Time"), Utterance::timeAsString.name, width = "15%"),
                        ColumnDefinition(tr("Content"), Utterance::content.name, width = "85%", formatter = Formatter.HTML)
                )
        ),
        { TurnWrapper(UtteranceResource(utterances), TurnResource(turns)) },
        deleteEnabled = false,
        saveEnabled = false,
        modalSize = ModalSize.XLARGE,
        hideHeader = true,
        dataExtractor = {
                obj {
                        setProperty(this, it::timeAsString)
                        setProperty(this, it::content)
                }
        },
)
