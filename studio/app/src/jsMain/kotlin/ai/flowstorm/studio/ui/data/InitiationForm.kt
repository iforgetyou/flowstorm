package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Initiation
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.resources.space.ApplicationResource
import ai.flowstorm.studio.resources.space.InitiationResource
import ai.flowstorm.studio.resources.space.UserAssignmentResource
import ai.flowstorm.common.model.idPair
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.ListIdSelect
import ai.flowstorm.common.ui.form.SelectId
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class InitiationForm : EntityForm<Initiation>(InitiationResource) {
    init {
        add(
                Initiation::name,
                Text(label = tr("Name")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Initiation::deviceType,
                Select(label = tr("Device Type"), options = UiHelpers.options(arrayOf(/*"Any", */"Phone", "Standalone", "Mobile", "Web")), value = "Any"),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Initiation::application_id,
                SelectId(label = tr("Application")).apply {
                    mainScope.launch {
                        options = UiHelpers.options(ApplicationResource)
                    }
                },
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Initiation::users_id,
                ListIdSelect(label = tr("Users")).apply {
                    mainScope.launch {
                        options = UiHelpers.options(UserAssignmentResource) { e:Space.UserAssignment -> e._user?.idPair() }
                    }
                },
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Initiation::portUrl,
                Text(label = tr("Custom Core URL"))
        )
    }
}