package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import kotlinx.serialization.Serializable
import ai.flowstorm.common.model.AbstractEntity

@Serializable
data class Account(
    override val _id: Id = newId(),
    var user_id: Id? = null,
    var name: String,
    var street: String? = null,
    var city: String? = null,
    var state: String? = null,
    var country: String? = null,
    var zip: String? = null
) : AbstractEntity() {
    override val label by this::name
}