package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.resources.space.DialogueSnippetResource
import ai.flowstorm.studio.ui.design.dialogue.OpenDialogueSnippetModal
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.ObjectId
import io.kvision.panel.Tab
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.*
import io.kvision.dropdown.DropDown
import io.kvision.dropdown.ddLink
import io.kvision.dropdown.separator
import io.kvision.html.span
import io.kvision.i18n.tr

class DialogueSnippetDesigner : ModelDesigner<DialogueSnippet>(), Panel, Navigable {

    override val caption = "Snippets"
    override suspend fun getNewModel() = DialogueSnippet()
    override suspend fun load(model: DialogueSnippet) {
        val existingTab = getTabs().firstOrNull {
                val component = it.component
                component is SnippetEditor && component.entity!!._id == model._id
            }

        activeTab = existingTab ?: run {
            if(!model.isNew) {
                model.graph = DialogueSnippetResource.loadGraph(model)
            }
            val editor = SnippetEditor()
            editor.load(model)
            Tab(editor.name, editor, closable = true)
                .also {
                    addTab(it)
                }
        }
    }

    override suspend fun load(id: Id) {
        load(DialogueSnippetResource.load(id, false))
    }

    fun load(id: Id, nodeId: Int?) = mainScope.launch {
        load(id)
        nodeId?.let { (activeChild as DialogueSourceEditor).graphEditor.diagramPanel.diagram.selectNodeById(nodeId) }
    }

    override suspend fun new() {
        val snippet = DialogueSnippet.Factory.empty()
        snippet.space_id = AppState.state.spaceId!!
        load(snippet)
    }

    override suspend fun open() {
        val snippet = OpenDialogueSnippetModal().open()
        load(snippet)
    }

    override fun onNavigate(route: String, params: dynamic) {
        if (params != null) {
            mainScope.launch {
                load(ObjectId(params.id))
            }
        }
    }

    override fun initContextMenu(dropDown: DropDown) {
        dropDown.apply {
            visible = true
            text = tr("Model")
            ddLink(tr("New") + "...", "#", Icon.New.def) {
                span("n", classes = setOf("shortcut"))
            }.onClick {
                mainScope.launch { new() }
            }
            separator()
            ddLink(tr("Open") + "...", "#", Icon.Open.def) {
                span("o", classes = setOf("shortcut"))
            }.onClick {
                mainScope.launch { open() }
            }
            ddLink(tr("Save"), "#", Icon.Save.def) {
                span("s", classes = setOf("shortcut"))
            }.onClick {
                mainScope.launch { save() }
            }
        }
    }
}
