package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.resources.space.SessionResource
import kotlinx.coroutines.launch
import ai.flowstorm.common.BotLogic
import ai.flowstorm.common.id.StringId
import kotlinx.serialization.json.JsonElement

class LastSessionPanel: JsonPanel<JsonElement>(JsonElement.serializer()) {

    override fun reload() {
        if (!lock && BotLogic.sessionId != null) {
            mainScope.launch {
                setData(SessionResource.last.load(StringId(BotLogic.sessionId!!)).attributes)
            }
        }
    }
}
