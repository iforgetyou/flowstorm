package ai.flowstorm.studio.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Enum
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.common.resources.EntityResource
import io.kvision.form.check.CheckBox
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.form.text.TextInputType
import io.kvision.i18n.tr

open class UserForm(model: EntityResource<User>): EntityForm<User>(model) {
    init {
        add(
                User::username,
                Text(label = tr("Username"), type = TextInputType.EMAIL),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                User::name,
                Text(label = tr("Name")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                User::surname,
                Text(label = tr("Surname")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                User::nickname,
                Text(label = tr("Nickname")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                User::language,
                Select(label = tr("Language"), options = UiHelpers.options(Enum.languages)),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                User::phoneNumber,
                Text(label = tr("Phone Number"))
        )
        add(
                User::excludePublicApps,
                CheckBox(label = tr("Exclude public applications"))
        )
        add(
                User::shouldLaunchAssistant,
                CheckBox(label = tr("Launch assistant on startup"))
        )
    }

    override fun load(entity: User): User {
        form.fields["username"]!!.disabled = (entity.username != null)
        form.fields["language"]!!.disabled = (
                !setOf(Space.Role.Owner, Space.Role.Admin).contains(AppState.state.selectedRole!!.role)
                        ||  AuthService.context.username == entity.username
                )
        return super.load(entity)
    }

    fun changeForProfilePage() {
        form.fields["name"]!!.disabled = true
        form.fields["surname"]!!.disabled = true
        form.fields["language"]!!.disabled = false
    }

    fun getLanguage(): String {
        return form.fields["language"]?.getValue().toString()
    }
}
