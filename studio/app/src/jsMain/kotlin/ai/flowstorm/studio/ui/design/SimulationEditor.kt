package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.model.Simulation
import ai.flowstorm.studio.ui.RunnableModelEditor
import ai.flowstorm.studio.ui.design.dialogue.panel.LogPanel
import ai.flowstorm.studio.ui.design.dialogue.panel.RunPanel
import ai.flowstorm.studio.ui.design.simulation.JourneyEditor
import ai.flowstorm.studio.ui.design.simulation.SimulationPropertiesEditor
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.JsonEditor
import ai.flowstorm.common.ui.ModelDesigner
import ai.flowstorm.common.ui.ToolbarButton
import ai.flowstorm.core.ui.GoogleMap
import io.kvision.core.onEvent
import io.kvision.html.Div
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr
import io.kvision.panel.TabPanel
import io.kvision.toolbar.ButtonGroup
import io.kvision.utils.px

class SimulationEditor(modelDesigner: ModelDesigner<Simulation>) : RunnableModelEditor<Simulation, TabPanel>(modelDesigner) {

    override var entity: Simulation? = null
    override val key get() = entity?._id?.value
    override val voice = ""
    override var savedVersion = Simulation.Factory.empty()
    override val name: String
        get() = entity!!.name
    val logPanel = LogPanel()
    val journeyEditor = JourneyEditor(this)
    val sourceEditor = JsonEditor(Simulation.serializer(), classes = setOf("panel-view-nav1"))
    val propertiesEditor = SimulationPropertiesEditor()
    val descriptionEditor = ModelCodeEditor("markdown").apply {
        onEvent { change = {entity?.description  = self.value} }
    }
    override val mainButtonGroup get() = ButtonGroup {
        add(ToolbarButton(Icon.Simulation, gettext("Start Simulation"), "i").onClick {
            runSimulation()
        })
    }
    override val mainComponent = TabPanel(classes = setOf("panel-view"), scrollableTabs = true) {
        height = mainDefaultHeight.px
        addTab(tr("Journey"), journeyEditor, Icon.Simulation.def)
        addTab(tr("Description"), descriptionEditor, Icon.Description.def)
        addTab(tr("Source"), sourceEditor, Icon.Source.def)
        addTab(tr("Logs"), Div("TODO"), Icon.List.def)
    }
    override val runPanel = RunPanel(this, false) {
        addTab(tr("Properties"), propertiesEditor, Icon.Properties.def)
        addTab(tr("Simulation"), logPanel, Icon.Simulation.def)
    }

    init {
        addPanels()
    }

    override val shortcuts: List<ShortcutProvider.Shortcut> = listOf(
            ShortcutProvider.Shortcut("Run simulation", "r") { runSimulation() }
    )
        get() = field + super.shortcuts

    override suspend fun save() {
        TODO("Not yet implemented")
    }

    override suspend fun saveAs() {
        TODO("Not yet implemented")
    }

    override suspend fun openAsNew() {
        TODO("Not yet implemented")
    }

    override fun load(entity: Simulation): Simulation {
        super.load(entity)
        // TODO fix attributes in JSON editor
        // sourceEditor.load(model)
        propertiesEditor.load(entity)
        descriptionEditor.set(entity.description)
        entity.waypoints.forEach {
            (journeyEditor.googleMap as GoogleMap).addMarker(it.location)
        }

        return entity
    }

    fun runSimulation() {
        console.log("TODO start simulation")
    }
}
