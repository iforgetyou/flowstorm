package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.User
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource

object UserResource : EntityResource<User>(resourceUri = "/users", serializer = User.serializer()) {
    suspend fun getUser(userId: Id) = remoteCall<User>("/${userId.value}")
}