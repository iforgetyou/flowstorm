package ai.flowstorm.studio.resources

import ai.flowstorm.studio.App
import ai.flowstorm.studio.model.analytics.Report
import ai.flowstorm.studio.util.*
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.model.Query.Filter
import ai.flowstorm.common.model.Query.Operator.*
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.AbstractResource
import ai.flowstorm.common.resources.EntityResource
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.TimeZone
import kotlinx.datetime.minus
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import kotlin.js.Date

open class BaseReportResource : AbstractResource<Report>(serializer = Report.serializer()) {

    suspend fun data(reportSettings: ReportSettings): Report {
        val query = createQuery(reportSettings)
        val params = createParams(reportSettings)

        return remoteCall("$query&$params", deserializer = serializer)
    }

    private fun createParams(reportSettings: ReportSettings): String {
        val params: MutableList<Pair<String, String>> = mutableListOf()
        params.add("granularity" to reportSettings.granularity)
        reportSettings.aggregations.split(",").forEach {
            params.add("aggregations" to it)
        }

        val q = params.map { "${it.first}=${it.second}" }.joinToString("&")
        return q
    }

    private fun createQuery(reportSettings: ReportSettings): Query {
        val filters: MutableList<Filter> = mutableListOf()

        val dateFrom = reportSettings.dateFrom.dayStart().toInstant(TimeZone.UTC)
        val dateTo = reportSettings.dateTo.dayEnd().toInstant(TimeZone.UTC)

        filters.add(Filter("datetime", gte, dateFrom))
        filters.add(Filter("datetime", lte, dateTo))

        with(reportSettings) {
            if (users_id.isNotEmpty()) filters.add(Filter("user._id", `in`, users_id.map { it.value }))
            if (namespaces.isNotEmpty()) filters.add(Filter("metrics.namespace", `in`, namespaces.map { it.value }))
            if (metrics.isNotEmpty()) filters.add(Filter("metrics.name", `in`, metrics.map { it.value }))
            if (applications.isNotEmpty()) filters.add(Filter("application._id", `in`, applications.map { it.value }))
            if (spaces.isNotEmpty()) filters.add(Filter("properties.organization_id", `in`, spaces.map { it.value }))

            when (ReportSettings.DataSource.valueOf(dataSource)) {
                ReportSettings.DataSource.Test -> filters.add(Filter("test", eq, true))
                ReportSettings.DataSource.Live -> filters.add(Filter("test", eq, false))
                else -> {
                }
            }
        }

        return Query(filters)
    }

    @Serializable
    data class ReportSettings(
            var users_id: List<Id> = listOf(),
            @Contextual //TODO Replace by LocalDateTime when we upgrade to KVision 5
            val dateFrom: Date = now().minus(14, DateTimeUnit.DAY, App.timeZone).toDate(),
            @Contextual //TODO Replace by LocalDateTime when we upgrade to KVision 5
            val dateTo: Date = Date().dayStart(), // today at 00:00am
            val namespaces: List<Id> = listOf(),
            val metrics: List<Id> = listOf(),
            val dataSource: String = DataSource.Live.name,
            val applications: List<Id> = listOf(),
            val spaces: List<Id> = listOf(),
            val granularity: String = "DAY",
            val aggregations: String = "METRIC"
    ) {
        enum class DataSource { All, Live, Test }
    }
}
