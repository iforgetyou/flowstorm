package ai.flowstorm.studio.ui.form

import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.resources.space.UserAssignmentResource
import ai.flowstorm.studio.ui.space.UserAssignmentEditor
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.form.NamedEntityRef
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize

class UserRef(label: String = tr("User")) : NamedEntityRef<User>(label = label) {
    init {
        linkButton.onClick {
            idPair?.first.let { id ->
                mainScope.launch {
                    UserAssignmentResource.load(id!!).let {
                        Modal(tr("User Assignment"), size = ModalSize.LARGE) {
                            add(UserAssignmentEditor().apply {
                                load(it)
                            })
                        }.show()
                    }
                }
            }
        }
    }
}