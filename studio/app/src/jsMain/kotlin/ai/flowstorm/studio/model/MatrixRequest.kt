package ai.flowstorm.studio.model

import kotlinx.serialization.Serializable

@Serializable
data class MatrixRequest(
        // Names changed to map directly to request parameteres
        val decision_node: Int,
        val name: String,
        val lang: String = "en",
        val algorithm: String = "FastText",
        val use_tfidf: Boolean = false
) {
    enum class Algorithm { FastText, FastTextSW, Sent2Vec }
}