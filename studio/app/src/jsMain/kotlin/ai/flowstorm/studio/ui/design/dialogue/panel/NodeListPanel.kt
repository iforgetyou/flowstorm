package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.Graph.*
import ai.flowstorm.common.ui.ObjectList
import io.kvision.core.Background
import io.kvision.core.Color
import io.kvision.core.Cursor
import io.kvision.html.Icon
import io.kvision.html.span
import io.kvision.panel.SimplePanel

open class NodeListPanel(provider: ObjectProvider<Node>) : ObjectList<Node>(provider) {

    override fun row(item: Node, index: Int, list: MutableList<Node>): SimplePanel {
        return SimplePanel(classes = setOf("object-list-row")) {
            cursor = Cursor.POINTER
            draggable = true
            add(nodeIcon(item))
            span(item.label + if (item.id != Node.NEW_ID) " (${item.id})" else "")
        }
    }

    protected fun nodeIcon(node: Node) = with(NodeIcons.icons[node::class.simpleName!!] ?: ("question" to 0x121925)) {
        Icon(first).apply {
            Color.hex(second).let {
                if (first.startsWith("icon"))
                    background = Background(it)
                else
                    color = it
            }
        }
    }

    override fun filter(item: Node, search: String): Boolean {
        val texts = mutableListOf(
                item.label,
                item.name,
                item.id.toString(),
                item.comment
        )
        when (item) {
            is Speech -> texts.addAll(item.responses)
            is Function -> texts.add(item.code)
            is UserInput -> texts.add(item.code)
            is ReInput -> texts.add(item.code)
            is Intent -> texts.addAll(item.utterances + item.negativeUtterances)
            is GlobalIntent -> texts.addAll(item.utterances + item.negativeUtterances)
            is SubDialogue -> texts.addAll(listOfNotNull(item.code, item.rcode, item.sourceId?.value))
            is Command -> texts.add(item.code)
        }
        val lowerCaseSearch = search.lowercase()
        return texts.any { it.lowercase().contains(lowerCaseSearch) }
    }
}
