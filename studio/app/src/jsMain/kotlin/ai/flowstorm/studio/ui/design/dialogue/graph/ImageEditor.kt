package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.studio.model.Graph.Image
import io.kvision.core.onEvent
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.form.UrlRef
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.tr
import io.kvision.i18n.I18n.gettext

class ImageEditor(graphEditor: GraphEditor) : NodeEditor<Image>(graphEditor) {

    override val form = object : NodeForm(Image.serializer()) {
        init {
            add(
                Image::label,
                Text(label = tr("Label"))
            )
            add(
                Image::name, Text(label = tr("Name")),
                required = true,
                validatorMessage = { gettext("Node name must be unique.") },
                validator = { text ->
                    !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                }
            )
            add(Image::source, UrlRef(label = tr("URL")).apply {
                onEvent {
                    change = {
                        if (this@apply.value != null)
                            graphEditor.diagramPanel.diagram.setNodeProperty(node!!.id, "source", this@apply.value ?: "")
                    }
                }
            })
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
        }
    }

    override fun load(node: Image) {
        super.load(node)
        form.load(node)
    }
}