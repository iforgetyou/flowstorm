package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.File
import ai.flowstorm.common.resources.EntityResource
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object FileAssetResource : EntityResource<File>(serializer = File.serializer()) {

    init {
        AppState.sub { it.spaceId }.onEach { DialogueMixinResource.clear() }.launchIn(App.scope)
    }

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/files"
}
