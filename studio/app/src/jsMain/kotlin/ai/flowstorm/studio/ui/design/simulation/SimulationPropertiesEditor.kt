package ai.flowstorm.studio.ui.design.simulation

import ai.flowstorm.studio.model.Simulation
import ai.flowstorm.studio.ui.form.DialogueId
import ai.flowstorm.common.ui.PropertiesEditor
import ai.flowstorm.common.ui.form
import io.kvision.form.check.CheckBox
import io.kvision.form.range.Range
import io.kvision.form.text.Text
import io.kvision.form.time.DateTime
import io.kvision.i18n.tr

class SimulationPropertiesEditor : PropertiesEditor<Simulation>() {

    override val form = form(Simulation.serializer()) {
        add(
                Simulation::name,
                Text(label = tr("Simulation name")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Simulation::dialogue_id,
                DialogueId(label = tr("Dialogue")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                Simulation::timeSpeed,
                Range(min = 1, max = 10, step = 1, label = tr("Time speed"))
        )
        add(
                Simulation::adaptiveSpeed,
                CheckBox(label = tr("Adaptive speed"))
        )
//TODO fix when we update to KVision 5
//        add(
//                Simulation::datetime,
//                DateTime(format = "YYYY-MM-DD HH:mm", label = tr("Start date and time")),
//                required = true,
//                requiredMessage = tr("Required field")
//        )
    }

    init {
        init()
    }
}