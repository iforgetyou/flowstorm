package ai.flowstorm.studio.ui.design.entity

import ai.flowstorm.studio.model.EntityDataset
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class SamplesForm(model: EntityResource<EntityDataset.DataSample>): EntityForm<EntityDataset.DataSample>(model) {
    init {
        add(
            EntityDataset.DataSample::text,
            Text(label = tr("Text")),
            required = true,
            requiredMessage = tr("Required field"))
    }
}