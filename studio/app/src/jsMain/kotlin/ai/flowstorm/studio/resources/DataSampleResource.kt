package ai.flowstorm.studio.resources

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.EntityDataset
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource

class DataSampleResource(private val datasetId: Id): EntityResource<EntityDataset.DataSample>(serializer = EntityDataset.DataSample.serializer()) {
    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/entityDatasets/${datasetId.value}/samples"
}
