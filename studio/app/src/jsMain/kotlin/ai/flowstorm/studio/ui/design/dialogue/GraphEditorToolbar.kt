package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.ui.design.dialogue.panel.DiagramPanel
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.ToolbarButton
import ai.flowstorm.common.ui.toolBarButton
import io.kvision.core.PosFloat
import io.kvision.i18n.gettext
import io.kvision.state.sub
import io.kvision.toolbar.Toolbar
import io.kvision.toolbar.buttonGroup

class GraphEditorToolbar(val diagramPanel: DiagramPanel) : Toolbar(classes = setOf("panel-toolbar")) {

    lateinit var undoButton: ToolbarButton
    lateinit var redoButton: ToolbarButton
    lateinit var historyButton: ToolbarButton
    lateinit var copyButton: ToolbarButton
    lateinit var pasteButton: ToolbarButton
    lateinit var deleteButton: ToolbarButton
    lateinit var leftPanelButton: ToolbarButton
    lateinit var rightPanelButton: ToolbarButton

    val graphEditor by diagramPanel::graphEditor

    init {
        buttonGroup {
            float = PosFloat.LEFT
            undoButton = toolBarButton(Icon.Undo, gettext("Undo"), "CTRL+Z").onClick {
                graphEditor.undo()
            }
            redoButton = toolBarButton(Icon.Redo, gettext("Redo"), "CTRL+Y").onClick {
                graphEditor.redo()
            }
            historyButton = toolBarButton(Icon.History, gettext("History")).onClick {
                graphEditor.openHistory()
            }
            copyButton = toolBarButton(Icon.Copy, gettext("Copy"), "CTRL+C").onClick {
                graphEditor.copyToClipboard()
            }
            pasteButton = toolBarButton(Icon.Paste, gettext("Paste"), "CTRL+V").onClick {
                graphEditor.pasteFromClipboard()
            }
            deleteButton = toolBarButton(Icon.Delete, gettext("Delete")).onClick {
                graphEditor.delete()
            }
            toolBarButton(Icon.ZoomIn, gettext("Zoom In"), "+").onClick {
                graphEditor.zoomIn()
            }
            toolBarButton(Icon.ZoomOut, gettext("Zoom Out"), "-").onClick {
                graphEditor.zoomOut()
            }
            toolBarButton(Icon.Fit, gettext("Fit to View"), ".").onClick {
                graphEditor.fitToViewport()
            }
            toolBarButton(Icon.Focus, gettext("Center Node"), "Enter").onClick {
                graphEditor.focusNode()
            }
        }
        buttonGroup {
            leftPanelButton = toolBarButton(Icon.LeftPanel, gettext("Toggle Left Panel"), "2") {
                active = graphEditor.leftPanelEnabled.value
                onClick {
                    graphEditor.toggleLeftPanel()
                }
            }
            rightPanelButton = toolBarButton(Icon.RightPanel, gettext("Toggle Right Panel"), "3") {
                active = graphEditor.rightPanelEnabled.value
                onClick {
                    graphEditor.toggleRightPanel()
                }
            }
        }

        graphEditor.history.canUndo.subscribe { undoButton.disable(!it) }
        graphEditor.history.canRedo.subscribe { redoButton.disable(!it) }
        graphEditor.leftPanelEnabled.subscribe { leftPanelButton.active = it }
        graphEditor.rightPanelEnabled.subscribe { rightPanelButton.active = it }
        GraphClipboard.graph.subscribe { pasteButton.disable(it == null || graphEditor.readOnly) }
        diagramPanel.diagram.sub { it.selection }.subscribe {
            copyButton.disable(it == null || (it.size == 1 && it.links.size == 1) )
            deleteButton.disable(it == null || graphEditor.readOnly )
        }
    }

    fun disableButtons() {
        deleteButton.disable(graphEditor.readOnly || graphEditor.selection == null)
        pasteButton.disable(graphEditor.readOnly || GraphClipboard.graph.value == null)
        undoButton.disable(graphEditor.readOnly || !graphEditor.history.canUndo.value)
        redoButton.disable(graphEditor.readOnly || !graphEditor.history.canRedo.value)
        historyButton.disable(graphEditor.readOnly)
    }
}