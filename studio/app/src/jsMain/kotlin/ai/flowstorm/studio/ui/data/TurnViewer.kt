package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Input
import ai.flowstorm.studio.model.Turn
import ai.flowstorm.studio.resources.TurnResource
import ai.flowstorm.studio.ui.design.dialogue.panel.LogPanel
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityComponent
import ai.flowstorm.common.ui.JsonEditor
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.html.Div
import io.kvision.i18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.panel.tabPanel
import kotlinx.serialization.json.JsonElement

class TurnViewer (val model: TurnResource) : SimplePanel(), EntityComponent<Turn> {
    override var entity: Turn? = null
    private val turnAttributes = JsonEditor(JsonElement.serializer())
    private val request = JsonEditor(Turn.Request.serializer())
    private val input = JsonEditor(Input.serializer())
    private val logPanel = LogPanel()


    override fun load(entity: Turn): Turn {
        add(SimplePanel(init = { padding = UiHelpers.spacing }).add(TurnForm().load(entity)))
        turnAttributes.setData(entity.attributes)
        // Request isn't used anymore
        // entity.request?.let { request.setData(it)}
        input.setData(entity.input)
        logPanel.log(entity.log.map { LogPanel.convertToClickable(it, "dark") })
        tabPanel {
            addTab(tr("Response"), Div(content = entity.response, rich = true, init = { padding = UiHelpers.spacing }))
            // addTab(tr("Request"), request)
            addTab(tr("Input"), input)
            addTab(tr("Logs"), SimplePanel(init = { padding = UiHelpers.spacing }).add(logPanel))
            addTab(tr("Attributes"), turnAttributes)
        }
        return entity
    }

    override fun load(id: Id): Turn {
        val turn = model.get(id)
        entity = turn
        return load(turn)
    }
}
