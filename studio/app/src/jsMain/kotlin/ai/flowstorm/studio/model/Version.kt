package ai.flowstorm.studio.model

import kotlinx.serialization.Serializable
import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.id.StringId

@Serializable
data class Version(
    var name: String,
    var value: String? = null
) : HasId {
    override val id get() = StringId(name)
}