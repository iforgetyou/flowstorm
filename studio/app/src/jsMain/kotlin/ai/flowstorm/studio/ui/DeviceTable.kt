package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.DeviceResource
import ai.flowstorm.studio.ui.space.DeviceEditor
import ai.flowstorm.studio.ui.space.DeviceForm
import ai.flowstorm.common.ui.EditableEntityComponent
import ai.flowstorm.common.ui.EntityTable
import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.model.Space
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.i18n.I18n.tr
import io.kvision.modal.ModalSize

class DeviceTable: EntityTable<Device>(
        "Client Device",
        DeviceResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Device ID"), Device::deviceId.name),
                        ColumnDefinition(tr("Description"), Device::description.name),
                        ColumnDefinition(tr("Location"), Device::location.name),
                        ColumnDefinition(tr("Last Talk"), Device::lastTalk.name),
                        ColumnDefinition(tr("Space"), Device::_space.name + "." + Space::name.name)
                )
        ),
        { DeviceForm(DeviceResource) },
        { Device() },
        entityEditorFactory = { editableEntityComponent: EditableEntityComponent<Device>, saveEnabled: Boolean, deleteEnabled: Boolean ->
            DeviceEditor(editableEntityComponent as DeviceForm, saveEnabled, deleteEnabled)
        },
        saveEnabled = true,
        deleteEnabled = false,
        modalSize = ModalSize.LARGE,
        className = "content-panel"
)
