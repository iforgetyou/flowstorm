package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.DevicePairing
import ai.flowstorm.studio.resources.space.DeviceResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.ErrorHandler
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.form.formPanel
import io.kvision.form.text.Text
import io.kvision.html.ButtonStyle
import io.kvision.html.button
import io.kvision.i18n.tr
import io.kvision.modal.Alert
import io.kvision.modal.Modal
import io.kvision.panel.simplePanel

object DevicePairingModal : Modal(tr("Device Pairing")) {

    private val mainScope = MainScope()

    init {
        simplePanel {
            padding = UiHelpers.spacing
            val form = formPanel<DevicePairing> {
                add(
                        DevicePairing::pairingCode,
                        Text(label = tr("Pairing Code")),
                        required = true,
                        requiredMessage = tr("Required field")
                )
            }
            simplePanel(classes = setOf("button-group")) {
                button(tr("Pair"), style = ButtonStyle.PRIMARY).onClick {
                    mainScope.launch {
                        try {
                            DeviceResource.pairDevice(form.getData().pairingCode!!).let {
                                Alert.show(tr("Pair Device"), tr("Device has been paired successfully."))
                            }
                        } catch (t: Throwable) {
                            ErrorHandler.handle(t)
                        }
                        UiHelpers.parent<Modal>(this@onClick)?.hide()
                    }
                }
            }
        }
    }
}