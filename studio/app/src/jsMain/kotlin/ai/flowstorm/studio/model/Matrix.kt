package ai.flowstorm.studio.model

import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.StringId
import kotlinx.serialization.Serializable

@Serializable
data class Matrix (
    val data: String? = null,
    val num_possible_classes: Int? = null,
    val accuracy: Double? = null,
    val oov: Array<String>? = null,
    val model_params: MatrixRequest? = null,
    val decision_node: Int? = null,
    val qaString: String? = null
) : HasId {
    override val id: Id
        get() = StringId(decision_node.toString())

}