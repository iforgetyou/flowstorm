package ai.flowstorm.studio.model

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.Cloneable
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.id.newId
import kotlinx.serialization.*
import io.kvision.i18n.gettext
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

/**
 * Dialogue source model V2 (for editor in studio)
 */
@Serializable
data class DialogueSource(
    override val _id: Id = newId(),
    val type: String = "v2",
    var dialogueName: String = "",
    val version: Int = NEW_VERSION,
    val contextClass: String? = null,
    var description: String = "",
    var comment: String = "",
    var language: String = "en",
    var background: String? = null,
    var parameters: String? = null,
    var speechRecognizer_id: Id? = null,
    var voice_id: Id? = null,
    var graph: Graph = Graph(),
    var initCode: String = "",
    var state: State = State.Draft,
    var space_id: Id = AppState.state.spaceId!!,
    var datetime: Instant? = null,
    val mixins_id: List<Id> = listOf(),
    var lastBuild: String? = null,
) : AbstractEntity(), Cloneable<DialogueSource> {

    var lock: String? = null
    // moved out from constructor to avoid using it in generated equals() method
    var lockTime: Instant? = null
    override var label = ""
        get() = SpaceResource.get(space_id).let {
            "[${it.name}]$dialogueName:" + if (version == NEW_VERSION) gettext("new") else version
        }

    enum class State {
        Draft, Published, Archived
    }

    val isEditable get() = state == State.Draft
    val isLockedByCurrentUser get() = lock == AuthService.context.username

    override fun clone() = Cloneable.clone(this)

    fun cloneAsNew() = clone().copy(_id = newId(), version = NEW_VERSION).apply {
        setLock()
        state = State.Draft
    }

    fun setLock() {
        lock = UserService.context.user.username
        lockTime = Clock.System.now()
    }

    fun clearLock() {
        lock = null
        lockTime = null
    }

    companion object {
        const val NEW_VERSION = 0
    }

    object Factory {
        const val DEFAULT_DIALOGUE_NAME = ""
        fun empty() = DialogueSource(dialogueName = DEFAULT_DIALOGUE_NAME).apply {
            setLock()
        }
    }
}
