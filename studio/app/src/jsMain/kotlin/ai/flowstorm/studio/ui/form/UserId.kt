package ai.flowstorm.studio.ui.form

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.resources.UserResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.RestClient
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.form.SelectId
import io.kvision.form.select.AjaxOptions
import io.kvision.utils.obj

class UserId(label: String) : SelectId(label = label) {
    init {
        ajaxOptions = AjaxOptions(
                App.config.restUrl + "/spaces/${AppState.state.spaceId!!.value}/users",
                beforeSend = RestClient.defaultBeforeSend,
                preprocessData = {
                    it.map { item ->
                        obj {
                            this.value = item._id
                            this.text = User.asString(item.name as? String, item.surname as? String, item.username as? String)
                            this.data = obj {
                                this.subtext = if (item.phoneNumber) "${item.phoneNumber}" else ""
                            }
                        }
                    }
                },
                data = obj {
                    q = "{{{q}}}"
                },
                minLength = 3,
                requestDelay = 1000
        )
    }

    override fun onValueChange(value: Id) {
        MainScope().launch {
            updateTitle(UserResource.getUser(value).label)
        }
    }
}
