package ai.flowstorm.studio

import io.kvision.require

class Config() {

    private val data = require("conf/app.json")

    init {
        @Suppress("UNUSED_VARIABLE")
        val c = data

        @Suppress("UNUSED_VARIABLE")
        val ce = data[data.env]
        js("for (var k in ce) c[k] = ce[k];")
    }

    val namespace = (if (data.namespace != "NAMESPACE") data.namespace else data.env) as String
    val sentryDsn = data.sentryDsn as String
    val env = data.env as String
    val restUrl = data.restUrl as String
    val botUrl = data.botUrl as String
    val coreUrl = data.coreUrl as String
    val fileUrl = data.fileUrl as String
    val assistantKey = data.assistantKey as String
    val goJsKey = data.goJsKey as String
    val version = data.version as String
    val idMap = data.idMap

    operator fun get(key: String): String = data[key] as String

}