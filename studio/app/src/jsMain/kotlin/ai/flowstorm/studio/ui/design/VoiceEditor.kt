package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Voice
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityEditor

class VoiceEditor : EntityEditor<Voice>(VoiceForm(), serializer = Voice.serializer()) {

    override fun load(id: Id) = super.load(id).also {
        if (it.space_id != AppState.state.spaceId!!) {
            disableAll()
            (entityComponent as VoiceForm).configForm.disableAll()
        }
    }
}
