package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Community
import ai.flowstorm.common.resources.EntityResource

object CommunityResource : EntityResource<Community>(serializer = Community.serializer()) {

    override val resourceUri: String
        get() = "/communities"
}
