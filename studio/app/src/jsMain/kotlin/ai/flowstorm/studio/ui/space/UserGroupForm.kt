package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.resources.space.ApplicationResource
import ai.flowstorm.studio.resources.space.UserResource
import kotlinx.coroutines.launch
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.UiHelpers.options
import ai.flowstorm.common.ui.form.ListIdSelect
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class UserGroupForm(model: EntityResource<Space.UserGroup>) : EntityForm<Space.UserGroup>(model) {

    private val usersIdSelect = ListIdSelect(label = tr("Users"))
    private val applicationsIdSelect = ListIdSelect(label = tr("Applications"))

    init {
        add(
                Space.UserGroup::name,
                Text(label = tr("Name")),
                required = true,
                requiredMessage = tr("Required field")
        ) {
            (it.value?.length ?: 0) > 0
        }
        add(Space.UserGroup::note, Text(label = tr("Note")))
        add(Space.UserGroup::users_id, usersIdSelect)
        add(Space.UserGroup::applications_id, applicationsIdSelect)
    }

    override fun load(entity: Space.UserGroup): Space.UserGroup {
        mainScope.launch {
            UserResource.load().let { usersIdSelect.options = options(it) }
            ApplicationResource.load().let { applicationsIdSelect.options = options(it) }
        }
        form.fields["name"]!!.disabled = (entity.name != null)
        return super.load(entity)
    }
}