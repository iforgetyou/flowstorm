package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Intent
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.ListIdSelect
import io.kvision.core.onEvent
import io.kvision.form.spinner.Spinner
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class IntentEditor(graphEditor: GraphEditor) : NodeEditor<Intent>(graphEditor), EditableCode {

    init {
        graphEditor.subscribe {
            mainScope.launch {
                mixinSelect.options = UiHelpers.options(DialogueMixinResource.load("Intent", it.language))
            }
        }
    }

    val mixinSelect = ListIdSelect(label = tr("Mixins")).apply {
        mainScope.launch {
            options = UiHelpers.options(DialogueMixinResource.load("Intent", graphEditor.state.language))
        }
        onEvent {
            change = {
                mainScope.launch {
                    codeEditor.set(codeEditor.value, this@apply.getValue() ?: listOf())
                }
            }
        }
    }

    override val codeEditor = CodeEditor("flowstorm", setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.utterances.joinToString("\n") != self.value) {
                    node?.utterances = self.value.lines()
                    graphEditor.commit(gettext("Intent utterances changed") + " [#${node!!.id}]", "intent[${node!!.id}].utterances")
                }
            }
        }
    }

    val negUtterancesEditor = CodeEditor("flowstorm", setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.negativeUtterances.joinToString("\n") != self.value) {
                    node?.negativeUtterances = self.value.lines()
                    graphEditor.commit(gettext("Intent negative utterances changed") + " [#${node!!.id}]", "intent[${node!!.id}].negativeUtterances")
                }
            }
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
            negUtterancesEditor.readOnly = value
        }

    override val form = object : NodeForm(Intent.serializer()) {
        init {
            add(
                    Intent::label,
                    Text(label = tr("Label")),
            )
            add(Intent::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
            add(
                    Intent::mixins_id,
                    mixinSelect

            )
            add(Intent::threshold, Spinner(label = tr("Threshold"), min = 0, max = 1, step = 0.01, decimals = 2).apply {
                onEvent {
                    change = { if (validate()){ save() } }
                }
            })
            add(Intent::entities, Text(label = tr("Entities")))
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Examples"), codeEditor, Icon.Text.def)
            addTab(tr("Negative Examples"), negUtterancesEditor, Icon.Strikethrough.def)
            activeIndex = 1
        }

        override fun tabInit(index: Int) = when (index) {
            1 -> codeEditor.focus()
            else -> super.tabInit(index)
        }
    }

    override fun load(node: Intent) {
        super.load(node)
        form.load(node)
        codeEditor.set(node.utterances.joinToString("\n"), node.mixins_id)
        negUtterancesEditor.set(node.negativeUtterances.joinToString("\n"))
    }
}