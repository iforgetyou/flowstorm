package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.resources.space.ApplicationResource
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityEditor
import io.kvision.utils.perc

// Third layer of the right side of application table. Wrapper for ApplicationForm (fourth layer, unedited in this branch)
// which adds the Save and Delete buttons and JSON tab.
class ApplicationEditor(val applicationForm: ApplicationForm) : EntityEditor<Application>(applicationForm, serializer = Application.serializer()) {

    override var entity: Application? = null

    init {
        height = 100.perc
    }

    override fun load(entity: Application): Application {
        applicationForm.load(entity)
        jsonEditor.setData(entity)
        return entity
    }

    override fun load(id: Id): Application {
        entity = ApplicationResource.get(id)
        return load(entity ?: Application())
    }

}
