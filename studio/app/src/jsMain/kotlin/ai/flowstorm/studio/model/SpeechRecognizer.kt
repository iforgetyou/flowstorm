package ai.flowstorm.studio.model

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.serialization.Serializable
import ai.flowstorm.core.model.SttConfig

@Serializable
data class SpeechRecognizer(
    override var _id: Id = newId(),
    var name: String? = null,
    val sttConfig: SttConfig = SttConfig(),
    val space_id: Id = AppState.state.spaceId!!,
    var description: String = ""
) : AbstractEntity() {
    override val label
        get() = SpaceResource.get(space_id).let {
            "[${it.name}]$name"
        }
}
