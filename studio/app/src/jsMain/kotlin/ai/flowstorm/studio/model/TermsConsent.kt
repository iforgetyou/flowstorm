package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class TermsConsent(
    var _id: Id,
    val datetime: Instant,
    val user_id: Id,
    val terms_id: Id
)