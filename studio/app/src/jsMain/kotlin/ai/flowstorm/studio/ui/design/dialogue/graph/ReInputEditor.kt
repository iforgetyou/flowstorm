package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.ReInput
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.studio.model.Graph
import io.kvision.core.onEvent
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class ReInputEditor(graphEditor: GraphEditor) : NodeEditor<ReInput>(graphEditor), EditableCode {

    override val codeEditor = CodeEditor(classes = setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.code != self.value) {
                    node?.code = self.value
                    graphEditor.commit(gettext("ReInput code changed") + " [#${node!!.id}]", "userInput[${node!!.id}].code")
                }
            }
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
        }

    override val form = object : NodeForm(ReInput.serializer()) {
        init {
            add(
                ReInput::label,
                Text(label = tr("Label")),
            )
            add(ReInput::name, Text(label = tr("Name")),
                required = true,
                validatorMessage = { gettext("Node name must be unique.") },
                validator = { text ->
                    !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                }
            )
            add(ReInput::skipGlobalIntents, CheckBox(label = tr("Skip Global Intents")))
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Code"), codeEditor, Icon.Function.def)
        }

        override fun tabInit(index: Int) = when (index) {
            1 -> codeEditor.focus()
            else -> super.tabInit(index)
        }
    }

    override fun load(node: ReInput) {
        super.load(node)
        form.load(node)
    }
}