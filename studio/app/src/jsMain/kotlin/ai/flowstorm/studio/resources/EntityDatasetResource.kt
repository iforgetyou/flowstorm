package ai.flowstorm.studio.resources

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.EntityDataset
import ai.flowstorm.common.resources.EntityResource
import kotlinx.coroutines.await
import io.kvision.rest.HttpMethod

object EntityDatasetResource : EntityResource<EntityDataset>(serializer = EntityDataset.serializer()) {

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/entityDatasets"


    suspend fun train(dataset: EntityDataset) {
        client.remoteCall(
            client.baseUrl + "${resourceUri}/${dataset.id.value}/train",
            method = HttpMethod.PUT,
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun status(dataset: EntityDataset): String? {
        if (dataset.isNew) {
            return null
        }
        return try {
            client.remoteRequest(
                client.baseUrl + "${resourceUri}/${dataset.id.value}/status",
                method = HttpMethod.GET,
                deserializer = EntityDataset.Status.serializer(),
                beforeSend = client.beforeSend
            ).then { it.data.status }
                .catch { null }
                .await()
        } catch (e: Exception) {
            null
        }
    }
}
