package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.DialogueSource.State.*
import ai.flowstorm.studio.ui.design.DialogueSourceEditor
import ai.flowstorm.studio.util.toDate
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.ui.ConfirmModal
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.html.Button
import io.kvision.html.ButtonStyle
import io.kvision.html.Span
import io.kvision.html.span
import io.kvision.i18n.gettext
import io.kvision.i18n.tr
import io.kvision.moment.Moment
import io.kvision.panel.SimplePanel
import io.kvision.table.*

class Workflow(private val dialogueSourceEditor: DialogueSourceEditor) : SimplePanel() {

    val mainScope = MainScope()

    private lateinit var dialogueSource: DialogueSource
    private lateinit var author: Span
    private lateinit var state: Span
    private lateinit var lock: Span
    private var publishButton = Button(gettext("Publish Model")).apply {
        style = ButtonStyle.SUCCESS
        marginLeft = UiHelpers.spacing
        onClick {
            mainScope.launch {
                if (ConfirmModal(tr("Publish Model"), tr("Publishing model can not be undone. Would you like to continue?")).open()) {
                    dialogueSourceEditor.publish()
                }
            }
        }
    }
    private var archiveButton = Button(gettext("Archive Model")).apply {
        style = ButtonStyle.DANGER
        marginLeft = UiHelpers.spacing
        onClick {
            mainScope.launch {
                if (ConfirmModal(tr("Archive Model"), tr("Archiving model can not be undone. Would you like to continue?")).open()) {
                    dialogueSourceEditor.archive()
                }
            }
        }
    }

    private var forceUnlock = Button(gettext("Force Unlock Model")).apply {
        style = ButtonStyle.DANGER
        marginLeft = UiHelpers.spacing
        onClick {
            mainScope.launch {
                if (ConfirmModal(tr("Force Unlock Model"), tr("Force unlocking might lead to loss of another user`s unsaved data. Would you like to continue?")).open()) {
                    dialogueSourceEditor.unlock(true)
                }

            }
        }
    }

    init {
        margin = UiHelpers.spacing
        table(
                types = setOf(TableType.BORDERLESS, TableType.SMALL),
                responsiveType = ResponsiveType.RESPONSIVE
        ) {
//            row {
//                cell {
//                    +"Author"
//                }
//                cell {
//                    author = span("Author")
//                }
//            }
            row {
                cell {
                    +tr("State")
                }
                cell {
                    state = span("State")
                }
                cell {
                    add(publishButton)
                    add(archiveButton)
                }
            }
            row {
                cell {
                    +tr("Currently open")
                }
                cell {
                    lock = span("no")
                }
                cell {
                    add(forceUnlock)
                }
            }
        }
    }

    fun load(dialogueSource: DialogueSource) {
        this.dialogueSource = dialogueSource
        updateUI()
    }

    fun updateUI() {
        publishButton.disabled = dialogueSource.isNew || dialogueSource.state != Draft || !dialogueSource.isLockedByCurrentUser
        archiveButton.disabled = dialogueSource.isNew || dialogueSource.state == Archived || (!dialogueSource.isLockedByCurrentUser && dialogueSource.state != Published)
        state.content = if (dialogueSource.isNew) tr("New") else dialogueSource.state.name

        val time: String = if (dialogueSource.lockTime != null) Moment(dialogueSource.lockTime!!.toDate()).format(gettext("YYYY/MM/DD HH:mm")) as String
        else tr("no")
        lock.content = "${time} ${dialogueSource.lock ?: ""}"
        forceUnlock.disabled = dialogueSource.isNew || AuthService.context.username == dialogueSource.lock || dialogueSource.lock == null
    }
}