package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Terms
import ai.flowstorm.studio.resources.space.TermsResource
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityEditor
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.html.button
import io.kvision.i18n.tr
import io.kvision.panel.SimplePanel
import kotlinx.browser.window

class TermsEditor : EntityEditor<Terms>(TermsForm(TermsResource), deleteEnabled = false, serializer = Terms.serializer()) {

    override fun load(id: Id) = super.load(id).also {
        if (it.space_id != AppState.state.spaceId!!) {
            disableAll()
        }
    }

    override fun otherButtons(panel: SimplePanel) {
        panel.button(tr("Preview")) {
            marginRight = UiHelpers.spacing
        }.onClick {
            window.open("#!/terms/${entity?._id?.value}", "_blank")
        }
    }
}
