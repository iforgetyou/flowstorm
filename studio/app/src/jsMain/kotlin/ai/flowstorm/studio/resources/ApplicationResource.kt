package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Application
import ai.flowstorm.common.resources.EntityResource

object ApplicationResource : EntityResource<Application>(resourceUri = "/applications", serializer = Application.serializer())