package ai.flowstorm.studio.resources.space.report

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Namespace
import ai.flowstorm.common.resources.EntityResource

object NamespaceResource : EntityResource<Namespace>(serializer = Namespace.serializer()) {

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/reports/namespaces"
}
