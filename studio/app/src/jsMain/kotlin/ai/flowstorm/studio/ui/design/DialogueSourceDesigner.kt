package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.space.DialogueSourceResource
import ai.flowstorm.studio.ui.design.dialogue.OpenDialogueModal
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.ui.design.dialogue.panel.AssistantPanel
import ai.flowstorm.common.Toast
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.StringId
import io.kvision.panel.Tab
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.*
import io.kvision.core.*
import io.kvision.i18n.gettext
import io.kvision.window.window
import kotlinx.browser.document
import kotlinx.coroutines.sync.withLock

class DialogueSourceDesigner : ModelDesigner<DialogueSource>(), Panel, Navigable {

    override val caption = "Dialogues"

    override suspend fun getNewModel(): DialogueSource {
        return DialogueSourceResource.getTemplate("main").also {
            it.setLock()
        }
    }

    override suspend fun load(model: DialogueSource) {
        val existingTab = getTabs().firstOrNull {
            val component = it.component
            AssistantPanel.dialoguePanel.setControls(true)
            component is DialogueSourceEditor && component.entity!!._id == model._id
        }

        activeTab = existingTab ?: run {
            if (!model.isNew && model.graph == Graph()) {
                model.graph = DialogueSourceResource.loadGraph(model)
            }
            AssistantPanel.dialoguePanel.setControls(true)
            val editor = DialogueSourceEditor(this@DialogueSourceDesigner)
            AssistantPanel.dialoguePanel.botPanel = editor.runPanel.botPanel
            editor.load(model)
            Tab(editor.name, editor, closable = true).also {
                it.setLinkTooltip(editor.name)
                it.onEvent {
                    click = {
                        AssistantPanel.dialoguePanel.botPanel = editor.runPanel.botPanel
                        AssistantPanel.dialoguePanel.setControls(true)
                    }
                    dblclick = {
                        @Suppress("UNUSED_VARIABLE")
                        val name = editor.name
                        js("window.navigator.clipboard.writeText(name);")
                        Toast.info(gettext("Copied to clipboard"))
                    }
                }
                addTab(it)
            }
        }
    }

    override suspend fun load(id: Id) {
        load(DialogueSourceResource.load(id, false))
    }

    fun load(id: Id, nodeId: Int?) = mainScope.launch {
        load(id)
        nodeId?.let { (activeChild as DialogueSourceEditor).graphEditor.diagramPanel.diagram.selectNodeById(nodeId) }
    }

    override suspend fun new() {
        val dialogue = DialogueSourceResource.getTemplate("subdialogue")
        dialogue.setLock()
        dialogue.space_id = AppState.state.spaceId!!
        load(dialogue)
    }

    override suspend fun open() {
        OpenDialogueModal().open().let {
            load(it)
        }
    }

    override fun onNavigate(route: String, params: dynamic) {
        if (params != null) {
            mainScope.launch {
                loadingModel.withLock {
                    when (params.id) {
                        "new" -> new()
                        "open" -> open()
                        else -> load(StringId(params.id as String))
                    }
                }
            }
        }
    }
}
