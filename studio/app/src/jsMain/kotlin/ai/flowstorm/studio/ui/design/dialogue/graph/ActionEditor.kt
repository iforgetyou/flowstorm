package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Action
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.common.ui.Icon
import io.kvision.form.text.Text
import io.kvision.i18n.I18n
import io.kvision.i18n.I18n.tr

class ActionEditor(graphEditor: GraphEditor) : NodeEditor<Action>(graphEditor) {

    override val form = object : NodeForm(Action.serializer()) {
        init {
            add(Action::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { I18n.gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
            add(Action::action, Text(label = tr("Action")), required = true)
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
        }
    }

    override fun load(node: Action) {
        super.load(node)
        form.load(node)
    }
}