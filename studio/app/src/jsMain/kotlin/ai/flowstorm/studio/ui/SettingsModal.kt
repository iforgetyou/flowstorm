package ai.flowstorm.studio.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.ui.UiState.Action.ChangeLanguage
import ai.flowstorm.studio.resources.CurrentUserResource
import ai.flowstorm.studio.resources.space.ProfileResource
import ai.flowstorm.common.ui.EntityEditor
import ai.flowstorm.common.Reloadable
import ai.flowstorm.studio.model.User
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.auth.UserService
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal

class SettingsModal : Modal(tr("User Settings")), Reloadable {

    val mainScope = MainScope()
    private val userForm = UserForm(CurrentUserResource)

    inner class ProfileEditor : EntityEditor<User>(userForm, true, false, serializer = User.serializer()) {

        override suspend fun save() {
            super.save()
            this@SettingsModal.hide()
            AppState.dispatch(ChangeLanguage((entityComponent as UserForm).getLanguage()))
        }

        override fun onload(entity: User): User {
            deleteButton?.disabled = entity.isNew
            jsonEditor.setData(entity)
            // we always switch to first tab (due to problems with re-rendering of JSON editor)
            tabPanel.activeIndex = 0
            return entity
        }
    }

    private val editor = ProfileEditor()

    init {
        add(editor)
    }

    override fun reload() {
        mainScope.launch {
            UserService.context.user.let { user ->
                try {
                    ProfileResource.getByUserId(user._id).let {
                        user.attributes = it.attributes
                        editor.load(user)
                        userForm.changeForProfilePage()
                    }
                } catch (e: NoSuchElementException) {
                    editor.load(user)
                    userForm.changeForProfilePage()
                }
            }
        }
    }
}
