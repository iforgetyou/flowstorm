package ai.flowstorm.studio.ui.form

import ai.flowstorm.studio.resources.space.SessionResource
import ai.flowstorm.studio.ui.data.SessionViewer
import kotlinx.coroutines.launch
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.ui.form.TextRef
import org.w3c.dom.events.MouseEvent
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize

class SessionRef : TextRef(label = tr("Session ID")) {

    override fun onButtonClick(e: MouseEvent) {
        mainScope.launch {
            SessionResource().let { model ->
                model.reload(Query(mutableListOf(
                        Query.Filter("sessionId", Query.Operator.eq, input.value!!)
                )))
                Modal(tr("Session"), size = ModalSize.LARGE) {
                    add(SessionViewer(model).apply {
                        load(model.first())
                    })
                }.show()
            }
        }
    }
}