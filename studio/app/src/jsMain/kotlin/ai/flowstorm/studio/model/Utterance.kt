package ai.flowstorm.studio.model

import ai.flowstorm.studio.util.toDate
import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.id.Id
import kotlinx.serialization.Serializable
import io.kvision.i18n.gettext
import io.kvision.moment.Moment
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.plus

@Serializable
class Utterance(val turn: Turn, val persona: Persona, override val id: Id = turn.turnId) : HasId {

    enum class Persona { User, Bot }

    val personaName = persona.name
    val timeAsString =
        Moment(
            (if (persona == Persona.User)
                turn.datetime
            else
                turn.datetime.plus(turn.duration ?: 0, DateTimeUnit.MILLISECOND)).toDate()
        ).format(gettext("HH:mm:ss.SSS")) as String
    val content = "<b>$persona</b>" + when (persona) {
        Persona.User -> {
            val text = turn.input.transcript.text
            if (text.startsWith('#')) {
                " " + when (text) {
                    "#intro" -> gettext("initiated conversation")
                    "#bye" -> gettext("left conversation")
                    "#signal" -> gettext("transmitted signal data")
                    "#silence" -> gettext("was silent")
                    "#toomanywords" -> gettext("said too many words")
                    else -> gettext("emitted action") +" $text"
                }
            } else ": $text"
        }
        Persona.Bot -> turn.response.let { text ->
            when {
                text.isBlank() -> " " + gettext("stayed silent")
                text.startsWith('#') -> " " + gettext("emitted command") + " $text"
                else -> ": $text"
            }
        }
    }
}
