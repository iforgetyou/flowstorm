package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.model.SpeechRecognizer
import ai.flowstorm.studio.resources.space.SpeechRecognizerResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

class SpeechRecognizerTable: EntityTable<SpeechRecognizer>(
    "Speech Recognizer",
    SpeechRecognizerResource,
    TabulatorOptions(
        layout = Layout.FITCOLUMNS,
        columns = listOf(ColumnDefinition(tr("Name"), SpeechRecognizer::label.name))
    ),
    { SpeechRecognizerEditor() },
    { SpeechRecognizer() },
    //deleteEnabled = false,
    splitMode = true,
    tabWidth = 320.px,
    className = "content-panel"
)