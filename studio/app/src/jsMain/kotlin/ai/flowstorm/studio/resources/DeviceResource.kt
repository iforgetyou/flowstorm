package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.model.MoveParameters
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.AbstractResource
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod

object DeviceResource : EntityResource<Device>(serializer = Device.serializer(), resourceUri = "/devices") {

    object MoveParametersModel : AbstractResource<MoveParameters>(serializer = MoveParameters.serializer(), resourceUri = resourceUri) {
        suspend fun move(deviceId: Id, moveParameters: MoveParameters) =
                MoveParametersModel.remoteCall("/${deviceId.value}/move", entity = moveParameters, method = HttpMethod.PUT, deserializer = Device.serializer())
    }
}