package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.resources.BaseReportResource.ReportSettings
import ai.flowstorm.studio.model.analytics.Report
import ai.flowstorm.studio.model.analytics.Report.Aggregation
import ai.flowstorm.studio.model.analytics.Report.Aggregation.*
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.UiHelpers.options
import ai.flowstorm.common.ui.form.ListIdSelect
import io.kvision.form.FormHorizontalRatio
import io.kvision.form.check.RadioGroup
import io.kvision.form.select.Select
import io.kvision.form.time.DateTime
import io.kvision.html.ButtonStyle
import io.kvision.html.button
import io.kvision.html.h3
import io.kvision.i18n.tr
import io.kvision.state.ObservableValue
import io.kvision.utils.px

class ReportForm(val singleSpace: Boolean = true) : Form<ReportSettings>(ReportSettings.serializer(), horizRatio = FormHorizontalRatio.RATIO_5), Reloadable {

    private val usersIdSelect = ListIdSelect(label = tr("Users"))
    private val namespacesSelect = ListIdSelect(label = tr("Namespaces"))
    private val metricsSelect = ListIdSelect(label = tr("Metrics"))
    private val applicationsSelect = ListIdSelect(label = tr("Applications"))
    private val spacesSelect = ListIdSelect(label = tr("Spaces"))

    var reportSettings = ObservableValue<ReportSettings>(ReportSettings())

    lateinit var viewer: ReportViewer

    init {
        padding = 20.px

        add(h3(tr("Filters")))
        add(ReportSettings::users_id, usersIdSelect)
        add(ReportSettings::dateFrom, DateTime(format = "YYYY-MM-DD", label = tr("Date From")))
        add(ReportSettings::dateTo, DateTime(format = "YYYY-MM-DD", label = tr("Date To")))
        if (!singleSpace) add(ReportSettings::spaces, spacesSelect)
        add(ReportSettings::applications, applicationsSelect)
        add(ReportSettings::namespaces, namespacesSelect)
        add(ReportSettings::metrics, metricsSelect)
        add(ReportSettings::dataSource, RadioGroup(label = tr("Data"),
                options = UiHelpers.enumOptions<ReportSettings.DataSource>(),
                inline = true
        ))
        add(h3(tr("Options")))
        add(ReportSettings::granularity, Select(options = options(Report.Granularity.values()), label = tr("Granularity")))

        val aggregationOptions = if (singleSpace) arrayOf(USER, NAMESPACE, METRIC, APPLICATION) else Aggregation.values()
        add(ReportSettings::aggregations, Select(multiple = true, options = options(aggregationOptions), label = tr("Group By")))

        button(tr("Update"), style = ButtonStyle.PRIMARY).onClick {
            update()
        }
    }

    fun updateOptions(users: List<User>, metrics: List<Metric>, namespaces: List<Namespace>, applications: List<Application>, spaces: List<Space>? = null) {
        usersIdSelect.options = options(users)
        metricsSelect.options = options(metrics)
        namespacesSelect.options = options(namespaces)
        applicationsSelect.options = options(applications)
        spaces?.let { spacesSelect.options = options(it) }
    }

    override fun reload() {
        setData(reportSettings.value)
    }

    private fun update() {
        val settings = ReportSettings()
        save(settings)
        reportSettings.value = settings
    }
}