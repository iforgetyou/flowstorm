package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Terms
import ai.flowstorm.studio.resources.space.TermsResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

class TermsTable : EntityTable<Terms>(
        "Terms and Condition",
        TermsResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Label"), "label")
                )
        ),
        { TermsEditor() },
        { Terms() },
        deleteEnabled = false,
        splitMode = true,
        tabWidth = 240.px,
        className = "content-panel"
)