package ai.flowstorm.studio.model

object Enum {

    val mixinTypes = arrayOf("Intent", "Speech", "Command", "Function", "UserInput", "Init")
    val locales = arrayOf("en_US", "en_GB", "en_AU", "en_IN", "de_DE", "es_ES", "cs_CZ")
    val languages = arrayOf("en", "de", "cs", "es")
    val voiceLanguages = mapOf("George" to "en", "Grace" to "en", "Gordon" to "en", "Gwyneth" to "en", "Gabriela" to "cs", "Anthony" to "en", "Audrey" to "en", "Arthur" to "en", "Amy" to "en", "Michael" to "en", "Mary" to "en", "Milan" to "en")
    val voices = voiceLanguages.keys.toTypedArray()
    val genders = arrayOf("Male", "Female")
    val sttProviders = arrayOf("Google", "Amazon", "Microsoft", "Deepgram")
}
