package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Turn
import ai.flowstorm.studio.model.Utterance
import ai.flowstorm.studio.resources.TurnResource
import ai.flowstorm.studio.resources.UtteranceResource
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityComponent
import io.kvision.panel.SimplePanel

class TurnWrapper(val utteranceModel: UtteranceResource, turnModel: TurnResource): EntityComponent<Utterance>, SimplePanel() {
    override var entity: Utterance? = null
    lateinit var turn: Turn

    val turnViewer: TurnViewer = TurnViewer(turnModel)

    override fun load(entity: Utterance): Utterance {
        removeAll()
        turnViewer.load(turn)
        add(turnViewer)
        return entity
    }

    override fun load(id: Id): Utterance {
        val utterance = utteranceModel.get(id)
        this.turn = utterance.turn
        entity = utterance
        return load(utterance)
    }
}