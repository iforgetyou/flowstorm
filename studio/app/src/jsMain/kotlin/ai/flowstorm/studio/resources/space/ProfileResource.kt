package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Profile
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.resources.EntityResource
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json

object ProfileResource : EntityResource<Profile>(serializer = Profile.serializer()) {
    init {
        AppState.sub { it.spaceId }.onEach { clear() }.launchIn(App.scope)
    }

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/profiles"

    suspend fun getByUserId(userId: Id): Profile {
        return remoteCall("?user_id[eq]=${userId.value}", deserializer = ListSerializer(Profile.serializer())).firstOrNull()
            ?: Profile(
                StringId("empty-profile"), user_id = userId, space_id = AppState.state.spaceId!!, attributes = Json.parseToJsonElement(
                "{}"))
    }
}
