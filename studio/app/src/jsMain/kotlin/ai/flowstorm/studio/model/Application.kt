package ai.flowstorm.studio.model

import ai.flowstorm.studio.App
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.serialization.Serializable

@Serializable
data class Application(
        override val _id: Id = newId(),
        var name: String = "",
        var description: String? = null,
        var public: Boolean = false,
        var anonymousAccessAllowed: Boolean = false,
        var icon: String? = null,
        var alexaId: String? = null,
        var phoneNumber: String? = null,
        var skimmerConfigUrl: String? = null,
        var space_id: Id? = null,
        var terms_id: Id? = null,
        var dialogue_id: Id? = null,
        //@Transient @JsonSerialize
        var _space_name: String? = null
) : AbstractEntity() {
    var url: String = ""
    override val label get() = name + if (phoneNumber != null) " <$phoneNumber>" else ""

    init {
        url  = if (!isNew) (App.config.botUrl + "/${id.value}") else ""
    }
}