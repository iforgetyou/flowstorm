package ai.flowstorm.studio.model

import ai.flowstorm.studio.util.toDate
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import io.kvision.i18n.gettext
import io.kvision.moment.Moment
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class DialogueEvent(
    override val _id: Id = newId(),
    val datetime: Instant,
    val type: Type,
    val user: User,
    val sessionId: String,
    var applicationName: String,
    var dialogue_id: Id? = null,
    val nodeId: Int?,
    val space_id: Id,
    var text: String
) : AbstractEntity() {
    override val label = "DialogueEvent"

    /*
     For unknown reason(bug in serializer?) this can't be inlined and initialization is in init block.
     Serializer throws exception "this.datetime is undefined"
    */
    var stringTime: String? = null
    var stringUser: String? = null
    var stringType: String? = null

    init {
        stringTime = Moment(datetime.toDate()).format(gettext("YYYY/MM/DD HH:mm"))
        stringUser = User.asString(user.name, user.surname, user.username)
        stringType = type.toString()
    }

    enum class Type { ServerError, UserError, UserComment }

}




