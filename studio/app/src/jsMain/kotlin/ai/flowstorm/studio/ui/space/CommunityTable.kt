package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Community
import ai.flowstorm.studio.resources.space.CommunityResource
import ai.flowstorm.studio.ui.CommunityEditor
import ai.flowstorm.common.setProperty
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.obj
import io.kvision.utils.px

open class CommunityTable(
        hideHeader: Boolean = false,
        className: String? = "content-panel"
) : EntityTable<Community>(
        // EntityTable adds "s" to the caption
        "Community Attribute",
        CommunityResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), "name")
                )
        ),
        { CommunityEditor(CommunityResource) },
        splitMode = true,
        hideHeader = hideHeader,
        tabWidth = 240.px,
        className = className,
        dataExtractor = {
                obj {
                        setProperty(this, it::name)
                }
        },
)
