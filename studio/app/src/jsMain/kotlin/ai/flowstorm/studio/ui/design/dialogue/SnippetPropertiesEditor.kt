package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.Enum
import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.common.ui.PropertiesEditor
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class SnippetPropertiesEditor : PropertiesEditor<DialogueSnippet>() {

    override val form = form(DialogueSnippet.serializer()) {
        add(
            DialogueSnippet::name,
            Text(label = tr("Name")),
            required = true,
            requiredMessage = tr("Required field"),
        )
        add(
            DialogueSnippet::language,
            Select(label = tr("Language"), options = UiHelpers.options(Enum.voiceLanguages.values.distinct().toTypedArray())),
            required = true,
            requiredMessage = tr("Required field")
        )
    }

    init {
        init()
    }
}