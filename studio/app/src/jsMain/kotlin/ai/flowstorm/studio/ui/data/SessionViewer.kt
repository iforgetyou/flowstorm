package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Session
import ai.flowstorm.studio.model.Turn
import ai.flowstorm.studio.model.Utterance
import ai.flowstorm.studio.model.analytics.MetricValue
import ai.flowstorm.studio.resources.space.ProfileResource
import ai.flowstorm.studio.resources.space.SessionResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import ai.flowstorm.studio.ui.design.dialogue.panel.LogPanel
import ai.flowstorm.studio.App
import ai.flowstorm.studio.model.LogEntry
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityComponent
import ai.flowstorm.common.ui.JsonEditor
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.i18n.tr
import com.github.snabbdom.VNode
import io.kvision.jquery.JQuery
import io.kvision.panel.SimplePanel
import io.kvision.panel.tabPanel
import io.kvision.rest.RestClient
import io.kvision.state.observableListOf
import io.kvision.table.TableType
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.tabulator.tabulator
import kotlinx.serialization.json.JsonElement

class SessionViewer(val model: SessionResource) : SimplePanel(), EntityComponent<Session> {
    override var entity: Session? = null

    val restClient = RestClient()
    val mainScope = MainScope()
    val panel: SimplePanel = SimplePanel()

    private val sessionProperties = JsonEditor(JsonElement.serializer())
    private val sessionAttributes = JsonEditor(JsonElement.serializer())
    private val userAttributes = JsonEditor(JsonElement.serializer())
    private val fileUrl get() = App.config.fileUrl

    private var lastScrollTop: Float = 0F


    override fun load(entity: Session): Session {
        removeAll()
        this.entity = entity
        add(SessionForm().load(entity))
        getAudio(entity)
        val turns = observableListOf<Turn>().apply { addAll(entity.turns) }
        val utterances = observableListOf<Utterance>().apply { addAll(entity.utterances) }
        val metrics = observableListOf<MetricValue>().apply { addAll(entity.metrics) }
        add(panel)
        mainScope.launch {
            // Workaround to make the table of turns appear with correct column widths
            delay(150)
            tabPanel {
                addTab(tr("Utterances"), UtteranceTable(utterances, turns))
                addTab(tr("Turns"), TurnTable(turns))
                addTab(tr("Run Log"), LogPanel().apply {
                    paddingLeft = UiHelpers.spacing
                    transformToRunLog(entity.turns)
                })
                addTab(tr("Client Log"), LogPanel().apply {
                    paddingLeft = UiHelpers.spacing
                    transformToRunLog(entity.clientLogEntries)
                })
                addTab(tr("Text"), LogPanel().apply {
                    paddingLeft = UiHelpers.spacing
                    transformToPureText(this, entity.turns)
                })
                addTab(tr("Metrics"), MetricValueTable(metrics))
                addTab(tr("Properties"), sessionProperties)
                addTab(tr("Attributes"), sessionAttributes)
                addTab(tr("Profile"), userAttributes)
            }
        }
        return entity
    }

    override fun load(id: Id): Session {
        val session = model.get(id)
        sessionProperties.setData(session.properties)
        sessionAttributes.setData(session.attributes)
        mainScope.launch {
            ProfileResource.getByUserId(session.user._id).let {
                userAttributes.setData(it.attributes)
            }
        }
        return load(session)
    }

    @Serializable
    data class SearchResult(val name: String)

    data class BotItem(val type: String, val text: String)



    private fun transformToPureText(logPanel: LogPanel, turns: List<Turn>) =
            logPanel.log(turns.map { it ->
                var isRich = false
                LogPanel.LogLine(
                    "User: ${it.input.transcript.text}\n" +
                    "Bot: ${
                        it.responseItems.map { item ->
                            when {
                                item.text != null -> {
                                    BotItem("text", item.text!!)
                                }
                                item.image != null -> {
                                    isRich = true
                                    BotItem("image", "[Image ${logPanel.convertToLink(item.image!!)}]")
                                }
                                item.audio != null -> {
                                    isRich = true
                                    BotItem("audio", "[Audio ${logPanel.convertToLink(item.audio!!)}]")
                                }
                                else -> {
                                    BotItem("text", "")
                                }
                            }
                        }.joinToString(" ") { botItem ->
                            if (botItem.type == "text" && isRich) botItem.text.replace(
                                "<",
                                "&lt;"
                            ) else botItem.text
                        }
                    }",
                    isRich)
            })

    private fun LogPanel.transformToRunLog(turns: List<Turn>) {
        val logOutput = mutableListOf<LogPanel.LogLine>()
        turns.forEach {
            logOutput.add(LogPanel.LogLine("> ${it.input.transcript.text}", false))
            it.responseItems.forEach { item ->
                if (item.text != null) {
                    if (item.text!!.isNotEmpty()) {
                        logOutput.add(LogPanel.LogLine("< ${item.text}", false))
                    }
                }
                if (item.image != null) {
                    logOutput.add(LogPanel.LogLine("{Image ${convertToLink(item.image!!)}}", true, "secondary"))
                }
            }
            logOutput.addAll(it.log.map { logEntry -> LogPanel.convertToClickable(logEntry, "info") })
        }
        log(logOutput)
    }

    private fun LogPanel.transformToRunLog(logEntries: List<LogEntry>) {
        val logOutput = mutableListOf<LogPanel.LogLine>()
        logOutput.addAll(logEntries.map { logEntry -> LogPanel.convertToClickable(logEntry, "info") })
        log(logOutput)
    }

    fun getAudio(session: Session) {
        mainScope.launch {
            restClient.call<SearchResult>(
                    "$fileUrl/session/${session.sessionId}.wav/_object"
            ).then {
                addAudio(session, "wav")
            }.catch {
                restClient.call<SearchResult>(
                        "$fileUrl/session/${session.sessionId}.mp3/_object"
                ).then {
                    addAudio(session, "mp3")
                }.catch {
                    console.log(it.message)
                }
            }
        }

    }

    private fun addAudio(session: Session, extension: String) {
        panel.add(
            tabulator(listOf(session),
                options = TabulatorOptions(
                    layout = Layout.FITCOLUMNS,
                    columns = listOf(
                        ColumnDefinition(tr("Audio"), "sessionId", formatterFunction = { cell, _, _ ->
                            cell.getValue().let {
                                "<audio controls style=\"width:100%;\" preload=\"auto\">\n" +
                                        "    <source src=\"$fileUrl/session/${session.sessionId}.${extension}\" type=\"audio/${extension}\" preload=\"none\">\n" +
                                        "    Your browser does not support the audio element.\n" +
                                        "</audio> "
                            }
                        })
                    )
                ), types = setOf(TableType.BORDERED, TableType.STRIPED)
            )
        )
    }

    fun getWrapper(): JQuery? {
        return this.getElementJQuery()?.parent()?.parent()
    }

    fun registerScrollEvent(): JQuery? {
        val elem = getWrapper()
        return elem?.on("scroll") { _, _ ->
            val scrollTop = elem.scrollTop()
            lastScrollTop = scrollTop.toFloat()
            Unit
        }
    }

    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        registerScrollEvent()
        mainScope.launch {
            delay(300)
            getWrapper()?.scrollTop(lastScrollTop)
        }
    }

    override fun afterDestroy() {
        super.afterDestroy()
        getWrapper()?.off("scroll")
    }
}
