package ai.flowstorm.studio.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.ui.UiState
import ai.flowstorm.studio.ui.design.dialogue.panel.RunPanel
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.model.AbstractEntity
import io.kvision.core.Widget
import io.kvision.i18n.I18n.gettext
import io.kvision.toolbar.ButtonGroup
import io.kvision.toolbar.buttonGroup
import io.kvision.toolbar.toolbar
import io.kvision.utils.perc
import kotlinx.browser.window
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.Hints.hint
import io.kvision.i18n.tr
import io.kvision.panel.*
import io.kvision.jquery.*
import io.kvision.jquery.jQuery

abstract class RunnableModelEditor<E : AbstractEntity, MC : Widget>(val modelDesigner: ModelDesigner<E>? = null, classes: Set<String> = setOf()) :
        ModelEditor<E>(classes), RunnableComponent, ShortcutProvider {

    lateinit var mainPanel: Widget
    lateinit var editorPanel: SplitPanel
    abstract val mainComponent: MC
    abstract val mainButtonGroup: ButtonGroup
    private var mainToolbar: SimplePanel? = null
    private val mainViewButton = ToolbarButton(Icon.Expand, gettext("Toggle Edit View"), "1") {
        hint("ToggleEditViewButton", Icon.Expand, index = 200)
        onClick {
            toggleMainPanelView()
        }
    }
    abstract val runPanel: RunPanel
    open val mainDefaultHeight get() = (window.innerHeight / 5 * 3)
    var mainViewMaximized = false
    var runViewMaximized = false

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            saveButton.disable(value)
            super.readOnly = value
        }

    override val shortcuts: List<Shortcut> = listOf(
            Shortcut(tr("Toggle Run View"), "0") { toggleRunPanelView() },
            Shortcut(tr("Toggle Main View"), "1") { toggleMainPanelView() },
    )
        get() =  field + runPanel.shortcuts

    private val saveButton = ToolbarButton(Icon.Save, gettext("Save Model"), "Ctrl+s") {
        hint("SaveModelButton", Icon.Save, index = 204)
    }.onClick {
        mainScope.launch { save() }
    }

    fun addPanels(addMainToolbar: Boolean = true) {
        editorPanel = splitPanel(direction = Direction.HORIZONTAL, classes = setOf("content-panel", "model-editor")) {
            mainPanel = simplePanel(classes = setOf("main-panel")) {
                width = 100.perc
                if (addMainToolbar) {
                    mainToolbar = simplePanel(classes = setOf("panel-view", "panel-toolbar")) {
                        // width = NAV_HEIGHT.px
                        toolbar(vertical = true) {
                            buttonGroup {
                                add(mainViewButton)
                            }
                            add(mainButtonGroup.apply { vertical = this@toolbar.vertical })
                            if (modelDesigner != null) {
                                buttonGroup {
                                    add(saveButton)
                                    add(ToolbarButton(Icon.Plus, gettext("New Version")/*, "Ctrl+N"*/) {
                                        hint("NewVersionButton", Icon.Plus, index = 205)
                                    }.onClick {
                                        mainScope.launch { openAsNew() }
                                    })
                                    add(ToolbarButton(Icon.Open, gettext("Open Model"), "o") {
                                        hint("OpenModelButton", Icon.Open, index = 206)
                                    }.onClick {
                                        mainScope.launch { modelDesigner.open() }
                                    })
                                    add(ToolbarButton(Icon.New, gettext("New Model"), "n") {
                                        hint("NewModelButton", Icon.New, index = 207)
                                    }.onClick {
                                        mainScope.launch { modelDesigner.new() }
                                    })
                                    add(ToolbarButton(Icon.Import, gettext("Import Model"), "I", true) {
                                        hint("ImportModelButton", Icon.Import, index = 208)
                                    })
                                    add(ToolbarButton(Icon.Export, gettext("Export Model"), "E", true) {
                                        hint("ExportModelButton", Icon.Export, index = 209)
                                    })
                                }
                            }
                        }
                    }
                }
                add(mainComponent)
            }
            add(runPanel)
        }
    }

    fun toggleMainPanelView() {
        runViewMaximized = false
        runPanel.runViewButton.active = false
        mainViewMaximized = !mainViewMaximized
        mainViewButton.active = mainViewMaximized
        resetMainPanel()

        if ( mainViewMaximized ) {
            AppState.dispatchLaunch(UiState.Action.ToggleView(editView = "on", runView = "off"))
        } else {
            AppState.dispatchLaunch(UiState.Action.ToggleView(editView = "off"))
        }
        
        // val navbar = AppPanel.instance.navbar
        // updateMainPanelHeight(if (mainViewMaximized) {
        //     if (navbar.visible)
        //         navbar.hide()
        //     window.innerHeight - NAV_HEIGHT * 3 - 2
        // } else {
        //     if (!navbar.visible)
        //         navbar.show()
        //     mainDefaultHeight
        // })
    }

    override fun toggleRunPanelView() {
        mainViewMaximized = false
        mainViewButton.active = false
        runViewMaximized = !runViewMaximized
        runPanel.runViewButton.active = runViewMaximized
        resetMainPanel()

        if ( runViewMaximized ) {
            AppState.dispatchLaunch(UiState.Action.ToggleView(editView = "off", runView = "on"))
        } else {
            AppState.dispatchLaunch(UiState.Action.ToggleView(runView = "off"))
        }

        // updateMainPanelHeight(if (runViewMaximized) {
        //     AppPanel.instance.navbar.hide()
        //     0
        // } else {
        //     AppPanel.instance.navbar.show()
        //     mainDefaultHeight
        // })
    }

    fun resetMainPanel() {
        jQuery(".main-panel").css("height", "");
    }
}
