package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.model.DialogueMixin
import ai.flowstorm.common.ui.*
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

class DialogueMixinTable : EntityTable<DialogueMixin>(
            "Mixin",
        DialogueMixinResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), DialogueMixin::label.name, width = "45%"),
                        ColumnDefinition(tr("Type"), DialogueMixin::type.name, width = "25%"),
                        ColumnDefinition(tr("Language"), DialogueMixin::language.name, width = "30%")
                )
        ),
        { DialogueMixinEditor() },
        { DialogueMixin(space_id = AppState.state.spaceId!!) },
        splitMode = true,
        tabWidth = 480.px,
        className = "content-panel",
        searchEnabled = true,
)
