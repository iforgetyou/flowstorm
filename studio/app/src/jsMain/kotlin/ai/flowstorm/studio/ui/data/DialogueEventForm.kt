package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.DialogueEvent
import ai.flowstorm.studio.resources.space.DialogueEventResource
import ai.flowstorm.studio.ui.form.DialogueId
import ai.flowstorm.studio.ui.form.SessionRef
import ai.flowstorm.studio.ui.form.UserRef
import ai.flowstorm.common.ui.EntityForm
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.i18n.tr

class DialogueEventForm(model: DialogueEventResource) : EntityForm<DialogueEvent>(model) {

    private val dialogueId = DialogueId(label = tr("Dialogue"))
    init {
        add(
                DialogueEvent::stringTime,
                Text(label = tr("Time"))
        )
        add(
                DialogueEvent::stringType,
                Text(label = tr("Event Type"))
        )
        add(
                DialogueEvent::sessionId,
                SessionRef()
        )
        add(
                DialogueEvent::user,
                UserRef()
        )
        add(
                DialogueEvent::applicationName,
                Text(label = tr("Application"))
        )
        add(
                DialogueEvent::dialogue_id,
                dialogueId
        )
        add(
                DialogueEvent::text,
                TextArea(label = tr("Text"), rows = 6)
        )
    }

    override fun load(entity: DialogueEvent): DialogueEvent {
        disableAll()
        dialogueId.nodeId = entity.nodeId
        return super.load(entity)
    }
}