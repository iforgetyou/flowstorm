package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.ui.RunnableComponent
import ai.flowstorm.core.ClientAttributes
import io.kvision.core.Color
import io.kvision.panel.SimplePanel

abstract class BotContainerPanel(classes: Set<String> = setOf()): SimplePanel(classes = classes) {

    val colorMap = mapOf(
            "SLEEPING" to Color.hex("c0c0d0".toInt(16)),
            "LISTENING" to Color.hex("00ff00".toInt(16)),
            "PROCESSING" to Color.hex("4040ff".toInt(16)),
            "RESPONDING" to Color.hex("c0c0d0".toInt(16)),
            "PAUSED" to Color.hex("4040ff".toInt(16))
    )
    var currentImage = ""

    abstract val runConsumer: RunnableComponent
    abstract val runPanel: RunPanel
    abstract val focusNodes: Boolean
    abstract val canReloadPanels: Boolean
    abstract fun getVoice(): String
    abstract fun getAttributes(): ClientAttributes
    abstract fun focusTab()
    abstract fun log(logLines: List<LogPanel.LogLine>)
    abstract fun logSignal(signal: String)
    abstract fun setCurrentImage(image: String)
    abstract fun setVideo(video: String, callback: dynamic)
    abstract fun setBackground(background: String)
    abstract fun setInputAudio(setOn: Boolean)
    fun log(logLine: LogPanel.LogLine) {
        log(listOf(logLine))
    }
}