package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.model.Enum
import ai.flowstorm.core.model.SttConfig
import ai.flowstorm.studio.model.SpeechRecognizer
import ai.flowstorm.studio.resources.space.SpeechRecognizerResource
import ai.flowstorm.common.ui.*
import io.kvision.form.range.Range
import io.kvision.form.select.Select
import io.kvision.form.spinner.Spinner
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.html.*
import io.kvision.i18n.tr

class SpeechRecognizerForm : EntityForm<SpeechRecognizer>(SpeechRecognizerResource) {

    val configForm = object : Form<SttConfig>(SttConfig.serializer()) {
        init {
            add(
                SttConfig::provider,
                Select(label = tr("Provider"), options = UiHelpers.options(Enum.sttProviders))
            )
            add(
                SttConfig::maxSilence,
                Spinner(min = 1, max = 60, step = 1, label = tr("Max. silence"))
            )
            add(
                SttConfig::maxPause,
                Range(min = 1, max = 5, step = 1, label = tr("Max. pause"))
            )
        }
    }

    init {
        add(
            SpeechRecognizer::name,
            Text(label = tr("Name")),
            required = true,
            requiredMessage = tr("Required field")
        )
        add(
            SpeechRecognizer::description,
            TextArea(label = tr("Description"))
        )
        tag(TAG.H3, tr("Speech-To-Text Configuration"))
        add(configForm)
    }

    override fun load(entity: SpeechRecognizer) = super.load(entity).also {
        configForm.setData(it.sttConfig)
    }

    override suspend fun save() {
        configForm.save(entity!!.sttConfig)
        super.save()
    }
}
