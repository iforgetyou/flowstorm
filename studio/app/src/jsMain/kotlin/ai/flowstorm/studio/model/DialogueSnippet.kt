package ai.flowstorm.studio.model

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.Cloneable
import ai.flowstorm.common.id.newId
import kotlinx.serialization.*

@Serializable
data class DialogueSnippet(
    override val _id: Id = newId(),
    var name: String = "",
    var description: String = "",
    var language: String = "en",
    var graph: Graph = Graph(),
    var space_id: Id = AppState.state.spaceId!!
) : AbstractEntity(), Cloneable<DialogueSnippet> {
    override val label
        get() = SpaceResource.get(space_id).let {
            "[${it.name}]$name"
        }

    override fun clone() = Cloneable.clone(this)
    fun cloneAsNew() = clone().copy(_id = newId())

    object Factory {
        fun empty() = DialogueSnippet()
    }
}
