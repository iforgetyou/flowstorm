package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.resources.space.ProfileResource
import ai.flowstorm.common.auth.UserService
import kotlinx.coroutines.launch
import kotlinx.serialization.json.JsonElement

class ProfilePanel : JsonPanel<JsonElement>(JsonElement.serializer()) {

    override fun reload() {
        if (!lock) {
            mainScope.launch {
                setData(ProfileResource.getByUserId(UserService.context.user._id).attributes)
            }
        }
    }
}
