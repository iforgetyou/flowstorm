package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.resources.space.DialogueSourceResource
import ai.flowstorm.studio.ui.RunnableModelEditor
import ai.flowstorm.studio.ui.design.dialogue.*
import ai.flowstorm.studio.ui.design.dialogue.panel.*
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.ui.design.dialogue.graph.NodeEditor
import ai.flowstorm.common.BotLogic
import ai.flowstorm.common.ErrorHandler
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.Toast
import ai.flowstorm.common.FeatureSwitch
import ai.flowstorm.common.FeatureSwitch.Feature
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Icon
import io.kvision.core.*
import io.kvision.i18n.I18n.gettext
import io.kvision.panel.*
import io.kvision.i18n.I18n.tr
import io.kvision.toolbar.ButtonGroup
import kotlinx.coroutines.*
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.Hints.hint
import org.w3c.dom.Audio

class DialogueSourceEditor(dialogueSourceDesigner: DialogueSourceDesigner) :
    RunnableModelEditor<DialogueSource, TabPanel>(dialogueSourceDesigner, classes = setOf("nav-tabs-content")), ShortcutProvider,
    Stateful<DialogueSourceEditor.State> by StatefulValue(State()) {

    data class State(
        // TODO move more properties here
        val language: String = "en"
    )

    override var entity: DialogueSource? = DialogueSource.Factory.empty()
    override val key get() = entity?._id?.let { "@${it.value}" }
    override val voice: String? = null

    private val buildSuccess = Audio("https://core.flowstorm.ai/file/assets/spaces/5de915aaffc8631a0b2d6fd4/5ffef500a12dcf5ff6b73549")
    private val buildFail = Audio("https://core.flowstorm.ai/file/assets/spaces/5de915aaffc8631a0b2d6fd4/5ffef4ddf8cefe048813b443")

    override val name: String
        get() = entity!!.label

    override val shortcuts: List<Shortcut> = listOf(
            Shortcut(tr("Build"), "b", ctrlKey = true) { resolveModelBuilding() },
            Shortcut(tr("Edit init code"), "C") { activateInitCodeTab() },
            Shortcut(tr("Open graph editor"), "G") { activateGraphEditorTab() },
            Shortcut(tr("Toggle edit mode"), "E") { toggleEditMode() },
            Shortcut(tr("Edit description"), "D") { activateDescriptionTab() },
    )
        get() = field + super.shortcuts + ((mainComponent.activeTab?.component as? ShortcutProvider)?.shortcuts?: listOf())

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            listOf<ReadOnly>(graphEditor, initCodeEditor, descriptionEditor, propertiesEditor).forEach { it.readOnly = value }
        }
    private val buildLogPanel = LogPanel()
    override val runPanel = RunPanel(this) {
        tab(tr("Build log"), buildLogPanel, Icon.Build.def) {
            hint("BuildTab", Icon.Build, Placement.TOP, index = 226)
        }
    }
    val graphEditor = GraphEditor().apply {
        newState { copy(language = entity?.language ?: language) }
    }
    //TODO change voice when property changed
    private val initCodeEditor = ModelCodeEditor().apply {
        onEvent { change = {entity?.initCode = self.value} }
    }

    private val descriptionEditor = ModelCodeEditor("markdown").apply {
        onEvent { change = {entity?.description  = self.value} }
    }
    private val sourceEditor = JsonEditor(DialogueSource.serializer(), classes = setOf("panel-view-nav1"))
    private val propertiesEditor = DialoguePropertiesEditor(initCodeEditor).also {
        it.form.onEvent {
            change = {
                newState { copy(language = entity?.language ?: language) }
            }
        }
    }
    private val workflow = Workflow(this)
    private val xmlEditor = DialogueXmlEditor(this)
    private val reloadButton: ToolbarButton = ToolbarButton(Icon.Reload, gettext("Reload"), "R") {
        hint("ReloadButton", Icon.Reload, index = 201)
        onClick {
            entity?.id?.let { id ->
                mainScope.launch {
                    if (ConfirmModal(
                            tr("Reload"),
                            gettext("Reloading the dialogue will cause you to lose current changes. Do you really want to continue?")
                        ).open()) {
                        val dialogueSource = DialogueSourceResource.load(id, false)
                        load(dialogueSource)
                        Toast.success(gettext("DialogueSource reloaded"))
                    }
                }
            }
        }
    }
    private val editModeButton: ToolbarButton = ToolbarButton(Icon.Edit, gettext("Edit Model")).apply {
        disabled = true
        active = false
        hint("EditModeButton", Icon.Edit, index = 202)
        onClick {
            toggleEditMode()
        }
    }

    private fun toggleEditMode() {
        mainScope.launch {
            if (entity!!.isEditable) {
                when (entity!!.lock) {
                    null -> lock()
                    UserService.context.user.username -> unlock()
                }
            }
        }
    }

    private fun updateEditModeButton() {
        with(editModeButton) {
            when {
                !entity!!.isEditable -> {
                    disabled = true
                    active = false
                }
                entity!!.lock == null -> {
                    disabled = false
                    active = false
                }
                entity!!.lock == UserService.context.user.username -> {
                    disabled = false
                    active = true
                }
                else -> {
                    disabled = true
                    active = false
                }
            }
            graphEditor.readOnly = !active
            graphEditor.diagramPanel.disableToolbarButtons()
        }
        workflow.updateUI()
    }

    private val buildModelButton: ToolbarButton = ToolbarButton(Icon.Build, gettext("Build Model"), "Ctrl+b").apply {
        disable(true)
        active = false
        hint("BuildModelButton", Icon.Build, index = 203)
        onClick {
            resolveModelBuilding()
        }
    }

    private fun resolveModelBuilding() {
        updateBuildModelButton()
        if (buildModelButton.disabled) return

        mainScope.launch { build() }
    }

    private fun updateBuildModelButton() {
        with(buildModelButton) {
            when {
                // disable btn in case entity currently locked(/editing) by another user
                !entity!!.isNew && entity!!.lock != null && !entity!!.isLockedByCurrentUser -> {
                    disable(true)
                }
                else -> {
                    disable(false)
                }
            }
        }
    }

    override val mainButtonGroup = ButtonGroup {
        add(editModeButton)
        add(buildModelButton)
        add(reloadButton)
    }

    private val graphEditorTab = Tab(tr("Graph"), graphEditor, Icon.Graph.def) {
        hint("DialogueGraphEditorTab", Icon.Graph, Placement.BOTTOM, index = 210)
    }
    private val initCodeEditorTab = Tab(tr("Code"), initCodeEditor, Icon.Function.def) {
        hint("DialogueInitCodeEditorTab", Icon.Function, Placement.BOTTOM, index = 211)
    }
    private val propertiesTab = Tab(tr("Properties"), propertiesEditor, Icon.Properties.def) {
        hint("DialoguePropertiesEditorTab", Icon.Properties, Placement.BOTTOM, index = 212)
    }
    private val descriptionTab = Tab(tr("Description"), descriptionEditor, Icon.Description.def) {
        hint("DialogueDescriptionEditorTab", Icon.Description, Placement.BOTTOM, index = 213)
    }
    private val sourceTab = Tab(tr("Source"), sourceEditor, Icon.Source.def) {
        hint("DialogueSourceEditorTab", Icon.Source, Placement.BOTTOM, index = 214)
    }
    private val workflowTab = Tab(tr("Workflow"), workflow, Icon.Workflow.def) {
        hint("DialogueWorkflowTab", Icon.Workflow, Placement.BOTTOM, index = 215)
    }
    private val xmlEditorTab = Tab(tr("XML Editor"), xmlEditor, Icon.Xml.def)

    override val mainComponent = TabPanel(classes = setOf("panel-view"), scrollableTabs = true) {
        // height = mainDefaultHeight.px
        addAll(listOf(graphEditorTab, initCodeEditorTab, propertiesTab, descriptionTab, sourceTab))
        if (!AuthService.context.isAnonymous) {
            add(workflowTab)
            if (FeatureSwitch.isEnabled(Feature.xmlEditor)) add(xmlEditorTab)
        }
    }

    override var savedVersion = DialogueSource.Factory.empty()

    init {
        addPanels()
        editorPanel.onEvent {
            dragEndSplitPanel = {
                graphEditor.updateView()
            }
        }
        subscribe {
            graphEditor.newState { copy(language = it.language) }
        }
    }

    override suspend fun close() {
        super.close()
        if (BotLogic::botPanel.isInitialized && BotLogic.runConsumer == this)
            BotLogic.stop()
        if (entity!!.isLockedByCurrentUser)
            unlock()
    }

    fun activateInitCodeTab() = activateTab(initCodeEditorTab)
    fun activateDescriptionTab() = activateTab(descriptionTab)
    fun activateGraphEditorTab() = activateTab(graphEditorTab)

    private fun activateTab(tab: Tab) {
        require(mainComponent.getTabs().any { it == tab }) { "Tab is not in main panel." }
        mainComponent.activeTab = tab
        tab.component.also {
            when (it) {
                is CodeEditor -> it.focus()
            }
        }
    }


    override fun load(entity: DialogueSource): DialogueSource {
        return load(entity,false)
    }

    fun load(entity: DialogueSource, keepView: Boolean): DialogueSource {
        super.load(entity)
        readOnly = !((entity.lock == UserService.context.user.username || entity.isNew) && entity.isEditable)

        modelState.value = when {
            entity.lastBuild != null -> StatefulModel.State.Built
            !entity.isNew -> StatefulModel.State.Saved
            else -> StatefulModel.State.New
        }
        newState { copy(language = entity.language) }

        graphEditor.load(entity.graph, isNew = entity.isNew, entity.id)
        propertiesEditor.load(entity)
        sourceEditor.setData(entity)
        initCodeEditor.set(entity.initCode, if (entity.dialogueName.isEmpty()) listOf() else entity.mixins_id)
        descriptionEditor.set(entity.description)
        workflow.load(entity)
        updateEditModeButton()
        updateBuildModelButton()
        if(!keepView) {
            mainScope.launch {
                delay(500)
                graphEditor.fitToViewport()
            }
        }
        return entity
    }

    private fun buildFeedback(verb: String, noun: String, sound: Audio, toastFun:  (String) -> Unit) {
        toastFun("<h3>${gettext("Dialogue build $verb for dialogue ")}<b>${entity!!.dialogueName}</b><h3>")
        buildLogPanel.log(gettext("BUILD $noun"))
        sound.play()
    }

    suspend fun build() = coroutineScope {
        with(runPanel) {
            runPanel.switchToTab(buildLogPanel)
            buildLogPanel.log(gettext("Saving dialogue..."))
            save()
            buildLogPanel.log(gettext("Building dialogue..."))
            try {
                val build = DialogueSourceResource.build(entity!!)
                buildLogPanel.log(build.logs)
                if (build.success) {
                    buildFeedback("succeeded", "SUCCESS", buildSuccess, Toast::success)
                    this@DialogueSourceEditor.modelState.value = StatefulModel.State.Built
                } else {
                    buildFeedback("failed", "FAILED", buildFail, Toast::error)
                }
                build
            } catch (t: Throwable) {
                ErrorHandler.handle(t, false)
                buildLogPanel.log(t.message ?: "Unknown cause")
                buildFeedback("failed", "FAILED", buildFail, Toast::error)
                DialogueSourceResource.Build("Unknown", false)
            }
        }
    }

    suspend fun publish() {
        try {
            DialogueSourceResource.publish(entity!!)
            entity!!.state = DialogueSource.State.Published
            readOnly = true
            entity!!.clearLock()
            savedVersion = entity!!.clone()
            updateEditModeButton()
            updateBuildModelButton()
        } catch (t: Throwable) {
            ErrorHandler.handle(t)
        }
    }

    suspend fun archive() {
        try {
            DialogueSourceResource.archive(entity!!)
            entity!!.state = DialogueSource.State.Archived
            readOnly = true
            entity!!.clearLock()
            savedVersion = entity!!.clone()
            updateEditModeButton()
            updateBuildModelButton()
        } catch (t: Throwable) {
            ErrorHandler.handle(t)
        }
    }

    suspend fun lock() {
        try {
            if (!entity!!.isNew) DialogueSourceResource.lock(entity!!)
            entity!!.setLock()
            savedVersion.lock = entity!!.lock
            savedVersion.lockTime = entity!!.lockTime
            entity?.let { dialogue ->
                if(!dialogue.isNew) {
                    reloadNodeEditor(load(DialogueSourceResource.load(dialogue.id, useCache = false), keepView = true))
                }
            }
        } catch (t: Throwable) {
            ErrorHandler.handle(t)
            return
        }
        readOnly = false
        updateEditModeButton()
        updateBuildModelButton()
    }

    suspend fun unlock(force: Boolean = false) {
        if(modelState.value == StatefulModel.State.Changed) {
            if(ConfirmModal(tr("Unsaved changes"), tr("Are you sure to leave edit mode? Your unsaved changes will be lost when you enter edit mode again.")).open() == false) {
                return
            }
        }

        if (!entity!!.isNew) DialogueSourceResource.unlock(entity!!, force)
        entity!!.clearLock()
        savedVersion.clearLock()
        readOnly = true
        updateEditModeButton()
        updateBuildModelButton()
    }

    override suspend fun save() {
        if (readOnly) return //do nothing - link/buttons should be disabled, but save shortcut is still active

        var dialogue = entity!!.clone()
        if (!DialogueName.isValidName(dialogue.dialogueName)) {
            val data = DialogueNameModal(tr("Save As"), tr("Save"), DialogueNameModal.FormData(dialogue.space_id, dialogue.dialogueName)).apply {
                onEvent {
                    hideBsModal = {
                        if (BotLogic.state.runningStatus == BotLogic.Status.Building) {
                            BotLogic.newState { copy(runningStatus = BotLogic.Status.Stopped) }
                        }
                    }
                }
            }.open()
            dialogue.dialogueName = data.name
            dialogue.space_id = data.spaceId
        }
        try {
            dialogue = with (DialogueSourceResource) {
                if (dialogue.isNew) create(dialogue) else update(dialogue)
            }
            load(dialogue)
        } catch (t: Throwable) {
            ErrorHandler.handle(t)
            return
        }
        // reload node in graph right panel editor
        reloadNodeEditor(dialogue)

        savedVersion = dialogue.clone()
        modelState.value = StatefulModel.State.Saved
    }

    override suspend fun saveAs() {
        var dialogue = entity!!.copy(_id = newId()).clone()
        dialogue.clearLock()
        val data = DialogueNameModal(tr("Save As"), tr("Save"), DialogueNameModal.FormData(dialogue.space_id, dialogue.dialogueName)).open()
        dialogue.dialogueName = data.name
        dialogue.space_id = data.spaceId

        dialogue.state = DialogueSource.State.Draft
        dialogue = DialogueSourceResource.create(dialogue)

        // dialogue model will be loaded into a new tab
        modelDesigner!!.load(dialogue)
    }

    override suspend fun openAsNew() {
        modelDesigner!!.load(entity!!.cloneAsNew())
    }

    private fun reloadNodeEditor(entity: DialogueSource) {
        (graphEditor.rightPanel.activeChild as? NodeEditor<*>)?.apply {
            entity.graph.nodes.firstOrNull { it.id == node?.id }?.let {
                load(it)
            }
        }
    }
}
