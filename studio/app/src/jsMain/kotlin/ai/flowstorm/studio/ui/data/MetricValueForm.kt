package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.analytics.MetricValue
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class MetricValueForm(model: EntityResource<MetricValue>): EntityForm<MetricValue>(model) {
    init{
        add(MetricValue::namespace, Text(label = tr("Namespace")))
        add(MetricValue::name, Text(label = tr("Name")))
        add(MetricValue::stringValue, Text(label = tr("Value")))
    }
}