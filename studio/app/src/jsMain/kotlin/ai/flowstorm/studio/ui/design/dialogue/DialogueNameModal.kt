package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.studio.resources.space.DialogueSourceResource
import kotlinx.coroutines.*
import kotlinx.serialization.Serializable
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.idPair
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.form
import ai.flowstorm.common.ui.form.SelectId
import ai.flowstorm.studio.ui.RootSpaceFirstComparator
import io.kvision.core.Color
import io.kvision.core.onEvent
import io.kvision.form.text.Text
import io.kvision.html.*
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.simplePanel
import io.kvision.state.bind
import io.kvision.utils.px

class DialogueNameModal(caption: String, submitLabel: String, val data: FormData) :
    Modal(caption, size = ModalSize.LARGE), Stateful<DialogueNameModal.State> by StatefulValue(State()) {

    private val mainScope = MainScope()
    private val saveClick = Job()
    private var validationJob: Job = Job()
    private var form: Form<FormData>

    data class State(
        val validating: Boolean = false,
        val validationError: String = "",
    ) {
        val valid get() = validationError.isEmpty()
    }

    @Serializable
    data class FormData(val spaceId: Id, val name: String)

    init {
        closeIcon.onEvent {
            click = { cancel() }
        }

        form = form(FormData.serializer()) {
            add(FormData::spaceId,
                SelectId(
                    label = tr("Space"),
                    options = UserService.context.getRoles(Space.Role.Editor).sortedWith(RootSpaceFirstComparator.thenBy { it.name })
                        .map { SpaceResource.get(it.spaceId).idPair() }).apply {
                    onEvent {
                        change = {
                            this@DialogueNameModal.validate()
                        }
                    }
                },
                required = true,
                requiredMessage = tr("Required field")
            )
            add(FormData::name, Text(label = tr("Model name")).apply {
                onEvent {
                    keyup = {
                        this@DialogueNameModal.validate()
                    }
                }
            }, required = true, requiredMessage = tr("Required field"))

            onEvent {
                submit = {
                    it.preventDefault()
                }
            }
        }

        simplePanel(classes = setOf("container-fluid")) {
            simplePanel(classes = setOf("row")) {
                height = 38.px
                simplePanel(classes = setOf("col-sm")) {
                    span(this@DialogueNameModal) { s ->
                        when {
                            s.validating -> {
                                content = tr("Validating...")
                                color = Color("black")
                            }
                            s.valid -> {
                                content = ""
                                color = Color("black")
                            }
                            else -> {
                                content = s.validationError
                                color = Color("red")
                            }
                        }
                    }
                }
            }
        }

        addButton(Button(submitLabel, type = ButtonType.SUBMIT).apply {
            onClick { saveClick.complete() }
            bind(this@DialogueNameModal) { s ->
                disabled = s.validating || !s.valid
            }
        })
        addButton(Button(gettext("Cancel"), style = ButtonStyle.SECONDARY).apply {
            onClick { cancel() }
        })
    }

    private fun validate(formData: FormData = getData()) {
        newState { copy(validating = true) }
        console.log("VAL")
        validationJob.cancel()

        validationJob = mainScope.launch {
            val error: String = when {
                formData.name.isEmpty()-> {
                    tr("Dialogue name is required.")
                }
                !DialogueName.isValidName(formData.name) -> {
                    tr("Dialogue name does not conform to naming convention (product-name/dialogue-name).")
                }
                formData != data -> {
                    delay(2000)
                    if (DialogueSourceResource.nameExists(formData.spaceId, formData.name)) {
                        tr("Name already exists!")
                    } else ""
                }
                else -> ""
            }

            newState { copy(validating = false, validationError = error) }
        }
    }

    suspend fun open(): FormData {
        form.setData(data)
        show()
        validate(data)
        saveClick.join()
        hide()
        return getData()
    }

    fun cancel() {
        hide()
        mainScope.cancel()
    }

    /**
     * Workaround: form.getData() fails on invalid type cast exception
     */
    private fun getData(): FormData = FormData(form[FormData::spaceId] as Id, form[FormData::name] as String)
}
