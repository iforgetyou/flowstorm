package ai.flowstorm.studio.state

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.state.RootState.Action.LoadLocalState
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.state.localStorage
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

object LocalStorageStateMapper {

    private val scope = MainScope()

    private val localState = LocalState()

    private class LocalState {
        var spaceId by localStorage<Id>()
        var theme by localStorage<String>()
        var viewMode by localStorage<String>()
        var editView by localStorage<String>()
        var runView by localStorage<String>()
    }

    fun init() {
        scope.launch {
            with(AppState) {
                //load state from localstorage
                dispatch(with(localState) { LoadLocalState(spaceId, theme, viewMode, editView, runView) })
                //save state to local storage
                collect { state ->
                    with(localState) {
                        spaceId = state.spaceId
                        theme = state.ui.theme
                        viewMode = state.ui.viewMode
                        editView = state.ui.editView
                        runView = state.ui.runView

                    }
                }
            }
        }
    }
}
