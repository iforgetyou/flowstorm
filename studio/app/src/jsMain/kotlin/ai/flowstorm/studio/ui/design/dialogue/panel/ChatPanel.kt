package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import com.github.snabbdom.VNode
import ai.flowstorm.studio.ui.RunnableComponent
import ai.flowstorm.botui.BotUI
import kotlinext.js.jsObject
import ai.flowstorm.common.BotLogic
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.core.ClientAttributes
import io.kvision.jquery.*
import io.kvision.state.sub
import io.kvision.utils.JSON.toObj
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class ChatPanel(panelId: String, appKey: String): BotContainerPanel(classes = setOf("wallpaper")) {

    data class HistoryItem(
            val type: String,
            val direction: String= "sent",
            val message: String = "",
            val imageUrl: String = "",
    )

    private var botUI: dynamic = undefined
    var bot: dynamic = null
    var botPanel: BotContainerPanel = this
    private val history: MutableList<HistoryItem> = mutableListOf()
    private var controlsDisabled = false
    private var currentBackground = "https://repository.flowstorm.ai/images/bgrnd.jpg"
    var autoStart = false
    private val iconMap: Map<String, String> = mapOf(
            "play" to "play",
            "barge" to "microphone",
            "mic" to "microphone",
            "stop" to "stop",
            "volume-mute" to "volume-mute",
            "menu" to "ellipsis-h",
            "blocked" to "microphone-slash",
    )

    class DummyRunnable(appKey: String): RunnableComponent {
        override val key = appKey
        override val voice: String = ""

        override fun toggleRunPanelView() {
        }

    }

    init {
        AppState.sub { it.coreUrl }.onEach {
            bot = BotLogic.createBot()
        }.launchIn(App.scope)
        AuthService.sub { it.isAuthenticated }.subscribe {
            if (it)
                bot = BotLogic.createBot()
        }
        id = panelId
    }

    override val runConsumer: RunnableComponent = DummyRunnable(appKey)
    override val runPanel: RunPanel
        get() = TODO("Not yet implemented")
    override var focusNodes: Boolean = false
    override var canReloadPanels: Boolean = false

    private fun showHistoryItem(item: HistoryItem) {
        when(item.type){
            "image" -> {
                botUI.setImage(item.imageUrl)
            }
            "message" -> {
                if (item.direction == "sent") {
                    botUI.setUserText(item.message)
                } else {
                    botUI.setBotText(item.message)
                }
            }
        }
    }


    override fun getVoice(): String {
        return ""
    }

    override fun getAttributes(): ClientAttributes {
        return ClientAttributes().apply {
            set("page", AssistantPanel.route)
            set("autoStart", autoStart)
        }
    }

    override fun focusTab() {

    }

    override fun logSignal(signal: String) {

    }

    override fun log(logLines: List<LogPanel.LogLine>) {
        if (logLines.first().type == "dark") {
            logLines.forEach {
                if (it.text.isNotEmpty()) {
                    val x = it.text.split(" ", limit = 2)
                    // Function names are swapped
                    if (x[0] == ">") {
                        history.add(HistoryItem("message", message = x[1]))
                        botUI.setUserText(x[1])
                    } else {
                        history.add(HistoryItem("message", direction = "received", message = x[1]))
                        botUI.setBotText(x[1])
                    }
                }
            }
        }
    }

    override fun setCurrentImage(image: String) {
        if (image.isNotBlank()) {
            history.add(HistoryItem("image", imageUrl = image))
            botUI.setImage(image)
        }
    }

    override fun setVideo(video: String, callback: dynamic) {
        botUI.setVideo(video, callback)
    }

    override fun setBackground(background: String) {
        currentBackground = background
        botUI.setBackgroundImage(background)
    }

    override fun setInputAudio(setOn: Boolean) {
        botUI.setMicrophone(setOn)
    }

    fun setControls(visible: Boolean){
        controlsDisabled = !visible
        if (botUI != undefined && AssistantPanel.tabPanel.activeIndex == 0) {
            botUI.setControls(visible)
        }
    }

    fun launchBot(startMessage: String? = null, autoStart: Boolean = false){
        this.autoStart = autoStart
        BotLogic.bot = bot
        BotLogic.botPanel = botPanel
        BotLogic.run(startMessage)
    }

    private fun swapIcons() {
        for (entry in iconMap) {
            swapIconToFA(entry.key, entry.value)
        }
        if (!BotLogic.state.inputAudio){
            jQuery(".icon--mic").removeClass("fa-microphone")
        }
    }

    private fun swapIconToFA(originalClass: String, faClass: String){
        val element = jQuery(".icon--${originalClass}")
        element.removeClass("icon--content--${originalClass}")
        element.addClass("fas fa-${faClass}")
        element.css("font-size", "1.25rem")
    }

    @Suppress("UNUSED_VARIABLE")
    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        val containerSize = mapOf("width" to "100%", "height" to "100%")
        val options = jsObject<dynamic> {
            guiMode = "chat"
            fullScreen = false
            customIcons = true
            // TODO improve passing of icons
            arrowIcon = "fa-arrow-up"
            micIcon = "fa-microphone"
            playIcon = "fa-play"
            pauseIcon = "fa-pause"
            widgetSize = containerSize.toObj()
            imageAverageColorOpacity = 0.5
            backgroundImageBlur = 0
            animationSpeed = 500
            backgroundAdvancedAnimationParticlesCount = 0
            backgroundSimpleAnimation = false
            inputAudio = BotLogic.state.inputAudio
            outputAudio = BotLogic.state.outputAudio
        }
        val currentId = this.id ?: ""
        botUI = BotUI(currentId, options)
        with(botUI) {
            chatInputCallback = { value: dynamic ->
                    if (BotLogic.state.clientStatus == "SLEEPING"){
                        launchBot(value as String)
                    } else {
                        BotLogic.sendText(value as String)
                    }
            }
            chatMicrophoneCallback = {
                BotLogic.newState { copy(inputAudio = !BotLogic.state.inputAudio) }
                BotLogic.changeAudio("Input")
            }
            chatMuteCallback = {
                BotLogic.newState { copy(outputAudio = !BotLogic.state.outputAudio) }
                BotLogic.changeAudio("Output")
            }
            chatBargeCallback = {
                if (BotLogic.state.clientStatus == "RESPONDING")
                    BotLogic.click()
                else if (BotLogic.state.clientStatus == "SLEEPING") {
                    launchBot("#listen")
                }
            }
            chatPlayCallback = {
                launchBot()
            }
            chatStopCallback = {
                BotLogic.stop()
            }
            chatTextInputElementCallback = {
                if (BotLogic.state.clientStatus == "LISTENING")
                    BotLogic.closeInputAudio()
                else if (BotLogic.state.clientStatus == "RESPONDING") {
                    BotLogic.startTyping()
                }
            }
        }
        botUI.setOrientation("portrait")
        botUI.setBackgroundImage(currentBackground)

        swapIcons()
        jQuery(".icon--blocked").css("right", "27%")

        history.forEach {
            showHistoryItem(it)
        }
        if (controlsDisabled)
            botUI.setControls(false)
        BotLogic.subscribe{
            if (it.clientStatus == "LISTENING") {
                botUI.setOutputAudio(1)
            } else {
                botUI.setInputAudio()
            }
            if (it.runningStatus == BotLogic.Status.Paused || it.runningStatus == BotLogic.Status.Stopped) {
                botUI.setPlayIcon("fa-play")
            } else {
                botUI.setPlayIcon("fa-pause")
            }
            botUI.disableStop(it.runningStatus != BotLogic.Status.Paused && it.runningStatus != BotLogic.Status.Running)
        }
    }
}
