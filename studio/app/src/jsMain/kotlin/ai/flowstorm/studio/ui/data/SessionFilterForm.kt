package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Session
import ai.flowstorm.studio.resources.space.ApplicationResource
import ai.flowstorm.studio.ui.form.UserId
import ai.flowstorm.studio.ui.data.SessionFilterForm.FormData.DataSource
import ai.flowstorm.studio.util.dayEnd
import ai.flowstorm.studio.util.dayStart
import ai.flowstorm.studio.util.toDate
import ai.flowstorm.studio.util.toInstant
import kotlinx.coroutines.launch
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.ui.FilterForm
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.SelectId
import io.kvision.core.onEvent
import io.kvision.form.check.RadioGroup
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.form.time.DateTime
import io.kvision.i18n.tr
import io.kvision.utils.perc
import kotlinx.datetime.Instant
import kotlin.js.Date

class SessionFilterForm : Form<SessionFilterForm.FormData>(serializer = FormData.serializer()), FilterForm {

    @Serializable
    data class FormData(
        val _id: String? = null,
        @Contextual //TODO Replace by LocalDateTime when we upgrade to KVision 5
        var dateFrom: Date? = null,
        @Contextual //TODO Replace by LocalDateTime when we upgrade to KVision 5
        var dateTo: Date? = null,
        var dataSource: String = DataSource.Live.name,
        var userId: Id? = null,
        var applicationId: Id? = null,
        var sessionId: String? = null,
        var clientType: String? = null,
        var sort: String = Session::datetime.name,
        var direction: String = Query.SortDirection.Desc.name,
    ) {
        enum class DataSource { All, Live, Test }
    }

    init {
        padding = UiHelpers.spacing
        width = 97.perc
        add(FormData::_id, Text(label = tr("Object ID")))
        add(FormData::sessionId, Text(label = tr("Session ID")))
        add(FormData::clientType, Text(label = tr("Client type")))
        add(FormData::dateFrom, DateTime(format = "YYYY-MM-DD", label = tr("Date From")).apply {
            onEvent {
                change = { this@SessionFilterForm.validate() }
            }
        })
        add(FormData::dateTo, DateTime(format = "YYYY-MM-DD", label = tr("Date To")).apply {
            onEvent {
                change = { this@SessionFilterForm.validate() }
            }
        })
        add(
            FormData::dataSource, RadioGroup(
                label = tr("Data"),
                options = UiHelpers.enumOptions<DataSource>(),
                inline = true
            )
        )
        add(FormData::userId, UserId(tr("User")))
        add(FormData::applicationId, SelectId(label = tr("Application")).apply {
            mainScope.launch {
                options = UiHelpers.options(ApplicationResource)
            }
        })

        add(
            FormData::sort, Select(
                label = tr("Sort by"),
                options = listOf(Session::datetime.name to tr("Time"), "turnsCount" to tr("Turn count")),
            )
        )

        add(
            FormData::direction, Select(
                label = tr("Sort direction"),
                options = listOf(
                    Query.SortDirection.Asc.name to tr("Ascending"),
                    Query.SortDirection.Desc.name to tr("Descending")
                )
            )
        )

        validator = {
            val from: Date? = it[FormData::dateFrom] as Date?
            val to: Date? = it[FormData::dateTo] as Date?

            if (from != null && to != null) {
                from.getTime() <= to.getTime()
            } else true
        }
        validatorMessage = { "Invalid date interval" }
    }

    override fun load(query: Query) {
        val dateFrom =
            (query.filters.firstOrNull { it.field == Session::datetime.name && it.operator == Query.Operator.gte }?.value as? Instant)?.toDate()
        val dateTo =
            (query.filters.firstOrNull { it.field == Session::datetime.name && it.operator == Query.Operator.lte }?.value as? Instant)?.toDate()
        val userId =
            query.filters.firstOrNull { it.field == "user._id" && it.operator == Query.Operator.eq }?.value as Id?
        val _id =
            query.filters.firstOrNull { it.field == "_id" && it.operator == Query.Operator.eq }?.value as String?
        val sessionId =
            query.filters.firstOrNull { it.field == "sessionId" && it.operator == Query.Operator.eq }?.value as String?
        val clientType =
            query.filters.firstOrNull { it.field == "clientType" && it.operator == Query.Operator.regex }?.let {
                (it.value as String).subSequence(1, it.value.length - 4) as String?
            }
        val applicationId =
            query.filters.firstOrNull { it.field == "application._id" && it.operator == Query.Operator.eq }?.value as Id?

        val dataSource = query.filters.firstOrNull { it.field == "test" && it.operator == Query.Operator.eq }?.let {
            if (it.value == true) DataSource.Test else DataSource.Live
        } ?: DataSource.All

        form.setData(
            FormData(
                _id,
                dateFrom,
                dateTo,
                dataSource.name,
                userId,
                applicationId,
                sessionId,
                clientType,
                query.sort?.field ?: Session::datetime.name,
                query.sort?.direction?.name ?: Query.SortDirection.Desc.name
            )
        )
    }

    override fun save(query: Query) {
        val filter = FormData()
        save(filter)

        val dateFrom = filter.dateFrom?.dayStart()?.toInstant()
        val dateTo = filter.dateTo?.dayEnd()?.toInstant()

        query.filters.clear()
        dateFrom?.let {
            query.filters.add(Query.Filter(Session::datetime.name, Query.Operator.gte, it))
        }
        dateTo?.let {
            query.filters.add(Query.Filter(Session::datetime.name, Query.Operator.lte, it))
        }
        when (DataSource.valueOf(filter.dataSource)) {
            DataSource.Test -> query.filters.add(Query.Filter("test", Query.Operator.eq, true))
            DataSource.Live -> query.filters.add(Query.Filter("test", Query.Operator.eq, false))
            else -> {
            }
        }
        filter.userId?.let {
            query.filters.add(Query.Filter("user._id", Query.Operator.eq, it))
        }
        filter._id?.let {
            query.filters.add(Query.Filter("_id", Query.Operator.eq, it))
        }
        filter.sessionId?.let {
            query.filters.add(Query.Filter("sessionId", Query.Operator.eq, it))
        }
        filter.clientType?.let {
            query.filters.add(Query.Filter("clientType", Query.Operator.regex, "^$it.*?$"))
        }
        filter.applicationId?.let {
            query.filters.add(Query.Filter("application._id", Query.Operator.eq, it))
        }

        query.sort = filter.sort?.let {
            Query.Sort(
                it, Query.SortDirection.valueOf(filter.direction)
            )
        }
    }
}
