package ai.flowstorm.studio.common.state

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.ui.UiState
import ai.flowstorm.common.stateManager.BaseStateManager
import ai.flowstorm.common.ui.StateLog
import io.kvision.utils.JSON.toObj
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

object AppState : BaseStateManager<RootState.State>(RootState.State()) {

    override val scope = App.scope

    override val modules = listOf(
        module(
            reducer = UiState.Reducer,
            getter = { rs -> rs.ui },
            setter = { rs, s -> rs.copy(ui = s) }
        ),
        module(
            reducer = RootState.Reducer,
            getter = { it },
            setter = { _, s -> s }
        )
    )

    init {
        scope.launch {
            stateFlow.collect {
                log("State changed:", it.toObj())
            }
        }
        scope.launch {
            start()
        }
    }

    override fun log(vararg o: Any?) {
        super.log(*o)
        StateLog.timeLog(o.joinToString(" "))
    }
}
