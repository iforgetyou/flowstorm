package ai.flowstorm.studio.ui

import ai.flowstorm.common.id.ObjectId
import ai.flowstorm.studio.App
import ai.flowstorm.studio.model.UserSpaceRole

object RootSpaceFirstComparator : Comparator<UserSpaceRole> {
    override fun compare(a: UserSpaceRole, b: UserSpaceRole): Int = when {
        a.spaceId == ObjectId(App.config.idMap.rootSpace as String) -> -1
        b.spaceId == ObjectId(App.config.idMap.rootSpace as String) -> 1
        else -> 0
    }
}
