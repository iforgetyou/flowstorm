package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Profile
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.resources.space.ProfileResource
import ai.flowstorm.studio.resources.space.UserAssignmentResource
import kotlinx.coroutines.launch
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityEditor
import ai.flowstorm.common.ui.JsonEditor
import io.kvision.core.TooltipOptions
import io.kvision.i18n.tr
import kotlinx.serialization.json.JsonElement

class UserAssignmentEditor : EntityEditor<Space.UserAssignment>(
    UserAssignmentForm(UserAssignmentResource),
    additionalTabs = mapOf("Attributes" to JsonEditor(JsonElement.serializer())),
    serializer = Space.UserAssignment.serializer(),
) {
    private val profileAttributesEditor get() = additionalTabs["Attributes"] as JsonEditor<JsonElement>
    private lateinit var profile: Profile

    override fun load(entity: Space.UserAssignment): Space.UserAssignment {
        mainScope.launch {
            profile = ProfileResource.getByUserId(super.entityForm.entity!!.user_id!!)
            profileAttributesEditor.setData(profile.attributes)
        }
        if (entity.user_id.toString() == UserService.context.user._id.toString()){
            deleteButton?.disabled = true
            deleteButton?.enableTooltip(TooltipOptions(tr("Use the Leave button in Flowstorm/Space Settings to leave a space")))
        }
        return super.load(entity)
    }

    override fun load(id: Id): Space.UserAssignment {
        val entity = entityComponent.load(id)
        jsonEditor.setData(entity)
        return super.load(this.load(entity))
    }
}
