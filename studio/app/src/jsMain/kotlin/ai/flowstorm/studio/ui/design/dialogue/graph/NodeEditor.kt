package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.model.Graph.Node
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import kotlinx.serialization.KSerializer
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.ReadOnly
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.core.Display
import io.kvision.core.Overflow
import io.kvision.core.onEvent
import io.kvision.form.text.TextAreaInput
import io.kvision.form.text.textAreaInput
import io.kvision.html.*
import io.kvision.i18n.gettext
import io.kvision.panel.SimplePanel
import io.kvision.panel.TabPanel
import io.kvision.panel.simplePanel
import io.kvision.state.ObservableValue
import kotlinx.coroutines.MainScope

abstract class NodeEditor<N : Node>(val graphEditor: GraphEditor) : SimplePanel(classes = setOf("panel-view-nav1")), Editor, ReadOnly {

    val mainScope = MainScope()

    open inner class NodeForm(serializer: KSerializer<N>) : Form<N>(serializer, setOf("panel-view-context")) {

        var nodeIdDiv: Div? = null

        init {
            overflow = Overflow.SCROLL
            marginRight = UiHelpers.spacing
            div(classes = setOf("form-group", "row")) {
                tag(TAG.LABEL, "#", classes = setOf("control-label", "col-sm-4", "col-form-label"))
                nodeIdDiv = div("?", classes = setOf("col-form-label"))
            }
            onEvent {
                change = {
                    if (validate()) {
                        save()
                    }
                }
            }
        }

        open fun load(node: N) {
            setData(node)
            nodeIdDiv?.content = node.id.toString()
            form.fields[Node::label.name]?.apply {
                setValue(node.label) // abstract parent properties must be set manually
            }
            form.fields[Node::name.name]?.apply {
                setValue(node.name) // abstract parent properties must be set manually
            }

            form.validate()
        }

        fun save() {
            save(node!!)
            graphEditor.diagramPanel.diagram.setNodeProperty(node!!.id, "label", node!!.label)
            graphEditor.commit(gettext("Node property changed") + " [#${node!!.id}]", "node[${node!!.id}].comment")
        }
    }

    open inner class NodeTabPanel : TabPanel(classes = setOf("node-editor")) {
        open fun tabInit(index: Int) {
            when (index) {
                0 -> form.form.fields[Node::name.name]?.focus()

            }
        }

        init {
            onEvent {
                tabChange = {
                    tabInit(it.detail.data.toString().toInt())
                }
            }
        }
    }

    var node: N? = null
    abstract val form: NodeForm
    abstract val tabPanel: NodeTabPanel
    lateinit var commentInput: TextAreaInput

    override var readOnly: Boolean = true
        set(value) {
            form.disabled = value
            commentInput.readonly = value
            field = value
        }

    val header = SimplePanel(classes = setOf("node-editor-header"))
    fun init(): NodeEditor<N> {
        add(header)
        add(tabPanel)

        commentInput = textAreaInput(classes = setOf("comment")) {
            display = Display.INLINE
            placeholder = gettext("Comment") + "..."
            onEvent {
                input = {
                    node?.comment = this@textAreaInput.value ?: ""
                    graphEditor.diagramPanel.diagram.setNodeProperty(node!!.id, "visible", !this@textAreaInput.value.isNullOrBlank())
                    graphEditor.commit(gettext("Node comment changed") + " [#${node!!.id}]", "node[${node!!.id}].comment")
                }
            }
        }
        return this
    }

    override fun load(item: Any) {
        load(item as N)
        with(header) {
            val node = item as Node
            removeAll()
            +"${node.name} (${node.id})"
        }

        commentInput.value = item.comment
    }

    open fun load(node: N) {
        this.node = node
    }

    fun edit() {
        tabPanel.activeIndex = 0
        form.form.fields[Graph.Node::name.name]?.focus()
    }
}
