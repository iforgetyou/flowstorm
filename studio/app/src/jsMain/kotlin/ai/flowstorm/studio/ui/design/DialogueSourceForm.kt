package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.resources.space.DialogueSourceResource
import ai.flowstorm.studio.ui.form.DialogueId
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.common.ui.EntityForm
import io.kvision.form.text.Text
import io.kvision.i18n.tr

open class DialogueSourceForm : EntityForm<DialogueSource>(DialogueSourceResource) {
    init {
        add(
                DialogueSource::_id,
                DialogueId(label = "ID")
        )
        add(
                DialogueSource::dialogueName,
                Text(label = tr("Name")),
                required = true,
                requiredMessage = tr("Required field")
        )
        add(
                DialogueSource::description,
                Text(label = tr("Description")),
                required = true,
                requiredMessage = tr("Required field")
        )
    }
}