package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.File
import ai.flowstorm.studio.ui.FileAssetForm
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityEditor

class FileAssetEditor(deleteEnabled: Boolean = true) : EntityEditor<File>(FileAssetForm(), deleteEnabled = deleteEnabled, serializer = File.serializer()) {

    override fun load(id: Id) = super.load(id).also {
        if (it.space_id != AppState.state.spaceId!!)
            disableAll()
    }
}
