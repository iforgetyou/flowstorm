package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Space
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod

object SpaceResource : EntityResource<Space>(resourceUri = "/spaces", serializer = Space.serializer()) {

    suspend fun joinCurrentUser(spaceId: Id) {
        remoteCall<Space.UserAssignment>("/${spaceId.value}/join", method = HttpMethod.PUT)

    }

    suspend fun leave(spaceId: Id) {
        remoteCall<Unit>("/${spaceId.value}/leave", method = HttpMethod.DELETE)
    }
}
