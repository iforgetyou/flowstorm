package ai.flowstorm.studio

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.state.RootState
import ai.flowstorm.studio.common.state.RootState.Action.InitApp
import ai.flowstorm.studio.common.state.RootState.Action.InitDone
import ai.flowstorm.studio.model.Enum
import ai.flowstorm.studio.resources.CurrentUserResource
import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.studio.resources.space.VersionResource
import ai.flowstorm.studio.state.LocalStorageStateMapper
import ai.flowstorm.studio.ui.*
import ai.flowstorm.studio.ui.design.dialogue.panel.AssistantPanel
import ai.flowstorm.common.*
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.auth.AnonymousAuthProviderWrapper
import ai.flowstorm.common.auth.Auth0LockProvider
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.state.LocalStorageDelegate
import ai.flowstorm.common.id.ObjectId
import ai.flowstorm.common.ui.BotApp
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.botApp
import kotlinx.browser.document
import kotlinx.browser.window
import ai.flowstorm.common.ui.route
import org.w3c.dom.HTMLElement
import org.w3c.dom.events.KeyboardEvent
import io.kvision.jquery.invoke
import io.kvision.jquery.jQuery
import io.kvision.require
import io.kvision.Application
import io.kvision.html.div
import io.kvision.i18n.DefaultI18nManager
import io.kvision.i18n.I18n
import io.kvision.i18n.tr
import io.kvision.modal.Alert
import io.kvision.panel.root
import io.kvision.panel.stackPanel
import io.kvision.routing.Routing
import io.kvision.routing.routing
import io.kvision.startApplication
import io.kvision.state.bind
import io.kvision.utils.obj
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.datetime.TimeZone

class App : Application(), ShortcutProvider {

    companion object {

        const val SNAPSHOT_VERSION = "1.0.0-SNAPSHOT"

        var config = Config()
        val scope = CoroutineScope(Dispatchers.Default + SupervisorJob())
        val timeZone =  TimeZone.currentSystemDefault()
    }

    private var versionAlerted = false
    private var lastVersion by LocalStorageDelegate(String::class)
    private lateinit var appComponent: BotApp

    override val shortcuts: List<Shortcut> = listOf(
            Shortcut(tr("Show available shortcut keys"), "k") { ShortcutModal(this).show() }
        )
        get() = field + ((appComponent as? ShortcutProvider)?.shortcuts ?: listOf())

    val translations = Enum.languages.map { it to require("i18n/messages-$it.json") }.toMap()

    init {
        require("@fortawesome/fontawesome-pro/css/all.min.css")
        require("scss/app.scss")
        ErrorHandler.init(config.sentryDsn, config.env)
        if (config.namespace != "default")
            document.title += " ${config.namespace}"

        CodeEditor.defineFlowstormHighlighter()
        ShortcutHandler.init(this)

        with(jQuery(document)) {
            keydown {
                if ((it.originalEvent as KeyboardEvent).key == "Escape" && document.activeElement != null)
                    (document.activeElement as HTMLElement).blur()
            }
        }

        Routing.init(window.location.toString().substringBefore("#"))

        I18n.manager = DefaultI18nManager(translations)
        AuthService.provider = AnonymousAuthProviderWrapper(Auth0LockProvider())

        AppState.sub { it.ui.language }.onEach { I18n.language = it }.launchIn(scope)
        AppState.sub { it.ui.theme }.onEach { jQuery("html").attr("data-theme", it) }.launchIn(scope)
        AppState.sub { it.ui.viewMode }.onEach { jQuery("html").attr("data-view-mode", it) }.launchIn(scope)
        AppState.sub { it.ui.editView }.onEach { jQuery("html").attr("data-toggle-edit-view", it) }.launchIn(scope)
        AppState.sub { it.ui.runView }.onEach { jQuery("html").attr("data-toggle-run-view", it) }.launchIn(scope)

        root("root", init = {}).bind(AppState.sub { it.appInitialized }) {
            if (!it) {
                div { content = "Loading..." }
            } else {
                appComponent = botApp {
                    mainComponent = stackPanel {
                        add(InvitationPage(), "/invitation")
                        add(TermsPage().route("/terms/:id").route("/terms"))
                        add(AccountPage().route("/account"))
                        add(SpacePanel())
                    }
                    botPanel = AssistantPanel()
                }
                removeSplashScreen()
            }
        }
    }

    private fun removeSplashScreen() {
        val element: dynamic = document.querySelector("#splash-screen")
        element.style.display = "none";
    }

    override fun start(state: Map<String, Any>) {
        scope.launch {
            console.log("INIT started")

            try {
                watchCoreUrls()
                initializeApp()
            } catch (t: Throwable) {
                ErrorHandler.handle(Exception("Application initialization failed.", t))
                return@launch
            }

            registerAuthRoutes()
            routing.resolve()

            console.log("INIT finished")

            scope.launch {
                checkVersion()
            }
        }
    }

    private suspend fun checkVersion() {
        while (true) {
            // check core version
            VersionResource.load().firstOrNull { it.name.equals("core", true) }?.let {
                if (lastVersion != it.value) {
                    lastVersion = it.value
                    AboutModal.show(if (lastVersion == SNAPSHOT_VERSION) 2 else 1)
                }
            }
            // check UI version
            val version = config["version"] as? String
            if (!versionAlerted && version != lastVersion && lastVersion != SNAPSHOT_VERSION) {
                versionAlerted = true
                Alert("Alert", "Please note that application UI version $version does not match core API version $lastVersion. To prevent potential compatibility issues, reload application in your browser.").show()
            }
            delay(10000)
        }
    }

    fun watchCoreUrls() {
        AppState.sub { it.spaceId }.onEach { id ->
            if (id != null) {
                val space = SpaceResource.load(id)
                val coreUrl = space.coreUrl
                    ?: App.config.coreUrl
                val fileUrl = space.coreUrl?.let { "$it/file" }
                    ?: App.config.fileUrl
                if (coreUrl != AppState.state.coreUrl || fileUrl != AppState.state.fileUrl) {
                    AppState.dispatch(RootState.Action.SetSpaceUrls(coreUrl, fileUrl))
                }
            }
        }.launchIn(scope)
    }

    private fun registerAuthRoutes() {
        with(routing) {
            on("/login", { _ -> AuthService.login() }, obj {
                before = { done: (suppress: Boolean?) -> Unit ->
                    done(!AuthService.isAuthenticated || AuthService.context.isAnonymous)
                }
            })

            on("/signup", { _ -> AuthService.signup() }, obj {
                before = { done: (suppress: Boolean?) -> Unit ->
                    done(!AuthService.isAuthenticated || AuthService.context.isAnonymous)
                }
            })
        }
    }

    private suspend fun initializeApp() {
        AuthService.authenticate()
        if (!AuthService.isAuthenticated) throw Exception("Authentication failed")

        with(UserService) {
            createContext()
            if (!isKnownUser) {
                if (AuthService.state.context!!.isAnonymous) {
                    CurrentUserResource.signUpAnonymous()
                } else {
                    CurrentUserResource.signUp()
                }
                createContext()
            }
            checkInvitations()
            getRoles()
            preFetchUserSpaces()
            val spaceId = context.roles.firstOrNull { it.spaceId == ObjectId(config.idMap.rootSpace) }?.spaceId
                ?: context.roles.first().spaceId

            AppState.dispatch(InitApp(context.roles, spaceId, context.user.language ?: "en"))
        }


        val route = window.location.hash.substringAfter("#!")
        if (route.isEmpty()) {
            if (AuthService.context.isAnonymous)
                routing.navigate("/space/dialogue")
            else
                routing.navigate("/space")
        }

        LocalStorageStateMapper.init()
        AppState.dispatch(InitDone)
    }

    override fun dispose() = mapOf<String, Any>()
}

fun main() = startApplication(::App)
