package ai.flowstorm.studio.ui.design.entity

import ai.flowstorm.studio.model.EntityDataset
import ai.flowstorm.studio.resources.EntityDatasetResource
import ai.flowstorm.common.setProperty
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.obj
import io.kvision.utils.pc

class EntityDatasetTable : EntityTable<EntityDataset>(
        caption = "Entitie",
        model = EntityDatasetResource,
        tabulatorOptions = TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), EntityDataset::name.name),
                        ColumnDefinition(tr("Language"), EntityDataset::language.name),
                        ColumnDefinition(tr("Id"), EntityDataset::_id.name)
                )
        ),
        entityComponentFactory = { EntityDatasetEditor(EntityDatasetResource) },
        tabWidth = 32.pc,
        splitMode = true,
        entityFactory = { EntityDataset() },
        className = "content-panel",
        dataExtractor = {
                obj {
                        setProperty(this, it::name)
                        setProperty(this, it::language)
                        _id = it._id.value
                }
        },
)
