package ai.flowstorm.studio.ui.space

import io.kvision.i18n.tr
import io.kvision.modal.Modal

object InvitationModal : Modal(tr("User Invitation")) {
    init {
        add(InvitationCreator())
    }
}