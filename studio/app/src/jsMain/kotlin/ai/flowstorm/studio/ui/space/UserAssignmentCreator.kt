package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.UserResource
import ai.flowstorm.studio.resources.space.UserAssignmentResource
import ai.flowstorm.studio.ui.UserForm
import ai.flowstorm.studio.model.User
import ai.flowstorm.common.ui.EntityEditor
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.panel.TabPanel
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal

class UserAssignmentCreator: TabPanel(classes = setOf("top-tabs")) {

    inner class ConsumerCreator: EntityEditor<User>(UserForm(UserResource).apply {
        getField("username")?.visible = false
    }, true, false, topTabs = false,serializer = User.serializer()) {

        override suspend fun save() {
            if (entityForm.validate()) {
                val user = User()
                entityForm.save(user)
                user.space_id = AppState.state.spaceId!!
                UserAssignmentResource.createConsumer(user)
                UiHelpers.parent<Modal>(this@UserAssignmentCreator)?.hide()
            }
        }
    }

    init {
        addTab(tr("Invite User"), InvitationCreator())
        addTab(tr("Create Consumer w/o E-mail"), ConsumerCreator())
    }
}
