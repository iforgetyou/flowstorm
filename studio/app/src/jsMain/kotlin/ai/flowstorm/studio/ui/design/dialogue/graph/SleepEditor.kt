package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Sleep
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.common.ui.Icon
import io.kvision.core.onEvent
import io.kvision.form.spinner.Spinner
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class SleepEditor(graphEditor: GraphEditor) : NodeEditor<Sleep>(graphEditor) {

    override val form = object : NodeForm(Sleep.serializer()) {
        init {
            add(Sleep::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
            add(Sleep::timeout, Spinner(label = tr("Timeout"), min = 1, max = 10080, step = 1.0, decimals = 0).apply {
                onEvent {
                    change = { if (validate()){ save() } }
                }
            })
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
        }
    }

    override fun load(node: Sleep) {
        super.load(node)
        form.load(node)
    }
}