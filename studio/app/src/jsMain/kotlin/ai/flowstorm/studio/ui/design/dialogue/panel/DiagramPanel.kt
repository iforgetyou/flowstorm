package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.common.state.AppState
import com.github.snabbdom.VNode
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.ui.design.dialogue.Diagram
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.studio.ui.design.dialogue.GraphEditorToolbar
import kotlinx.browser.window
import ai.flowstorm.common.ui.ReadOnly
import org.w3c.dom.DragEvent
import org.w3c.dom.HTMLCanvasElement
import io.kvision.core.Overflow
import io.kvision.core.onEvent
import io.kvision.i18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.state.subFlow
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class DiagramPanel(val graphEditor: GraphEditor) : SimplePanel(classes = setOf("panel-view-nav1")), ReadOnly {

    override var readOnly: Boolean = true

    val diagram = Diagram().apply {
        onCtrlC { graphEditor.copyToClipboard() }
        onCtrlV { graphEditor.pasteFromClipboard() }
    }

    val diagramPanel = object : SimplePanel(classes = setOf("panel-view-nav2")) {
        init {
            overflow = Overflow.HIDDEN

            onEvent {
                dragover = { it.preventDefault() }
                drop = { drop(it) }
            }
        }

        override fun afterInsert(node: VNode) {
            super.afterInsert(node)
            diagram.diagramDiv = node.elm!!
        }
    }

    private val graphEditorToolbar = GraphEditorToolbar(this)

    private val scope = MainScope()

    init {
        add(graphEditorToolbar)
        add(diagramPanel)
        scope.launch {
            AppState.subFlow { it.ui.theme }.collect {
                diagram.changeGroupMode()
                diagram.changeGridMode()
                diagram.updateView()
            }
        }
    }

    fun load(graph: Graph) {
        diagram.load(graph)
    }

    fun drop(event: DragEvent) {
        if(readOnly) return

        val canvas = event.target
        if (canvas is HTMLCanvasElement) {
            val bbox = canvas.getBoundingClientRect()
            var bbw = bbox.width
            if (bbw == 0.0) bbw = 0.001
            var bbh = bbox.height
            if (bbh == 0.0) bbh = 0.001
            val pixelratio = window.devicePixelRatio
            val mx = event.clientX - bbox.left * ((canvas.width / pixelratio) / bbw)
            val my = event.clientY - bbox.top * ((canvas.height / pixelratio) / bbh)

            val point = Diagram.Point(mx, my)
                    .let { diagram.transformViewToDoc(it) }
                    .let { diagram.snapToGrid(it) }

            with(graphEditor) {
                when {
                    draggedNode != null -> {
                        val node = draggedNode!!
                        node.loc = "${point.x} ${point.y}"
                        diagram.addNode(node)
                        diagram.selectNode(node, center = false, scaleViewport = false)
                        diagram.bringNodeToFront(node)
                        draggedNode = null
                    }
                    draggedGraph != null -> {
                        val snippetGraph = draggedGraph!!.clone()
                        snapSnippetToGrid(snippetGraph, point)
                        val idMap = graph.merge(snippetGraph)
                        reload(tr("Snippet graph inserted."))
                        diagram.selectGraph(snippetGraph, idMap)
                        diagram.bringGraphToFront(snippetGraph, idMap)
                        draggedGraph = null
                    }
                }
            }
        }
    }

    private fun snapSnippetToGrid(graph: Graph, center: Diagram.Point) {
        with(graph) {
            if (nodes.isEmpty()) return
            val mostTop = nodes.map { it.point.y }.minOrNull()!!
            val topNode = nodes.first { it.point.y == mostTop }
            move(
                    center.x - topNode.point.x,
                    center.y - topNode.point.y
            )
        }
    }

    private fun centerGraph(graph: Graph, center: Diagram.Point) {
        with(graph) {
            if (nodes.isEmpty()) return
            val mostLeft = nodes.map { it.point.x }.minOrNull()!!
            val mostTop = nodes.map { it.point.y }.minOrNull()!!
            val mostRight = nodes.map { it.point.x }.maxOrNull()!!
            val mostBottom = nodes.map { it.point.y }.maxOrNull()!!
            move(
                center.x - (mostLeft + mostRight) / 2,
                center.y - (mostTop + mostBottom) / 2
            )
        }
    }

    fun disableToolbarButtons() = graphEditorToolbar.disableButtons()
}
