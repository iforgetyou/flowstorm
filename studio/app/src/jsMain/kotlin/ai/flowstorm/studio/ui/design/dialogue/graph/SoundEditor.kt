package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Sound
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.form.UrlRef
import io.kvision.core.onEvent
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class SoundEditor(graphEditor: GraphEditor) : NodeEditor<Sound>(graphEditor) {

    override val form = object : NodeForm(Sound.serializer()) {
        init {
            add(
                    Sound::label,
                    Text(label = tr("Label")),
            )
            add(Sound::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
            add(Sound::source, UrlRef(label = tr("URL")).apply {
                onEvent {
                    change = {
                        if (!this@apply.value.isNullOrBlank())
                            graphEditor.diagramPanel.diagram.setNodeProperty(node!!.id, "source", this@apply.value ?: "")
                    }
                }
            }, required = true)
            add(Sound::repeatable, CheckBox(label = tr("Repeatable")))
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
        }
    }

    override fun load(node: Sound) {
        super.load(node)
        form.load(node)
    }
}