package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.Graph.*

object NodeIcons {
    val icons = mapOf(
            GlobalAction::class.simpleName to ("fas fa-square" to 0x058891),
            GlobalIntent::class.simpleName to ("fas fa-square" to 0x009509),
            UserInput::class.simpleName to ("icon-diamond" to 0x17c25c),
            ReInput::class.simpleName to ("icon-diamond" to 0x2facb4),
            Action::class.simpleName to ("fas fa-square" to 0x2facb4),
            Intent::class.simpleName to ("fas fa-square" to 0x04d511),
            Speech::class.simpleName to ("fas fa-square" to 0x245cfe),
            Sound::class.simpleName to ("fas fa-circle" to 0x245cfe),
            Image::class.simpleName to ("fas fa-square" to 0x245cfe),
            Command::class.simpleName to ("fas fa-square" to 0x222670),
            Function::class.simpleName to ("icon-diamond" to 0x222670),
            SubDialogue::class.simpleName to ("icon-diamond" to 0x7019fe),
            Enter::class.simpleName to ("fas fa-circle" to 0xc204e8),
            GoBack::class.simpleName to ("fas fa-circle" to 0xc204e8),
            Exit::class.simpleName to ("fas fa-circle" to 0xc204e8),
            Sleep::class.simpleName to ("fas fa-circle" to 0xc204e8),
            End::class.simpleName to ("fas fa-circle" to 0xc204e8)
    )
}