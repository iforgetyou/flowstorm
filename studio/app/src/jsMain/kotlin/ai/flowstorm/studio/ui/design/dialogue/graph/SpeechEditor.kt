package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Speech
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.ui.design.MatrixForm
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.studio.ui.form.VoiceId
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.ListIdSelect
import io.kvision.core.onEvent
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.html.Div
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class SpeechEditor(graphEditor: GraphEditor) : NodeEditor<Speech>(graphEditor), EditableCode {

    init {
        graphEditor.subscribe {
            mainScope.launch {
                mixinSelect.options = UiHelpers.options(DialogueMixinResource.load("Speech", it.language))
            }
        }
    }

    val mixinSelect = ListIdSelect(label = tr("Mixins")).apply {
        mainScope.launch {
            options = UiHelpers.options(DialogueMixinResource.load("Speech", graphEditor.state.language))
        }
        onEvent {
            change = {
                mainScope.launch {
                    codeEditor.set(codeEditor.value, this@apply.getValue() ?: listOf())
                }
            }
        }
    }

    override val codeEditor = CodeEditor("flowstorm", setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.responses.joinToString("\n") != self.value) {
                    node?.responses = self.value.lines()
                    graphEditor.commit(gettext("Speech responses changed") + " [#${node!!.id}]", "speech[${node!!.id}].responses")
                }
            }
        }
    }
    val payloadEditor = CodeEditor("yaml", setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.code != self.value) {
                    node?.code = self.value
                    graphEditor.commit(gettext("Speech payload changed") + " [#${node!!.id}]", "speech[${node!!.id}].code")
                }
            }
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
            payloadEditor.readOnly = value
        }

    private val voice = VoiceId().apply {
        load(null)
    }

    override val form = object : NodeForm(Speech.serializer()) {
        init {
            add(
                Speech::label,
                Text(label = tr("Label"))
            )
            add(Speech::name, Text(label = tr("Name")),
                required = true,
                validatorMessage = { gettext("Node name must be unique.") },
                validator = { text ->
                    !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                }
            )
            add(
                Speech::voice_id,
                voice
            )
            add(
                    Speech::mixins_id,
                    mixinSelect
            )
            add(Speech::background, Text(label = tr("Background")))
            add(Speech::repeatable, CheckBox(label = tr("Repeatable")))
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Texts"), codeEditor, Icon.Text.def)
            addTab(tr("Payload"), payloadEditor, Icon.Payload.def)
            addTab(tr("C-Matrix"), MatrixForm(form.nodeIdDiv ?: Div(content = ""), graphEditor), Icon.Matrix.def)
            activeIndex = 1
        }
    }

    override fun load(node: Speech) {
        super.load(node)
        form.load(node)
        codeEditor.set(node.responses.joinToString("\n"), node.mixins_id)
        payloadEditor.set(node.code ?: "")
    }
}