package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.resources.space.ActivityResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.modal.ModalSize
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions

class ActivityTable : EntityTable<Activity>(
    "Space Activitie",
    ActivityResource,
    TabulatorOptions(
        layout = Layout.FITCOLUMNS,
        columns = listOf(
            ColumnDefinition(tr("User"), Activity::username.name, width = "20%"),
            ColumnDefinition(tr("Type"), Activity::type.name, width = "10%"),
            ColumnDefinition(tr("Name"), Activity::name.name, width = "15%"),
            ColumnDefinition(tr("Reference"), Activity::refName.name, width = "35%"),
            ColumnDefinition(tr("Date"), Activity::datetimeString.name, width = "20%")
        )
    ),
    modalSize = ModalSize.LARGE,
    className = "content-panel"
)