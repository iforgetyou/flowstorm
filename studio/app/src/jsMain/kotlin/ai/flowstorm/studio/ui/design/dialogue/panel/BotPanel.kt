package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.ui.UiState.Action.ToggleBotPanel
import ai.flowstorm.studio.model.Enum
import ai.flowstorm.studio.ui.RunnableComponent
import ai.flowstorm.common.BotLogic
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Hints.hint
import io.kvision.core.*
import io.kvision.form.select.Select
import io.kvision.html.ButtonStyle
import io.kvision.i18n.I18n.tr
import io.kvision.i18n.gettext
import io.kvision.panel.simplePanel
import io.kvision.toolbar.buttonGroup
import io.kvision.toolbar.toolbar
import io.kvision.utils.perc
import io.kvision.utils.px
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BotPanel(override val runConsumer: RunnableComponent, override val runPanel: RunPanel, clientPanel: ClientPanel) :
    BotContainerPanel(), ShortcutProvider {
    private val mainScope = MainScope()

    override val shortcuts: List<Shortcut> = listOf(
            Shortcut(tr("Run"), "r") {
                this.runBot()
            },
            Shortcut(tr("Stop"), "q") { BotLogic.stop() },
    )

    override var focusNodes = false
    override val canReloadPanels: Boolean = true

    private val voiceSelect = Select(UiHelpers.options(Enum.voices).apply {
        hint("VoiceSelect", Icon.Voice, index = 234)
        add("" to gettext("by dialogue"))
    }, "").apply {
        minWidth = 130.px
        maxHeight = 130.px
    }
    private val botFocusButton = ToolbarButton(Icon.Focus, gettext("Focus Nodes")) {
        onClick {
            focusNodes = !focusNodes
            active = !active
        }
    }
    private val clientAttributesButton: ToolbarButton = ToolbarButton(Icon.Signal, gettext("Attributes"), "T", style = ButtonStyle.LIGHT) {
        active = true
        onClick {
            clientPanel.activeIndex = 0
            clientPanel.overflow = Overflow.SCROLL
            clientTextAttributesButton.active = false
            active = true
        }
    }
    private val clientTextAttributesButton: ToolbarButton = ToolbarButton(Icon.List, gettext("Attributes as Text"), "t", style = ButtonStyle.LIGHT) {
        onClick {
            clientPanel.activeIndex = 1
            clientPanel.overflow = Overflow.HIDDEN
            clientAttributesButton.active = false
            active = true
        }
    }

    private val runButton: ToolbarButton = ToolbarButton(Icon.Run, gettext("Run"), style = ButtonStyle.LIGHT) {
        text = " Test run"
        onClick {
            runBot()
        }
    }

    init {
        float = PosFloat.LEFT
        minWidth = 33.perc
        height = 100.perc
        simplePanel(classes = setOf("voice-select")) {
            simplePanel(classes = setOf("panel-toolbar")) {
                hint("BotPanelToolbar", Icon.Tools, index = 233)
                toolbar {
                    float = PosFloat.LEFT
                    buttonGroup {
                        if (runPanel.botFocus)
                        add(botFocusButton)
                }
                buttonGroup {
                    add(clientAttributesButton)
                    add(clientTextAttributesButton)
                }
            }
            toolbar {
                float = PosFloat.RIGHT
                buttonGroup {
                    addCssClass("test-btn")
                    add(runButton)
                }
            }
            simplePanel {
                float = PosFloat.RIGHT
                height = ModelEditor.NAV_HEIGHT.px

                }/*.apply {
                    add(voiceSelect)
                }*/
                /*
                simplePanel {
                    float = PosFloat.RIGHT
                    height = ModelEditor.NAV_HEIGHT.px

                }.apply {
                    add(sttModeSelect)
                }*/
            }
            add(clientPanel)
        }
    }

    override fun getVoice() = voiceSelect.value ?: ""

    override fun getAttributes() = runPanel.clientPanel.attributes

    override fun focusTab() = runPanel.switchToTab(runPanel.runLogPanel)

    override fun log(logLines: List<LogPanel.LogLine>) {
        AssistantPanel.dialoguePanel.log(logLines)
        runPanel.runLogPanel.log(logLines)
    }

    override fun logSignal(signal: String) {
        runPanel.runLogPanel.log(listOf(signal), "dark")
    }

    override fun setCurrentImage(image: String) {
        AssistantPanel.dialoguePanel.setCurrentImage(image)
        currentImage = image
    }

    override fun setBackground(background: String) {
        AssistantPanel.dialoguePanel.setBackground(background)
    }

    override fun setVideo(video: String, callback: dynamic) {
        AssistantPanel.dialoguePanel.setVideo(video, callback)
        runPanel.runLogPanel.log("{Video $video}", "secondary")
    }

    override fun setInputAudio(setOn: Boolean) {
        AssistantPanel.dialoguePanel.setInputAudio(setOn)
    }

    private fun runBot() {
        AssistantPanel.tabPanel.activeIndex = 0
        mainScope.launch {
            delay(200)
            BotLogic.bot = AssistantPanel.dialoguePanel.bot
            BotLogic.botPanel = this@BotPanel
            if (BotLogic.state.runningStatus == BotLogic.Status.Stopped)
                AppState.dispatch(ToggleBotPanel(true))
            BotLogic.run()
        }
    }
}
