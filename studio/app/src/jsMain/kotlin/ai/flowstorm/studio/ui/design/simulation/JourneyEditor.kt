package ai.flowstorm.studio.ui.design.simulation

import ai.flowstorm.studio.model.Simulation
import ai.flowstorm.studio.model.Simulation.Waypoint
import ai.flowstorm.studio.ui.SignalForm
import ai.flowstorm.studio.ui.design.SimulationEditor
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.core.type.Location
import ai.flowstorm.core.ui.GoogleMap
import io.kvision.core.*
import io.kvision.form.text.Text
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.panel.*
import io.kvision.utils.perc
import io.kvision.utils.px

class JourneyEditor(simulationEditor: SimulationEditor) : SimplePanel() {

    lateinit var leftPanelWrapper: SimplePanel
    lateinit var leftPanel: TabPanel
    lateinit var googleMap: SimplePanel
    lateinit var rightPanel: TabPanel

    var selectedWaypoint: Waypoint? = null
    val propertiesEditor: PropertiesEditor<Waypoint> = object : PropertiesEditor<Waypoint>(setOf("panel-view-nav2")) {
        override val form = form(Waypoint.serializer()) {
            add(
                    Waypoint::name,
                    Text(label = tr("Name")),
                    required = true,
                    requiredMessage = tr("Required field")
            )
            add(
                    Waypoint::timeAsString,
                    Text(label = tr("Time")),
                    required = true,
                    requiredMessage = tr("Required field")
            )
            // TODO move elsewhere?
            add(Button(tr("Delete"), style = ButtonStyle.DANGER).onClick {
                if (selectedWaypoint != null){
                    waypointList.selected = null
                    simulationEditor.entity?.waypoints?.remove(selectedWaypoint!!)
                    (googleMap as GoogleMap).removeMarker(selectedWaypoint?.location!!)
                    form.clearData()
                    locationEditor.form.clearData()
                    signalEditor.form.clearData()
                    selectedWaypoint = null
                    waypointList.search()
                }
            })
        }
        init {
            init()
            form.onEvent { change = {waypointList.search()} }
        }
    }
    val locationEditor = object : PropertiesEditor<Location>(setOf("panel-view-nav2")) {
        override val form = LocationForm {}
        init { init() }
    }
    val signalEditor = object : PropertiesEditor<Waypoint>(setOf("panel-view-nav2")) {
        override val form = SignalForm()
        init { init() }
    }
    val waypointList = object : ObjectList<Waypoint>(object : ObjectProvider<Waypoint> {
        override suspend fun getItems() = simulationEditor.entity?.waypoints ?: mutableListOf()
    }) {
        override fun row(item: Waypoint, index: Int, list: MutableList<Waypoint>): Component {
            val row = Tag(TAG.DIV, "${item.timeAsString} ${item.name}", classes = setOf("object-list-row")).apply {
                cursor = Cursor.POINTER
            }
            row.onClick {
                propertiesEditor.load(item)
                locationEditor.load(item.location)
                signalEditor.load(item)
                (googleMap as GoogleMap).centerOnLocation(item.location)
                setColors(selected, Col.BLACK, Col.WHITE)
                selected = this
                setColors(selected, Col.WHITE, Col.BLUE)
                selectedWaypoint = item
            }
            return row
        }

        override fun filter(item: Waypoint, search: String) = item.name.contains(search, true)
    }
    // TODO rework styling
    fun setColors (tag: Tag?, font: Col, background: Col) {
        tag?.color = Color.name(font)
        tag?.background = Background(Color.name(background))
    }

    fun processWaypoint (value: dynamic, simulationEditor: SimulationEditor) {
        simulationEditor.entity?.waypoints?.add(Simulation.Waypoint("Waypoint", 0, location = Location(value.lat, value.lng)))
        this.waypointList.search()
    }

    init {
        splitPanel {
            width = 100.perc
            leftPanelWrapper = simplePanel {
                width = 220.px
                leftPanel = tabPanel(classes = setOf("sub-nav"), scrollableTabs = true) {
                    addTab("", waypointList, icon = Icon.Waypoint.def)
                }
            }
            simplePanel {
                splitPanel {
                    googleMap = GoogleMap(processWaypoint = { processWaypoint(it, simulationEditor) }, clickable = true, classes = setOf("panel-view-nav1")) {
                        width = 67.perc
                    }
                    add(googleMap)
                    rightPanel = tabPanel {
                        addTab(tr("Properties"), propertiesEditor, Icon.Properties.def)
                        addTab(tr("Location"), locationEditor, Icon.Location.def)
                        addTab(tr("Signals"), signalEditor, Icon.Signal.def)
                    }
                }
            }
        }
    }
}