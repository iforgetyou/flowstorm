package ai.flowstorm.studio.common.ui

import ai.flowstorm.studio.model.*
import ai.flowstorm.common.model.Entity
import ai.flowstorm.common.ui.Icon

fun Entity.icon(): String = when (this) {
    is Voice -> Icon.Voice.def
    is DialogueSnippet -> Icon.Graph.def
    is DialogueMixin -> Icon.DialogueMixin.def
    is File -> Icon.FileAsset.def
    is Application -> Icon.Application.def
    is DialogueSource -> Icon.DialogueSource.def
    else -> {
        error("Missing entity icon definition.")
    }
}
