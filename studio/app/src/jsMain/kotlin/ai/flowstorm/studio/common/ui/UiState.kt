package ai.flowstorm.studio.common.ui

import ai.flowstorm.studio.common.ui.UiState.Action.*
import ai.flowstorm.common.stateManager.ReducerAction
import ai.flowstorm.common.stateManager.StateReducer
import kotlinx.serialization.Serializable

class UiState {

    @Serializable
    data class State(
        val language: String = "en",
        val botPanelOpened: Boolean = false,
        val theme: String = "light-mode",
        val viewMode: String = "desktop",
        val editView: String = "off",
        val runView: String = "off",
    )

    sealed interface Action : ReducerAction {
        data class ToggleBotPanel(val opened: Boolean? = null) : Action
        data class ChangeTheme(val theme: String) : Action
        data class ChangeViewMode(val mode: String) : Action
        data class ToggleView(val editView: String? = null, val runView: String? = null) : Action
        data class ChangeLanguage(val language: String) : Action
    }

    object Reducer : StateReducer<State, Action> {
        override fun canReduce(action: ReducerAction) = action is Action
        override fun reduce(state: State, action: Action): State = when (action) {
            is ToggleBotPanel -> state.copy(botPanelOpened = action.opened ?: !state.botPanelOpened)
            is ChangeTheme -> state.copy(theme = action.theme)
            is ChangeViewMode -> state.copy(viewMode = action.mode)
            is ToggleView -> state.copy(
                editView = action.editView ?: state.editView,
                runView = action.runView ?: state.runView,
            )
            is ChangeLanguage -> state.copy(language = action.language)
        }
    }
}
