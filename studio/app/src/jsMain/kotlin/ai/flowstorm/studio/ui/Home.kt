package ai.flowstorm.studio.ui

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.BaseReportResource.ReportSettings
import ai.flowstorm.studio.ui.data.RecentActivityPanel
import ai.flowstorm.studio.ui.data.RecentDialoguePanel
import ai.flowstorm.studio.ui.data.ReportViewer
import ai.flowstorm.studio.ui.design.dialogue.panel.DialogueEventPanel
import ai.flowstorm.studio.util.now
import ai.flowstorm.studio.util.toDate
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.ui.CardPanel
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.ui.Icon
import io.kvision.core.onEvent
import io.kvision.form.check.radioGroup
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.panel.simplePanel
import io.kvision.panel.tabPanel
import io.kvision.routing.routing
import io.kvision.state.bind
import io.kvision.state.sub
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.minus

object Home: CardPanel("Home"), Reloadable {

    private var trafficReport = ReportViewer(
            ReportSettings(
                    aggregations = "NAMESPACE,METRIC",
                    namespaces = listOf(StringId("session")),
                    metrics = listOf(StringId("count"), StringId("turns")),
                    dateFrom = now().minus(14, DateTimeUnit.DAY, App.timeZone).toDate()
            )
    )

    private var appPerformanceReport = ReportViewer(
            ReportSettings(
                    aggregations = "APPLICATION",
                    namespaces = listOf(StringId("session")),
                    metrics = listOf(StringId("count")),
                    dateFrom = now().minus(7, DateTimeUnit.DAY, App.timeZone).toDate()
            )
    )

    private var dialogueEventPanel = DialogueEventPanel().apply {
        paddingTop = UiHelpers.spacing
    }

    private var recentDialoguePanel = RecentDialoguePanel()

    private var recentActivityPanel = RecentActivityPanel().apply {
        paddingTop = UiHelpers.spacing
    }

    private var charts = simplePanel(classes = setOf("col-12", "content-panel")) {
        simplePanel(classes = setOf("dashboard")) {
            simplePanel(classes = setOf("row", "pt-3")) {
                simplePanel(classes = setOf("col-12", "col-sm-6", "col-md-3", "pr-sm-0")) {
                    simplePanel(classes = setOf("dashboard__pane", "dashboard__pane--news")) {
                        div {
                            button(tr("New dialogue model"), Icon.DialogueSnippetDesigner.def) {
                            }.onClick {
                                routing.navigate("/space/dialogue/new")
                            }
                        }
                        div {
                            button(tr("New application"), Icon.Application.def) {
                            }.onClick {
                                routing.navigate("/space/applications")
                            }
                        }
                        div {
                            button(tr("Open") + "...", Icon.DialogueSnippetDesigner.def) {
                            }.onClick {
                                routing.navigate("/space/dialogue/open")
                            }
                        }
                    }
                }
                simplePanel(classes = setOf("col-12", "col-sm-6", "col-md-3", "pl-sm-0", "pr-md-4")) {
                    simplePanel(classes = setOf("dashboard__pane", "dashboard__pane--recents")) {
                        h3(tr("Recent models"))
                        add(recentDialoguePanel)
                    }
                }
                simplePanel(classes = setOf("col-12", "pt-5", "col-md-6", "pt-md-0", "pl-md-4")) {
                    simplePanel(classes = setOf("dashboard__pane", "dashboard__pane--raw", "dashboard__pane--video")) {
                        simplePanel(classes = setOf("embed-responsive", "h-100", "embed-responsive-21by9")) {
                            div("""<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-rd8pYsKoek" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>""".trimIndent(), rich = true)
                        }
                    }
                }
            }
            simplePanel(classes = setOf("row", "pt-5")) {
                simplePanel(classes = setOf("col-12", "col-md-6", "pr-md-4")) {
                    simplePanel(classes = setOf("dashboard__pane", "dashboard__pane--stripped")) {
                        h3(tr("Traffic"))
                        radioGroup(listOf("Me" to tr("Me"), "Space" to tr("Space")), inline = true, value = "Space") {
                            onEvent {
                                change = {
                                    val users = when (this@radioGroup.value) {
                                        "Me" -> listOf(UserService.context.user.id)
                                        "Space" -> listOf()
                                        else -> error("")
                                    }
                                    listOf(trafficReport, appPerformanceReport).forEach {
                                        it.reportSettings = it.reportSettings.copy(users_id = users)
                                        it.reload()
                                    }
                                }
                            }
                        }
                        tabPanel {
                            addTab(tr("Last 14 days"), trafficReport)
                            addTab(tr("Sessions for last 7 days"), appPerformanceReport)
                        }
                    }
                }
                simplePanel(classes = setOf("col-12", "pt-5", "col-md-6", "pt-md-0", "pl-md-4")) {
                    simplePanel(classes = setOf("dashboard__pane", "dashboard__pane--stripped")) {
                        h3(tr("News"))
                        tabPanel {
                            addTab(tr("Activity"), recentActivityPanel)
                            addTab(tr("Dialogue Events"), dialogueEventPanel)
                        }
                    }
                }
            }
        }
    }

    init {
        addPanelHeader()
        add(charts)

        bind(AppState.sub { it.spaceId }, false) {
            reload()
        }
        bind(AuthService, false) { state ->
            panelHeader.content = tr("Welcome") + ", " + state.context!!.name.substringBefore( ' ') + "!"
        }
    }

    override fun reload() {
        trafficReport.reload()
        dialogueEventPanel.reload()
        appPerformanceReport.reload()
        recentDialoguePanel.reload()
        recentActivityPanel.reload()
    }
}
