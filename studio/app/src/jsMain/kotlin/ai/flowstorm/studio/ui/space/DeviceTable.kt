package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.resources.space.DeviceResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.modal.ModalSize
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions

class DeviceTable: EntityTable<Device>(
        "Client Device",
        DeviceResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Device ID"), Device::deviceId.name),
                        ColumnDefinition(tr("Description"), Device::description.name),
                        ColumnDefinition(tr("Location"), Device::location.name),
                        ColumnDefinition(tr("Username"), Device::_user.name + "." + User::username.name),
                        ColumnDefinition(tr("Last Talk"), Device::lastTalk.name)
                )
        ),
        { DeviceForm(DeviceResource) },
        deleteEnabled = false,
        modalSize = ModalSize.LARGE,
        className = "content-panel"
)