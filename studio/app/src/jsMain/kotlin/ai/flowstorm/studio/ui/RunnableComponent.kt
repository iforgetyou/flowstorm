package ai.flowstorm.studio.ui

interface RunnableComponent {

    val key: String?
    val voice: String?
    fun toggleRunPanelView()
}