package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Version
import ai.flowstorm.common.resources.EntityResource
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object VersionResource : EntityResource<Version>(serializer = Version.serializer()) {

    init {
        AppState.sub { it.spaceId }.onEach { clear() }.launchIn(App.scope)
    }

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/versions"
}
