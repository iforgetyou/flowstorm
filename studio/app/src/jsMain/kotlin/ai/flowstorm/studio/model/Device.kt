package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class Device(
    override val _id: Id = newId(),
    var deviceId: String? = null,
    var deviceToken: String? = null,
    var description: String? = null,
    var user_id: Id? = null,
    var space_id: Id? = null,
    var location: String? = null,
    var configuration: String? = "{}",
    var lastTalk: Instant? = null,
    var _user: User? = null,
    var _space: Space? = null
) : AbstractEntity() {

    override val label: String
        get() = deviceId ?: ""

    override fun match(input: String): Boolean =
        "$deviceId $description $location".contains(input, true)
}