package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Terms
import ai.flowstorm.studio.model.TermsConsent as Consent
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod
import io.kvision.rest.NotFound

object TermsResource : EntityResource<Terms>(resourceUri = "/terms", serializer = Terms.serializer()) {

    suspend fun getConsent(termsId: Id) = try {
        remoteCall("/$termsId/consent", deserializer = Consent.serializer())
    } catch (t: NotFound) {
        null
    }

    suspend fun giveConsent(termsId: Id) =
        remoteCall("/$termsId/consent", method = HttpMethod.POST, deserializer = Consent.serializer(), doToast = false)

    suspend fun revokeConsent(termsId: Id) =
        remoteCall<Unit>("/$termsId/consent", method = HttpMethod.DELETE, doToast = false)
}