package ai.flowstorm.studio.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class LogEntry(
    val time: Instant,
    val relativeTime: Float,
    val level: Level,
    val text: String
) {
    enum class Level { ERROR, WARN, INFO, DEBUG, TRACE }
}