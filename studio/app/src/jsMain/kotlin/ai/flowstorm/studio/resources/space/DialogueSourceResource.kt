package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.ObjectId
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod
import kotlinx.coroutines.await
import kotlinx.coroutines.coroutineScope
import kotlinx.serialization.Serializable

object DialogueSourceResource : EntityResource<DialogueSource>(serializer = DialogueSource.serializer()) {

    override val resourceUri: String = ""
    private val xmlSerializer = js("new XMLSerializer()")

    override suspend fun load(id: Id, useCache: Boolean): DialogueSource = coroutineScope {
        val e = entities.find { it.id == id }
        if (e != null && useCache) {
            e
        } else {
            remoteCall<DialogueSource>(client.baseUrl + "/dialogueSources/${id.value}", null, HttpMethod.GET).also { newEntity ->
                if (e != null) {
                    replaceEntity(e, newEntity)
                } else {
                    entities.add(newEntity)
                }
            }
        }
    }

    private fun createUri(spaceId: Id): String = "/spaces/${spaceId.value}/dialogueSources"

    override fun createEntityUri(entity: DialogueSource?): String =
        createUri(entity?.space_id ?: AppState.state.spaceId!!) + super.createEntityUri(entity)

    suspend fun build(dialogueSource: DialogueSource): Build {
        return client.remoteCall(
            url = resourceUrl + createEntityUri(dialogueSource) + "/build",
            method = HttpMethod.POST,
            deserializer = Build.serializer(),
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun publish(dialogueSource: DialogueSource) {
        client.remoteCall(
            url = resourceUrl + createEntityUri(dialogueSource) + "/publish",
            method = HttpMethod.PUT,
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun archive(dialogueSource: DialogueSource) {
        client.remoteCall(
            url = resourceUrl + createEntityUri(dialogueSource) + "/archive",
            method = HttpMethod.PUT,
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun lock(dialogueSource: DialogueSource) {
        client.remoteCall(
            url = resourceUrl + createEntityUri(dialogueSource) + "/lock",
            method = HttpMethod.PUT,
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun unlock(dialogueSource: DialogueSource, force:Boolean = false) {
        client.remoteCall(
            url = resourceUrl + createEntityUri(dialogueSource) + "/lock?force=$force",
            method = HttpMethod.DELETE,
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun getXsl(dialogueSource: DialogueSource): String {
        val xmlDocument = client.remoteCall(resourceUrl + createEntityUri(dialogueSource) + ".xsl",
                method = HttpMethod.GET,
                beforeSend = client.beforeSend
        ).await()
        return xmlSerializer.serializeToString(xmlDocument.documentElement) as String
    }

    suspend fun getTransformedXml(dialogueSource: DialogueSource, xml: String): String {
        val xmlDocument = client.remoteCall(resourceUrl + createEntityUri(dialogueSource) + "/transformed.xml",
                data = xml,
                method = HttpMethod.PUT,
                contentType = "text/xml",
                beforeSend = client.beforeSend
        ).await()
        return xmlSerializer.serializeToString(xmlDocument.documentElement) as String
    }

    suspend fun getXml(dialogueSource: DialogueSource): String {
        val xmlDocument = client.remoteCall(resourceUrl + createEntityUri(dialogueSource) + ".xml",
                method = HttpMethod.GET,
                beforeSend = client.beforeSend
        ).await()
        return xmlSerializer.serializeToString(xmlDocument.documentElement) as String
    }

    suspend fun createXml(xml: String): DialogueSource {
        val spaceId: String = "<space_id>(.*)</space_id>".toRegex().find(xml)?.groupValues?.get(1)?: error("Missing space_id in xml document")

        return client.remoteCall(client.baseUrl + createUri(ObjectId(spaceId)),
                data = xml,
                method = HttpMethod.POST,
                contentType = "text/xml",
                beforeSend = client.beforeSend
        ).await() as DialogueSource
    }

    suspend fun transformXml(dialogueSource: DialogueSource, xml: String): DialogueSource {
        return client.remoteCall(client.baseUrl + createUri(dialogueSource.space_id) + "/${dialogueSource.id.value}.xsl",
                data = xml,
                method = HttpMethod.POST,
                contentType = "text/xml",
                beforeSend = client.beforeSend
        ).await() as DialogueSource
    }

    suspend fun updateXml(dialogueSource: DialogueSource, xml: String): String {
        val xmlDocument = client.remoteCall(client.baseUrl + createUri(dialogueSource.space_id) + "/${dialogueSource.id.value}.xml",
                data = xml,
                method = HttpMethod.PUT,
                contentType = "text/xml",
                beforeSend = client.beforeSend
        ).await()
        return xmlSerializer.serializeToString(xmlDocument.documentElement) as String
    }

    suspend fun nameExists(spaceId: Id, dialogueName: String): Boolean {
        val response = client.remoteRequest(
            url = resourceUrl + createUri(spaceId) + "?dialogueName[eq]=$dialogueName",
            method = HttpMethod.HEAD,
            beforeSend = client.beforeSend
        ).await()
        return response.jqXHR.getResponseHeader("doc-count")!!.toInt() > 0
    }

    suspend fun loadGraph(dialogueSource: DialogueSource): Graph {
        return client.remoteCall<Graph>(
            url = resourceUrl + createEntityUri(dialogueSource) + "/graph",
            deserializer = Graph.serializer(),
            method = HttpMethod.GET,
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun getTemplate(name: String): DialogueSource = client.remoteCall(
        url = resourceUrl + createUri(AppState.state.spaceId!!) + "/template/$name",
        deserializer = DialogueSource.serializer(),
        method = HttpMethod.GET,
        beforeSend = client.beforeSend
    ).await().copy(_id = newId(), version = 0, space_id = AppState.state.spaceId!!)

    @Serializable
    data class Build(
        val _id: String,
        val success: Boolean,
        val logs: List<String> = listOf(),
        val error: String = ""
    )
}
