package ai.flowstorm.studio.resources

object ReportResource : BaseReportResource() {
    override val resourceUri: String
        get() = "/reports/data"
}
