package ai.flowstorm.studio.model

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.id.newId
import kotlinx.serialization.Serializable

@Serializable
data class EntityDataset(
    override val _id: Id = newId(),
    var space_id: Id = AppState.state.spaceId!!,
    val name: String = "",
    val language: String = "en",
    var valueSamples: MutableMap<String, List<String>> = mutableMapOf(),
    var samples: List<DataSample> = mutableListOf(),
    val dataFrom: Int = 0
) : AbstractEntity() {
    override val label by this::name

    @Serializable
    data class DataSample(var text: String = "", var _id: Int = -1) : HasId {
        override val id: Id
            get() = StringId(_id.toString())
    }

    @Serializable
    data class Status(val status: String)
}
