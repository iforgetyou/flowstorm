package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.resources.space.TermsResource
import ai.flowstorm.studio.ui.form.DialogueId
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.SelectId
import ai.flowstorm.common.ui.form.UrlRef
import io.kvision.core.Background
import io.kvision.core.BgRepeat
import io.kvision.core.BgSize
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.i18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.utils.perc
import io.kvision.utils.px
import kotlinx.coroutines.launch

class ApplicationForm(model: EntityResource<Application>) : EntityForm<Application>(model) {
    private val urlRegex = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)".toRegex()
    init {
        add(
            Application::name,
            Text(label = tr("Name")),
            required = true,
            requiredMessage = tr("Required field")
        ) {
            (it.value?.length ?: 0) > 0
        }
        add(
            Application::description,
            TextArea(label = tr("Description"), rows = 3),
            required = true,
            requiredMessage = tr("Required field")
        )
        add(
            Application::dialogue_id,
            DialogueId(label = tr("Main Dialogue")),
            required = true,
            requiredMessage = tr("Required field")
        )
        add(
            Application::url,
            UrlRef(label = tr("Web Bot URL"), mapOf("allow" to "microphone"))
        )
        add(
            Application::terms_id,
            SelectId(label = tr("Terms and Conditions")).apply {
                mainScope.launch {
                    options = UiHelpers.options(TermsResource.load(), true)
                }
            }
        )
        add(
            Application::alexaId,
            Text(label = tr("Alexa Skill ID"))
        )
        add(
            Application::phoneNumber,
            Text(label = tr("Phone Number"))
        )
        add(
            Application::icon,
            Text(label = tr("Icon URL")),
            validatorMessage = { "Input a valid URL. Only PNG files are allowed." }
        ) {
            val url = it.getValueAsString() ?: ""
            (url.endsWith(".png") && urlRegex.matches(url)) || url == ""
        }
        add(
            Application::skimmerConfigUrl,
            Text(label = tr("Skimmer Config URL"))
        )
        add(
            Application::public,
            CheckBox(label = tr("Public"))
        )
        add(
            Application::anonymousAccessAllowed,
            CheckBox(label = tr("Anonymous Access Allowed"))
        )
    }

    override fun load(entity: Application): Application {
        val imageComponent = SimplePanel {
            padding = UiHelpers.spacing
            width = 128.px
            height = 128.px
        }
        imageComponent.background = Background(
                image = entity.icon,
                size = BgSize.CONTAIN,
                repeat = BgRepeat.NOREPEAT,
                positionX = 50.perc,
                positionY = 50.perc
        )
        // this.add(imageComponent)
        return super.load(entity)
    }
}