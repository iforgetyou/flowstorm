package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.DialogueMixin
import ai.flowstorm.common.resources.EntityResource
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object DialogueMixinResource : EntityResource<DialogueMixin>(serializer = DialogueMixin.serializer()) {

    init {
        AppState.sub { it.spaceId }.onEach { clear() }.launchIn(App.scope)
    }

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/dialogueMixins"

    // Entities are always empty since only loading method is this one which does not fill them
    suspend fun load(type: String? = null, language: String? = null) = load().filter {
            (type == null || it.type == type)
                    && (language == null || it.language.isNullOrEmpty() || it.language == language)
        }
}
