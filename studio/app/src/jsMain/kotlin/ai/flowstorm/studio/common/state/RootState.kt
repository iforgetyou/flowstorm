package ai.flowstorm.studio.common.state

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.RootState.Action.*
import ai.flowstorm.studio.common.ui.UiState
import ai.flowstorm.studio.model.UserSpaceRole
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.stateManager.ReducerAction
import ai.flowstorm.common.stateManager.StateReducer
import kotlinx.serialization.Serializable

class RootState {
    @Serializable
    data class State(
        val spaceId: Id? = null,
        val userRoles: List<UserSpaceRole> = listOf(),
        val coreUrl: String = App.config.coreUrl,
        val fileUrl: String = App.config.fileUrl,
        val ui: UiState.State = UiState.State(),
        val routeAssist: Boolean = false,
        val appInitialized: Boolean = false,
    ) {
        val selectedRole get() = userRoles.firstOrNull { it.spaceId == spaceId }
    }

    sealed interface Action : ReducerAction {
        data class LoadLocalState(
            val spaceId: Id?,
            val theme: String?,
            val viewMode: String?,
            val editView: String?,
            val runView: String?,
        ) : Action

        data class InitApp(
            val userRoles: List<UserSpaceRole>,
            val spaceId: Id,
            val language: String
        ) : Action

        object InitDone : Action

        data class SpaceLeft(val spaceId: Id) : Action
        data class SpaceSelected(val spaceId: Id) : Action
        data class SetSpaceUrls(val coreUrl: String, val fileUrl: String) : Action
    }

    object Reducer : StateReducer<State, Action> {
        override fun canReduce(action: ReducerAction) = action is Action

        override fun reduce(state: State, action: Action): State = when (action) {
            is LoadLocalState -> {
                if (state.userRoles.any { it.spaceId == action.spaceId }) {
                    state.copy(spaceId = action.spaceId)
                } else {
                    state
                }.copy(ui = with(state.ui) {
                    copy(
                        theme = action.theme ?: theme,
                        viewMode = action.viewMode ?: viewMode,
                        editView = action.editView ?: editView,
                        runView = action.runView ?: runView,
                        )
                })
            }
            is InitApp -> {
                state.copy(
                    userRoles = action.userRoles,
                    ui = state.ui.copy(language = action.language),
                    spaceId = action.spaceId,
                )
            }
            is InitDone -> {
                state.copy(appInitialized = true)
            }
            is SpaceLeft -> {
                val roles = state.userRoles - state.userRoles.first { it.spaceId == action.spaceId }
                state.copy(
                    userRoles = roles,
                    spaceId = if (state.spaceId == action.spaceId) roles.first().spaceId else state.spaceId
                )
            }
            is SpaceSelected -> state.copy(spaceId = action.spaceId)
            is SetSpaceUrls -> state.copy(coreUrl = action.coreUrl, fileUrl = action.fileUrl)
        }
    }
}
