package ai.flowstorm.studio.resources.space.report

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.BaseReportResource

object ReportResource : BaseReportResource() {
    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/reports/data"
}
