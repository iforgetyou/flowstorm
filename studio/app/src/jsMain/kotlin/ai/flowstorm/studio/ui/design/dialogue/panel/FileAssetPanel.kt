package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.model.Graph.Node
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.studio.ui.fileAssetPreview
import io.kvision.core.onEvent
import io.kvision.html.div
import io.kvision.html.span
import io.kvision.panel.SimplePanel
import io.kvision.panel.simplePanel

class FileAssetPanel(val graphEditor: GraphEditor, nodeProvider: ObjectProvider<Node>) : NodeListPanel(nodeProvider) {

    override fun row(item: Node, index: Int, list: MutableList<Node>): SimplePanel =
        SimplePanel(classes = setOf("object-list-row")) {
            when (item) {
                is Graph.Image -> item.asset
                is Graph.Sound -> item.asset
                else -> null
            }?.let { asset ->
                fileAssetPreview(asset, classes = setOf("object-list-preview"))
                simplePanel(className = "object-list-detail") {
                    draggable = true
                    add(nodeIcon(item))
                    span(asset.label)
                    div("&nbsp;${asset.stringTime}", className = "hint", rich = true)
                }
            }
        }.apply {
            onEvent {
                dragstart = { dragStart(item) }
            }
        }

    private fun dragStart(node: Node) {
        graphEditor.draggedNode = Node.copy(node)
    }
}