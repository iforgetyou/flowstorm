package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.model.MoveParameters
import ai.flowstorm.studio.resources.DeviceResource
import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.id.TempId
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.EntityEditor
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form
import ai.flowstorm.common.ui.form.SelectId
import io.kvision.html.*
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal
import io.kvision.panel.SimplePanel
import io.kvision.panel.simplePanel

class DeviceEditor(deviceForm: DeviceForm, saveEnabled: Boolean, deleteEnabled: Boolean) :
        EntityEditor<Device>(deviceForm, saveEnabled, deleteEnabled, serializer = Device.serializer()) {

    private val spaceIdSelect = SelectId(label = tr("Space"))
    private var movePanel: SimplePanel
    private val moveParameters = MoveParameters(StringId(""))

    init {
        movePanel = simplePanel {
            visible = false
            tag(TAG.H3, tr("Move device to another space")).apply {
                padding = UiHelpers.spacing
            }
            val moveParametersForm = form(serializer = MoveParameters.serializer()) {
                add(MoveParameters::spaceId, spaceIdSelect)
            }
            simplePanel(classes = setOf("button-group")) {
                button(tr("Move"), style = ButtonStyle.PRIMARY).onClick {
                    moveParametersForm.save(moveParameters)

                    mainScope.launch {
                        DeviceResource.MoveParametersModel.move(entity!!._id, moveParameters).let { device ->
                            DeviceResource.replaceEntity(entity as Device, device)
                        }
                    }

                    UiHelpers.parent<Modal>(this)?.hide()
                }
            }
        }
    }

    override fun onload(entity: Device): Device {
        mainScope.launch {
            SpaceResource.load().let { spaceIdSelect.options = UiHelpers.options(it) }
        }
        spaceIdSelect.value = null
        movePanel.visible = (!entity.isNew/* && Admin */)
        return super.onload(entity)
    }
}
