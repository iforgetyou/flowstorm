package ai.flowstorm.studio.model

import kotlinx.serialization.Serializable

@Serializable
class DevicePairing {
    var pairingCode: String? = null
}