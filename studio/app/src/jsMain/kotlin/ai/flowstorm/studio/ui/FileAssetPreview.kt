package ai.flowstorm.studio.ui

import ai.flowstorm.studio.model.File
import io.kvision.core.Container
import io.kvision.html.Div

class FileAssetPreview(
    asset: File? = null,
    classes: Set<String> = setOf(),
    init: (FileAssetPreview.() -> Unit)? = null
) : Div(classes = classes, rich = true) {

    var asset: File? = asset
        get() = field
        set(value) {
            field = value
            content = value?.let { content(it) }
        }

    init {
        content = asset?.let { content(it) }

        @Suppress("LeakingThis")
        init?.invoke(this)
    }

    private fun content(asset: File) = with(asset) {
        when (type.substringBefore("/")) {
            "image" -> """<img src="$source?time=${datetime.toEpochMilliseconds()}" class="preview">"""
            "audio" -> """<audio controls style="width:100%;" preload="auto">
                            <source src="$source?time=${datetime.toEpochMilliseconds()}" preload="none">
                            Your browser does not support the audio element.
                        </audio>"""
            "text" -> {
                if (type.substringAfter("/") == "csv") {
                    source
                } else
                    """<iframe src="$source?time=${datetime.toEpochMilliseconds()}" style="border: 0"></iframe>"""
            }
            "application" -> {
                if (type.substringAfter("/") == "vnd.ms-excel") {
                    source
                } else
                    """<iframe src="$source?time=${datetime.toEpochMilliseconds()}" style="border: 0"></iframe>"""

            }
            else -> """<iframe src="$source?time=${datetime.toEpochMilliseconds()}" style="border: 0"></iframe>"""
        }
    }
}

fun Container.fileAssetPreview(
    asset: File,
    classes: Set<String> = setOf(),
    init: (FileAssetPreview.() -> Unit)? = null,
): FileAssetPreview {
    val preview = FileAssetPreview(asset, classes, init)
    this.add(preview)
    return preview
}
