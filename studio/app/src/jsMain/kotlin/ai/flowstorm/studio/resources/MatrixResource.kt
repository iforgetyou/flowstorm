package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Matrix
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod

object MatrixResource: EntityResource<Matrix>(resourceUri = "/matrix", serializer = Matrix.serializer()) {
    suspend fun generate(entity: Matrix): Matrix {
        return remoteCall("", entity, HttpMethod.POST)
    }
}