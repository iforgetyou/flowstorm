package ai.flowstorm.studio.ui

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.ui.design.entity.EntityDatasetTable
import ai.flowstorm.studio.ui.space.*
import ai.flowstorm.studio.ui.space.DeviceTable as SpaceDeviceTable
import ai.flowstorm.studio.ui.space.CommunityTable as SpaceCommunityTable
import ai.flowstorm.studio.ui.data.*
import ai.flowstorm.studio.model.Space.Role.*
import ai.flowstorm.studio.resources.space.VersionResource
import ai.flowstorm.studio.ui.data.ReportPanel
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.studio.ui.design.*
import ai.flowstorm.studio.ui.design.dialogue.panel.AssistantPanel
import ai.flowstorm.common.*
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.FeatureSwitch.Feature
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.auth.UserService
import ai.flowstorm.common.id.ObjectId
import ai.flowstorm.common.ui.Hints.hint
import ai.flowstorm.common.ui.Icon
import com.github.snabbdom.VNode
import io.kvision.core.Component
import io.kvision.core.Widget
import io.kvision.dropdown.DropDown
import io.kvision.dropdown.ddLink
import io.kvision.dropdown.dropDown
import io.kvision.dropdown.separator
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.navbar.Nav
import io.kvision.navbar.nav
import io.kvision.navbar.navForm
import io.kvision.routing.routing
import io.kvision.state.bind
import io.kvision.state.sub
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class SpacePanel : AppPanel() {

    val mainScope = MainScope()

    var spaceNav: Nav
    override val homeUrl get() = "#!/space"
    lateinit var mainDropDown: DropDown
    lateinit var contextDropDown: DropDown
    lateinit var designDropDown: DropDown
    lateinit var toolsDropDown: DropDown
    lateinit var newDialogueModelLink: Link
    private var dialogueSourceDesigner: DialogueSourceDesigner
    private var rootAdminLinks = mutableSetOf<Link>()
    private val spaceModal = object : Modal(tr("Space Settings"), size = ModalSize.LARGE) {
        private val editor = CurrentSpace()
        init {
            add(editor)
        }

        override fun show(): Widget {
            editor.load(AppState.state.spaceId!!)
            return super.show()
        }
    }

    override fun focus(component: Component) {
        super.focus(component)

        if (component is ContextMenuProvider) {
            contextDropDown.removeAll()
            component.initContextMenu(contextDropDown)
            contextDropDown.removeCssClass("nav-item--disabled")
        } else {
            contextDropDown.addCssClass("nav-item--disabled")
        }
    }

    init {
        navbar.apply {
            nav {
                mainDropDown = dropDown("Flowstorm", forNavbar = true) {
                    hint("MainDropDown", Icon.List, isMain = true)
                    ddLinkSecured("Dashboard", "#!/space")
                    ddLinkSecured("Space Activities", "#!/space/activities", requiredRole = Maintainer)
                    ddLink(tr("Space Settings"), "#")
                        .bind(AppState.sub { it.selectedRole }) {
                            visible = listOf(Admin, Owner).contains(it?.role)
                        }.onClick {
                            spaceModal.apply {
                                show()
                            }
                        }
                    separator()
                    with(rootAdminLinks) {
                        add(ddLinkSecured("Users", "#!/users"))
                        add(ddLinkSecured("Spaces", "#!/spaces"))
                        add(ddLinkSecured("Client Devices", "#!/devices"))
                        add(ddLinkSecured("Community Attributes", "#!/communities", Admin))
                        add(ddLinkSecured("Metrics Reports", "#!/reports", Admin))
                    }

                    ddLink(tr("Documentation"), "https://docs.flowstorm.ai")
                        .apply { setAttribute("target", "_blank") }
                        .bind(AppState.sub { it.selectedRole }) {
                            visible = it != null
                        }
                    ddLink(tr("About"), "#")
                        .onClick {
                            AboutModal.show()
                        }
                }
            }
            spaceNav = nav {
                visible = false
                designDropDown = dropDown(tr("Design"), forNavbar = true) {
                    visible = false
                    hint("DesignDropDown", Icon.List, isMain = true)
                    ddLinkSecured("Dialogues", "#!/space/dialogue")
                    ddLinkSecured("Entities", "#!/space/datasets")
                    ddLinkSecured("Snippets", "#!/space/snippets")
                    ddLinkSecured("Mixins", "#!/space/mixins")
                    ddLinkSecured("Files", "#!/space/assets")
                    ddLinkSecured("Voices", "#!/space/voices")
                    ddLinkSecured("Speech Recognizers", "#!/space/speechRecognizers")
                }
                contextDropDown = dropDown(tr("Model"), forNavbar = true, classes = setOf("context-menu")) {
                    hint("ContextDropDown", Icon.List, isMain = true)
                }
                dropDown(tr("Access"), forNavbar = true) {
                    hint("AccessDropDown", Icon.List, isMain = true)
                    ddLinkSecured("Applications", "#!/space/applications", requiredRole = Maintainer)
                    ddLinkSecured("Terms and Conditions", "#!/space/terms", requiredRole = Maintainer)
                    separator()
                    ddLinkSecured("Session Initiations", "#!/space/initiations", requiredRole = Maintainer)
                    ddLinkSecured("Client Devices", "#!/space/devices", requiredRole = Maintainer)
                    separator()
                    ddLinkSecured("User Assignments", "#!/space/assignments", requiredRole = Maintainer)
                    ddLinkSecured("User Invitations", "#!/space/invitations", requiredRole = Maintainer)
                    ddLinkSecured("User Groups", "#!/space/groups", requiredRole = Maintainer)
                }
                dropDown(tr("Data"), forNavbar = true) {
                    hint("DataDropDown", Icon.List, isMain = true)
                    ddLinkSecured("Sessions", "#!/space/sessions", requiredRole = Maintainer)
                    ddLinkSecured("Dialogue Events", "#!/space/events", requiredRole = Maintainer)
                    ddLinkSecured("Metrics Reports", "#!/space/reports", requiredRole = Maintainer)
                    ddLinkSecured("Community Attributes", "#!/space/communities", requiredRole = Maintainer)
                }
                toolsDropDown = dropDown(tr("Tools"), forNavbar = true) {
                    visible = false
                    hint("ToolsDropDown", Icon.List, isMain = true)
                    ddLinkSecured("Dialogue Bulk Builder", "#!/space/rebuild")
                    ddLinkSecured("Search", "#!/space/search")
                    if (FeatureSwitch.isEnabled(Feature.simulator))
                        ddLinkSecured( "Journey Simulator", "#!/space/simulator")
                }
                dropDown("", icon = Icon.Plus.def, style = ButtonStyle.INFO) {
                    hint("NewDropDown", Icon.Plus, isMain = true)
                    newDialogueModelLink = ddLink(tr("Dialogue"), icon = Icon.DialogueSourceDesigner.def) {
                        visible = false
                    }.onClick {
                        routing.navigate("/space/dialogue/new")
                    }
                    ddLink(tr("File"), icon = Icon.FileAsset.def).onClick {
                        FileAssetModal.show()
                    }
                    separator()
                    ddLink(tr("User Invitation"), icon = Icon.Invitation.def).onClick {
                        InvitationModal.show()
                    }
                    ddLink(tr("User Assignment"), icon = Icon.UserAssignment.def).onClick {
                        Modal(tr("User Assignment"), size = ModalSize.LARGE) {
                            add(UserAssignmentCreator())
                        }.show()
                    }
                    ddLink(tr("Session Initiation"), icon = Icon.Initiation.def).onClick {
                        InitiationModal.show()
                    }
                    ddLink(tr("Device Pairing"), icon = Icon.Device.def).onClick {
                        DevicePairingModal.show()
                    }
                }
            }
            navForm(classes = setOf("navbar-form", "ml-auto")) {
                add(SpaceRoleSwitcher)
            }
            add(usernav)
        }
        body.apply {
            add(Home.route("/space"))

            add(UserTable().route("/users"))
            add(SpaceTable().route("/spaces"))
            add(DeviceTable().route("/devices"))
            add(CommunityTable().route("/communities"))
            add(RootReportPanel().route("/reports"))

            add(UserAssignmentTable().route("/space/assignments"))
            add(InvitationTable().route("/space/invitations"))
            add(UserGroupTable().route("/space/groups"))
            add(ApplicationTable().route("/space/applications"))
            add(SpaceDeviceTable().route("/space/devices"))
            add(TermsTable().route("/space/terms"))

            add(ReportPanel().route("/space/reports"))
            add(SessionTable().route("/space/sessions"))
            add(InitiationTable().route("/space/initiations"))
            add(SpaceCommunityTable().route("/space/communities"))
            add(DialogueEventTable().route("/space/events"))
            add(ActivityTable().route("/space/activities"))

            dialogueSourceDesigner = DialogueSourceDesigner()
                    .route("/space/dialogue/:id")
                    .route("/space/dialogue")
            add(BulkBuilder().route("/space/rebuild"))
            add(SearchPanel().route("/space/search"))
            add(dialogueSourceDesigner)
            add(DialogueMixinTable().route("/space/mixins"))
            add(DialogueSnippetDesigner().route("/space/snippets"))
            add(EntityDatasetTable().route("/space/datasets"))
            add(FileAssetTable().route("/space/assets"))
            add(VoiceTable().route("/space/voices"))
            add(SpeechRecognizerTable().route("/space/speechRecognizers"))
            if (FeatureSwitch.isEnabled(Feature.simulator))
                add(SimulationDesigner().route("/space/simulator"))
        }

        footerSpan.bind(AppState.sub { it.coreUrl }) {
            mainScope.launch {
                content = getFooterText()
            }
        }

        AppState.sub { it.selectedRole }.onEach {
            val spaceRole = it!!
            designDropDown.visible = setOf(Editor, Admin, Owner).contains(spaceRole.role)
            toolsDropDown.visible = setOf(Editor, Admin, Owner).contains(spaceRole.role)
            newDialogueModelLink.visible = setOf(Editor, Owner, Admin).contains(spaceRole.role)
            rootAdminLinks.forEach {
                it.visible = ((spaceRole.role == Admin) && (spaceRole.spaceId == ObjectId(App.config.idMap.rootSpace)))
            }
            spaceNav.visible = setOf(Maintainer, Editor, Owner, Admin).contains(spaceRole.role)
            focus(body.activeChild)
        }.launchIn(mainScope)
    }

    private suspend fun getFooterText() = StringBuilder().apply {
        VersionResource.load().forEach { version ->
            append(" · ${version.name} ${version.value}")
        }
        append(" @ ${AppState.state.coreUrl.replace("https://", "")}")
    }.toString()

    fun loadDialogue(id: Id, nodeId: Int? = null) {
        dialogueSourceDesigner.load(id, nodeId)
        routing.navigate("/space/dialogue")
    }

    override fun afterInsert(node: VNode) {
        if (UserService.context.user.shouldLaunchAssistant) {
            AssistantPanel.assistantPanel.launchBot("#signal", true)
        }
        super.afterInsert(node)
    }

    override val shortcuts: List<Shortcut> = listOf(
            Shortcut(tr("Show about"), "?") { AboutModal.show() },
            Shortcut(tr("Open applications"), "a") { routing.navigate("/space/applications") },
            Shortcut(tr("Open dialogue designer"), "d") { if (designDropDown.visible) routing.navigate("/space/dialogue")},
            Shortcut(tr("Open simulation designer"), "i") { if (designDropDown.visible) routing.navigate("/space/simulator") },
            Shortcut(tr("Pair Device"), "p") { DevicePairingModal.show() },
            Shortcut(tr("Activate last panel"), "!") { routing.navigate(panelHistory[1].first) },
            Shortcut(tr("Toggle navigation bar"), "1") {
                if (body.activeChild !is ModelDesigner<*>)
                    instance.navbar.let {
                        if (it.visible)
                            it.hide()
                        else
                            it.show()
                    }
            },
    )
        get() = field + super.shortcuts
}
