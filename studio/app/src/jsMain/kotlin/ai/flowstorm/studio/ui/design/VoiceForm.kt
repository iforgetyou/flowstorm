package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Enum
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.studio.model.Voice
import ai.flowstorm.studio.resources.space.TtsResource
import ai.flowstorm.studio.resources.space.VoiceResource
import ai.flowstorm.common.id.ObjectId
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.core.tts.TtsRequest
import io.kvision.core.Resize
import kotlinx.coroutines.launch
import io.kvision.core.onEvent
import io.kvision.dropdown.DropDown
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.form.text.textArea
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.modal.Alert

class VoiceForm : EntityForm<Voice>(VoiceResource) {

    val configForm = object : Form<TtsConfig>(TtsConfig.serializer()) {
        init {
            add(
                TtsConfig::provider,
                Text(label = tr("Provider")),
                required = true,
                requiredMessage = tr("Required field")
            )
            add(
                TtsConfig::locale,
                Select(label = tr("Locale"), options = UiHelpers.options(Enum.locales)).apply {
                    onEvent {
                        change = {
                            changePreviewText(value)
                        }
                    }
                },
                required = true,
                requiredMessage = tr("Required field")
            )
            add(
                TtsConfig::gender,
                Select(label = tr("Gender"), options = UiHelpers.options(Enum.genders)),
                required = true,
                requiredMessage = tr("Required field")
            )
            add(
                TtsConfig::name,
                Text(label = tr("Name")),
                required = true,
                requiredMessage = tr("Required field")
            )
            add(
                TtsConfig::engine,
                Text(label = tr("Engine"))
            )
            add(
                TtsConfig::amazonAlexa,
                Text(label = tr("Amazon Alexa"))
            )
            add(
                TtsConfig::googleAssistant,
                Text(label = tr("Google Assistant"))
            )
        }
    }

    private lateinit var previewDiv: Div
    private lateinit var previewButton: Button
    private var previewUrl: TextArea =
        TextArea(rows = 1) {
            disabled = true
            input.resize = Resize.NONE
        }
    private var previewText: TextArea
    private val typeDropdown = Select(listOf("wav" to "wav", "mp3" to "mp3"), name = tr("File type"), value = "mp3")

    init {
        add(
            Voice::name,
            Text(label = tr("Name")),
            required = true,
            requiredMessage = tr("Required field")
        )
        add(
            Voice::description,
            TextArea(label = tr("Description"))
        )
        tag(TAG.H3, tr("Text-To-Speech Configuration"))
        add(configForm)

        add(typeDropdown)

        previewText = textArea()
        if (AppState.state.userRoles.any { it.spaceId == ObjectId(App.config.idMap.rootSpace) })
            add(previewUrl)
        previewButton = button(tr("Try Voice"), Icon.Run.def).apply {
            onClick {
                previewButton.disabled = true
                val ttsConfig = configForm.getData()
                val text = previewText.value!!
                //val taggedText = if (ttsConfig.provider == "Microsoft") "<ssml>$text</ssml>" else text
                val ttsRequest = TtsRequest(ttsConfig, text, true)

                mainScope.launch {
                    try {
                        val ttsResponse = TtsResource.synthesizeType(ttsRequest, typeDropdown.options!![typeDropdown.selectedIndex].first)
                        console.log("ttsResponse", ttsResponse)
                        previewDiv.removeAll()
                        previewUrl.value = ttsResponse.url
                        previewDiv.add(
                            Div(
                                content = """<audio autoplay style="width:100%;">
                    <source src="${ttsResponse.url}" type="${ttsResponse.type}" preload="none">
                    Your browser does not support the audio element.
                </audio>""", rich = true
                            )
                        )
                    } catch (e: Throwable) {
                        Alert.show(tr("Voice Preview"), tr("Error occurred while generating voice preview. Check your sample text."))
                    } finally {
                        previewButton.disabled = false
                    }
                }
            }
        }
        previewDiv = div()
    }

    private fun changePreviewText(locale: String?) {
        previewText.value = when (locale?.substringBefore('_')) {
            "en" -> "The quick brown fox jumps over the lazy dog."
            "cs" -> "Příliš žluťoučký kůň úpěl ďábelské ódy."
            else -> "Test test test."
        }
    }

    override fun load(entity: Voice) = super.load(entity).also {
        configForm.setData(it.ttsConfig)
        changePreviewText(it.ttsConfig.locale)
    }

    override suspend fun save() {
        configForm.save(entity!!.ttsConfig)
        super.save()
    }
}
