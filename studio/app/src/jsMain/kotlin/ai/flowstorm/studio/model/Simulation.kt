package ai.flowstorm.studio.model

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.id.ObjectId
import ai.flowstorm.common.id.newId
import ai.flowstorm.core.type.DEFAULT_LOCATION
import ai.flowstorm.core.type.Location
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
open class Simulation(
    override val _id: Id = newId(),
    var name: String,
    var datetime: Instant? = null,
    var timeSpeed: Int = 10,
    var adaptiveSpeed: Boolean = true,
    var description: String = "",
    var dialogue_id: Id,
    var space_id: Id = AppState.state.spaceId!!,
    var waypoints: MutableList<Waypoint> = mutableListOf()
) : AbstractEntity() {
    @Serializable
    data class Waypoint(
            var name: String,
            var time: Long,
            val location: Location = DEFAULT_LOCATION,
            val attributes: Map<String, @Contextual Any> = mapOf(),
            var timeAsString: String = "+${time / 60}:${time.rem(60).toString().padStart(2, '0')}",
            var attributesAsString: String = attributes.entries.map { (key, value) -> "$key=$value"}.joinToString( "\n" )
    )

    override val label by this::name

    object Factory {
        fun empty() = Simulation(
                name = "new",
                dialogue_id = ObjectId("5eea35e67ef5bd28c909c9fe"),
                datetime = Clock.System.now(),
                waypoints = mutableListOf(
                        Waypoint("at home", 0, Location(50.144, 14.323), attributes = mapOf("boolExample" to true, "intExample" to 42)),
                        Waypoint("at work", 45, DEFAULT_LOCATION),
                        Waypoint("lunch", 180, Location(50.097774447568206, 14.400971082882238), attributes = mapOf("stringExample" to "test"))
                )
        )
    }
}
