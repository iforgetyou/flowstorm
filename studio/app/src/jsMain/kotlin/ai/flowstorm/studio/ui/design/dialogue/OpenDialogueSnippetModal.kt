package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.resources.space.DialogueSnippetResource
import kotlinx.coroutines.*
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.ui.Finder
import io.kvision.core.*
import io.kvision.html.Button
import io.kvision.html.ButtonStyle
import io.kvision.html.Span
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.hPanel
import io.kvision.require

class OpenDialogueSnippetModal(allowOpenAsNew: Boolean = false) : Modal(tr("Load Dialogue Snippet"), size = ModalSize.LARGE, classes = setOf("finder")) {

    class DialogueSnippetItem(label: String, val snippet: DialogueSnippet) : Finder.Item(label)

    private lateinit var snippets: List<DialogueSnippet>
    private val mainScope = MainScope()
    private val open = Job()
    var createNewVersion = false

    private val loading = Span(tr("Loading ..."))

    private val openButton = Button(gettext("Open")).apply {
        disabled = true
        onClick {
            open.complete()
        }
    }

    private val openAsNewButton = Button(gettext("Open as new"), style = ButtonStyle.SECONDARY).apply {
        disabled = true
        onClick {
            createNewVersion = true
            open.complete()
        }
    }

    private val cancelButton = Button(gettext("Cancel"), style = ButtonStyle.SECONDARY).apply {
        onClick { cancel() }
    }

    private val finder = Finder().apply {
        itemTemplates = mapOf(
                "cs" to require("hbs/finderItemDialogueSnippet.cs.hbs"),
                "de" to require("hbs/finderItemDialogueSnippet.de.hbs"),
                "en" to require("hbs/finderItemDialogueSnippet.en.hbs"),
                "es" to require("hbs/finderItemDialogueSnippet.es.hbs")
        )

        selectedItem.subscribe {
            openButton.disabled = (it == null)
            openAsNewButton.disabled = (it == null)
        }
    }

    init {
        onEvent {
            keydown = {
                if (it.key == "Enter" && finder.selectedItem.value != null)
                    open.complete()
            }
        }

        closeIcon.onEvent {
            click = { cancel() }
        }

        hPanel(className = "content-header") {
            style {
                justifyContent = JustifyContent.FLEXEND
            }
        }
        addButton(openButton)
        if (allowOpenAsNew) addButton(openAsNewButton)
        addButton(cancelButton)
    }

    suspend fun open(): DialogueSnippet {
        show()
        load()
        open.join()
        hide()

        val snippet = (finder.selectedItem.value as DialogueSnippetItem).snippet
        return if (createNewVersion)
            snippet.cloneAsNew()
        else
            snippet
    }

    private suspend fun load() {
        remove(finder)
        add(loading)
        val query = Query(limit = 0)
        snippets = DialogueSnippetResource.load(query).sortedWith(compareBy(DialogueSnippet::name))
        finder.items = finderItems(snippets)
        remove(loading)
        add(finder)
        refresh()
    }

    private fun finderItems(snippets: List<DialogueSnippet>): Array<Finder.Item> {
        val items = mutableListOf<Finder.Item>()

        snippets.forEach { snippet ->
            val dialogueItem = DialogueSnippetItem(snippet.name, snippet)
            items.add(dialogueItem)
        }
        return items.toTypedArray()
    }

    private fun cancel() {
        hide()
        mainScope.cancel()
    }
}