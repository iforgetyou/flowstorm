package ai.flowstorm.studio.ui

import ai.flowstorm.common.ui.PagePanel
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.cardPanel
import io.kvision.html.*
import io.kvision.panel.hPanel
import io.kvision.panel.simplePanel

class InvitationPage : PagePanel() {
    init {
        body.apply {
            cardPanel("Invitation", className = "body-panel") {
                addPanelHeader()
                hPanel(className = "content-panel") {
                    simplePanel {
                        margin = UiHelpers.spacing
                        p("You've been invited to Flowstorm application", className = "bigger")
                        span("To accept the invitation please ")
                        link("Log in", "#!/login")
                        span(" or ")
                        link("Sign up", "#!/signup")
                    }
                }
            }
        }
    }
}
