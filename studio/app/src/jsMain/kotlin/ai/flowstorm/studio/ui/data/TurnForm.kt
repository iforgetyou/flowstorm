package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Turn
import ai.flowstorm.studio.ui.form.EntityId
import ai.flowstorm.common.ui.Form
import io.kvision.form.spinner.ButtonsType
import io.kvision.form.spinner.Spinner
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class TurnForm: Form<Turn>(Turn.serializer()) {
    init {
        add(Turn::_id, EntityId(label = tr("ID")))
        add(Turn::timeAsString, Text(label = tr("Time")))
        add(Turn::duration, Spinner(label = tr("Duration"), buttonsType = ButtonsType.NONE))
        add(Turn::endFrameName, Text(label = tr("End frame")))
        add(Turn::locale, Text(label = tr("Locale")))
        add(Turn::transcript, Text(label = tr("Transcript")))
        disableAll()
    }

    fun load(turn: Turn): TurnForm {
        disableAll()
        setData(turn)
        return this
    }
}