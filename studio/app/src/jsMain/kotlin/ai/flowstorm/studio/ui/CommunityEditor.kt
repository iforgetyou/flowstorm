package ai.flowstorm.studio.ui

import ai.flowstorm.common.ui.CardPanel
import ai.flowstorm.common.ui.EntityComponent
import ai.flowstorm.common.ui.JsonEditor
import com.github.snabbdom.VNode
import ai.flowstorm.studio.model.Community
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import io.kvision.panel.SimplePanel
import kotlinx.serialization.json.JsonElement

class CommunityEditor(val model: EntityResource<Community>) :
        CardPanel("Attributes", hideHeader = true), EntityComponent<Community> {
    val mainScope = MainScope()
    override var entity: Community? = null
    val jsonEditor = JsonEditor(JsonElement.serializer(), classes = setOf("panel-view-nav1"))
    val panel = SimplePanel()
    var loaded: Boolean

    init {
        add(panel)
        loaded = false
    }

    override fun load(entity: Community): Community {
        this.entity = entity
        return entity
    }

    override fun load(id: Id): Community {
        return load(model.get(id))
    }

    // Hack to render the editor immediately instead of after resizing
    override fun render(): VNode {
        if (!loaded) {
            mainScope.launch {
                delay(20)
                jsonEditor.setData(entity!!.attributes)
                panel.add(jsonEditor)
                loaded = true
            }
        }
        return super.render()
    }
}
