package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.Graph
import io.kvision.state.ObservableValue

object GraphClipboard {
    var graph = ObservableValue<Graph?>(null)
}