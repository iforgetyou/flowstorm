package ai.flowstorm.studio.ui.design.entity

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.EntityDataset
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityEditor

class EntityDatasetEditor(model: EntityResource<EntityDataset>): EntityEditor<EntityDataset>(EntityDatasetForm(model), serializer = EntityDataset.serializer())  {
    override fun load(id: Id) = super.load(id).also {
        if (it.space_id != AppState.state.spaceId!!) {
            disableAll()
            (entityComponent as EntityDatasetForm).disableRest()
        }
    }
}
