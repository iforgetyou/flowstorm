package ai.flowstorm.studio.ui

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.ui.design.dialogue.panel.LogPanel
import kotlinx.serialization.Serializable
import ai.flowstorm.common.Initiable
import ai.flowstorm.common.RestClient
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.*
import org.w3c.xhr.XMLHttpRequest
import io.kvision.core.Clear
import io.kvision.core.PosFloat
import io.kvision.core.onEvent
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.html.ButtonType
import io.kvision.html.button
import io.kvision.i18n.tr
import io.kvision.panel.simplePanel
import io.kvision.state.bind
import io.kvision.state.sub

class BulkBuilder : CardPanel("Dialogue Bulk Builder"), Initiable,
    Stateful<BulkBuilder.State> by StatefulValue(State())
{
    @Serializable
    data class BuildOptions(
        val search: String = "",
        val verbose: Boolean = false,
        val dryRun: Boolean = true,
    )

    data class State(
        val running: Boolean = false,
        val dryRunOptions: BuildOptions? = null, //last dryrun options
        val options: BuildOptions = BuildOptions(),
    )

    val form = Form(BuildOptions.serializer()).apply {
        add(BuildOptions::search, Text(label = tr("Search"), value = ""))
        add(BuildOptions::verbose, CheckBox(label = tr("Verbose")))
        simplePanel {
            float = PosFloat.RIGHT
            button(tr("Build"))
                .bind(this@BulkBuilder) {
                    disabled = it.running || it.dryRunOptions != it.options
                }
                .onClick {
                    build(getData().copy(dryRun = false))
                }
            button(tr("Dry Run"), type=ButtonType.SUBMIT) {
                marginLeft = UiHelpers.spacing
            }
                .bind(this@BulkBuilder.sub { it.running }) {
                    disabled = it
                }
                .onClick {
                    build(getData().copy(dryRun = true))
                }
            button(tr("Abort")) {
                marginLeft = UiHelpers.spacing
            }
                .bind(this@BulkBuilder.sub { it.running }) {
                    disabled = !it
                }
                .onClick {
                    abort()
                }
        }
        simplePanel {
            clear = Clear.BOTH
        }
        onEvent {
            change = {
                newState { copy(options = getData()) }
            }
            submit = {
                it.preventDefault()
            }
        }
    }

    private var xhr: XMLHttpRequest? = null

    private val url
        get() = App.config.restUrl + "/spaces/${AppState.state.spaceId!!.value}/dialogueSources/bulkbuild"

    private val logPanel = LogPanel()

    init {
        addPanelHeader()
        simplePanel(classes = setOf("content-panel")) {
            simplePanel(classes = setOf("content-form")) {
                add(form)
            }
            add(logPanel)
        }
    }

    override fun init() {
        form.setData(BuildOptions())
    }

    private fun createUri(options: BuildOptions): String {
        val q = Query(
            limit = null,
            filters = mutableListOf(
                Query.Filter(
                    DialogueSource::dialogueName.name,
                    Query.Operator.like,
                    options.search
                )
            ),
        )

        return url + q.toString() + "&verbose=${options.verbose}&dryrun=${options.dryRun}"
    }

    private fun build(options: BuildOptions) {
        abort()
        newState { copy(running = true) }
        with(XMLHttpRequest()) {
            xhr = this
            open("POST", createUri(options), true)
            setRequestHeader("Authorization", "Bearer " + AuthService.context.accessToken.token)
            onprogress = {
                logPanel.clearLog()
                logPanel.log(responseText.split("\n"))
            }
            onloadend = {
                newState { copy(running = false, dryRunOptions = if (options.dryRun) options else null) }
                Unit
            }
            send()
        }
    }

    private fun abort() {
        newState { copy(running = false) }
        xhr?.abort()
        xhr = null
    }
}
