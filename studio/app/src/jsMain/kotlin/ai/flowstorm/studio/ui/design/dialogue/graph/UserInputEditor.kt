package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.ListIdSelect
import ai.flowstorm.studio.model.Graph.UserInput
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.ui.design.MatrixForm
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.studio.ui.form.SpeechRecognizerId
import io.kvision.core.onEvent
import io.kvision.form.check.CheckBox
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.html.Div
import io.kvision.html.Link
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr
import io.kvision.panel.SimplePanel
import kotlinx.coroutines.launch

class UserInputEditor(graphEditor: GraphEditor) : NodeEditor<UserInput>(graphEditor), EditableCode {

    override val codeEditor = CodeEditor(classes = setOf("panel-view-context")).apply {
        onEvent {
            change = {
                if (node!!.code != self.value) {
                    node?.code = self.value
                    graphEditor.commit(
                        gettext("UserInput code changed") + " [#${node!!.id}]",
                        "userInput[${node!!.id}].code"
                    )
                }
            }
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
        }

    private val speechRecognizer = SpeechRecognizerId().apply {
        load(null)
    }

    override val form = object : NodeForm(UserInput.serializer()) {
        init {
            add(
                UserInput::label,
                Text(label = tr("Label")),
            )
            add(UserInput::name, Text(label = tr("Name")),
                required = true,
                validatorMessage = { gettext("Node name must be unique.") },
                validator = { text ->
                    !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                }
            )
            add(
                UserInput::speechRecognizer_id,
                speechRecognizer
            )
            add(
                UserInput::mixins_id,
                ListIdSelect(label = tr("Mixins")).apply {
                    mainScope.launch {
                        options = UiHelpers.options(DialogueMixinResource.load("UserInput"))
                    }
                    onEvent {
                        change = {
                            mainScope.launch {
                                codeEditor.set(codeEditor.value, this@apply.getValue() ?: listOf())
                            }
                        }
                    }
                }
            )
            add(UserInput::skipGlobalIntents, CheckBox(label = tr("Skip Global Intents")))
            add(UserInput::expectedPhrases, TextArea(label = tr("Expected Phrases"), rows = 6))
        }
    }


    val kibanaLink = Link("Traffic in last 7 days", null, target = "_blank")

    val kibanaPanel = SimplePanel {
        add(kibanaLink)
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Code"), codeEditor, Icon.Function.def)
            addTab(tr("C-Matrix"), MatrixForm(form.nodeIdDiv ?: Div(content = ""), graphEditor), Icon.Matrix.def)
            addTab(tr("Kibana"), kibanaPanel)
        }

        override fun tabInit(index: Int) = when (index) {
            1 -> codeEditor.focus()
            else -> super.tabInit(index)
        }
    }

    override fun load(node: UserInput) {
        super.load(node)
        form.load(node)
        kibanaLink.apply {
            visible = graphEditor.dialogueId != null
            url = getKibanaLink()
        }

        codeEditor.set(node.code, node.mixins_id)
    }

    fun getKibanaLink(): String {
        val node = "${graphEditor.dialogueId?.value}%23${node?.id}"
        return "https://kibana.flowstorm.ai/app/dashboards#/view/a5cb9cc0-89a0-11ec-a3d0-276cf7b4a2ab?_a=(filters:!((query:(match_phrase:(input_id.keyword:'$node')))))&_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-7d%2Fd,to:now))"
    }
}
