package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.resources.AbstractResource
import ai.flowstorm.core.tts.TtsRequest
import ai.flowstorm.core.tts.TtsResponse
import io.kvision.rest.HttpMethod

object TtsResource : AbstractResource<TtsRequest>(serializer = TtsRequest.serializer()) {

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/tts"

    suspend fun synthesize(ttsRequest: TtsRequest): TtsResponse =
        remoteCall("/synthesize", ttsRequest, HttpMethod.POST, TtsResponse.serializer(), doToast = false)

    suspend fun synthesizeType(ttsRequest: TtsRequest, fileType: String): TtsResponse =
        remoteCall("/synthesize/${fileType}", ttsRequest, HttpMethod.POST, TtsResponse.serializer(), doToast = false)

}
