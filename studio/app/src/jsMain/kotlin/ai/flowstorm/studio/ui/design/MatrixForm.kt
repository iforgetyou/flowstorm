package ai.flowstorm.studio.ui.design

import com.github.snabbdom.VNode
import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.resources.MatrixResource
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import ai.flowstorm.common.ExampleExpander
import ai.flowstorm.common.Toast
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.form.check.CheckBox
import io.kvision.form.select.Select
import io.kvision.form.spinner.Spinner
import io.kvision.form.text.Text
import io.kvision.html.Button
import io.kvision.html.Div
import io.kvision.i18n.gettext
import io.kvision.i18n.tr
import io.kvision.utils.obj
import kotlin.random.Random

class MatrixForm(val nodeIdDiv: Div, val graphEditor: GraphEditor) : Form<MatrixRequest>(MatrixRequest.serializer()) {

    @Serializable
    data class Example(val answer: String, val questions: Array<String>, val threshold: Float)

    val expander = ExampleExpander()
    val modal = MatrixModal()

    init {
        padding = UiHelpers.spacing
        marginRight = UiHelpers.spacing
        add(
                MatrixRequest::decision_node,
                Spinner(label = tr("Node key")).apply {
                    disabled = true
                }
        )
        add(
                MatrixRequest::name,
                Text(label = tr("Model Name")).apply {
                    disabled = true
                }
        )
        add(
                MatrixRequest::lang,
                Select(label = tr("Language"), options = UiHelpers.options(Enum.languages), value = "en")
        )
        add(
                MatrixRequest::algorithm,
                Select(
                    label = tr("Algorithm"),
                    options = UiHelpers.options(MatrixRequest.Algorithm.values()),
                    value = "FastText"
                )
        )
        add(
                MatrixRequest::use_tfidf,
            CheckBox(label = tr("Use TF-IDF"), value = false)
        )
        add(Button(tr("Open")).onClick {
            modal.showLoading()
            mainScope.launch {
                val matrix = MatrixResource.generate(getRequestData())
                modal.addMatrix(
                        matrix.data ?: "{}",
                        matrix.num_possible_classes ?: 1,
                        matrix.accuracy
                                ?: 0.0,
                        matrix.oov ?: arrayOf())
            }
            modal.show()
        })
    }

    override fun render(): VNode {
        val nodeKey = nodeIdDiv.content?.toInt()
        form.fields["decision_node"]?.setValue(nodeKey)
        //TODO why we need a name??
//        form.fields["name"]?.setValue(dialogueEditor.entity?.id.toString())
        form.fields["name"]?.setValue(Random.Default.nextBytes(10).toString())
        return super.render()
    }

    fun getRequestData(): Matrix {
        val entity = form.getData()
        return Matrix(
            model_params = entity,
            decision_node = entity.decision_node,
            qaString = convertToExamples(entity.decision_node)
        )
    }

    fun convertToExamples(nodeKey: Int): String {
        var result = obj {}
        graphEditor.graph.globalIntents.forEach {
            result[it.name] = Example(it.name, expandUtterances(it.utterances)!!, it.threshold)
        }
        getNodeChildren(nodeKey).forEach {
            result[it.answer] = it
        }
        return JSON.stringify(result)
    }

    fun getNodeChildren(nodeKey: Int): List<Example> {
        val result = mutableListOf<Example>()
        val childrenIds = graphEditor.graph.links.filter { it.from == nodeKey }.map { it.to }
        val intentChildren = graphEditor.graph.nodes.filter {
            (childrenIds).contains(it.id) && it is Graph.Intent
        }
        intentChildren.forEach {
            result.add(Example(it.name, expandUtterances((it as Graph.Intent).utterances)!!, it.threshold))
        }
        return result
    }

    fun expandUtterances(utterances: List<String>): Array<String>? = try {
        val mapped = utterances.map { expander.expand(it) }
        mapped.flatten().toTypedArray()
    } catch (e: Exception) {
        Toast.error(e.message ?: gettext("Error in expanding utterances"))
        null
    }
}