package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Utterance
import ai.flowstorm.common.resources.EntityResource
import io.kvision.state.ObservableList

class UtteranceResource(entities: ObservableList<Utterance>) : EntityResource<Utterance>(serializer = Utterance.serializer(), entities = entities)