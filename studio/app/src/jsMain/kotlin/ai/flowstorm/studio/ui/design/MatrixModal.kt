package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.ui.design.dialogue.panel.MatrixPanel
import io.kvision.html.TAG
import io.kvision.html.Tag
import io.kvision.i18n.gettext
import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.SimplePanel

class MatrixModal: Modal(tr("Confusion matrix")) {
    private val loading = Tag(TAG.H4, tr("Loading..."))
    var panel = SimplePanel()
    init {
        size = ModalSize.XLARGE
        panel.add(loading)
        add(panel)
    }


    fun addMatrix(data: String, numOfClasses: Int, accuracy: Double, oov: Array<String>) {
        panel.remove(loading)
        panel.add(MatrixPanel(data, numOfClasses))
        panel.add(Tag(TAG.H4, gettext("One-left accuracy is ") + accuracy))
        panel.add(Tag(TAG.H4, gettext("OOV words: ") + oov.joinToString(", ")))
    }

    fun showLoading() {
        panel.removeAll()
        panel.add(loading)
    }
}