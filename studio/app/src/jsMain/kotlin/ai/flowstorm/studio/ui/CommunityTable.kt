package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.CommunityResource
import ai.flowstorm.common.setProperty
import ai.flowstorm.common.ui.EntityTable
import ai.flowstorm.studio.model.Community
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.obj
import io.kvision.utils.px

open class CommunityTable(
        className: String? = "content-panel"
) : EntityTable<Community>(
        // EntityTable adds "s" to the caption
        "Communitie",
        CommunityResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), Community::name.name, width = "40%"),
                        ColumnDefinition(tr("Space"), Community::space_id.name, width = "60%")
                )
        ),
        { CommunityEditor(CommunityResource) },
        splitMode = true,
        hideHeader = false,
        tabWidth = 400.px,
        className = className,
        dataExtractor = {
                obj {
                        setProperty(this, it::name)
                }
        },
)
