package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Metric
import ai.flowstorm.common.resources.EntityResource

object ReportMetricResource : EntityResource<Metric>(serializer = Metric.serializer()) {

    override val resourceUri: String
        get() = "/reports/metrics"

}
