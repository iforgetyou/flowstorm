package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.ObjectList
import io.kvision.core.Cursor
import io.kvision.core.onEvent
import io.kvision.html.icon
import io.kvision.html.span
import io.kvision.panel.SimplePanel
import io.kvision.utils.px

class DialogueSnippetPanel(val graphEditor: GraphEditor, provider: ObjectProvider<DialogueSnippet>) :
    ObjectList<DialogueSnippet>(provider) {

    override fun row(item: DialogueSnippet, index: Int, list: MutableList<DialogueSnippet>): SimplePanel {
        return SimplePanel(classes = setOf("object-list-row")) {
            cursor = Cursor.POINTER
            draggable = true
            icon(Icon.Graph.def) {
                padding = 3.px
            }

            span(item.label)
        }.apply {
            onEvent {
                dragstart = { dragStart(item) }
            }
        }
    }

    private fun dragStart(snippet: DialogueSnippet) {
        graphEditor.draggedGraph = snippet.graph.clone()
    }

    override fun filter(item: DialogueSnippet, search: String): Boolean = item.name.lowercase().contains(search.lowercase())
}