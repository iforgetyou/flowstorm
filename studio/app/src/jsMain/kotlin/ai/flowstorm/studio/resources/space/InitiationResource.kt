package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Initiation
import ai.flowstorm.common.resources.EntityResource

object InitiationResource : EntityResource<Initiation>(serializer = Initiation.serializer()) {

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/initiations"
}
