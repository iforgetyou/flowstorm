package ai.flowstorm.studio.ui.design.entity

import ai.flowstorm.studio.resources.DataSampleResource
import ai.flowstorm.studio.model.EntityDataset
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.modal.ModalSize
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.PaginationMode
import io.kvision.tabulator.TabulatorOptions

class SamplesTable(dataSampleModel: DataSampleResource) : EntityTable<EntityDataset.DataSample>(
        "Data Sample",
        dataSampleModel,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                pagination = PaginationMode.LOCAL,
                paginationSize = 50,
                columns = listOf(
                        ColumnDefinition(tr("Text"), EntityDataset.DataSample::text.name)
                )
        ),
        { SamplesForm(dataSampleModel) },
        entityFactory = { EntityDataset.DataSample() },
        modalSize = ModalSize.XLARGE
)