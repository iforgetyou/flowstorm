package ai.flowstorm.studio.ui

import ai.flowstorm.common.Toast
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.CardPanel
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.studio.model.SearchResult
import ai.flowstorm.studio.resources.SearchResource
import io.kvision.core.*
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.panel.simplePanel
import io.kvision.state.bind
import io.kvision.utils.px
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable

open class SearchPanel(val singleSpace: Boolean = true) : CardPanel("Search"),
    Stateful<SearchPanel.State> by StatefulValue(State()) {

    val mainScope = MainScope()

    data class State(
        val loading: Boolean = false,
        val data: List<SearchResult> = listOf(),
        val formData: FormData = FormData(""),
    )

    @Serializable
    data class FormData(
        val search: String = "",
        val entity: String = "dialogue",
        val type: String = "Fulltext",
    )

    val form = Form(FormData.serializer()).apply {
        add(FormData::search, Text(label = tr("Search"), value = ""), required = true)
        add(
            FormData::entity,
            Select(
                label = tr("Entity"),
                value = state.formData.entity,
                options = UiHelpers.options(arrayOf("dialogue", "dialogueMixin", "dialogueSnippet"))
            )
        )
        add(
            FormData::type,
            Select(
                label = tr("Type"),
                value = state.formData.type,
                options = UiHelpers.options(arrayOf("Exact", "Fulltext"))
            )
        )
        simplePanel {
            float = PosFloat.RIGHT
            button(tr("Search")).bind(this@SearchPanel) {
                disabled = it.loading
                text = if (it.loading) tr("Searching") else tr("Search")
            }.onClick {
                search(getData())
            }
        }
        simplePanel {
            clear = Clear.BOTH
        }
        onEvent {
            change = {
                newState { copy(formData = getData()) }
            }
            submit = {
                it.preventDefault()
            }
        }
    }


    init {
        addPanelHeader()
        simplePanel(classes = setOf("content-panel")) {
            add(form)
            simplePanel().bind(this@SearchPanel) { s: State ->
                padding = 12.px
                if (s.loading == false) {
                    h2(tr("Results") + " (found ${s.data.size})")
                    s.data.forEach {
                        simplePanel {
                            b {
                                background = Background(color = Color("#f0f4f7"))
                                icon(iconDef(it.entity))
                                span(it.name)
                                span(" ")
                                span("(score:${it.score})")
                            }
                            p {
                                span { +it.data.toString() }
                            }
                            if (it.highlights.isNotEmpty()) {
                                p {
                                    it.highlights.forEach {
                                        p {
                                            b(it.path + " ")
                                            it.texts.forEach {
                                                span(it.value) {
                                                    color = if (it.type == "hit") Color("red") else Color("black")
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


//            table {
//                s.data.forEach {
//                    tr {
//                        td(it.id.value)
//                        td(it.entity)
//                        td(it.name)
//                        td(it.score.toString())
//                    }
//                }
//
//            }
            }
        }

    }

    fun iconDef(entity: String): String {
        return when (entity) {
            "dialogue" -> Icon.Graph.def
            "dialogueMixin" -> Icon.DialogueMixin.def
            "dialogueSnippet" -> Icon.Graph.def //fixme
            else -> error("")

        }
    }

    fun search(data: FormData) = mainScope.launch {
        newState { copy(loading = true, data = emptyList()) }

        try {
            val res = SearchResource.search(SearchResource.SearchRequest(search = data.search, data.entity, data.type))
            newState { copy(loading = false, data = res) }
        } catch (t: Throwable) {
            Toast.error("Search failed.")
            newState { copy(loading = false, data = emptyList()) }
        }
    }

    lateinit var subscriber: () -> Unit

}
