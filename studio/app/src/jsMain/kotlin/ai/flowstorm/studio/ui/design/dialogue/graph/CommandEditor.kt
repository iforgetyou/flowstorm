package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph.Command
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import kotlinx.coroutines.launch
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.SelectId
import io.kvision.core.onEvent
import io.kvision.form.text.Text
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr

class CommandEditor(graphEditor: GraphEditor) : NodeEditor<Command>(graphEditor), EditableCode {

    override val codeEditor = CodeEditor("flowstorm", setOf("panel-view-context")).apply {
        onEvent { change = { node?.code = self.value } }
    }
    override val form = object : NodeForm(Command.serializer()) {
        init {
            add(Command::name, Text(label = tr("Name")),
                    required = true,
                    validatorMessage = { gettext("Node name must be unique.") },
                    validator = { text ->
                        !graphEditor.graph.nodes.any { it.name == text.value && it != node }
                    }
            )
            add(
                    Command::command,
                    Text(label = tr("Command")),
                    required = true
            )
            add(
                    Command::mixin_id,
                    SelectId(label = tr("Mixin")).apply {
                        mainScope.launch {
                            options = UiHelpers.options(DialogueMixinResource.load("Command"))
                        }
                        onEvent {
                            change = {
                                mainScope.launch {
                                    codeEditor.set(codeEditor.value, listOf(this@apply.getValue() ?: StringId("")))
                                }
                            }
                        }
                    }
            )
        }
    }

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            codeEditor.readOnly = value
        }
    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
            addTab(tr("Payload"), codeEditor, Icon.Payload.def)
            activeIndex = 1
        }

        override fun tabInit(index: Int) = when (index) {
            1 -> codeEditor.focus()
            else -> super.tabInit(index)
        }
    }

    override fun load(node: Command) {
        super.load(node)
        form.load(node)
        val mixinList = mutableListOf<Id>()
        if (node.mixin_id != null)
            mixinList.add(node.mixin_id)
        codeEditor.set(node.code, mixinList)
    }
}