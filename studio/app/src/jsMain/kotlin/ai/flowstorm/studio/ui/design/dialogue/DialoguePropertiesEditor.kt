package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.Enum
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.ui.form.SpeechRecognizerId
import ai.flowstorm.studio.ui.form.VoiceId
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Form
import ai.flowstorm.common.ui.PropertiesEditor
import ai.flowstorm.common.ui.UiHelpers.options
import ai.flowstorm.common.ui.form.ListIdSelect
import io.kvision.core.onEvent
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class DialoguePropertiesEditor(val codeEditor: CodeEditor) : PropertiesEditor<DialogueSource>() {

    inner class PropertiesForm : Form<DialogueSource>(DialogueSource.serializer()) {

        private val speechRecognizer = SpeechRecognizerId().apply {
            load(null)
        }

        private val voice = VoiceId().apply {
            load(null)
        }

        init {
            add(
                DialogueSource::dialogueName,
                Text(label = tr("Name")),
                required = true,
                requiredMessage = tr("Required field")
            )
            add(
                DialogueSource::language,
                Select(label = tr("Language"), options = options(Enum.languages), value = "en").apply {
                    onEvent {
                        change = {
                            voice.load(value)
                        }
                    }
                }
            )
            add(
                DialogueSource::speechRecognizer_id,
                speechRecognizer
            )
            add(
                DialogueSource::voice_id,
                voice
            )
            add(
                DialogueSource::contextClass,
                Text(label = tr("Context Class"))
            )
            add(
                DialogueSource::background,
                Text(label = tr("Background"))
            )
            add(
                DialogueSource::mixins_id,
                ListIdSelect(label = tr("Mixins")).apply {
                    mainScope.launch {
                        options = options(DialogueMixinResource.load("Init"))
                    }
                    onEvent {
                        change = {
                            mainScope.launch {
                                codeEditor.set(codeEditor.value, this@apply.getValue() ?: listOf())
                            }
                        }
                    }
                }
            )
            add(
                DialogueSource::parameters,
                Text(label = tr("Parameters"))
            )
        }
    }

    override val form = PropertiesForm()

    init {
        init()
    }
}