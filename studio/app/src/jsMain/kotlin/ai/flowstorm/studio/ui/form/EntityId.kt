package ai.flowstorm.studio.ui.form

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.form.IdFormControl
import io.kvision.core.Cursor
import io.kvision.core.onEvent
import io.kvision.form.FieldLabel
import io.kvision.form.InvalidFeedback
import io.kvision.form.text.TextInput
import io.kvision.form.text.TextInputType
import io.kvision.html.Button
import io.kvision.panel.SimplePanel
import io.kvision.state.ObservableState
import io.kvision.state.ObservableValue
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

open class EntityId(
    type: TextInputType = TextInputType.TEXT, value: String? = null, name: String? = null,
    label: String? = null, rich: Boolean = false
) : SimplePanel(setOf("form-group")), IdFormControl, ObservableState<String?> {

    val mainScope = MainScope()

    override var disabled: Boolean = false

    var nodeId: Int? = null
    private val _value = ObservableValue<Id?>(null)

    fun onChange(lambda: (Id?) -> Unit) = _value.subscribe(lambda)

    override var value: Id?
        get() = _value.value
        set(value) {
            mainScope.launch {
                _value.value = value
                if (value != null) {
                    input.value = textValue(value)
                } else {
                    input.value = ""
                }
            }
        }

    protected val idc = "kv_form_entity_${counter}"

    final override val input: TextInput = TextInput(type, value).apply {
        this.id = idc
        this.name = name
        this.readonly = true
        this.cursor = Cursor.POINTER
        onEvent {
            keydown = {
                if (it.key == "Enter")
                    mainScope.launch {
                        openEntity()
                    }
            }
            click = {
                mainScope.launch {
                    openEntity()
                }
            }
        }
    }

    protected val linkButton = Button("", icon = "fas fa-external-link-alt").apply {
        visible = false
    }
    open suspend fun openEntity() {}
    open suspend fun textValue(value: Id) = value.value

    final override val flabel: FieldLabel = FieldLabel(idc, label, rich, setOf("control-label"))
    final override val invalidFeedback: InvalidFeedback = InvalidFeedback().apply { visible = false }

    companion object {
        internal var counter = 0
    }

    init {
        @Suppress("LeakingThis")
        input.eventTarget = this
        this.addInternal(flabel)
        this.addInternal(input)
        this.addInternal(invalidFeedback)
        add(linkButton)
        counter++
    }

    override fun focus() {
        input.focus()
    }

    override fun blur() {
        input.blur()
    }

    override fun getState(): String? = input.getState()

    override fun subscribe(observer: (String?) -> Unit): () -> Unit {
        return input.subscribe(observer)
    }
}