package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Initiation
import ai.flowstorm.studio.resources.space.InitiationResource
import kotlinx.coroutines.launch
import ai.flowstorm.common.ErrorHandler
import ai.flowstorm.common.ui.AppPanel
import io.kvision.html.ButtonStyle
import io.kvision.html.button
import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.simplePanel

object InitiationModal : Modal(tr("Session Initiation"), size = ModalSize.LARGE) {

    private val form = InitiationForm()

    init {
        add(form)
        simplePanel(classes = setOf("button-group")) {
            button(tr("Create"), style = ButtonStyle.PRIMARY).onClick {
                with(form) {
                    mainScope.launch {
                        if (form.validate()) {
                            Initiation(space_id = AppState.state.spaceId!!).let {
                                save(it)
                                try {
                                    InitiationResource.create(it)
                                    this@InitiationModal.hide()
                                    AppPanel.instance.reload()
                                } catch (t: Throwable) {
                                    ErrorHandler.handle(t)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
