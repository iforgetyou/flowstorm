package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Initiation
import ai.flowstorm.studio.resources.space.InitiationResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

class InitiationTable : EntityTable<Initiation>(
        "Session Initiation",
        InitiationResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), Initiation::name.name, width = "40%"),
                        ColumnDefinition(tr("Time"), Initiation::stringTime.name, width = "40%"),
                        ColumnDefinition(tr("Device Type"), Initiation::deviceTypeTranslated.name, width = "30%")
                )
        ),
        { InitiationForm() },
        { Initiation(space_id = AppState.state.spaceId!!) },
        deleteEnabled = false,
        splitMode = true,
        tabWidth = 500.px,
        saveEnabled = false,
        className = "content-panel"
)
