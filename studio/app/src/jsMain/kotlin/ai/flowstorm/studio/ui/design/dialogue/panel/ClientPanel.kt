package ai.flowstorm.studio.ui.design.dialogue.panel

import kotlinx.browser.localStorage
import ai.flowstorm.common.ui.PropertiesEditor
import ai.flowstorm.common.ui.form
import ai.flowstorm.common.ui.form.LocationRef
import ai.flowstorm.core.ClientAttributes
import io.kvision.core.Display
import io.kvision.core.onEvent
import io.kvision.form.check.CheckBox
import io.kvision.form.spinner.Spinner
import io.kvision.form.text.Text
import io.kvision.form.text.TextAreaInput
import io.kvision.form.text.textAreaInput
import io.kvision.i18n.tr
import io.kvision.panel.StackPanel
import io.kvision.utils.perc

class ClientPanel : StackPanel(false, setOf("panel-view-nav1")) {

    var attributes = ClientAttributes()
    lateinit var textInput: TextAreaInput
    val editor = object : PropertiesEditor<ClientAttributes>(emptySet()) {

        override val form = form(ClientAttributes.serializer()) {
            add(
                ClientAttributes::clientType,
                Text(label = tr("Type")),
                required = true,
                requiredMessage = tr("Required field")
            )
            add(
                ClientAttributes::clientScreen,
                CheckBox(label = tr("Screen"))
            )
            add(
                ClientAttributes::clientLocation,
                LocationRef(store = { store() })
            )
            add(
                ClientAttributes::clientTemperature,
                Spinner(label = tr("Temperature"), min = -50.0, max = 50.0, step = 0.5, decimals = 1)
            )
            add(
                ClientAttributes::clientAmbientLight,
                Spinner(label = tr("Ambient Light"), min = 0.0, max = 1.0, step = 0.1, decimals = 1)
            )
            add(
                ClientAttributes::clientSpatialMotion,
                Spinner(label = tr("Spatial Motion"), min = 0.0, max = 1.0, step = 0.1, decimals = 1)
            )
            add(
                ClientAttributes::clientSpeechAngle,
                Spinner(label = tr("Speech Angle"), min = -1.0, max = 359.0, step = 1.0, decimals = 0)
            )
            onEvent {
                change = { store() }
            }
        }

        fun store() {
            form.getData().copyTo(attributes)
            textInput.value = attributes.toText()
            attributes.store()
        }
    }

    private fun ClientAttributes.store() = localStorage.setItem("clientAttributesText", toText())

    init {
        attributes.fromText(localStorage.getItem("clientAttributesText") ?: "")
        editor.init()
        editor.load(attributes)
        add(editor)
        textInput = textAreaInput(classes = setOf("comment", "textarea-panel"), value = attributes.toText()) {
            display = Display.INLINE
            height = 100.perc
            onEvent {
                input = {
                    val text = this@textAreaInput.value ?: ""
                    attributes.fromText(text).store()
                    editor.form.setData(attributes)
                }
            }
        }
    }
}