package ai.flowstorm.studio.model

import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.model.Labeled
import kotlinx.serialization.Serializable

@Serializable
data class Namespace(val namespace: String) : HasId, Labeled {
    override val id get() = StringId(namespace)
    override val label by this::namespace
}