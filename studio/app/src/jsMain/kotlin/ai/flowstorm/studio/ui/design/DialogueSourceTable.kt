package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.resources.space.DialogueSourceResource
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.common.ui.DialogueSourceFilterForm
import ai.flowstorm.common.ui.EntityTable
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.i18n.I18n.tr

class DialogueSourceTable : EntityTable<DialogueSource>(
        caption = "Dialogue Model",
        model = DialogueSourceResource,
        tabulatorOptions = TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), DialogueSource::label.name, width = "20%"),
                        ColumnDefinition(tr("State"), DialogueSource::state.name, width = "10%"),
                        ColumnDefinition(tr("Description"), DialogueSource::description.name, width = "30%"),
                        ColumnDefinition(tr("Model ID"), "_id.value", width = "20%"),
                        ColumnDefinition(tr("Last Build ID"), DialogueSource::lastBuild.name, width = "20%")
                )
        ),
        entityComponentFactory = { DialogueSourceForm() },
        entityFactory = { DialogueSource() },
        filterFormFactory = { DialogueSourceFilterForm() },
        createEnabled = false,
        deleteEnabled = false,
        className = "content-panel"
)