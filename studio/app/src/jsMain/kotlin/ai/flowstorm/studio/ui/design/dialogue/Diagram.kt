package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.model.Graph.*
import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import io.kvision.state.sub
import org.w3c.dom.Audio
import io.kvision.utils.JSON.toObj
import io.kvision.utils.obj
import kotlin.math.sign
import org.w3c.dom.Node as DomNode

class Diagram() : Stateful<Diagram.State> by StatefulValue(State()) {
    private lateinit var graph: Graph

    val go = js("go")

    val audio = Audio()
    var isPlaying = false
    var nodeId = 0
    var zOrder = 1

    val goGraph by lazy {
        @Suppress("UNUSED_VARIABLE")
        val goJsKey = App.config.goJsKey
        with(js("new Graph(goJsKey)")) {
            handleClick ={ e: dynamic, obj:dynamic -> handleClick(e, obj) }
            init()
        }
    }

    // The values are defined at graph.js, lines 4-5
    val gridSize by lazy {
        GridSize(goGraph.gridSize.x, goGraph.gridSize.y)
    }

    val position: Point
        get() = Point(goGraph.diagram.position.x, goGraph.diagram.position.y)

    var readOnly: Boolean
        get() = state.readOnly
        set(value) = newState { copy(readOnly = value) }

    data class State(
        val readOnly: Boolean = true,
        val selection: Graph? = null
    ) {
        val selectedItem
            get() = selection?.takeIf { it.size == 1 }?.let { it.nodes.firstOrNull() ?: it.links.firstOrNull() }
    }

    private val modelChangeListeners = mutableListOf<() -> Unit>()

    /**
     * Avoid catching mouse-up events when event not started in diagram
     * see https://forum.nwoods.com/t/gojs-diagram-eating-mouse-up-event/8136/11
     */
    var mouseOnDiagram = false

    init {
        with(goGraph.diagram) {
            initialContentAlignment = go.Spot.Center
            scrollsPageOnFocus = false
            padding = 500
            allowDrop = true
            isReadOnly = true
            allowClipboard = false
            undoManager.isEnabled = true
            undoManager.maxHistoryLength = 0
            commandHandler.archetypeGroupData = obj {
                label = "Group"
                isGroup = true
            }
            grid.visible = false
            toolManager.hoverDelay = 500
            with(toolManager) {
                with(draggingTool) {
                    isGridSnapEnabled = true
                    isCopyEnabled = false
                }
                resizingTool.isGridSnapEnabled = true
                linkingTool.temporaryLink.routing = go.Link.Orthogonal
                relinkingTool.temporaryLink.routing = go.Link.Orthogonal

                doMouseDown = fun () {
                    go.ToolManager.prototype.doMouseDown.call(this)
                    mouseOnDiagram = true
                };

                doMouseUp = fun () {
                    go.ToolManager.prototype.doMouseUp.call(this)
                    mouseOnDiagram = false
                }
            }
            doFocus = fun() {
                if (mouseOnDiagram) this.focus()
            }

            addDiagramListener("ChangedSelection", ::changedSelection)
            addDiagramListener("LinkDrawn", ::nameLink)
            addDiagramListener("LinkRelinked", ::nameLink)

            model = js("new go.GraphLinksModel()")
            model.linkFromPortIdProperty = "fromPort"
            model.linkToPortIdProperty = "toPort"
            model.nodeKeyProperty = "id"
            model.addChangedListener(::modelChanged)

            val d  = this
            val ch = commandHandler

            commandHandler.doKeyDown = fun() {
                val e = d.lastInput
                val control = e.control || e.meta
                val key = e.key
                if (control && key == "C" ) {
                    ctrlCHandler()
                    return
                }
                if (control && key == "V" ) {
                    ctrlVHandler()
                    return
                }

                // call base method with no arguments (default functionality)
                go.CommandHandler.prototype.doKeyDown.call(ch);
            }
        }

        sub { it.readOnly }.subscribe {
            with(goGraph.diagram) {
                startTransaction(TRANSACTION_TOGGLE_READONLY)
                isReadOnly = it
                grid.visible = !it
                commitTransaction(TRANSACTION_TOGGLE_READONLY)
            }
        }
    }

    lateinit var ctrlCHandler: () -> Unit
    lateinit var ctrlVHandler: () -> Unit

    fun onCtrlC(handler: () -> Unit) { ctrlCHandler = handler}
    fun onCtrlV(handler: () -> Unit) { ctrlVHandler = handler}

    fun clear() {
        with(goGraph.diagram) {
            startTransaction(TRANSACTION_CLEAR)
            model.nodeDataArray = arrayOf<dynamic>()
            model.linkDataArray = arrayOf<dynamic>()
            commitTransaction(TRANSACTION_CLEAR)
        }
    }

    fun clearSelection() {
        newState { copy(selection = null) }
        goGraph.diagram.clearSelection()
    }

    private fun modelChanged(evt: dynamic) {
        // ignore unimportant Transaction events
        if (!evt.isTransactionFinished) return
        val transaction = evt.`object`
        if (transaction === null) return
        if (transaction.name in listOf(TRANSACTION_LOAD, TRANSACTION_DIV)) return
        var graphChanged = false
        // iterate over all of the actual ChangedEvents of the Transaction
        transaction.changes.each(fun(e: dynamic) {
            if (e.model == null) {
                //ignore changes other then model
                return
            }
            graphChanged = true
            when (e.modelChange) {
                "nodeDataArray" ->
                    // record node insertions and removals
                    when (e.change) {
                        go.ChangedEvent.Insert -> onNodeInsert(e.newValue)
                        go.ChangedEvent.Remove -> onNodeRemove(e.oldValue)
                    }
                "linkDataArray" -> when (e.change) {
                    go.ChangedEvent.Insert -> onLinkInsert(e.newValue)
                    go.ChangedEvent.Remove -> onLinkRemove(e.oldValue)
                }
                else -> when (e.change) {
                    go.ChangedEvent.Property -> onPropertyChanged(e)
                }
            }
        })

        if (graphChanged) modelChangeListeners.forEach { it() }
    }

    fun onModelChange(callback: () -> Unit) {
        modelChangeListeners.add(callback)
    }

    private fun changedSelection(e: dynamic) {
        val selection = e.subject.toArray() as Array<dynamic>
        val newGraph = Graph()
        selection.forEach { obj ->
            if (js("obj instanceof go.Node")) {

                val node = graph.nodes.firstOrNull { it.id == obj.data.id } ?: error("Node not found ${obj.data.id}")
                newGraph.addNode(node)

            } else if (js("obj instanceof go.Link")) {
                val link = graph.links.firstOrNull { it.from == obj.data.from && it.to == obj.data.to }
                    ?: error("Link not found ${obj.data.from}>${obj.data.to}")
                newGraph.addLink(link)
            }
        }
        newState { copy(selection = if (!newGraph.isEmpty()) newGraph else null) }
    }

    private fun onPropertyChanged(e: dynamic) {
        val obj = e.`object`

        //hacky way to detect which object is updated
        if (obj.id != null) { //node
            val node = graph.nodes.firstOrNull { it.id == obj.id } ?: error("Node not found ${obj.id}")
            when (e.propertyName) {
                "loc" -> node.loc = e.newValue
                "group" -> node.groupId = e.newValue
            }
        } else if (obj.from != null) { //link
            val from = if (e.propertyName == "from") e.oldValue else obj.from
            val to = if (e.propertyName == "to") e.oldValue else obj.to

            val link = graph.links.firstOrNull { it.from == from && it.to == to }
                    ?: error("Link not found")
            when (e.propertyName) {
                "points" -> {
                    link.points.clear()
                    e.newValue.each(fun(v: String) {
                        link.points.add(v)
                    })
                }
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun onNodeInsert(node: dynamic) {
        if (node.isGroup) {
            val g = Group(id = node.id, label = node.label).apply { name = graph.newName(this) }
            graph.addNode(g)

        }
    }

    private fun onNodeRemove(node: dynamic) {
        graph.removeNode(node.id)
    }

    private fun onLinkInsert(link: dynamic) {
        val points = mutableListOf<String>()
        link.points.each({ x -> points.add(x) })

        graph.links.add(Link(link.name, link.from, link.to, link.fromPort, link.toPort, points))
    }

    private fun onLinkRemove(link: dynamic) {
        graph.links.removeAll { it.from == link.from && it.to == link.to }
    }

    var diagramDiv
        set(element: DomNode) {
            with(goGraph.diagram) {
                startTransaction(TRANSACTION_DIV)
                div = element
                commitTransaction(TRANSACTION_DIV)
                requestUpdate()
            }
        }
        get() = goGraph.diagram.div

    fun load(graph: Graph) {
        this.graph = graph
        with(goGraph.diagram) {
            startTransaction(TRANSACTION_LOAD)
            model.nodeDataArray = convertNodes(graph.nodes).toTypedArray()
            model.linkDataArray = convertLinks(graph.links).toTypedArray()
            commitTransaction(TRANSACTION_LOAD)
        }

        //restore references to selected items
        val selection = when (state.selectedItem) {
            is Node -> Graph().apply {
                graph.nodes.firstOrNull { it.id == (state.selectedItem as Node).id }?.let { addNode(it) }
            }
            is Link -> Graph().apply {
                graph.links.firstOrNull {
                    val link = state.selectedItem as Link
                    it.from == link.from && it.to == link.to
                }?.let { addLink(it) }
            }
            else -> null
        }

        newState { copy(selection = selection) }

        changeGridMode()
        changeGroupMode()
    }

    fun addNode(node: Node) {
        graph.addNode(node)

        goGraph.diagram.model.commit({ m ->
            m.addNodeData(convertNode(node))
        }, "add node")
    }

    fun setNodeProperty(id: Int, property: String, value: Any) {
        goGraph.diagram.model.commit({ m ->
            val data = m.findNodeDataForKey(id)
            m.set(data, property, value)
        }, "update node property")
    }

    fun setMultipleNodesProperty(ids: List<Int>, property: String, value: Any) {
        goGraph.diagram.model.commit({ m ->
            ids.forEach {
                val data = m.findNodeDataForKey(it)
                m.set(data, property, value)
            }
        }, "update multiple nodes property")
    }

    fun setLinkProperty(from: Int, to: Int, property: String, value: Any) {
        goGraph.diagram.commit({ d ->
            val example = obj { this.from = from; this.to = to }
            val link = d.findLinksByExample(example).first().data
            d.model.set(link, property, value)
        }, "update link property")
    }

    fun selectNode(node: Node, center: Boolean = true, scaleViewport: Boolean = true) {
        with(goGraph.diagram) {
            val part = findNodeForKey(node.id)
            if (scaleViewport)
                scale = 1
            select(part)
            if(center)
                centerRect(part.actualBounds)
        }
    }

    fun selectGraph(graph: Graph, idMap: MutableMap<Int, Int> = mutableMapOf()) {
        val mapEmpty = idMap.isEmpty()
        with(goGraph.diagram) {
            graph.nodes.forEach {
                val node = findNodeForKey(if (mapEmpty) it.id else idMap[it.id])
                node.isSelected = true
            }
        }
    }

    fun selectNodeById(nodeId: Int) {
        val node = graph.nodes.firstOrNull { it.id == nodeId }
        if (node != null) {
            selectNode(node)
        }
    }

    fun bringNodeToFront(node: Node) {
        setNodeProperty(node.id, "zOrder", zOrder)
        zOrder += 1
    }

    fun bringGraphToFront(graph: Graph, idMap: MutableMap<Int, Int> = mutableMapOf()){
        val mapEmpty = idMap.isEmpty()
        val nodeIds = graph.nodes.map { if (mapEmpty) it.id else (idMap[it.id] ?: it.id) }
        this@Diagram.setMultipleNodesProperty(nodeIds, "zOrder", zOrder)
        zOrder += 1
    }

    fun centerOnSelectedNode(){
        (state.selectedItem as? Node)?.let { selectNode(it) }
    }

    private fun setPadding(padding: Int) {
        with(goGraph.diagram) {
            startTransaction("set padding")
            this.padding = padding
            commitTransaction("set padding")
        }
    }

    fun fitGraphToViewport(){
        setPadding(50)
        goGraph.diagram.zoomToFit()
        setPadding(500)
    }

    fun changeGridMode() {
        if (this::graph.isInitialized) {
            val make = js("go.GraphObject.make")
            val color1 = js("getComputedStyle(document.documentElement).getPropertyValue('--edge-llll')")
            val color2 = js("getComputedStyle(document.documentElement).getPropertyValue('--edge-lll')")
            goGraph.diagram.commit({ d ->
                @Suppress("UNUSED_VARIABLE")
                val gridSizeX = gridSize.x
                @Suppress("UNUSED_VARIABLE")
                val gridSizeY = gridSize.y
                d.grid = make(go.Panel, go.Panel.Grid,
                    js("{visible: true, gridCellSize: new go.Size(gridSizeX, gridSizeY), gridOrigin: new go.Point(0, 0)}"),
                    make(go.Shape, "LineH", gridLine(color2, 1, 1)),
                    make(go.Shape, "LineH", gridLine(color1, 1, 5)),
                    make(go.Shape, "LineH", gridLine(color1, 1, 10)),
                    make(go.Shape, "LineV", gridLine(color2, 1, 1)),
                    make(go.Shape, "LineV", gridLine(color1, 1, 5)),
                    make(go.Shape, "LineV", gridLine(color1, 1, 10))
                )
                d.grid.visible = !state.readOnly
                Unit
            }, "update grid settings")
        }
    }

    fun changeGroupMode() {
        if (this::graph.isInitialized) {
            goGraph.diagram.groupTemplate = if (AppState.state.ui.theme == "light-mode") goGraph.diagram.lightGroupTemplate else goGraph.diagram.darkGroupTemplate
        }
    }

    fun zoomIn(){
        goGraph.diagram.commandHandler.increaseZoom(ZOOM_FACTOR)
    }

    fun zoomOut(){
        goGraph.diagram.commandHandler.decreaseZoom(1.0/ZOOM_FACTOR)
    }

    fun resetSize() {
        with(goGraph.diagram) {
            this.position = initialPosition
            scale = 1
        }
    }

    private fun gridLine(_stroke: dynamic, _strokeWidth: Int, _interval: Int): dynamic = obj {
        stroke = _stroke
        strokeWidth = _strokeWidth
        interval = _interval
    }

    private fun transformName(name: String) = name.split("\\s+".toRegex()).joinToString("") {
        it.replaceFirstChar(Char::uppercase)
    }

    private fun convertNodes(nodes: List<Node>): List<dynamic> = nodes.map { convertNode(it) }

    private fun convertNode(it: Node): dynamic {
        val obj = it.toObj(Node.serializer(it))
        obj.category = it::class.simpleName
        obj.isGroup = it is Group
        obj.label = it.label
        obj.name = it.name
        obj.group = it.groupId ?: ""

        return obj
    }

    private fun convertLinks(links: List<Link>) = links.map { convertLink(it) }

    private fun convertLink(link: Link): dynamic = link.toObj()

    fun updateLinks() {
        goGraph.diagram.links.each(fun(link: dynamic) {
            link.invalidateRoute()
        })
    }

    data class Point(val x: Double, val y: Double)
    data class GridSize(val x: Int, val y: Int)

    fun transformViewToDoc(point: Point): Point {
        @Suppress("UNUSED_VARIABLE")
        val x = point.x

        @Suppress("UNUSED_VARIABLE")
        val y = point.y
        val p = goGraph.diagram.transformViewToDoc(js("new go.Point(x, y)"))
        return Point(p.x, p.y)
    }

    fun snapToGrid(point: Point): Point {
        // Round both coordinates to closest grid line coordinate, causing the node to snap to grid
        val xRounded = ((point.x / gridSize.x) + (sign(point.x) * 0.5)).toInt() * gridSize.x
        val yRounded = ((point.y / gridSize.y) + (sign(point.y) * 0.5)).toInt() * gridSize.y
        return Point(xRounded.toDouble(), yRounded.toDouble())
    }

    private fun nameLink(e: dynamic) {
        val link = e.subject
        val node = goGraph.diagram.findNodeForKey(link.data.to)
        goGraph.diagram.model.commit({ m ->
            m.set(link.data, "name", "to" + transformName(node.data.name))
        }, "changed link name")
    }

    fun updateView() {
        goGraph.diagram.requestUpdate()
    }

    private fun setButtonDecoration (type: String, nodeId: Int) {
        setNodeProperty(nodeId, "buttonPictureSource", "/img/icon-$type.png")
    }

    private fun handleClick(e: dynamic, obj: dynamic) {
        val node = obj.part.data
        val isDifferentNode = nodeId != node.id
        if (isPlaying) {
            audio.pause()
            setButtonDecoration("play", nodeId)
            isPlaying = false
            if (isDifferentNode) {
                handleAudioEnded(nodeId)
                handleClick(e, obj)
            }
        } else {
            if (audio.src != node.source) {
                audio.src = node.source as String
                audio.addEventListener("ended", { handleAudioEnded(node.id as Int) })
                nodeId = node.id as Int
            }
            setButtonDecoration("pause", node.id)
            audio.play().then { isPlaying = true }.catch {
                handleAudioEnded (node.id as Int)
            }
        }
    }

    private fun handleAudioEnded (nodeId: Int) {
        setButtonDecoration("play", nodeId)
        isPlaying = false
        audio.removeAttribute("src")
        audio.removeEventListener("ended", { handleAudioEnded(nodeId) })
    }

    companion object {
        // Step in zooming, GoJS default is 1.05
        private const val ZOOM_FACTOR: Double = 1.4
        private const val TRANSACTION_LOAD = "Load graph"
        private const val TRANSACTION_CLEAR = "Clear diagram"
        private const val TRANSACTION_TOGGLE_READONLY = "Toggle readonly"
        private const val TRANSACTION_DIV = "Set digram DIV"
    }
}
