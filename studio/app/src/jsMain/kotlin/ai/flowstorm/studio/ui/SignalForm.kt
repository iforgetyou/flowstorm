package ai.flowstorm.studio.ui

import ai.flowstorm.studio.model.Simulation
import ai.flowstorm.common.ui.Form
import io.kvision.form.text.TextArea
import io.kvision.i18n.tr

class SignalForm : Form<Simulation.Waypoint>(serializer = Simulation.Waypoint.serializer()) {
    init {
        add(
                Simulation.Waypoint::attributesAsString,
            TextArea(label = tr("Signals"))
        )
    }
}