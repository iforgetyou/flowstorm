package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.model.Graph.*
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import io.kvision.core.onEvent
import io.kvision.panel.SimplePanel

class NodeTypePanel(val graphEditor: GraphEditor) : NodeListPanel(
        object : ObjectProvider<Node> {
            override suspend fun getItems() =
                listOf(
                    GlobalAction(),
                    GlobalIntent(),
                    UserInput(),
                    ReInput(),
                    Action(),
                    Intent(),
                    Function(),
                    Command(),
                    Speech(),
                    SubDialogue(),
                    Enter(),
                    GoBack(),
                    Sleep(),
                    Exit(),
                    End()
                )
        }
) {
    override fun row(item: Node, index: Int, list: MutableList<Node>): SimplePanel =
            super.row(item, index, list).apply {
                onEvent {
                    dragstart = { dragStart(item) }
                }
            }

    private fun dragStart(node: Node) {
        graphEditor.draggedNode = Node.copy(node)
    }
}