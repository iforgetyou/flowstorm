package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.resources.space.DialogueSnippetResource
import ai.flowstorm.studio.ui.design.dialogue.*
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Icon
import io.kvision.panel.*
import io.kvision.i18n.I18n.tr
import kotlinx.coroutines.*
import ai.flowstorm.common.ErrorHandler
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.Hints.hint
import ai.flowstorm.common.ui.StatefulModel.State.New
import ai.flowstorm.common.ui.StatefulModel.State.Saved
import io.kvision.core.onEvent
import io.kvision.state.bind
import io.kvision.toolbar.Toolbar
import io.kvision.toolbar.buttonGroup
import io.kvision.toolbar.toolbar

class SnippetEditor : ModelEditor<DialogueSnippet>(classes = setOf("content-panel")), ShortcutProvider,
    Stateful<SnippetEditor.State> by StatefulValue(State()) {

    data class State(
        val modelState: StatefulModel.State = New,
        val label: String = "",
        val saving: Boolean = false,
        val language: String = "en"
    )

    override var entity: DialogueSnippet? = DialogueSnippet()

    override val name get() = "${entity?.label}:"

    override val shortcuts: List<Shortcut> = listOf(
        Shortcut(tr("Open graph editor"), "G") { activateGraphEditorTab() },
        Shortcut(tr("Edit description"), "D") { activateDescriptionTab() },
    )
        get() = field + ((mainComponent.activeTab?.component as? ShortcutProvider)?.shortcuts?: listOf())

    override var readOnly: Boolean
        get() = super.readOnly
        set(value) {
            super.readOnly = value
            listOf<ReadOnly>(graphEditor, descriptionEditor, propertiesEditor).forEach { it.readOnly = value }
        }

    val graphEditor = GraphEditor().apply {
        newState { copy(language = entity?.language ?: language) }
    }

    val descriptionEditor = ModelCodeEditor("markdown").apply {
        onEvent { change = {entity?.description  = self.value} }
    }

    val sourceEditor = JsonEditor(DialogueSnippet.serializer(), classes = setOf("panel-view-nav1"))
    val propertiesEditor = SnippetPropertiesEditor().also {
        it.form.onEvent {
            change = {
                modelState.value = modelState.value
                newState { copy(language = entity!!.language) }
            }
        }
    }

    val graphEditorTab = Tab(tr("Graph"), graphEditor, Icon.Graph.def)
    val propertiesTab = Tab(tr("Properties"), propertiesEditor, Icon.Properties.def)
    val descriptionTab = Tab(tr("Description"), descriptionEditor, Icon.Description.def)
    val sourceTab = Tab(tr("Source"), sourceEditor, Icon.Source.def)

    override var savedVersion = DialogueSnippet.Factory.empty()

    lateinit var toolbar: Toolbar
    lateinit var mainComponent: TabPanel

    init {
        readOnly = false
        modelState.subscribe {
            newState { copy(modelState = it) }
        }
        subscribe {
            graphEditor.newState { copy(language = it.language) }
        }


        simplePanel(classes = setOf("main-panel", "main-panel--snippets-designer")) {
            simplePanel(classes = setOf("panel-toolbar", "panel-view")) {
                toolbar = toolbar(vertical = true) {
                    buttonGroup {
                        toolBarButton(Icon.Save, "Save")
                            .bind(this@SnippetEditor) {
                                disable(it.modelState == Saved || it.saving)
                            }
                            .onClick {
                                mainScope.launch {
                                    save()
                                }
                            }
                            .hint("SaveModelButton", Icon.Save)

                        toolBarButton(Icon.Delete, "Delete")
                            .bind(this@SnippetEditor) {
                                disable(it.modelState == New || it.saving)
                            }
                            .onClick {
                                mainScope.launch {
                                    if (ConfirmModal.Delete().open()) {
                                        UiHelpers.parent<DialogueSnippetDesigner>(this@SnippetEditor)?.removeTabByComponent(this@SnippetEditor)
                                        delete()
                                    }
                                }
                            }
                    }
                }
            }

            mainComponent = tabPanel(classes = setOf("panel-view"), scrollableTabs = true) {
                addAll(listOf(graphEditorTab, propertiesTab, descriptionTab, sourceTab))
            }
        }
    }

    fun activateDescriptionTab() = activateTab(descriptionTab)
    fun activateGraphEditorTab() = activateTab(graphEditorTab)

    private fun activateTab(tab: Tab) {
        require(mainComponent.getTabs().any { it == tab }) { "Tab is not in main panel." }
        mainComponent.activeTab = tab
        tab.component.also {
            when (it) {
                is CodeEditor -> it.focus()
            }
        }
    }


    override fun load(entity: DialogueSnippet): DialogueSnippet {
        super.load(entity)
        graphEditor.load(entity.graph, isNew = entity.isNew)
        propertiesEditor.load(entity)
        descriptionEditor.set(entity.description)
        sourceEditor.setData(entity)
        return entity
    }

    override suspend fun save() {
        newState { copy(saving = true) }

        var snippet = entity!!.clone()

        if (snippet.name.isBlank()) {
            ConfirmModal(tr("Missing Snippet Name"), tr("Dialogue snippet name must not be empty.")).open()
            newState { copy(saving = false) }
            return
        }

        try {
            snippet = if (snippet.isNew) {
                DialogueSnippetResource.create(snippet)
            } else {
                DialogueSnippetResource.update(snippet)
            }
            load(snippet)
        } catch (t: Throwable) {
            ErrorHandler.handle(t)

            return
        } finally {
            newState { copy(saving = false) }
        }

        savedVersion = snippet.clone()
        modelState.value = Saved
    }

    override suspend fun saveAs() {
        TODO()
    }

    override suspend fun openAsNew() {
        TODO()
    }

    override suspend fun delete() {
        DialogueSnippetResource.delete(entity!!)
    }
}
