package ai.flowstorm.studio.ui.form

import ai.flowstorm.studio.resources.space.SpeechRecognizerResource
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.SelectId
import io.kvision.i18n.tr
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class SpeechRecognizerId : SelectId(label = tr("Speech Recognizer")) {

    val mainScope = MainScope()

    fun load(language: String?) = mainScope.launch {
        val speechRecognizers = SpeechRecognizerResource.load()
        options = UiHelpers.options(speechRecognizers, true)
        console.log("speech recognizers loaded", speechRecognizers)
    }
}