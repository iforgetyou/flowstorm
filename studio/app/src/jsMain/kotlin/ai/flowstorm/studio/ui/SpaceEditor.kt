package ai.flowstorm.studio.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.state.RootState.Action.SpaceLeft
import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.common.ui.EntityEditor
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.studio.model.Space
import ai.flowstorm.common.ErrorHandler
import io.kvision.html.Button
import kotlinx.coroutines.launch
import io.kvision.html.ButtonStyle
import io.kvision.html.button
import io.kvision.i18n.tr
import io.kvision.modal.Alert
import io.kvision.modal.ModalSize
import io.kvision.panel.SimplePanel
import io.kvision.rest.RemoteRequestException
import kotlinx.coroutines.cancel

open class SpaceEditor : EntityEditor<Space>(SpaceForm(SpaceResource), serializer = Space.serializer()) {

    lateinit var joinButton: Button
    lateinit var leaveButton: Button

    override fun otherButtons(panel: SimplePanel) {
        joinButton = panel.button(tr("Join"), style = ButtonStyle.INFO) {
            marginRight = UiHelpers.spacing
        }.onClick {
            mainScope.launch {
                try {
                    SpaceResource.joinCurrentUser(entity!!._id)
                } catch (e: RemoteRequestException) {
                    ErrorHandler.handle(e)
                }
            }
        }

        leaveButton = panel.button(tr("Leave"), style = ButtonStyle.INFO) {
            marginRight = UiHelpers.spacing
        }.onClick {
            mainScope.launch {
                try {
                    SpaceResource.leave(entity!!._id)
                } catch (e: RemoteRequestException) {
                    Alert.show("Can not leave space", "<ol>${e.message}</ol>", rich = true, size = ModalSize.LARGE)
                    throw Exception("Can not leave space", e)
                } catch (e: ClassCastException) {
                    //ignore ... some cast exception in AbstractResource, will be removed with new
                }
                AppState.dispatch(SpaceLeft(entity!!._id ))
            }
        }
    }

    override fun onload(entity: Space) = super.onload(entity).apply {
        joinButton.disabled = (entity.isNew) || AppState.state.userRoles.any { it.spaceId == entity.id }
        leaveButton.disabled =
            (entity.isNew) || !AppState.state.userRoles.any { it.spaceId == entity.id } || AppState.state.userRoles.size == 1
        deleteButton?.disabled = AppState.state.selectedRole?.spaceId == entity.id
    }
}
