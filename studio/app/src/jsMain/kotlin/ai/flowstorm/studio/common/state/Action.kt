package ai.flowstorm.studio.common.state

import ai.flowstorm.common.stateManager.AsyncActionImpl
import ai.flowstorm.common.stateManager.StateManager

fun action(name: String? = null, block: suspend StateManager<RootState.State>.() -> Unit) = AsyncActionImpl(name, block)
