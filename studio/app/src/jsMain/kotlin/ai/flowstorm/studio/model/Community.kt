package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.id.newId
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class Community (
        override val _id: Id = newId(),
        val name: String,
        val space_id: String? = null,
        val attributes: JsonElement
) : AbstractEntity() {
    override val id get() = StringId(name)
    override val label get() = id.value
}