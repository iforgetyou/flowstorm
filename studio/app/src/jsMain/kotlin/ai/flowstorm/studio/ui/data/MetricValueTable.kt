package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.resources.MetricValueResource
import ai.flowstorm.studio.model.analytics.MetricValue
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.state.ObservableList
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions

class MetricValueTable(messages: ObservableList<MetricValue>) : EntityTable<MetricValue>(
        "Metric",
        MetricValueResource(messages),
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Namespace"), MetricValue::namespace.name),
                        ColumnDefinition(tr("Name"), MetricValue::name.name),
                        ColumnDefinition(tr("Value"), MetricValue::value.name)
                )
        ),
        { MetricValueForm(MetricValueResource(messages)) },
        deleteEnabled = false,
        saveEnabled = false,
        hideHeader = true
)