package ai.flowstorm.studio.model

import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.util.toDate
import kotlinx.serialization.Serializable
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import io.kvision.moment.Moment
import io.kvision.types.KFile
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

@Serializable
data class File(
    override val _id: Id = newId(),
    val name: String? = null,
    val type: String = "",
    val tags: MutableList<String> = mutableListOf(),
    val description: String = "",
    val datetime: Instant = Clock.System.now(),
    val owner_id: Id? = null,
    val space_id: Id? = null,
    val upload: List<KFile>? = null
) : AbstractEntity() {
    override val label
        get() = if (space_id != null)
            SpaceResource.get(space_id).let {
                "[${it.name}]$name"
            } else name ?: ""
    var source: String? = null
        get() {
            return if (field == null && !isNew)
                "$fileUrl/${_id.value}"
            else field
        }
        set(value) {
            if (value?.startsWith(fileUrl) == false)
                field = value
        }
    var stringTime: String = ""

    init {
        stringTime =  Moment(datetime.toDate()).fromNow() as String
    }

    companion object {
        val fileUrl get() = "${AppState.state.fileUrl}/assets/spaces"
    }
}
