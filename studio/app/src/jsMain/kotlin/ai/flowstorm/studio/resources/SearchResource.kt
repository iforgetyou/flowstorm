package ai.flowstorm.studio.resources

import ai.flowstorm.common.resources.AbstractResource
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.SearchResult
import io.kvision.rest.HttpMethod
import kotlinx.serialization.builtins.ListSerializer

object SearchResource :
    AbstractResource<SearchResult>(serializer = SearchResult.serializer(), resourceUri = "/search") {


    suspend fun search(searchRequest: SearchRequest): List<SearchResult> {

        val spaceId = AppState.state.spaceId!!


        val query = with(searchRequest) {

            "?search=${this.search}&entity=${this.entity}&type=${this.type}"
        }

        return client.remoteCall("/search/${spaceId.value}$query", ListSerializer(SearchResult.serializer()), HttpMethod.GET)

    }

    data class SearchRequest(
        val search:String,
        val entity:String,
        val type:String,
    )

}
