package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.space.InvitationResource
import ai.flowstorm.common.ui.AppPanel
import ai.flowstorm.common.ui.UiHelpers
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.ConfirmModal
import io.kvision.core.TextAlign
import io.kvision.html.ButtonStyle
import io.kvision.html.button
import io.kvision.i18n.I18n.tr
import io.kvision.i18n.I18n.gettext
import io.kvision.modal.Alert
import io.kvision.modal.Modal
import io.kvision.panel.simplePanel
import io.kvision.utils.perc

class InvitationEditor: InvitationForm(InvitationResource) {
    init {
        disableAll()
        simplePanel {
            width = 100.perc
            padding = UiHelpers.spacing
            textAlign = TextAlign.RIGHT
            button(tr("Resend"), style = ButtonStyle.PRIMARY).onClick {
                mainScope.launch {
                    InvitationResource.resend(entity).let {
                        Alert.show(tr("User Invitation"),
                                gettext("Invitation") + " #${it.id.value} " + gettext("has been resent to user") + " ${it.username}."
                        )
                        UiHelpers.parent<Modal>(this@onClick)?.hide()
                    }
                }

            }
            button(tr("Delete"), style = ButtonStyle.DANGER) {
                marginLeft = UiHelpers.spacing
            }.onClick {
                mainScope.launch {
                    if (ConfirmModal.Delete().open()) {
                        delete()
                        UiHelpers.parent<Modal>(this@onClick)?.hide()
                        AppPanel.instance.reload()
                    }
                }
            }
        }
    }
}