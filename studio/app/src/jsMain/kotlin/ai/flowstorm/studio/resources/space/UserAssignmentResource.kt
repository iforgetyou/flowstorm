package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.common.resources.EntityResource
import io.kvision.state.sub
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object UserAssignmentResource : EntityResource<Space.UserAssignment>(serializer = Space.UserAssignment.serializer()) {

    init {
        AppState.sub { it.spaceId }.onEach { clear() }.launchIn(App.scope)

    }

    override val resourceUri: String
        get() = "/spaces/${AppState.state.spaceId!!.value}/userAssignments"

    suspend fun createConsumer(user: User) {
        UserResource.create(user).let {
            create(Space.UserAssignment(user_id = it.id, _user = it))
        }
    }
}
