package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.*
import kotlinx.serialization.Serializable
import io.kvision.i18n.gettext

@Serializable
data class Space(
    override val _id: Id = newId(),
    var name: String = "",
    var deviceConfiguration: String? = "{}",
    var coreUrl: String? = null,
    var coreKey: String? = null,
    var twilioAccountID: String? = null,
    var twilioAuthToken: String? = null,
    var userAssignments: Array<UserAssignment> = arrayOf(),
    var groups: Array<UserGroup> = arrayOf()
) : AbstractEntity() {

    //abstract class Configuration(open var settings: Settings? = null, open var applications_id: Array<Id>? = null): NamedEntity()

    @Serializable
    data class UserAssignment(
        var user_id: Id? = null,
        var role: String = Role.Consumer.toString(),
        var note: String? = null,
        var settings: Settings = Settings(),
        var applications_id: List<Id> = listOf(),
        var _user: User? = null
    ) : HasId, Matchable, Labeled {
        override val id get() = user_id!!
        override var label = ""
            get() = _user?.label?:""
        var roleName: String? = null
            get()  {
                return gettext(role)
            }
        override fun match(input: String): Boolean = label.contains(input, true)

    }

    @Serializable
    data class UserGroup(
        var name: String? = null,
        var note: String? = null,
        var users_id: List<Id> = listOf(),
        var settings: Settings = Settings(),
        var applications_id: List<Id> = listOf()
    ) : HasId, Matchable {
        override val id: Id
            get() = if (name != null) StringId(name!!) else StringId("")
        override fun match(input: String): Boolean =
                "$name $note".contains(input, true)
    }

    @Serializable
    data class Settings(
            var description: String = ""
    )

    enum class Role {
        Consumer,
        Maintainer,
        Editor,
        Admin,
        Owner;

        companion object {
            private val order = listOf(Consumer, Maintainer, Editor, Admin, Owner)
            fun isAllowed(required: Role, actual: Role): Boolean = order.indexOf(required) <= order.indexOf(actual)
        }
    }

    override val label by this::name
}