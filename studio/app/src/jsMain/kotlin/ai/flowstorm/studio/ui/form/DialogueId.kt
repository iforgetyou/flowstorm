package ai.flowstorm.studio.ui.form

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.resources.space.DialogueSourceResource
import ai.flowstorm.studio.ui.design.dialogue.OpenDialogueModal
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.AppPanel
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.form.text.TextInputType
import io.kvision.modal.Modal

open class DialogueId(
        type: TextInputType = TextInputType.TEXT, value: String? = null, name: String? = null,
        label: String? = null, rich: Boolean = false
) : EntityId(type, value, name, label, rich) {

    init {
        linkButton.apply {
            visible = true
            disabled = AppState.state.selectedRole?.role == Space.Role.Consumer ||
                    AppState.state.selectedRole?.role == Space.Role.Maintainer
        }.onClick {
            this@DialogueId.value?.let {
                (AppPanel.instance as ai.flowstorm.studio.ui.SpacePanel).loadDialogue(it, nodeId)
                UiHelpers.parent<Modal>(this)?.hide()
            }
        }
    }

    override suspend fun textValue(value: Id) = DialogueSourceResource.load(value).let {
        it.label + if (nodeId != null) "#$nodeId" else ""
    }

    override suspend fun openEntity() {
        if (!disabled) {
            OpenDialogueModal().open().let {
                value = it._id
            }
        }
    }
}
