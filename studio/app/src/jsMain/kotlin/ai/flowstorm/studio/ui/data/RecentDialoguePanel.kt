package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.resources.space.DialogueSourceResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.model.Query
import io.kvision.html.link
import io.kvision.panel.SimplePanel
import io.kvision.panel.simplePanel
import kotlin.math.min

class RecentDialoguePanel(limit: Int = 8) : SimplePanel(), Reloadable {

    private val mainScope = MainScope()
    private val query = Query(limit = limit)

    override fun reload() {
        mainScope.launch {
            with (DialogueSourceResource) {
                reload(query)
                removeAll()
                simplePanel(classes = setOf("panel-list")) {
                    for (i in 0 until min(entities.size, 10)) {
                        val model = entities[i]
                        simplePanel(classes = setOf("panel-list__item")) {
                            link("${model.dialogueName}:${model.version}", "#!/space/dialogue/${model._id.value}")
                        }
                    }
                }
            }
        }
    }
}
