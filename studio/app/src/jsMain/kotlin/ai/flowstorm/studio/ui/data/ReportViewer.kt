package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.resources.BaseReportResource
import ai.flowstorm.studio.resources.BaseReportResource.ReportSettings
import ai.flowstorm.studio.model.analytics.Report
import ai.flowstorm.studio.resources.space.report.ReportResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.ErrorHandler
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.chart.Chart
import io.kvision.chart.ChartType
import io.kvision.chart.Configuration
import io.kvision.chart.DataSets
import io.kvision.core.Color
import io.kvision.panel.SimplePanel
import io.kvision.utils.obj

class ReportViewer(var reportSettings: ReportSettings = ReportSettings(), val reportModel: BaseReportResource = ReportResource) : SimplePanel(), Reloadable {
    private val mainScope = MainScope()

    private val colors = arrayOf(Color.hex(0x381483), Color.hex(0x00a8b7), Color.hex(0x00d095), Color.hex(0x2c1b57), Color.hex(0x7424ff), Color.hex(0xff0239), Color.hex(0x5a3153), Color.hex(0xffc14a), Color.hex(0xb70229), Color.hex(0xff7b00))
    private var report: Report? = null

    private var chart = Chart(Configuration(ChartType.BAR, listOf()))

    init {
        paddingRight = UiHelpers.spacing
        add(chart)
    }

    override fun reload() {
        mainScope.launch {
            try {
                report = reportModel.data(reportSettings)

                update()
            } catch (t: Throwable) {
                ErrorHandler.handle(Exception("Can not load report data", t))
            }
        }
    }

    class Exception(message: String, cause: Throwable) : Throwable(message, cause)

    fun update() {
        chart.configuration = Configuration(
                ChartType.BAR,
                report!!.dataSets.mapIndexed { index, dataSet ->
                    DataSets(
                            label = dataSet.label,
                            borderColor = listOf(colors[index.rem(colors.size)]),
                            backgroundColor = listOf(colors[index.rem(colors.size)]),
                            fill = false,
                            data = (report!!.columns.indices).map {
                                obj {
                                    x = it
                                    y = dataSet.data[it]
                                }
                            }
                    )
                },
                report!!.columns.toList()
        )
    }
}