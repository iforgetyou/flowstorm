package ai.flowstorm.studio.model.analytics

import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.model.Matchable
import ai.flowstorm.common.model.Labeled
import kotlinx.serialization.Serializable

@Serializable
data class MetricValue(
        var namespace: String,
        var name: String,
        var value: Long = 0
): HasId, Labeled, Matchable {
    override val id get() = StringId("${namespace}/${name}/${value}")
    override val label by this::name
    val stringValue get() = value.toString()
}