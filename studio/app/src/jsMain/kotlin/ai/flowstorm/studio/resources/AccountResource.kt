package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Account
import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod

object AccountResource : EntityResource<Account>(resourceUri = "/accounts", serializer = Account.serializer()) {
    suspend fun create() {
        remoteCall<Invitation>(method = HttpMethod.POST)
    }
}
