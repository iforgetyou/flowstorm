package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import io.kvision.rest.HttpMethod
import kotlinx.coroutines.await

object DialogueSnippetResource : EntityResource<DialogueSnippet>(serializer = DialogueSnippet.serializer()) {

    override val resourceUri get() = createUri()

    private fun createUri(spaceId: Id): String = "/spaces/${spaceId.value}/dialogueSnippets"
    private fun createUri() = createUri(AppState.state.spaceId!!)

    suspend fun loadGraph(dialogueSnippet: DialogueSnippet): Graph {
        return client.remoteCall(
            client.baseUrl + createUri(dialogueSnippet.space_id) + "/${dialogueSnippet.id.value}/graph",
            deserializer = Graph.serializer(),
            method = HttpMethod.GET,
            beforeSend = client.beforeSend
        ).await()
    }

    suspend fun load(language: String): List<DialogueSnippet> = (if (entities.isEmpty()) load() else entities).filter {
        it.language == language
    }
}
