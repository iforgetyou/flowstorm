package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.SpeechRecognizer
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityEditor

class SpeechRecognizerEditor : EntityEditor<SpeechRecognizer>(SpeechRecognizerForm(), serializer = SpeechRecognizer.serializer()) {

    override fun load(id: Id) = super.load(id).also {
        if (it.space_id != AppState.state.spaceId!!) {
            disableAll()
            (entityComponent as SpeechRecognizerForm).configForm.disableAll()
        }
    }
}
