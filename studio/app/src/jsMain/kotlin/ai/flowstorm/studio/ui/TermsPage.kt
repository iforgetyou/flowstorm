package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.TermsResource
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.StringId
import ai.flowstorm.common.ui.*
import io.kvision.html.*
import io.kvision.panel.vPanel
import io.kvision.i18n.tr
import io.kvision.require
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class TermsPage : PagePanel(), Navigable {

    private var cardPanel: CardPanel
    private lateinit var textSpan: Span
    private lateinit var consentButton: Button
    private lateinit var termsId: Id
    private var hasConsent: Boolean = false
        set(value) {
            with(consentButton) {
                disabled = false
                text = if (value)
                    tr("Revoke Consent")
                else
                    tr("Give Consent")
            }
            field = value
        }
    private val mainScope = MainScope()
    private val converter by lazy {
        @Suppress("UNUSED_VARIABLE")
        val showdown = require("showdown")
        js("new showdown.Converter()")
    }

    init {
        body.apply {
            cardPanel = cardPanel("Terms and Conditions", className = "body-panel") {
                addPanelHeader()
                vPanel(className = "content-panel") {
                    padding = UiHelpers.spacing
                    textSpan = span(rich = true)
                }
                consentButton = button("...", disabled = true).onClick {
                    mainScope.launch {
                        if (hasConsent)
                            TermsResource.revokeConsent(termsId)
                        else
                            TermsResource.giveConsent(termsId)
                        hasConsent = !hasConsent
                    }
                }
            }
        }
    }

    override fun onNavigate(route: String, params: dynamic) {
        termsId = StringId(params.id as String)
        mainScope.launch {
            TermsResource.load(termsId).let { terms ->
                if (terms.title.isNotBlank())
                    cardPanel.panelHeader.content = terms.title
                textSpan.content = converter.makeHtml(terms.text) as? String
                val consent = TermsResource.getConsent(terms._id)
                hasConsent = (consent != null)
            }
        }
    }
}
