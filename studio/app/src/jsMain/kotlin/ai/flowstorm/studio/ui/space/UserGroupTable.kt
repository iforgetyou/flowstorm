package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.resources.space.UserGroupResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

class UserGroupTable : EntityTable<Space.UserGroup>(
        "User Group",
        UserGroupResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), Space.UserGroup::name.name)
                )
        ),
        { UserGroupForm(UserGroupResource) },
        { Space.UserGroup() },
        //deleteEnabled = false,
        splitMode = true,
        tabWidth = 240.px,
        className = "content-panel"
) {
        override fun onSave() {
                needsReload = true
                reload()
                super.onSave()
        }
}