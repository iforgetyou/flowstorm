package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Utterance
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import io.kvision.form.text.Text
import io.kvision.i18n.tr

class UtteranceForm(model: EntityResource<Utterance>): EntityForm<Utterance>(model) {
    init {
        add(Utterance::timeAsString, Text(label = tr("Time")))
        add(Utterance::personaName, Text(label = tr("Persona")))
        disableAll()
    }
}