package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.resources.space.DialogueSourceResource
import ai.flowstorm.studio.ui.design.DialogueSourceEditor
import ai.flowstorm.common.Toast
import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.Icon
import ai.flowstorm.common.ui.toolBarButton
import io.kvision.i18n.gettext
import io.kvision.panel.SimplePanel
import io.kvision.toolbar.buttonGroup
import io.kvision.toolbar.toolbar
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class DialogueXmlEditor(private val dialogueSourceEditor: DialogueSourceEditor) : SimplePanel() {

    private val mainScope = MainScope()
    private val xmlEditor = CodeEditor("xml", setOf("panel-view-nav2")).apply {
        readOnly = false
    }

    init {
        toolbar(className = "panel-toolbar") {
            buttonGroup {
                toolBarButton(Icon.Clear, gettext("Clear")).onClick {
                    xmlEditor.set("")
                }
                toolBarButton(Icon.Xml, gettext("Source XML")).onClick {
                    mainScope.launch {
                        DialogueSourceResource.getXml(dialogueSourceEditor.entity!!).let {
                            xmlEditor.set(it)
                            Toast.success(gettext("Source XML loaded"))
                        }
                    }
                }
                toolBarButton(Icon.Xsl, gettext("Source XSL")).onClick {
                    mainScope.launch {
                        DialogueSourceResource.getXsl(dialogueSourceEditor.entity!!).let {
                            xmlEditor.set(it)
                            Toast.success(gettext("Source XSL loaded"))
                        }
                    }
                }
                toolBarButton(Icon.Magic, gettext("Transform XML Data with Source XSL to XML")).onClick {
                    mainScope.launch {
                        DialogueSourceResource.getTransformedXml(dialogueSourceEditor.entity!!, xmlEditor.value).let {
                            xmlEditor.set(it)
                            Toast.success(gettext("Source XML generated"))
                        }
                    }
                }
                toolBarButton(Icon.Save, gettext("Update from Source XML")).onClick {
                    mainScope.launch {
                        DialogueSourceResource.updateXml(dialogueSourceEditor.entity!!, xmlEditor.value).let {
                            xmlEditor.set(it)
                            Toast.success(gettext("Model updated from source XML"))
                        }
                    }
                }
                toolBarButton(Icon.New, gettext("Create from Source XML")).onClick {
                    mainScope.launch {
                        DialogueSourceResource.createXml(xmlEditor.value).let {
                            //TODO open new tab with create dialogue source?
                            xmlEditor.set("")
                            Toast.success(gettext("Model created from source XML"))
                        }
                    }
                }
            }
        }
        add(xmlEditor)
    }
}