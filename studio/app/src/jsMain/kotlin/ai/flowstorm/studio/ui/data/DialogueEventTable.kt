package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.DialogueEvent
import ai.flowstorm.studio.resources.space.DialogueEventResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.modal.ModalSize
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

open class DialogueEventTable(splitMode: Boolean = true, hideHeader: Boolean = false, seekMode: Boolean = true, className: String? = "content-panel") : EntityTable<DialogueEvent>(
        "Dialogue Event",
        DialogueEventResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Type"), DialogueEvent::type.name, width = "23%"),
                        ColumnDefinition(tr("Application"), DialogueEvent::applicationName.name, width = "44%"),
                        ColumnDefinition(tr("Time"), DialogueEvent::stringTime.name, width = "33%")
                )
        ),
        { DialogueEventForm(DialogueEventResource) },
        tabWidth = 600.px,
        splitMode = splitMode,
        seekEnabled = seekMode,
        saveEnabled = false,
        deleteEnabled = false,
        modalSize = ModalSize.LARGE,
        hideHeader = hideHeader,
        className = className,
        searchEnabled = true,
)
