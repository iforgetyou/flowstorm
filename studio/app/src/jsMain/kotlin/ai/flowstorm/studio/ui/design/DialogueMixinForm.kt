package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.model.Enum
import ai.flowstorm.studio.model.DialogueMixin
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.UiHelpers
import ai.flowstorm.common.ui.form.CodeFormControl
import io.kvision.form.FormHorizontalRatio
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.i18n.tr

class DialogueMixinForm : EntityForm<DialogueMixin>(DialogueMixinResource, FormHorizontalRatio.RATIO_2) {
    init {
        add(
            DialogueMixin::name,
            Text(label = tr("Name")),
            required = true,
            requiredMessage = tr("Required field")
        )
        add(
            DialogueMixin::type,
            Select(label = tr("Type"), options = UiHelpers.options(Enum.mixinTypes))
        )
        add(
            DialogueMixin::description,
            TextArea(label = tr("Description"))
        )
        add(
            DialogueMixin::language,
            Select(label = tr("Language"), options = UiHelpers.options(Enum.languages, true))
        )
        add(
            DialogueMixin::text,
            CodeFormControl(label = tr("Text"))
        )
    }
}