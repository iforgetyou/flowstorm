package ai.flowstorm.studio.ui.data

import ai.flowstorm.studio.model.Session
import ai.flowstorm.studio.resources.space.SessionResource
import ai.flowstorm.common.setProperty
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.I18n.tr
import io.kvision.modal.ModalSize
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.obj
import io.kvision.utils.px

class SessionTable(splitMode: Boolean = true, hideHeader: Boolean = false, className: String? = "content-panel") : EntityTable<Session>(
        "Session",
        SessionResource.all,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("User"), Session::userName.name, width = "29%"),
                        ColumnDefinition(tr("Application"), Session::applicationName.name, width = "28%"),
                        ColumnDefinition(tr("Time"), Session::stringTime.name, width = "35%"),
                        ColumnDefinition(tr("T#"), Session::turnCount.name, width = "8%")
                ),
        ),
        { SessionViewer(SessionResource.all) },
        entityCreatorFactory = { InitiationModal },
        tabWidth = 450.px,
        filterFormFactory = { SessionFilterForm() },
        splitMode = splitMode,
        seekEnabled = true,
        searchEnabled = true,
        modalSize = ModalSize.XLARGE,
        hideHeader = hideHeader,
        className = className,
        dataExtractor = {
                obj {
                        setProperty(this, it::userName)
                        setProperty(this, it::applicationName)
                        setProperty(this, it::stringTime)
                        setProperty(this, it::turnCount)
                }
        },
)
