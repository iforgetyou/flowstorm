package ai.flowstorm.studio.ui.design.entity

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.resources.DataSampleResource
import ai.flowstorm.studio.resources.EntityDatasetResource
import ai.flowstorm.studio.model.EntityDataset
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.JsonEditor
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import kotlinx.coroutines.launch
import ai.flowstorm.common.ui.ConfirmModal
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.core.StringPair
import io.kvision.core.TooltipOptions
import io.kvision.core.onEvent
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.form.text.TextInput
import io.kvision.html.Button
import io.kvision.html.Div
import io.kvision.html.div
import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.panel.Tab
import io.kvision.panel.TabPanel
import io.kvision.panel.TabPanelNavDropDown
import io.kvision.panel.tabPanel
import kotlinx.browser.document
import kotlinx.serialization.builtins.ListSerializer
import org.w3c.dom.Element
import org.w3c.dom.HTMLCollection
import org.w3c.dom.asList

open class EntityDatasetForm(model: EntityResource<EntityDataset>) : EntityForm<EntityDataset>(model) {
    private val initialHeight = 10
    private val jsonComponent = JsonEditor(ListSerializer(EntityDataset.DataSample.serializer()))

    lateinit var tabs: TabPanel
    private val contextSamples = TextArea(value = "", rows = initialHeight)
    private val valueSamples = LinkedHashMap<String, TextArea>()
    private val trainingStatus: TrainingStatus
    private val addTypeButton = Button(tr("Add type")).onClick {
        addTypeModal.show()
    }

    private val addTypeModal = Modal(tr("Entity type name")).apply {
        val input = TextInput()
        add(div(tr("Specify the type name")))
        add(input)
        addButton(Button("OK").onClick {
            input.value?.trim()?.let {
                valueSamples[it] = TextArea(value = entity?.valueSamples?.get(it)?.joinToString(separator = "\n") ?: "",
                                            rows = entity?.valueSamples?.get(it)?.size ?: initialHeight)
                tabs.addTab(it, valueSamples[it]!!, closable = true)
            }
            this@apply.hide()
        })
    }

    init {
        trainingStatus = TrainingStatus(Button(tr("Train"), disabled = true).onClick {
            disabled = true
            mainScope.launch {
                save()
                EntityDatasetResource.train(entity!!)
            }
        })

        add(
            EntityDataset::name,
            Text(label = tr("Name")),
            required = true,
            requiredMessage = tr("Required field")
        )
        add(
            EntityDataset::language,
            Select(label = tr("Language"), options = listOf(StringPair("en", "en"), StringPair("cs", "cs"))),
            required = true,
            requiredMessage = tr("Required field")
        )

        div {
            add(addTypeButton)
            add(trainingStatus.button.apply { margin = UiHelpers.spacing })
            add(trainingStatus.description.apply { marginBottom = UiHelpers.spacing })
        }
    }

    override fun load(entity: EntityDataset): EntityDataset {
        this.entity = entity
        setData(entity)
        loadSamples(entity)

        tabs = tabPanel(classes = setOf("designer"), init = {nav = TabPanelNavDropDown(this, navClasses)}).apply {
            addTab(tr("Context Samples"), contextSamples)
            val fixedTabs = getSize()

            entity.valueSamples.keys.forEach {
                valueSamples[it] = TextArea(value = entity.valueSamples[it]?.joinToString(separator = "\n") ?: "",
                                            rows = entity.valueSamples[it]?.size ?: initialHeight)
                addTab(it, valueSamples[it]!!, closable = true, tooltipOptions = TooltipOptions(it))
            }
            onEvent {
                tabClosing = { e ->
                    val id = e.detail.data as Int
                    val name = valueSamples.keys.toList()[id - fixedTabs]
                    mainScope.launch {
                        if (ConfirmModal(
                                tr("Confirm closing"),
                                tr("Do you really want to remove $name type")).open()
                        ) {
                            this@EntityDatasetForm.tabs.removeTab(id)
                            refresh()
                            valueSamples.remove(name)
                        }
                    }
                    e.preventDefault()
                }
            }
        }
        mainScope.launch {
            if (entity.space_id == AppState.state.spaceId!!) {
                trainingStatus.updateStatus(EntityDatasetResource.status(entity))
            }
        }
        return entity
    }

    override fun load(id: Id): EntityDataset {
        val e = EntityDatasetResource.get(id)
        jsonComponent.setData(e.samples)
        return load(e)
    }

    private fun loadSamples(entity: EntityDataset) {
        val dataSampleModel = DataSampleResource(entity._id)
        mainScope.launch {
            dataSampleModel.reload(Query(limit = -1))
            contextSamples.rows = dataSampleModel.entities.size
            contextSamples.value = dataSampleModel.entities.joinToString(separator = "\n") { it.text }
        }
    }

    override suspend fun save() {
        entity!!.samples = contextSamples.value!!.lines().map {
            val s = EntityDataset.DataSample()
            s.text = it
            s
        }

        entity!!.valueSamples.clear()
        valueSamples.forEach {
            entity!!.valueSamples[it.key] = it.value.value!!.lines()
        }
        super.save()
    }

    fun disableRest() {
        disableAll()
        tabs.getTabs().forEach {
            it.closable = false
            it.getChildren().forEach { tab ->
                if (tab is TextArea) {
                    tab.disabled = true
                }
            }
        }
        trainingStatus.button.disabled = true
        contextSamples.disabled = true
        addTypeButton.disabled = true
    }

    override fun afterDestroy() {
        document.getElementsByClassName("tooltip").asList().forEach {
            it.remove()
        }
        super.afterDestroy()
    }

    class TrainingStatus(val button: Button) {
        var description = Div("")

        fun updateStatus(status: String?) {
            if (status != null) {
                description.content = "Last training status: $status"
            }
            if (status != "IN_PROGRESS") {
                button.disabled = false
            }
        }
    }
}
