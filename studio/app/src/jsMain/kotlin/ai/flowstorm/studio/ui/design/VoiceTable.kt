package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.model.Voice
import ai.flowstorm.studio.resources.space.VoiceResource
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px

class VoiceTable: EntityTable<Voice>(
    "Voice",
    VoiceResource,
    TabulatorOptions(
        layout = Layout.FITCOLUMNS,
        columns = listOf(ColumnDefinition(tr("Name"), Voice::label.name))
    ),
    { VoiceEditor() },
    { Voice() },
    //deleteEnabled = false,
    splitMode = true,
    tabWidth = 320.px,
    className = "content-panel"
)