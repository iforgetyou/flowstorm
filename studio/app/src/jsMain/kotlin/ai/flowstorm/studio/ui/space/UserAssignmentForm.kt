package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.resources.UserResource
import ai.flowstorm.studio.resources.space.ApplicationResource
import ai.flowstorm.studio.ui.UserForm
import kotlinx.coroutines.launch
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.ui.EntityForm
import ai.flowstorm.common.ui.UiHelpers.options
import ai.flowstorm.common.ui.form.ListIdSelect
import io.kvision.form.select.Select
import io.kvision.form.text.Text
import io.kvision.html.TAG
import io.kvision.html.tag
import io.kvision.i18n.I18n.tr

class UserAssignmentForm(model: EntityResource<Space.UserAssignment>) : EntityForm<Space.UserAssignment>(model) {

    private val applicationsIdSelect = ListIdSelect(label = tr("Applications"))
    private val userForm = UserForm(UserResource)
    private val roleSelector = Select(label = tr("Role"), options = options(Space.Role.values())).apply {
        disabled = AppState.state.selectedRole?.role?.let { !Space.Role.isAllowed(Space.Role.Admin, it) } ?: true
    }

    init {
        add(
                Space.UserAssignment::role,
                roleSelector,
                required = true,
                requiredMessage = tr("Required field")
        )
        add(Space.UserAssignment::note, Text(label = tr("Note")))
        add(Space.UserAssignment::applications_id, applicationsIdSelect)
        tag(TAG.H3, tr("User Properties"))
        add(userForm)
    }

    private fun loadApplications() = mainScope.launch {
        val app = ApplicationResource.load()
        applicationsIdSelect.options = options(app)
    }

    override fun load(entity: Space.UserAssignment): Space.UserAssignment {
        loadApplications()
        val userAssignment = super.load(entity)
        userForm.load(userAssignment._user!!)
        return userAssignment
    }

    override fun load(id: Id): Space.UserAssignment {
        loadApplications()
        val userAssignment = super.load(id)
        userForm.load(userAssignment._user!!)
        return userAssignment
    }

    override suspend fun save() {
        super.save()
        userForm.save()
        if (entity!!._user !is User) {
            // ^^^ in case entity was set from JSON (see EntityEditor.save() method)
            // _user is no more object of class User (but plain object) and therefore cannot be saved
            userForm.save(entity!!._user!!)
        }
    }
}
