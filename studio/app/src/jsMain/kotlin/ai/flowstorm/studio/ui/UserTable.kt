package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.UserResource
import ai.flowstorm.common.ui.EntityTable
import ai.flowstorm.studio.model.User
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.i18n.I18n.tr

class UserTable: EntityTable<User>(
        "User",
        UserResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Username"), User::username.name, width = "35%"),
                        ColumnDefinition(tr("Name"), User::name.name, width = "15%"),
                        ColumnDefinition(tr("Surname"), User::surname.name, width = "15%"),
                        ColumnDefinition(tr("Nickname"), User::nickname.name, width = "15%"),
                        ColumnDefinition(tr("Phone Number"), User::phoneNumber.name, width = "20%")
                )
        ),
        { UserForm(UserResource) },
        { User() },
        className = "content-panel",
        searchEnabled = true,
)