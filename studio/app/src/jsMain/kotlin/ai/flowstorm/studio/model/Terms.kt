package ai.flowstorm.studio.model

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.serialization.*

@Serializable
data class Terms(
    override val _id: Id = newId(),
    var name: String = "",
    var version: Int = -1,
    val space_id: Id = AppState.state.spaceId!!,
    //@Serializable(with= DateSerializer::class)
    //var datetime: Date? = null,
    var title: String = "",
    var text: String = "",
    @Transient
    var newVersion: Boolean = true
) : AbstractEntity() {
    override val label get() = "$name:$version"
}
