package ai.flowstorm.studio.resources

import ai.flowstorm.studio.App
import ai.flowstorm.common.RestClient
import io.kvision.rest.HttpMethod
import kotlinx.serialization.Serializable

object AuthResource {

    @Serializable
    data class Tokens(val id: String, val access: String)

    private val client: RestClient = RestClient(App.config.restUrl) //no beforeSend

    private val resourceUri = "/auth"

    suspend fun auth(deviceId: String): Tokens = client.remoteCall(
        url = "$resourceUri/$deviceId",
        method = HttpMethod.GET,
        deserializer = Tokens.serializer()
    )
}