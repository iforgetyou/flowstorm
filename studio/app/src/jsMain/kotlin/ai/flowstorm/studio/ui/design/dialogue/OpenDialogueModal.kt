package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.resources.space.DialogueSourceResource
import kotlinx.coroutines.*
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.ui.Finder
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.core.*
import io.kvision.form.select.Select
import io.kvision.form.text.TextInput
import io.kvision.form.text.TextInputType
import io.kvision.html.Button
import io.kvision.html.ButtonStyle
import io.kvision.html.Span
import io.kvision.i18n.I18n.gettext
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.hPanel
import io.kvision.require
import io.kvision.utils.px

class OpenDialogueModal(allowOpenAsNew: Boolean = false) : Modal(tr("Load Dialogue"), size = ModalSize.LARGE, classes = setOf("finder")) {

    class DialogueItem(label: String, val dialogue: DialogueSource) : Finder.Item(label)

    private lateinit var dialogues: List<DialogueSource>
    private val mainScope = MainScope()
    private val open = Job()
    var createNewVersion = false

    private val loading = Span(tr("Loading ..."))

    private val openButton = Button(gettext("Open")).apply {
        disabled = true
        onClick {
            open.complete()
        }
    }

    private val openAsNewButton = Button(gettext("Open as new"), style = ButtonStyle.SECONDARY).apply {
        disabled = true
        onClick {
            createNewVersion = true
            open.complete()
        }
    }

    private val cancelButton = Button(gettext("Cancel"), style = ButtonStyle.SECONDARY).apply {
        onClick { cancel() }
    }

    private val searchInput = TextInput(TextInputType.SEARCH).apply {
        margin = UiHelpers.spacing
        placeholder = tr("Quick search...")
        onEvent {
            input = {
                finder.items = finderItems(dialogues.filter { it.dialogueName.contains(this@apply.value ?: "")  })
                this@OpenDialogueModal.remove(finder)
                this@OpenDialogueModal.add(finder)
                this@apply.focus()
            }
        }
    }

    private val stateSelect = Select().apply {
        options = DialogueSource.State.values().map { it.name to it.name }
        width = 300.px
        margin = UiHelpers.spacing
        multiple = true
        value = DialogueSource.State.Draft.name

        var old = value
        onEvent {
            hideBsSelect = {
                if (old != value) mainScope.launch {
                    load()
                }
            }
            showBsSelect = {
                old = value
            }
        }
    }

    private val finder = Finder().apply {
        itemTemplates = mapOf(
                "cs" to require("hbs/finderItemDialogue.cs.hbs"),
                "de" to require("hbs/finderItemDialogue.de.hbs"),
                "en" to require("hbs/finderItemDialogue.en.hbs"),
                "es" to require("hbs/finderItemDialogue.es.hbs")
        )

        selectedItem.subscribe {
            openButton.disabled = (it == null)
            openAsNewButton.disabled = (it == null)
        }
    }

    init {
        onEvent {
            keydown = {
                if (it.key == "Enter" && finder.selectedItem.value != null)
                    open.complete()
            }
        }

        closeIcon.onEvent {
            click = { cancel() }
        }

        hPanel(className = "content-header") {
            style {
                justifyContent = JustifyContent.SPACEBETWEEN
            }
            add(searchInput)
            add(stateSelect)
        }
        addButton(openButton)
        if (allowOpenAsNew) addButton(openAsNewButton)
        addButton(cancelButton)
    }

    suspend fun open(): DialogueSource {
        show()
        load()
        open.join()
        hide()

        val dialogue = (finder.selectedItem.value as DialogueItem).dialogue
        return if (createNewVersion)
            dialogue.cloneAsNew()
        else
            dialogue
    }

    private suspend fun load() {
        remove(finder)
        add(loading)
        val query = Query(limit = 0)
        query.filters.add(Query.Filter(DialogueSource::state.name, Query.Operator.`in`, stateSelect.value?.split(",")
                ?: ""))
        dialogues = DialogueSourceResource.load(query).sortedWith(compareBy(DialogueSource::dialogueName, DialogueSource::version))
        finder.items = finderItems(dialogues.filter { it.dialogueName.contains(searchInput.value ?: "")  })
        remove(loading)
        add(finder)
        refresh()
        searchInput.focus()
    }

    private fun finderItems(dialogues: List<DialogueSource>): Array<Finder.Item> {
        val items = mutableListOf<Finder.Folder>()
        dialogues.forEach { dialogue ->
            val names = dialogue.dialogueName.split("/")
            val productFolder = items.firstOrNull { it.label == names[0] }
                    ?: Finder.Folder(names[0]).apply {
                        items.add(this)
                    }
            val dialogueFolder = productFolder.items.filterIsInstance<Finder.Folder>().firstOrNull { it.label == names[1] }
                    ?: Finder.Folder(names[1]).apply {
                        productFolder.items.add(this)
                    }
            val dialogueItem = DialogueItem(dialogue.version.toString(), dialogue)
            dialogueFolder.items.add(dialogueItem)
        }
        return items.toTypedArray()
    }

    private fun cancel() {
        hide()
        mainScope.cancel()
    }
}