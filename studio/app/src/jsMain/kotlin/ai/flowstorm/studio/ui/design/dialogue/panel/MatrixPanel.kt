package ai.flowstorm.studio.ui.design.dialogue.panel

import com.github.snabbdom.VNode
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import io.kvision.html.Div
import io.kvision.panel.SimplePanel

class MatrixPanel(val data: String, numClasses: Int): SimplePanel() {
    private val trueLabel = "true_label"
    private val numClassesStr = numClasses.toString()
    val mainScope = MainScope()
    init {
        add(Div(content = "<facets-dive id=\"fd\" height=\"800\"/>", rich = true))
    }

    override fun render(): VNode {
        mainScope.launch {
            delay(200)
            val fd = document.getElementById("fd")
            fd?.setAttribute("data", data)
            fd?.setAttribute("vertical-facet", trueLabel)
            fd?.setAttribute("vertical-buckets", numClassesStr)
            fd?.setAttribute("horizontal-facet", "predicted_label")
            fd?.setAttribute("horizontal-buckets", numClassesStr)
            fd?.setAttribute("color-by", trueLabel)
        }
        return super.render()
    }


}