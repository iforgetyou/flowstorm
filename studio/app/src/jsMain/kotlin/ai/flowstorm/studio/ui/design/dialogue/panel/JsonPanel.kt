package ai.flowstorm.studio.ui.design.dialogue.panel

import kotlinx.coroutines.MainScope
import ai.flowstorm.common.Lockable
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.ui.JsonEditor
import kotlinx.serialization.KSerializer

abstract class JsonPanel<E : Any>(serializer: KSerializer<E>) :
    JsonEditor<E>(serializer, classes = setOf("panel-view-nav1")), Reloadable,
    Lockable {

    val mainScope = MainScope()
    override var lock = false
}
