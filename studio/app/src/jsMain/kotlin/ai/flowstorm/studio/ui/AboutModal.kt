package ai.flowstorm.studio.ui

import ai.flowstorm.studio.App
import ai.flowstorm.studio.util.nowLocal
import ai.flowstorm.studio.util.toDate
import ai.flowstorm.common.resources.ChangelogResource
import ai.promethist.services.changelog.model.Changelog
import io.kvision.core.Container
import io.kvision.core.Widget
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.i18n.I18n.gettext
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.moment.Moment
import io.kvision.panel.TabPanel
import io.kvision.panel.simplePanel
import io.kvision.panel.tab
import io.kvision.panel.tabPanel
import io.kvision.state.ObservableValue
import io.kvision.state.bind
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

object AboutModal : Modal(tr("About"), size = ModalSize.LARGE, classes = setOf("modal-about")) {

    private val scope = MainScope()
    private val data = ObservableValue<Changelog?>(null)
    private var tabs: TabPanel

    init {
        div(content = """
            <a href="https://www.flowstorm.ai/" target="_blank" title="Go to Flowstorm.ai site">
                <div class="embed-responsive embed-responsive-20by7">
                    <video class="embed-responsive-item" poster="img/flowstorm-about-poster.png" width="800" height="280" autoplay="true">                  
                        <source src="media/flowstorm-about.webm" type="video/webm">
                        <source src="media/flowstorm-about.mp4" type="video/mp4">
                        <p>Your browser doesn't support HTML5 video. Here is a <a href="media/flowstorm-about.mp4">link to the video</a> instead.</p>          
                    </video>
               </div>
            </a>""".trimIndent(), rich = true)

        tabs = tabPanel {
            tab(tr("Credits")) {
                simplePanel(classes = setOf("nav-tabs-fixed")) {
                    simplePanel {
                        p {
                            b(gettext("Flowstorm Studio") + " " + gettext("version") + " " + App.config["version"])
                        }
                        p("Tomáš&nbsp;Zajíček, Tomáš&nbsp;Kormaňák, Jan&nbsp;Pichl, Martin&nbsp;Matulík, Jakub&nbsp;Konrád, Petr&nbsp;Lorenc, Petr&nbsp;Marek, Ondřej&nbsp;Hrách, Vít&nbsp;Jakimiv " + gettext("and") + " Jan&nbsp;Šedivý", rich = true)
                        p("© 2019-${nowLocal().year} PromethistAI&nbsp;a.s. " + gettext("All rights reserved."), rich = true)
                    }
                }
            }
            tab(tr("What's new")).bind(data) {
                simplePanel(classes = setOf("nav-tabs-fixed")) {
                    it?.releases?.filter { it.items.isNotEmpty() }?.take(5)?.forEach { release ->
                        simplePanel(classes = setOf("logs")) {
                            h2(release.name)
                            add(release.items)
                        }
                    }
                }
            }
            tab(tr("Coming soon")).bind(data) {
                simplePanel(classes = setOf("nav-tabs-fixed")) {
                    if (it != null) {
                        simplePanel(classes = setOf("logs")) {
                            add(it.previewItems)
                        }
                    }
                }
            }
            tab(tr("Under construction")).bind(data) {
                simplePanel(classes = setOf("nav-tabs-fixed")) {
                    if (it != null) {
                        simplePanel(classes = setOf("logs")) {
                            add(it.openItems)
                        }
                    }
                }
            }
        }
    }

    private fun Container.add(items: MutableList<Changelog.Item>) {
        items.forEach { item ->
            simplePanel(classes = setOf("log")) {
                simplePanel(classes = setOf("log__avatar")) {
                    image(item.author.avatar_url)
                }
                simplePanel(classes = setOf("log__body")) {
                    simplePanel(classes = setOf("log__author")) {
                        link(item.author.name, item.author.web_url, target = "_blank")
                        span("authored ${Moment((item.merged ?: item.updated ?: item.created).toDate()).fromNow()}")
                    }
                    simplePanel(classes = setOf("log__title")) {
                        link(item.title, item.gitlab_url, target = "_blank")
                    }
                    simplePanel(classes = setOf("log__description")) {
                        p(item.description)
                    }
                }
            }
        }
    }

    fun show(tabIndex: Int): Widget {
        tabs.activeIndex = tabIndex
        return show()
    }

    override fun show(): Widget {
        scope.launch {
            data.value = ChangelogResource.load("https://services.promethist.ai/projects/23512224/changelog")
        }
        return super.show()
    }
}
