package ai.flowstorm.studio.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.File
import ai.flowstorm.studio.resources.space.FileAssetResource
import ai.flowstorm.studio.ui.design.FileAssetEditor
import ai.flowstorm.common.ui.EntityTable
import io.kvision.i18n.tr
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px
import kotlin.js.Date

class FileAssetTable: EntityTable<File>(
        caption = "File",
        model = FileAssetResource,
        tabulatorOptions = TabulatorOptions(
            layout = Layout.FITCOLUMNS,
            columns = listOf(
                ColumnDefinition(tr("Name"), File::label.name, width = "40%"),
                ColumnDefinition(tr("Type"), File::type.name, width = "25%"),
                ColumnDefinition(tr("Time"), File::stringTime.name, width = "35%", sorterFunction = sorterFunction<Date>(File::datetime.name))
            )
        ),
        entityComponentFactory = { FileAssetEditor() },
        entityFactory = { File(space_id = AppState.state.spaceId!!) },
        splitMode = true,
        tabWidth = 480.px,
        className = "content-panel",
        searchEnabled = true,
)
