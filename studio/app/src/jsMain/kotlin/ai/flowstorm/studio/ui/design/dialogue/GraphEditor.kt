package ai.flowstorm.studio.ui.design.dialogue

import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.model.Graph.*
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import ai.flowstorm.studio.resources.space.DialogueSnippetResource
import ai.flowstorm.studio.resources.space.FileAssetResource
import ai.flowstorm.studio.ui.design.dialogue.graph.*
import ai.flowstorm.studio.ui.design.dialogue.panel.*
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.*
import ai.flowstorm.common.ui.Hints.hint
import ai.flowstorm.common.ui.Icon
import io.kvision.core.Placement
import io.kvision.core.onEvent
import io.kvision.html.*
import io.kvision.i18n.gettext
import io.kvision.i18n.tr
import io.kvision.panel.*
import io.kvision.state.ObservableValue
import io.kvision.state.sub
import io.kvision.utils.perc
import io.kvision.utils.px
import kotlin.reflect.KClass

class GraphEditor : SimplePanel(), ReadOnly, ShortcutProvider,
    Stateful<GraphEditor.State> by StatefulValue(State()) {

    lateinit var graph: Graph
    lateinit var leftPanel: TabPanel
    lateinit var rightPanel: StackPanel
    lateinit var leftPanelWrapper: SimplePanel
    lateinit var diagramPanelWrapper: SimplePanel

    var dialogueId: Id? = null

    data class State(
        val language: String = "en"
    )

    override val shortcuts = listOf(
        Shortcut(tr("Undo"), "z", true) { undo() },
            Shortcut(tr("Redo"), "y", true) { redo() },
            Shortcut(tr("Toggle Left Panel"), "2") { toggleLeftPanel() },
            Shortcut(tr("Toggle Right Panel"), "3") { toggleRightPanel() },
            Shortcut(tr("Zoom In"), "+") { zoomIn() },
            Shortcut(tr("Zoom Out"), "-") { zoomOut() },
            Shortcut(tr("Fit to viewport"), ".") { fitToViewport() },
            Shortcut(tr("Reset size"), "/") { resetSize() },
            Shortcut(tr("Find node"), "f") { find() },
            Shortcut(tr("Edit node properties"), "e") { edit() },
            Shortcut(tr("Edit node texts"), "c") { editCode() },
            Shortcut(tr("Focus node"), "Enter") { focusNode() },
    )

    val leftPanelEnabled = ObservableValue(true)
    val rightPanelEnabled = ObservableValue(true)

    val history = GraphHistory()
    private val nodeTypePanel = NodeTypePanel(this)
    private val fileAssetPanel = FileAssetPanel(this, object : ObjectList.ObjectProvider<Node> {
        override suspend fun getItems() = mutableListOf<Node>().apply {
            FileAssetResource.load(Query(limit = 5000)).forEach { asset ->
                val type = asset.type.substring(0, asset.type.indexOf('/'))
                when (type) {
                    "image" -> Graph.Image(label = asset.name!!, asset_id = asset._id, source = asset.source, asset = asset)
                    "audio" -> Sound(label = asset.name!!, asset_id = asset._id, source = asset.source, asset = asset)
                    else -> null
                }?.let {
                    add(it)
                }
            }
        }
    })
    private val mixinPanel = DialogueMixinPanel(this, object : ObjectList.ObjectProvider<Node> {
        override suspend fun getItems() = mutableListOf<Node>().apply {
            DialogueMixinResource.load(language = state.language).forEach { mixin ->
                when (mixin.type) {
                    "Intent" -> Intent(label = mixin.label, mixins_id = listOf(mixin._id))
                    "Speech" -> Speech(label = mixin.label, mixins_id = listOf(mixin._id))
                    "Function" -> Function(label = mixin.label, mixins_id = listOf(mixin._id))
                    "UserInput" -> UserInput(label = mixin.label, mixins_id = listOf(mixin._id))
                    else -> null
                }?.let {
                    add(it)
                }
            }
        }
    })
    private val snippetPanel = DialogueSnippetPanel(this, object : ObjectList.ObjectProvider<DialogueSnippet> {
        override suspend fun getItems(): List<DialogueSnippet> {
           return DialogueSnippetResource.load(language = state.language!!)
        }
    })

    private val searchPanel = NodeSearchPanel(this, object : ObjectList.ObjectProvider<Node> {
        override suspend fun getItems() = if (::graph.isInitialized) graph.nodes else listOf()
    })
    val diagramPanel = DiagramPanel(this)
    private val editors: Map<KClass<*>?, Editor> = mapOf(
        GlobalIntent::class to IntentEditor(this).init(),
        Intent::class to IntentEditor(this).init(),
        Action::class to ActionEditor(this).init(),
        GlobalAction::class to ActionEditor(this).init(),
            Command::class to CommandEditor(this).init(),
            Speech::class to SpeechEditor(this).init(),
            Sound::class to SoundEditor(this).init(),
            Graph.Image::class to ImageEditor(this).init(),
            Function::class to FunctionEditor(this).init(),
            UserInput::class to UserInputEditor(this).init(),
            ReInput::class to ReInputEditor(this).init(),
            GoBack::class to GoBackEditor(this).init(),
            Sleep::class to SleepEditor(this).init(),
            SubDialogue::class to SubDialogueEditor(this).init(),
            Group::class to GroupEditor(this).init(),
            Graph.Link::class to LinkEditor(this),
            null to object : Span(), Editor {
                override fun load(item: Any) {}
            }
    )

    val selection get() = diagramPanel.diagram.state.selection
    var draggedNode: Node? = null
    var draggedGraph: Graph? = null

    private lateinit var searchPanelTab: Tab

    init {
        splitPanel {
            id = "graphEditor"
            width = 100.perc
            leftPanelWrapper = simplePanel {
                width = 12.perc
                width = 280.px
                leftPanel = tabPanel(classes = setOf("sub-nav"), scrollableTabs = true) {
                    tab("", nodeTypePanel, icon = Icon.Node.def) {
                        hint("NodeTypePanel", Icon.Node, Placement.BOTTOM, index = 216)
                    }
                    tab("", mixinPanel, icon = Icon.DialogueMixin.def) {
                        hint("MixinPanel", Icon.DialogueMixin, Placement.BOTTOM, index = 217)
                    }
                    tab("", fileAssetPanel, icon = Icon.FileAsset.def) {
                        hint("FileAssetPanel", Icon.FileAsset, Placement.BOTTOM, index = 218)
                    }
                    tab("", snippetPanel, icon = Icon.Graph.def) {
                        hint("SnippetPanel", Icon.Graph, Placement.BOTTOM, index = 219)
                    }
                    searchPanelTab = tab("", searchPanel, icon = Icon.Search.def) {
                        hint("SearchPanel", Icon.Search, Placement.BOTTOM, index = 220)
                    }
                    onEvent {
                        tabChange = {
                            val c = this@tabPanel.activeTab?.component
                            (c as? ObjectList<*>)?.search()
                        }
                    }
                }
            }
            simplePanel(classes = setOf("designer-panel")) {
                splitPanel {
                    diagramPanelWrapper = simplePanel {
                        width = 67.perc
                        add(diagramPanel)
                    }
                    rightPanel = stackPanel(classes = setOf("context-panel")) {
                        editors.values.forEach { add(it) }
                    }
                    onEvent {
                        dragEndSplitPanel = {
                            console.log("drag")
                            updateView()
                        }
                    }
                }
            }
            onEvent {
                dragEndSplitPanel = {
                    updateView()
                }
            }
        }
    }

    override var readOnly: Boolean = true
        set(value) {
            diagramPanel.readOnly = value
            diagramPanel.diagram.readOnly = value
            editors.values.forEach { if (it is ReadOnly) it.readOnly = value }
            field = value
        }

    init {
        diagramPanel.diagram.sub {it.selectedItem }.subscribe {activateEditor(it) }
        diagramPanel.diagram.onModelChange { commit(gettext("Graph structure changed")) }
    }

    private fun activateEditor(item: Any?) {
        val editor = item?.let {
            if (editors.containsKey(it::class)) editors.get(it::class) else null
        } ?: editors.getValue(null)

        rightPanel.activeChild = editor
        item?.let { editor.load(item) } //load needs to happen after setting active child
    }

    fun toggleLeftPanel() {
        leftPanelEnabled.value = !leftPanelEnabled.value
        leftPanelWrapper.width = (if (leftPanelEnabled.value) 220 else 0).px
        updateView()
    }

    fun toggleRightPanel() {
        rightPanelEnabled.value = !rightPanelEnabled.value
        diagramPanelWrapper.width = (if (rightPanelEnabled.value) 67 else 100).perc
        updateView()
    }

    fun load(graph: Graph, isNew: Boolean = true, dialogueId: Id? = null) {
        this.dialogueId = dialogueId
        this.graph = graph
        reload(if (isNew) gettext("Created") else gettext("Saved version"))
    }

    fun reload(commitMessage: String) {
        diagramPanel.load(graph)
        deselect()
        commit(commitMessage)
    }

    fun openHistory() {
        HistoryModal(this).open()
    }

    fun commit(note: String, squashId: String? = null) = history.commit(graph, note, squashId)

    fun restore(rev: Int) {
        history.restore(rev)
        reloadCurrent()
    }

    fun openRevision(rev: Int) {
        history.rev = rev
        reloadCurrent()
    }

    fun undo() {
        if (!history.canUndo.value) return
        history.undo()
        reloadCurrent()
    }

    fun redo() {
        if (!history.canRedo.value) return
        history.redo()
        reloadCurrent()
    }

    fun deselect(){
        diagramPanel.diagram.clearSelection()
    }

    private fun reloadCurrent() {
        deselect()
        graph.replace(history.current().clone())
        diagramPanel.diagram.load(graph)
    }

    fun copyToClipboard() {
        if (selection == null) return
        GraphClipboard.graph.value = selection!!.clone()
    }

    fun pasteFromClipboard() {
        if (readOnly) return
        val g = GraphClipboard.graph.value!!.clone()
        moveGraphToCenter(g)
        val idMap = graph.merge(g)
        reload(gettext("Graph structure changed"))
        if (graph.nodes.size == 1)
            diagramPanel.diagram.selectNode(graph.nodes[0], center = false, scaleViewport = false)
        else if (graph.nodes.size > 1)
            diagramPanel.diagram.selectGraph(g, idMap)
        diagramPanel.diagram.bringGraphToFront(g, idMap)
    }

    fun delete() {
        if (selection == null) return
        graph.delete(selection!!)
        deselect()
        reload(gettext("Graph structure changed"))
    }

    fun find() {
        leftPanel.activeTab = searchPanelTab
        searchPanel.searchInput.focus()
    }

    fun edit() {
        val activeChild = rightPanel.activeChild
        if (activeChild is NodeEditor<*>) {
            activeChild.edit()
        }
    }

    fun editCode() {
        val activeChild = rightPanel.activeChild
        if (activeChild is EditableCode) {
            (activeChild as NodeEditor<*>).apply {
                tabPanel.activeIndex = 1
            }
            activeChild.codeEditor.focus()
        }
    }

    private fun moveGraphToCenter(graph: Graph) {
        val mostLeft = graph.nodes.filterNot { it is Group }.map { it.loc!!.substringBefore(" ").toDouble() }.minOrNull() ?: 0.0
        val mostTop = graph.nodes.filterNot{it is Group}.map { it.loc!!.substringAfter(" ").toDouble() }.minOrNull() ?: 0.0
        val bounds = diagramPanel.diagram.goGraph.diagram.viewportBounds
        val point = diagramPanel.diagram.snapToGrid(Diagram.Point(bounds.centerX - mostLeft, bounds.centerY - mostTop ))
        graph.move(point.x, point.y)
    }

    fun zoomIn() = diagramPanel.diagram.zoomIn()
    fun zoomOut() = diagramPanel.diagram.zoomOut()
    fun resetSize() = diagramPanel.diagram.resetSize()
    fun fitToViewport() = diagramPanel.diagram.fitGraphToViewport()
    fun focusNode() = diagramPanel.diagram.centerOnSelectedNode()
    fun updateView() = diagramPanel.diagram.updateView()
}
