package ai.flowstorm.studio.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class User(
    override val _id: Id = newId(),
    var username: String? = "",
    var name: String? = null,
    var surname: String? = null,
    var nickname: String? = null,
    var phoneNumber: String? = null,
    var language: String? = null,
    var lastTalk: Instant? = null,
    var excludePublicApps: Boolean = false,
    var shouldLaunchAssistant: Boolean = true,
    var space_id: Id? = null,
    var attributes: JsonElement? = null,
    var seenHints: List<String> = listOf()
) : AbstractEntity() {
    override val label
        get() = asString(name, surname, username)

    companion object {
        fun asString(name: String?, surname: String?, username: String?) =
                "$name $surname <" +
                        (if ((username != null) && username.endsWith("flowstorm.ai"))
                            username.substring(0, username.indexOf('@'))
                        else
                            username?.let {
                                if (it.length > 20)
                                    it.substring(0, 20) + "..."
                                else
                                    it
                            } ?: ""
                        ) + ">".trim()
    }
}