package ai.flowstorm.studio.ui.design

import ai.flowstorm.studio.model.Simulation
import ai.flowstorm.common.ui.ModelDesigner
import ai.flowstorm.common.ui.Panel
import ai.flowstorm.common.id.Id

class SimulationDesigner : ModelDesigner<Simulation>(), Panel {

    override val caption = "Journey Simulator"

    override suspend fun getNewModel() = Simulation.Factory.empty()

    override suspend fun load(model: Simulation) {
        val editor = SimulationEditor(this@SimulationDesigner)
        editor.load(model)
        addTab(model.name, editor)
    }

    override suspend fun load(id: Id) {
        TODO("Not yet implemented")
    }

    override suspend fun new() {
        TODO("Not yet implemented")
    }

    override suspend fun open() {
        TODO("Not yet implemented")
    }
}