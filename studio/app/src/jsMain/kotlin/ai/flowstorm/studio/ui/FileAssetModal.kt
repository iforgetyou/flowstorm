package ai.flowstorm.studio.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.File
import ai.flowstorm.studio.ui.design.FileAssetEditor
import ai.flowstorm.common.ui.EntityEditorModal
import io.kvision.core.Widget
import io.kvision.modal.ModalSize

object FileAssetModal : EntityEditorModal<File>("File Asset", FileAssetEditor(false), size = ModalSize.LARGE) {

    override fun show(): Widget {
        component.load(File(space_id = AppState.state.spaceId!!))
        return super.show()
    }
}
