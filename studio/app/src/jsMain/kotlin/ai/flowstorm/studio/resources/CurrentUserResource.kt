package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.model.UserSpaceRole
import ai.flowstorm.common.resources.EntityResource
import ai.flowstorm.common.auth.DeviceId
import io.kvision.rest.HttpMethod
import kotlinx.coroutines.await
import kotlinx.coroutines.coroutineScope
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer

object CurrentUserResource : EntityResource<User>(resourceUri = "/user", serializer = User.serializer()) {

    suspend fun loadUserRoles() = remoteCall("/roles", deserializer = ListSerializer(UserSpaceRole.serializer()))
    suspend fun loadUserSpaces() = remoteCall("/spaces", deserializer = ListSerializer(Space.serializer()))

    suspend fun invitations() = remoteCall("/invitations", deserializer = ListSerializer(Invitation.serializer()))

    suspend fun getUser() = remoteCall("", deserializer = User.serializer())

    @Serializable
    data class Params(val deviceId: String)

    suspend fun signUp() = client.remoteCall(
        client.baseUrl + "$resourceUri/signup",
        data = Params(DeviceId.id),
        method = HttpMethod.POST,
        beforeSend = client.beforeSend,
        serializer = Params.serializer(),
    ).await()


    suspend fun signUpAnonymous() = client.remoteCall(
        client.baseUrl + "$resourceUri/signupAnonymous",
        method = HttpMethod.POST,
        beforeSend = client.beforeSend
    ).await()

    override suspend fun update(entity: User, doToast: Boolean) = coroutineScope {
        val e = remoteCall<User>("", entity, HttpMethod.PUT, doToast = doToast)
        replaceEntity(entity, e)
        e
    }
}