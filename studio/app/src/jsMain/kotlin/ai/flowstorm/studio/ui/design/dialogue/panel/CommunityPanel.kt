package ai.flowstorm.studio.ui.design.dialogue.panel

import ai.flowstorm.studio.ui.space.CommunityTable
import io.kvision.utils.perc

class CommunityPanel : CommunityTable(hideHeader = true, className = "panel-view-nav1") {
    init {
        height = 100.perc
    }
}