package ai.flowstorm.studio.ui.space

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.resources.space.ApplicationResource
import ai.flowstorm.studio.ui.RunnableModelEditor
import ai.flowstorm.studio.ui.design.dialogue.panel.RunPanel
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.EntityComponent
import ai.flowstorm.studio.ui.design.dialogue.panel.AssistantPanel
import com.github.snabbdom.VNode
import io.kvision.toolbar.ButtonGroup

class ApplicationRunnableEditor() :
        EntityComponent<Application>, RunnableModelEditor<Application, ApplicationEditor>(classes = setOf("designer")) {

    override val mainComponent: ApplicationEditor = ApplicationEditor(ApplicationForm(ApplicationResource))
    override val mainButtonGroup: ButtonGroup = ButtonGroup().apply {
        hide()
    }
    override val runPanel: RunPanel = RunPanel(this)
    override var savedVersion: Application = Application()

    init {
        addPanels(false)
    }

    override suspend fun saveAs() {
        console.log("not implemented")
    }

    override suspend fun save() {
        mainComponent.save()
    }

    override suspend fun openAsNew() {
        TODO("Not yet implemented")
    }

    override fun load(id: Id): Application {
        return mainComponent.load(id)
    }

    override fun afterDestroy() {
        super.afterDestroy()
        AssistantPanel.dialoguePanel.setControls(false)
    }

    override fun render(): VNode {
        AssistantPanel.dialoguePanel.botPanel = this.runPanel.botPanel
        AssistantPanel.dialoguePanel.setControls(true)
        return super.render()
    }

    override fun load(entity: Application) = mainComponent.load(entity)

    override var entity: Application? = Application()
        get() = mainComponent.entity
    override val name get() = mainComponent.applicationForm.form.fields["name"]?.getValueAsString() ?: "?"
    override val voice: Nothing? get() = undefined
    override val key get() = entity?._id?.value
}