package ai.flowstorm.studio.ui.design.dialogue.graph

import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.model.Graph.GoBack
import ai.flowstorm.studio.ui.design.dialogue.GraphEditor
import ai.flowstorm.common.ui.Icon
import io.kvision.form.check.CheckBox
import io.kvision.i18n.I18n.tr

class GoBackEditor(graphEditor: GraphEditor) : NodeEditor<GoBack>(graphEditor) {

    override val form = object : NodeForm(Graph.GoBack.serializer()) {
        init {
            add(GoBack::repeat, CheckBox(label = tr("Repeat after going back")))
        }
    }

    override val tabPanel = object : NodeTabPanel() {
        init {
            addTab(tr("Properties"), form, Icon.Properties.def)
        }
    }

    override fun load(node: GoBack) {
        super.load(node)
        form.load(node)
    }
}