package ai.flowstorm.studio.ui

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.state.RootState.Action.SpaceSelected
import ai.flowstorm.common.id.ObjectId
import ai.flowstorm.common.ui.Hints.hint
import ai.flowstorm.common.ui.Icon
import io.kvision.core.StringPair
import io.kvision.form.select.Select
import io.kvision.state.bind
import io.kvision.state.stateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object SpaceRoleSwitcher : Select() {

    init {
        hint("SpaceRoleSwitcher", Icon.Space, isMain = true)
        bind(AppState.sub { it.userRoles }) { roles ->
            val opts = roles.sortedWith(RootSpaceFirstComparator.thenBy { it.name }).map { StringPair(it.spaceId.value, "${it.name} · ${it.role}") }
            options = opts
            visible = opts.size > 1
        }
        bind(AppState.sub { it.spaceId }, false) {
            value = it?.value
        }

        stateFlow.onEach {
            it?.let {
                AppState.dispatch(SpaceSelected(ObjectId(it)))
                blur()
            }
        }.launchIn(App.scope)
    }
}
