package ai.flowstorm.studio.ui

import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.common.ui.EntityTable
import ai.flowstorm.studio.model.Space
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.utils.px
import io.kvision.i18n.I18n.tr

class SpaceTable: EntityTable<Space>(
        "Space",
        SpaceResource,
        TabulatorOptions(
                layout = Layout.FITCOLUMNS,
                columns = listOf(
                        ColumnDefinition(tr("Name"), Space::name.name)
                )
        ),
        { SpaceEditor() },
        { Space() },
        //deleteEnabled = false,
        splitMode = true,
        tabWidth = 240.px,
        className = "content-panel",
        searchEnabled = true,
)