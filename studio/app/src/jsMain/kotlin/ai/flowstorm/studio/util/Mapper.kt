package ai.flowstorm.studio.util

import io.kvision.types.DateSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlin.js.Date

object Mapper {
    val instance = Json {
        ignoreUnknownKeys = true
        isLenient = true //TODO try to set false and test it
        encodeDefaults = true
        serializersModule = SerializersModule {
            contextual(Date::class, DateSerializer)
        }
    }
}
