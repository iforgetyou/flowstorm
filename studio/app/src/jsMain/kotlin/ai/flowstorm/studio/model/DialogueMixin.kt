package ai.flowstorm.studio.model

import ai.flowstorm.studio.resources.SpaceResource
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.newId
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.serialization.Serializable

@Serializable
class DialogueMixin(
    override val _id: Id = newId(),
    val type: String? = null,
    val name: String? = null,
    val text: String? = null,
    val language: String? = null,
    var description: String = "",
    val space_id: Id
) : AbstractEntity() {
    override val label
        get() = SpaceResource.get(space_id).let {
            "[${it.name}]$name"
        }
}