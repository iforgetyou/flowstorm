package ai.flowstorm.common.ui

import io.kvision.modal.Modal
import io.kvision.modal.ModalSize

open class CanvasModal(caption: String) : Modal(caption, classes = setOf("modal-canvas"), size = ModalSize.LARGE)