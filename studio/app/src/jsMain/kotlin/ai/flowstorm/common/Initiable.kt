package ai.flowstorm.common

interface Initiable {

    fun init()

}