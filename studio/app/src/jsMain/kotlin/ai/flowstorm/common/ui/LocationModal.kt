package ai.flowstorm.common.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.core.ui.GoogleMap
import ai.flowstorm.core.type.Location
import kotlinx.coroutines.Job
import io.kvision.i18n.tr
import io.kvision.panel.hPanel
import io.kvision.panel.simplePanel
import io.kvision.utils.px

class LocationModal(var location: Location, editable: Boolean = false, caption: String = tr("Location")) : CanvasModal(caption) {

    private val open = Job()
    private val form = object : LocationForm(editable, {
        location = it
        open.complete()
    }) {
        init {
            width = 240.px
        }
    }
    private val googleMap = GoogleMap(
            processWaypoint = { processWaypoint(it) },
            clickable = editable,
            center = location,
            initMarkers = mutableListOf(location),
            zoom = 15,
            darkMode = AppState.state.ui.theme == "dark-mode"
    ) {
        width = 558.px
        height = 558.px
    }

    init {
        hPanel {
            add(form)
            simplePanel {
                add(googleMap)
            }
        }
        form.setData(location)
    }

    fun processWaypoint(value: dynamic) {
        val originalLocation = form.getData()
        val newLocation = Location(
                value.lat,
                value.lng,
                originalLocation.accuracy,
                originalLocation.altitude,
                originalLocation.altitudeAccuracy,
                originalLocation.speed,
                originalLocation.speedAccuracy,
                originalLocation.heading,
                originalLocation.headingAccuracy
        )
        form.setData(newLocation)


        googleMap.clearMarkers()
    }

    override fun show() = super.show().apply { googleMap.refresh() }

    suspend fun open(): Location {
        show()
        open.join()
        hide()
        return location
    }
}
