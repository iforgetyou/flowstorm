package ai.flowstorm.common.ui.form

import ai.flowstorm.common.model.IdPair
import ai.flowstorm.common.model.AbstractEntity
import kotlinx.coroutines.MainScope
import io.kvision.form.FieldLabel
import io.kvision.form.InvalidFeedback
import io.kvision.form.text.TextInput
import io.kvision.html.Button
import io.kvision.panel.SimplePanel

open class NamedEntityRef<E : AbstractEntity>(
        value: E? = null, name: String? = null, label: String? = null) :
        SimplePanel(setOf("form-group")), NamedEntityFormControl {

    override var idPair: IdPair? = null
    override var value: Any? = null
    override fun setValue(v: Any?) {
        super.setValue(v)
        input.value = idPair?.second
    }

    protected val linkButton = Button("", icon = "fas fa-external-link-alt")
    protected val mainScope = MainScope()
    private val idc = "kv_form_namedentityref_$counter"
    final override val input: TextInput = TextInput(value = value?.label, classes = setOf("form-control")).apply {
        this.id = idc
        this.name = name
        this.disabled = true
    }

    final override val flabel: FieldLabel = FieldLabel(idc, label, false)
    final override val invalidFeedback: InvalidFeedback = InvalidFeedback().apply { visible = false }

    init {
        @Suppress("LeakingThis")
        input.eventTarget = this
        this.addInternal(flabel)
        this.addInternal(input)
        this.addInternal(linkButton)
        this.addInternal(invalidFeedback)
        counter++
    }

    override fun focus() = input.focus()

    override fun blur() = input.blur()

    companion object {
        internal var counter = 0
    }
}