package ai.flowstorm.common.state

import io.kvision.state.ObservableValue

class StatefulValue<T>(state: T) : Stateful<T> {
    private val v = ObservableValue(state)
    override fun getState(): T = v.getState()
    override fun subscribe(observer: (T) -> Unit): () -> Unit = v.subscribe(observer)
    override var state by v::value
}