package ai.flowstorm.common.ui

import ai.flowstorm.common.ui.form.TextRef
import ai.flowstorm.common.ui.form.ListIdFormControl
import ai.flowstorm.common.ui.form.IdFormControl
import ai.flowstorm.common.ui.form.NamedEntityFormControl
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.AbstractEntity
import io.kvision.core.Container
import io.kvision.form.*
import com.github.snabbdom._get
import com.github.snabbdom._set
import kotlinx.coroutines.MainScope
import kotlinx.serialization.KSerializer
import kotlin.reflect.KProperty1

open class Form<E: Any>(serializer: KSerializer<E>, classes: Set<String> = setOf(),
                        type: FormType = FormType.HORIZONTAL, horizRatio: FormHorizontalRatio = FormHorizontalRatio.RATIO_4) :
        FormPanel<E>(serializer = serializer, classes = classes, type = type, horizRatio = horizRatio) {

    val mainScope = MainScope()

    var disabled: Boolean = false
        set(value) {
            if (value)
                disableAll()
            else
                enableAll()
            field = value
        }

    override fun <C : StringFormControl> add(
            key: KProperty1<E, String?>, control: C, required: Boolean, requiredMessage: String?,
            legend: String?,
            validatorMessage: ((C) -> String?)?,
            validator: ((C) -> Boolean?)?
    ) = super.add(key, control, required, requiredMessage, legend, validatorMessage, validator).also {
        if (control is TextRef)
            control.input.apply {
                removeCssClass("col-sm-8")
                addCssClass("col-sm-6")
            }
    }

    open fun <C : NamedEntityFormControl> add(
        key: KProperty1<E, AbstractEntity?>, control: C, required: Boolean = false, requiredMessage: String? = null,
        legend: String? = null,
        validatorMessage: ((C) -> String?)? = null,
        validator: ((C) -> Boolean?)? = null
    ) = addInternal(key, control, required, requiredMessage, legend, validatorMessage, validator).also {
        control.input.apply {
            removeCssClass("col-sm-8")
            addCssClass("col-sm-6")
        }
    }

    open fun <C : IdFormControl> add(
        key: KProperty1<E, Id?>, control: C, required: Boolean = false, requiredMessage: String? = null,
        legend: String? = null,
        validatorMessage: ((C) -> String?)? = null,
        validator: ((C) -> Boolean?)? = null
    ) = addInternal(key, control, required, requiredMessage, legend, validatorMessage, validator).also {
        control.input.apply {
            removeCssClass("col-sm-8")
            addCssClass("col-sm-6")
        }
    }

    open fun <C : ListIdFormControl> add(
        key: KProperty1<E, List<Id>>, control: C, required: Boolean = false, requiredMessage: String? = null,
        legend: String? = null,
        validatorMessage: ((C) -> String?)? = null,
        validator: ((C) -> Boolean?)? = null
    ) = addInternal(key, control, required, requiredMessage, legend, validatorMessage, validator)

    fun save(e: E) {
        form.fields.forEach {
            //workaround for non-nullable model properties, set empty string when form control returns null
            val value = if (e._get(it.key) is String)
                it.value.getValue() ?: ""
            else
                it.value.getValue()
            e._set(it.key, value)
        }
    }

    fun disableAll() = setDisabled(true)
    fun enableAll() = setDisabled(false)

    private fun setDisabled(disabled: Boolean) {
        form.fields.keys.forEach {
            form.fields[it]!!.disabled = disabled
        }
    }
}

inline fun <reified E : Any> Container.form(
        serializer: KSerializer<E>,
        noinline init: (Form<E>.() -> Unit)? = null
): Form<E> {
    val form = Form(serializer)
    init?.invoke(form)
    this.add(form)
    return form
}