package ai.flowstorm.common

interface Lockable {

    var lock: Boolean
}