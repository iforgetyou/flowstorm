package ai.flowstorm.common.ui

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.id.ObjectId
import kotlinx.browser.window
import ai.flowstorm.common.ui.Hints.hint
import io.kvision.jquery.*
import io.kvision.core.*
import io.kvision.html.*
import io.kvision.i18n.tr
import io.kvision.navbar.Navbar
import io.kvision.navbar.NavbarColor
import io.kvision.navbar.navbar
import io.kvision.panel.SimplePanel
import io.kvision.panel.StackPanel
import io.kvision.panel.simplePanel
import io.kvision.panel.stackPanel
import io.kvision.state.bind
import io.kvision.utils.px

abstract class RootPanel : SimplePanel(), ShortcutProvider {

    lateinit var navbar: Navbar
    var body: StackPanel
    val footerSpan = Span()
    open val homeUrl = "#!/"
    open val bodyComponent: Component get() = body.activeChild

    override val shortcuts: List<Shortcut> = listOf(
        Shortcut(tr("Show screen useful hints again"), "h") {
            Hints.show(Hints.Reset.SCREEN)
        },
        Shortcut(tr("Show ALL useful hints again"), "H") {
            Hints.show(Hints.Reset.ALL)
        })
        get() = field + ((bodyComponent as? ShortcutProvider)?.shortcuts ?: listOf())

    val footer = object : SimplePanel(classes = setOf("footer-panel")) {
        init {
            hint("FooterPanel", Icon.Footer, isMain = true, index = 300)
            simplePanel {
                span(App.config["version"] as? String + " ")
                add(footerSpan)
                icon(Icon.Help.def) {
                    hint("HintButton", Icon.Help, isMain = true, index = 301)
                    cursor = Cursor.POINTER
                    marginLeft = 6.px
                }.onClick {
                    Hints.show(Hints.Reset.SCREEN)
                }
            }
            simplePanel().bind(AppState.sub {it.userRoles}) {
                if (it.any { it.spaceId == ObjectId(App.config.idMap.rootSpace) }) {
                    visible = true
                    float = PosFloat.RIGHT
                    icon(Icon.Debug.def) {
                        cursor = Cursor.POINTER
                    }.onClick {
                        DebugModal().open()
                    }
                } else {
                    visible = false
                }
            }
        }
    }
    val usernav = UserNav()

    init {
        simplePanel(classes = setOf("navbar-panel")) {
            navbar = navbar(nColor = NavbarColor.DARK, bgColor = BsBgColor.TRANSPARENT) {
                link("", homeUrl, className = "navbar-logo")
            }
        }
        body = stackPanel(classes = setOf("stack-panel", "body-panel"), activateLast = false) {
        }
        add(footer)

        with(jQuery(window)) {
            // detect scroll to end of page
            scroll {
                val windowScrollTop = jQuery(kotlinx.browser.window).scrollTop().toInt()
                val offsetTop = jQuery("#root").offset().top.toInt()
                val outerHeight = jQuery("#root").outerHeight().toInt()
                val innerHeight = window.innerHeight
                if (windowScrollTop >= offsetTop + outerHeight - innerHeight) {
                    console.log("end of page")
                    if (bodyComponent is Seekable)
                        (bodyComponent as Seekable).loadMore()
                }
            }
        }
    }
}
