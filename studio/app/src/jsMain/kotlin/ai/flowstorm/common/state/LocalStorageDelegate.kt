package ai.flowstorm.common.state

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.IdFactory
import kotlinx.browser.localStorage
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

class LocalStorageDelegate<V : Any>(private val type: KClass<V>, private val namespace: String? = null) {

    operator fun getValue(thisRef: Any, property: KProperty<*>): V? =
        localStorage.getItem("state:${namespace ?: thisRef::class.simpleName}.${property.name}")?.let { value ->
            val v: Any = when (type) {
                Int::class -> value.toInt()
                Float::class -> value.toFloat()
                Double::class -> value.toDouble()
                Boolean::class -> value.toBoolean()
                String::class -> value
                Id::class -> IdFactory.create(value)
                else -> error("Unsupported local storage value type: $type")
            }
            @Suppress("UNCHECKED_CAST")
            v as V
        }


    operator fun setValue(thisRef: Any, property: KProperty<*>, any: V?) {
        if (any != null) {
            val value = when(any) {
                is Id -> any.value
                else -> any.toString()
            }
            localStorage.setItem("state:${namespace ?: thisRef::class.simpleName}.${property.name}", value)
        }
    }
}

inline fun <reified V: Any> Any.localStorage(namespace: String? = null) = LocalStorageDelegate(V::class, namespace)
