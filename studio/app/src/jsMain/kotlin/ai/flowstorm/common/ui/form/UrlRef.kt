package ai.flowstorm.common.ui.form

import kotlinx.browser.window
import org.w3c.dom.events.MouseEvent
import io.kvision.html.iframe
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize

class UrlRef(label: String, val attributes: Map<String, String> = mapOf(), readonly: Boolean = true) :
    TextRef(label = label, readonly = readonly) {

    override fun onButtonClick(e: MouseEvent) {
        if (e.shiftKey)
            window.open(value!!, "_blank")
        else
            Modal(label, size = ModalSize.XLARGE) {
                iframe(value, classes = setOf("web-view")).apply {
                    attributes.entries.forEach {(key, value) ->
                        this.setAttribute(key, value)
                    }
                }
            }.show()
    }
}