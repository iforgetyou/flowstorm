package ai.flowstorm.common.ui

import ai.flowstorm.common.model.Query
import ai.flowstorm.studio.model.DialogueSource
import io.kvision.form.FormPanel
import io.kvision.form.FormType
import io.kvision.form.text.Text
import io.kvision.i18n.tr
import kotlinx.serialization.Serializable

class DialogueSourceFilterForm : FormPanel<DialogueSourceFilterForm.FormData>(serializer = FormData.serializer(), type = FormType.HORIZONTAL), FilterForm {
    @Serializable
    data class FormData(val name: String?)

    init {
        add(
                FormData::name,
                Text(label = tr("Dialogue name")),
                required = true,
                requiredMessage = tr("Required field")
        )
    }

    override fun load(query: Query) {
        val name = query.filters.firstOrNull { it.field == DialogueSource::dialogueName.name }?.value as String?
        form.setData(FormData(name = name))
    }

    override fun save(query: Query) {
        val filter = form.getData()

        query.filters.clear()
        if (!filter.name.isNullOrEmpty()) {
            query.filters.add(Query.Filter(DialogueSource::dialogueName.name, Query.Operator.like, filter.name))
        }
    }
}