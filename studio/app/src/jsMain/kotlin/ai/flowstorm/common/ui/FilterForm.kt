package ai.flowstorm.common.ui

import ai.flowstorm.common.model.Query
import io.kvision.core.Component

interface FilterForm : Component {
    fun load(query: Query)
    fun save(query: Query)
}