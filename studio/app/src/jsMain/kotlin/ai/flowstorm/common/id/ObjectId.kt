package ai.flowstorm.common.id

data class ObjectId(override val value: String) : Id {
    init {
        require(isValid(value)) { "Invalid objectId [$value] " }
    }

    companion object {
        fun isValid(value: String) = value.matches("^[a-fA-F0-9]{24}$")
    }
}