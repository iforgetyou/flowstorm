package ai.flowstorm.common.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.isNew

interface Entity {
    val _id: Id

    val isNew get() = _id.isNew()
}