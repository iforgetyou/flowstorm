package ai.flowstorm.common.stateManager

import io.kvision.utils.JSON.toObj
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

abstract class AbstractStateManager<S>(private val mutableStateFlow: MutableStateFlow<S>) :
    StateManager<S>, StateFlow<S> by mutableStateFlow {

    protected abstract val scope: CoroutineScope

    private val mutableActionFlow = MutableSharedFlow<Action>()

    override val stateFlow = mutableStateFlow.asStateFlow() //expose immutable stateFlow
    override val actionFlow = mutableActionFlow.asSharedFlow() //expose immutable actionFlow
    override val state get() = stateFlow.value

    fun <T> sub(sub: (S) -> T): StateFlow<T> =
        this.map { sub(it) }.stateIn(scope, SharingStarted.WhileSubscribed(), sub(value))

    class WrappedAsyncAction<S>(action: AsyncAction<S>, job: CompletableJob) : AsyncAction<S> by action,
        CompletableJob by job

    override suspend fun dispatch(action: Action) = coroutineScope {
        log("Action dispatched: ${action.name}")
        if (action is AsyncAction<*>) {
            WrappedAsyncAction(action, Job(coroutineContext[Job])).let {
                mutableActionFlow.emit(it)
                it.join()
            }
        } else {
            mutableActionFlow.emit(action)
        }
    }

    suspend fun dispatch(name: String? = null, block: suspend StateManager<S>.() -> Unit) =
        dispatch(AsyncActionImpl(name, block))

    fun dispatchLaunch(action: Action) = scope.launch { dispatch(action) }
    fun dispatchLaunch(name: String? = null, block: suspend StateManager<S>.() -> Unit) =
        dispatchLaunch(AsyncActionImpl(name, block))

    suspend fun start() = coroutineScope {
        launch {
            launch(CoroutineName("Async Action Processor")) {
                mutableActionFlow.filterIsInstance<WrappedAsyncAction<S>>().collect { action ->
                    log("Launching async action: ${action.name}")
                    launch(CoroutineName("Async Action Coroutine for ${action.name}")) {
                        try {
                            with(action) {
                                start()
                                block.invoke(this@AbstractStateManager)
                                complete()
                            }
                        } catch (c: CancellationException) {
                            log("Async action canceled: ${action.name}, ${c.message}", c)
                            action.cancel(c)
                            throw c
                        } catch (t: Throwable) {
                            log("Async action failed: ${action.name}, ${t.message}", t)
                            action.completeExceptionally(t)
                        }
                    }
                }
            }

            launch(CoroutineName("State Reducer")) {
                mutableActionFlow.filterIsInstance<ReducerAction>().collect { action ->
                    log("Reducing action: ${action.name}", action)
                    mutableStateFlow.value = reduce(state, action)
                }
            }
        }
    }

    abstract val modules: List<StateModule<S, out Any, out ReducerAction>>

    open fun reduce(state: S, action: ReducerAction): S {
        val module = modules.firstOrNull { it.reducer.canReduce(action) } ?: error("Missing reducer")
        val reducer = module.reducer as StateReducer<Any, ReducerAction>
        val getter = module.getter
        val setter = module.setter as (S, Any) -> S

        return setter(state, reducer.reduce(getter(state), action))
    }

    protected open fun log(vararg o: Any?) {
        console.log(*prefixFirstMessage(o))
    }

    private fun prefixFirstMessage(list: Array<out Any?>): Array<Any?> {
        var msg = list.firstOrNull()

        if (msg is String) {
            msg = "SM $msg"
        }
        val res = list.drop(1).toMutableList()
        res.add(0, msg)
        return res.toTypedArray()
    }

    fun <RS, S, A : ReducerAction> StateManager<RS>.module(
        reducer: StateReducer<S, A>,
        getter: (state: RS) -> S,
        setter: (state: RS, s: S) -> RS,
    ) = object : StateModule<RS, S, A> {
        override val reducer: StateReducer<S, A> = reducer
        override val getter: (state: RS) -> S = getter
        override val setter: (state: RS, s: S) -> RS = setter
    }
}
