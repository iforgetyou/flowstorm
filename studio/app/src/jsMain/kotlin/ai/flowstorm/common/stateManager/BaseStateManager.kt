package ai.flowstorm.common.stateManager

import kotlinx.coroutines.flow.*

abstract class BaseStateManager<S>(initialState: S) : AbstractStateManager<S>(MutableStateFlow(initialState))
