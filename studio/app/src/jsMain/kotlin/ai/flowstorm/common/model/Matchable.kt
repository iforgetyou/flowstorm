package ai.flowstorm.common.model

interface Matchable {
    fun match(input: String): Boolean
}