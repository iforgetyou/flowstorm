package ai.flowstorm.common.ui

import ai.flowstorm.common.ui.StatefulModel.State.*
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.Cloneable
import kotlinx.coroutines.*
import ai.flowstorm.common.Closeable
import io.kvision.i18n.I18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.state.ObservableValue

abstract class ModelEditor<E : AbstractEntity>(classes: Set<String> = setOf()) : SimplePanel(classes), EntityComponent<E>, NamedComponent, ReadOnly, StatefulModel,
    Closeable {

    companion object {
        const val NAV_HEIGHT = 38
    }

    override var readOnly: Boolean = true

    val mainScope = MainScope()
    abstract var savedVersion: E
    override val modelState = ObservableValue(New)

    private val checkStateJob = mainScope.launch {
        while (isActive) {
            delay(1000)
            var state = modelState.value
            if (entity == savedVersion && savedVersion.isNew) {
                state = New
            } else if (entity == savedVersion) {
                if (state != Built)
                    state = Saved
            } else {
                state = Changed
            }

            if (state != this@ModelEditor.modelState.value) {
                this@ModelEditor.modelState.value = state
            }
        }
    }

    override suspend fun close() = coroutineScope {
        var close = true

        if (modelState.value != Saved && modelState.value != Built) {
            close = ConfirmModal(tr("Close"), tr("You have unsaved changes that will be lost.")).open()
        }

        if (close) {
            checkStateJob.cancel()
        } else {
            cancel()
        }
    }

    override fun load(entity: E): E {
        modelState.value = Saved
        savedVersion = if (entity is Cloneable<*>) (entity as Cloneable<E>).clone() else entity
        this.entity = entity
        return entity
    }

    override fun load(id: Id): E = TODO("not implemented")

    abstract suspend fun save()
    abstract suspend fun saveAs()
    abstract suspend fun openAsNew()
    open suspend fun delete() {}
}