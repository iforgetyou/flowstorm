package ai.flowstorm.common.ui.form

import io.kvision.form.FieldLabel
import io.kvision.form.FormControl
import io.kvision.form.InvalidFeedback
import io.kvision.panel.SimplePanel

abstract class CustomFormControl(label: String? = null, rich: Boolean = false) :
        SimplePanel(setOf("form-group")), FormControl {

    protected val idc = "kv_form_custom_${counter}"
    final override val flabel: FieldLabel = FieldLabel(idc, label, rich)
    final override val invalidFeedback: InvalidFeedback = InvalidFeedback().apply { visible = false }

    fun addComponents() {
        @Suppress("LeakingThis")
        this.addInternal(flabel)
        this.addInternal(input)
        this.addInternal(invalidFeedback)
        counter++
    }

    override fun focus() = input.focus()

    override fun blur() = input.blur()

    companion object {
        internal var counter = 0
    }
}