package ai.flowstorm.common.stateManager

open class AsyncActionImpl<S>(name: String? = null, block: (suspend StateManager<S>.() -> Unit)? = null) :
    AsyncAction<S> {
    override val name = name ?: super.name
    override val block: (suspend StateManager<S>.() -> Unit) = block!!
}