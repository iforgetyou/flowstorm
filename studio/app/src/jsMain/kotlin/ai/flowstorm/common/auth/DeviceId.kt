package ai.flowstorm.common.auth

import ai.flowstorm.common.state.cookieStorage
import io.kvision.utils.obj
import kotlinx.browser.window
import kotlin.math.absoluteValue
import kotlin.random.Random

object DeviceId {

    val domain = if(!window.location.hostname.contains("localhost")) "flowstorm.ai" else undefined

    private var _id: String? by cookieStorage("deviceId", obj { expires = 3650; this.domain = domain })

    val id get() = _id!!

    init {
        if (_id == null) _id = createID()
    }

    private fun createID() = Random.nextInt().absoluteValue.toString(36)
}