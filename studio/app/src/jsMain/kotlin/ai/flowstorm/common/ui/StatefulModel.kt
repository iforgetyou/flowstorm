package ai.flowstorm.common.ui

import io.kvision.state.ObservableValue

interface StatefulModel {
    val modelState: ObservableValue<State>

    enum class State { New, Saved, Built, Changed }
}