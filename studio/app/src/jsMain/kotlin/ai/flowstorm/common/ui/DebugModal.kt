package ai.flowstorm.common.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.state.RootState
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.auth.AuthService
import io.kvision.core.onEvent
import io.kvision.html.button
import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.simplePanel
import io.kvision.panel.tab
import io.kvision.panel.tabPanel

class DebugModal : Modal(tr("Debug Tools"), size = ModalSize.LARGE, scrollable = true) {

    init {
        closeIcon.onEvent {
            click = { cancel() }
        }

        tabPanel {
            tab("Auth log") {
                add(AuthLog)
                simplePanel {
                    button("Refresh Token").onClick {
                        AuthLog.timeLog("Refreshing.")
                        MainScope().launch { AuthService.refreshToken() }
                    }
                }
            }
            tab("Application State") {
                add(JsonEditor(RootState.State.serializer()).apply { setData(AppState.state) })
            }
            tab("State log") {
                add(StateLog)
            }
        }
    }

    fun open() {
        show()
    }

    fun cancel() {
        hide()
    }
}
