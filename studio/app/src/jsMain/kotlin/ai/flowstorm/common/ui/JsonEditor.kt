package ai.flowstorm.common.ui

import ai.flowstorm.studio.util.Mapper
import com.github.snabbdom.VNode
import io.kvision.panel.SimplePanel
import io.kvision.utils.obj
import io.kvision.utils.px
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.decodeFromDynamic
import kotlinx.serialization.json.encodeToDynamic

open class JsonEditor<E : Any>(
    val serializer: KSerializer<E>? = null, // setting serializer to null results in using data object as is, which may leads to unexpected behaviour
    val options: Options = Options(),
    classes: Set<String> = setOf()
) :
    SimplePanel(classes = classes) {
    var editor: dynamic = null

    lateinit var data: E

    private val mapper by Mapper::instance

    init {
        if (classes.isEmpty())
            height = 500.px
    }

    @Suppress("UNUSED_VARIABLE")
    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        val element = node.elm
        val opt = options.toJs()
        editor = js("new JSONEditor(element, opt);")
        if (this::data.isInitialized)
            set()
    }

    private fun set() {
        editor?.set(if (serializer != null) mapper.encodeToDynamic(serializer, data) else data)
    }

    fun setData(data: E) {
        this.data = data
        set()
    }

    fun getData(): E {
        return if (serializer != null) mapper.decodeFromDynamic(serializer, data) else data as E
    }

    data class Options(val mode: Mode = Mode.VIEW, val modes: List<Mode> = listOf(Mode.VIEW, Mode.PREVIEW)) {
        fun toJs() = obj {
            this.mode = mode.name.lowercase()
            this.modes = modes.map { it.name.lowercase() }.toTypedArray()
        }
    }

    enum class Mode { VIEW, FORM, CODE, TEXT, TREE, PREVIEW }
}
