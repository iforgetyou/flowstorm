package ai.flowstorm.common.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.ui.UiState.Action.ToggleBotPanel
import ai.flowstorm.common.BotLogic
import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import io.kvision.core.Component
import io.kvision.core.Container
import io.kvision.i18n.tr
import io.kvision.panel.SplitPanel
import io.kvision.panel.StackPanel
import io.kvision.panel.simplePanel
import io.kvision.state.bind
import io.kvision.state.subFlow
import io.kvision.utils.perc

class BotApp(init: (BotApp.() -> Unit)? = null) : SplitPanel(classes = setOf("botapp")), ShortcutProvider {

    lateinit var mainComponent: Component
    lateinit var botPanel: Component

    override val shortcuts: List<Shortcut> = listOf(
        Shortcut(
            tr("Toggle bot panel"),
            "B"
        ) { AppState.dispatchLaunch(ToggleBotPanel()) },
    )
        get() = field +
                listOf(
                    mainComponent,
                    (mainComponent as? StackPanel)?.activeChild,
                    botPanel,
                ).filterIsInstance<ShortcutProvider>().map { it.shortcuts }.flatten()

    init {
        @Suppress("LeakingThis")
        init?.invoke(this)

        width = 100.perc
        simplePanel {
            width = 100.perc
            add(mainComponent)
        }.bind(AppState.subFlow { it.ui.botPanelOpened }, false) {
            width = if (it) {
                78.perc
            } else {
                BotLogic.stop()
                100.perc
            }
        }
        simplePanel(classes = setOf("bot-panel", "wallpaper")) {
            add(botPanel)
        }
    }
}

fun Container.botApp(
    init: (BotApp.() -> Unit)? = null
): BotApp {
    val app = BotApp(init)
    this.add(app)
    return app
}
