package ai.flowstorm.common.auth

data class AuthContext(
    val idToken: JwtToken<IdToken>,
    val accessToken: JwtToken<AccessToken>,
    val isAnonymous: Boolean
) {
    val username by idToken.payload::email
    val name by idToken.payload::name
    val emailVerified by idToken.payload::email_verified

    interface IdToken {
        val name: String
        val email: String
        val picture: String
        val email_verified: Boolean
    }

    interface AccessToken //has no payload
}
