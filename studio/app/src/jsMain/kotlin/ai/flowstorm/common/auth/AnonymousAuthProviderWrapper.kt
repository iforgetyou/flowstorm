package ai.flowstorm.common.auth

import ai.flowstorm.studio.resources.AuthResource

class AnonymousAuthProviderWrapper(private val actualProvider: JwtAuthProvider) : JwtAuthProvider by actualProvider {

    override var authService = AuthService
        set(value) {
            field = value
            actualProvider.authService = value
        }

    override suspend fun authenticate() {
        actualProvider.authenticate()
        if (!authService.isAuthenticated) {

            val tokens = AuthResource.auth(DeviceId.id)
            authService.context = AuthContext(JwtToken(tokens.id), JwtToken(tokens.access), true)
        }
    }
}