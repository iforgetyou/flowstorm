package ai.flowstorm.common.ui

import io.kvision.core.Component
import io.kvision.core.ResString
import io.kvision.panel.Tab
import io.kvision.panel.TabPanel

val Tab.component: Component
    get() {
        require(getChildren().size == 1)
        return getChildren().first()
    }

fun TabPanel.tab(
        label: String? = null, child: Component, icon: String? = null,
        image: ResString? = null, closable: Boolean = false, route: String? = null,
        init: (Tab.() -> Unit)? = null
): Tab {
    val tab = Tab(label, child, icon, image, closable, route, init)
    this.add(tab)
    return tab
}

val TabPanel.activeChild: Component? get() = activeTab?.component
