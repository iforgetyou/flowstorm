package ai.flowstorm.common

import ai.flowstorm.studio.common.state.AppState
import io.kvision.jquery.invoke
import io.kvision.jquery.jQuery
import kotlinx.browser.document
import org.w3c.dom.events.KeyboardEvent

object ShortcutHandler {

    private lateinit var shortcutProvider: ShortcutProvider

    fun init(shortcutProvider: ShortcutProvider) {
        this.shortcutProvider = shortcutProvider
        registerEvent()
    }

    private fun registerEvent() {
        with(jQuery(document)) {
            keydown {
                if (!listOf("TEXTAREA", "INPUT").contains(it.target.nodeName)) {
                    onKeyDown(it.originalEvent as KeyboardEvent)
                }
            }
        }
    }

    private fun onKeyDown(e: KeyboardEvent) {
        //hotfix, disable shortcuts until app is initialized
        if (!AppState.state.appInitialized) return

        val shortcut =
            shortcutProvider.shortcuts.lastOrNull { it.key == e.key && it.ctrlKey == e.ctrlKey && it.metaKey == e.metaKey }
        shortcut?.let {
            console.log("Shortcut pressed ", it)
            e.preventDefault()
            it.handler()
        }
    }
}
