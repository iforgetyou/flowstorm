package ai.flowstorm.common.stateManager

interface AsyncAction<S> : Action {
    val block: (suspend StateManager<S>.() -> Unit)
}

