package ai.flowstorm.common.ui

interface Navigable {
    fun onNavigate(route: String, params: dynamic)
}