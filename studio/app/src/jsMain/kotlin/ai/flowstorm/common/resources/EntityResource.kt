package ai.flowstorm.common.resources

import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.isNew
import ai.flowstorm.common.model.Query
import kotlinx.coroutines.coroutineScope
import kotlinx.serialization.*
import io.kvision.rest.HttpMethod
import io.kvision.state.ObservableList
import io.kvision.state.observableListOf
import kotlinx.serialization.builtins.ListSerializer

abstract class EntityResource<E : HasId>(
    resourceUri: String = "/",
    serializer: KSerializer<E>,
    val entities: ObservableList<E> = observableListOf()
) : AbstractResource<E>(resourceUri, serializer) {

    private fun reset(data: List<E>, append: Boolean = false) {
        if (append) {
            entities.addAll(data)
        } else {
            val s1 = entities.size
            val s2 = data.size
            if (s2 < s1)
                for (i in s2 until s1)
                    entities.removeAt(s2)
            for (i in 0 until s2) {
                val e = data[i]
                if (i < s1)
                    entities[i] = e
                else
                    entities.add(e)
            }
        }
    }

    fun clear() = entities.clear()

    fun first() = entities.first()

    private fun lastId(): Id? {
        return entities.last().id
    }

    open fun createEntityUri(entity: E? = null): String = entity?.id?.takeIf { !it.isNew() }?.let { "/${it.value}" } ?: ""

    suspend fun reload(query: Query) = coroutineScope {
        val e = load(query)
        reset(e)
    }

    suspend fun loadMore(query: Query) = coroutineScope {
        val e = load(query)
        reset(e, true)
    }

    open suspend fun load(id: Id, useCache: Boolean = true): E = coroutineScope {
        val e = entities.find { it.id == id }
        if (e != null && useCache) {
            e
        } else {
            remoteCall<E>("/${id.value}", null, HttpMethod.GET).also { newEntity ->
                if (e != null) {
                    replaceEntity(e, newEntity)
                } else {
                    entities.add(newEntity)
                }
            }
        }
    }

    fun get(id: Id): E = entities.find { it.id == id }
        ?: throw NoSuchElementException("Entity not loaded. [${this::class.simpleName} ${id.value}]")

    suspend fun create(entity: E) = coroutineScope {
        val e = remoteCall<E>(createEntityUri(entity), entity, HttpMethod.POST)
        entities.add(e)
        e
    }

    open suspend fun update(entity: E, doToast: Boolean = true) = coroutineScope {
        val e = remoteCall<E>(createEntityUri(entity), entity, HttpMethod.PUT, doToast = doToast)
        replaceEntity(entity, e)
        e
    }

    fun replaceEntity(entity: E, new: E) {
        val index = entities.map { it.id }.indexOf(entity.id)
        if (index >= 0)
            entities[index] = new
    }

    fun addEntity(entity: E) {
        with(entities) {
            if (!contains(entity)) add(entity)
        }
    }

    suspend fun delete(entity: E) = coroutineScope {
        remoteCall<Unit>(createEntityUri(entity), null, HttpMethod.DELETE)
        entities.find { it.id == entity.id }?.let { e ->
            entities.remove(e)
        }
        Unit
    }

    override suspend fun load(query: Query?) = coroutineScope {
        remoteCall(
            createEntityUri() + (query?.toString() ?: ""),
            null,
            HttpMethod.GET,
            deserializer = ListSerializer(serializer)
        )
    }
}