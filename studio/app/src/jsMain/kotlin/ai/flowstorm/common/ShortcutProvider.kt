package ai.flowstorm.common

interface ShortcutProvider {

    val shortcuts: List<Shortcut>

    data class Shortcut(
            val name: String,
            val key: String,
            val ctrlKey: Boolean = false,
            val metaKey: Boolean = false,
            val handler: () -> Unit
    ) {
        val shortcut = mutableListOf<String>().let {
            if (ctrlKey) it.add("CTRL")
            if (metaKey) it.add("META")
            it.add(key)
            it.joinToString(" + ")
        }
    }
}