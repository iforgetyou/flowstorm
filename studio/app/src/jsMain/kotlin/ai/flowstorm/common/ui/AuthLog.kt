package ai.flowstorm.common.ui

import ai.flowstorm.studio.ui.design.dialogue.panel.LogPanel
import ai.flowstorm.studio.util.now
import kotlin.math.round

object AuthLog : LogPanel() {

    private val zeroTime: Double = timestamp()

    init {
        log("App started at $zeroTime")
    }

    fun timeLog(text: String, type: String = "dark"): LogPanel {
        val time = (round((timestamp() - zeroTime) * 100) / 100).asDynamic().toFixed(2)
        return log("[$time] $text", type)
    }

    private fun timestamp(): Double = now().toEpochMilliseconds().toDouble() / 1000 //convert to seconds with decimal point
}