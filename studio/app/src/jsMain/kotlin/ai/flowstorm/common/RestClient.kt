package ai.flowstorm.common

import ai.flowstorm.studio.App
import ai.flowstorm.studio.util.Mapper
import ai.flowstorm.common.auth.AuthService
import io.kvision.jquery.JQueryAjaxSettings
import io.kvision.jquery.JQueryXHR
import io.kvision.rest.HttpMethod
import io.kvision.rest.RestClient
import kotlinx.coroutines.await
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy

class RestClient(
    val baseUrl: String? = null,
    val beforeSend: ((JQueryXHR, JQueryAjaxSettings) -> Boolean)? = null
) : RestClient() {

    override val Json by Mapper::instance

    /**
     * Helper function to prefix url, add beforeSend and make call suspendable.
     */
    suspend fun <T : Any> remoteCall(
        url: String,
        deserializer: DeserializationStrategy<T>,
        method: HttpMethod
    ): T =
        remoteCall(
            url = (baseUrl ?: "") + url,
            deserializer = deserializer,
            method = method,
            beforeSend = beforeSend
        ).await()

    /**
     * Helper function to prefix url, add beforeSend and make call suspendable.
     */
    suspend fun <T : Any, V : Any> remoteCall(
        url: String,
        serializer: SerializationStrategy<V>,
        data: V,
        deserializer: DeserializationStrategy<T>,
        method: HttpMethod
    ): T = remoteCall(
        url = (baseUrl ?: "") + url,
        serializer = serializer,
        data = data,
        deserializer = deserializer,
        method = method,
        beforeSend = beforeSend
    ).await()

    companion object {
        val defaultBeforeSend: ((JQueryXHR, JQueryAjaxSettings) -> Boolean) = { xhr, ajaxSettings ->
            xhr.setRequestHeader("Authorization", "Bearer " + AuthService.context.accessToken.token)
            ajaxSettings.dataType = "json"
            true
        }

        fun createDefault(): ai.flowstorm.common.RestClient = RestClient(App.config.restUrl, defaultBeforeSend)
    }
}
