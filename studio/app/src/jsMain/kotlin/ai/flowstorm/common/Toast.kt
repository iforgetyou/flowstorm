package ai.flowstorm.common

import io.kvision.toast.Toast
import io.kvision.toast.ToastOptions
import io.kvision.toast.ToastPosition

/**
 * Wraps kvision Toast to use default options.
 */
object Toast {
    val defaultOptions = ToastOptions(positionClass = ToastPosition.BOTTOMCENTER, timeOut = 5000)

    fun success(message: String, title: String? = null, options: ToastOptions? = defaultOptions) = Toast.success(message, title, options)
    fun info(message: String, title: String? = null, options: ToastOptions? = defaultOptions) = Toast.info(message, title, options)
    fun warning(message: String, title: String? = null, options: ToastOptions? = defaultOptions) = Toast.warning(message, title, options)
    fun error(message: String, title: String? = null, options: ToastOptions? = defaultOptions) = Toast.error(message, title, options)
}