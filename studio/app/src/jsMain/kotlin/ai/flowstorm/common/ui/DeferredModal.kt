package ai.flowstorm.common.ui

import io.kvision.core.Widget
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.cancel

abstract class DeferredModal<T>(
    caption: String? = null,
    closeButton: Boolean = true,
    size: ModalSize? = null,
    animation: Boolean = true,
    centered: Boolean = false,
    scrollable: Boolean = false,
    escape: Boolean = true,
    classes: Set<String> = setOf(), init: (Modal.() -> Unit)? = null,
) : Modal(caption, closeButton, size, animation, centered, scrollable, escape, classes, init) {

    protected lateinit var deferred: CompletableDeferred<T>

    override fun hide(): Widget {
        if (::deferred.isInitialized) deferred.cancel("Modal closed")
        return super.hide()
    }

    abstract suspend fun open(): T
}
