package ai.flowstorm.common.ui

interface EntityEditorListener {
    fun onSave()
    fun onDelete()
}