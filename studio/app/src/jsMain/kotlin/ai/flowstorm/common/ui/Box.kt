package ai.flowstorm.common.ui

import io.kvision.core.Container
import io.kvision.core.CssSize
import io.kvision.html.*
import io.kvision.panel.SimplePanel
import io.kvision.panel.simplePanel

class Box(caption: String, icon: String?, height: CssSize, init: (SimplePanel.() -> Unit)? = null): SimplePanel() {
    lateinit var container: SimplePanel
    init {
        button(caption, icon, ButtonStyle.INFO).onClick {
            container.toggleVisible()
        }
        container = simplePanel {
            minHeight = height
            init?.invoke(this)
        }
    }
}

fun Container.box(caption: String, icon: String?, height: CssSize, init: (SimplePanel.() -> Unit)? = null): Box {
    val box = Box(caption, icon, height, init)
    this.add(box)
    return box
}