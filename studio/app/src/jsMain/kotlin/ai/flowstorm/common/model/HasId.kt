package ai.flowstorm.common.model

import ai.flowstorm.common.id.Id

interface HasId {

    /**
     * id is property used to identify entity by its client. it should be mapped onto appropriate persistent
     * unique identification property (e.g. _id coming from remote model)
     */
    val id: Id

}
