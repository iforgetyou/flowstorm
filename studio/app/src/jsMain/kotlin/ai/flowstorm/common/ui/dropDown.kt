package ai.flowstorm.common.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.model.Space
import io.kvision.dropdown.DropDown
import io.kvision.dropdown.ddLink
import io.kvision.html.Link
import io.kvision.i18n.tr
import io.kvision.state.bind

fun DropDown.ddLinkSecured(
    label: String,
    url: String,
    requiredRole: Space.Role? = null
): Link = ddLink(label = tr(label), url = url).apply {
    if (requiredRole != null)
        bind(AppState.sub { it.selectedRole }) {
            visible = it?.let { Space.Role.isAllowed(requiredRole, it.role) } ?: false
        }
}
