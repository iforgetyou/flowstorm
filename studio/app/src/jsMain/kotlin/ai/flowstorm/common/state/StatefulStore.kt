package ai.flowstorm.common.state

import io.kvision.state.sub

class StatefulStore<S, T>(
    private val store: Stateful<S>,
    private val getter: (S) -> T,
    private val setter: S.(T) -> S
) : Stateful<T> {

    override fun getState(): T = state

    override fun subscribe(observer: (T) -> Unit): () -> Unit = store.sub { getter(it) }.subscribe(observer)

    override var state: T
        get() = getter(store.state)
        set(value) = with(store) {
            state = setter.invoke(state, value)
        }
}
