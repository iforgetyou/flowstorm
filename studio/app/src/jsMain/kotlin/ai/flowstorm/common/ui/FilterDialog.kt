package ai.flowstorm.common.ui

import ai.flowstorm.common.model.Query
import io.kvision.html.button
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.simplePanel

class FilterDialog(val form: FilterForm, val query: Query, val onClose: () -> Any) : Modal() {
    init {
        form.load(query)

        caption = "Filters"
        size = ModalSize.LARGE

        add(form)
        simplePanel(classes = setOf("button-group")) {
            button("Reset").onClick { reset() }
            button("Save") {
                marginLeft = UiHelpers.spacing
            }.onClick { save() }
        }
    }

    fun save() {
        form.save(query)
        close()
    }

    private fun reset() {
        query.filters.clear()
        query.sort = null
        close()
    }

    private fun close() {
        onClose()
        hide()
    }
}
