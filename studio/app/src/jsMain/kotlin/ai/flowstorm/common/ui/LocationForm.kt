package ai.flowstorm.common.ui

import ai.flowstorm.core.type.Location
import io.kvision.form.FormHorizontalRatio
import io.kvision.form.spinner.Spinner
import io.kvision.html.Button
import io.kvision.i18n.tr

open class LocationForm (editable: Boolean = false, callback: (Location) -> Unit)  : Form<Location>(serializer = Location.serializer(), horizRatio = FormHorizontalRatio.RATIO_7) {
    init {
        add(
                Location::latitude,
                Spinner(label = tr("Latitude"), min = -85.0, max = 85.0, step = 0.01, decimals = 3).apply {
                    disabled = true
                }
        )
        add(
                Location::longitude,
                Spinner(label = tr("Longitude"), min = -180.0, max = 180.0, step = 0.01, decimals = 3).apply {
                    disabled = true
                }
        )
        add(Location::accuracy, Spinner(label = tr("Accuracy"), min = 0.0, max = 1000.0, step = 1.0, decimals = 0))
        add(Location::altitude, Spinner(label = tr("Altitude"), min = 0.0, max = 8000.0, step = 0.1, decimals = 1))
        add(Location::altitudeAccuracy, Spinner(label = tr("Altitude Acc."), min = 0.0, max = 1000.0, step = 0.1, decimals = 1))
        add(Location::speed, Spinner(label = tr("Speed"), min = 0.0, max = 400.0, step = 0.1, decimals = 1))
        add(Location::speedAccuracy, Spinner(label = tr("Speed Acc."), min = 0.0, max = 10.0, step = 0.1, decimals = 1))
        add(Location::heading, Spinner(label = tr("Heading"), min = 0.0, max = 359.9, step = 0.1, decimals = 1))
        add(Location::headingAccuracy, Spinner(label = tr("Heading Acc."), min = 0.0, max = 10.0, step = 0.1, decimals = 1))
        if (editable){
            add(Button(tr("Update")).onClick {
                callback(getData())
            })
        } else {
            disableAll()
        }
    }
}