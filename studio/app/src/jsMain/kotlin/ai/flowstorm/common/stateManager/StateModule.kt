package ai.flowstorm.common.stateManager

interface StateModule<SS, S, A : ReducerAction> {
    val reducer: StateReducer<S, A>
    val getter: (state: SS) -> S
    val setter: (state: SS, s: S) -> SS
}
