package ai.flowstorm.common

import kotlinx.browser.window

object WindowUnloadHandler {

    init {
        registerEvent()
    }

    private val callbacks = mutableListOf<() -> Boolean>()

    /**
     * Registers callback function to test whether to show warning on page unload.
     */
    fun register(callback: () -> Boolean) {
        callbacks.add(callback)
    }

    private fun anyUnsavedChanges(): Boolean = callbacks.any { it.invoke() }

    private fun registerEvent() {
        // add event handler on browser tab close/refresh
        window.onbeforeunload = { e ->
            if (anyUnsavedChanges()) {
                e.preventDefault() // for Firefox
                e.returnValue = "" // for Chrome
                ""
            } else {
                null
            }
        }
    }
}