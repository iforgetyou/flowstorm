package ai.flowstorm.common.ui

interface NamedComponent {
    val name: String
}