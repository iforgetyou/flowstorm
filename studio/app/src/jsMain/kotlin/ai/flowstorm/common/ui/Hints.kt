package ai.flowstorm.common.ui

import ai.flowstorm.studio.resources.CurrentUserResource
import kotlinx.browser.localStorage
import ai.flowstorm.studio.resources.UserResource
import ai.flowstorm.common.auth.UserService
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import io.kvision.jquery.invoke
import io.kvision.jquery.jQuery
import io.kvision.core.*
import io.kvision.utils.obj
import io.kvision.i18n.I18n.gettext

object Hints {

    var nextIndex = 0

    private val mainScope = MainScope()
    var updateJob:Job = Job()

    data class Hint(val id: String, val index: Int, val widget: Widget, val content: String, val main: Boolean) {
        val name = "hint:$id"
        var seen: Boolean = false
            set(value) {
                if (UserService.isKnownUser) {
                    val currentUser = UserService.context.user
                    if (!(value && currentUser.seenHints.contains(name))) {
                        if (value)
                            currentUser.seenHints += name
                        else
                            currentUser.seenHints = currentUser.seenHints.filter { it != name }

                        if(updateJob.isActive) {
                            updateJob.cancel()
                        }
                        updateJob = mainScope.launch {
                            delay(5000)
                            console.log("Saving ${currentUser.seenHints.size} hint(s) to user")
                            CurrentUserResource.update(currentUser, doToast = false)
                        }
                    }
                } else {
                    if (value)
                        localStorage.setItem(name, true.toString())
                    else
                        localStorage.removeItem(name)
                }
                field = value
            }

        init {
            seen = if(UserService.isKnownUser) {
                UserService.context.user.seenHints.contains(name)
            } else {
                localStorage.getItem(name) == true.toString()
            }
        }

        fun show() {
            console.log("Showing hint $id (seen=$seen)")
            currentHint = this
            widget.getElementJQueryD()?.popover("show")
            jQuery("#hintbs_$id").click {
                hide()
            }
            jQuery("#hintbf_$id").click {
                next()
            }
        }

        fun hide() {
            seen = true
            currentHint = null
            widget.getElementJQueryD()?.popover("hide")
        }

        fun next() {
            hide()
            Hints.show()
        }
    }

    enum class Reset { NONE, SCREEN, ALL }

    private val hints = mutableListOf<Hint>()
    private var currentHint: Hint? = null

    fun show(reset: Reset = Reset.NONE) {
        if (currentHint != null) {
            currentHint!!.next()
        } else {
            if (reset != Reset.NONE) {
                // reset seen value of all visible hint widgets
                hints.forEach {
                    if ((reset == Reset.ALL) || (it.widget.getElement() != null && !it.main))
                        it.seen = false
                }
            }
            for (index in 0 until hints.size) {
                val it = hints[index]
                if (it.widget.getElement() != null && it.widget.visible && !it.seen) {
                    it.show()
                    break
                }
            }
        }
    }

    fun Widget.hint(hintId: String, icon: Icon, hintPlacement: Placement = Placement.AUTO, isMain: Boolean = false, index: Int = nextIndex++): Widget {
        val hintKey = "hint:${hintId}"
        if (hintKey == gettext(hintKey)) {
//            console.log("Missing hint for ID $hintId")
            return this
        }
        if (id == null)
            id = hintId
        val hint = Hint(hintId, index, this, gettext(hintKey), isMain)
        val options = obj {
            content = "<div style=\"float:left; padding-top: 8px\"><i class=\"${icon.def} fa-2x\"></i></div>" +
                    "<div style=\"float:right;\">" +
                        "<button id=\"hintbs_$id\" class=\"btn btn-secondary btn-hint\" type=\"button\"><i class=\"fas fa-stop\"></i></button>" +
                        "<button id=\"hintbf_$id\" class=\"btn btn-secondary btn-hint\" type=\"button\"><i class=\"fas fa-forward\"></i></button>" +
                    "</div>" +
                    "<p class=\"hint\">${hint.content}</p>"
            html = true
            delay = 2000
            placement = hintPlacement.name
            trigger = "manual"
            sanitize = false
            boundary = "viewport"
        }
        this@hint.addAfterInsertHook {
            val popoverFun = getElementJQueryD()?.popover
            if (popoverFun != undefined) getElementJQueryD()?.popover(options)
        }
        this@hint.addAfterDestroyHook {
            hint.hide()
        }
        hints.add(hint)
        hints.sortBy { it.index }
//            console.log("Hint $id added")
        return this
    }
}