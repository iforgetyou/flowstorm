package ai.flowstorm.common.ui

import org.w3c.dom.events.MouseEvent
import io.kvision.core.Container
import io.kvision.core.Placement
import io.kvision.core.TooltipOptions
import io.kvision.core.Trigger
import io.kvision.html.Button
import io.kvision.html.ButtonStyle

class ToolbarButton(icon: Icon, tooltip: String, shortcut: String? = null, disabled: Boolean = false, style: ButtonStyle = ButtonStyle.PRIMARY, init: (ToolbarButton.() -> Unit)? = null) :
        Button("", icon.def, style, disabled = disabled) {

    var active: Boolean = false
        set(value) {
            if (value)
                addCssClass("active")
            else
                removeCssClass("active")
            field = value
        }

    fun setIcon(icon: Icon) {
        this.icon = icon.def
    }

    // Workaround for tooltip remaining on the page when the button gets disabled (Chrome browser)
    // Small issue: the tooltip won't appear until the button is enabled again
    fun disable(shouldDisable: Boolean = true) {
        disabled = if (shouldDisable) {
            hideTooltip()
            true
        } else {
            false
        }
    }


    init {
        enableTooltip(TooltipOptions(
                title = tooltip + if (shortcut != null) " &nbsp; <span class=\"shortcut\">$shortcut</span>" else "",
                rich = true,
                delay = 600,
                animation = true,
                placement = Placement.BOTTOM,
                triggers = listOf(Trigger.HOVER)
        ))
        init?.invoke(this)
    }

    override fun onClick(handler: Button.(MouseEvent) -> Unit): ToolbarButton {
        return super.onClick(handler) as ToolbarButton
    }
}

/**
 * DSL builder extension function for ToolBarButton.
 */
fun Container.toolBarButton(
        icon: Icon, tooltip: String, shortcut: String? = null, disabled: Boolean = false, style: ButtonStyle = ButtonStyle.PRIMARY, init: (ToolbarButton.() -> Unit)? = null
): ToolbarButton {
    val button = ToolbarButton(icon, tooltip, shortcut, disabled, style, init)
    this.add(button)
    return button
}
