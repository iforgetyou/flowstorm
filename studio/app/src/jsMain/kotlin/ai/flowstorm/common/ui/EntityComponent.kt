package ai.flowstorm.common.ui

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.HasId
import io.kvision.core.Component

interface EntityComponent<E: HasId> : Component {

    var entity: E?
    fun load(entity: E): E
    fun load(id: Id): E
}