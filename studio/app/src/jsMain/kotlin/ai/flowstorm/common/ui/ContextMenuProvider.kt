package ai.flowstorm.common.ui

import io.kvision.dropdown.DropDown

interface ContextMenuProvider {
    fun initContextMenu(dropDown: DropDown)
}