package ai.flowstorm.common.ui

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Confirm

open class ConfirmModal(
    val caption: String,
    val text: String,
    val yesTitle: String = tr("OK"),
    val noTitle: String = tr("Cancel"),
) {
    class Delete : ConfirmModal(tr("Are you sure?"), tr("Do you really want to delete this?"), tr("Yes"), tr("No"))

    suspend fun open(): Boolean = coroutineScope {
        val click = CompletableDeferred<Boolean>(coroutineContext[Job])
        Confirm.show(caption, text, yesTitle = yesTitle, noTitle = noTitle, noCallback = {
            click.complete(false)
        }) {
            click.complete(true)
        }
        click.await()
    }
}