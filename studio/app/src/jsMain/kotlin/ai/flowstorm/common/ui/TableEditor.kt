package ai.flowstorm.common.ui

import ai.flowstorm.common.RestClient
import com.github.snabbdom.VNode
import io.kvision.panel.SimplePanel
import kotlinext.js.jsObject
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.await
import kotlinx.coroutines.launch

class TableEditor(val dataSource: String?, dataType: String, classes: Set<String> = setOf()) : SimplePanel(classes = classes) {
    val containerId = "handsOnTable"
    val client = RestClient()
    val mainScope = MainScope()
    var hot: dynamic = undefined
    val handsOnTable = io.kvision.require("handsontable")
    val delimiter = if (dataType == "text/csv") "," else ";"
    // From https://stackoverflow.com/a/48806378
    val csvRegex = Regex("(?:$delimiter|\\n|^)(\"(?:(?:\"\")*[^\"]*)*\"|[^\"$delimiter\\n]*|(?:\\n|\$))")
    // from https://stackoverflow.com/a/43033826
    val quoteRegex = Regex("(?<!\\\\)[\"']")


    init {
        id = containerId
        io.kvision.require("handsontable/dist/handsontable.full.css")
    }

    private fun csvStringToArray(inputData: String): Array<Array<String>> {
        val lines = inputData.split("\n")
        val result = lines.map { line ->
            val match = csvRegex.findAll(line).map { it.groups.elementAt(1)!!.value.replace(quoteRegex, "") }
            match.toList().toTypedArray()
        }
        return result.toTypedArray()
    }

    @Suppress("UNUSED_VARIABLE")
    override fun afterInsert(node: VNode) {
        super.afterInsert(node)

        mainScope.launch {
            val response = client.remoteCall(dataSource!!).await() as String
            val assetData = csvStringToArray(response)
            val options = jsObject<dynamic> {
                data = assetData
                rowHeaders = true
                colHeaders = true
                filters = true
                dropdownMenu = true
                contextMenu = true
                licenseKey = "non-commercial-and-evaluation"
            }
            val container = document.getElementById(containerId)
            val table = handsOnTable
            hot = js("new table.default(container, options);")
        }
    }
}