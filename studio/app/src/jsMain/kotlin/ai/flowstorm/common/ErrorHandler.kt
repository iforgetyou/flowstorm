package ai.flowstorm.common

import io.kvision.modal.Alert
import io.kvision.modal.ModalSize
import io.kvision.utils.obj

object ErrorHandler {

    private var sentry: dynamic = null

    fun init(url: String, env: String) {
        if (!listOf("_local_", "local", "develop", "feature").contains(env)) {
            sentry = js("Sentry")
            sentry.init(
                    obj {
                        dsn = url
                        tracesSampleRate = 1.0
                        integrations = arrayOf(js("new Sentry.Integrations.BrowserTracing()"))
                        environment = env
                    }
            )
        }
    }

    fun handle(m: String) {
        sentry?.captureMessage(m)
    }

    fun handle(t: Throwable, show: Boolean = true) {
        sentry?.captureException(t)
        if (show)
            showExceptionModal(t)
    }

    private fun showExceptionModal(t: Throwable) {
        var e = t
        console.error(e, e.asDynamic().stack)
        val exceptions = mutableListOf(e)
        while (e.cause != null) {
            e = e.cause as Throwable
            exceptions.add(e)
        }

        val message = exceptions.map { "<li><b>${it::class.simpleName}</b><br><pre> ${it}</pre></li>" }.reduce { a, b -> "$a $b" }

        Alert.show("Exception", "<ol>$message</ol>", rich = true, size = ModalSize.LARGE)
    }
}