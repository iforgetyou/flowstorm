package ai.flowstorm.common.ui

import ai.flowstorm.common.ui.Hints.hint
import io.kvision.core.*
import io.kvision.html.*
import io.kvision.i18n.I18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.panel.simplePanel
import io.kvision.utils.px

open class CardPanel(
        override val caption: String,
        open val hideHeader: Boolean = false,
        val icon: Icon? = null,
        classes: Set<String> = setOf("card-panel")
) : SimplePanel(classes = classes), Panel {

    lateinit var panelHeader: Tag

    fun addPanelHeader() {
        simplePanel(classes = setOf("content-header")) {
            visible = !hideHeader
            simplePanel(classes = setOf("content-header-title")) {
                panelHeader = tag(TAG.H2, tr(caption)) {
                    if (this@CardPanel::class.simpleName != CardPanel::class.simpleName) {
                        hint(this@CardPanel::class.simpleName!!, icon ?: iconFor(this@CardPanel) ?: Icon.Help)
                    }
                    (icon ?: iconFor(this@CardPanel))?.let {
                        icon(icon = it.def) {
                            paddingRight = 8.px
                        }
                    }
                }
            }
            simplePanel(classes = setOf("content-header-controls")) {
                add(actionComponent())
            }
        }
    }

    open fun actionComponent(): Component = span()
}

fun Container.cardPanel(
    caption: String,
    hideHeader: Boolean = false,
    icon: Icon? = null,
    className: String = "card-panel",
    init: (CardPanel.() -> Unit)? = null
): CardPanel {
    val panel = CardPanel(caption, hideHeader, icon, setOf(className))
    init?.invoke(panel)
    this.add(panel)
    return panel
}
