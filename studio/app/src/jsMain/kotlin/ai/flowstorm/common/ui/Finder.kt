package ai.flowstorm.common.ui

import ai.flowstorm.studio.common.state.AppState
import com.github.snabbdom.VNode
import kotlinext.js.asJsObject
import org.w3c.dom.Node
import io.kvision.jquery.*
import io.kvision.panel.SimplePanel
import io.kvision.require
import io.kvision.state.ObservableValue
import io.kvision.utils.obj
import kotlinx.browser.document

// see https://github.com/mynameistechno/finderjs/blob/master/example/example-static-select-path.js
class Finder : SimplePanel() {

    open class Item(var label: String)
    open class Folder(label: String, var items: MutableList<Item> = mutableListOf()) : Item(label) {
        val children get() = items.toTypedArray()
    }

    var itemTemplates = mapOf<String, dynamic>()

    val finder = require("finderjs")

    var items: Array<Item> = arrayOf()
    var selectedItem = ObservableValue<Item?>(null)

    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        val emitter = finder(node.elm, items, obj {})
        emitter.on("leaf-selected",
                fun(item: Item) {
                    onItemSelected(item)
                    emitter.emit("create-column", createInfoColumn(item));

                }
        )
        emitter.on("interior-selected", this::onItemDeselected)
        jQuery(".fjs-item a").first().focus()
        jQuery(".fjs-col").scrollTop(0)
    }

    private fun onItemSelected(item: Item) {
        selectedItem.value = item
    }

    private fun onItemDeselected() {
        selectedItem.value = null
    }

    private fun createInfoColumn(item: Item): Node {
        val element = document.createElement("template")
        element.innerHTML = if (itemTemplates.containsKey(AppState.state.ui.language))
            itemTemplates[AppState.state.ui.language](item.asJsObject())
        else "<div></div>"
        return js("element.content.firstChild")
    }
}
