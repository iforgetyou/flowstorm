package ai.flowstorm.common.ui.form

import ai.flowstorm.common.model.IdPair
import ai.flowstorm.common.id.ObjectId
import io.kvision.form.FormControl

interface NamedEntityFormControl: FormControl {

    var idPair: IdPair?

    var value: Any?

    override fun getValue(): Any? = value
    override fun setValue(v: Any?) {
        //KVision use plain JS object when setting value - it can't be casted to entity
        if (v == null) return
        val dyn = v.asDynamic()
        idPair = IdPair(ObjectId(dyn._id),dyn.label)
        value = v
    }
    override fun getValueAsString(): String? = idPair?.second
}