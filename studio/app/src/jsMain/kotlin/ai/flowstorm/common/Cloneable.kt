package ai.flowstorm.common

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer

interface Cloneable<T> {

    fun clone(): T

    companion object {
        inline fun <reified T : Cloneable<T>> clone(obj: T): T = clone(obj, serializer())

        fun <T> clone(obj: T, serializer: KSerializer<T>): T {
            val json = Json
            val serialized = json.encodeToString(serializer, obj)
            return json.decodeFromString(serializer, serialized)
        }
    }
}