package ai.flowstorm.common.ui

import io.kvision.core.Component

interface Panel : Component {
    val caption: String
}