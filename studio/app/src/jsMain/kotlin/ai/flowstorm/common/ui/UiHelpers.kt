package ai.flowstorm.common.ui

import ai.flowstorm.common.model.IdPair
import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.model.Labeled
import ai.flowstorm.common.model.idPair
import ai.flowstorm.common.resources.EntityResource
import io.kvision.jquery.*
import io.kvision.core.StringPair
import io.kvision.core.Widget
import io.kvision.i18n.tr
import io.kvision.types.toStringF
import io.kvision.i18n.gettext
import io.kvision.utils.px
import kotlinx.browser.window
import kotlin.js.Date

external fun unescape(s: String): String
external fun encodeURIComponent(str: String): String

object UiHelpers {

    val spacing = 12.px
    val spacingDouble = 24.px

    fun <T> options(values: Array<T>, nullOption: Boolean = false): MutableList<StringPair> {
        val options = mutableListOf<StringPair>()
        if (nullOption)
            options.add("" to "-")
        for (value in values) {
            val str = value.toString()
            options.add(StringPair(str, tr(str)))
        }
        return options
    }

    fun <E> options(list: List<E>, nullOption: Boolean = false, transform: ((E) -> IdPair?)? = null): List<IdPair>
            where E: HasId, E: Labeled
    {
        val options = mutableListOf<IdPair>()
        if (nullOption)
            options.add(null to "-")
        options.addAll(list.mapNotNull {
            if (transform != null)
                transform(it)
            else
                IdPair(it.id, it.label)
        })
        return options
    }

    inline fun <reified T : Enum<T>> enumOptions(): List<StringPair> = enumValues<T>().map { it.name to gettext(it.name) }

    suspend fun <E> options(model: EntityResource<E>, nullOption: Boolean = false, transform: ((E) -> IdPair?) = { it.idPair() }) : List<IdPair>
            where E: HasId, E: Labeled
    {
        return options(model.load(), nullOption, transform)
    }

    inline fun<reified T> parent(w: Widget): T? {
        var p = w.parent
        while (p != null) {
            if (T::class.isInstance(p))
                return (p as T)
            p = p.parent
        }
        return null
    }

    fun encode(date: Date): String = encodeURIComponent(date.toStringF("YYYY-MM-DDTHH:mm:ss+001"))

    fun getUrlParameter(name: String): String? {
        val search = window.location.search.substring(1)
        val params = search.split("&")
        params.forEach {
            val p = it.split("=")
            if (p.size == 2 && p[0] == name) {
                return unescape(p[1])
            }
        }
        return null
    }



    fun getHeight(sourceSelector: Collection<String>): Int {
        var height = 0
        for (selector in sourceSelector) {
            height += jQuery(selector).outerHeight().toInt()
        }
        return height
    }

     fun resizeHeight(targetSelectors: Collection<String>, height: Int) {
         targetSelectors.forEach {
             jQuery(it).innerHeight(height)
         }
     }

     fun resizeHeight(targetSelectors: Collection<String>, sourceSelectors: Collection<String>): Int {
         val height = window.innerHeight - getHeight(sourceSelectors)
         //console.log("resizing $targetSelectors.innerHeight = window.innerHeight - $sourceSelectors.outerHeight = $height")
         resizeHeight(targetSelectors, height)
         return height
     }
}