package ai.flowstorm.common.ui

import io.kvision.core.Overflow
import io.kvision.core.onEvent
import io.kvision.panel.SimplePanel

abstract class PropertiesEditor<E : Any>(val classes: Set<String> = setOf("panel-view-nav1")) : SimplePanel(classes = classes), ReadOnly {

    private lateinit var model: E
    abstract val form: Form<E>

    override var readOnly: Boolean = true
        set(value) {
            form.disabled = value
            field = value
        }


    fun init() {
        paddingTop = UiHelpers.spacing
        paddingRight = UiHelpers.spacing
        if (classes.isNotEmpty())
            overflow = Overflow.SCROLL
        add(form.apply {
            onEvent {
                change = {
                    save(model)
                }
            }
        })
    }

    fun load(model: E) {
        this.model = model
        form.setData(model)
    }
}