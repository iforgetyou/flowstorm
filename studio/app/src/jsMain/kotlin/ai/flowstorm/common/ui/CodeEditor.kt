package ai.flowstorm.common.ui

import com.github.snabbdom.VNode
import ai.flowstorm.studio.resources.space.DialogueMixinResource
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.id.Id
import io.kvision.panel.SimplePanel
import io.kvision.rest.RemoteRequestException
import io.kvision.utils.obj

open class CodeEditor(private val mode: String = "kotlin", classes: Set<String> = setOf()) :
        SimplePanel(classes = classes), ReadOnly {
    val mainScope = MainScope()

    var value: String = ""
        private set
    var editor: dynamic = null

    var mixinLength = 0
    var mixins_id: List<Id> = listOf()

    override var readOnly: Boolean = true
        set(value) {
            editor?.setReadOnly(value)
            field = value
        }

    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        @Suppress("UNUSED_VARIABLE")
        val element = node.elm
        editor = js("ace.edit(element);")
        with (editor) {
            setTheme("ace/theme/crimson_editor")
            getSession().setUseWrapMode(true)
            session.setNewLineMode("unix")
            setFontSize("12pt")
            session.setMode("ace/mode/$mode")
            `$blockScrolling` = js("Infinity")
            setReadOnly(readOnly)
            session.on("change") {
                value = (getValue() as String).substring(mixinLength)
                this@CodeEditor.dispatchEvent("change", obj { detail = value })
            }
        }
        mainScope.launch {
            loadTextWithMixins(mixins_id)
        }
        setRangeReadOnly()
    }

    fun set(value: String, mixins_id: List<Id> = listOf()) {
        this.value = value
        mainScope.launch {
            loadTextWithMixins(mixins_id)
        }
    }

    override fun focus() = editor.focus()

    @Suppress("UNUSED_VARIABLE")
    fun addFold(foldEnd: Int) {
        if (editor != null) {
            editor?.session.foldAll(0, foldEnd, 0)
        }
    }

    // Based on https://stackoverflow.com/questions/48077089/make-only-last-line-editable-in-ace-editor
    @Suppress("UNUSED_VARIABLE")
    fun setRangeReadOnly(){
        val currentEditor = editor
        js("""currentEditor.commands.on('exec', function(e) {
                    if (e.command.readOnly)
                        return;
                    var deletesLeft = e.command.name == 'backspace' || e.command.name == 'removewordleft';
                    var deletesRight = e.command.name == 'del' || e.command.name == 'removewordright';
                    var notEditable = currentEditor.selection.getAllRanges().some(function(r) {
                        if (deletesRight && r.start.row < currentEditor.readOnlyEndIndex) return true;
                        if (deletesLeft && (r.start.row < currentEditor.readOnlyEndIndex || (r.start.column == 0 && r.start.row == currentEditor.readOnlyEndIndex && r.isEmpty()))) return true;
                        return !(r.start.row >= currentEditor.readOnlyEndIndex);
                    });
                    if (notEditable)
                        e.preventDefault();
                });"""
        )
    }

    @Suppress("UNUSED_VARIABLE")
    private suspend fun loadTextWithMixins(loaded_mixins_id: List<Id>) {
        var endIndex = 0
        if (loaded_mixins_id.isNotEmpty()) {
            mixins_id = loaded_mixins_id
            var mixinText = ""
            loaded_mixins_id.forEach {
                mixinText += try {
                    val mixin = DialogueMixinResource.load(it)
                    "#mixin ${mixin.name} {\n${mixin.text}\n}\n"
                } catch (t: RemoteRequestException) {
                    "// Mixin with ID $it could not be loaded\n\n"
                }
            }
            mixinLength = mixinText.length - (mixinText.count { it == '\r' })
            editor?.setValue(mixinText + this.value, -1)
            val mixinLineCount = mixinText.lines().size
            addFold(mixinLineCount - 2)
            endIndex = mixinLineCount - 1
        } else {
            mixins_id = listOf()
            mixinLength = 0
            editor?.setValue(this.value, -1)
        }
        val currentEditor = editor
        if (currentEditor != null) {
            // JS HACK
            val i = endIndex
            js("currentEditor.readOnlyEndIndex = i")
        }
    }

    companion object {
        // Define a custom highlighter for Ace which has highlight rules for text but folding rules for Kotlin
        // This is so that mixins in plain text nodes can be folded

        // JS is based on https://github.com/ajaxorg/ace/wiki/Creating-or-Extending-an-Edit-Mode#minimal-new-mode
        fun defineFlowstormHighlighter (){
            js("""ace.define('ace/mode/flowstorm',function(require, exports, module) {
                    var oop = require('ace/lib/oop');
                    var TextMode = require('ace/mode/text').Mode;
                    var TextHighlightRules = require('ace/mode/text_highlight_rules').TextHighlightRules;
                    var CstyleBehaviour = require('ace/mode/behaviour/cstyle').CstyleBehaviour;
                    var FoldMode = require('ace/mode/folding/cstyle').FoldMode;
            
                    var Mode = function() {
                        this.HighlightRules = TextHighlightRules;
                        this.foldingRules = new FoldMode();
                        this.${'$'}behaviour = new CstyleBehaviour();
                    };
                    oop.inherits(Mode, TextMode);
                    (function() {}).call(Mode.prototype);
                    
                    exports.Mode = Mode;
                });""")
        }
    }
}