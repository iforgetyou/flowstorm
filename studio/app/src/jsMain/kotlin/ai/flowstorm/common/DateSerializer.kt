package ai.flowstorm.common

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import io.kvision.moment.Moment
import kotlin.js.Date

@Serializer(forClass = Date::class)
@Deprecated("You should not need this serializer. Use kotlinx.datetime")
object DateSerializer: KSerializer<Date> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("DateSerializer", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: Date) {
        encoder.encodeString(value.toISOString())
    }

    override fun deserialize(decoder: Decoder): Date {
        return Date(Moment(decoder.decodeString()).valueOf() as Number)
    }
}