package ai.flowstorm.common.auth

import io.kvision.require
import kotlinext.js.jsObject
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import io.kvision.utils.obj

class Auth0LockProvider : AbstractAuth0Provider() {

    val options = jsObject<dynamic> {
        languageDictionary = jsObject {
            title = "Log in"
            emailInputPlaceholder = "Email address"
            passwordInputPlaceholder = "Password"
            loginWithLabel = "Continue with %s"
            forgotPasswordAction = "Forgot password?"
            LoginLabel = "Continue"
            loginSubmitLabel = "Continue"
        }
        theme = jsObject {
            // Empty value = no logo, shorter header
            logo = ""
            primaryColor = "#4c54e1"
            authButtons = jsObject {

                apple = jsObject {
                    primaryColor = "#ffffff"
                    foregroundColor = "#333333"
                    icon = "https://upload.wikimedia.org/wikipedia/commons/f/fa/Apple_logo_black.svg"
                    border = "1px solid #dee0e2"
                }
                twitter = jsObject {
                    primaryColor = "#ffffff"
                    foregroundColor = "#333333"
                    icon = "https://upload.wikimedia.org/wikipedia/en/9/9f/Twitter_bird_logo_2012.svg"
                }
                slack = jsObject {
                    primaryColor = "#ffffff"
                    foregroundColor = "#333333"
                    displayName = "Slack"
                    icon = "https://upload.wikimedia.org/wikipedia/commons/d/d5/Slack_icon_2019.svg"
                }
                facebook = jsObject {
                    primaryColor = "#ffffff"
                    foregroundColor = "#333333"
                    icon = "https://upload.wikimedia.org/wikipedia/commons/c/cd/Facebook_logo_%28square%29.png"
                }
            }
        }

        allowSignUp = true
        this.auth = jsObject {
            responseType = "token id_token"
            audience = "https://admin.flowstorm"
            leeway = 60
            params = jsObject {
                scope = "openid profile email"
            }
            useRefreshTokens = true
            autoParseHash = false
        }
        rememberLastLogin = true
        autoclose = true
    }


    override var auth: dynamic = createLock()

    override fun login() {
        auth.show(obj { initialScreen = "login" })
    }

    override fun signup() {
        auth.show(obj { initialScreen = "signUp" })
    }

    @Suppress("UNUSED_VARIABLE")
    private fun createLock() {
        val auth0 = require("auth0-lock")
        val clientID = CLIENT_ID
        val domain = AUTH_DOMAIN
        val opt = this.options

        return js("new auth0.Auth0Lock(clientID, domain, opt)")
    }

    override suspend fun parseHash(hash: String) = coroutineScope {
        val job = Job(coroutineContext[Job])
        auth.resumeAuth(hash) { error, authResult ->
            if (authResult) {
                onAuthResult(authResult)
                job.complete()
            }
            if (error) {
                job.completeExceptionally(Auth0Error.create(error))
            }
        }
    }
}