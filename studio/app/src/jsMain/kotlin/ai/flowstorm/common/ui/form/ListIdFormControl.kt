package ai.flowstorm.common.ui.form

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.IdFactory
import io.kvision.form.FormControl

interface ListIdFormControl: FormControl {

    companion object {
        const val SEPARATOR = ","
    }

    var value: List<Id>?

    override fun getValue(): List<Id>? = value
    override fun setValue(v: Any?) {
        value = v?.unsafeCast<Array<String>>()?.map { IdFactory.create(it) }
    }

    override fun getValueAsString(): String? = value?.joinToString(SEPARATOR) { it.value }
}