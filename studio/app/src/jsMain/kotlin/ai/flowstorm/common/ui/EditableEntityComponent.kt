package ai.flowstorm.common.ui

import ai.flowstorm.common.model.HasId

interface EditableEntityComponent<E: HasId>: EntityComponent<E> {

    val isValid: Boolean
    suspend fun save()
    suspend fun delete()
    fun disableAll()
}