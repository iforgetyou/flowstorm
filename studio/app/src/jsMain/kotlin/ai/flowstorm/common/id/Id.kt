package ai.flowstorm.common.id

import kotlinx.serialization.Serializable

@Serializable(IdSerializer::class)
sealed interface Id {
    val value: String
}
