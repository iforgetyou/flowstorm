package ai.flowstorm.common.stateManager

interface Action {
    val name get() = this::class.simpleName
}