package ai.flowstorm.common.ui.form

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.IdFactory
import ai.flowstorm.common.model.IdPair
import io.kvision.core.StringPair
import io.kvision.form.select.AjaxOptions
import io.kvision.form.select.SelectInput

@Suppress("TooManyFunctions")
open class SelectId(
    options: List<IdPair>? = null, value: String? = null, name: String? = null,
    ajaxOptions: AjaxOptions? = null, label: String? = null,
    rich: Boolean = false
) : CustomFormControl(label, rich), IdFormControl {

    var options: List<IdPair>?
        get() = unstringify(input.options)
        set(value) {
            input.options = stringify(value)
        }

    override var value: Id?
        get() = if (input.value != null) IdFactory.create(input.value!!) else null
        set(value) {
            input.value = value?.also { onValueChange(it) }?.value
        }

    open fun onValueChange(value: Id) {}

    fun updateTitle(title: String) = input.getElementJQuery()?.next()?.apply {
        removeClass("bs-placeholder")
        prop("title", title)
        find(".filter-option-inner-inner").text(title)
    }

    var ajaxOptions
        get() = input.ajaxOptions
        set(value) {
            input.ajaxOptions = value
        }

    final override val input: SelectInput = SelectInput(
            stringify(options), value, false, ajaxOptions,
            setOf("form-control")
    ).apply {
        this.id = idc
        this.name = name
    }

    init {
        addComponents()
    }

    companion object {
        fun stringify(list: List<IdPair>?): List<StringPair>? =
                list?.map { StringPair(it.first?.value?:"", it.second) }
        fun unstringify(list: List<StringPair>?): List<IdPair>? =
                list?.map { Pair(IdFactory.create(it.first), it.second) }
    }
}
