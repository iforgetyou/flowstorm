package ai.flowstorm.common.ui.form

import ai.flowstorm.common.ui.CodeEditor
import ai.flowstorm.common.ui.UiHelpers
import io.kvision.form.FormInput
import io.kvision.form.InputSize
import io.kvision.form.StringFormControl
import io.kvision.form.ValidationStatus

@Suppress("TooManyFunctions")
open class CodeFormControl(label: String? = null, mode: String = "kotlin") :
        CustomFormControl(label), StringFormControl {

    class CodeInput(mode: String) : CodeEditor(mode), FormInput {
        override var disabled = false
            set(value) {
                if (value)
                    readOnly = true
                field = value
            }
        override var name: String? = null
        override var size: InputSize? = null
        override var validationStatus: ValidationStatus? = null
        override fun blur() {}

        init {
            readOnly = false
        }
    }

    override val input = CodeInput(mode)
    override var value: String?
        get() = input.value
        set(value) {
            input.set(value ?: "")
        }

    override fun subscribe(observer: (String?) -> Unit): () -> Unit {
        TODO("Not yet implemented")
    }

    init {
        addComponents()
    }
}