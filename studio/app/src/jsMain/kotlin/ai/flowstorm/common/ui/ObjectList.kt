package ai.flowstorm.common.ui

import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import io.kvision.core.*
import io.kvision.data.dataContainer
import io.kvision.form.text.TextInput
import io.kvision.form.text.textInput
import io.kvision.html.Tag
import io.kvision.i18n.I18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.panel.simplePanel
import io.kvision.state.observableListOf
import io.kvision.utils.perc

abstract class ObjectList<T : Any>(val provider: ObjectProvider<T>, classes: Set<String> = setOf("object-list")) : SimplePanel(classes = classes) {

    val mainScope = MainScope()

    var searchInput: TextInput

    private val filteredItems = observableListOf<T>()

    var selected: Tag? = null

    init {
        float = PosFloat.LEFT
        width = 100.perc
        height = 100.perc
        searchInput = textInput(className = "object-list-search-input") {
            display = Display.INLINE
            placeholder = tr("Search")
            onEvent {
                input = { search() }
            }
        }
        simplePanel(classes = setOf("panel-view-nav3")) {
            dataContainer(filteredItems, { item, index, list -> row(item, index, list) })
        }
    }

    init {
        search()
    }

    abstract fun row(item: T, index: Int, list: MutableList<T>): Component

    abstract fun filter(item: T, search: String): Boolean

    fun search() {
        mainScope.launch {
            val search = searchInput.value
            val nodes = if (search == null || search.isEmpty()) {
                provider.getItems()
            } else {
                provider.getItems().filter { filter(it, search) }
            }
            replace(nodes)
        }
    }

    private fun replace(items: List<T>) = with(filteredItems) {
        clear()
        addAll(items)
    }

    interface ObjectProvider<T : Any> {
        suspend fun getItems(): List<T>
    }
}