package ai.flowstorm.common.id

data class TempId internal constructor(override val value: String) : Id {
    constructor() : this(newId())

    companion object {
        private var tempIdCounter = 0
        const val TEMP_PREFIX = "new_"

        fun newId() = "$TEMP_PREFIX${tempIdCounter++}"

        fun isTempId(value: String) = value.startsWith(TEMP_PREFIX)
    }
}