package ai.flowstorm.common.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.ui.UiState.Action.*
import ai.flowstorm.studio.ui.SettingsModal
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.ui.Hints.hint
import io.kvision.dropdown.DropDown
import io.kvision.dropdown.ddLink
import io.kvision.dropdown.dropDown
import io.kvision.html.ButtonStyle
import io.kvision.i18n.tr
import io.kvision.navbar.Nav
import io.kvision.navbar.navLink
import io.kvision.state.bind
import io.kvision.state.sub
import io.kvision.state.subFlow

class UserNav : Nav() {

    var userDropDown: DropDown? = null

    private val settingsModal = SettingsModal()

    init {
        rightAlign = false
        bind(AuthService) { state ->
            if (state.isAuthenticated) {
                if(state.context!!.isAnonymous){
                    navLink("Log In", "#!/login") {
                        hint("LogIn", Icon.LogIn, isMain = true)
                    }
                    navLink("Sign Up", "#!/signup") {
                        hint("SignUp", Icon.SignUp, isMain = true)
                    }
                }
                userDropDown = dropDown(state.context.name, style = ButtonStyle.OUTLINEINFO, classes = setOf("user-dropdown")) {
                    image = state.context.idToken.payload.picture
                    hint("UserDropDown", Icon.User, isMain = true, index = 101)
                    if (!state.context.isAnonymous) {
                        ddLink(tr("User Settings"), "#", Icon.Properties.def).onClick {
                            this@bind.settingsModal.apply {
                                reload()
                                show()
                            }
                        }
                        ddLink(tr("Space"), "#!/space", Icon.Space.def)
                        ddLink(tr("Account"), "#!/account", Icon.Account.def)
                    }

                    ddLink(tr("Light mode"), "#", Icon.Sun.def).onClick {
                        AppState.dispatchLaunch(ChangeTheme("light-mode"))
                    }.bind(AppState.subFlow { it.ui.theme }) {
                        visible = it != "light-mode"
                    }

                    ddLink(tr("Dark mode"), "#", Icon.Moon.def).onClick {
                        AppState.dispatchLaunch(ChangeTheme("dark-mode"))
                    }.bind(AppState.subFlow { it.ui.theme }) {
                        visible = it != "dark-mode"
                    }

                    ddLink(tr("Tablet view"), "#", Icon.TabletAlt.def).onClick {
                        AppState.dispatchLaunch(ChangeViewMode("tablet"))
                    }.bind(AppState.subFlow { it.ui.viewMode }) {
                        visible = it != "tablet"
                    }

                    ddLink(tr("Desktop view"), "#", Icon.Desktop.def).onClick {
                        AppState.dispatchLaunch(ChangeViewMode("desktop"))
                    }.bind(AppState.subFlow { it.ui.viewMode }) {
                        visible = it != "desktop"
                    }

                    if (!state.context.isAnonymous) {
                        ddLink(tr("Log Out"), icon = Icon.LogOut.def).onClick {
                            AuthService.logout()
                        }
                    }
                }
            }

            toolBarButton(Icon.Bot, "Toggle bot panel") {
                hint("BotButton", Icon.Bot, isMain = true, index = 102)
                style = ButtonStyle.INFO
            }.bind(AppState.subFlow { it.ui.botPanelOpened }, false) {
                active = !it
            }.onClick {
                AppState.dispatchLaunch(ToggleBotPanel())
            }
        }
    }
}
