package ai.flowstorm.common.ui

import kotlinx.coroutines.*

class Debouncer(val delay: Long = 2000) {

    private var job: Job = Job()

    suspend fun run(block: () -> Unit) = coroutineScope {
        job.cancel()

        job = launch {
            delay(delay)
            block.invoke()
        }
    }
}