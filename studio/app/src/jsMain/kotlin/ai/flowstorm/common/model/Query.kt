package ai.flowstorm.common.model

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.ui.UiHelpers
import kotlin.js.Date

data class Query(
    val filters: MutableList<Filter> = mutableListOf(),
    var sort: Sort? = null,
    val seek_id: Id? = null,
    val limit: Int? = null,
    val search: String? = null,
) {
    data class Filter(val field: String, val operator: Operator, val value: Any)
    data class Sort(val field: String, val direction: SortDirection = SortDirection.Asc)

    enum class Operator {
        eq, ne, gt, gte, lt, lte, `in`, like, regex
    }

    enum class SortDirection {
        Asc, Desc
    }

    override fun toString(): String {

        val params: MutableList<Pair<String, String>> = mutableListOf()

        seek_id?.let { params.add("seek_id" to it.value) }
        limit?.let { params.add("limit" to it.toString()) }

        filters.forEach {
            val value = when (it.value) {
                is Date -> UiHelpers.encode(it.value)
                is Iterable<*> -> it.value.map { it.toString() }.reduce { acc, s -> "$acc,$s" }
                is Id -> it.value.value
                else -> it.value.toString()
            }

            params.add("${it.field}[${it.operator}]" to value)
        }

        sort?.let {
            params.add("sort[${it.field}]" to it.direction.name)
        }

        search?.let {
            params.add("search" to it)
        }

        return "?".plus(params.map { "${it.first}=${it.second}" }.joinToString("&"))
    }
}