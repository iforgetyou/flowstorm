package ai.flowstorm.common.ui

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.model.HasId
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize

open class EntityComponentModal<E: HasId, C: EntityComponent<E>>(caption: String, val component: C, size: ModalSize? = null):
        Modal(tr(caption), size = size), EntityComponent<E> {

    override var entity: E? = null
        get() = component.entity

    init {
        add(component)
    }

    override fun load(entity: E) = component.load(entity)

    override fun load(id: Id) = component.load(id)
}