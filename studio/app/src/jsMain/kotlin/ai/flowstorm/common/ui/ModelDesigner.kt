package ai.flowstorm.common.ui

import ai.flowstorm.common.ShortcutProvider
import ai.flowstorm.common.ShortcutProvider.Shortcut
import ai.flowstorm.common.model.AbstractEntity
import ai.flowstorm.common.id.Id
import io.kvision.dropdown.DropDown
import io.kvision.dropdown.ddLink
import io.kvision.dropdown.separator
import io.kvision.html.span
import io.kvision.i18n.tr
import kotlinx.coroutines.launch
import ai.flowstorm.common.Initiable
import io.kvision.html.Iframe
import io.kvision.panel.Tab
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

abstract class ModelDesigner<E : AbstractEntity>(private val docUrl: String? = null) : DesignerPanel(), Initiable, ContextMenuProvider, ShortcutProvider {

    abstract suspend fun getNewModel(): E

    val loadingModel = Mutex()

    override fun init() {
        mainScope.launch {
            loadingModel.withLock {
                if (activeTab == null) {
                    if (docUrl != null) {
                        val tab = Tab("", Iframe(docUrl, classes = setOf("content-panel")), Icon.Doc.def)
                        addTab(tab)
                    }

                    load(getNewModel())
                }
            }
        }
    }

    override fun initContextMenu(dropDown: DropDown) {
        dropDown.apply {
            visible = true
            text = tr("Model")
            ddLink(tr("New") + "...", "#", Icon.New.def) {
                span("Ctrl+n", classes = setOf("shortcut"))
            }.onClick {
                mainScope.launch { new() }
            }
            separator()
            ddLink(tr("Open") + "...", "#", Icon.Open.def) {
                span("o", classes = setOf("shortcut"))
            }.onClick {
                mainScope.launch { open() }
            }
            ddLink(tr("Save"), "#", Icon.Save.def) {
                span("Ctrl+s", classes = setOf("shortcut"))
            }.onClick {
                mainScope.launch { save() }
            }
            ddLink(tr("Save As..."), "#") {
                span("Ctrl+S", classes = setOf("shortcut"))
            }.onClick {
                mainScope.launch { saveAs() }
            }
        }
    }

    override val shortcuts: List<Shortcut> = listOf(
        Shortcut(tr("New Model"), "n", ctrlKey = true) { mainScope.launch {  new() } },
        Shortcut(tr("Open Model"), "o") { mainScope.launch { open() } },
        Shortcut(tr("Save Model"), "s", ctrlKey = true) { mainScope.launch { save() }},
        Shortcut(tr("Save Model As"), "S", ctrlKey = true) { mainScope.launch { saveAs() }},
    )
        get() = field + ((activeChild as? ShortcutProvider)?.shortcuts ?: listOf())

    abstract suspend fun new()
    abstract suspend fun open()
    abstract suspend fun load(model: E)
    abstract suspend fun load(id: Id)
    open suspend fun save() = (activeChild as ModelEditor<*>).save()
    open suspend fun saveAs() = (activeChild as ModelEditor<*>).saveAs()
}