package ai.flowstorm.common.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.jsonObject

class UnderscorePropertyRemoveSerializer<E : Any>(serializer: KSerializer<E>) :
    JsonTransformingSerializer<E>(serializer) {
    override fun transformSerialize(element: kotlinx.serialization.json.JsonElement): kotlinx.serialization.json.JsonElement =
        JsonObject(element.jsonObject.filterNot { (k, _) ->
            k.startsWith("_") && k != "_id"
        })
}
