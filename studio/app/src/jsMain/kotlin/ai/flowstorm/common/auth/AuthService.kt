package ai.flowstorm.common.auth

import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.AuthLog

object AuthService: Stateful<AuthService.State> by StatefulValue(State()) {

    private var _provider: JwtAuthProvider? = null
    var provider: JwtAuthProvider
        get() = _provider ?: error("Missing auth provider")
        set(value) = value.let {
            _provider = it
            it.authService = this
        }

    data class State(val context: AuthContext? = null) {
        val isAuthenticated get() = this.context != null
    }

    val isAuthenticated get() = state.isAuthenticated

    var context: AuthContext
        get() = state.context ?: notAuthenticated()
        internal set(value) {
            newState { State(value) }
        }

    private fun notAuthenticated(): Nothing = error("User is not authenticated")

    suspend fun authenticate() {
        console.log("Authenticating ...")
        try {
            provider.authenticate()
        } catch (t: Throwable) {
            AuthLog.timeLog("Authentication error: ${t.message}")
            throw t
        }
    }

    fun login() {
        provider.login()
    }

    fun logout() {
        provider.logout()
        newState { State() }
    }

    fun signup() {
        provider.signup()
    }

    suspend fun refreshToken() {
        provider.refreshToken()
    }
}