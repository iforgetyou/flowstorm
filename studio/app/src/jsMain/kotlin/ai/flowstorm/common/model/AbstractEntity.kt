package ai.flowstorm.common.model

abstract class AbstractEntity :  Entity, HasId, Labeled {
    override val id by ::_id
}