package ai.flowstorm.common.state

import io.kvision.state.ObservableValue

fun <T> ObservableValue<T>.update(lambda: (T.() -> T)) { value = lambda(value) }
