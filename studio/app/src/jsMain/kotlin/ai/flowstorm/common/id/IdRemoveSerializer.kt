package ai.flowstorm.common.id

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

class IdRemoveSerializer<E : Any>(serializer: KSerializer<E>) :
    JsonTransformingSerializer<E>(serializer) {
    override fun transformSerialize(element: kotlinx.serialization.json.JsonElement): kotlinx.serialization.json.JsonElement =
        JsonObject(element.jsonObject.filterNot { (k, v) ->
            k == "_id" && (v.jsonPrimitive.content == "null" || TempId.isTempId(v.jsonPrimitive.content))
        })
}
