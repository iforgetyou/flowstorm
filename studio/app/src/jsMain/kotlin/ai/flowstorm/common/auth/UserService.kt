package ai.flowstorm.common.auth

import ai.flowstorm.studio.resources.CurrentUserResource
import ai.flowstorm.studio.resources.InvitationResource
import ai.flowstorm.studio.resources.SpaceResource
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import io.kvision.i18n.I18n
import io.kvision.i18n.tr
import io.kvision.modal.Confirm
import io.kvision.rest.NotFound

object UserService {

    val mainScope = MainScope()

    private var _context: UserContext? = null

    var context: UserContext
        get() = _context ?: throw UnknownUser()
        set(value) {
            _context = value
        }

    val isKnownUser get() = _context != null

    suspend fun preFetchUserSpaces() {
        CurrentUserResource.loadUserSpaces().forEach { SpaceResource.addEntity(it) }
    }

    suspend fun createContext() {
        val user = try {
            CurrentUserResource.getUser()
        } catch (t: NotFound) {
            return
        }

        context = UserContext(user, listOf())
    }

    suspend fun getRoles() {
        val roles = CurrentUserResource.loadUserRoles()
        context = context.copy(roles = roles)
    }

    class UnknownUser : Exception()

    suspend fun checkInvitations() = coroutineScope {
        val checkInvitationsJob = Job(coroutineContext[Job])
        CurrentUserResource.invitations().let {
            if (it.isNotEmpty()) {
                it.first().let { invitation ->
                    Confirm.show(
                            tr("Invitation"),
                            I18n.gettext("Do you want to accept invitation") + " #${invitation.id.value}?",
                            yesTitle = tr("Yes"),
                            noTitle = tr("No")
                    ) {
                        launch {
                            InvitationResource.accept(invitation.id)
                            checkInvitationsJob.complete()
                        }
                    }
                }
            } else {
                checkInvitationsJob.complete()
            }
        }

        checkInvitationsJob.join()
    }
}
