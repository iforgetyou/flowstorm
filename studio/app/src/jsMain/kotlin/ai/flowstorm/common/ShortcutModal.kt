package ai.flowstorm.common

import io.kvision.i18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.table.cell
import io.kvision.table.row
import io.kvision.table.table

class ShortcutModal(shortcutProvider: ShortcutProvider) : Modal(tr("Shortcut Keys"), size = ModalSize.LARGE, scrollable = true) {
    init {
        table {
            shortcutProvider.shortcuts.reversed().distinctBy { it.shortcut }.reversed().forEach { shortcut ->
                row {
                    cell(shortcut.shortcut)
                    cell(shortcut.name)
                }
            }
        }
    }
}