package ai.flowstorm.common.auth

import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.model.UserSpaceRole

data class UserContext(
        val user: User,
        val roles: List<UserSpaceRole>
) {
    fun getRoles(required: Space.Role) = roles.filter { Space.Role.isAllowed(required, it.role) }
}