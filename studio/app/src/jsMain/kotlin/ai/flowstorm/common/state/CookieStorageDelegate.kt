package ai.flowstorm.common.state

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.IdFactory
import ai.flowstorm.common.id.StringId
import io.kvision.utils.obj
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

class CookieStorageDelegate<V : Any>(private val name: String, val options:dynamic, private val type: KClass<V>) {

    private val cookies by lazy { io.kvision.require("js-cookie") }

    operator fun getValue(thisRef: Any, property: KProperty<*>): V? {
        return (cookies.get(name) as? String)?.let { value ->
            val v: Any = when (type) {
                Int::class -> value.toInt()
                Float::class -> value.toFloat()
                Double::class -> value.toDouble()
                Boolean::class -> value.toBoolean()
                String::class -> value
                Id::class -> IdFactory.create(value)
                else -> error("Unsupported cookie value type: $type")
            }
            @Suppress("UNCHECKED_CAST")
            v as V
        }
    }


    operator fun setValue(thisRef: Any, property: KProperty<*>, value: V?) {
        cookies.set(name, value, options)
    }
}

inline fun <reified V : Any> Any.cookieStorage(name:String, options: dynamic = obj {  }) = CookieStorageDelegate(name, options, V::class)