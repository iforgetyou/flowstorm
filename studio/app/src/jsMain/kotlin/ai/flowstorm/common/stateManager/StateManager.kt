package ai.flowstorm.common.stateManager

import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow

interface StateManager<S> {
    val state: S get() = stateFlow.value
    val stateFlow: StateFlow<S>
    val actionFlow: SharedFlow<Action>
    suspend fun dispatch(action: Action)
}
