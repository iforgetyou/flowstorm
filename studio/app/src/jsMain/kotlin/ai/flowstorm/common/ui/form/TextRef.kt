package ai.flowstorm.common.ui.form

import ai.flowstorm.common.Toast
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import org.w3c.dom.events.MouseEvent
import io.kvision.form.text.Text
import io.kvision.html.Button
import io.kvision.i18n.gettext

open class TextRef(value: String? = null, name: String? = null, label: String? = null, readonly: Boolean = true) :
        Text(value = value, name = name, label = label) {

    protected val mainScope = MainScope()
    private val linkButton = Button("", icon = "fas fa-external-link-alt")

    init {
        add(linkButton)
        this.readonly = readonly
        linkButton.onClick { e ->
            if (e.altKey) {
                val disabled = input.disabled
                input.disabled = false
                @Suppress("UNUSED_VARIABLE")
                val el = input.getElement()
                js("el.select(); el.setSelectionRange(0, 99999);")
                document.execCommand("copy")
                input.disabled = disabled
                input.blur()
                Toast.info(gettext("Copied to clipboard"))
            } else {
                onButtonClick(e)
            }
        }
    }

    open fun onButtonClick(e: MouseEvent) {}
}