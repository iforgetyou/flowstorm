package ai.flowstorm.common.ui

import ai.flowstorm.common.model.HasId
import io.kvision.modal.ModalSize

open class EntityEditorModal<E: HasId>(caption: String, component: EntityEditor<E>, size: ModalSize? = null):
        EntityComponentModal<E, EntityEditor<E>>(caption, component, size), EditableEntityComponent<E>, EntityEditorListener {

    override fun show() = super.show().also {
        component.listener = this
    }

    override suspend fun save() = component.save()

    override suspend fun delete() = component.delete()

    override fun disableAll() = component.disableAll()

    override val isValid get() = component.isValid

    override fun onSave() {
        hide()
        AppPanel.instance.reload()
    }

    override fun onDelete() {
        hide()
        AppPanel.instance.reload()
    }
}