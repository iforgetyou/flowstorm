package ai.flowstorm.common

import ai.flowstorm.studio.App
import io.kvision.routing.Routing
import kotlinx.coroutines.*


/**
 * Workaround for https://github.com/krasimir/navigo/issues/172
 * It can be removed when we upgrade to Navigo 8
 */
object Router {

    suspend fun Routing.setRoute(route: String) = coroutineScope {
        with(this@setRoute) {
            pause()
            navigate(route)
            delay(100)
            resume()
        }
    }

    fun Routing.setRouteLaunch(route: String) = App.scope.launch { setRoute(route) }
}
