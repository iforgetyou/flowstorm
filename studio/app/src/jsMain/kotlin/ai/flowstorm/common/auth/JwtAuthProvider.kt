package ai.flowstorm.common.auth

interface JwtAuthProvider {
    /**
     * Try to authenticate user within existing session.
     */
    suspend fun authenticate():Unit
    fun login()
    fun signup()
    fun logout()
    suspend fun refreshToken()
    var authService: AuthService
}