package ai.flowstorm.common.ui

import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.isNew
import io.kvision.html.*
import io.kvision.i18n.I18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.panel.tabPanel
import io.kvision.panel.TabPanel
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import io.kvision.panel.simplePanel
import kotlinx.serialization.KSerializer

open class EntityEditor<E: HasId>(
        open val entityComponent: EditableEntityComponent<E>,
        val saveEnabled: Boolean = true,
        val deleteEnabled: Boolean = true,
        val caption: String = "Properties",
        val additionalTabs: Map<String, SimplePanel> = mapOf(),
        val topTabs: Boolean = true,
        serializer: KSerializer<E>,
) : SimplePanel(), EditableEntityComponent<E> {

    val mainScope = MainScope()
    var listener: EntityEditorListener? = null
    override var entity: E? = null
        get() = entityComponent.entity
    val entityForm: EntityForm<E>
        get() = entityComponent as EntityForm
    var tabPanel: TabPanel
    val jsonEditor = JsonEditor(serializer)
    var saveButton: Button? = null
    var deleteButton: Button? = null

    open fun otherButtons(panel: SimplePanel) {
    }

    init {
        tabPanel = tabPanel(classes = if (topTabs) setOf("top-tabs") else setOf()) {
            addTab(tr(caption), entityComponent)
            addTab(tr("JSON"), jsonEditor)
            additionalTabs.forEach {
                addTab(tr(it.key), it.value)
            }
        }
        simplePanel(classes = setOf("button-group")) {
            otherButtons(this)
            if (saveEnabled) {
                saveButton = button(tr("Save"), style = ButtonStyle.PRIMARY).onClick {
                    mainScope.launch {
                        save()
                    }
                }
            }
            if (deleteEnabled) {
                deleteButton = button(tr("Delete"), style = ButtonStyle.DANGER) {
                    marginLeft = UiHelpers.spacing
                }.onClick {
                    mainScope.launch {
                        if (ConfirmModal.Delete().open()) {
                            delete()
                        }
                    }
                }
            }
        }
    }

    override val isValid get() = entityComponent.isValid

    protected open fun onload(entity: E): E {
        deleteButton?.disabled = (entity.id.isNew() || deleteButton!!.disabled)
        jsonEditor.setData(entity)
        // we always switch to first tab (due to problems with re-rendering of JSON editor)
        tabPanel.activeIndex = 0
        return entity
    }

    protected open fun unload(): EntityEditor<E> {
        // when rendered in entity table in split mode, swich to first (empty) component in stack panel
        UiHelpers.parent<EntityTable<E>>(this)?.mainPanel?.removeAll()
        return this
    }

    override fun load(entity: E): E = onload(entityComponent.run {
        if (!saveEnabled)
            entityComponent.disableAll()
        load(entity)
    })

    override fun load(id: Id): E = onload(entityComponent.run {
        if (!saveEnabled)
            entityComponent.disableAll()
        load(id)
    })

    override suspend fun save() {
        entityComponent.save()
        if (entityComponent.isValid)
            listener?.onSave()
    }

    override suspend fun delete() {
        unload().entityComponent.delete()
        listener?.onDelete()
    }

    override fun disableAll() {
        entityComponent.disableAll()
        saveButton?.disabled = true
        deleteButton?.disabled = true
    }
}
