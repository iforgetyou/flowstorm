package ai.flowstorm.common.resources

import kotlinx.coroutines.await
import ai.flowstorm.common.RestClient
import ai.flowstorm.common.Toast
import ai.flowstorm.common.id.IdRemoveSerializer
import ai.flowstorm.common.model.Query
import ai.flowstorm.common.model.UnderscorePropertyRemoveSerializer
import kotlinx.coroutines.coroutineScope
import kotlinx.serialization.*
import kotlinx.serialization.builtins.ListSerializer
import io.kvision.i18n.gettext
import io.kvision.rest.HttpMethod

abstract class AbstractResource<E : Any>(
    open val resourceUri: String = "/",
    val serializer: KSerializer<E>) {

    open val client = RestClient.createDefault()
    val resourceUrl get() = (if (!resourceUri.startsWith("http")) client.baseUrl else "") + resourceUri

    private val wrappedSerializer = IdRemoveSerializer(UnderscorePropertyRemoveSerializer(serializer))

    private fun entityToJson(entity: E?) = entity?.let { client.Json.encodeToString(wrappedSerializer, entity) }

    suspend fun <R : Any> remoteCall(
        uri: String = "",
        entity: E? = null,
        method: HttpMethod = HttpMethod.GET,
        deserializer: DeserializationStrategy<R>? = null,
        noResult: Boolean = false,
        doToast: Boolean = true
    ): R = coroutineScope {

        val url = if (uri.startsWith("http")) uri else resourceUrl + uri
        console.log("remoteCall url = $url, method = $method, entity =", entity)

        val p = (if ((method == HttpMethod.DELETE) || noResult)
            client.remoteCall(url, entityToJson(entity), method, beforeSend = client.beforeSend)
        else {
            client.remoteCall(url, entityToJson(entity),
                    deserializer ?: serializer, method, beforeSend = client.beforeSend)
        })
        val result = p.await()
        if (method != HttpMethod.GET) {
            val message = (
                when {
                    entity != null -> entity::class.simpleName?.let { gettext(it) } + " "
                    noResult || method == HttpMethod.DELETE -> ""
                    else -> result::class.simpleName?.let { gettext(it) } + " "
                } +
                when (method) {
                    HttpMethod.POST -> gettext("created")
                    HttpMethod.PUT -> gettext("updated")
                    HttpMethod.DELETE -> gettext("deleted")
                    else -> gettext("processed")
                }
            ).replaceFirstChar(Char::uppercase)
            if (doToast)
                Toast.success(message)
        }
        if (method == HttpMethod.DELETE){
            Any()
        } else {
            result
        }
    } as R

    open suspend fun load(query: Query? = null) = coroutineScope {
        remoteCall( query?.toString() ?: "", null, HttpMethod.GET, deserializer = ListSerializer(serializer))
    }
}
