package ai.flowstorm.common

import kotlin.reflect.KProperty0

/**
 * For setting dynamic property from kotlin object property.
 */
fun setProperty(obj: dynamic, prop: KProperty0<*>) {
    obj[prop.name] = prop.get()
}
