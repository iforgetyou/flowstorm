package ai.flowstorm.common

interface Closeable {
    suspend fun close()
}