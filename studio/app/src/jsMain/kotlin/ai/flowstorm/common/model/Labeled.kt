package ai.flowstorm.common.model

interface Labeled : Matchable {
    val label: String
    override fun match(input: String): Boolean = label.contains(input, true)
}