package ai.flowstorm.common.model

import ai.flowstorm.common.id.Id

typealias IdPair = Pair<Id?, String>

fun <E> E.idPair() where E : Labeled, E : HasId = IdPair(id, label)
