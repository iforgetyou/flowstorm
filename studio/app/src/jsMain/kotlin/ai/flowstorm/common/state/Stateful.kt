package ai.flowstorm.common.state

import io.kvision.state.ObservableState

interface Stateful<T> : ObservableState<T> {
    var state: T
    fun newState(lambda: T.() -> T) {
        state = lambda.invoke(this.state)
    }
}