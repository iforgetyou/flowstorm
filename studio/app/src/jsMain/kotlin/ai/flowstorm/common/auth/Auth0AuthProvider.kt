package ai.flowstorm.common.auth

import io.kvision.require
import kotlinext.js.jsObject
import kotlinx.coroutines.*
import io.kvision.utils.obj
import kotlinx.browser.window

class Auth0AuthProvider : AbstractAuth0Provider() {

    override var auth: dynamic = createAuth()

    private fun createAuth(): dynamic {
        val location = window.location

        @Suppress("UNUSED_VARIABLE")
        val auth0 = require("auth0-js")

        @Suppress("UNUSED_VARIABLE")
        val options = jsObject<dynamic> {
            domain = AUTH_DOMAIN
            clientID = CLIENT_ID
            redirectUri = location.href
            responseType = "token id_token"
            scope = "openid profile email"
            audience = "https://admin.flowstorm"
            leeway = 60
            useRefreshTokens = true
        }
        return js("new auth0.WebAuth(options)")
    }

    override fun login() {
        auth.authorize()
    }

    override fun signup() {
        TODO("Not yet implemented")
    }

    override suspend fun parseHash(hash: String) = coroutineScope {
        val job = Job(coroutineContext[Job])
        auth.parseHash(obj { this.hash = hash }) { error, authResult ->
            if (authResult) {
                onAuthResult(authResult)
                job.complete()
            }
            if (error) {
                job.completeExceptionally(Auth0Error.create(error))
            }
        }
    }
}