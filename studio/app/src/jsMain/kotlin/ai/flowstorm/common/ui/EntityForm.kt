package ai.flowstorm.common.ui

import ai.flowstorm.common.model.HasId
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.isNew
import ai.flowstorm.common.resources.EntityResource
import io.kvision.form.FormControl
import io.kvision.form.FormHorizontalRatio

open class EntityForm<E : HasId>(val model: EntityResource<E>, horizRatio: FormHorizontalRatio = FormHorizontalRatio.RATIO_4) :
        Form<E>(model.serializer, setOf("entity-form"), horizRatio = horizRatio), EditableEntityComponent<E> {

    override var entity: E? = null
    override var isValid: Boolean = false

    override fun load(entity: E): E {
        this.entity = entity
        setData(entity)
        return entity
    }

    override fun load(id: Id): E {
        val e = model.get(id)
        return load(e)
    }

    override suspend fun save() {
        isValid = validate()
        if (isValid) {
            save(entity!!)
            console.log("EntityForm.save", entity)
            createOrUpdate(entity!!)
        }
    }

    protected open suspend fun createOrUpdate(entity: E) {
        if (entity.id.isNew()) {
            model.create(entity)
        } else {
            model.update(entity)
        }
    }

    override suspend fun delete() {
        console.log("EntityForm.delete", entity)
        model.delete(entity!!)
    }

    fun getFields(names: List<String>) = form.fields.filter { names.contains(it.key) }.values

    fun getField(name: String): FormControl? = form.fields[name]
}