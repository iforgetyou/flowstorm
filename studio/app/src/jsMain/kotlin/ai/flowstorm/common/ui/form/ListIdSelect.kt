package ai.flowstorm.common.ui.form

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.IdFactory
import ai.flowstorm.common.model.IdPair
import io.kvision.form.FieldLabel
import io.kvision.form.InvalidFeedback
import io.kvision.form.select.AjaxOptions
import io.kvision.form.select.SelectInput
import io.kvision.panel.SimplePanel

@Suppress("TooManyFunctions")
open class ListIdSelect(
    options: List<IdPair>? = null, value: String? = null, name: String? = null,
    ajaxOptions: AjaxOptions? = null, label: String? = null,
    rich: Boolean = false, classes: Set<String> = setOf()
) : SimplePanel(setOf("form-group")), ListIdFormControl {

    var options: List<IdPair>?
        get() = SelectId.unstringify(input.options)
        set(value) {
            input.options = SelectId.stringify(value)
        }

    override var value: List<Id>?
        get() = if (input.value != null) {
            if (input.value!!.isBlank())
                listOf()
            else
                input.value?.split(ListIdFormControl.SEPARATOR)?.map { IdFactory.create(it) }
        } else listOf()
        set(value) { input.value = value?.joinToString(ListIdFormControl.SEPARATOR) { it.value } }

    var ajaxOptions
        get() = input.ajaxOptions
        set(value) {
            input.ajaxOptions = value
        }

    private val cssClasses = setOf("form-control") union classes

    private val idc = "kv_form_select_$counter"
    final override val input: SelectInput = SelectInput(
            SelectId.stringify(options), value, true, ajaxOptions, cssClasses
    ).apply {
        this.id = idc
        this.name = name
    }
    final override val flabel: FieldLabel = FieldLabel(idc, label, rich)
    final override val invalidFeedback: InvalidFeedback = InvalidFeedback().apply { visible = false }

    init {
        @Suppress("LeakingThis")
        input.eventTarget = this
        this.addInternal(flabel)
        this.addInternal(input)
        this.addInternal(invalidFeedback)
        counter++
    }

    override fun focus() = input.focus()

    override fun blur() = input.blur()

    companion object {
        internal var counter = 0
    }
}