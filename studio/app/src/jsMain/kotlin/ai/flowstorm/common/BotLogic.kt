package ai.flowstorm.common

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.common.ui.UiState
import ai.flowstorm.studio.model.LogEntry
import ai.flowstorm.studio.ui.RunnableComponent
import ai.flowstorm.studio.ui.design.DialogueSourceEditor
import ai.flowstorm.studio.ui.design.dialogue.panel.BotContainerPanel
import ai.flowstorm.studio.ui.design.dialogue.panel.LogPanel
import ai.flowstorm.studio.ui.design.dialogue.panel.RunPanel
import ai.flowstorm.common.auth.AuthService
import ai.flowstorm.common.state.Stateful
import ai.flowstorm.common.state.StatefulValue
import ai.flowstorm.common.ui.StatefulModel
import com.benasher44.uuid.uuid4
import ai.flowstorm.botservice.botService
import io.kvision.utils.obj
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.w3c.dom.*

@Suppress("UNUSED_VARIABLE")
object BotLogic : Stateful<BotLogic.State> by StatefulValue(State()) {

    enum class Status { Running, Stopped, Paused, Building }

    data class State(
            var inputAudio: Boolean = true,
            var outputAudio: Boolean = true,
            var maskSignals: Boolean = true,
            var clientStatus: String = "SLEEPING",
            var runningStatus: Status = Status.Stopped,
    )

    lateinit var botPanel: BotContainerPanel
    val mainScope = MainScope()
    var bot: dynamic = null
    var sessionId: String? = null
    var pastFirstTurn: Boolean = false

    val runConsumer: RunnableComponent
        get() = botPanel.runConsumer
    val runPanel: RunPanel
        get() = botPanel.runPanel

    val audios = mapOf(
            "in" to Audio("https://core.flowstorm.ai/file/assets/audio/pop1.mp3"),
            "out" to Audio("https://core.flowstorm.ai/file/assets/audio/pop2.mp3")
    )


    init {
        audios.keys.forEach {
            audios[it]?.autoplay = false
        }
    }


    // BOT CODE BELOW
    private fun onError(param: dynamic) {
        botPanel.log(LogPanel.LogLine("{ Error: ${param.type} ${param.message} }", false, "danger"))
    }

    fun run(startMessage: String? = null) {
        when (state.clientStatus) {
            "SLEEPING" -> {
                pastFirstTurn = false
                if (state.runningStatus == Status.Stopped) {
                    newState { copy(runningStatus = Status.Building) }
                    mainScope.launch {
                        if (runConsumer is DialogueSourceEditor) {
                            if ((runConsumer as DialogueSourceEditor).modelState.value != StatefulModel.State.Built) {
                                val build = (runConsumer as DialogueSourceEditor).build()
                                if (!build.success) {
                                    newState { copy(runningStatus = Status.Stopped) }
                                    return@launch
                                }
                            }
                        }
                        botPanel.focusTab()
                        startBot(bot, runConsumer.key, startMessage ?: getStartMessage())
                    }
                }
            }
            "RESPONDING" -> {
                bot.pause()
                newState { copy(runningStatus = Status.Paused) }
            }
            "PAUSED" -> {
                bot.resume()
                newState { copy(runningStatus = Status.Running) }
            }
        }
    }

    fun stop() {
        if (state.runningStatus == Status.Paused || state.runningStatus == Status.Running) {
            bot.onStopClick()
            onEnd()
        }
    }

    fun changeAudio(direction: String) {
        if (bot != null) {
            if (direction == "Input") {
                bot.inAudio(state.clientStatus)
            } else {
                bot.outAudio(state.clientStatus)
            }
        }
    }

    fun click() {
        bot.click(state.clientStatus)
    }

    fun sendText(text: String) {
        if (text.isEmpty()) return
        if (state.clientStatus == "LISTENING" || state.clientStatus == "RESPONDING") {
            val audioOn = state.clientStatus == "LISTENING"
            bot.handleOnTextInput(text, audioOn)
        }
    }

    fun onEnd() {
        newState { copy(runningStatus = Status.Stopped) }
        botPanel.setCurrentImage("")
    }

    fun getUUID(): String {
        sessionId = uuid4().toString()
        return sessionId as String
    }

    fun getVoice() = when {
        botPanel.getVoice().isNotBlank() -> botPanel.getVoice()
        runConsumer.voice?.isNotBlank() == true -> runConsumer.voice
        else -> undefined
    }

    fun changeClientStatus(newStatus: dynamic) {
        val newClientStatus = if (newStatus.status != null) newStatus.status else state.clientStatus
        if (state.clientStatus != newClientStatus) {
            botPanel.log(LogPanel.LogLine("{${state.clientStatus} > $newClientStatus}", false, "secondary"))
            newState { copy(clientStatus = newClientStatus) }
        }
    }

    fun addMessage(type: String, text: String?, image: String?, background: String?, signal: String) {
        val prefix = if (type == "sent") ">" else "<"
        if (text != null) {
            if (text.isNotEmpty()) {
                botPanel.log(LogPanel.LogLine("$prefix $text", false, "dark"))
            }
        } else {
            botPanel.logSignal("$prefix $signal")
        }
        if (image != null) {
            botPanel.log(LogPanel.LogLine("{Image ${image}}", false, "secondary"))
            botPanel.setCurrentImage(image)
        }

        if (background != null) {
            botPanel.log(LogPanel.LogLine("{Background ${background}}", false, "secondary"))
            botPanel.setBackground(background)
        }

    }

    private fun getStartMessage(): String {
        val textInput = document.getElementById("chatWindowTextInput")!! as HTMLInputElement
        val startMessage = if (textInput.value.isBlank()) "#intro" else textInput.value
        textInput.value = ""
        return startMessage
    }

    fun reloadPanels() {
        if (botPanel.canReloadPanels) {
            with(runPanel) {
                if (pastFirstTurn) {
                    lastTurnPanel.reload()
                    lastSessionPanel.reload()
                }
                pastFirstTurn = true
                profilePanel.reload()
            }
        }
    }

    fun addLogs(param: dynamic) {
        botPanel.log((param as Array<LogEntry>).map { LogPanel.convertToClickable(it, "info") })
    }

    fun addVideo(video: dynamic, callback: dynamic) {
        botPanel.setVideo(video as String, callback)
    }

    fun playSound(sound: String) {
        if (audios.containsKey(sound) && state.outputAudio) {
            audios[sound]?.play()
        } else {
            console.log(sound)
        }
    }

    fun focusNode(node: Int) {
        if (botPanel.focusNodes && runConsumer is DialogueSourceEditor) {
            val graph = (runConsumer as DialogueSourceEditor).graphEditor.diagramPanel.diagram
            graph.selectNodeById(node)
        }
    }

    fun handleCommand(command: String, code: String) {
        botPanel.log(LogPanel.LogLine("{Command #${command} ${code}}", false, "secondary"))
    }

    fun closeInputAudio() {
        bot.closeAudioStream("User started typing", true)
    }

    fun startTyping() {
        bot.startTyping()
    }

    fun createBot(): dynamic {
        val bot by lazy {
            val wsUrl = AppState.state.coreUrl.replace("http", "ws")
            val deviceId = undefined
            val clientCallback = obj {}
            with(clientCallback) {
                setStatus = { newStatus: dynamic -> changeClientStatus(newStatus) }
                addMessage = { type: dynamic, text: dynamic, message: dynamic, background: dynamic, signal: dynamic -> addMessage(type, text, message, background, signal) }
                addLogs = { logs: dynamic -> addLogs(logs) }
                onError = { error: dynamic -> onError(error) }
                setInputAudio = { setOn: dynamic -> botPanel.setInputAudio(setOn) }
                onEnd = { onEnd() }
                getAttributes = { botPanel.getAttributes().toObj() }
                getUUID = { getUUID() }
                getVoice = { getVoice() }
                play = { sound: dynamic -> playSound(sound) }
                focusOnNode = { node: dynamic -> focusNode(node) }
                handleCommand = { command: dynamic, code: dynamic -> handleCommand(command, code) }
                addVideo = {video: dynamic, callback: dynamic -> addVideo(video, callback)}
            }
            botService(wsUrl, deviceId, true, clientCallback, false)
        }
        return bot
    }


    fun startBot(bot: dynamic, key: String?, startMessage: String) {
        AppState.dispatchLaunch(UiState.Action.ToggleBotPanel(true))

        val language = "en"
        val token = if (AuthService.isAuthenticated) AuthService.context.accessToken.token else null

        bot.init(key, language, BotLogic.state.inputAudio, BotLogic.state.outputAudio, startMessage, state.maskSignals, arrayOf<String>(), true, token)
        newState { copy(runningStatus = Status.Running) }
        this.bot = bot
    }
}
