package ai.flowstorm.common

import ai.flowstorm.common.FeatureSwitch.Env.*
import io.kvision.require

object FeatureSwitch {

    private val conf = require("conf/app.json")

    fun isEnabled(feature: Feature): Boolean {
        return feature.envs.contains(valueOf(conf.env))
    }

    enum class Env {
        _local_, feature, preview, default
    }

    enum class Feature(val envs: List<Env>) {
        simulator(listOf(_local_, feature, preview)),
        ssmlValidator(listOf(_local_, feature, preview)),
        xmlEditor(listOf(_local_, feature, preview))
    }
}

