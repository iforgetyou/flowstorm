package ai.flowstorm.common.ui

import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import ai.flowstorm.common.Closeable
import ai.flowstorm.common.WindowUnloadHandler
import io.kvision.core.Component
import io.kvision.core.onEvent
import io.kvision.panel.Tab
import io.kvision.panel.TabPanel
import io.kvision.panel.TabPanelNavDropDown
import kotlinx.browser.document

/**
 * Designer panel allows to edit multiple entities at the same time in fullscreen tab panel.
 */
abstract class DesignerPanel : TabPanel(classes = setOf("designer"), scrollableTabs = true, draggableTabs = true,
    init = {
        nav = TabPanelNavDropDown(this, navClasses)
    }
) {

    val mainScope = MainScope()

    private val tabClasses = listOf("tab-new", "tab-saved", "tab-built")

    init {
        onEvent {
            tabClosing = {
                val tab = getTab(it.detail.data.toString().toInt())!!
                if (getSize() > 1) {
                    mainScope.launch {
                        (tab.component as Closeable).close()
                        document.getElementsByClassName("tooltip").item(0)?.remove()
                        removeTab(tab)
                        refresh()
                    }
                }
                it.preventDefault()
            }
        }

        WindowUnloadHandler.register {
            getTabs().any {
                val component = it.component
                component is StatefulModel && component is NamedComponent
                        && component.modelState.value === StatefulModel.State.Changed
            }
        }
    }

    private fun setTabLabel(tab: Tab, label: String, classes: List<String> = listOf()) {
        tab.label = label
        tab.setLinkTooltip(label)
        tabClasses.forEach { tab.removeCssClass(it) }
        classes.forEach { tab.addCssClass(it) }
    }

    fun removeTabByComponent(component: Component) {
        getTabs().firstOrNull { it.component == component }?.let { tab ->
            removeTab(tab)
        }
    }

    override fun addTab(tab: Tab, position: Int?) {
        super.addTab(tab, position)
        // RootPanel.resizeBody()
        val component = tab.component
        if (component is StatefulModel) {
            component.modelState.subscribe {
                require(component is NamedComponent)
                when (it) {
                    StatefulModel.State.New, StatefulModel.State.Changed -> setTabLabel(tab, component.name, listOf("tab-new"))
                    StatefulModel.State.Saved -> setTabLabel(tab, component.name, listOf("tab-saved") )
                    StatefulModel.State.Built -> setTabLabel(tab, component.name, listOf("tab-built"))
                }
            }
        }
    }

    override fun afterDestroy() {
        document.getElementsByClassName("tooltip").item(0)?.remove()
        super.afterDestroy()
    }
}
