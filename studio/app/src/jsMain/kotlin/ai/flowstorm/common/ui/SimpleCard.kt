package ai.flowstorm.common.ui

import io.kvision.html.div

open class SimpleCard(caption: String = "", text: String = "") : CardPanel(caption) {
    init {
        addPanelHeader()
        div(text, className = "content-panel")
    }
}