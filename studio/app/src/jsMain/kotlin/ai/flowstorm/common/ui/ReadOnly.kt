package ai.flowstorm.common.ui

interface ReadOnly {
    var readOnly: Boolean
}