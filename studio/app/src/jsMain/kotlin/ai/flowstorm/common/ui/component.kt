package ai.flowstorm.common.ui

import io.kvision.core.Component
import io.kvision.panel.Root
import io.kvision.panel.StackPanel
import io.kvision.panel.Tab
import io.kvision.panel.TabPanel
import io.kvision.routing.routing

fun Component.navigate(route: String, params: dynamic) {
    var c: Component = this
    while (c.parent != null && !(c.parent is Root)) {
        val p = c.parent!!
        if (p is StackPanel) p.activeChild = c
        if (p is TabPanel) p.activeTab = c as Tab
        if (c is Navigable) c.onNavigate(route, params)

        c = p
    }
}

fun <T : Component> T.route(route: String, handler: ((params: dynamic) -> Unit)? = null): T {
    with(routing) {
        on(route, { params ->
            navigate(route, params)
            handler?.invoke(params)
        })
    }
    return this
}