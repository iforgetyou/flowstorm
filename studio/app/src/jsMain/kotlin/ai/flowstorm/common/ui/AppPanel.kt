package ai.flowstorm.common.ui

import ai.flowstorm.studio.App
import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.studio.ui.design.DialogueSourceDesigner
import ai.flowstorm.studio.ui.design.DialogueSourceEditor
import ai.flowstorm.studio.ui.design.dialogue.panel.AssistantPanel
import ai.flowstorm.common.BotLogic
import ai.flowstorm.common.Initiable
import ai.flowstorm.common.Reloadable
import io.kvision.jquery.invoke
import io.kvision.jquery.jQuery
import io.kvision.core.Component
import io.kvision.data.dataContainer
import io.kvision.html.Ul
import io.kvision.html.li
import io.kvision.html.link
import io.kvision.i18n.tr
import io.kvision.state.observableListOf

abstract class AppPanel : RootPanel(), Navigable {

    companion object {
        lateinit var instance: AppPanel
    }

    protected val panelHistory = observableListOf<Pair<String, Panel>>()


    init {
        instance = this
        footer.apply {
            dataContainer(panelHistory, factory = { panel: Pair<String, Panel>, i: Int, _: MutableList<Pair<String, Panel>> ->
                li(classes = setOf("nav-item")) {
                    val classes = mutableSetOf("nav-link")
                    if (i == 0) classes.add("active")
                    link(tr(panel.second.caption), "#!" + panel.first, iconFor(panel.second)?.def, classes = classes)
                }
            }, Ul(classes = setOf("nav", "nav-tabs", "nav-tabs-bottom")))
        }
    }

    private fun activatePanel(panel: Panel) {
        body.activeChild = panel
        // resizeBody()
        focus(panel)
    }

    open fun focus(component: Component) {
        if (component is Reloadable)
            component.reload()
        if (component is CardPanel)
            jQuery("body").css("background-image", "none")
        if (component is Initiable)
            component.init()
    }

    override fun onNavigate(route: String, params: dynamic) {
        val panel = bodyComponent as Panel
        AssistantPanel.route = route
        if (!panelHistory.isEmpty() && AppState.state.routeAssist)
            launchAssistant()
        if (panel is DialogueSourceDesigner) {
            if (panel.getTabs().isNotEmpty()) {
                val editor = (panel.activeTab!!.component as DialogueSourceEditor)
                AssistantPanel.dialoguePanel.botPanel = editor.runPanel.botPanel
                AssistantPanel.dialoguePanel.setControls(true)
            }

        } else {
            AssistantPanel.dialoguePanel.setControls(false)
        }
        activatePanel(panel)
        addPanelToHistory(panel, route)
    }

    private fun addPanelToHistory(panel: Panel, route: String) = with(panelHistory) {
        removeAll { it.second == panel }
        add(0, route to panel)
        if (size > 6) removeLast()
    }

    fun launchAssistant(startMessage: String = "#signal") {
        if (BotLogic.state.runningStatus == BotLogic.Status.Running)
            BotLogic.stop()
        BotLogic.botPanel = AssistantPanel.assistantPanel.botPanel
        BotLogic.startBot(AssistantPanel.assistantPanel.bot, App.config.assistantKey, startMessage)
    }

    fun reload() {
        if (body.activeChild is Reloadable) {
            if (body.activeChild is EntityTable<*>)
                (body.activeChild as EntityTable<*>).needsReload = true
            (body.activeChild as Reloadable).reload()
        }
    }
}
