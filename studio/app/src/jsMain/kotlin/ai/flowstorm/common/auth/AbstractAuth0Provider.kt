package ai.flowstorm.common.auth

import ai.flowstorm.studio.util.now
import kotlinx.browser.window
import kotlinx.coroutines.*
import ai.flowstorm.common.ui.AuthLog
import io.kvision.utils.obj

abstract class AbstractAuth0Provider : JwtAuthProvider {

    val mainScope = MainScope()

    override lateinit var authService: AuthService

    abstract var auth: dynamic

    override fun logout() {
        auth.logout(obj {
            returnTo = window.location.href.substringBefore("#")
            clientId = CLIENT_ID
        })
    }

    override suspend fun authenticate() {
        val hash = window.location.hash
        if (hash.startsWith("#access_token=")) {
            parseHash(hash)
            window.location.hash = ""
        } else {
            try {
                checkSession()
            } catch (t: Auth0Error.LoginRequired) {
                //Ignore login required error
            }
        }
    }

    protected fun onAuthResult(authResult: dynamic) {
        AuthLog.timeLog("Token refreshed.")
        scheduleRefresh(authResult.expiresIn)
        authService.context = AuthContext(
            JwtToken(authResult.idToken),
            JwtToken(authResult.accessToken),
            false,
        )
    }

    abstract suspend fun parseHash(hash: String): Unit

    private suspend fun checkSession() = coroutineScope {
        val job = Job(coroutineContext[Job])
        auth.checkSession(obj { }) { error, authResult ->
            if (authResult) {
                onAuthResult(authResult)
                job.complete()
            }
            if (error) {
                job.completeExceptionally(Auth0Error.create(error))
            }
        }
    }

    private var expires: Int = 0 // in seconds

    @Suppress("unused")
    private val refreshJob = mainScope.launch {
        while (true) {
            delay(2000)
            if (expires > 0 && expires < timestamp() + EXPIRE_IN_THRESHOLD) {
                AuthLog.timeLog("Token is about to expire. Refreshing.")
                refreshToken()
            }
        }
    }

    override suspend fun refreshToken() {
        try {
            checkSession()
        } catch (t:Throwable) {
            AuthLog.timeLog("Refreshing token failed: ${t.message}")
        }
    }

    private fun timestamp() = now().epochSeconds.toInt()

    private fun scheduleRefresh(expiresIn: Number) {
        AuthLog.timeLog("Token expires in ${expiresIn}s. Scheduled for refresh.")
        expires = timestamp() + expiresIn.toInt()
    }

    open class Auth0Error(val error: dynamic) : Exception(error.description as String) {
        val code get() = error.code

        class LoginRequired(error: dynamic) : Auth0Error(error)

        companion object {
            fun create(error: dynamic) =
                    when (error.code) {
                        "login_required" -> LoginRequired(error)
                        else -> Auth0Error(error)
                    }
        }
    }

    companion object {
        protected const val CLIENT_ID = "yAFOkdKX7cQClregph9WejJ7UNjxGu10"
        protected const val AUTH_DOMAIN = "auth.flowstorm.ai"
        protected const val EXPIRE_IN_THRESHOLD = 120 // token is refreshed
    }
}