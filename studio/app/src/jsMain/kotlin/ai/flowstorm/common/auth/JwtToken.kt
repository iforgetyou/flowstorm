package ai.flowstorm.common.auth

import io.kvision.require

data class JwtToken<P>(val token: String) {
    val payload: P by lazy { jwtDecode(token) }

    companion object {
        private val jwtDecode = require("jwt-decode")
    }
}