package ai.flowstorm.common.id

object IdFactory {
    fun create() = TempId()
    fun create(value: String) = when {
        TempId.isTempId(value) -> TempId(value)
        ObjectId.isValid(value) -> ObjectId(value)
        else -> StringId(value)
    }
}

fun Id.isNew() = this is TempId

fun newId() = IdFactory.create()