package ai.flowstorm.common

import io.kvision.state.ObservableListWrapper

fun <T> ObservableListWrapper<T>.replaceAll(elements: Collection<T>) {
    mutableList.clear()
    mutableList.addAll(elements)
    onUpdate.forEach { it(this) }
}