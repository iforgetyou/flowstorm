package ai.flowstorm.common.ui.form

import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.IdFactory
import io.kvision.form.FormControl

interface IdFormControl: FormControl {

    var value: Id?

    override fun getValue(): Id? = value
    override fun setValue(v: Any?) {
        value = when (v) {
            null -> v
            is Id -> v
            is String -> IdFactory.create(v)
            else -> error("Can't set value")
        }
    }

    override fun getValueAsString(): String? = value?.value.toString()
}
