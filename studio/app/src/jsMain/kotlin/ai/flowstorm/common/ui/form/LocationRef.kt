package ai.flowstorm.common.ui.form

import ai.flowstorm.common.ui.LocationModal
import ai.flowstorm.core.type.Location
import ai.flowstorm.core.type.toLocation
import kotlinx.coroutines.launch
import org.w3c.dom.events.MouseEvent
import io.kvision.i18n.I18n.tr

class LocationRef(label: String = tr("Location"), private val editable: Boolean = true, val store: () -> Unit = {}) : TextRef(label = label, readonly = false) {

    override fun onButtonClick(e: MouseEvent) {
        mainScope.launch {
            LocationModal(value?.toLocation() ?: Location(0.0, 0.0), editable).open().let { location ->
                value = location.toString()
                store()
            }
        }
    }
}