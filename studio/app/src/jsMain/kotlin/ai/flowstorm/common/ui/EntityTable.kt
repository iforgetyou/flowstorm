package ai.flowstorm.common.ui

import ai.flowstorm.studio.common.state.AppState
import ai.flowstorm.common.ErrorHandler
import ai.flowstorm.common.Reloadable
import ai.flowstorm.common.Initiable
import ai.flowstorm.common.id.Id
import ai.flowstorm.common.id.IdFactory
import ai.flowstorm.common.model.*
import ai.flowstorm.common.replaceAll
import ai.flowstorm.common.resources.EntityResource
import io.kvision.jquery.*
import io.kvision.core.*
import io.kvision.form.text.TextInput
import io.kvision.form.text.TextInputType
import io.kvision.form.text.textInput
import io.kvision.html.*
import io.kvision.tabulator.*
import io.kvision.i18n.I18n.tr
import io.kvision.modal.Modal
import io.kvision.modal.ModalSize
import io.kvision.panel.*
import io.kvision.tabulator.Tabulator
import io.kvision.utils.perc
import io.kvision.utils.px
import io.kvision.tabulator.js.Tabulator as JsTabulator
import io.kvision.tabulator.js.Tabulator.ColumnComponent
import kotlinx.coroutines.*
import ai.flowstorm.common.ui.Hints.hint
import com.github.snabbdom.VNode
import io.kvision.state.ObservableListWrapper
import io.kvision.state.observableListOf
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlin.js.Date

/**
 * @var caption table caption
 * @var model entity model
 * @var tabulatorOptions tabulator component options
 * @var entityFormFactory factory creating entity forms for creation/editation of model entity
 * @var entityFactory factory creating new entities. if set to null as well as entityCreateFactory,
 * table will be shown without create functionality.
 * @var entityCreatorFactory factory creating create component (optional). if not defined, entity form will be used
 * @var deleteEnabled enabling delete operation from entity editor
 */
abstract class EntityTable<E : HasId>(
        caption: String,
        val model: EntityResource<E>,
        var tabulatorOptions: TabulatorOptions<E>,
        val entityComponentFactory: (() -> EntityComponent<E>)? = null,
        val entityFactory: (() -> E)? = null,
        val entityCreatorFactory: (() -> Component)? = null,
        val entityEditorFactory: ((EditableEntityComponent<E>, Boolean, Boolean) -> EntityEditor<E>)? = null,
        val filterFormFactory: (() -> FilterForm)? = null,
        val saveEnabled: Boolean = true,
        val deleteEnabled: Boolean = true,
        val createEnabled: Boolean = true,
        val splitMode: Boolean = false,
        tabWidth: CssSize = 640.px,
        val modalSize: ModalSize? = null,
        val seekEnabled: Boolean = true,
        val searchEnabled: Boolean = false,
        hideHeader: Boolean = false,
        className: String? = null,
        /**
         * Tabulator converts data into plain JS objects, which causes problems in complex objects.
         * In this case you can use extractor and define how to get the data from the object.
         */
        val dataExtractor: ((E) -> dynamic)? = null,
) : CardPanel(caption + "s", hideHeader), Reloadable, Seekable, Initiable, EntityEditorListener {

    private val mainScope = MainScope()
    var query: Query = Query()
    var mainPanel: VPanel = vPanel()
    var needsReload: Boolean = true
    private lateinit var searchInput: TextInput
    private var tabulator: Tabulator<E>
    private var lastScrollTop: Float = 0F
    private var currentId: String? = null
    private val tabulatorIndexName = "tabulator_index"

    private fun createEntityCreator(): Component? {
        val entityCreatorComponent = entityCreatorFactory?.let { it() }
                ?: createEntityComponent(saveEnabled = true, deleteEnabled = false)
        return if (entityCreatorComponent == null)
            null
        else when {
            splitMode -> entityCreatorComponent
            entityCreatorComponent is EntityEditor<*> -> EntityEditorModal(caption, entityCreatorComponent, modalSize)
            else -> Modal(caption, size = modalSize) { add(entityCreatorComponent) }
        }
    }

    private fun createWrappedEntityComponent(): EntityComponent<E>? {
        val entityComponent = createEntityComponent(saveEnabled, deleteEnabled)
        return when {
            splitMode -> entityComponent
            (entityComponent is EntityEditor<E>) -> EntityEditorModal(caption, entityComponent, modalSize)
            (entityComponent is EntityComponent<E>) -> EntityComponentModal(caption, entityComponent, modalSize)
            else -> entityComponent
        }
    }

    private fun createEntityComponent(saveEnabled: Boolean, deleteEnabled: Boolean): EntityComponent<E>?  = entityComponentFactory?.let {
        val entityComponent = it()

        if (entityComponent is EditableEntityComponent<E> && entityComponent !is EntityEditor) {
            entityEditorFactory?.invoke(entityComponent, saveEnabled, deleteEnabled)
                    ?: EntityEditor(entityComponent, saveEnabled, deleteEnabled, serializer = model.serializer)
        } else {
            entityComponent
        }
    }

    val data: ObservableListWrapper<E> = observableListOf<E>() as ObservableListWrapper<E>

    init {
        addPanelHeader()
        tabulatorOptions = tabulatorOptions.copy(
            height = "100%", //table needs initial height, otherwise, resizing and scroll does not work
            selectable = if (splitMode) 1 else 0,
            headerSort = tabulatorOptions.headerSort ?: false,
            index = tabulatorIndexName,
        )

        addCssClass("entity-table")

        model.entities.subscribe {
            val elements = it.map { e ->
                val obj = dataExtractor?.let { it(e) } ?: e
                obj[tabulatorIndexName] = e.id.value
                obj
            }
            data.replaceAll(elements)
        }

        tabulator = Tabulator(data, options = tabulatorOptions).apply {
            setEventListener<Tabulator<dynamic>> {
                tabulatorRowClick = { e ->
                    val index:String = e.detail.unsafeCast<JsTabulator.RowComponent>().getIndex() as String
                    val id = IdFactory.create(index)
                    currentId = id.value

                    val entityComponent = createWrappedEntityComponent()
                    if (entityComponent != null) {
                        entityComponent.load(id)
                        showEntityComponent(entityComponent)
                    }
                    this@apply.selectRow(e.detail.unsafeCast<JsTabulator.RowComponent>())
                }
            }
            setFilter { e: dynamic ->
                //tabulator convert entities to plain object, we need to get actual entity from model.entities
                val entity = model.entities.first { it.id.value == e[tabulatorIndexName] }
                if (entity is Matchable) {
                    entity.match(searchInput.value ?: "")
                } else {
                    true
                }
            }
        }
        simplePanel(className = className) {
            if (splitMode) {
                splitPanel {
                    width = 100.perc
                    height = 100.perc
                    simplePanel {
                        minWidth = tabWidth
                        add(tabulator)
                    }
                    add(mainPanel)
                }
            } else {
                add(tabulator)
            }
        }

        AppState.sub { it.spaceId }.onEach { needsReload = true }.launchIn(mainScope)
    }

    override fun onSave() {
        mainPanel.removeAll()
    }

    override fun onDelete() {
        reload()
    }

    open fun showEntityComponent(entityComponent: Component) {
        if (entityComponent is Modal) {
            (entityComponent).show()
        } else {
            with(mainPanel) {
                removeAll()
                add(entityComponent)
            }
            if (entityComponent is EntityEditor<*>)
                entityComponent.listener = this
        }
    }

    private val searchInputDebouncer = Debouncer()

    override fun actionComponent() = SimplePanel {
        searchInput = textInput(TextInputType.SEARCH) {
            width = 200.px
            display = if(searchEnabled) Display.INLINE else Display.NONE
            placeholder = tr("Quick search...")
            hint("SearchTableInput", Icon.Search)
            onEvent {
                input = {
                    mainScope.launch { searchInputDebouncer.run { reload(false) } }
                }
            }
        }
        button("", "fas fa-sync-alt", ButtonStyle.INFO) {
            marginLeft = UiHelpers.spacing
            hint("ReloadTableButton", Icon.Reload)
        }.onClick {
            reload(false)
        }
        if ((entityFactory != null || entityCreatorFactory != null) && createEnabled) {
            button("", "fas fa-plus") {
                marginLeft = UiHelpers.spacing
            }.onClick {
                @Suppress("UNUSED_VARIABLE")
                val jsTabulator = tabulator.jsTabulator
                js("jsTabulator.deselectRow();")
                val entityCreatorComponent = createEntityCreator()
                if (entityCreatorComponent != null) {
                    if (entityCreatorComponent is EntityComponent<*>)
                        entityCreatorComponent.unsafeCast<EntityComponent<E>>().load(entityFactory!!())
                    showEntityComponent(entityCreatorComponent)
                }
            }
        }

        button("","fas fa-search") {
            hint("FilterTableButton", Icon.Search)
            marginLeft = UiHelpers.spacing
            style = ButtonStyle.PRIMARY
            disabled = filterFormFactory == null
        }.onClick {
            val dialog = FilterDialog(filterFormFactory!!.invoke(), query) {
                style = if (query.filters.isEmpty() && query.sort == null) ButtonStyle.PRIMARY else ButtonStyle.WARNING
                reload(false)
            }

            dialog.show()
        }
    }

    fun registerScrollEvent() {
        val holder = jQuery(".tabulator-tableHolder")
        val table = jQuery(".tabulator-table")

        holder.on("scroll") { _, _ ->
            val scrollTop = holder.scrollTop().toFloat()
            val height = holder.height().toFloat()
            val innerHeight = table.height().toFloat()

            val paddingTop = table.css("padding-top").substringBefore("px").toFloat()
            val paddingBottom = table.css("padding-bottom").substringBefore("px").toFloat()
//            console.log("Values: ", scrollTop, height, innerHeight, paddingTop, paddingBottom, "SUM", (innerHeight + paddingTop + paddingBottom) - height)

            val tolerance = 1 //to avoid rounding issues
            if (scrollTop > 0 && scrollTop + tolerance >= (innerHeight + paddingTop + paddingBottom) - height) {
                loadMore()
            }
            lastScrollTop = scrollTop
            Unit
        }
    }

    override fun reload() {
        if (!needsReload) {
            val holder = jQuery(".tabulator-tableHolder")
            holder.scrollTop(lastScrollTop)
            return
        }
        reload(false)
        needsReload = false
    }

    fun reload(clearOnly: Boolean) {
        query = query.copy(seek_id = null, limit = if(seekEnabled) getLimit() else null, search = searchInput.value)

        model.clear()
        mainPanel.removeAll()
        currentId = null
        mainScope.launch {
            try {
                if (!clearOnly)
                    model.reload(query)
                tabulator.redraw()
            } catch (t: Throwable) {
                ErrorHandler.handle(Exception("Can not reload table ${this@EntityTable::class.simpleName}", t))
            }
        }
    }

    private fun getLimit(): Int {
        val holder = jQuery(".tabulator-tableHolder")
        return (holder.height().toInt() / 48) + 3
    }

    private var loading = false

    override fun loadMore() {
        if (loading || !seekEnabled) return
        query = query.copy(seek_id = model.entities.last().id, limit = getLimit())
        mainScope.launch {
            try {
                loading = true
                val size = model.entities.size
                model.loadMore(query)
                if(size < model.entities.size) tabulator.redraw()
                highlightRow()
                loading = false
            } finally {
                loading = false
            }
        }
    }

    override fun init() {
            registerScrollEvent()
    }

    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        highlightRow()
    }

    private fun highlightRow() {
        if (splitMode) {
            tabulator.jsTabulator?.getRows("active")
                ?.firstOrNull {
                    it.getData().asDynamic()[tabulatorIndexName] == currentId
                }?.let {
                    tabulator.selectRow(it)
                }
        }
    }

    companion object {
        fun <T> sorterFunction(name: String, comparator: ((val1: T, val2: T) -> Int)? = null):
                (dynamic, dynamic, JsTabulator.RowComponent, JsTabulator.RowComponent, ColumnComponent, SortingDir, dynamic) -> Number =
        { _, _, row1, row2, _, _, _ ->

            val val1 = row1.getData().asDynamic()[name]
            val val2 = row2.getData().asDynamic()[name]
            comparator?.invoke(val1 as T, val2 as T) ?:
                if (val1 is Comparable<T> && val2 is Comparable<T>)
                    val1.compareTo(val2 as T)
                else if (val1 is Date && val2 is Date)
                    val1.getTime() - val2.getTime()
                else
                    0
        }
    }
}
