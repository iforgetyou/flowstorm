package ai.flowstorm.common.stateManager

interface StateReducer<S, A : ReducerAction> {
    fun reduce(state: S, action: A): S
    fun canReduce(action: ReducerAction): Boolean
}
