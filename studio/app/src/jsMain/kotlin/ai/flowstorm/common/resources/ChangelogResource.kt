package ai.flowstorm.common.resources

import ai.flowstorm.common.RestClient
import ai.promethist.services.changelog.model.Changelog
import io.kvision.rest.HttpMethod

object ChangelogResource {

    suspend fun load(url: String): Changelog = RestClient().remoteCall(
        url = url,
        method = HttpMethod.GET,
        deserializer = Changelog.serializer()
    )
}