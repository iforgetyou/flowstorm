package ai.flowstorm.common.ui

interface Seekable {
    fun loadMore()
}