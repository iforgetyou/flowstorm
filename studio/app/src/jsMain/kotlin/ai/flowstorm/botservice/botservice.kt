@file:JsModule("@flowstorm/bot-service")
@file:JsNonModule
package ai.flowstorm.botservice

@JsName("default")
external val botService: dynamic
