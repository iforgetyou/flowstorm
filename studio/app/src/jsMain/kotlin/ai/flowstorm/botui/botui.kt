@file:JsModule("@flowstorm/bot-ui")
@file:JsNonModule
package ai.flowstorm.botui

@JsName("default")
external class BotUI(node: String, options: dynamic) {}
