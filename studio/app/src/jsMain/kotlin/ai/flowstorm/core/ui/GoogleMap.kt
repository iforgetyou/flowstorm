package ai.flowstorm.core.ui

import com.github.snabbdom.VNode
import ai.flowstorm.core.type.DEFAULT_LOCATION
import ai.flowstorm.core.type.Location
import io.kvision.panel.SimplePanel
import io.kvision.utils.obj

class GoogleMap(
    var center: Location = DEFAULT_LOCATION,
    var zoom: Int = 6,
    var initMarkers: List<Location> = mutableListOf(),
    val processWaypoint: (value: dynamic) -> Unit = {},
    val clickable: Boolean = false,
    val darkMode: Boolean = false,
    classes: Set<String> = setOf(),
    init: (GoogleMap.() -> Unit)? = null
        ): SimplePanel(classes = classes) {

    var currentZoom = zoom
    var currentCenter = locationToDynamic(center)
    var dynamicMarkers = initMarkers.map {
        "lat=${it.latitude},lng=${it.longitude}" to locationToDynamic(it)
    }.toMap().toMutableMap()
     var map: dynamic = null

    var markerMap = mutableMapOf<String, dynamic>()

    init {
        init?.invoke(this)
    }

    fun addMarker(location: Location){
        val position = locationToDynamic(location)
        dynamicMarkers["lat=${location.latitude},lng=${location.longitude}"] = position
    }

    fun setCurrentCenter (value: dynamic){
        this.currentCenter = value
    }

    fun setCurrentZoom (value: dynamic){
        this.currentZoom = value
    }

    fun addWaypoint (latLng: dynamic, map: dynamic) {
        if (clickable) {
            val value = obj {
                lat = latLng.lat()
                lng = latLng.lng()
            }
            processWaypoint(value)
            val key = "lat=${value.lat},lng=${value.lng}"
            dynamicMarkers[key] = value
            createMarker(value, map)
        }
    }

    fun createMarker(pos: dynamic, map: dynamic) {
        @Suppress("UNUSED_VARIABLE")
        val p = pos
        @Suppress("UNUSED_VARIABLE")
        val m = map
        val marker = js("new google.maps.Marker({ position: p, map: m });")
        val key = "lat=${pos["lat"]},lng=${pos["lng"]}"
        markerMap[key] = marker
    }

    fun centerOnLocation (location: Location) {
        @Suppress("UNUSED_VARIABLE")
        val map = this.map
        @Suppress("UNUSED_VARIABLE")
        val position = locationToDynamic(location)
        js("map.setCenter(position)")
        js("map.setZoom(15)")
    }

    @Suppress("UNUSED_ANONYMOUS_PARAMETER")
    fun clearMarkers() {
        markerMap.values.forEach { marker ->
            js("marker.setMap(null);")
        }
        markerMap = mutableMapOf()
        dynamicMarkers = mutableMapOf()
    }

    fun removeMarker(location: Location) {
        val pos = locationToDynamic(location)
        val key = "lat=${pos["lat"]},lng=${pos["lng"]}"
        @Suppress("UNUSED_VARIABLE")
        val marker = markerMap[key]
        js("marker.setMap(null);")
        markerMap.remove(key)
        dynamicMarkers.remove(key)
    }

    fun locationToDynamic(location: Location) = obj {
        lat = location.latitude
        lng = location.longitude
    }

    override fun afterInsert(node: VNode) {
        super.afterInsert(node)
        @Suppress("UNUSED_VARIABLE")
        val element = node.elm
        val map = js("new google.maps.Map(element, { zoom: this.currentZoom, center: this.currentCenter });")
        val styles = js(
            """
            {styles: [
              { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
              { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
              { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
              {
                featureType: "administrative.locality",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "poi",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "poi.park",
                elementType: "geometry",
                stylers: [{ color: "#263c3f" }],
              },
              {
                featureType: "poi.park",
                elementType: "labels.text.fill",
                stylers: [{ color: "#6b9a76" }],
              },
              {
                featureType: "road",
                elementType: "geometry",
                stylers: [{ color: "#38414e" }],
              },
              {
                featureType: "road",
                elementType: "geometry.stroke",
                stylers: [{ color: "#212a37" }],
              },
              {
                featureType: "road",
                elementType: "labels.text.fill",
                stylers: [{ color: "#9ca5b3" }],
              },
              {
                featureType: "road.highway",
                elementType: "geometry",
                stylers: [{ color: "#746855" }],
              },
              {
                featureType: "road.highway",
                elementType: "geometry.stroke",
                stylers: [{ color: "#1f2835" }],
              },
              {
                featureType: "road.highway",
                elementType: "labels.text.fill",
                stylers: [{ color: "#f3d19c" }],
              },
              {
                featureType: "transit",
                elementType: "geometry",
                stylers: [{ color: "#2f3948" }],
              },
              {
                featureType: "transit.station",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "water",
                elementType: "geometry",
                stylers: [{ color: "#17263c" }],
              },
              {
                featureType: "water",
                elementType: "labels.text.fill",
                stylers: [{ color: "#515c6d" }],
              },
              {
                featureType: "water",
                elementType: "labels.text.stroke",
                stylers: [{ color: "#17263c" }],
              },
            ]} 
        """)
        this.map = map
        with(map) {
            setCurrentCenter = { newCenter: dynamic -> setCurrentCenter(newCenter) }
            setCurrentZoom = { newZoom: dynamic -> setCurrentZoom(newZoom) }
            addWaypointToMap = { waypointLocation: dynamic, map: dynamic -> addWaypoint(waypointLocation, map) }
            setOptions(if (darkMode) styles else "{}")
        }
        js("map.addListener('center_changed', function() { map.setCurrentCenter(map.getCenter()) })")
        js("map.addListener('zoom_changed', function() { map.setCurrentZoom(map.getZoom()) })")
        js("map.addListener('rightclick', function(event) { map.addWaypointToMap(event.latLng, this) })")
        dynamicMarkers.values.forEach {
            createMarker(it, map)
        }
    }
}