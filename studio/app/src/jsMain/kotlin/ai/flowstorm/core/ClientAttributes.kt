package ai.flowstorm.core

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import io.kvision.utils.obj

@Serializable
data class ClientAttributes(
        var clientType: String = "web:2.0.0",
        var clientScreen: Boolean = true,
        var clientLocation: String = "lat=50.101,lng=14.395",
        var clientTemperature: Double = 20.0,
        var clientAmbientLight: Double = 0.0,
        var clientSpatialMotion: Double = 0.0,
        var clientSpeechAngle: Int = -1
) {
    @Transient
    val other = mutableMapOf<String, Any>()

    operator fun set(key: String, value: Any) {
        val str = value.toString()
        when (key) {
            "clientType" -> clientType = str
            "clientScreen" -> clientScreen = str.toBoolean()
            "clientLocation" -> clientLocation = str
            "clientTemperature" -> clientTemperature = str.toDouble()
            "clientAmbientLight" -> clientAmbientLight = str.toDouble()
            "clientSpatialMotion" -> clientSpatialMotion = str.toDouble()
            "clientSpeechAngle" -> clientSpeechAngle = str.toInt()
            else -> if (key.isNotBlank())
                other[key] = value
        }
    }

    operator fun get(key: String) = when (key) {
        "clientType" -> clientType
        "clientScreen" -> clientScreen
        "clientLocation" -> clientLocation
        "clientTemperature" -> clientTemperature
        "clientAmbientLight" -> clientAmbientLight
        "clientSpatialMotion" -> clientSpatialMotion
        "clientSpeechAngle" -> clientSpeechAngle
        else -> other[key]
    }

    fun fromText(text: String): ClientAttributes {
        val map = text.split("\n").map {
            val pair = it.split("=")
            pair[0] to pair.subList(1, pair.size).joinToString("=")
        }.toMap()
        other.clear()
        map.forEach {
            this[it.key] = it.value
        }
        return this
    }

    fun copyTo(attributes: ClientAttributes, copyOther: Boolean = false) {
        attributes.clientType = clientType
        attributes.clientScreen = clientScreen
        attributes.clientLocation = clientLocation
        attributes.clientTemperature = clientTemperature
        attributes.clientAmbientLight = clientAmbientLight
        attributes.clientSpatialMotion = clientSpatialMotion
        attributes.clientSpeechAngle = clientSpeechAngle
        if (copyOther) {
            attributes.other.clear()
            attributes.other.putAll(other)
        }
    }

    fun toMap() = mutableMapOf<String, Any>().also {
        it["clientType"] = clientType
        it["clientScreen"] = clientScreen
        it["clientLocation"] = clientLocation
        it["clientTemperature"] = clientTemperature
        it["clientAmbientLight"] = clientAmbientLight
        it["clientSpatialMotion"] = clientSpatialMotion
        it["clientSpeechAngle"] = clientSpeechAngle
        it.putAll(other)
    }

    fun toObj() = asObj(this)

    fun toText() = toMap().entries.joinToString("\n") { "${it.key}=${it.value}" }

    companion object {
        private val integerRegex = Regex("[1-9]+[0-9]?")
        private val decimalRegex = Regex("^[+-]?(\\d*\\.)?\\d+\$")

        fun asObj(attributes: ClientAttributes): dynamic {
            val result = obj {
                clientType = attributes.clientType
                clientScreen = attributes.clientScreen
                clientLocation = attributes.clientLocation
                clientTemperature = attributes.clientTemperature
                clientAmbientLight = attributes.clientAmbientLight
                clientSpatialMotion = attributes.clientSpatialMotion
            }
            attributes.other.forEach {
                result[it.key] = if (it.value is String) {
                    val value = it.value as String
                    if (value == "true" || value == "false") {
                        value == "true"
                    } else if (integerRegex.matches(value)) {
                        value.toInt()
                    } else if (decimalRegex.matches(value)) {
                        value.toFloat()
                    } else {
                        value
                    }
                } else {
                    it.value
                }
            }
            return result
        }
    }
}