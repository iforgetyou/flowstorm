package ai.flowstorm.core.model

import kotlinx.serialization.Serializable

@Serializable
data class SttConfig(
    var provider: String = "Google",
    val maxSilence: Int = 5,
    val maxPause: Int = 5,
)