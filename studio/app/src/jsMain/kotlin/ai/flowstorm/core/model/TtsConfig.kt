package ai.flowstorm.core.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class TtsConfig(
    var provider: String = "Google",
    var locale: String = "en_US",
    var gender: String = "Male",
    var name: String = "en-US-Standard-B",
    var engine: String? = null,
    val amazonAlexa: String? = null,
    val googleAssistant: String? = null
) {
    val language get() = locale.substringBefore("_")
}