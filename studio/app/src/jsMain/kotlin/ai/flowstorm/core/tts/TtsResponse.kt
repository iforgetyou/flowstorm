package ai.flowstorm.core.tts

import ai.flowstorm.studio.common.state.AppState
import kotlinx.serialization.Serializable

@Serializable
data class TtsResponse(val path: String, val type: String) {
    val url get() = "${AppState.state.fileUrl}/$path"
}
