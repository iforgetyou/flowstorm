package ai.flowstorm.core.tts

import ai.flowstorm.core.model.TtsConfig
import kotlinx.serialization.Serializable

@Serializable
data class TtsRequest(
    val config: TtsConfig,
    var text: String,
    var isSsml: Boolean = false
)
