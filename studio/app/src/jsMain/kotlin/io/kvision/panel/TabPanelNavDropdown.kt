package io.kvision.panel

import com.github.snabbdom.VNode
import io.kvision.core.AttributeSetBuilder
import io.kvision.core.PosFloat
import io.kvision.dropdown.DropDown
import io.kvision.html.Div
import io.kvision.html.Ul
import io.kvision.html.div
import io.kvision.utils.px

internal class TabPanelNavDropDown(override val tabPanel: TabPanel, classes: Set<String>) : SimplePanel(), TabNav {

    inner class DropDownDiv(private val ariaId: String) : Div(null, false, null, setOf("dropdown-menu")) {

        init {
            width = 350.px
        }

        override fun buildAttributeSet(attributeSetBuilder: AttributeSetBuilder) {
            super.buildAttributeSet(attributeSetBuilder)
            attributeSetBuilder.add("aria-labelledby", ariaId)
        }

        override fun childrenVNodes(): Array<VNode> {
            return tabPanel.tabs.filter { it.visible }.map { it.renderVNode() }.toTypedArray()
        }
    }

    init {
        addCssClass("tabs-panel")

        div(classes = setOf("tabs-panel-holder")) {
            add(
                object : Ul(classes = classes) {
                    init {
                        addCssClass("tabs-panel__tabs")
                    }

                    override fun childrenVNodes(): Array<VNode> {
                        return tabPanel.tabs.filter { it.visible }.map { it.renderVNode() }.toTypedArray()
                    }
                }
            )
        }

        add(
            object : DropDown("") {
                init {
                    addCssClass("tabs-panel__dropdown")
                    this.privateChildren.removeLast()
                    this.privateChildren.add(DropDownDiv(""))
                }
            }
        )
    }
}