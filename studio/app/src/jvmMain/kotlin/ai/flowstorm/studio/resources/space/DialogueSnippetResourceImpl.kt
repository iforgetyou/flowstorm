package ai.flowstorm.studio.resources.space

import com.fasterxml.jackson.annotation.JsonView
import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.model.DialogueSnippet.View.WithGraph
import ai.flowstorm.studio.resources.DialogueSnippetResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Space.Role.Admin)
@RequireRole(Space.Role.Editor)
class DialogueSnippetResourceImpl : DialogueSnippetResource, MongoAbstractEntityRepository<DialogueSnippet>(DialogueSnippet::class) {

    @Inject
    lateinit var context: StudioContext

    @JsonView(WithGraph::class)
    override fun find() = find(context.query)
    override fun find(query: Query): List<DialogueSnippet> = query(query, DialogueSnippet::name, true).apply {
        add(match(context.spaceFilter()))
    }.run()

    @JsonView(WithGraph::class)
    override fun get(id: Id<DialogueSnippet>): DialogueSnippet {
        return collection.find(DialogueSnippet::_id eq id, DialogueSnippet::space_id eq context.spaceId).first()
            ?: throw NotFoundException("Snippet $id does not exist.")
    }

    @JsonView(WithGraph::class)
    override fun create(dialogueSnippet: DialogueSnippet): DialogueSnippet =
        dialogueSnippet.copy(space_id = context.spaceId).let {
            super.create(it).also {
                context.activity(DialogueSnippet::class, it._id, it.name, Activity.Type.Created)
            }
        }

    @JsonView(WithGraph::class)
    override fun update(id: Id<DialogueSnippet>, dialogueSnippet: DialogueSnippet): DialogueSnippet {
        collection.updateOne(and(DialogueSnippet::_id eq id, DialogueSnippet::space_id eq context.spaceId), dialogueSnippet)
        context.activity(DialogueSnippet::class, id, dialogueSnippet.name, Activity.Type.Updated)
        return dialogueSnippet
    }

    override fun delete(id: Id<DialogueSnippet>) {
        val entity = get(id)
        super.remove(id)
        context.activity(DialogueSnippet::class, id, entity.name, Activity.Type.Deleted)
    }

    override fun getGraph(id: Id<DialogueSnippet>): Graph = get(id).graph
}