package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.builder.Builder
import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.DialogueSourceV2
import ai.flowstorm.studio.model.DialogueSourceV2.View.WithGraph
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.model.Space.Role.Editor
import ai.flowstorm.studio.repository.DialogueSourceRepository
import ai.flowstorm.studio.resources.DialogueSourceResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.studio.util.IndentingXMLStreamWriter
import ai.flowstorm.common.ObjectUtil.xmlMapper
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.query.MongoFiltersFactory
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.model.DialogueBuild
import ai.flowstorm.security.Authenticated
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.Xml
import com.fasterxml.jackson.annotation.JsonView
import com.fasterxml.jackson.core.type.TypeReference
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Filters.eq
import kotlinx.datetime.Clock
import org.bson.conversions.Bson
import org.bson.types.ObjectId
import org.eclipse.jetty.io.EofException
import org.glassfish.hk2.api.IterableProvider
import org.glassfish.jersey.server.ChunkedOutput
import org.litote.kmongo.*
import org.litote.kmongo.id.toId
import java.io.*
import java.nio.charset.Charset
import javax.inject.Inject
import javax.ws.rs.BadRequestException
import javax.ws.rs.ForbiddenException
import javax.ws.rs.NotFoundException
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.StreamingOutput
import javax.xml.stream.XMLOutputFactory
import javax.xml.transform.TransformerFactory
import javax.xml.transform.sax.SAXResult
import javax.xml.transform.sax.SAXTransformerFactory
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Admin)
@RequireRole(Editor)
@Authorized
class DialogueSourceResourceImpl : DialogueSourceResource {

    val logger by LoggerDelegate()

    @Inject
    lateinit var context: StudioContext

    @Inject
    lateinit var builders: IterableProvider<Builder>

    @Inject
    lateinit var repository: DialogueSourceRepository

    private val builder: Builder get() = builders.named(DialogueSource.V2).get()
    private val transformerFactory = TransformerFactory.newInstance() as SAXTransformerFactory
    private val xmlOutputFactory = XMLOutputFactory.newInstance()
    private val charset = Charset.forName("UTF-8")
    private val sortingTemplates = transformerFactory.newTemplates(StreamSource("""
        <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslt="http://xml.apache.org/xslt" exclude-result-prefixes="xsl xslt">
          <xsl:output method="xml" indent="yes" xslt:indent-amount="2"/>
          <xsl:template match="@*|node()">
            <xsl:copy>
              <xsl:apply-templates select="@*"/>
              <xsl:apply-templates select="node()">
                <xsl:sort select="name()" />
                <xsl:sort select="." />
              </xsl:apply-templates>
            </xsl:copy>
          </xsl:template>
        </xsl:stylesheet>
    """.trimIndent().byteInputStream(charset)))

    override fun head(): Response {
        val count = repository.countDocuments(findFilters(context.query),context.query.limit)

        return Response.status(Response.Status.OK).header("doc-count", count).header("Access-Control-Expose-Headers", "doc-count").build()
    }

    override fun find() = find(context.query)

    fun find(query: Query): List<DialogueSourceV2> = repository.query(query, DialogueSourceV2::datetime, false).apply {
        add(match(context.spaceFilter(false)))
    }.run()

    private fun findFilters(query: Query): Bson = mutableListOf<Bson>().apply {
        add(context.spaceFilter(false))
        add(eq(DialogueSource.TYPE_PROPERTY, DialogueSource.V2))
        add(MongoFiltersFactory.createFilter(DialogueSourceV2::class, query))
    }.let { Filters.and(it) }

    @JsonView(WithGraph::class)
    override fun get(id: Id<DialogueSource>): DialogueSourceV2 {
        val dialogue = repository.find(id.cast())

        return dialogue?.takeIf { it.space_id == context.spaceId }
            ?: throw NotFoundException("Dialogue $id not found in the space ${context.spaceId}.")
    }

    @JsonView(WithGraph::class)
    override fun getXml(id: Id<DialogueSource>) = get(id)

    private fun getXsl(dialogueSource: DialogueSourceV2): String {

        // create graph groups to be serialized into XSL
        val graph = Graph()

        dialogueSource.graph.nodes.forEach { node ->
            (node.groupId?.let { groupId ->
                val group = dialogueSource.graph.groups.find { it.id == groupId }
                (graph.groupGraphs.find { it.groupId == groupId } ?: Graph(groupId = groupId, groupName = group?.name ?: "unknown").also {
                    graph.groupGraphs.add(it)
                })
            } ?: graph).add(node)
        }

        dialogueSource.graph.links.forEach { link ->
            val fromGroupGraph = graph.groupGraphs.firstOrNull { group ->
                group.nodes.any { node ->
                    node.id == link.from
                }
            }
            val toGroupGraph = graph.groupGraphs.firstOrNull { group ->
                group.nodes.any { node ->
                    node.id == link.to
                }
            }
            if (fromGroupGraph == null && toGroupGraph == null) {
                graph.links.add(link)
            } else if (fromGroupGraph == null) {
                link.type = Graph.Link.Type.Inbound
                toGroupGraph!!.links.add(link)
            } else if (toGroupGraph == null) {
                link.type = Graph.Link.Type.Outbound
                fromGroupGraph!!.links.add(link)
            } else if (toGroupGraph == fromGroupGraph) {
                link.type = Graph.Link.Type.Inner
                fromGroupGraph!!.links.add(link)
            } else
                error("Link from group #${fromGroupGraph.groupId} to group #${toGroupGraph.groupId} not allowed")
        }

        //graph.groupGraphs.add(Graph(groupId = -50, groupName = "data1").apply { intents.add(graph.intents[0]) })
        //graph.groupGraphs.add(Graph(groupId = -51, groupName = "data2").apply { intents.add(graph.intents[1]) })

        val dialogueSource = dialogueSource.copy(graph = graph)

        // generate XSL
        val output = ByteArrayOutputStream()
        val writer = IndentingXMLStreamWriter(xmlOutputFactory.createXMLStreamWriter(OutputStreamWriter(output, charset)))
        writer.writeStartDocument(charset.name(), "1.0")
        writer.writeStartElement("xsl", "stylesheet", Xml.XSL_NAMESPACE_URI)
        writer.writeAttribute("version", "1.0")
        writer.writeNamespace("xsl", Xml.XSL_NAMESPACE_URI)
        writer.writeNamespace("xslt", Xml.XSLT_NAMESPACE_URI)
        writer.writeAttribute("exclude-result-prefixes", "xsl xslt")
        writer.writeStartElement(Xml.XSL_NAMESPACE_URI, "output")
        writer.writeAttribute("method", "xml")
        writer.writeAttribute("indent", "yes")
        writer.writeAttribute(Xml.XSLT_NAMESPACE_URI, "indent-amount", "2")
        writer.writeEndElement()

        // template
        // template matching XML root
        writer.writeStartElement(Xml.XSL_NAMESPACE_URI, "template")
        writer.writeAttribute("match", "/")
        Xml.serialize(writer, dialogueSource)
        writer.writeEndElement()
        writer.writeEndElement()
        writer.writeEndDocument()
        output.flush()

        // replace special syntax with XSL tags
        // @[element ...]  -> <xsl:element ...>
        // @[element .../] -> <xsl:element .../>
        // @[/element]     -> </xsl:element>
        // @{xpath}        -> <xsl:value-of select="xpath"/>
        val xsl = String(output.toByteArray())
                .replace(Regex("\\@\\{(.*)\\}"), "<xsl:value-of select=\"$1\"/>")
                .replace(Regex("\\@\\[\\/(.*)\\]"), "</xsl:$1>")
                .replace(Regex("\\@\\[(.*)\\/\\]"), "<xsl:$1/>")
                .replace(Regex("\\@\\[(.*)\\]"), "<xsl:$1>")
        return xsl
    }

    @JsonView(WithGraph::class)
    override fun getXsl(id: Id<DialogueSource>): Response {
        val dialogueSource = get(id)
        return Response.ok(StreamingOutput { out ->
            val xsl = getXsl(dialogueSource)
            out.write(xsl.toByteArray())
        }, MediaType.TEXT_XML).build()
    }

    @ConfigValue("idMap.defaultMainDialogue")
    lateinit var defaultMainDialogueId: String

    @ConfigValue("idMap.defaultSubDialogue")
    lateinit var defaultSubDialogueId: String

    @JsonView(WithGraph::class)
    override fun getTemplate(name: String): DialogueSourceV2 {
        return when (name) {
            "main" -> repository.get(ObjectId(defaultMainDialogueId).toId())
            "subdialogue" -> repository.get(ObjectId(defaultSubDialogueId).toId())
            else -> error("Unknown template")
        }
    }

    @JsonView(WithGraph::class)
    override fun create(dialogueSource: DialogueSourceV2): DialogueSourceV2 {
        requireSpace(dialogueSource)
        val latest = repository.findLatest(dialogueSource.space_id, dialogueSource.dialogueName)
        val version = (latest?.version ?: 0) + 1
        val new = dialogueSource.copy(_id = newId(), version = version)
        repository.create(new)
        context.activity(DialogueSource::class, new._id, "${new.dialogueName}:${new.version}", Activity.Type.Created)
        return new
    }

    @JsonView(WithGraph::class)
    override fun createXml(dialogueSource: DialogueSourceV2) = create(dialogueSource)

    private fun transformXml(id: Id<DialogueSource>, xmlSourceInput: InputStream, xmlResultOutput: OutputStream) {
        val templateDialogueSource = get(id)
        val xsl = getXsl(templateDialogueSource)
        val xslSource = StreamSource(ByteArrayInputStream(xsl.toByteArray(charset)))
        val xslTemplates = transformerFactory.newTemplates(xslSource)
        val xmlSource = StreamSource(xmlSourceInput)
        val xmlOutputResult = StreamResult(xmlResultOutput)

        val handler1 = transformerFactory.newTransformerHandler(xslTemplates)
        val handler2 = transformerFactory.newTransformerHandler(sortingTemplates)

        handler2.setResult(xmlOutputResult)
        handler1.transformer.transform(xmlSource, SAXResult(handler2))
    }

    override fun getTransformedXml(id: Id<DialogueSource>, xmlSourceInput: InputStream): Response {
        requireSpace(id)
        return Response.ok(StreamingOutput { xmlResultOutput ->
            transformXml(id, xmlSourceInput, xmlResultOutput)
        }, MediaType.TEXT_XML).build()
    }

    override fun transformXml(id: Id<DialogueSource>, xmlSourceInput: InputStream): DialogueSourceV2 {
        val xmlResultOutput = ByteArrayOutputStream()
        transformXml(id, xmlSourceInput, xmlResultOutput)

        val xmlResultInput = ByteArrayInputStream(xmlResultOutput.toByteArray())
        val newDialogueSource = xmlMapper.readValue(xmlResultInput, object : TypeReference<DialogueSourceV2>() {})

        return create(newDialogueSource)
    }

    @JsonView(WithGraph::class)
    override fun update(id: Id<DialogueSource>, dialogueSource: DialogueSourceV2): DialogueSourceV2 {
        val savedDialogueSource = get(id)
        requireLock(savedDialogueSource)

        dialogueSource.lastBuild = null
        dialogueSource.datetime = Clock.System.now()
        repository.updateById(id.cast(), dialogueSource)
        context.activity(DialogueSource::class, id, "${dialogueSource.dialogueName}:${dialogueSource.version}", Activity.Type.Updated)
        return dialogueSource
    }

    @JsonView(WithGraph::class)
    override fun updateXml(id: Id<DialogueSource>, dialogueSource: DialogueSourceV2): DialogueSourceV2 = update(id, dialogueSource)

    override fun build(id: Id<DialogueSource>): DialogueBuild {
        val dialogueSource = get(id)
        val build = builder.build(dialogueSource)

        dialogueSource.lastBuild = build._id
        repository.updateById(id.cast(), dialogueSource)
        context.activity(DialogueSource::class, id, "${dialogueSource.dialogueName}:${dialogueSource.version}", Activity.Type.Built)
        return build
    }

    override fun bulkBuild(verbose: Boolean, dryrun:Boolean): ChunkedOutput<String> {
        val output = ChunkedOutput<String>(String::class.java)
        object : Thread() {
            override fun run() {
                val fails = mutableListOf<DialogueSourceRepository.DialogueInfo>()
                try {
                    val dialogues = repository.getInfo(context.spaceId, context.query)

                    output.write("There are ${dialogues.size} dialogues to build.\n")

                    dialogues.forEachIndexed { i, it ->
                        if (dryrun) {
                            output.write("[${i + 1}/${dialogues.size}] ${it.dialogueName}#${it.version}\n")
                            return@forEachIndexed
                        }

                        output.write("<< BUILD START [${i + 1}/${dialogues.size}] ${it.dialogueName}#${it.version}\n")
                        val result = try {
                            build(it._id)
                        } catch (e: Throwable) {
                            DialogueBuild(
                                    "n/a",
                                    false,
                                    listOf(),
                                    "Failed with exception ${e::class.simpleName}:${e.message}"
                            )
                        }

                        if (verbose or !result.success) result.logs.forEach { output.write("   $it\n") }
                        output.write("   ${if (result.success) "SUCCESS" else "FAILED"} [id=${result._id}]\n")
                        if (result.error.isNotEmpty()) output.write("   ${result.error}\n")
                        output.write("<< BUILD FINISHED ${it.dialogueName}\n")
                        if (!result.success) {
                            fails.add(it)
                        }
                    }
                    output.write("RESULTS:\n")
                    output.write("success: ${dialogues.size - fails.size} fails: ${fails.size}\n")
                    if (fails.isNotEmpty()) {
                        output.write("failed builds:\n")
                        fails.forEach { output.write("   ${it.dialogueName}[${it._id}]\n") }
                    }
                } catch (t: EofException) {
                    logger.warn("Bulk build aborted: ${t.message}")
                    //Client disconnected, request aborted
                } finally {
                    output.close()
                }
            }
        }.start()

        return output
    }

    override fun lock(id: Id<DialogueSource>) {
        val username = context.getUser().username

        val dialogueSource = get(id)
        requireLock(dialogueSource)

        repository.updateById(id.cast(), dialogueSource.copy(lock = username, lockTime = Clock.System.now()))
    }

    override fun unlock(id: Id<DialogueSource>, force: Boolean) {
        val dialogueSource = get(id)
        val username = context.getUser().username
        if (!force && dialogueSource.lock != username && dialogueSource.lock != null) {
            throw ForbiddenException("Force unlock is required when dialogues is locked by someone else.")
        }

        repository.updateById(id.cast(), dialogueSource.copy(lock= null, lockTime = null))
    }

    override fun publish(id: Id<DialogueSource>) {
        val dialogueSource = get(id)
        requireLock(dialogueSource)

        val subdialogues = repository.findReferenced(dialogueSource)

        if (subdialogues.any { (it as DialogueSourceV2).state != DialogueSourceV2.State.Published })
            BadRequestException("Dialogue is referencing unpublished subdialogue. Publish all dialogues first.")

        dialogueSource.state = DialogueSourceV2.State.Published
        clearLock(dialogueSource)
        repository.updateById(id.cast(), dialogueSource)
    }

    override fun archive(id: Id<DialogueSource>) {
        val dialogueSource = get(id)
        requireLock(dialogueSource)

        val usages = repository.findUsages(id.cast())

        if (usages.isNotEmpty())
            BadRequestException("Can not archive dialogue, it is referenced in ${usages.size} dialogues [" + usages.joinToString(",") { it.dialogueName } + "]")

        //TODO check whether dialogue is not used in any app
        dialogueSource.state = DialogueSourceV2.State.Archived
        clearLock(dialogueSource)

        repository.updateById(id.cast(), dialogueSource)
    }

    override fun updateGraph(id: Id<DialogueSource>, graph: Graph): Graph = update(id, get(id).copy(graph = graph)).graph

    override fun getGraph(id: Id<DialogueSource>): Graph = get(id).graph

    private fun requireLock(dialogueSource: DialogueSourceV2) {
        val username = context.getUser().username
        if (dialogueSource.lock != null && dialogueSource.lock != username)
            throw ForbiddenException("Dialogue is locked by user ${dialogueSource.lock}")
    }

    private fun clearLock(dialogueSource: DialogueSourceV2) {
        dialogueSource.lock = null
        dialogueSource.lockTime = null
    }

    private fun requireSpace(id:Id<DialogueSource>) = get(id)
    private fun requireSpace(dialogueSource: DialogueSource) {
        if (context.spaceId != dialogueSource.space_id) throw BadRequestException("Dialogue does not belong to specified space.")
    }
}