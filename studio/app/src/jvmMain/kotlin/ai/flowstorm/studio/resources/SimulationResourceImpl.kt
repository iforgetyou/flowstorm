package ai.flowstorm.studio.resources

import com.mongodb.client.MongoDatabase
import ai.flowstorm.studio.model.Simulation
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.security.RequireRootRole
import org.litote.kmongo.Id
import org.litote.kmongo.findOneById
import org.litote.kmongo.getCollection
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/simulations")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@Authorized
@RequireRootRole(Admin)
class SimulationResourceImpl : SimulationResource {

    @Inject
    lateinit var database: MongoDatabase

    private val simulations by lazy { database.getCollection<Simulation>() }

    override fun getSimulations(): List<Simulation> {
        return simulations.find().toList()
    }

    override fun createApplication(simulation: Simulation): Simulation {
        simulations.insertOne(simulation)
        return simulation
    }

    override fun getSimulation(simulationId: Id<Simulation>): Simulation {
        return simulations.findOneById(simulationId)
                ?:throw NotFoundException("Simulation $simulationId does not exist.")
    }
}