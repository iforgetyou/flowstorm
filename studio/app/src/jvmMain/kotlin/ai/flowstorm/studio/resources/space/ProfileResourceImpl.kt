package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.resources.ProfileResource
import ai.flowstorm.common.client.resourceMethod
import org.litote.kmongo.Id
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.core.model.Profile
import ai.flowstorm.core.model.Space as CoreSpace
import javax.inject.Inject
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import ai.flowstorm.core.resources.ProfileResource as CoreProfileResource

@Produces(MediaType.APPLICATION_JSON)
class ProfileResourceImpl : ProfileResource {

    @Inject
    lateinit var query: Query

    @Suppress("UnresolvedRestParam")
    @PathParam("spaceId")
    lateinit var spaceId: Id<ai.flowstorm.core.model.Space>

    @Inject
    lateinit var coreWebTarget: WebTarget

    @Inject
    lateinit var profileResource: CoreProfileResource

    override fun find(): List<Profile> = coreWebTarget.path("/profiles").resourceMethod(CoreProfileResource::find)
        .query(createQueryWithSpaceId(spaceId)).request().get(object : GenericType<List<Profile>>() {})


    override fun get(profileId: Id<Profile>): Profile {
        return profileResource.get(profileId)
    }

    override fun create(profile: Profile) {
        profileResource.create(profile)
    }

    override fun update(profileId: Id<Profile>, profile: Profile) {
        profileResource.update(profileId, profile)
    }

    private fun createQueryWithSpaceId(spaceId: Id<CoreSpace>) =
        query.copy(
            filters = query.filters + Query.Filter(
                Profile::space_id.name,
                Query.Operator.eq,
                spaceId.toString()
            )
        )
}