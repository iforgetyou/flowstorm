package ai.flowstorm.studio.resources.space

import ai.flowstorm.common.client.resourceMethod
import org.litote.kmongo.Id
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.core.model.Report
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.resources.ReportResource
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.core.type.PropertyMap
import javax.inject.Inject
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
class ReportResourceImpl : ReportResource {

    @Inject
    lateinit var query: Query

    @Suppress("UnresolvedRestParam")
    @PathParam("spaceId")
    lateinit var spaceId: Id<ai.flowstorm.core.model.Space>

    @Inject
    lateinit var coreWebTarget: WebTarget

    override fun getData(granularity: Report.Granularity, aggregations: List<String>): Report {
        val q = createQueryWithSpaceId(spaceId)
        return coreWebTarget.path("/reports").resourceMethod(ReportResource::getData)
                .queryParam("granularity", granularity)
                .queryParam("aggregations", *aggregations.toTypedArray())
                .query(q).request().get(Report::class.java)
    }

    override fun getMetrics(): List<PropertyMap> {
        val q = createQueryWithSpaceId(spaceId)
        return coreWebTarget.path("/reports").resourceMethod(ReportResource::getMetrics)
                .query(q).request().get(object : GenericType<List<PropertyMap>>() {})
    }

    override fun getNamespaces(): List<PropertyMap> {
        val q = createQueryWithSpaceId(spaceId)
        return coreWebTarget.path("/reports").resourceMethod(ReportResource::getNamespaces)
                .query(q).request().get(object : GenericType<List<PropertyMap>>() {})
    }

    private fun createQueryWithSpaceId(spaceId: Id<CoreSpace>) =
            query.copy(filters = query.filters + Query.Filter(Session::space_id.name, Query.Operator.eq, spaceId.toString()))
}
