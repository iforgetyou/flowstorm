package ai.flowstorm.studio.resources

import com.mongodb.client.MongoDatabase
import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.repository.ApplicationRepository
import ai.flowstorm.studio.repository.UserRepository
import ai.flowstorm.studio.security.BaseIdentity
import ai.flowstorm.studio.services.UserService
import io.netty.handler.codec.http.QueryStringDecoder
import org.bson.types.ObjectId
import org.litote.kmongo.*
import org.litote.kmongo.id.toId
import ai.flowstorm.core.resources.ContentDistributionResource
import ai.flowstorm.core.resources.ContentDistributionResource.ContentRequest
import ai.flowstorm.core.resources.ContentDistributionResource.ContentResponse
import ai.flowstorm.core.type.MutablePropertyMap
import ai.flowstorm.util.DataConverter
import ai.flowstorm.util.Locale
import ai.flowstorm.util.LoggerDelegate
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.toList
import ai.flowstorm.core.model.Application as CoreApplication
import ai.flowstorm.core.model.Space as CoreSpace

@Path("/contentDistribution")
@Produces(MediaType.APPLICATION_JSON)
class ContentDistributionResourceImpl : ContentDistributionResource {

    class DeviceNotAssignedException(message: String?) : NotFoundException(message)
    class UserNotFoundException(message: String?) : NotFoundException(message)
    class NoApplicationException(message: String?) : NotFoundException(message)

    @Inject
    lateinit var spaceResource: SpaceResource

    @Inject
    lateinit var applicationRepository: ApplicationRepository

    @Inject
    lateinit var deviceResource: DeviceResource

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var userService: UserService

    private val logger by LoggerDelegate()

    @Inject
    lateinit var database: MongoDatabase

    private val dialogues by lazy { database.getCollection<DialogueSourceV2>("dialogue") }
    private val consents by lazy { database.getCollection<TermsConsent>() }
    private val spaces by lazy { database.getCollection<Space>() }

    override fun resolve(contentRequest: ContentRequest): ContentResponse {
        var application = selectApplication(contentRequest.appKey)
        val (user, device) = identifyClient(contentRequest, application.anonymousAccessAllowed)

        val sessionProperties: MutablePropertyMap = mutableMapOf()

        val spaceId: Id<CoreSpace>?
        val dialogue: DialogueSourceV2? = dialogues.findOne(DialogueSource::_id eq application.dialogue_id as Id<DialogueSourceV2>)
        if (application is Application) {
            spaceId = application.space_id
            sessionProperties["deviceId"] = contentRequest.deviceId
        } else {
            spaceId = dialogue?.space_id
        }

        val space = spaces.findOneById(spaceId!!)!!

        val hasConsent = if (application is Application && application.terms_id != null)
            consents.find(and(TermsConsent::user_id eq user._id, TermsConsent::terms_id eq application.terms_id)).toList().isNotEmpty()
        else
            false

        logger.info("Resolved content (spaceId=$spaceId, applicationId=${application._id}, name=${application.name}, deviceId=${device.deviceId}, username=${user.username})")
        return ContentResponse(application, Locale(dialogue?.language), space, device, user, application.name.startsWith("@"), hasConsent, sessionProperties)
    }

    private fun identifyClient(contentRequest: ContentRequest, anonymousAccessAllowed: Boolean): Pair<User, Device> = with (contentRequest) {

        val device =
            try {
                deviceResource.get(deviceId)
            } catch (e: NotFoundException) {
                if (anonymousAccessAllowed || token != null)
                    null // anonymous access does not require existing device; if we know the user, we can pair him to a new device
                else
                    throw e
            }

        val identity = when {
            token != null -> {
                if (token!!.startsWith("alexa:"))
                    try {
                        userService.getIdentityFromAlexa(token!!.substring(6))
                    } catch (e: Exception) {
                        e.printStackTrace()
                        null
                    }
                else
                    userService.getIdentityFromJwtToken(token!!)
            }
            else -> null
        }

        var user = when {
            identity?.email != null -> {
                // try to get user by identity
                userRepository.findByUsername(identity.email!!)
            }
            identity?.deviceId != null -> {
                userRepository.findByUsername(identity.deviceId!!)
            }
            device != null -> {
                // get user by device
                device.user_id?.let {
                    userRepository.get(it as Id<User>)
                } ?: throw DeviceNotAssignedException("Device ${device.deviceId} is not assigned to a user")
            }
            else -> null
        }

        if (user == null) {
            if (anonymousAccessAllowed) {
                // try to get anonymous user by sender value
                user = userRepository.findByUsername(deviceId)?.also {
                    if (!it.isAnonymous)
                        error("User identified by device ID is not anonymous")

                    if (identity != null) {
                        userService.updateUserFromIdentity(it, identity)
                        logger.info("Updating user $this")
                        userRepository.updateById(it._id as Id<User>, it)
                    }
                }

                if (user == null) {
                    // create user with identity ?: anonymous properties
                    user = userService.createUserFromIdentity(BaseIdentity(deviceId = deviceId)).apply {
                        logger.info("Creating user $this")
                        userRepository.create(this)
                    }
                }
            } else {
                throw UserNotFoundException("User not identified")
            }
        }

        return Pair(user, device ?: Device(deviceId = deviceId, user_id = user._id, space_id = null))
    }

    private fun selectApplication(key: String): CoreApplication {
        (if (key.startsWith('@'))
            "dialogue:" + key.substring(1)
        else key).let { key ->
            val colon = key.indexOf(':') >= 0
            val type = if (colon) key.substringBefore(':') else ""
            var id = if (colon) key.substringAfter(':') else key
            return when (type) {
                "dialogue" -> {
                    val decoder = QueryStringDecoder(id)
                    id = id.substringBefore('?')
                    val app = CoreApplication(
                            name = "@${id}",
                            dialogue_id = ObjectId(id).toId()
                    )
                    decoder.parameters().forEach {
                        val prop = it.key.split(':')
                        app.properties[prop[0]] = DataConverter.valueFromString(prop[0], prop.getOrElse(1) { "String" }, it.value[0])
                    }
                    app
                }
                "flowstorm", "promethist", "application", "" -> {
                    applicationRepository.get(ObjectId(id).toId())
                }
                "alexa" -> {
                    applicationRepository.getByAlexaId(id)
                }
                else -> {
                    error("invalid key format")// (<supported-type>):<id>(?par1=val1&par2=val2..)
                }
            }
        }
    }

    private fun selectApplication(user: User, device: Device?, language: String?): Application {
        val apps = if (device != null) {
            // get only applications from user assignment in space
            spaceResource.getApplicationsForUserInSpace(spaceId = device.space_id!!, userId = user._id)
        } else {
            // get all user's applications from all assignments
            userRepository.findApplicationsForUser(user._id as Id<User>)
        }
        return if (apps.isEmpty())
            throw NoApplicationException("No assigned application for the user")
        else
            apps.random()
    }
}
