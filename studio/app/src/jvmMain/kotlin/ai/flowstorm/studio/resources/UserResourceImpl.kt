package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.model.UserSpaceRole
import ai.flowstorm.studio.repository.UserRepository
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import org.litote.kmongo.*
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.toList

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@Authorized
@RequireRootRole(Admin)
class UserResourceImpl : UserResource {

    @Inject
    lateinit var context: StudioContext

    @Inject
    lateinit var userRepository: UserRepository

    override fun find(): List<User> = userRepository.find(context.query)

    override fun create(user: User): User = userRepository.create(user)

    override fun update(id: Id<User>, user: User): User = userRepository.updateById(id, user)

    override fun get(id: Id<User>): User = userRepository.get(id)

    override fun delete(id: Id<User>) = userRepository.remove(id)

    override fun getRolesForUser(id: Id<User>): List<UserSpaceRole> = userRepository.findRolesForUser(id)

    override fun getSpaces(id: Id<User>): List<Space> = (userRepository.findSpacesForUser(id) union listOf(context.rootSpace)).toList()

    override fun getApplications(id: Id<User>): List<Application> = userRepository.findApplicationsForUser(id)
}
