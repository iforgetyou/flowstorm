package ai.flowstorm.studio.filters

import com.auth0.jwt.exceptions.JWTVerificationException
import org.glassfish.jersey.server.wadl.processor.WadlModelProcessor
import ai.flowstorm.security.Authenticated
import ai.flowstorm.security.JwtToken
import ai.flowstorm.security.jwt.JwtVerifier
import java.security.Principal
import javax.annotation.Priority
import javax.inject.Inject
import javax.ws.rs.NotAuthorizedException
import javax.ws.rs.Priorities
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.container.ResourceInfo
import javax.ws.rs.core.Context
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.ext.Provider

@Provider
@Priority(Priorities.AUTHENTICATION)
class AuthenticationFilter : ContainerRequestFilter {

    @Context
    lateinit var resourceInfo: ResourceInfo

    @Inject
    lateinit var jwtVerifier: JwtVerifier

    companion object {
        const val SCHEME = "Bearer"
    }

    override fun filter(requestContext: ContainerRequestContext) {
        if (resourceInfo.resourceClass == WadlModelProcessor.OptionsHandler::class.java) return //ignore requests handled by implicit OPTIONS handler
        val annotation = getAnnotation(resourceInfo) ?: return

        val authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)

        if (authorizationHeader == null && !annotation.required) {
            return
        }

        if (authorizationHeader == null || !authorizationHeader.lowercase().startsWith(SCHEME.lowercase() + " "))
            throw NotAuthorizedException ("Missing authentication token", SCHEME)

        val jwt = JwtToken.create(authorizationHeader.substring(SCHEME.length).trim())

        try {
            jwtVerifier.verify(jwt.decodedJWT)
        } catch (e: JWTVerificationException) {
            throw NotAuthorizedException(e.message, e, SCHEME)
        }

        val currentContext = requestContext.securityContext

        requestContext.securityContext = object : SecurityContext {
            override fun isUserInRole(role: String?) = true
            override fun getAuthenticationScheme() = SCHEME
            override fun getUserPrincipal() = Principal { jwt.username }
            override fun isSecure() = currentContext.isSecure
        }
    }

    private fun getAnnotation(resourceInfo: ResourceInfo): Authenticated? = with(resourceInfo) {

        resourceMethod.getAnnotation(Authenticated::class.java)
            ?: resourceClass.getAnnotation(Authenticated::class.java)
    }
}