package ai.flowstorm.studio.services

import org.bson.types.ObjectId
import org.glassfish.hk2.api.Factory
import org.litote.kmongo.id.toId
import ai.flowstorm.core.model.Space
import ai.flowstorm.core.resources.*
import javax.inject.Inject
import javax.ws.rs.PathParam
import javax.ws.rs.client.WebTarget

abstract class CoreFactory <T> : Factory<T> {

    protected abstract val instance: T

    @Inject
    lateinit var coreService: CoreService

    @Suppress("UnresolvedRestParam")
    @PathParam("spaceId")
    var spaceId: String? = null

    fun spaceId() = spaceId?.let { ObjectId(it).toId<Space>() }

    override fun provide(): T = instance

    override fun dispose(obj: T) { }

    class FileStorageResourceFactory : CoreFactory<FileStorageResource>() {
        override val instance by lazy { coreService.resource<FileStorageResource>(spaceId(), "/file") }
    }

    class ProfileResourceFactory : CoreFactory<ProfileResource>() {
        override val instance by lazy { coreService.resource<ProfileResource>(spaceId(), "/profiles") }
    }

    class ReportResourceFactory : CoreFactory<ReportResource>() {
        override val instance by lazy { coreService.resource<ReportResource>(spaceId(), "/reports") }
    }

    class DevicePairingResourceFactory : CoreFactory<DevicePairingResource>() {
        override val instance by lazy { coreService.resource<DevicePairingResource>(spaceId(), "/devicePairing") }
    }

    class CommunityResourceFactory : CoreFactory<CommunityResource>() {
        override val instance by lazy { coreService.resource<CommunityResource>(spaceId(), "/communities") }
    }

    class TtsResourceFactory : CoreFactory<TtsResource>() {
        override val instance by lazy { coreService.resource<TtsResource>(spaceId(), "/tts") }
    }

    class WebTargetFactory : CoreFactory<WebTarget>() {
        override val instance by lazy { coreService.webTarget(spaceId()) }
    }
}
