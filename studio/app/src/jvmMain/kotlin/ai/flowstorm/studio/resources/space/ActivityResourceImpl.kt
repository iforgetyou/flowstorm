package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.model.Space.Role
import ai.flowstorm.studio.resources.ActivityResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.Space
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Role.Admin)
@RequireRole(Role.Consumer)
class ActivityResourceImpl : ActivityResource, MongoAbstractEntityRepository<Activity>(Activity::class) {

    @Inject
    lateinit var query: Query

    @Inject
    lateinit var context: StudioContext


    @Suppress("UnresolvedRestParam")
    @PathParam("spaceId")
    lateinit var spaceId: Id<Space>

    override val collection by lazy { database.getCollection<Activity>() }

    override fun find() = find(query)

    override fun find(query: Query): List<Activity> = query(query, Activity::datetime, false).apply {
        add(match(context.spaceFilter(includeRoot = false)))
    }.run()
}