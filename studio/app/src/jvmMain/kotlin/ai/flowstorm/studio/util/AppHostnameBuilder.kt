package ai.flowstorm.studio.util

import ai.flowstorm.common.config.ConfigValue

class AppHostnameBuilder(
        @ConfigValue("namespace") val namespace: String,
        @ConfigValue("domain", "flowstorm.ai") val domain: String
) {
    fun frontend(): String = "app" + when {
        namespace == "default" -> ""
        namespace.startsWith("flowstorm") -> namespace.substringAfter("flowstorm")
        else -> "-$namespace"
    } + ".$domain"
}
