package ai.flowstorm.studio.resources

import com.mongodb.client.MongoDatabase
import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.model.Space
import org.litote.kmongo.eq
import org.litote.kmongo.getCollection
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/client")
@Produces(MediaType.APPLICATION_JSON)
class ClientResourceImpl : ClientResource {

    companion object {
        const val EMPTY_CONFIG = "{}"
    }

    @Inject
    lateinit var database: MongoDatabase

    val devices by lazy { database.getCollection<Device>() }
    val spaces by lazy { database.getCollection<Space>() }

    override fun deviceConfiguration(deviceId: String) = devices.find(Device::deviceId eq deviceId).singleOrNull()
        ?.let { device ->
            when {
                device.configuration != null && device.configuration != EMPTY_CONFIG ->
                    device.configuration
                device.space_id != null ->
                    spaces.find(Space::_id eq device.space_id).firstOrNull()?.deviceConfiguration ?: EMPTY_CONFIG
                    else ->
                        EMPTY_CONFIG
                }
            } ?: throw NotFoundException("Device does not exist")

}