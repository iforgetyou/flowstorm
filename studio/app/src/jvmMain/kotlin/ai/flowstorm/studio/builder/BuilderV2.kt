package ai.flowstorm.studio.builder

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.parser.ParenthesisMismatchException
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.services.CoreService
import org.litote.kmongo.*
import org.litote.kmongo.id.WrappedObjectId
import ai.flowstorm.core.builder.Request
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.dialogue.AbstractDialogue.Companion.GENERATED_USER_INPUT_ID
import ai.flowstorm.core.model.DialogueSourceCode
import ai.flowstorm.core.model.DialogueBuild
import ai.flowstorm.util.LoggerDelegate
import ai.flowstorm.util.TextExpander.expand
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import kotlin.math.absoluteValue

/**
 * Builder for V2 source model - builds to V2 runtime model
 */
class BuilderV2(private val coreService: CoreService) : Builder {

    private val logger by LoggerDelegate()

    lateinit var contextClass: String

    lateinit var dialogueSources: MongoCollection<DialogueSource>

    lateinit var dialogueMixins: MongoCollection<DialogueMixin>

    lateinit var speechRecognizers: MongoCollection<SpeechRecognizer>

    lateinit var voices: MongoCollection<Voice>

    override fun build(dialogueSource: DialogueSource): DialogueBuild {
        require(dialogueSource is DialogueSourceV2)

        val time = System.currentTimeMillis()
        logger.info("Starting build of dialogue ${dialogueSource.dialogueName}")

        val sourceCodeBuilder = DialogueSourceCodeBuilder(
            dialogueSource.dialogueId,
            dialogueSource.dialogueName,
            dialogueSource.version,
            dialogueSource.language,
            if (dialogueSource.contextClass == null || dialogueSource.contextClass!!.isBlank())
                this@BuilderV2.contextClass
            else
                dialogueSource.contextClass!!
        ).apply {
            parameters = dialogueSource.parameters ?: ""
            initCode = StringBuilder().apply {
                dialogueSource.mixins_id.forEach { id ->
                    dialogueMixins.findOneById(id)?.let {
                        appendLine(it.text)
                    }
                }
                appendLine(dialogueSource.initCode)
            }
            properties = mutableMapOf<String, Any>(
                AbstractDialogue<*>::background.name to (dialogueSource.background ?: "")
            ).also {
                if (dialogueSource.speechRecognizer_id != null) {
                    speechRecognizers.findOne(SpeechRecognizer::_id eq dialogueSource.speechRecognizer_id)?.let { speechRecognizer ->
                        it[AbstractDialogue<*>::sttConfig.name] = speechRecognizer.sttConfig
                    }
                }
                if (dialogueSource.voice_id != null) {
                    voices.findOne(Voice::_id eq dialogueSource.voice_id)?.let { voice ->
                        it[AbstractDialogue<*>::ttsConfig.name] = voice.ttsConfig
                    }
                }
            }
            //TODO set source.extensionCode
        }
        addNodesToSource(sourceCodeBuilder, dialogueSource)
        val sourceCode = sourceCodeBuilder.build()
        val request = Request(sourceCode)
        val resource = coreService.builderResource(dialogueSource.space_id)
        val response = resource.build(request)
        val duration = System.currentTimeMillis() - time
        logger.info("Finished build of dialogue ${dialogueSource.dialogueName} in $duration ms")
        return response.build
    }

    private fun getFileNodeSource(node: Graph.FileNode, dialogueSource: DialogueSourceV2) =
        node.asset_id?.let {
            coreService.fileUrl(dialogueSource.space_id) + "/assets/spaces/$it"
        } ?: node.source

    private fun expandText(text: String, nodeId: Int): List<String> = try {
            expand(text)
        } catch (pme: ParenthesisMismatchException) {
            throw IllegalStateException("${pme.message} in node $nodeId", pme)
        } catch (e: Throwable) {
            throw Exception("Text expansion exception at node #$nodeId: \"$text\"", e)
        }

    private fun addNodesToSource(source: DialogueSourceCodeBuilder, dialogueSource: DialogueSourceV2) {

        var generatedUserInputId = GENERATED_USER_INPUT_ID

        dialogueSource.graph.nodes.forEach { node ->
            when (node) {
                is Graph.GlobalIntent -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(mapName(node) to mapName(nextNode))

                    val utterances = mutableListOf<String>().apply {
                        node.mixins_id.forEach { id ->
                            dialogueMixins.findOneById(id)?.let { addAll(it.text.split('\n')) }
                        }
                        addAll(node.utterances)
                    }.map { expandText(it, node.id) }.flatten()
                    val negativeUtterances = node.negativeUtterances.map { expandText(it, node.id) }.flatten()
                    val entities = node.entities.split(",").filter { it.isNotBlank() }
                    source.addNode(DialogueSourceCode.GlobalIntent(node.id, mapName(node), node.threshold, utterances, negativeUtterances, entities))
                }
                is Graph.Intent -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(mapName(node) to mapName(nextNode))
                    val utterances = mutableListOf<String>().apply {
                        node.mixins_id.forEach { id ->
                            dialogueMixins.findOneById(id)?.let { addAll(it.text.split('\n')) }
                        }
                        addAll(node.utterances)
                    }.map { expandText(it, node.id) }.flatten()
                    val negativeUtterances = node.negativeUtterances.map { expandText(it, node.id) }.flatten()
                    val entities = node.entities.split(",").filter { it.isNotBlank() }
                    source.addNode(DialogueSourceCode.Intent(node.id, mapName(node), node.threshold, utterances, negativeUtterances, entities))
                }
                is Graph.Function -> {
                    val transitions = getTransitionsForNode(dialogueSource, node)
                    val code = StringBuilder()
                    node.mixins_id.forEach { id ->
                        dialogueMixins.findOneById(id)?.let { code.append(it.text) }
                    }
                    code.append(node.code)
                    source.addNode(DialogueSourceCode.Function(node.id, mapName(node), transitions, code))
                }
                is Graph.SubDialogue -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(mapName(node) to mapName(nextNode))

                    source.addNode(DialogueSourceCode.SubDialogue(node.id, mapName(node), node.sourceId.toString(), node.code, node.rcode))
                }
                is Graph.UserInput -> {
                    val transitions = getTransitionsForNode(dialogueSource, node)
                    val intents = nextNodes(dialogueSource, node).filterIsInstance<Graph.Intent>().map { mapName(it) }
                    val actions = nextNodes(dialogueSource, node).filterIsInstance<Graph.Action>().map { mapName(it) }
                    val code = StringBuilder()
                    node.mixins_id.forEach { id ->
                        dialogueMixins.findOneById(id)?.let { code.append(it.text) }
                    }
                    code.append(node.code)
                    source.addNode(DialogueSourceCode.UserInput(node.id, mapName(node), intents, actions, node.speechRecognizer_id?.let {
                        speechRecognizers.findOne(SpeechRecognizer::_id eq it)?.let { speechRecognizer ->
                            speechRecognizer.sttConfig
                        }
                    }, node.skipGlobalIntents, transitions, node.expectedPhrases, code))
                }
                is Graph.ReInput -> {
                    val transitions = getTransitionsForNode(dialogueSource, node)
                    val intents = nextNodes(dialogueSource, node).filterIsInstance<Graph.Intent>().map { mapName(it) }
                    val actions = nextNodes(dialogueSource, node).filterIsInstance<Graph.Action>().map { mapName(it) }
                    val code = StringBuilder()
                    code.append(node.code)
                    source.addNode(DialogueSourceCode.ReInput(node.id, mapName(node), intents, actions, node.skipGlobalIntents, transitions, code))
                }
                is Graph.GoBack -> {
                    source.addNode(DialogueSourceCode.GoBack(node.id, mapName(node), node.repeat))
                }
                is Graph.Enter -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(mapName(node) to mapName(nextNode))
                }
                is Graph.End, is Graph.Exit -> {
                    // nothing to do, we use predefined nodes
                }
                is Graph.Sleep -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(node.name to mapName(nextNode))
                    source.addNode(DialogueSourceCode.Sleep(node.id, mapName(node), node.timeout))
                }
                is Graph.Speech -> {
                    val nextNodes = nextNodes(dialogueSource, node)
                    val nextNodeName: String
                    if (nextNodes.size == 1 && nextNodes.first() !is Graph.Intent) {
                        nextNodeName = mapName(nextNodes.first())
                    } else {
                        nextNodeName = "generatedUserInput_${++generatedUserInputId}"
                        source.addNode(
                            DialogueSourceCode.UserInput(
                                generatedUserInputId,
                                nextNodeName, nextNodes.map { it.name },
                                listOf(), null,false, mapOf(), "",""))
                    }

                    val responses = mutableListOf<String>().apply {
                        node.mixins_id.forEach { id ->
                            dialogueMixins.findOneById(id)?.let { addAll(it.text.split('\n')) }
                        }
                        addAll(node.responses)
                    }
                    responses.forEach { expandText(it, node.id) }
                    if (responses.isEmpty())
                        responses.add("")
                    source.addTransition(node.name to nextNodeName)
                    source.addNode(DialogueSourceCode.Speech(node.id, node.name, node.background, node.voice_id?.let {
                        voices.findOne(Voice::_id eq it)?.let { voice ->
                            voice.ttsConfig
                        }
                    }, node.repeatable, responses, node.code))
                }
                is Graph.Image -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(node.name to mapName(nextNode))
                    source.addNode(DialogueSourceCode.Image(node.id, node.name, getFileNodeSource(node, dialogueSource)))
                }
                is Graph.Sound -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(node.name to mapName(nextNode))
                    source.addNode(DialogueSourceCode.Sound(node.id, node.name, getFileNodeSource(node, dialogueSource), node.repeatable))
                }
                is Graph.Command -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    val code = (node.mixin_id?.let { id ->
                        dialogueMixins.findOneById(id)?.let { it.text }
                    } ?: "") + node.code
                    source.addTransition(node.name to mapName(nextNode))
                    source.addNode(DialogueSourceCode.Command(node.id, node.name, node.command, code))
                }
                is Graph.Action -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(node.name to mapName(nextNode))
                    source.addNode(DialogueSourceCode.Action(node.id, node.name, node.action))
                }
                is Graph.GlobalAction -> {
                    val nextNode = nextNodes(dialogueSource, node).first()
                    source.addTransition(node.name to mapName(nextNode))
                    source.addNode(DialogueSourceCode.GlobalAction(node.id, node.name, node.action))
                }
                is Graph.Group -> {
                    // do nothing
                }
                else -> {
                    logger.warn("Unsupported node type: ${node::class.simpleName}")
                }
            }
        }
    }

    private fun mapName(node: Graph.Node): String = when (node) {
        is Graph.Enter -> AbstractDialogue<*>::enter.name
        is Graph.Exit -> AbstractDialogue<*>::exit.name
        is Graph.End -> AbstractDialogue<*>::end.name
        is Graph.GoBack -> "goBack_${node.id.absoluteValue}"
        else -> node.name
    }

    private fun getTransitionsForNode(dialogueSource: DialogueSourceV2, node: Graph.Node): Map<String, String> {
        return dialogueSource.graph.links.filter { it.from == node.id }
                .map { link ->
                    link.name to mapName(dialogueSource.graph.nodes.first { it.id == link.to })
                }.toMap()
    }

    private fun nextNodes(sourceDialogue: DialogueSourceV2, node: Graph.Node): List<Graph.Node> {
        val nextNodes = sourceDialogue.graph.links.filter { it.from == node.id }.map { link ->
            sourceDialogue.graph.nodes.find { it.id == link.to } ?: error("node not found")
        }

        if (nextNodes.isEmpty()) error("Node ${node.id} must have at least one following node.")

        return nextNodes
    }

    private fun getDialogueName(id: Id<DialogueSource>): String {
        val dialogue = dialogueSources.findOneById(id)?: throw NotFoundException("Dialogue with id=$id not found.")
        return when (dialogue) {
            is DialogueSourceV2 -> dialogue.dialogueName
            else -> error("unsupported")
        }
    }

    class Factory : org.glassfish.hk2.api.Factory<BuilderV2> {


        @ConfigValue("core.contextClass", "BasicContext")
        lateinit var contextClass: String

        @Inject
        lateinit var database: MongoDatabase

        @Inject
        lateinit var coreService: CoreService

        override fun provide(): BuilderV2 = BuilderV2(coreService).apply {
            dialogueSources = database.getCollection<DialogueSource>("dialogue")
            dialogueMixins = database.getCollection<DialogueMixin>("dialogueMixin")
            speechRecognizers = database.getCollection()
            voices = database.getCollection()
            contextClass = this@Factory.contextClass
        }

        override fun dispose(builder: BuilderV2) {}
    }
}
