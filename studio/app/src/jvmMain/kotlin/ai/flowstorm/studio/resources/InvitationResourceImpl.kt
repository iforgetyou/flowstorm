package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.repository.UserRepository
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.studio.services.UserService
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/invitations")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
class InvitationResourceImpl : InvitationResource, MongoAbstractEntityRepository<Invitation>(Invitation::class) {

    @Inject
    lateinit var spaceResource: SpaceResource

    @Inject
    lateinit var userService: UserService

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var context: StudioContext

    private val users by lazy { database.getCollection<User>() }

    override fun get(invitationId: Id<Invitation>) = super.get(invitationId)

    override fun delete(invitation: Invitation): Invitation {
        collection.insertOne(invitation)

        return invitation
    }

    override fun delete(invitationId: Id<Invitation>) {
        collection.deleteOneById(invitationId)
    }

    override fun accept(invitationId: Id<Invitation>): Invitation {
        val username = context.getUser().username
        val inv = get(invitationId)

        if (!inv.username.equals(username,true)){
            error("Invitation does not match current user.")
        }


        val regex = "^$username$".replace(".", "\\.").toRegex(RegexOption.IGNORE_CASE)
        var user = users.find(User::username.regex(regex)).firstOrNull()

        if (user == null) {
            user = userService.createFromToken()
            userRepository.create(user)
        }

        spaceResource.createUserAssignment(
                inv.space_id,
                Space.UserAssignment(user._id, inv.role, "", Space.Settings("user setting"))
        )

        delete(invitationId)
        return inv
    }

    override fun find(query: Query): List<Invitation>  = error("Not supported")
}