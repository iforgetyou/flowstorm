package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.common.client.resourceMethod
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.model.Session
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import ai.flowstorm.core.resources.SessionResource as CoreSessionResource

@Path("/sessions")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@Authorized
@RequireRootRole(Admin)
class SessionResourceImpl: SessionResource {

    @Inject
    lateinit var query: Query
    
    @Inject
    lateinit var coreWebTarget: WebTarget

    override fun find(): List<Session> =
            coreWebTarget.resourceMethod(CoreSessionResource::find)
                .query(query).request().get(object : GenericType<List<Session>>() {})
}
