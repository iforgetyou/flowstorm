package ai.flowstorm.studio.security

open class BaseIdentity(
    override val name: String? = null,
    override val surname: String? = null,
    override val nickname: String? = null,
    override val email: String? = null,
    override val phoneNumber: String? = null,
    override val deviceId: String? = null,
) : Identity
