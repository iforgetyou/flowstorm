package ai.flowstorm.studio.resources

import ai.flowstorm.studio.security.BaseIdentity
import ai.flowstorm.studio.services.UserService
import ai.flowstorm.common.config.ConfigValue
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
class AuthResourceImpl : AuthResource {

    @ConfigValue("jwt.secret")
    lateinit var secret: String

    @ConfigValue("jwt.audience")
    lateinit var audience: String

    @ConfigValue("jwt.issuer")
    lateinit var issuer: String

    @Inject
    lateinit var userService: UserService

    override fun authDevice(deviceId: String): AuthResource.Tokens {
        //TODO add token expiration and refreshing
        val user = userService.createUserFromIdentity(BaseIdentity(deviceId = deviceId))

        val id = JWT.create()
            .withIssuer(issuer)
            .withClaim(UserService.DEVICE_ID_CLAIM, deviceId)
            .withClaim("name", user.fullName)
            .withClaim("email_verified", false)
            .sign(Algorithm.HMAC256(secret))

        val access = JWT.create()
            .withIssuer(issuer)
            .withAudience(audience)
            .withClaim(UserService.DEVICE_ID_CLAIM, deviceId)
            .withClaim("https://promethist/user.email", deviceId) //should be removed
            .sign(Algorithm.HMAC256(secret))

        return AuthResource.Tokens(id, access)
    }
}