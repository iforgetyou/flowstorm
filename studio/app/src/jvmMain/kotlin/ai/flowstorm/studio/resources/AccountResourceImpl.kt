package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Account
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.common.config.ConfigValue
import org.litote.kmongo.eq
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import org.bson.types.ObjectId
import org.litote.kmongo.id.toId
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
class AccountResourceImpl : AccountResource, MongoAbstractEntityRepository<Account>(Account::class) {

    @ConfigValue("idMap.anonymousAccount")
    lateinit var anonymousAccountId: String

    @Inject
    lateinit var context: StudioContext

    override fun find(): List<Account> {
        val user = context.getUser()
        val accounts = collection.find(Account::user_id eq user._id).toList()
        if (accounts.isEmpty())
            throw NotFoundException("Account not found for user ${user.username}")
        return accounts
    }

    override fun find(query: Query) = find()

    override fun getAnonymousAccount(): Account = get(ObjectId(anonymousAccountId).toId())
}