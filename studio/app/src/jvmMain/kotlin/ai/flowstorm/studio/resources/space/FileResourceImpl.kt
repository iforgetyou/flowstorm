package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.model.File
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.model.Space.Role.Editor
import ai.flowstorm.studio.resources.FileResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.resources.FileStorageResource
import ai.flowstorm.security.Authenticated
import ai.flowstorm.studio.model.DialogueMixin
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.regex
import kotlinx.datetime.Clock
import java.io.ByteArrayInputStream
import java.util.*
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Admin)
@RequireRole(Editor)
class FileResourceImpl : FileResource, MongoAbstractEntityRepository<File>(File::class) {

    @Inject
    lateinit var fileStorageResource: FileStorageResource

    @Inject
    lateinit var context: StudioContext

    override val collection: MongoCollection<File> by lazy {
        //TODO remove custom name of collection and rename it in mongodb
        database.getCollection("fileAsset", model.java)
    }

    override fun find() = find(context.query)

    override fun find(query: Query) = query(query, File::name, true).apply {
        add(match(context.spaceFilter()))
        query.search?.let {
            val r = ".*${it}.*"
            add(
                match(
                    or(
                        regex(File::name.path(), r, "i"),
                        File::type regex r,
                    )
                )
            )
        }
    }.run()

    private fun process(asset: File) {
        Regex("^data:([-\\w.]+/[-\\w.]+);(base64),(.*)$").find(asset.source!!)?.let {
            asset.type = it.groupValues[1]
            if (it.groupValues[2].lowercase() != "base64")
                error("File asset inline src data must be in base64 format")
            val path = "assets/spaces/${asset._id}"
            val bytes = Base64.getDecoder().decode(it.groupValues[3])
            val input = ByteArrayInputStream(bytes)
            fileStorageResource.writeFile(path, asset.type, listOf(), input)
            asset.source = null//ServiceUrlResolver.getEndpointUrl("core") + "/file/$path"
        } ?: if (!(asset.source?:"").startsWith("http")) {
            error("File asset src must contain http(s) URL or base64 inline data")
        }
        asset.datetime = Clock.System.now()
    }

    override fun create(file: File): File {
        process(file)
        val newAsset = file.copy(type = file.type, owner_id = context.getUser()._id)
        collection.insertOne(newAsset)
        context.activity(File::class, file._id, file.name, Activity.Type.Created)
        return newAsset
    }

    override fun get(id: Id<File>): File =
        collection.find(and(File::_id eq id, File::space_id eq context.spaceId)).singleOrNull()
                ?: throw NotFoundException("File asset $id does not exist")

    override fun update(id: Id<File>, file: File): File {
        process(file)
        collection.updateOne(File::_id eq id, file)
        context.activity(File::class, file._id, file.name, Activity.Type.Updated)
        return file
    }

    override fun delete(id: Id<File>) {
        //we do not intentionally delete file to keep existing reference URLs working
        val asset = get(id)
        collection.deleteOneById(id)
        context.activity(File::class, id, asset.name, Activity.Type.Deleted)
    }
}
