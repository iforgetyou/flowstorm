package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.core.resources.TtsResource
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.nlp.tts.TtsRequest
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Space.Role.Admin)
@RequireRole(Space.Role.Editor)
class TtsResourceImpl : TtsResource {

    @Inject
    lateinit var ttsResource: TtsResource

    override fun synthesize(request: TtsRequest) = ttsResource.synthesize(request)
    override fun synthesizeFormat(request: TtsRequest, fileType: AudioFileType) = ttsResource.synthesizeFormat(request, fileType)
}