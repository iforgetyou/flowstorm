package ai.flowstorm.studio.resources

import ai.flowstorm.common.client.resourceMethod
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.core.model.Report
import ai.flowstorm.core.type.PropertyMap
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType
import ai.flowstorm.core.resources.ReportResource

@Path("/reports")
@Produces(MediaType.APPLICATION_JSON)
class ReportResourceImpl : ReportResource {

    @Inject
    lateinit var reportResource: ReportResource

    @Inject
    lateinit var coreWebTarget: WebTarget

    @Inject
    lateinit var query: Query

    override fun getData(granularity: Report.Granularity, aggregations: List<String>): Report {
        return coreWebTarget
                .path("/reports")
                .resourceMethod(ReportResource::getData)
                .queryParam("granularity", granularity)
                .queryParam("aggregations", *aggregations.toTypedArray())
                .query(query).request().get(Report::class.java)
    }

    override fun getMetrics(): List<PropertyMap> = reportResource.getMetrics()

    override fun getNamespaces(): List<PropertyMap> = reportResource.getNamespaces()
}