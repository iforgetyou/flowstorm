package ai.flowstorm.studio.security

import com.mongodb.client.MongoDatabase
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.model.UserSpaceRole
import ai.flowstorm.common.config.ConfigValue
import org.bson.types.ObjectId
import org.glassfish.jersey.server.wadl.processor.WadlModelProcessor
import org.litote.kmongo.elemMatch
import org.litote.kmongo.eq
import org.litote.kmongo.getCollection
import org.litote.kmongo.id.toId
import ai.flowstorm.common.security.AuthorizationAdapter
import ai.flowstorm.common.security.AuthorizationAdapter.AuthorizationFailed
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ResourceInfo
import javax.ws.rs.core.Context

class RoleAuthorizationAdapter : AuthorizationAdapter {

    @Inject
    lateinit var database: MongoDatabase

    @Context
    lateinit var resourceInfo: ResourceInfo

    @Context
    lateinit var requestContext: ContainerRequestContext

    @ConfigValue("idMap.rootSpace")
    lateinit var rootSpaceId: String

    override fun authorize() {
        if (resourceInfo.resourceClass == WadlModelProcessor.OptionsHandler::class.java) return //ignore requests handled by implicit OPTIONS handler

        val (annotation, rootAnnotation) = getAnnotations(resourceInfo)
        val annotationPresent = annotation != null || rootAnnotation != null

        if (annotationPresent) {
            val username = requestContext.securityContext.userPrincipal?.name
                ?: throw Exception("Can not authorize user without authentication.(Missing @Authenticated annotation on requested method?)")
            val userRoles = getUserRoles(username)
            var allowed = false

            val spaceId = requestContext.uriInfo.pathParameters.getFirst("spaceId")
            if (annotation != null && spaceId != null) {
                val roleInSpace = userRoles.find { it.spaceId == ObjectId(spaceId).toId<Space>() }?.role
                if (roleInSpace != null && Space.Role.isAllowed(annotation.role, roleInSpace)) allowed = true
            }

            if (rootAnnotation != null) {
                val roleInSpace = userRoles.find { it.spaceId == ObjectId(rootSpaceId).toId<Space>() }?.role
                if (roleInSpace != null && Space.Role.isAllowed(rootAnnotation.role, roleInSpace)) allowed = true
            }

            if (!allowed) {
                val roles = listOfNotNull(
                    annotation?.let { "${it.role} in current space" },
                    rootAnnotation?.let { "${it.role} in ROOT space" }
                ).joinToString(" or ")

                throw AuthorizationFailed("Required role $roles")
            }
        } else {
            throw Exception("Required authorization, but neither ${RequireRole::class.simpleName} nor ${RequireRootRole::class.simpleName} annotation present[${resourceInfo.resourceClass.simpleName}${resourceInfo.resourceMethod.name}].")
        }
    }

    private fun getAnnotations(resourceInfo: ResourceInfo): Pair<RequireRole?, RequireRootRole?> = with(resourceInfo) {
        Pair(
            resourceMethod.getAnnotation(RequireRole::class.java)
                ?: resourceClass.getAnnotation(RequireRole::class.java),
            resourceMethod.getAnnotation(RequireRootRole::class.java)
                ?: resourceClass.getAnnotation(RequireRootRole::class.java)
        )
    }

    private fun getUserRoles(username: String): List<UserSpaceRole> {
        //fixme: this code is copy pasted from resource class because it is not possible to inject resource class into the filter

        val user = database.getCollection<User>().find(User::username eq username).singleOrNull()
            ?: throw NotFoundException("User does not exist")
        val spaces = database.getCollection<Space>(Space.COLLECTION_NAME).find(Space::userAssignments elemMatch (Space.UserAssignment::user_id eq user._id)).toList()
        val userRoles = mutableListOf<UserSpaceRole>()

        for (space in spaces) {
            for (userAssignment in space.userAssignments) {
                if (userAssignment.user_id == user._id) {
                    userRoles.add(UserSpaceRole(space._id, space.name, userAssignment.role))
                }
            }
        }

        return userRoles
    }
}