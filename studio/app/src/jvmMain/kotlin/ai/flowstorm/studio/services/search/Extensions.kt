package ai.flowstorm.studio.services.search

import ai.flowstorm.studio.model.SearchRequest
import org.bson.types.ObjectId

val SearchRequest.id get() = if (ObjectId.isValid(search)) ObjectId(search) else null
