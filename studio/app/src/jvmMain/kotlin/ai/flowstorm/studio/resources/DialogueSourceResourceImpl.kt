package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.DialogueSourceV2
import ai.flowstorm.studio.model.DialogueSourceV2.View.WithGraph
import ai.flowstorm.studio.repository.DialogueSourceRepository
import com.fasterxml.jackson.annotation.JsonView
import org.litote.kmongo.Id
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Path("/dialogueSources")
class DialogueSourceResourceImpl : DialogueSourceSimpleResource {

    @Inject
    lateinit var repository: DialogueSourceRepository

    @JsonView(WithGraph::class)
    override fun get(id: Id<DialogueSource>): DialogueSourceV2 {
        return repository.get(id.cast())
    }
}