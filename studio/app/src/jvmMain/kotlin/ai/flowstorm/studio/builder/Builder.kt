package ai.flowstorm.studio.builder

import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.core.model.DialogueBuild

interface Builder {
    fun build(dialogueSource: DialogueSource): DialogueBuild
}