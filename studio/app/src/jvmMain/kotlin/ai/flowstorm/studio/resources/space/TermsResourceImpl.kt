package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.Terms
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import kotlinx.datetime.Clock
import org.litote.kmongo.*
import javax.inject.Inject
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Space.Role.Admin)
@RequireRole(Space.Role.Editor)
open class TermsResourceImpl : TermsResource, MongoAbstractEntityRepository<Terms>(Terms::class) {

    @Inject
    lateinit var context: StudioContext

    override fun find() = find(context.query)
    override fun find(query: Query)  = query(query, Terms::name, true).apply {
        add(match(context.spaceFilter()))
    }.run()

    override fun create(entity: Terms) = entity.apply {
        space_id = context.spaceId
        val latest = collection.find(Terms::name eq entity.name, Terms::space_id eq space_id)
                .sort(orderBy(Terms::version, ascending = false)).firstOrNull()
        version = (latest?.version ?: -1) + 1
        collection.insertOne(this)
    }

    override fun update(id: Id<Terms>, entity: Terms) = entity.apply {
        space_id = context.spaceId
        datetime = Clock.System.now()
        collection.updateOneById(id, this)
    }

    override fun delete(id: Id<Terms>) {
        collection.deleteOneById(id)
    }
}