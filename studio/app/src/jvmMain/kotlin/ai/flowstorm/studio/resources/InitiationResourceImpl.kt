package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.services.CoreService
import com.twilio.http.TwilioRestClient
import com.twilio.rest.api.v2010.account.Call
import com.twilio.type.PhoneNumber
import com.twilio.type.Twiml
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.Defaults
import ai.flowstorm.util.LoggerDelegate
import java.util.*
import javax.inject.Inject
import javax.ws.rs.BadRequestException
import javax.ws.rs.NotFoundException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.toList
import ai.flowstorm.core.model.Application as CoreApplication
import ai.flowstorm.core.model.Space as CoreSpace

@Path("/initiations")
@Produces(MediaType.APPLICATION_JSON)
class InitiationResourceImpl : InitiationResource, MongoAbstractEntityRepository<Initiation>(Initiation::class) {

    @Inject
    lateinit var coreService: CoreService

    private val applications by lazy { database.getCollection<Application>() }
    private val dialogues by lazy { database.getCollection<DialogueSourceV2>("dialogue") }
    private val devices by lazy { database.getCollection<Device>() }
    private val users by lazy { database.getCollection<User>() }
    private val twilioRestClients = mutableMapOf<String, TwilioRestClient>()
    private val logger by LoggerDelegate()


    override fun create(initiation: Initiation, space: Space): Initiation {
        collection.insertOne(initiation)
        if (initiation.deviceType == Initiation.DeviceType.Phone)
            phoneInitiation(initiation, space.twilioAccountID!!, space.twilioAuthToken!!)
        return initiation
    }

    override fun listForDevice(deviceId: String, deviceToken: String): List<Initiation> {
        val device = devices.findOne(Device::deviceId eq deviceId, Device::deviceToken eq deviceToken)
                ?: throw NotFoundException("No valid device $deviceId")

        return if (device.user_id != null)
            collection.find(Initiation::users_id contains device.user_id).toList()
        else
            listOf()
    }

    override fun list(spaceId: Id<CoreSpace>) =
        collection.find(Initiation::space_id eq spaceId).sort(descending(Initiation::datetime)).toList()

    data class PhoneCall(val from: String, val to: String, val twiml: String)

    private val callUrl get() = coreService.defaultConnectionInfo.url.replaceFirst("http", "ws") + "/call/"

    private fun phoneInitiation(initiation: Initiation, username: String, password: String) {
        val application = applications.find(Application::_id eq initiation.application_id).singleOrNull()
                ?: throw NotFoundException("Application ${initiation.application_id} does not exist")
        val from = application.phoneNumber
                ?: throw BadRequestException("Application ${application.name} has no phone number set")
        val phoneCalls = mutableListOf<PhoneCall>()
        initiation.users_id.forEach { user_id ->
            val user = users.find(User::_id eq user_id).singleOrNull()
                    ?: throw NotFoundException("User ${user_id} does not exist")
            val to = user.phoneNumber
                    ?: throw BadRequestException("User ${user.username} has no phone number set")
            val call = PhoneCall(from, to, """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <Response>
                      <Connect>
                        <Stream url="$callUrl">
                          <Parameter name="locale" value="${user.language}" />
                          <Parameter name="deviceId" value="$to" />
                          <Parameter name="appKey" value=":${application._id}" />
                          <Parameter name="initiationId" value="${initiation._id}"/>
                        </Stream>  
                      </Connect>
                    </Response>
                """.trimIndent()
            )
            phoneCalls.add(call)
        }
        phoneCalls.forEach { phoneCall ->
            logger.info("Initiating call session via Twilio - ${phoneCall.twiml}")
            val sid = Call.creator(PhoneNumber(phoneCall.to), PhoneNumber(phoneCall.from), Twiml(phoneCall.twiml))
                    .create(twilioRestClients.getOrPut(username) {
                        TwilioRestClient.Builder(username, password).build()
                    })
                    .sid
            logger.info("Phone call initiated (sid=$sid)")
        }
    }

    override fun getCallTwiML(applicationId: Id<CoreApplication>, deviceId: String?, zip: String?, city: String?, state: String?, country: String?) =
            applications.findOneById(applicationId)?.let { application ->
                val locale = dialogues.findOneById(application.dialogue_id!!)?.let { dialogue ->
                    Locale(dialogue.language)
                } ?: Defaults.locale
                """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <Response>
                      <Connect>
                        <Stream url="$callUrl">
                          <Parameter name="locale" value="$locale" />
                          <Parameter name="zip" value="$zip" />
                          <Parameter name="city" value="$city" />
                          <Parameter name="state" value="$state" />
                          <Parameter name="country" value="$country" />
                          <Parameter name="deviceId" value="$deviceId" />
                          <Parameter name="appKey" value="${application._id}" />
                        </Stream>  
                      </Connect>
                    </Response>""".trimIndent()
    } ?: throw NotFoundException("Application $applicationId does not exist")

    override fun find(query: Query): List<Initiation>  = error("Not supported")
}