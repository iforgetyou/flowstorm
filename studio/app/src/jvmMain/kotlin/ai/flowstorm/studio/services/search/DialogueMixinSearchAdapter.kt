package ai.flowstorm.studio.services.search

import ai.flowstorm.studio.model.DialogueMixin
import ai.flowstorm.studio.model.DialogueSourceV2
import ai.flowstorm.studio.model.SearchRequest
import ai.flowstorm.studio.model.SearchResult
import org.bson.conversions.Bson
import org.litote.kmongo.*

class DialogueMixinSearchAdapter(sr: SearchRequest) : SearchAdapter(sr) {

    override fun exactSearch(): Bson = match(
        or(
            DialogueMixin::name regex sr.regex,
            DialogueMixin::text regex sr.regex,
        )
    )

    override fun projectionStage() = project(
        SearchResult::id from "\$_id",
        DialogueMixin::name from DialogueMixin::name,
        SearchResult::data / DialogueMixin::type from DialogueMixin::type,
    )

    override fun filterStage(): Bson = match()
}
