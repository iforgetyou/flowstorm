package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.model.Space.Role.Editor
import ai.flowstorm.studio.model.Voice
import ai.flowstorm.studio.resources.VoiceResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import com.mongodb.client.model.Filters.regex
import javax.inject.Inject
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Admin)
@RequireRole(Editor)
open class VoiceResourceImpl : VoiceResource, MongoAbstractEntityRepository<Voice>(Voice::class) {

    @Inject
    lateinit var context: StudioContext

    override fun find() = find(context.query)
    override fun find(query:Query) = query(query, Voice::name, true).apply {
        add(match(context.spaceFilter()))
        query.search?.let {
            val r = ".*${it}.*"
            add(
                match(
                    or(
                        regex(Voice::name.path(), r, "i"),
                    )
                )
            )
        }
    }.run()

    override fun get(id: Id<Voice>) = super.get(id)

    override fun create(entity: Voice) = entity.also {
        it.space_id = context.spaceId
        collection.insertOne(it)
        context.activity(Voice::class, it._id, it.name, Activity.Type.Created)
    }

    override fun update(id: Id<Voice>, entity: Voice) = entity.also {
        it.space_id = context.spaceId
        collection.updateOneById(id, it)
        context.activity(Voice::class, it._id, it.name, Activity.Type.Updated)
    }

    override fun delete(id: Id<Voice>) {
        val it = get(id)
        collection.deleteOneById(id)
        context.activity(Voice::class, id, it.name, Activity.Type.Deleted)
    }
}
