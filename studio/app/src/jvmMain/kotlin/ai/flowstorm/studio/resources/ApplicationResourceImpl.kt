package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.repository.ApplicationRepository
import ai.flowstorm.studio.security.RequireRootRole
import org.litote.kmongo.Id
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.NotAllowedException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/applications")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
class ApplicationResourceImpl : ApplicationResource {

    @Inject
    lateinit var applicationRepository: ApplicationRepository

    @Authorized
    @RequireRootRole(Admin)
    override fun find(): List<Application> = applicationRepository.all()

    override fun get(applicationId: Id<Application>) = applicationRepository.get(applicationId)

    override fun create(application: Application): Nothing = throw NotAllowedException("Not Allowed")

    override fun delete(applicationId: Id<Application>): Nothing = throw NotAllowedException("Not Allowed")

    override fun update(applicationId: Id<Application>, application: Application): Nothing = throw NotAllowedException("Not Allowed")
}