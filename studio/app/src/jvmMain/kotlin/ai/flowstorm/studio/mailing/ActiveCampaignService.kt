package ai.flowstorm.studio.mailing

import ai.flowstorm.studio.model.User
import ai.flowstorm.common.config.ConfigValue
import java.net.URL

class ActiveCampaignService : MailingService() {

    data class ContactRequest(val contact: Contact, val fieldValues: List<FieldValue> = listOf()) {
        data class Contact(val email: String, val firstName: String, val lastName: String, val phone: String)
        data class FieldValue(val field: String, val value: String)
    }

    data class ContactResponse(val contact: Contact) {
        data class Contact(val id: String)
    }

    data class ContactListRequest(val contactList: ContactList) {
        data class ContactList(val list: String, val contact: String, val status: String = "1")
    }

    override val name = "activecampaign"

    @ConfigValue("mailing.url", "")
    lateinit var url: String

    @ConfigValue("mailing.listId", "")
    lateinit var listId: String

    override fun getAuthenticationHeader() = "Api-Token" to apiKey

    override fun createContact(user: User) = call<ContactResponse>(
        URL("$url/contacts"),
        Method.POST,
        ContactRequest(ContactRequest.Contact(user.username, user.name, user.surname, user.phoneNumber ?: ""))
    ).also { contactResponse ->
        if (contactResponse != null) {
            logger.info("Contact ${contactResponse.contact} created")
            call<Any>(
                URL("$url/contactLists"), Method.POST, ContactListRequest(
                    ContactListRequest.ContactList(listId, contactResponse.contact.id)
                )
            )
        }
    }
}