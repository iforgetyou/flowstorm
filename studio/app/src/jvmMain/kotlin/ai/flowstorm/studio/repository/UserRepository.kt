package ai.flowstorm.studio.repository

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.model.UserSpaceRole
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import org.litote.kmongo.*
import kotlin.collections.toList
import ai.flowstorm.core.model.User as CoreUser

class UserRepository : MongoAbstractEntityRepository<User>(User::class) {

    private val spaces by lazy { database.getCollection<Space>() }
    private val applications by lazy { database.getCollection<Application>() }

    override fun find(query: Query): List<User> = query(query, User::username, true).apply {
        query.search?.let {
            val r = ".*${it}.*"
            add(
                match(
                    or(
                        com.mongodb.client.model.Filters.regex(User::name.path(), r, "i"),
                        com.mongodb.client.model.Filters.regex(User::surname.path(), r, "i"),
                        com.mongodb.client.model.Filters.regex(User::username.path(), r, "i"),
                    )
                )
            )
        }
    }.run()

    fun findByUsername(username: String): User? = collection.findOne(User::username eq username)

    fun findRolesForUser(userId: Id<User>): List<UserSpaceRole> {
        val spaces = spaces.find(Space::userAssignments elemMatch (Space.UserAssignment::user_id eq userId as Id<CoreUser>)).toList()

        val userRoles = mutableListOf<UserSpaceRole>()

        for (space in spaces) {
            for (userAssignment in space.userAssignments) {
                if (userAssignment.user_id == userId) {
                    userRoles.add(UserSpaceRole(space._id, space.name, userAssignment.role))
                }
            }
        }

        return userRoles
    }

    fun findSpacesForUser(userId: Id<User>): List<Space> {
        return spaces.find(
            or(
                Space::userAssignments elemMatch (Space.UserAssignment::user_id eq userId as Id<CoreUser>),
            )
        ).toList()
    }

    fun findApplicationsForUser(userId: Id<User>): List<Application> {
        val user = get(userId)
        val apps = mutableListOf<Application>()
        //TODO get user apps more efficiently (probably store assigned app ids into user object)
        for (space in spaces.find()) {

            val ids = space.userAssignments.filter { u: Space.UserAssignment -> u.user_id == user._id }
                .singleOrNull()?.applications_id ?: mutableListOf()

            for (app in applications.find(Application::_id `in` ids)) {
                app._space_name = space.name
                apps.add(app)
            }
        }
        return apps
    }
}
