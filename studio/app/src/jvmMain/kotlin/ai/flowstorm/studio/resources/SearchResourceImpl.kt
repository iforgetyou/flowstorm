package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.SearchRequest
import ai.flowstorm.studio.model.SearchResult
import ai.flowstorm.studio.model.SearchType
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.services.search.SearchService
import org.litote.kmongo.Id
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/search")
@Produces(MediaType.APPLICATION_JSON)
class SearchResourceImpl : SearchResource {

    @Inject
    lateinit var searchService: SearchService

    override fun search(spaceId: Id<Space>, entity: String, search: String, type:String): List<SearchResult> {
        return searchService.search(
            SearchRequest(
                search,
                entity,
                SearchType.valueOf(type),
                spaceId.cast(),
            )
        )
    }
}
