package ai.flowstorm.studio.util

import java.util.*
import javax.xml.namespace.NamespaceContext
import javax.xml.stream.XMLStreamWriter

class IndentingXMLStreamWriter(
        private val writer: XMLStreamWriter,
        private var indentStep: String = "  ",
        private val lineSeparator: String = "\n") : XMLStreamWriter {
    
    enum class State { SEEN_NOTHING, SEEN_ELEMENT, SEEN_DATA }
    
    private var state: State = State.SEEN_NOTHING
    private val stateStack = Stack<State>()
    private var depth = 0
    
    private fun onStartElement() {
        stateStack.push(State.SEEN_ELEMENT)
        state = State.SEEN_NOTHING
        if (depth > 0) {
            writer.writeCharacters(lineSeparator)
        }
        doIndent()
        depth++
    }
    
    private fun onEndElement() {
        depth--
        if (state === State.SEEN_ELEMENT) {
            writer.writeCharacters(lineSeparator)
            doIndent()
        }
        state = stateStack.pop()
    }
    
    private fun onEmptyElement() {
        state = State.SEEN_ELEMENT
        if (depth > 0) {
            writer.writeCharacters(lineSeparator)
        }
        doIndent()
    }

    private fun doIndent() {
        for (i in 0 until depth) writer.writeCharacters(indentStep)
    }
    
    override fun writeStartDocument() {
        writer.writeStartDocument()
        writer.writeCharacters(lineSeparator)
    }

    override fun writeStartDocument(version: String) {
        writer.writeStartDocument(version)
        writer.writeCharacters(lineSeparator)
    }
    
    override fun writeStartDocument(encoding: String, version: String) {
        writer.writeStartDocument(encoding, version)
        writer.writeCharacters(lineSeparator)
    }
    
    override fun writeStartElement(localName: String) {
        onStartElement()
        writer.writeStartElement(localName)
    }
    
    override fun writeStartElement(namespaceURI: String, localName: String) {
        onStartElement()
        writer.writeStartElement(namespaceURI, localName)
    }
    
    override fun writeStartElement(prefix: String, localName: String, namespaceURI: String) {
        onStartElement()
        writer.writeStartElement(prefix, localName, namespaceURI)
    }
    
    override fun writeEmptyElement(namespaceURI: String, localName: String) {
        onEmptyElement()
        writer.writeEmptyElement(namespaceURI, localName)
    }
    
    override fun writeEmptyElement(prefix: String, localName: String, namespaceURI: String) {
        onEmptyElement()
        writer.writeEmptyElement(prefix, localName, namespaceURI)
    }
    
    override fun writeEmptyElement(localName: String) {
        onEmptyElement()
        writer.writeEmptyElement(localName)
    }
    
    override fun writeEndElement() {
        onEndElement()
        writer.writeEndElement()
    }

    override fun writeEndDocument() = writer.writeEndDocument()

    override fun close() = writer.close()

    override fun flush() = writer.flush()

    override fun writeAttribute(localName: String, value: String) = writer.writeAttribute(localName, value)

    override fun writeAttribute(prefix: String, namespaceURI: String, localName: String, value: String) = writer.writeAttribute(prefix, namespaceURI, localName, value)

    override fun writeAttribute(namespaceURI: String, localName: String, value: String) = writer.writeAttribute(namespaceURI, localName, value)

    override fun writeNamespace(prefix: String, namespaceURI: String) = writer.writeNamespace(prefix, namespaceURI)

    override fun writeDefaultNamespace(namespaceURI: String) = writer.writeDefaultNamespace(namespaceURI)

    override fun writeComment(data: String) = writer.writeComment(data)

    override fun writeProcessingInstruction(target: String) = writer.writeProcessingInstruction(target)

    override fun writeProcessingInstruction(target: String, data: String) = writer.writeProcessingInstruction(target, data)

    override fun writeCData(data: String) {
        state = State.SEEN_DATA
        writer.writeCData(data)
    }

    override fun writeDTD(dtd: String) = writer.writeDTD(dtd)

    override fun writeEntityRef(name: String) = writer.writeEntityRef(name)
    
    override fun writeCharacters(text: String) {
        state = State.SEEN_DATA
        writer.writeCharacters(text)
    }
    
    override fun writeCharacters(text: CharArray, start: Int, len: Int) { 
        state = State.SEEN_DATA
        writer.writeCharacters(text, start, len)
    }
    
    override fun getPrefix(uri: String): String = writer.getPrefix(uri)
    
    override fun setPrefix(prefix: String, uri: String) = writer.setPrefix(prefix, uri)
    
    override fun setDefaultNamespace(uri: String) = writer.setDefaultNamespace(uri)
    
    override fun setNamespaceContext(context: NamespaceContext) { writer.namespaceContext = context }

    override fun getNamespaceContext(): NamespaceContext = writer.namespaceContext
    
    override fun getProperty(name: String): Any = writer.getProperty(name)
}
