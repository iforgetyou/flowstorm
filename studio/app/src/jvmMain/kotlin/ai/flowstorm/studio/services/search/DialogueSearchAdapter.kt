package ai.flowstorm.studio.services.search

import ai.flowstorm.studio.model.DialogueSourceV2
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.studio.model.SearchRequest
import ai.flowstorm.studio.model.SearchResult
import com.mongodb.client.model.Filters
import org.bson.conversions.Bson
import org.litote.kmongo.*
import org.litote.kmongo.id.toId

class DialogueSearchAdapter(sr: SearchRequest) : SearchAdapter(sr) {
    override fun getIdFieldsPaths(): List<String> = listOf(
        "graph.subDialogues.sourceId",
    )

    override fun exactSearch(): Bson = match(
        if (sr.id != null)
            or(
                DialogueSourceV2::mixins_id contains sr.id!!.toId(),
                DialogueSourceV2::graph / Graph::userInputs / Graph.UserInput::mixins_id contains sr.id!!.toId(),
                DialogueSourceV2::graph / Graph::intents / Graph.Intent::mixins_id contains sr.id!!.toId(),
                DialogueSourceV2::graph / Graph::globalIntents / Graph.Intent::mixins_id contains sr.id!!.toId(),
                DialogueSourceV2::graph / Graph::speeches / Graph.Speech::mixins_id contains sr.id!!.toId(),
                DialogueSourceV2::graph / Graph::functions / Graph.Function::mixins_id contains sr.id!!.toId(),
                DialogueSourceV2::graph / Graph::commands / Graph.Command::mixin_id eq sr.id!!.toId(),
                DialogueSourceV2::graph / Graph::subDialogues / Graph.SubDialogue::sourceId eq sr.id!!.toId(),
            )
        else
            or(
                DialogueSourceV2::dialogueName regex sr.regex,
                DialogueSourceV2::initCode regex sr.regex,
                DialogueSourceV2::graph / Graph::functions / Graph.Function::code regex sr.regex,
                DialogueSourceV2::graph / Graph::userInputs / Graph.UserInput::code regex sr.regex,
                DialogueSourceV2::graph / Graph::intents / Graph.Intent::utterances elemMatch Filters.regex(
                    "",
                    sr.regex
                ),
                DialogueSourceV2::graph / Graph::globalIntents / Graph.Intent::utterances elemMatch Filters.regex(
                    "",
                    sr.regex
                ),
            )
    )

    override fun projectionStage() = project(
        SearchResult::id from "\$_id",
        SearchResult::name from DialogueSourceV2::dialogueName,
        SearchResult::data / DialogueSourceV2::version from DialogueSourceV2::version,
        SearchResult::data / DialogueSourceV2::state from DialogueSourceV2::state
    )

    override fun filterStage(): Bson = match(
        DialogueSourceV2::state ne DialogueSourceV2.State.Archived,
    )
}
