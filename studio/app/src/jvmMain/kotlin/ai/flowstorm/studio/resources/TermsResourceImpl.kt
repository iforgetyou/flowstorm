package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.Terms
import ai.flowstorm.studio.model.TermsConsent as Consent
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import org.litote.kmongo.*
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.toList
import ai.flowstorm.core.model.Application as CoreApplication

@Path("/terms")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
class TermsResourceImpl : TermsResource, MongoAbstractEntityRepository<Terms>(Terms::class) {

    @Inject
    lateinit var context: StudioContext

    private val consents by lazy { database.getCollection<Consent>() }
    private val applications by lazy { database.getCollection<Application>() }

    override fun find(id: Id<Terms>) = super.find(id)

    override fun giveConsent(termsId: Id<Terms>): Consent {
        val terms = get(termsId)
        val user = context.getUser()
        val consent = Consent(user_id = user._id, terms_id = terms._id)
        consents.insertOne(consent)
        return consent
    }

    override fun getConsent(termsId: Id<Terms>): Consent {
        val terms = get(termsId)
        val user = context.getUser()
        return consents.find(and(Consent::user_id eq user._id, Consent::terms_id eq terms._id)).toList().firstOrNull() ?: throw NotFoundException("Consent for terms $termsId not found")
    }

    override fun revokeConsent(termsId: Id<Terms>) {
        val terms = get(termsId)
        val user = context.getUser()
        consents.find(and(Consent::user_id eq user._id, Consent::terms_id eq terms._id)).toList().firstOrNull()?.let {
            consents.deleteOneById(it._id)
        }
    }

    override fun findByApp(appId: Id<Application>): Terms? {
        return applications.findOne(Application::_id eq appId as Id<CoreApplication>)?.terms_id?.let { get(it) }
    }

    override fun find(query: Query): List<Terms> = error("Not supported")
}