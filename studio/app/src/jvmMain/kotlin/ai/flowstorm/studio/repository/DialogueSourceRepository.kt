package ai.flowstorm.studio.repository

import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.DialogueSourceV2
import ai.flowstorm.studio.model.Graph
import ai.flowstorm.common.query.MongoFiltersFactory
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.Space
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.CountOptions
import org.bson.conversions.Bson
import org.litote.kmongo.*
import kotlin.collections.toList

class DialogueSourceRepository : MongoAbstractEntityRepository<DialogueSourceV2>(DialogueSourceV2::class) {

    override val collection: MongoCollection<DialogueSourceV2> by lazy {
        database.getCollection("dialogue", model.java)
    }

    fun findLatest(spaceId: Id<Space>, dialogueName: String): DialogueSource? =
        collection.find(DialogueSource::dialogueName eq dialogueName, DialogueSource::space_id eq spaceId)
            .sort(orderBy(DialogueSource::version, ascending = false)).firstOrNull()

    fun findUsages(id: Id<DialogueSourceV2>): List<DialogueSourceV2> {
        return collection.find(
            (DialogueSourceV2::graph / Graph::subDialogues / Graph.SubDialogue::sourceId).eq<Id<out DialogueSource>>(id),
            DialogueSourceV2::state eq DialogueSourceV2.State.Published
        ).toList()
    }

    fun findReferenced(dialogueSource: DialogueSourceV2): List<DialogueSourceV2> {
        val ids = dialogueSource.graph.subDialogues.map { it.sourceId as Id<DialogueSourceV2> }
        return collection.find(DialogueSource::_id `in` ids).toList()
    }

    data class DialogueInfo(val _id: Id<DialogueSource>, val dialogueName: String, val version:Int)

    fun getInfo(spaceId:Id<Space>, query:Query): List<DialogueInfo> {
        return collection.aggregate<DialogueInfo>(
            match(DialogueSource::space_id eq spaceId),
            match(MongoFiltersFactory.createFilter(DialogueSourceV2::class, query)),
            sort(orderBy(DialogueSource::dialogueName, DialogueInfo::version)),
            project(DialogueInfo::dialogueName from DialogueSource::dialogueName, DialogueInfo::version from DialogueSource::version)
        ).toList()
    }

    fun countDocuments(filter: Bson, limit:Int): Long = collection.countDocuments(filter, CountOptions().limit(limit))
}