package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.repository.ApplicationRepository
import ai.flowstorm.studio.resources.ApplicationResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.security.Authenticated
import org.litote.kmongo.*
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@Authorized
@RequireRole(Admin)
class ApplicationResourceImpl : ApplicationResource {

    @Inject
    lateinit var repository: ApplicationRepository

    @Inject
    lateinit var context: StudioContext

    override fun find(): List<Application> = find(context.query)

    override fun create(application: Application): Application {
        return application.let { app ->
            app.space_id = context.spaceId
            repository.create(app).also {
                context.activity(DialogueSnippet::class, it._id, it.name, Activity.Type.Created)
            }
        }
    }

    override fun get(applicationId: Id<Application>) =
        repository.get(applicationId).takeIf { it.space_id == context.spaceId }
            ?: throw NotFoundException("Application not found")

    private fun find(query: Query): List<Application> = repository.query(query, Application::name, true).apply {
        add(match(context.spaceFilter(false)))
    }.run()


    override fun update(applicationId: Id<Application>, application: Application): Application {
        val app = get(applicationId) //ensure app exists and belongs to space
        repository.updateById(applicationId, application)
        context.activity(Application::class, applicationId, application.name, application.space_id, Activity.Type.Updated)
        return application
    }

    override fun delete(applicationId: Id<Application>) {
        val application = get(applicationId)
        repository.remove(application._id as Id<Application>)
        context.activity(Application::class, application._id, application.name, application.space_id, Activity.Type.Deleted)
    }
}