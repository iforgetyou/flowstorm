package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.Space.Role.*
import ai.flowstorm.studio.repository.ApplicationRepository
import ai.flowstorm.studio.repository.UserRepository
import ai.flowstorm.studio.resources.space.*
import ai.flowstorm.studio.resources.space.ApplicationResourceImpl
import ai.flowstorm.studio.resources.space.CommunityResource
import ai.flowstorm.studio.resources.space.ReportResourceImpl
import ai.flowstorm.studio.resources.space.TermsResource
import ai.flowstorm.studio.resources.space.TermsResourceImpl
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.studio.services.CoreService
import ai.flowstorm.studio.services.InvitationSender
import ai.flowstorm.common.client.resourceMethod
import ai.flowstorm.common.query.MongoFiltersFactory
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.core.model.DialogueEvent
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.core.resources.DevicePairingResource
import ai.flowstorm.core.resources.SessionResource
import ai.flowstorm.common.query.Query.Filter
import ai.flowstorm.common.query.Query.Operator
import ai.flowstorm.core.model.Application as CoreApplication
import ai.flowstorm.core.model.User as CoreUser
import ai.flowstorm.util.LoggerDelegate
import org.litote.kmongo.*
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.common.resources.Check
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.resources.ReportResource
import ai.flowstorm.core.resources.TtsResource
import ai.flowstorm.security.Authenticated
import com.mongodb.client.model.Filters.regex
import org.apache.logging.log4j.util.FilteredObjectInputStream
import javax.inject.Inject
import javax.ws.rs.BadRequestException
import javax.ws.rs.NotFoundException
import javax.ws.rs.client.WebTarget
import javax.ws.rs.container.ResourceContext
import javax.ws.rs.core.Context
import javax.ws.rs.core.GenericType
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.toList
import ai.flowstorm.core.model.Device as CoreDevice

@Path("/spaces")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@Authorized
class SpaceResourceImpl : SpaceResource, MongoAbstractEntityRepository<Space>(Space::class) {

    private val logger by LoggerDelegate()

    @Inject
    lateinit var context: StudioContext

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var deviceResource: DeviceResource

    @Inject
    lateinit var invitationSender: InvitationSender

    @Inject
    lateinit var devicePairingResource: DevicePairingResource

    @Inject
    lateinit var initiationResource: InitiationResource

    @Inject
    lateinit var coreWebTarget: WebTarget

    @Inject
    lateinit var coreService: CoreService

    @Context
    lateinit var resourceContext: ResourceContext

    @Inject
    lateinit var applicationRepository: ApplicationRepository

    private val simulations by lazy { database.getCollection<Simulation>() }
    private val invitations by lazy { database.getCollection<Invitation>() }
    private val devices by lazy { database.getCollection<Device>() }
    private val users by lazy { database.getCollection<User>() }

    @RequireRootRole(Admin)
    override fun find(): List<Space>  = find(context.query)

    override fun find(query: Query): List<Space> {
        val spaces = query(query, Space::name, true).apply {
            query.search?.let {
                val r = ".*${it}.*"
                add(match(regex(Space::name.path(), r, "i")))
            }
        }.run()

        for (space in spaces)
            for (userAssignment in space.userAssignments) {
                populateUserAssignment(userAssignment)
            }

        return spaces
    }

    override fun findAnonymousSpace(anonymousUser: User):Space {
        return collection.findOne(Space::userAssignments elemMatch (and(Space.UserAssignment::user_id eq anonymousUser._id, Space.UserAssignment::role eq Owner )))!!
    }

    @RequireRootRole(Admin)
    override fun create(space: Space): Space {
        collection.insertOne(space)
        val user = context.getUser()
        createUserAssignment(space._id, Space.UserAssignment(user_id = user._id, role = Space.Role.Owner, settings = Space.Settings("")))
        context.activity(Space::class, space._id, space.name, space._id, Activity.Type.Created)
        return space
    }

    @RequireRole(Owner)
    @RequireRootRole(Admin)
    override fun update(spaceId: Id<CoreSpace>, space: Space): Space {
        val original = collection.findOneById(spaceId)!!

        space.userAssignments = original.userAssignments
        space.groups = original.groups
        space.availableApplications_id = original.availableApplications_id

        collection.updateOneById(spaceId, space)

        return space
    }

    @RequireRootRole(Admin)
    override fun delete(spaceId: Id<CoreSpace>) {
        collection.deleteOneById(spaceId)
    }

    @RequireRole(Consumer)
    override fun get(spaceId: Id<CoreSpace>): Space {
        val space = collection.find(Space::_id eq spaceId).singleOrNull()
                ?: throw NotFoundException("Space $spaceId does not exist")

        for (userAssignment in space.userAssignments) {
            populateUserAssignment(userAssignment)
        }
        return space
    }

    @RequireRole(Editor)
    override fun getVersions(spaceId: Id<CoreSpace>) = listOf(Version("Core", getCoreVersion(spaceId)))

    private fun getCoreVersion(spaceId: Id<CoreSpace>) = coreService.call<Check>(spaceId, "/check").version

    @RequireRole(Maintainer)
    override fun getUsers(spaceId: Id<CoreSpace>, query: String?) =
        when {
            query?.startsWith("anonym") == true -> {
                users.find(User::name eq query).toList()
            }
            query?.startsWith("Anonym ") == true -> {
                val (name, surname) = query.split(" ")
                users.find(User::name eq name, User::surname eq surname).toList()
            }
            else -> {
                mutableListOf<User>().apply {
                    if (query != null)
                        addAll(users.find(User::username eq query).toList())
                    addAll(users.find(User::_id `in` get(spaceId).userAssignments.map { it.user_id }).toList().filter {
                        if (query != null)
                            it.name.indexOf(query, ignoreCase = true) >= 0 || it.surname.indexOf(query, ignoreCase = true) >= 0
                        else
                            true
                    })
                }
            }
        }

    @RequireRole(Maintainer)
    override fun createUser(spaceId: Id<CoreSpace>, user: User): User {
        user.space_id = spaceId
        userRepository.create(user)
        context.activity(User::class, user._id, user.username, spaceId, Activity.Type.Created)
        return user
    }

    @RequireRootRole(Admin)
    override fun joinCurrentUser(spaceId: Id<CoreSpace>): Space.UserAssignment {
        val space = get(spaceId)
        val currentUser = context.getUser()
        val userAssignment = space.userAssignments.find { it._user == currentUser }?.apply {
            error("Current user is already assigned to role ${role}")
        } ?: Space.UserAssignment(currentUser._id, Admin, "Joined from ROOT")
        space.userAssignments.add(userAssignment)
        collection.updateOneById(spaceId, space)
        return userAssignment
    }

    @RequireRole(Consumer)
    override fun leave(spaceId: Id<CoreSpace>) {
        val space = get(spaceId)
        val currentUser = context.getUser()
        val roles = userRepository.findRolesForUser(currentUser._id as Id<User>)
        val userAssignment =
            space.userAssignments.firstOrNull { it._user == currentUser } ?: error("User has no role in this space.")

        val afterRemove = space.userAssignments - userAssignment

        when {
            roles.size == 1 -> error("Can not leave space, user must have at least one space.")
            afterRemove.isEmpty() -> error("Can not remove last user in space.")
            !afterRemove.any { it.role == Owner } -> error("Can not remove last space owner.")
        }

        space.userAssignments = afterRemove.toMutableList()
        collection.updateOneById(spaceId, space)
    }

    @RequireRole(Maintainer)
    override fun getUserAssignments(spaceId: Id<CoreSpace>): List<Space.UserAssignment> {

        return collection.aggregate<Space.UserAssignment>(
            match(Space::_id eq spaceId),
            unwind("\$" + Space::userAssignments.name),
            replaceRoot(Space::userAssignments),

            context.query.seek_id?.let { match(Space.UserAssignment::user_id gt it.toId()) } ?: match(),
            match(MongoFiltersFactory.createFilter(Space.UserAssignment::class, context.query)),
            sort(orderBy(Space.UserAssignment::user_id)),
            limit(context.query.limit),
            lookup(
                User::class.simpleName!!.lowercase(),
                Space.UserAssignment::user_id.name,
                User::_id.name,
                Space.UserAssignment::_user.name
            ),
            unwind("\$" + Space.UserAssignment::_user.name)
        ).toList()
    }

    // UserAssignments

    @RequireRole(Maintainer)
    override fun getUserAssignment(spaceId: Id<CoreSpace>, userAssignmentId: Id<Space.UserAssignment>): Space.UserAssignment {
        val userAssignment = get(spaceId).userAssignments.find { t: Space.UserAssignment -> t.user_id == userAssignmentId }
                ?: throw NotFoundException("UserAssignment $userAssignmentId does not exist")

        populateUserAssignment(userAssignment)

        return userAssignment
    }

    @RequireRole(Admin)
    override fun createUserAssignment(spaceId: Id<CoreSpace>, userAssignment: Space.UserAssignment): Space.UserAssignment {
        val space = get(spaceId)

        space.userAssignments.removeIf { t: Space.UserAssignment -> t.user_id == userAssignment.user_id }
        space.userAssignments.add(userAssignment)

        collection.updateOneById(spaceId, space)
        context.activity(Space.UserAssignment::class, userAssignment.user_id, userAssignment._user?.username ?: "?", spaceId, Activity.Type.Created)

        return userAssignment
    }

    @RequireRole(Maintainer)
    override fun addApplicationForUser(spaceId: Id<CoreSpace>, userId: Id<CoreUser>, applicationId: Id<CoreApplication>): Space.UserAssignment {
        val space = get(spaceId)

        val userAssignment: Space.UserAssignment
        space.userAssignments.find { assignment: Space.UserAssignment -> assignment.user_id == userId }!!
                .apply { this.applications_id.add(applicationId) }
                .let { userAssignment = it}

        collection.updateOneById(spaceId, space)

        return userAssignment
    }

    @RequireRole(Maintainer)
    override fun getApplicationsForUserInSpace(spaceId: Id<CoreSpace>, userId: Id<CoreUser>): List<Application> {
        val space = get(spaceId)
        val userAssignment = space.userAssignments.singleOrNull { it.user_id == userId }
        val apps = mutableListOf<Application>()
        if (userAssignment != null) {
            for (id in userAssignment.applications_id) {
                apps.add(applicationRepository.get(id as Id<Application>))
            }
        }
        return apps
    }

    @RequireRole(Maintainer)
    override fun deleteApplicationForUserInSpace(spaceId: Id<CoreSpace>, userId: Id<CoreUser>, applicationId: Id<CoreApplication>): Space {
        val space = get(spaceId)
        space.userAssignments.replaceAll { t: Space.UserAssignment -> if (t.user_id == userId) t.apply { this.applications_id.remove(applicationId) } else t }
        update(spaceId, space)
        return space
    }

    @RequireRole(Admin)
    override fun updateUserAssignment(spaceId: Id<CoreSpace>, userAssignmentId: Id<Space.UserAssignment>, userAssignment: Space.UserAssignment): Space.UserAssignment {
        val space = get(spaceId)
        space.userAssignments.removeIf { t: Space.UserAssignment -> t.user_id == userAssignmentId }
        space.userAssignments.add(userAssignment)

        collection.updateOneById(spaceId, space)
        populateUserAssignment(userAssignment)
        context.activity(Space.UserAssignment::class, userAssignmentId, userAssignment._user?.username ?: "?", spaceId, Activity.Type.Updated)
        return userAssignment
    }

    @RequireRole(Admin)
    override fun deleteUserAssignment(spaceId: Id<CoreSpace>, userAssignmentId: Id<Space.UserAssignment>) {
        val space = get(spaceId)
        space.userAssignments.removeIf { t: Space.UserAssignment -> t.user_id == userAssignmentId }
        collection.updateOneById(spaceId, space)
        context.activity(Space.UserAssignment::class, userAssignmentId, userAssignmentId.toString(), spaceId, Activity.Type.Deleted)
    }

    private fun populateUserAssignment(userAssignment: Space.UserAssignment) {
        userAssignment._user = userRepository.get(userAssignment.user_id as Id<User>)
    }

    // Invitations

    @RequireRole(Maintainer)
    override fun getInvitations(spaceId: Id<CoreSpace>): List<Invitation> {
        return invitations
            .find(MongoFiltersFactory.createFilter(Invitation::class, context.query), Invitation::space_id eq spaceId)
            .limit(context.query.limit)
            .toList()
    }

    @RequireRole(Admin)
    override fun getInvitation(spaceId: Id<CoreSpace>, invitationId: Id<Invitation>): Invitation {
        return invitations.findOne(
                Invitation::space_id eq spaceId,
                Invitation::_id eq invitationId
        ) ?: throw NotFoundException("Invitation $invitationId does not exists")
    }

    @RequireRole(Admin)
    override fun createInvitation(spaceId: Id<CoreSpace>, invitation: Invitation): Invitation {
        invitation.space_id = spaceId
        invitations.insertOne(invitation)
        context.activity(Invitation::class, invitation._id, invitation.username, spaceId, Activity.Type.Created)
        invitationSender.sendInvitation(invitation)

        return invitation
    }

    @RequireRole(Admin)
    override fun resendInvitation(spaceId: Id<CoreSpace>, invitationId: Id<Invitation>): Invitation {
        val invitation: Invitation = invitations.findOne(Invitation::_id eq invitationId)!!
        invitationSender.sendInvitation(invitation)

        return invitation
    }

    @RequireRole(Admin)
    override fun deleteInvitation(spaceId: Id<CoreSpace>, invitationId: Id<Invitation>) {
        invitations.deleteOne(
                Invitation::_id eq invitationId,
                Invitation::space_id eq spaceId
        )
        context.activity(Invitation::class, invitationId, invitationId.toString(), spaceId, Activity.Type.Deleted)
    }

    // UserGroups

    @RequireRole(Maintainer)
    override fun getUserGroupsInSpace(spaceId: Id<CoreSpace>): List<Space.UserGroup> {
        return get(spaceId).groups
    }

    @RequireRole(Maintainer)
    override fun createUserGroupInSpace(spaceId: Id<CoreSpace>, userGroup: Space.UserGroup): Space.UserGroup {
        val space = get(spaceId)
        if (space.groups.count { t: Space.UserGroup -> t.name == userGroup.name } != 0)
            throw BadRequestException("User group ${userGroup.name} already exists in space")

        collection.updateOne(Space::_id eq spaceId, push(Space::groups, userGroup))

        return userGroup
    }

    @RequireRole(Maintainer)
    override fun getUserGroupInSpace(spaceId: Id<CoreSpace>, groupName: String): Space.UserGroup {
        val space = get(spaceId)
        return space.groups.find { t: Space.UserGroup -> t.name == groupName }
                ?: throw BadRequestException("User group ${groupName} does not exist")
    }

    @RequireRole(Maintainer)
    override fun updateUserGroupInSpace(spaceId: Id<CoreSpace>, groupName: String, userGroup: Space.UserGroup): Space.UserGroup {
        val space = get(spaceId)
        space.groups.removeIf { t: Space.UserGroup -> t.name == groupName  }
        space.groups.add(userGroup)

        collection.updateOneById(spaceId, space)

        return userGroup
    }

    @RequireRole(Maintainer)
    override fun deleteUserGroupInSpace(spaceId: Id<CoreSpace>, groupName: String) {
        val space = get(spaceId)
        space.groups.removeIf { t: Space.UserGroup -> t.name == groupName }
        collection.updateOneById(spaceId, space)
    }

    // Devices

    @RequireRole(Maintainer)
    override fun getDevicesInSpace(spaceId: Id<CoreSpace>): List<Device> {
        val devicesInSpace =
            devices.find(MongoFiltersFactory.createFilter(Device::class, context.query), context.spaceFilter(false))
                .limit(context.query.limit)
                .toList()
        devicesInSpace.forEach { populateUserInDevice(it) }

        return devicesInSpace
    }

    private fun populateUserInDevice(device: Device) {
        device._user = if (device.user_id != null ) userRepository.get(device.user_id!! as Id<User>) else null
    }

    @RequireRole(Maintainer)
    override fun getDeviceInSpace(spaceId: Id<CoreSpace>, deviceId: Id<Device>): Device {
        return devices.findOne(Device::space_id eq spaceId, Device::_id eq deviceId as Id<CoreDevice>)
                ?: throw NotFoundException("Device $deviceId not exist")
    }

    @RequireRole(Maintainer)
    override fun updateDeviceInSpace(spaceId: Id<CoreSpace>, deviceId: Id<Device>, device: Device): Device {
        val filter = and(Device::space_id eq spaceId, Device::_id eq deviceId as Id<CoreDevice>)

        devices.updateOne(filter, device)
        populateUserInDevice(device)
        context.activity(Device::class, deviceId, device.deviceId, spaceId, Activity.Type.Updated)
        return device
    }

    @RequireRole(Consumer)
    override fun pairDeviceInSpace(spaceId: Id<CoreSpace>, pairingCode: String): Device {
        val devicePairing = devicePairingResource.pairDevice(pairingCode)
        val device = Device(
                deviceId = devicePairing.deviceId,
                user_id = context.getUser()._id,
                space_id = spaceId,
                description = "Pairing code ${devicePairing.pairingCode} created ${devicePairing.date}"
        )
        return deviceResource.create(device)
    }

    // Simulations

    @RequireRole(Maintainer)
    override fun getSimulations(spaceId: Id<CoreSpace>): List<Simulation> {
        val pipeline = MongoFiltersFactory.createPipeline(Simulation::class, context.query)
        pipeline.add(0, match(Simulation::space_id eq spaceId))

        return simulations.aggregate(pipeline).toList()
    }

    @RequireRole(Maintainer)
    override fun createSimulationInSpace(spaceId: Id<CoreSpace>, simulation: Simulation): Simulation {
        simulation.space_id = spaceId
        simulations.insertOne(simulation)

        return simulation
    }

    @RequireRole(Maintainer)
    override fun updateSimulationInSpace(spaceId: Id<CoreSpace>, simulationId: Id<Simulation>, simulation: Simulation): Simulation {
        if (simulation.space_id != spaceId)
            throw NotFoundException("Simulation ${simulationId} does not belong to space $spaceId")
        simulations.updateOneById(simulationId, simulation)

        return simulation
    }

    @RequireRole(Maintainer)
    override fun deleteSimulation(spaceId: Id<CoreSpace>, simulationId: Id<Simulation>) {
        val simulation = simulations.findOne(
                Simulation::_id eq simulationId, Simulation::space_id eq spaceId)
                ?: throw NotFoundException("Simulation $simulationId does not belong to space $spaceId")

        simulations.deleteOneById(simulation._id)
    }

    // Sessions
    private fun getSessions(spaceId: Id<CoreSpace>, query: Query): List<Session> {
        val q = query.copy(filters = query.filters + Filter(Session::space_id.name, Operator.eq, spaceId.toString()))
        return coreWebTarget.path("/sessions").resourceMethod(SessionResource::find)
                .query(q).request().get(object : GenericType<List<Session>>() {})
    }

    @RequireRole(Maintainer)
    override fun getSessionsInSpace(spaceId: Id<CoreSpace>): List<Session> = getSessions(spaceId, context.query)

    @RequireRole(Maintainer)
    override fun getSessionInSpace(spaceId: Id<CoreSpace>, sessionId: String): Session {
        return getSessions(spaceId, Query(seek_id = null, filters = mutableListOf(Filter("sessionId", Operator.eq, sessionId)))).firstOrNull()
                ?: throw NotFoundException("Session $sessionId does not exist")
    }

    @RequireRole(Maintainer)
    override fun getDialogueEventsInSpace(spaceId: Id<CoreSpace>): List<DialogueEvent> {
        val q = context.query.copy(filters = context.query.filters + Filter(DialogueEvent::space_id.name, Operator.eq, spaceId.toString()))

        return coreWebTarget.path("/dialogueEvent").resourceMethod(DialogueEventResource::getDialogueEvents)
                .query(q).request().get(object : GenericType<List<DialogueEvent>>() {})
    }

    @RequireRole(Maintainer)
    override fun getInitiationsInSpace(spaceId: Id<CoreSpace>) =
            initiationResource.list(spaceId)

    @RequireRole(Maintainer)
    override fun createInitiationInSpace(spaceId: Id<CoreSpace>, initiation: Initiation): Initiation {
        val space = collection.find(Space::_id eq spaceId).singleOrNull()
                ?: throw NotFoundException("Space $spaceId does not exist")
        initiation.space_id = space._id
        if ((initiation.deviceType == Initiation.DeviceType.Phone) && (space.twilioAccountID == null || space.twilioAuthToken == null))
            error("Space ${space.name} has no Twilio credentials set")

        return initiationResource.create(initiation, space)
    }

    override fun reports(spaceId: Id<CoreSpace>): ReportResource =
        resourceContext.getResource(ReportResourceImpl::class.java)

    override fun activities(spaceId: Id<CoreSpace>): ActivityResource =
        resourceContext.getResource(ActivityResourceImpl::class.java)

    override fun dialogueSources(spaceId: Id<CoreSpace>): DialogueSourceResource =
        resourceContext.getResource(DialogueSourceResource::class.java)

    override fun dialogueMixins(spaceId: Id<CoreSpace>): DialogueMixinResource =
        resourceContext.getResource(DialogueMixinResource::class.java)

    override fun entityDatasets(spaceId: Id<CoreSpace>): EntityDatasetResource =
        resourceContext.getResource(EntityDatasetResource::class.java)

    override fun dialogueSnippets(spaceId: Id<CoreSpace>): DialogueSnippetResource =
        resourceContext.getResource(DialogueSnippetResourceImpl::class.java)

    override fun files(spaceId: Id<CoreSpace>): FileResource =
        resourceContext.getResource(FileResource::class.java)

    override fun communities(spaceId: Id<CoreSpace>): CommunityResource =
        resourceContext.getResource(CommunityResource::class.java)

    override fun profiles(spaceId: Id<CoreSpace>): ProfileResource =
        resourceContext.getResource(ProfileResource::class.java)

    override fun voices(spaceId: Id<CoreSpace>): VoiceResource =
        resourceContext.getResource(VoiceResourceImpl::class.java)

    override fun speechRecognizers(spaceId: Id<CoreSpace>): SpeechRecognizerResource =
        resourceContext.getResource(SpeechRecognizerResourceImpl::class.java)

    override fun terms(spaceId: Id<CoreSpace>): TermsResource =
        resourceContext.getResource(TermsResourceImpl::class.java)

    override fun tts(spaceId: Id<CoreSpace>): TtsResource =
        resourceContext.getResource(TtsResourceImpl::class.java)

    override fun applications(spaceId: Id<ai.flowstorm.core.model.Space>): ApplicationResource =
        resourceContext.getResource(ApplicationResourceImpl::class.java)
}
