package ai.flowstorm.studio.resources.space

import ai.flowstorm.core.builder.EntityDataset
import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.model.Space.Role.Editor
import ai.flowstorm.studio.resources.EntityDatasetResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.studio.services.CoreService
import ai.flowstorm.common.query.MongoFiltersFactory
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import com.mongodb.BasicDBObject
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Projections
import org.litote.kmongo.*
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.toList

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Admin)
@RequireRole(Editor)
class EntityDatasetResourceImpl : EntityDatasetResource, MongoAbstractEntityRepository<EntityDataset>(EntityDataset::class) {

    @Inject
    lateinit var context: StudioContext

    @Inject
    lateinit var coreService: CoreService

    override fun list(): List<EntityDataset> {
        return collection.find(MongoFiltersFactory.createFilter(EntityDataset::class, context.query), context.spaceFilter()).projection(Projections.exclude(EntityDataset::data.name)).limit(context.query.limit).toList()
    }

    override fun create(entityDataset: EntityDataset): EntityDataset {
        collection.insertOne(entityDataset)
        return entityDataset
    }

    override fun update(datasetId: Id<EntityDataset>, entityDataset: EntityDataset): EntityDataset {
        collection.updateOneById(datasetId, entityDataset)
        return entityDataset
    }

    override fun delete(entityDatasetId: Id<EntityDataset>) {
        get(entityDatasetId).let {
            coreService.builderResource(it.space_id).deleteEntityModel(it._id)
        }
        collection.deleteOneById(entityDatasetId)
    }

    override fun deleteSample(entityDatasetId: Id<EntityDataset>, sampleId: Int) {
        collection.updateOneById(entityDatasetId,
            BasicDBObject("\$unset", BasicDBObject("${EntityDataset::data.name}.$sampleId", ""))
        )
        collection.updateOneById(entityDatasetId,
            BasicDBObject("\$pull", BasicDBObject(EntityDataset::data.name, null))
        )
    }

    override fun train(entityDatasetId: Id<EntityDataset>) {
        get(entityDatasetId).let {
            coreService.builderResource(it.space_id).trainEntityModel(it.apply {
                if (context.rootSpace._id == space_id) {
                    space_id = null
                }
            })
        }
    }

    override fun modelStatus(entityDatasetId: Id<EntityDataset>) =
        get(entityDatasetId).let {
            coreService.builderResource(it.space_id).modelStatus(it._id)
        }

    override fun getDataSamples(datasetId: Id<EntityDataset>): List<EntityDataset.DataSample> {
        val dataset = collection.find(Filters.eq(EntityDataset::_id.name, datasetId))
                .projection(
                    Projections.slice(
                        EntityDataset::data.name, context.query.seek_id?.toInt() ?: 0,
                        if (context.query.limit >= 0) context.query.limit else Int.MAX_VALUE
                    )
                )
                .first() ?: throw NotFoundException("Dataset does not exist.")

        val dataFrom = context.query.seek_id?.toInt() ?: 0
        return dataset.data.mapIndexed {i, s -> EntityDataset.DataSample(s, i + dataFrom) }
    }

    override fun putDataSample(datasetId: Id<EntityDataset>, sampleId: Int, dataSample: EntityDataset.DataSample): EntityDataset.DataSample {
        if (sampleId < 0) {
            collection.updateOneById(datasetId,
                BasicDBObject("\$push", BasicDBObject(EntityDataset::data.name, dataSample.text))
            )
        } else {
            collection.updateOneById(datasetId,
                BasicDBObject("\$set", BasicDBObject("${EntityDataset::data.name}.$sampleId", dataSample.text))
            )
        }
        return dataSample
    }

    override fun find(query: Query): List<EntityDataset> = error("Not supported")

}