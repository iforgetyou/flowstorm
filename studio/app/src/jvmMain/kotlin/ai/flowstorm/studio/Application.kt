package ai.flowstorm.studio

import ai.flowstorm.studio.builder.Builder
import ai.flowstorm.studio.builder.BuilderV2
import ai.flowstorm.studio.mailing.ActiveCampaignService
import ai.flowstorm.studio.mailing.MailchimpService
import ai.flowstorm.studio.mailing.MailingService
import ai.flowstorm.studio.mailing.MailingServiceResolver
import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.repository.ApplicationRepository
import ai.flowstorm.studio.repository.DialogueSourceRepository
import ai.flowstorm.studio.repository.UserRepository
import ai.flowstorm.studio.resources.*
import ai.flowstorm.studio.resources.space.*
import ai.flowstorm.studio.resources.space.DialogueSourceResourceImpl
import ai.flowstorm.studio.security.ComposedJwtVerifier
import ai.flowstorm.studio.security.HMAC256Verifier
import ai.flowstorm.studio.security.RoleAuthorizationAdapter
import ai.flowstorm.studio.services.*
import ai.flowstorm.studio.services.CoreFactory.*
import ai.flowstorm.studio.util.AppHostnameBuilder
import ai.flowstorm.common.*
import ai.flowstorm.common.binding.MongoDatabaseFactory
import ai.flowstorm.common.config.Config
import ai.flowstorm.common.indexer.ElasticClientFactory
import ai.flowstorm.common.indexer.Elastic
import ai.flowstorm.common.messaging.MailgunSender
import ai.flowstorm.common.messaging.MessageSender
import ai.flowstorm.common.mongo.KMongoIdParamConverterProvider
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.common.monitoring.SentryMonitoring
import ai.flowstorm.common.monitoring.LoggerMonitoring
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.QueryValueFactory
import ai.flowstorm.common.security.AuthorizationAdapter
import ai.flowstorm.connector.mailgun.Contact
import ai.flowstorm.connector.mailgun.Mailgun
import ai.flowstorm.core.resources.ContentDistributionResource
import ai.flowstorm.core.resources.DevicePairingResource
import ai.flowstorm.core.resources.FileStorageResource
import ai.flowstorm.core.resources.TtsResource
import ai.flowstorm.security.jwt.JwtVerifier
import ai.flowstorm.security.jwt.NullJwtVerifier
import ai.flowstorm.security.jwt.RsaJwtVerifier
import ai.flowstorm.studio.services.search.SearchService
import com.mongodb.client.MongoDatabase
import org.glassfish.hk2.api.PerLookup
import org.glassfish.jersey.process.internal.RequestScoped
import org.elasticsearch.client.RestHighLevelClient
import javax.inject.Singleton
import javax.ws.rs.client.WebTarget
import javax.ws.rs.ext.ParamConverterProvider
import ai.flowstorm.studio.resources.space.CommunityResource as SpaceCommunityResource
import ai.flowstorm.studio.resources.space.CommunityResourceImpl as SpaceCommunityResourceImpl
import ai.flowstorm.core.resources.CommunityResource as CoreCommunityResource
import ai.flowstorm.core.resources.ProfileResource as CoreProfileResource
import ai.flowstorm.core.resources.ReportResource as CoreReportResource

class Application(config: Config) : JerseyApplication(config) {

    init {
        logger.info("Creating application (nameSuffix=${config.nameSuffix}, dataSuffix=${config.dataSuffix})")
        register(object : ResourceBinder() {
            override fun configure() {
                // resources
                (if (config.getOrNull("sentry.dsn") != null)
                    bind(SentryMonitoring::class.java)
                else
                    bind(LoggerMonitoring::class.java)).to(Monitoring::class.java).`in`(Singleton::class.java)
                bindTo(ClientResource::class.java, ClientResourceImpl::class.java)
                bindTo(CurrentUserResource::class.java, CurrentUserResourceImpl::class.java)
                bindTo(SpaceResource::class.java, SpaceResourceImpl::class.java)
                bindTo(DeviceResource::class.java, DeviceResourceImpl::class.java)
                bindTo(InvitationResource::class.java, InvitationResourceImpl::class.java)
                bindTo(SessionResource::class.java, SessionResourceImpl::class.java)
                bindTo(InitiationResource::class.java, InitiationResourceImpl::class.java)
                bindTo(DialogueEventResource::class.java, DialogueEventResourceImpl::class.java)
                bindTo(DialogueSourceResource::class.java, DialogueSourceResourceImpl::class.java)
                bindTo(DialogueMixinResource::class.java, DialogueMixinResourceImpl::class.java)
                bindTo(FileResource::class.java, FileResourceImpl::class.java)
                bindTo(EntityDatasetResource::class.java, EntityDatasetResourceImpl::class.java)
                bindTo(ContentDistributionResource::class.java, ContentDistributionResourceImpl::class.java)
                bindTo(CommunityResource::class.java, CommunityResourceImpl::class.java)
                bindTo(SpaceCommunityResource::class.java, SpaceCommunityResourceImpl::class.java)
                bindTo(ProfileResource::class.java, ProfileResourceImpl::class.java)
                bindTo(AccountResource::class.java, AccountResourceImpl::class.java)
                bindTo(SearchResource::class.java, SearchResourceImpl::class.java)


                bindFactory(MongoDatabaseFactory::class.java).to(MongoDatabase::class.java).`in`(Singleton::class.java)

                // builder
                bindFactory(BuilderV2.Factory::class.java).to(Builder::class.java).named(DialogueSource.V2)

                val namespace = config.get("dataSuffix", config.namespace)
                val illusionistUrl = ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.illusionist, nameSuffix = config.dataSuffix)
                val matrixResourceImpl = MatrixResourceImpl(illusionistUrl, config["illusionist.apiKey"])

                bind(matrixResourceImpl).to(MatrixResource::class.java)

                //services
                val mailgun = Mailgun.Builder(config["mailgun.domain"], config["mailgun.apikey"])
                        .baseUrl(config["mailgun.baseUrl"])
                        .build()
                bindTo(MessageSender::class.java, MailgunSender(mailgun, Contact(config["sender.from.email"], config["sender.from.name"])))

                bind(MailchimpService::class.java).to(MailingService::class.java).`in`(Singleton::class.java)
                bind(ActiveCampaignService::class.java).to(MailingService::class.java).`in`(Singleton::class.java)
                bindAsContract(MailingServiceResolver::class.java).`in`(Singleton::class.java)

                bindAsContract(InvitationSender::class.java)
                bindAsContract(AppHostnameBuilder::class.java)
                bindAsContract(CoreService::class.java).`in`(Singleton::class.java)

                bind(RoleAuthorizationAdapter::class.java).to(AuthorizationAdapter::class.java).`in`(RequestScoped::class.java)

                // core resources
                bindFactory(WebTargetFactory::class.java, RequestScoped::class.java).to(WebTarget::class.java)
                bindFactory(FileStorageResourceFactory::class.java, RequestScoped::class.java).to(FileStorageResource::class.java)
                bindFactory(ProfileResourceFactory::class.java, RequestScoped::class.java).to(CoreProfileResource::class.java)
                bindFactory(ReportResourceFactory::class.java, RequestScoped::class.java).to(CoreReportResource::class.java)
                bindFactory(DevicePairingResourceFactory::class.java, RequestScoped::class.java).to(DevicePairingResource::class.java)
                bindFactory(CommunityResourceFactory::class.java,RequestScoped::class.java).to(CoreCommunityResource::class.java)
                bindFactory(TtsResourceFactory::class.java).to(TtsResource::class.java).`in`(PerLookup::class.java)

                bindAsContract(UserRepository::class.java).`in`(Singleton::class.java)
                bindAsContract(ApplicationRepository::class.java).`in`(Singleton::class.java)
                bindAsContract(DialogueSourceRepository::class.java).`in`(Singleton::class.java)
                bindAsContract(UserService::class.java)
                bindAsContract(SearchService::class.java)

                //JWT
                val verifier = if (config["jwtauth.verify"] == "false")
                    NullJwtVerifier()
                else
                    ComposedJwtVerifier(
                        RsaJwtVerifier(config["jwtauth.issuer"]),
                        HMAC256Verifier(config["jwt.issuer"], config["jwt.secret"])
                    )

                bindTo(JwtVerifier::class.java, verifier)

                bind(KMongoIdParamConverterProvider::class.java).to(ParamConverterProvider::class.java).`in`(Singleton::class.java)

                bindFactory(QueryValueFactory::class.java).to(Query::class.java).`in`(PerLookup::class.java)

                bindAsContract(StudioContext::class.java).`in`(RequestScoped::class.java)

                bindAsContract(Elastic::class.java).`in`(Singleton::class.java)
                config.getOrNull("es.host")?.let {
                    bindFactory(ElasticClientFactory::class.java).to(RestHighLevelClient::class.java).`in`(Singleton::class.java)
                }
            }
        })
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            val serverApplication = Application(AppConfig)
            JettyServer.run(serverApplication, serverApplication)
        }
    }
}
