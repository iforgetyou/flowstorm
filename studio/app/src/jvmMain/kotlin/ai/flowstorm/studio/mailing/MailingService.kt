package ai.flowstorm.studio.mailing

import ai.flowstorm.studio.filters.AuthenticationFilter
import ai.flowstorm.studio.model.User
import ai.flowstorm.common.client.RestClient
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.util.LoggerDelegate
import java.net.URL
import javax.ws.rs.core.HttpHeaders

abstract class MailingService {

    enum class Method { GET, PUT, POST }

    abstract val name: String

    open fun getAuthenticationHeader() = HttpHeaders.AUTHORIZATION to "${AuthenticationFilter.SCHEME} $apiKey"

    @ConfigValue("mailing.apiKey", "")
    lateinit var apiKey: String

    protected val logger by LoggerDelegate()

    protected inline fun <reified R : Any> call(url: URL, method: Method = Method.GET, output: Any? = null) =
        if (apiKey.isBlank()) {
            logger.warn("Cannot call $url (missing apiKey)")
            null
        } else {
            logger.info("Calling $method $url - $output")
            RestClient.call(url, R::class.java, method.name, mapOf(getAuthenticationHeader()), output)
        }
    abstract fun createContact(user: User): Any?
}