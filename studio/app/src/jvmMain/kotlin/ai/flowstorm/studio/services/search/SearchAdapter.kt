package ai.flowstorm.studio.services.search

import ai.flowstorm.studio.model.SearchRequest
import ai.flowstorm.studio.model.SearchType
import org.bson.Document
import org.bson.conversions.Bson
import org.bson.types.ObjectId

abstract class SearchAdapter(val sr: SearchRequest) {

    fun getCollectionName(): String = sr.entity
    fun getSearchIndex(): String = getCollectionName() + "_search"

    protected open fun getIdFieldsPaths(): List<String> = listOf()

    private fun fulltextSearch(): Document {
        return Document()
            .append(
                "\$search",
                Document()
                    .append("index", getSearchIndex())
                    .append("compound", Document()
                        .append("must", mutableListOf<Document>().apply {
                            if (sr.id != null) {
                                getIdFieldsPaths().forEach {
                                    add(
                                        Document(
                                            "equals", Document()
                                                .append("value", sr.id)
                                                .append("path", it)
                                        )
                                    )
                                }

                            } else {
                                add(
                                    Document(
                                        "text", Document()
                                            .append("query", sr.search)
                                            .append("path", Document("wildcard", "*"))
                                    )
                                )
                            }

                            add(
                                Document(
                                    "equals", Document()
                                        .append("value", ObjectId(sr.spaceId.toString()))
                                        .append("path", "space_id")
                                )
                            )
                        })
                    )
                    .append(
                        "highlight", Document().append("path", Document("wildcard", "*"))
                    )
                    .append("returnStoredSource", true)
            )
    }

    fun searchStage(): Bson = when (sr.type) {
        SearchType.Fulltext -> fulltextSearch()
        SearchType.Exact -> exactSearch()
    }

    protected abstract fun exactSearch(): Bson
    abstract fun projectionStage(): Bson
    abstract fun filterStage(): Bson

}
