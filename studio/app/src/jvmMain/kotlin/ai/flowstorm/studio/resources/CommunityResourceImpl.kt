package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.common.client.resourceMethod
import javassist.NotFoundException
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.model.Community
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.NotAllowedException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import ai.flowstorm.core.resources.CommunityResource as CoreCommunityResource

@Path("/communities")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@Authorized
@RequireRootRole(Admin)
@RequireRole(Admin)
class CommunityResourceImpl : CommunityResource {

    @Inject
    lateinit var coreWebTarget: WebTarget

    @Inject
    lateinit var communityResource: CoreCommunityResource

    @Inject
    lateinit var query: Query

    override fun getCommunities(): List<Community> {
        val communities = coreWebTarget.path("/communities").resourceMethod(CoreCommunityResource::getCommunities)
                .query(query).request().get(object : GenericType<List<Community>>() {})

        return communities
    }

    override fun getCommunitiesInSpace(spaceId: String): List<Community> {
        throw NotAllowedException("Filtering by space not allowed here.")
    }

    override fun get(communityName: String, spaceId: String): Community {
        return communityResource.get(communityName, spaceId)
                ?: throw NotFoundException("Community $communityName does not exist")
    }

    override fun create(community: Community) {
        communityResource.create(community)
    }

    override fun update(community: Community) {
        communityResource.update(community)
    }
}