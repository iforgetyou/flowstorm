package ai.flowstorm.studio.mailing

import org.glassfish.hk2.api.IterableProvider
import javax.inject.Inject

class MailingServiceResolver {

    @Inject
    lateinit var boundServices: IterableProvider<MailingService>

    fun get(name: String) = boundServices.find { it.name == name } ?: error("Unknown mailing service: $name")
}