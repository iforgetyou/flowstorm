package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.repository.ApplicationRepository
import ai.flowstorm.studio.repository.UserRepository
import ai.flowstorm.studio.resources.CurrentUserResource.SignUpParams
import com.mongodb.client.MongoDatabase
import ai.flowstorm.studio.services.StudioContext
import ai.flowstorm.studio.services.UserService
import ai.flowstorm.studio.mailing.MailingServiceResolver
import ai.flowstorm.common.config.ConfigValue
import org.litote.kmongo.getCollection
import org.litote.kmongo.regex
import ai.flowstorm.security.Authenticated
import kotlinx.datetime.Clock
import org.bson.types.ObjectId
import org.litote.kmongo.Id
import org.litote.kmongo.id.toId
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.LinkedHashSet

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
class CurrentUserResourceImpl : CurrentUserResource {

    @ConfigValue("mailing.type", "")
    lateinit var mailingServiceName: String

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var userService: UserService

    @Inject
    lateinit var applicationRepository: ApplicationRepository

    @Inject
    lateinit var spaceResource: SpaceResource

    @Inject
    lateinit var accountResource: AccountResource

    @Inject
    lateinit var database: MongoDatabase

    @Inject
    lateinit var mailingServiceResolver: MailingServiceResolver

    @Inject
    lateinit var context: StudioContext

    private val invitations by lazy { database.getCollection<Invitation>() }

    override fun getUser(): User = context.getUser().also {
        context.activity(it::class, it._id, it.username, ObjectId(context.rootSpaceId).toId(), Activity.Type.Used)
    }

    override fun update(user: User): User = userRepository.updateById(context.getUser()._id as Id<User>, user)

    private fun isUserAuthenticated() = context.isUserAuthenticated()

    override fun getUserRoles(): List<UserSpaceRole> {
        val user = context.getUser()

        return userRepository.findRolesForUser(user._id as Id<User>)
    }

    override fun getSpaces(): List<Space> {
        val user = context.getUser()
        return (userRepository.findSpacesForUser(user._id as Id<User>) union listOf(context.rootSpace)).toList()
    }

    override fun getInvitations(): List<Invitation> {
        val regex = "^${context.getUser().username}$".replace(".", "\\.").toRegex(RegexOption.IGNORE_CASE)
        return invitations.find(Invitation::username.regex(regex)).toList()
    }

    private fun getPublicApplications() = applicationRepository.findPublic().toHashSet()

    @Authenticated(false)
    override fun getApplications() =
        if (isUserAuthenticated()) {
            val user = context.getUser()
            val apps = LinkedHashSet(userRepository.findApplicationsForUser(user._id as Id<User>).toHashSet())
            if (!user.excludePublicApps)
                apps.addAll(getPublicApplications())
            apps
        } else {
            getPublicApplications()
        }


    override fun signUpAnonymous() {
        val user = userService.createFromToken()
        userRepository.create(user)
        val account = accountResource.getAnonymousAccount()
        val space = Space(name = user.fullName, account_id = account._id)
        spaceResource.create(space)
    }

    override fun signUp(params: SignUpParams) {
        val existingAnonymousUser = params.deviceId?.let { userRepository.findByUsername(it) }

        val user = if (existingAnonymousUser == null) {
            userService.createFromToken()
        } else {
            userService.updateFromToken(existingAnonymousUser)
            existingAnonymousUser
        }

        if (user.name == User.DEFAULT_ANONYMOUS_NAME) user.name = User.DEFAULT_NAME

        if (user.agreement == null) {
            user.agreement = Clock.System.now()
        }
        userRepository.updateById(user._id as Id<User>, user)

        val account = Account(name = user.fullName, user_id = user._id)
        accountResource.create(account)

        if (existingAnonymousUser == null) {
            spaceResource.create(Space(name = account.name, account_id = account._id))
        } else {
            spaceResource.findAnonymousSpace(existingAnonymousUser).let {
                it.account_id = account._id
                it.name = "${user.name} ${user.surname}".trim()
                spaceResource.update(it._id, it)

            }
        }

        if (mailingServiceName.isNotBlank()) {
            mailingServiceResolver.get(mailingServiceName).createContact(user)
        }
    }
}