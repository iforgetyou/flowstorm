package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.model.Space.Role.Maintainer
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.common.client.resourceMethod
import org.litote.kmongo.Id
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.core.model.Community
import ai.flowstorm.core.model.Space
import ai.flowstorm.security.Authenticated
import ai.flowstorm.core.resources.CommunityResource as CoreCommunityResource
import javax.inject.Inject
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Admin)
@RequireRole(Maintainer)
class CommunityResourceImpl : CommunityResource {

    @Inject
    lateinit var query: Query

    @Suppress("UnresolvedRestParam")
    @PathParam("spaceId")
    lateinit var spaceId: Id<Space>

    @Inject
    lateinit var coreWebTarget: WebTarget

    override fun getCommunities(): List<Community> {
        val webTarget = coreWebTarget.path("/communities").resourceMethod(CoreCommunityResource::getCommunitiesInSpace).resolveTemplate("spaceId", spaceId)
        val communities = webTarget.query(query).request().get(object : GenericType<List<Community>>() {})

        return communities ?: listOf()
    }

    override fun getCommunity(communityName: String): Community {
        error("Not implemented")
    }

    override fun createCommunity(community: Community): Community {
        error("Not implemented")
    }

    override fun updateCommunity(communityName: String, community: Community): Community {
        error("Not implemented")
    }
}
