package ai.flowstorm.studio.builder

import ai.flowstorm.core.model.DialogueSourceCode
import ai.flowstorm.core.model.DialogueSourceCode.*
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.runtime.DialogueSecurityManager
import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.core.model.SttConfig
import ai.flowstorm.security.Digest
import ai.flowstorm.util.LoggerDelegate
import kotlinx.datetime.Clock
import java.io.ByteArrayOutputStream
import java.io.FileInputStream
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.StandardCharsets

class DialogueSourceCodeBuilder(
    private val dialogueId: String,
    val name: String,
    private val version: Int,
    val language: String,
    val contextClass: String
) {
    var properties: PropertyMap = mapOf()
    var initCode: CharSequence = ""
    var parameters: String = ""
    var extensionCode: CharSequence = ""
    var parentClass: String? = null
    private val buildId = "id" + Digest.md5()
    private val className: String
    private val source = StringBuilder()
    private val logger by LoggerDelegate()
    private val names: MutableList<String>

    init {
        if (!name.matches(Regex("([\\w\\-]+)/([\\w\\-]+)")))
            error("dialogue name $name does not conform to naming convention (product-name/dialogue-name)")
        names = name.split("/").toMutableList()
        className = "Model"// + names.removeAt(names.size - 1)
    }

    fun build(): DialogueSourceCode {
        if (parentClass == null) {
            parentClass = when (language) {
                "en" -> "BasicEnglishDialogue"
                "cs" -> "BasicCzechDialogue"
                else -> "BasicDialogue"
            }
        }

        logger.info("Building source code for dialogue $name class $className : $parentClass<$contextClass>")

        source.clear()
        writeHeader()
        writeClassSignature()
        writeInit()

        with(nodes) {
            filterIsInstance<GoBack>().forEach { write(it) }
            filterIsInstance<Sleep>().forEach { write(it) }
            filterIsInstance<GlobalIntent>().forEach { write(it) }
            filterIsInstance<Intent>().forEach { write(it) }
            filterIsInstance<Action>().forEach { write(it) }
            filterIsInstance<GlobalAction>().forEach { write(it) }
            filterIsInstance<UserInput>().forEach { write(it) }
            filterIsInstance<ReInput>().forEach { write(it) }
            filterIsInstance<Speech>().forEach { write(it) }
            filterIsInstance<Image>().forEach { write(it) }
            filterIsInstance<Sound>().forEach { write(it) }
            filterIsInstance<Command>().forEach { write(it) }
            filterIsInstance<DialogueSourceCode.Function>().forEach { write(it) }
            filterIsInstance<SubDialogue>().forEach { write(it) }
        }

        writeTransitions()

        source.appendLine('}')
        if (extensionCode.isNotBlank()) {
            source.append(extensionCode)
        }
        logger.info("Built source code for dialogue $name")
        println(source.toString())

        return DialogueSourceCode(dialogueId, name, version, className, buildId, mapOf(), source.toString())
    }

    private val nodes = mutableListOf<Node>()
    private val transitions = mutableMapOf<String, String>()

    fun addNode(node: Node) = nodes.add(node)
    fun addTransition(transition: Pair<String, String>) = transitions.put(transition.first, transition.second)

    private fun writeHeader() = with (source) {
        appendLine("//--dialogue-model;name:$name;time:" + Clock.System.now().toString())
        appendLine("package model.$buildId")
        appendLine()
        DialogueSecurityManager.importedPackages.forEach {
            appendLine("import $it.*")
        }
        appendLine()
    }

    private fun StringBuilder.appendSource(sttConfig: SttConfig) = with (sttConfig) {
        append("""SttConfig("$provider", $maxSilence, $maxPause)""")
    }

    private fun StringBuilder.appendSource(ttsConfig: TtsConfig) = with (ttsConfig) {
        append("""TtsConfig("$provider", java.util.Locale("${locale.language}", "${locale.country}"), TtsConfig.Gender.${gender.name}, "$name"""")
        if (engine != null)
            append(",\"").append(engine).append('"')
        append(')')
    }

    private fun writeInit() {
        source.appendLine("\toverride val dialogueId = \"$dialogueId\"")
        source.appendLine("\toverride val buildId = \"$buildId\"")
        source.appendLine("\toverride val dialogueName = \"$name\"")
        source.appendLine("\toverride val language = \"$language\"")
        source.appendLine("\toverride val version = $version")

        properties.forEach {
            val className = it.value::class.simpleName
            source.append("\toverride val ${it.key}: $className = ")
            when (it.value) {
                is String -> {
                    source.append('"').append((it.value as String).trim().replace("\"", "\\\"")).append('"')
                }
                is Enum<*> -> with (it.value as Enum<*>) {
                    source.append(className).append('.').append(name)
                }
                is SttConfig -> source.appendSource(it.value as SttConfig)
                is TtsConfig -> source.appendSource(it.value as TtsConfig)
                else -> source.append(it.value.toString())
            }
            source.appendLine()
        }

        if (initCode.isNotBlank()) {
            source.appendLine("//--code-start;type:init")
            source.appendLine(initCode.replace(Regex("//\\#include ([^\\s]+)")) {
                val path = it.groupValues[1]
                (if (path.startsWith("http://") || path.startsWith("https://")) {
                    val conn = URL(path).openConnection() as HttpURLConnection
                    if (conn.responseCode != 200)
                        error("include from url $path returned code ${conn.responseCode}")
                    else
                        conn.inputStream
                } else {
                    FileInputStream(path)
                }).use { input ->
                    val buf = ByteArrayOutputStream()
                    input.copyTo(buf)
                    "//--include-start;url:$path\n" + String(buf.toByteArray(), StandardCharsets.UTF_8) + "\n//--include-end\n"
                }
            })
            source.appendLine("//--code-end;type:init")
        }
    }

    private fun writeClassSignature() =
        source.appendLine("class $className($parameters) : $parentClass<$contextClass>() {")

    private fun write(goBack: GoBack) = with(goBack) {
        source.appendLine("\tval $nodeName = GoBack($nodeId, $repeat)")
    }

    private fun write(sleep: Sleep) = with(sleep) {
        source.appendLine("\tval $nodeName = Sleep($nodeId, $timeout)")
    }

    private fun write(intent: Intent) = with(intent) {
        source.append("\tval $nodeName = Intent($nodeId, \"$nodeName\", ${threshold}F, " +
                "listOf(${(extractEntities(utterances) + extractEntities(negativeUtterances)).joinToString(", ") { "\"" + it + "\"" }}), " +
                "listOf(${entities.joinToString(", ") { "\"" + it + "\"" }}), ")
        source.append("listOf(${utterances.joinToString(", ") { "\"" + it.trim().replace("\"", "\\\"") + "\"" }}), ")
        source.append("listOf(${negativeUtterances.joinToString(", ") { "\"" + it.trim().replace("\"", "\\\"") + "\"" }})")
        source.appendLine(')')
    }

    private fun write(intent: GlobalIntent) = with(intent) {
        source.append("\tval $nodeName = GlobalIntent($nodeId, \"$nodeName\", ${threshold}F, " +
                "listOf(${(extractEntities(utterances) + extractEntities(negativeUtterances)).joinToString(", ") { "\"" + it + "\"" }}), " +
                "listOf(${entities.joinToString(", ") { "\"" + it + "\"" }}), ")
        source.append("listOf(${utterances.joinToString(", ") { "\"" + it.trim().replace("\"", "\\\"") + "\"" }}), ")
        source.append("listOf(${negativeUtterances.joinToString(", ") { "\"" + it.trim().replace("\"", "\\\"") + "\"" }})")
        source.appendLine(')')
    }

    private fun extractEntities(utterances: List<String>) =
        utterances.map { utterance ->
            Regex("""\{([^\{\}]+)\}""").findAll(utterance).map { it.destructured.toList().firstOrNull() }.filterNotNull().toList()
        }.flatten().toSet()

    private fun write(userInput: UserInput) = with(userInput) {
        source.append("\tval $nodeName: UserInput = UserInput($nodeId, $skipGlobalIntents, ")
        if (sttConfig != null)
            source.appendSource(sttConfig!!)
        else
            source.append("null")
        val intents  = intentNames.joinToString(", ")
        val actions = actionNames.joinToString(", ")
        val expectedPhrases = this.expectedPhrases.toString().let { s ->
            if (s.isBlank())
                ""
            else
                s.replace('\n', ',')
                    .split(',').joinToString(", ") { """ExpectedPhrase("${it.trim()}")""" }
        }
        source.appendLine(", listOf($expectedPhrases), arrayOf($intents), arrayOf($actions) ) {")
        transitions.forEach { source.appendLine("\t\tval ${it.key} = Transition(${it.value})") }
        source.appendLine("//--code-start;type:userInput;name:$nodeName")
        if (code.isNotEmpty()) {
            source.appendLine(code)
        } else {
            source.appendLine("\t\tpass")
        }
        source.appendLine("//--code-end;type:userInput;name:$nodeName").appendLine("\t}")
    }

    private fun write(reInput: ReInput) = with(reInput) {
        val intents  = intentNames.joinToString(", ")
        val actions = actionNames.joinToString(", ")
        source.appendLine("\tval $nodeName: ReInput = ReInput($nodeId, $skipGlobalIntents, arrayOf($intents), arrayOf($actions)) {")
        transitions.forEach { source.appendLine("\t\tval ${it.key} = Transition(${it.value})") }
        source.appendLine("//--code-start;type:reInput;name:$nodeName")
        if (code.isNotEmpty()) {
            source.appendLine(code)
        } else {
            source.appendLine("\t\tpass")
        }
        source.appendLine("//--code-end;type:reInput;name:$nodeName").appendLine("\t}")
    }

    fun enumerateExpressions(s: String): String {
        var i = 0
        var l = 0
        val b = StringBuilder()
        while (++i < s.length) {
            if (s[i - 1] == '#' && s[i] == '{') {
                b.append(s.substring(l, i - 1))
                l = i + 1
                var c = 1
                while (c > 0 && ++i < s.length) {
                    if (s[i - 1] != '\\') {
                        if (s[i] == '{')
                            c++
                        if (s[i] == '}')
                            c--
                    }
                }
                b.append("\${enumerate(").append(s.substring(l, i)).append(")}")
                l = ++i
            }
        }
        if (l < s.length)
            b.append(s.substring(l, s.length))
        return b.toString()
    }

    private fun write(speech: Speech) = with(speech) {
        source.append("\tval $nodeName = Response($nodeId, $repeatable, " +
                (if (background != null) "\"$background\"" else "null"))
        source.append(", ")
        if (code.isNullOrBlank())
            source.append("{ null }")
        else
            source.append("{ \"\"\"${code}\"\"\" }")
        source.append(", ")
        if (speech.ttsConfig != null)
            source.appendSource(speech.ttsConfig!!)
        else
            source.append("null")
        texts.forEach { text ->
            source.append(", { \"\"\"").append(enumerateExpressions(text)).append("\"\"\" }")
        }
        source.appendLine(')')
    }

    private fun write(image: Image) = with(image) {
        this@DialogueSourceCodeBuilder.source.appendLine("\tval $nodeName = Response($nodeId, image = \"${source}\")")
    }

    private fun write(sound: Sound) = with(sound) {
        this@DialogueSourceCodeBuilder.source.appendLine("\tval $nodeName = Response($nodeId, audio = \"${source}\", isRepeatable = ${sound.repeatable})")
    }

    private fun write(command: Command) = with(command) {
        this@DialogueSourceCodeBuilder.source.appendLine("\tval $nodeName = Command($nodeId, \"${this.command}\", { \"\"\"${code}\"\"\" })")
    }

    private fun write(function: DialogueSourceCode.Function) = with(function) {
        source.appendLine("\tval $nodeName: Function = Function($nodeId) {")
        source.appendLine("\t\tval transitions = mutableListOf<Transition>()")
        val lastLine = code.toString().trim().substringAfterLast('\n')
        var lastLineTransition = (lastLine.indexOf("Transition(") >= 0)
        transitions.forEach {
            if (lastLine.indexOf(it.key) >= 0)
                lastLineTransition = true
            source.append("\t\t")
            if (code.indexOf(it.key) >= 0)
                source.append("val ${it.key} = ")
            source.appendLine("Transition(${it.value}).apply { transitions.add(this) }")
        }
        source.appendLine("//--code-start;type:function;name:$nodeName")
        source.appendLine(if (code.isEmpty()) "\t\ttransitions.random()" else code)
        source.appendLine("//--code-end;type:function;name:$nodeName")

        if (transitions.size == 1 && !lastLineTransition) {
            transitions.entries.first().let {
                source.appendLine("\t\tTransition(${it.value})")
            }
        }
        source.appendLine("\t}")
    }

    private fun write(subDialogue: SubDialogue) = with(subDialogue) {
        source.appendLine("\tval $nodeName = SubDialogue($nodeId, \"$subDialogueId\") {")
        source.appendLine("//--code-start;type:subDialogue;name:$nodeName")
        source.appendLine(if (code.isNotEmpty()) code else "it.create()")
        source.appendLine("//--code-end;type:subDialogue;name:$nodeName")
        source.append("\t}")
        if (rcode.isNotEmpty()) {
            source.appendLine(".apply { selector.apply {")
            source.appendLine("//--r-code-start;type:subDialogue;name:$nodeName")
            source.append(rcode)
            source.appendLine("//--r-code-end;type:subDialogue;name:$nodeName")
            source.append("\t} }")
        }
        source.appendLine()
    }

    private fun write(action: Action) = with(action) {
        source.appendLine("\tval $nodeName = Action($nodeId,\"$nodeName\", \"${this.action}\")")
    }

    private fun write(action: GlobalAction) = with(action) {
        source.appendLine("\tval $nodeName = GlobalAction($nodeId,\"$nodeName\", \"${this.action}\")")
    }

    private fun writeTransitions() {
        source.appendLine()
        source.appendLine("\tinit {")
        transitions.forEach { source.appendLine("\t\t${it.key}.next = ${it.value}") }
        source.appendLine("\t}")
    }
}
