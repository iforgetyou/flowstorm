package ai.flowstorm.studio.mailing

import ai.flowstorm.studio.model.User
import ai.flowstorm.common.config.ConfigValue
import org.litote.kmongo.id.WrappedObjectId
import java.net.URL

class MailchimpService : MailingService() {

    data class Contact(val email_address: String, val status: String, val merge_fields: Map<String, Any>, val email_type: String = "html")

    override val name = "mailchimp"

    @ConfigValue("mailing.listId", "")
    lateinit var listId: String

    override fun createContact(user: User): Any? {
        val map = mapOf(
            "FNAME" to if (user.name == User.DEFAULT_NAME) "Flowstormer" else user.name,
            "LNAME" to if (user.name == User.DEFAULT_NAME) "" else user.surname,
        )
        val contact = Contact(user.username, "subscribed", map)
        val hash = (user._id as WrappedObjectId).id.toHexString()
        return call<Contact>(
            URL("https://${apiKey.substringAfterLast('-')}.api.mailchimp.com/3.0/lists/$listId/members/$hash"),
            Method.PUT,
            contact
        ).also {
            logger.info("Contact $contact created")
        }
    }
}