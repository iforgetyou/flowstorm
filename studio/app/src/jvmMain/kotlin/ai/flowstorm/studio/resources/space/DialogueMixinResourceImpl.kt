package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.model.DialogueMixin
import ai.flowstorm.studio.model.Space.Role.*
import ai.flowstorm.studio.resources.DialogueMixinResource
import ai.flowstorm.studio.security.RequireRole
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.studio.services.StudioContext
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.security.Authenticated
import com.mongodb.client.model.Filters.regex
import javax.inject.Inject
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@RequireRootRole(Admin)
@RequireRole(Editor)
open class DialogueMixinResourceImpl : DialogueMixinResource,
    MongoAbstractEntityRepository<DialogueMixin>(DialogueMixin::class) {

    @Inject
    lateinit var context: StudioContext

    override fun find() = find(context.query)
    override fun find(query: Query): List<DialogueMixin> = query(query).apply {
        add(match(context.spaceFilter()))
        query.search?.let {
            val r = ".*${it}.*"
            add(
                match(
                    or(
                        regex( DialogueMixin::name.path(), r, "i"),
                    )
                )
            )
        }
    }.run()

    override fun get(id: Id<DialogueMixin>) = super.get(id)

    override fun create(entity: DialogueMixin): DialogueMixin {
        entity.space_id = context.spaceId
        return super.create(entity).also {
            context.activity(DialogueMixin::class, it._id, it.name, Activity.Type.Created)
        }
    }

    override fun update(id: Id<DialogueMixin>, entity: DialogueMixin): DialogueMixin {
        entity.space_id = context.spaceId
        return super.updateById(id, entity).also {
            context.activity(DialogueMixin::class, it._id, it.name, Activity.Type.Updated)
        }
    }

    override fun delete(id: Id<DialogueMixin>) {
        val entity = get(id)
        super.remove(id)
        context.activity(DialogueMixin::class, id, entity.name, Activity.Type.Deleted)
    }
}
