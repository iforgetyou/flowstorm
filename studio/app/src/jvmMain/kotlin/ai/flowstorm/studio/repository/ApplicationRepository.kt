package ai.flowstorm.studio.repository

import ai.flowstorm.studio.model.Application
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import org.litote.kmongo.eq


class ApplicationRepository : MongoAbstractEntityRepository<Application>(Application::class) {

    fun getByAlexaId(alexaId: String): Application = collection.find(Application::alexaId eq alexaId).single()

    fun findPublic(): List<Application> = collection.find(Application::public eq true).toList()
}