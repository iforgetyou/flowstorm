package ai.flowstorm.studio.resources

import com.mongodb.client.model.UnwindOptions
import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.services.StudioContext
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.collections.toList
import ai.flowstorm.core.model.Device as CoreDevice

@Path("/devices")
@Produces(MediaType.APPLICATION_JSON)
class DeviceResourceImpl : DeviceResource, MongoAbstractEntityRepository<Device>(Device::class) {

    @Inject
    lateinit var context: StudioContext

    override fun find(): List<Device> = query(context.query).apply {
        add(lookup(Space.COLLECTION_NAME, Device::space_id.name, Space::_id.name, Device::_space.name))
        add(unwind("\$${Device::_space.name}", UnwindOptions().apply { preserveNullAndEmptyArrays(true) }))
    }.run()

    override fun create(device: Device): Device {
        if (device.space_id == null)
            device.space_id = context.rootSpace._id

        collection.insertOne(device)
        return device
    }

    override fun get(deviceId: Id<Device>): Device {
        return collection.aggregate<Device>(
                match(Device::_id eq deviceId as Id<CoreDevice>),
                lookup(Space.COLLECTION_NAME, Device::space_id.name, Space::_id.name, Device::_space.name),
                unwind("\$${Device::_space.name}", UnwindOptions().apply { preserveNullAndEmptyArrays(true) } ),
        ).singleOrNull()
                ?: throw NotFoundException("Device $deviceId does not exist")
    }

    override fun get(deviceId: String): Device {
        return collection.find(Device::deviceId eq deviceId).toList().firstOrNull() ?: throw NotFoundException("Device $deviceId does not exist")
    }

    override fun update(deviceId: Id<Device>, device: Device): Device {
        collection.updateOne(Device::_id eq deviceId as Id<CoreDevice>, device)

        return device
    }

    override fun move(deviceId: Id<Device>, parameters: DeviceResource.MoveParameters): Device {
        collection.updateOneById(deviceId, set(SetTo(Device::space_id, parameters.spaceId)))
        return get(deviceId)
    }

    override fun find(query: Query): List<Device> = error("Not supported")

}