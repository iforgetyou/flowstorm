package ai.flowstorm.studio.security

import ai.flowstorm.security.jwt.JwtVerifier
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT

class HMAC256Verifier(private val issuer:String, private val secret:String) : JwtVerifier {

    override fun verify(jwt: DecodedJWT): DecodedJWT {
        val algorithm = Algorithm.HMAC256(secret)
        val verifier = JWT.require(algorithm)
            .withIssuer(issuer)
            .build()
        return verifier.verify(jwt)
    }
}