package ai.flowstorm.studio.security

interface Identity {
//    val username: String?
    val name: String?
    val surname: String?
    val nickname: String?
    val email: String?
    val deviceId:String?
    val phoneNumber: String?
}