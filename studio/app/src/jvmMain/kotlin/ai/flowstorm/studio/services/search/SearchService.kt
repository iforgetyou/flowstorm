package ai.flowstorm.studio.services.search

import ai.flowstorm.studio.model.SearchRequest
import ai.flowstorm.studio.model.SearchResult
import ai.flowstorm.studio.model.SpaceEntity
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Field
import org.bson.Document
import org.bson.types.ObjectId
import org.litote.kmongo.addFields
import org.litote.kmongo.aggregate
import org.litote.kmongo.eq
import org.litote.kmongo.match
import javax.inject.Inject

class SearchService {
    @Inject
    lateinit var database: MongoDatabase

    fun search(sr: SearchRequest): List<SearchResult> {

        val adapter = getAdapter(sr)

        return database.getCollection(adapter.getCollectionName()).aggregate<SearchResult>(
            adapter.searchStage(),
            match(SpaceEntity::space_id eq sr.spaceId.cast()),
            adapter.filterStage(),
            adapter.projectionStage(),

            addFields(
                Field(SearchResult::entity.name, sr.entity),
                Field("highlights", Document("\$meta", "searchHighlights")),
                Field("score", Document("\$meta", "searchScore"))
            )
        ).toList()
    }

    fun getAdapter(sr: SearchRequest): SearchAdapter = when (sr.entity) {
        "dialogue" -> DialogueSearchAdapter(sr)
        "dialogueMixin" -> DialogueMixinSearchAdapter(sr)
        "dialogueSnippet" -> DialogueSnippetSearchAdapter(sr)
        else -> error("unknown entity")
    }
}
