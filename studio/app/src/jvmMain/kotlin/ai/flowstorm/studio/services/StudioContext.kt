package ai.flowstorm.studio.services

import com.mongodb.client.MongoDatabase
import ai.flowstorm.studio.model.Activity
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.SpaceEntity
import ai.flowstorm.studio.model.User
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.common.indexer.Elastic
import ai.flowstorm.common.jackson.IgnoreIdMixin
import ai.flowstorm.common.model.Entity
import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import org.bson.types.ObjectId
import org.litote.kmongo.id.toId
import javax.inject.Inject
import javax.ws.rs.NotFoundException
import javax.ws.rs.PathParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.SecurityContext
import kotlin.reflect.KClass
import ai.flowstorm.core.model.Space as CoreSpace

class StudioContext {

    @Inject
    lateinit var database: MongoDatabase

    @Suppress("UnresolvedRestParam")
    @PathParam("spaceId")
    var _spaceId: Id<CoreSpace>? = null

    val spaceId get() = _spaceId!!

    @Context
    lateinit var securityContext: SecurityContext

    @Inject
    lateinit var query: Query

    @Inject
    lateinit var elastic: Elastic

    @ConfigValue("idMap.rootSpace")
    lateinit var rootSpaceId:String

    val spaces by lazy { database.getCollection<Space>() }
    val rootSpace by lazy { spaces.findOne { Space::_id eq ObjectId(rootSpaceId).toId() } ?: error("Missing ROOT space") }
    private val users by lazy { database.getCollection<User>() }
    private val activities by lazy { database.getCollection<Activity>() }

    fun getUser() = with (securityContext.userPrincipal) {
        users.find(User::username eq name).singleOrNull() ?: throw NotFoundException("User $name does not exist")
    }

    fun isUserAuthenticated() = (securityContext.userPrincipal != null)

    fun activity(clazz: KClass<*>, ref_id: Id<*>, refName: String, space_id: Id<CoreSpace>, type: Activity.Type) {
        val user = getUser()
        val activity = Activity(
            type = type,
            name = clazz.simpleName ?: "Unknown",
            ref_id = ref_id,
            refName = refName,
            user_id = user._id,
            username = "${user.name} ${user.surname}",
            space_id = space_id
        )
        activities.insertOne(activity)
        elastic.save(jsonWriter, activity)
    }

    fun activity(clazz: KClass<*>, ref_id: Id<*>, refName: String, type: Activity.Type) =
        activity(clazz, ref_id, refName, spaceId, type)

    fun spaceFilter(includeRoot: Boolean = true) = if (rootSpace._id != spaceId && includeRoot)
        SpaceEntity::space_id `in` listOf(rootSpace._id, spaceId)
    else
        SpaceEntity::space_id eq spaceId

    companion object {
        private val jsonWriter = ObjectUtil.createMapper()
                .addMixIn(Entity::class.java, IgnoreIdMixin::class.java)
    }
}
