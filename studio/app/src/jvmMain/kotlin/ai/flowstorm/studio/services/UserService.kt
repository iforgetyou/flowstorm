package ai.flowstorm.studio.services

import ai.flowstorm.studio.filters.AuthenticationFilter.Companion.SCHEME
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.security.BaseIdentity
import ai.flowstorm.common.config.ConfigValue
import java.net.URL
import javax.inject.Inject
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.core.HttpHeaders
import ai.flowstorm.studio.security.Identity
import ai.flowstorm.common.client.RestClient
import ai.flowstorm.security.Digest
import com.auth0.jwt.JWT
import kotlin.random.Random

class UserService {

    @Inject
    lateinit var requestContext: ContainerRequestContext

    @ConfigValue("jwtauth.issuer")
    lateinit var issuer: String

    @ConfigValue("jwtauth.ttpIssuer")
    lateinit var ttpIssuer: String

    @ConfigValue("jwt.issuer")
    lateinit var ourIssuer: String

    data class Auth0Identity(
        val given_name: String?,
        val family_name: String?,
        override val nickname: String?,
        override val email: String
    ) : BaseIdentity() {
        override val name by this::given_name
        override val surname by this::family_name
    }

    data class AlexaIdentity(
        override val email: String?
    ) : BaseIdentity()


    fun createFromToken() =
        createFromToken(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION).substringAfter("$SCHEME "))

    fun updateFromToken(user:User) {
        val identity = getIdentityFromJwtToken(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION).substringAfter("$SCHEME "))
        updateUserFromIdentity(user, identity)
    }

    fun getIdentityFromJwtToken(accessToken: String): Identity {
        val decoded = JWT.decode(accessToken)

        return when(decoded.issuer) {
            issuer -> getIdentityFromAuth0(accessToken, issuer)
            ttpIssuer -> getIdentityFromAuth0(accessToken, ttpIssuer)
            ourIssuer -> BaseIdentity(deviceId = decoded.getClaim(DEVICE_ID_CLAIM).asString())
            else -> error("Unsupported issuer")
        }
    }

    private fun createFromToken(accessToken: String): User {
        val identity = getIdentityFromJwtToken(accessToken)
        return createUserFromIdentity(identity)
    }

    fun createUserFromIdentity(identity: Identity): User = User(
        username = identity.email ?: identity.deviceId!!,
        name = identity.name ?: User.DEFAULT_ANONYMOUS_NAME,
        surname = identity.name ?: generateAnonymousName(identity.deviceId),
        nickname = identity.nickname ?: "",
        phoneNumber = identity.phoneNumber ?: if (identity.deviceId != null && identity.deviceId!!.startsWith('+'))
            identity.deviceId else "",
        isAnonymous = true
    )

    private fun generateAnonymousName(seed: String?) =
        Digest.md5(seed?.toByteArray() ?: Random.nextBytes(32)).substring(0, 7).toInt(16).toString(36).padStart(6, '0')

    fun updateUserFromIdentity(user: User, identity: Identity) = with(user) {
        // update anonymous user from identity
        identity.email?.let {
            username = it
            isAnonymous = false
        }
        identity.name?.let { name = it }
        identity.surname?.let { surname = it }
        identity.nickname?.let {
            if (nickname.isBlank())
                nickname = it
        }
    }

    fun getIdentityFromAuth0(accessToken: String, tokenIssuer: String): Identity = RestClient.call(
        URL("${tokenIssuer}userinfo"),
        responseType = Auth0Identity::class.java,
        headers = mapOf(HttpHeaders.AUTHORIZATION to "$SCHEME $accessToken")
    )

    fun getIdentityFromAlexa(accessToken: String): Identity = AlexaIdentity(
        email = RestClient.call(
            URL("https://api.amazonalexa.com/v2/accounts/~current/settings/Profile.email"),
            responseType = String::class.java,
            headers = mapOf(HttpHeaders.AUTHORIZATION to "$SCHEME $accessToken")
        )
    )

    companion object {
        const val DEVICE_ID_CLAIM = "https://flowstorm/deviceId"
    }
}