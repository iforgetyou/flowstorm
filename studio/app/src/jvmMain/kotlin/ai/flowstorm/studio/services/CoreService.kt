package ai.flowstorm.studio.services

import org.litote.kmongo.*
import com.mongodb.client.MongoDatabase
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.Space.CoreAuthType.ApiKey
import org.eclipse.jetty.http.HttpHeader
import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.client.RestClient
import ai.flowstorm.core.resources.BuilderResource
import ai.flowstorm.util.LoggerDelegate
import java.net.URL
import javax.inject.Inject
import javax.ws.rs.client.WebTarget

class CoreService {

    @Inject
    lateinit var database: MongoDatabase

    val logger by LoggerDelegate()

    val spaces by lazy { database.getCollection<Space>(Space.COLLECTION_NAME) }

    private val defaultCoreUrl by lazy { ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.core) }

    data class CoreConnectionInfo(
        val url: String,
        val token: String,
    )

    val defaultConnectionInfo get() = CoreConnectionInfo(defaultCoreUrl, AppConfig["core.apiKey"])

    private fun createDefault(forBuilder: Boolean) =
        if (forBuilder) {
            defaultConnectionInfo.let {
                val builderUrl = (if (it.url.startsWith("http://localhost")) defaultCoreUrl else it.url) + "/builder"
                it.copy(url = builderUrl)
            }
        } else {
            defaultConnectionInfo
        }


    fun getConnectionInfo(spaceId: Id<CoreSpace>? = null, forBuilder: Boolean = false): CoreConnectionInfo {
        if (spaceId == null) {
            return createDefault(forBuilder)
        }

        val space = spaces.findOne(CoreSpace::_id eq spaceId)!!

        return if (space.coreUrl.isNullOrEmpty()) {
            createDefault(forBuilder)
        } else {
            val coreUrl: String = if (forBuilder) {
                space.coreUrl + "/builder"
            } else {
                space.coreUrl!!
            }

            val token = when (space.coreAuthType) {
                ApiKey -> space.coreKey ?: ""
            }

            CoreConnectionInfo(coreUrl, token)
        }
    }

    fun fileUrl(spaceId: Id<CoreSpace>) = getConnectionInfo(spaceId).url + "/file"

    fun webTarget(spaceId: Id<CoreSpace>?): WebTarget {
        val connectionInfo = getConnectionInfo(spaceId)
        logger.debug("Providing web target with URL ${connectionInfo.url}")
        return RestClient.webTarget(connectionInfo.url, connectionInfo.token)
    }

    inline fun <reified I> call(spaceId: Id<CoreSpace>?, uri: String): I {
        val connectionInfo = getConnectionInfo(spaceId).let { it.copy(url = it.url + uri) }
        logger.info("Calling target URL ${connectionInfo.url}")
        return RestClient.call(
            url = URL(connectionInfo.url),
            responseType = I::class.java,
            headers = mapOf(HttpHeader.AUTHORIZATION.asString() to "Bearer ${connectionInfo.token}")
        )
    }

    inline fun <reified I> resource(spaceId: Id<CoreSpace>?, uri: String): I {
        val connectionInfo = getConnectionInfo(spaceId).let { it.copy(url = it.url + uri) }
        logger.debug("Providing resource of type ${I::class.java.name} with target URL ${connectionInfo.url}")
        return RestClient.resource(I::class.java, connectionInfo.url, connectionInfo.token)
    }
    inline fun builderResource(spaceId: Id<CoreSpace>?): BuilderResource {
        val connectionInfo = getConnectionInfo(spaceId, true)
        logger.info("Providing builder resource with target URL ${connectionInfo.url}")
        return RestClient.resource(BuilderResource::class.java, connectionInfo.url, connectionInfo.token)
    }
}