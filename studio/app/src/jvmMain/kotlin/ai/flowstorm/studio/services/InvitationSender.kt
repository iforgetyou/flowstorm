package ai.flowstorm.studio.services

import ai.flowstorm.studio.model.Invitation
import ai.flowstorm.studio.util.AppHostnameBuilder
import ai.flowstorm.common.messaging.MessageSender
import javax.inject.Inject

class InvitationSender {

    @Inject
    lateinit var emailSender: MessageSender

    @Inject
    lateinit var hostnameBuilder: AppHostnameBuilder

    companion object {
        private const val TEMPLATE_NAME = "flowstorm-studio-invitation"
        private const val SUBJECT = "Flowstorm Studio Invitation"
        private const val VAR_NAME_LINK = "invitation-link"
    }

    fun sendInvitation(invitation: Invitation) {
        val vars = mapOf(VAR_NAME_LINK to createLink())

        emailSender.sendMessage(
                MessageSender.Recipient(invitation.username, ""),
                SUBJECT,
                TEMPLATE_NAME,
                vars
        )
    }

    private fun createLink(): String {
        val hostname = hostnameBuilder.frontend()
        return "https://$hostname/#!/invitation"
    }
}
