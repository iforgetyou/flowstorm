package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Matrix
import ai.flowstorm.studio.model.MatrixRequest
import ai.flowstorm.studio.model.QAData
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.security.Authenticated
import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.client.RestClient
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/matrix")
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
class MatrixResourceImpl(val illusionistURL: String, val apiKey: String) : MatrixResource {
    private val url by lazy {
        (if (illusionistURL.startsWith("http://localhost"))
            ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.illusionist_training)
        else
            illusionistURL
        ) + "/training"
    }

    override fun generateMatrix(request: Matrix): String {
        val qaString = ObjectUtil.defaultMapper.readValue(request.qaString, Any::class.java)
        val apiPath = URL("$url/model/analysis/confusion/?key=$apiKey")
        val conn = RestClient.call(
                apiPath,
                "POST",
                output = QAData(request.model_params ?: MatrixRequest(0, ""),
                        request.decision_node ?: 0,
                        qaString))
        var jsonResponse = ""
        val br = BufferedReader(InputStreamReader(conn.inputStream))
        var text: String? = ""
        while (br.readLine().also { text = it } != null) {
            jsonResponse += text
        }

        return jsonResponse
    }
}