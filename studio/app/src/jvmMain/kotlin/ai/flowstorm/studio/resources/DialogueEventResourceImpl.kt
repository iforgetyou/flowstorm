package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Space.Role.Admin
import ai.flowstorm.studio.security.RequireRootRole
import ai.flowstorm.common.client.resourceMethod
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.query
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.model.DialogueEvent
import ai.flowstorm.security.Authenticated
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import ai.flowstorm.core.resources.DialogueEventResource as CoreDialogueEventResource

@Path("/dialogueEvent")
@Produces(MediaType.APPLICATION_JSON)
@Authorized
@Authenticated
@RequireRootRole(Admin)
class DialogueEventResourceImpl: DialogueEventResource {

    @Inject
    lateinit var coreWebTarget: WebTarget

    @Inject
    lateinit var query: Query

    override fun getDialogueEvents(): List<DialogueEvent> =
            coreWebTarget.resourceMethod(CoreDialogueEventResource::list)
                    .query(query).request().get(object : GenericType<List<DialogueEvent>>() {})

}