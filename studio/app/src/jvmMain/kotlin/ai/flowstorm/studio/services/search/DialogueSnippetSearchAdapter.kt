package ai.flowstorm.studio.services.search

import ai.flowstorm.studio.model.*
import com.mongodb.client.model.Filters
import org.bson.conversions.Bson
import org.litote.kmongo.*
import org.litote.kmongo.id.toId

class DialogueSnippetSearchAdapter(sr: SearchRequest) : SearchAdapter(sr) {

    override fun exactSearch(): Bson = match(
        if (sr.id != null)
            or(
                DialogueSnippet::graph / Graph::userInputs / Graph.UserInput::mixins_id contains sr.id!!.toId(),
                DialogueSnippet::graph / Graph::intents / Graph.Intent::mixins_id contains sr.id!!.toId(),
                DialogueSnippet::graph / Graph::globalIntents / Graph.Intent::mixins_id contains sr.id!!.toId(),
                DialogueSnippet::graph / Graph::speeches / Graph.Speech::mixins_id contains sr.id!!.toId(),
                DialogueSnippet::graph / Graph::functions / Graph.Function::mixins_id contains sr.id!!.toId(),
                DialogueSnippet::graph / Graph::commands / Graph.Command::mixin_id eq sr.id!!.toId(),
            )
        else
        or(
            DialogueSnippet::name regex sr.regex,
            DialogueSnippet::graph / Graph::functions / Graph.Function::code regex sr.regex,
            DialogueSnippet::graph / Graph::userInputs / Graph.UserInput::code regex sr.regex,
            DialogueSnippet::graph / Graph::intents / Graph.Intent::utterances elemMatch Filters.regex(
                "",
                sr.regex
            ),
            DialogueSnippet::graph / Graph::globalIntents / Graph.Intent::utterances elemMatch Filters.regex(
                "",
                sr.regex
            ),
        )
    )

    override fun projectionStage() = project(
        SearchResult::id from "\$_id",
        SearchResult::name from DialogueSnippet::name,
    )

    override fun filterStage(): Bson = match(
        DialogueSourceV2::state ne DialogueSourceV2.State.Archived
    )
}
