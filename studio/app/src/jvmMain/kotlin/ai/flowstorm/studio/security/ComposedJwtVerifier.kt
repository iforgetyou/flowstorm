package ai.flowstorm.studio.security

import ai.flowstorm.security.jwt.JwtVerifier
import com.auth0.jwt.interfaces.DecodedJWT

class ComposedJwtVerifier(private vararg val verifiers: JwtVerifier) : JwtVerifier {

    override fun verify(jwt: DecodedJWT): DecodedJWT {
        var result: DecodedJWT? = null

        verifiers.forEach {
            if (result == null)
            result = try {
                it.verify(jwt)
            } catch (t: Throwable) {
                null
            }
        }

        return result?: error("Can not verify JWT token.")
    }
}