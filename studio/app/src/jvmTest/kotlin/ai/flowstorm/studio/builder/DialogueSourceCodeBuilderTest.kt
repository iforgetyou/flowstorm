package ai.flowstorm.studio.builder

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import ai.flowstorm.core.model.DialogueSourceCode

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class DialogueSourceCodeBuilderTest {

    @Test
    fun `test dialogue source building`() {
        val sourceCodeBuilder = DialogueSourceCodeBuilder("dialogue1", "product/dialogue", 1, "en", "BasicContext").apply {
            parameters = "val str: String = \"bla\", val num: Int = 123, val chk: Boolean = true"
            initCode = "data class Test(val i: Int)\nfun time() = System.currentTimeMillis()\nval i = 1"

            var nodeId = 0
            addNode(
                DialogueSourceCode.Speech(
                    --nodeId,
                    "response1",
                    null,
                    null,
                    true,
                    listOf("hello, say some animal", "hi, say some animal"),
                    null
                )
            )
            addNode(DialogueSourceCode.Intent(--nodeId, "intent1", 0F, listOf("no", "nope", "quit", "stop"), listOf(), listOf()))
            addNode(DialogueSourceCode.Intent(--nodeId, "intent2", 0F, listOf("dog", "cat", "tiger"), listOf(), listOf()))
            addNode(
                DialogueSourceCode.Function(
                    --nodeId,
                    "function1",
                    mapOf("trans1" to "stop"),
                    "println(trans1)\ntrans1"
                )
            )
            addNode(
                DialogueSourceCode.Speech(
                    --nodeId,
                    "response2",
                    null,
                    null,
                    true,
                    listOf("Your response was \${input.transcript.text}. Intent node \${input.intent.name}."),
                    null
                )
            )
            addNode(
                DialogueSourceCode.UserInput(
                    --nodeId,
                    "input1",
                    listOf("intent1", "intent2"),
                    listOf(),
                    null,
                    false,
                    mapOf(),
                    ""
                )
            )

            addTransition("start" to "response1")
            addTransition("response1" to "input1")
            addTransition("intent1" to "function1")
            addTransition("intent2" to "response2")
            addTransition("response2" to "stop")
        }

        sourceCodeBuilder.build()
    }
}