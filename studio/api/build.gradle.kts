group = "ai.flowstorm.studio"
description = "Flowstorm Studio API"

val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
    implementation(project(":flowstorm-core-api"))
}

