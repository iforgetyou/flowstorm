package ai.flowstorm.studio.model

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.Described
import ai.flowstorm.core.model.DialogueModel
import ai.flowstorm.core.model.Application as CoreApplication
import ai.flowstorm.core.model.Space as CoreSpace

open class Application(
        override var _id: Id<CoreApplication> = newId(),
        override var name: String,
        override var dialogue_id: Id<DialogueModel>? = null,
        override var public: Boolean = false,
        override var anonymousAccessAllowed: Boolean = false,
        override var icon: String? = null,
        override var skimmerConfigUrl: String? = null,
        override var description: String = "",
        var alexaId: String? = null,
        var phoneNumber: String? = null,
        var termsUrl: String? = null,
        var terms_id: Id<Terms>? = null,
        var space_id: Id<CoreSpace>,
        @Transient @JsonSerialize
        var _space_name: String? = null
) : CoreApplication(name = name, dialogue_id = dialogue_id, public = public, skimmerConfigUrl = skimmerConfigUrl, icon = icon), Described {

        override fun equals(other: Any?): Boolean {
                return try {
                        _id.toString() == (other as Application)._id.toString()
                } catch (e: ClassCastException) {
                        false
                }
        }

        override fun hashCode(): Int {
                return _id.toString().hashCode()
        }
}
