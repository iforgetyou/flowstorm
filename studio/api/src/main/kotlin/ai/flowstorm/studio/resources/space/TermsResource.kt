package ai.flowstorm.studio.resources.space

import ai.flowstorm.studio.model.Terms
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Terms"])
@Produces(MediaType.APPLICATION_JSON)
interface TermsResource {

    @GET
    fun find(): List<Terms>

    @GET
    @Path("/{id}")
    fun get(@ApiParam(required = true) @PathParam("id") id: Id<Terms>): Terms

    @POST
    fun create(@ApiParam(required = true) entity: Terms): Terms

    @PUT
    @Path("/{id}")
    fun update(
            @ApiParam(required = true) @PathParam("id") id: Id<Terms>,
            @ApiParam(required = true) entity: Terms): Terms

    @DELETE
    @Path("/{id}")
    fun delete(@ApiParam(required = true) @PathParam("id") id: Id<Terms>)
}