package ai.flowstorm.studio.model

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.Device as CoreDevice
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.core.model.User as CoreUser

data class Device(
        override var _id: Id<CoreDevice> = newId(),
        override var deviceId: String,
        var deviceToken: String? = null,
        override var description: String = "",
        var user_id: Id<CoreUser>?,
        var space_id: Id<CoreSpace>?,
        var location: String = "",
        var configuration: String? = null,
        var lastTalk: Instant? = null,
        @Transient @JsonSerialize
        var _user: User? = null,
        var _space: Space? = null
) : CoreDevice(_id = _id, deviceId = deviceId, description = description)
