package ai.flowstorm.studio.model

import java.io.Serializable

data class Matrix(
        val data: String?,
        val num_possible_classes: Int?,
        val accuracy: Double?,
        val oov: Array<String>?,
        val model_params: MatrixRequest?,
        val decision_node: Int?,
        val qaString: String?
)

data class MatrixRequest(
        // Names changed to map directly to request parameteres
        val decision_node: Int,
        val name: String,
        val lang: String = "en",
        val algorithm: String = "FastText",
        val use_tfidf: Boolean = false
)

data class QAData(val model_params: MatrixRequest, val decision_node: Int, val qa: Any): Serializable