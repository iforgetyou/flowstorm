package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.NullId
import ai.flowstorm.common.model.Entity
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import ai.flowstorm.core.model.User as CoreUser

data class Account(
    override val _id: Id<Account> = newId(),
    var user_id: Id<CoreUser> = NullId(),
    val date: Instant = Clock.System.now(),
    var name: String,
    var street: String? = null,
    var city: String? = null,
    var state: String? = null,
    var country: String? = null,
    var zip: String? = null
) : Entity