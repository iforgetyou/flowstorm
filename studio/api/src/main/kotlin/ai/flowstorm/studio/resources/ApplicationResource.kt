package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Application
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Applications"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface ApplicationResource {

    @GET
    fun find(): List<Application>

    @POST
    fun create(@ApiParam(required = true) application: Application): Application

    @GET
    @Path("/{applicationId}")
    fun get(
            @ApiParam(required = true) @PathParam("applicationId") applicationId: Id<Application>
    ): Application

    @PUT
    @Path("/{applicationId}")
    fun update(@ApiParam(required = true) @PathParam("applicationId") applicationId: Id<Application>, @ApiParam application: Application): Application

    @DELETE
    @Path("/{applicationId}")
    fun delete(@ApiParam(required = true) @PathParam("applicationId") applicationId: Id<Application>)
}