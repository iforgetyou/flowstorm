package ai.flowstorm.studio.model

import org.litote.kmongo.Id

data class SearchResult(
    val id: Id<*>,
    val entity: String,
    val name: String,
    val score: Float,
    val highlights: List<HighLight> = listOf(),
    val data: Map<String, String> = mapOf(),
)

data class HighLight(
    val score: Float,
    val path: String,
    val texts: List<HighLightText>
)

data class HighLightText(
    val value: String,
    val type: String,
)
