package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.DialogueSourceV2
import ai.flowstorm.studio.model.Graph
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import ai.flowstorm.core.model.DialogueBuild
import java.io.InputStream
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Api(tags = ["DialogueSource"])
@Produces(MediaType.APPLICATION_JSON)
interface DialogueSourceResource {

    @GET
    fun find(): List<DialogueSourceV2>

    @HEAD
    fun head(): Response

    @GET
    @Path("/template/{templateName}")
    fun getTemplate(
        @ApiParam(required = true) @PathParam("templateName") name:String
    ): DialogueSourceV2

    @GET
    @Path("/{dialogueId}")
    fun get(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    ): DialogueSourceV2

    @GET
    @Path("/{dialogueId}.xml")
    @Produces(MediaType.TEXT_XML)
    fun getXml(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    ): DialogueSourceV2

    @GET
    @Path("/{dialogueId}.xsl")
    @Produces(MediaType.TEXT_XML)
    fun getXsl(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    ): Response

    @PUT
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    @Path("/{dialogueId}/transformed.xml")
    fun getTransformedXml(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>,
            xmlSourceInput: InputStream
    ): Response

    @POST
    fun create(
            @ApiParam(required = true) dialogueSource: DialogueSourceV2
    ): DialogueSourceV2

    @POST
    @Consumes(MediaType.TEXT_XML)
    fun createXml(
            @ApiParam(required = true) dialogueSource: DialogueSourceV2
    ): DialogueSourceV2

    @POST
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    @Path("/{dialogueId}.xsl")
    fun transformXml(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>,
            xmlSourceInput: InputStream
    ): DialogueSourceV2

    @PUT
    @Path("/{dialogueId}")
    fun update(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>,
            @ApiParam(required = true) dialogueSource: DialogueSourceV2
    ): DialogueSourceV2

    @PUT
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    @Path("/{dialogueId}.xml")
    fun updateXml(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>,
            @ApiParam(required = true) dialogueSource: DialogueSourceV2
    ): DialogueSourceV2

    @POST
    @Path("/{dialogueId}/build")
    fun build(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    ): DialogueBuild

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/bulkbuild")
    fun bulkBuild(
        @ApiParam(required = true) @QueryParam("verbose") verbose: Boolean = false,
        @ApiParam(required = true) @QueryParam("dryrun") dryrun: Boolean = false,
    ): Any //Return type is Any to avoid adding jersey-server as dependency (actually it is ChunkedOutput)

    @PUT
    @Path("/{dialogueId}/lock")
    fun lock(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    )

    @DELETE
    @Path("/{dialogueId}/lock")
    fun unlock(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>,
            @ApiParam(required = false) @QueryParam("force") force: Boolean
    )

    @PUT
    @Path("/{dialogueId}/publish")
    fun publish(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    )

    @PUT
    @Path("/{dialogueId}/archive")
    fun archive(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    )

    @GET
    @Path("/{dialogueId}/graph")
    fun getGraph(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    ): Graph

    @PUT
    @Path("/{dialogueId}/graph")
    fun updateGraph(
            @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>,
            @ApiParam(required = true) graph: Graph
    ): Graph
}