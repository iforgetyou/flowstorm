package ai.flowstorm.studio.model

import ai.flowstorm.common.model.Entity
import ai.flowstorm.core.model.User
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId

data class TermsConsent(
        override val _id: Id<TermsConsent> = newId(),
        var datetime: Instant = Clock.System.now(),
        val user_id: Id<User>,
        val terms_id: Id<Terms>
) : Entity