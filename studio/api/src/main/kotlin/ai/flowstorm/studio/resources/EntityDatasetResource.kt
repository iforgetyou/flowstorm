package ai.flowstorm.studio.resources

import ai.flowstorm.core.builder.EntityDataset as EntityDataset
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Named Entity Recognition Datasets"])
@Produces(MediaType.APPLICATION_JSON)
interface EntityDatasetResource {
    @GET
    fun list(): List<EntityDataset>

    @GET
    @Path("/{datasetId}")
    fun get(
            @ApiParam(required = true) @PathParam("datasetId") datasetId: Id<EntityDataset>
    ): EntityDataset

    @POST
    fun create(
            @ApiParam(required = true) entityDataset: EntityDataset
    ): EntityDataset

    @PUT
    @Path("/{datasetId}")
    fun update(
        @ApiParam(required = true) @PathParam("datasetId") datasetId: Id<EntityDataset>,
        @ApiParam(required = true) entityDataset: EntityDataset
    ): EntityDataset

    @GET
    @Path("/{datasetId}/samples")
    fun getDataSamples(
            @ApiParam(required = true) @PathParam("datasetId") datasetId: Id<EntityDataset>
    ): List<EntityDataset.DataSample>

    @PUT
    @Path("/{datasetId}/samples/{sampleId}")
    fun putDataSample(
        @ApiParam(required = true) @PathParam("datasetId") datasetId: Id<EntityDataset>,
        @ApiParam(required = true) @PathParam("sampleId") sampleId: Int,
        @ApiParam(required = true) dataSample: EntityDataset.DataSample
    ): EntityDataset.DataSample

    @DELETE
    @Path("/{datasetId}")
    fun delete(@ApiParam(required = true) @PathParam("datasetId") entityDatasetId: Id<EntityDataset>)

    @DELETE
    @Path("/{datasetId}/samples/{sampleId}")
    fun deleteSample(@ApiParam(required = true) @PathParam("datasetId") entityDatasetId: Id<EntityDataset>,
                     @ApiParam(required = true) @PathParam("sampleId") sampleId: Int)

    @PUT
    @Path("/{datasetId}/train")
    fun train(@ApiParam(required = true) @PathParam("datasetId") entityDatasetId: Id<EntityDataset>)

    @GET
    @Path("/{datasetId}/status")
    fun modelStatus(@ApiParam(required = true) @PathParam("datasetId") entityDatasetId: Id<EntityDataset>): EntityDataset.Status
}