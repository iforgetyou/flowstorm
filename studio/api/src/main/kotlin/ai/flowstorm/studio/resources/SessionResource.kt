package ai.flowstorm.studio.resources

import io.swagger.annotations.Api
import io.swagger.annotations.Authorization
import ai.flowstorm.core.model.Session
import javax.ws.rs.GET
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Sessions"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface SessionResource {
    @GET
    fun find():List<Session>
}
