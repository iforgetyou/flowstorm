package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.SpeechRecognizer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["SpeechRecognizer"])
@Produces(MediaType.APPLICATION_JSON)
interface SpeechRecognizerResource {

    @GET
    fun find(): List<SpeechRecognizer>

    @GET
    @Path("/{id}")
    fun get(@ApiParam(required = true) @PathParam("id") id: Id<SpeechRecognizer>): SpeechRecognizer

    @POST
    fun create(@ApiParam(required = true) entity: SpeechRecognizer): SpeechRecognizer

    @PUT
    @Path("/{id}")
    fun update(
        @ApiParam(required = true) @PathParam("id") id: Id<SpeechRecognizer>,
        @ApiParam(required = true) entity: SpeechRecognizer): SpeechRecognizer

    @DELETE
    @Path("/{id}")
    fun delete(@ApiParam(required = true) @PathParam("id") id: Id<SpeechRecognizer>)
}