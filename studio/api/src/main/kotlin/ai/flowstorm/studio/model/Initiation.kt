package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.common.model.Entity
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import ai.flowstorm.core.model.Application as CoreApplication
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.core.model.User as CoreUser

data class Initiation(
    override var _id: Id<Initiation> = newId(),
    var name: String,
    val datetime: Instant = Clock.System.now(),
    val deviceType: DeviceType,
    var space_id: Id<CoreSpace>,
    val application_id: Id<CoreApplication>,
    val users_id: MutableList<Id<CoreUser>> = mutableListOf()
) : Entity {

    enum class DeviceType {
        Any, Phone, Standalone, Mobile, Web
    }
}