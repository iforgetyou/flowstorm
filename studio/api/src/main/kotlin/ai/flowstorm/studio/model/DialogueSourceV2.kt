package ai.flowstorm.studio.model

import com.fasterxml.jackson.annotation.JsonView
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.util.Xml
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import ai.flowstorm.core.model.Space as CoreSpace

/**
 * Dialogue source model of type V2
 */
@Xml.Attribute("type", "v2")
data class DialogueSourceV2(
    @Xml.Property(flattened = true)
    override val _id: Id<DialogueSourceV2> = newId(),
    override val dialogueName: String,
    override val version: Int = 0,
    override val contextClass: String? = null,
    override val description: String = "",
    override val comment: String = "",
    override val language: String = "en",
    @Xml.Property(flattened = true)
    override var voice_id: Id<Voice>? = null,
    override val speechRecognizer_id: Id<SpeechRecognizer>? = null,
    override val background: String? = null,
    @Xml.Property(flattened = true)
    override val parameters:String? = null,
    override var space_id: Id<CoreSpace>,
    override var datetime: Instant = Clock.System.now(),
    val mixins_id: List<Id<DialogueMixin>> = listOf(),
    @JsonView(View.WithGraph::class)
    val graph: Graph,
    val initCode: String = "",
    var state: State = State.Draft,
    @Xml.Property(ignored = true)
    var lock: String?,
    @Xml.Property(ignored = true)
    var lockTime: Instant?,
    var lastBuild: String? = null
) : DialogueSource, SpaceEntity {

    interface View {
        interface WithGraph
    }

    enum class State {
        Draft, Published, Archived
    }

    override val dialogueId get() = _id.toString()
}
