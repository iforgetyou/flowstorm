package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.DialogueSnippet
import ai.flowstorm.studio.model.Graph
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Dialogue Event"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface DialogueSnippetResource {
    @GET
    fun find(): List<DialogueSnippet>

    @GET
    @Path("/{snippetId}")
    fun get(
        @PathParam("snippetId") id: Id<DialogueSnippet>
    ): DialogueSnippet

    @POST
    fun create(
        @ApiParam(required = true) dialogueSnippet: DialogueSnippet
    ): DialogueSnippet

    @PUT
    @Path("/{snippetId}")
    fun update(
        @PathParam("snippetId") id: Id<DialogueSnippet>,
        dialogueSnippet: DialogueSnippet
    ): DialogueSnippet

    @DELETE
    @Path("/{snippetId}")
    fun delete(@ApiParam(required = true) @PathParam("snippetId") id: Id<DialogueSnippet>)

    @GET
    @Path("/{snippetId}/graph")
    fun getGraph(
        @PathParam("snippetId") id: Id<DialogueSnippet>
    ): Graph
}