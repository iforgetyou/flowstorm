package ai.flowstorm.studio.model

import com.fasterxml.jackson.annotation.JsonView
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.Space as CoreSpace

data class DialogueSnippet(
    override val _id: Id<DialogueSnippet> = newId(),
    val name: String,
    val language: String,
    val description: String = "",
    @JsonView(View.WithGraph::class)
    val graph: Graph,
    override var space_id: Id<CoreSpace>,
) : SpaceEntity {
    interface View {
        interface WithGraph
    }
}
