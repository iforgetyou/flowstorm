package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.User
import ai.flowstorm.core.model.Space
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

data class Activity(
    override val _id: Id<Activity> = newId(),
    var datetime: Instant = Clock.System.now(),
    val type: Type,
    val name: String,
    val ref_id: Id<*>,
    val refName: String,
    val user_id: Id<User>,
    val username: String,
    override var space_id: Id<Space>
) : SpaceEntity {

    enum class Type { Created, Updated, Used, Built, Deleted }
}