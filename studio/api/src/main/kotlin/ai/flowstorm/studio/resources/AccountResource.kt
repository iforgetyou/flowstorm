package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Account
import io.swagger.annotations.Api
import io.swagger.annotations.Authorization
import javax.ws.rs.GET
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Accounts"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface AccountResource {

    @GET
    fun find(): List<Account>

    fun create(entity: Account): Account

    fun getAnonymousAccount(): Account
}