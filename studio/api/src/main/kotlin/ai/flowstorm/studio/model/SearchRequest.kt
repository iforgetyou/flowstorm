package ai.flowstorm.studio.model

import org.litote.kmongo.Id

data class SearchRequest(
    val search: String,
    val entity: String,
    val type: SearchType,
    val spaceId: Id<Space>,
) {
    val regex get() = ".*${Regex.escape(search)}.*"
}

enum class SearchType {
    Exact, Fulltext
}
