package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.common.model.Entity
import ai.flowstorm.core.model.Space as CoreSpace

data class Invitation(
        override var _id: Id<Invitation> = newId(),
        var space_id: Id<CoreSpace>,
        var username: String,
        var role: Space.Role
) : Entity
