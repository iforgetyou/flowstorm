package ai.flowstorm.studio.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.litote.kmongo.Id
import ai.flowstorm.util.Xml
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import javax.xml.stream.XMLStreamWriter
import kotlin.math.abs

@Xml.Serialize(Graph.XmlSerializer::class)
data class Graph(
        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val intents: MutableList<Intent> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val actions: MutableList<Action> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val globalActions: MutableList<GlobalAction> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val globalIntents: MutableList<GlobalIntent> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val responses: MutableList<Response> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val subDialogues: MutableList<SubDialogue> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val goBacks: MutableList<GoBack> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val functions: MutableList<Function> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val userInputs: MutableList<UserInput> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val reInputs: MutableList<ReInput> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val startDialogues: MutableList<Enter> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val stopDialogues: MutableList<Exit> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val stopSessions: MutableList<End> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val sleeps: MutableList<Sleep> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val speeches: MutableList<Speech> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val sounds: MutableList<Sound> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val images: MutableList<Image> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val commands: MutableList<Command> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val links: MutableList<Link> = mutableListOf(),

        @JacksonXmlElementWrapper(useWrapping = false)
        @Xml.Property(wrapped = false)
        val groups: MutableList<Group> = mutableListOf(),

        @JsonIgnore
        @Xml.Property(ignored = true)
        val groupId: Int? = null,

        @JsonIgnore
        @Xml.Property(ignored = true)
        val groupName: String = "",

        @Transient
        @JsonIgnore
        @Xml.Property(wrapped = false)
        val groupGraphs: MutableList<Graph> = mutableListOf()
) {
    class XmlSerializer : Xml.Serializer() {
        override fun serialize(writer: XMLStreamWriter, obj: Any, refs: List<Any>) {
            if (obj is Graph && obj.groupId != null) {
                // nested group graph - wrap by <xsl:for-each> instead of <group> element
                writer.writeStartElement(Xml.XSL_NAMESPACE_URI, "for-each")
                writer.writeAttribute("select", obj.groupName)
                writer.writeStartElement(Xml.XSL_NAMESPACE_URI, "variable")
                writer.writeAttribute("name", "groupId")
                writer.writeAttribute("select", "'${obj.groupId}'")
                writer.writeEndElement()
                serialize(writer, obj, refs, wrap = false, disableObjectSerializer = true) // write group graph children only
                writer.writeEndElement()
            } else {
                // main graph = complete <graph> element
                serialize(writer, obj, refs, disableObjectSerializer = true)
            }
        }
    }

    open class XmlNodeIdSerializer(val name: String = "id") : Xml.Serializer() {

        open fun getIdShift(refs: List<Any>) =
                ((if (refs.size > 2) refs[refs.size - 3] as? Graph else null)?.groupId ?: 0) * 1000

        override fun serialize(writer: XMLStreamWriter, obj: Any, refs: List<Any>) {
            val id = getIdShift(refs) + obj as Int
            writer.writeStartElement(name)
            writer.writeStartElement(Xml.XSL_NAMESPACE_URI, "value-of")
            writer.writeAttribute("select", "position() * $id")
            writer.writeEndElement()
            writer.writeEndElement()
            //serialize(writer, getIdShift(refs) + obj as Int, refs, name)
        }
    }

    class XmlLinkFromSerializer : XmlNodeIdSerializer("from") {
        override fun getIdShift(refs: List<Any>): Int {
            val link = refs[refs.size - 1] as? Link
            return super.getIdShift(refs) * (if (link?.type == Link.Type.Outbound || link?.type == Link.Type.Inner) 1 else 0)
        }

        override fun serialize(writer: XMLStreamWriter, obj: Any, refs: List<Any>) {
            val id = getIdShift(refs) + obj as Int
            val link = refs[refs.size - 1] as? Link
            writer.writeStartElement(name)
            writer.writeStartElement(Xml.XSL_NAMESPACE_URI, "value-of")
            writer.writeAttribute("select", (if (link?.type == Link.Type.Outbound || link?.type == Link.Type.Inner) "position() * $id" else "$id"))
            writer.writeEndElement()
            writer.writeEndElement()
            //serialize(writer, getIdShift(refs) + obj as Int, refs, name)
        }
    }

    class XmlLinkToSerializer : XmlNodeIdSerializer("to") {
        override fun getIdShift(refs: List<Any>): Int {
            val link = refs[refs.size - 1] as? Link
            return super.getIdShift(refs) * (if (link?.type == Link.Type.Inbound || link?.type == Link.Type.Inner) 1 else 0)
        }

        override fun serialize(writer: XMLStreamWriter, obj: Any, refs: List<Any>) {
            val id = getIdShift(refs) + obj as Int
            val link = refs[refs.size - 1] as? Link
            writer.writeStartElement(name)
            writer.writeStartElement(Xml.XSL_NAMESPACE_URI, "value-of")
            writer.writeAttribute("select", (if (link?.type == Link.Type.Inbound || link?.type == Link.Type.Inner) "position() * $id" else "$id"))
            writer.writeEndElement()
            writer.writeEndElement()
            //serialize(writer, getIdShift(refs) + obj as Int, refs, name)
        }
    }

    @Xml.Property(ignored = true)
    val nodes: List<Node>
        @JsonIgnore
        get() = intents + globalIntents + responses + subDialogues + goBacks + functions + userInputs + reInputs + startDialogues + stopDialogues + stopSessions + sleeps + speeches + sounds + images + commands + actions + globalActions + groups

    fun add(node: Node) = when (node) {
        is Intent -> intents += node
        is Action -> actions += node
        is GlobalAction -> globalActions += node
        is GlobalIntent -> globalIntents += node
        is Response -> responses += node
        is SubDialogue -> subDialogues += node
        is GoBack -> goBacks += node
        is Function -> functions += node
        is UserInput -> userInputs += node
        is ReInput -> reInputs += node
        is Enter -> startDialogues += node
        is Exit -> stopDialogues += node
        is End -> stopSessions += node
        is Sleep -> sleeps += node
        is Speech -> speeches += node
        is Sound -> sounds += node
        is Image -> images += node
        is Command -> commands += node
        is Group -> groups += node
        else -> error("Cannot add node of type ${node::class.qualifiedName}")
    }
    /*
        We dont use polymorphic serialization because it does not work on client properly.
             @JsonTypeInfo(
                    use = JsonTypeInfo.Id.NAME,
                    include = JsonTypeInfo.As.PROPERTY,
                    property = "type")
            @JsonSubTypes(value = [
                JsonSubTypes.Type(value = Intent::class, name = "Intent"),
                JsonSubTypes.Type(value = GlobalIntent::class, name = "GlobalIntent"),
                JsonSubTypes.Type(value = Response::class, name = "Response"),
                JsonSubTypes.Type(value = Function::class, name = "Function"),
                JsonSubTypes.Type(value = SubDialogue::class, name = "SubDialogue"),
                JsonSubTypes.Type(value = StopSession::class, name = "StopSession"),
                JsonSubTypes.Type(value = GoBack::class, name = "GoBack"),
                JsonSubTypes.Type(value = UserInput::class, name = "UserInput")
            ]
            )
    */
    interface Node {
        val id: Int
        val name: String
        val loc: String?
        val comment: String
        val groupId: Int?
    }

    interface FileNode : Node {
        val asset_id: Id<File>?
        val source: String?
    }

    abstract class AbstractNode : Node {
        override val name: String
            get() = this::class.simpleName + abs(this.id)
    }

    data class Intent(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            @JacksonXmlElementWrapper(useWrapping = false)
            @Xml.Property(flattened = true, wrapped = false)
            val mixins_id: List<Id<DialogueMixin>> = listOf(),
            val utterances: List<String> = listOf(),
            val negativeUtterances: List<String> = listOf(),
            val threshold: Float = 0F,
            val entities: String = ""
    ) : AbstractNode()

    data class GlobalIntent(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            @JacksonXmlElementWrapper(useWrapping = false)
            @Xml.Property(flattened = true, wrapped = false)
            val mixins_id: List<Id<DialogueMixin>> = listOf(),
            val utterances: List<String> = listOf(),
            val negativeUtterances: List<String> = listOf(),
            val threshold: Float = 0.5F,
            val entities: String = ""
    ) : AbstractNode()

    data class Response(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            val responses: List<String> = listOf(),
            val repeatable: Boolean = true
    ) : AbstractNode()

    data class Speech(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            @Xml.Property(flattened = true)
            val voice_id: Id<Voice>? = null,
            @JacksonXmlElementWrapper(useWrapping = false)
            @Xml.Property(flattened = true, wrapped = false)
            val mixins_id: List<Id<DialogueMixin>> = listOf(),
            val responses: List<String> = listOf(),
            val code: String? = null,
            val background: String? = null,
            val repeatable: Boolean = true
    ) : AbstractNode()

    data class Sound(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            @Xml.Property(flattened = true)
            override val asset_id: Id<File>? = null,
            override val source: String? = null, // deprecated
            val repeatable: Boolean = true
    ) : AbstractNode(), FileNode

    data class Image(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            @Xml.Property(flattened = true)
            override val asset_id: Id<File>? = null,
            override val source: String? = null // deprecated
    ) : AbstractNode(), FileNode

    data class Command(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val command: String,
            @Xml.Property(flattened = true)
            val mixin_id: Id<DialogueMixin>? = null,
            val code: String = ""
    ) : AbstractNode()

    data class Function(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            @JacksonXmlElementWrapper(useWrapping = false)
            @Xml.Property(flattened = true, wrapped = false)
            val mixins_id: List<Id<DialogueMixin>> = listOf(),
            val code: String = ""
    ) : AbstractNode()

    data class SubDialogue(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val label: String = "",
            @Xml.Property(flattened = true)
            val sourceId: Id<DialogueSource>,
            val code: String = "",
            val rcode: String = ""
    ) : AbstractNode()

    data class GoBack(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val repeat: Boolean = false
    ) : AbstractNode()

    data class UserInput(
        @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
        override val name: String,
        override val loc: String? = null,
        override val comment: String = "",
        override val groupId: Int? = null,
        val label: String = "",
        @JacksonXmlElementWrapper(useWrapping = false)
            @Xml.Property(flattened = true, wrapped = false)
            val mixins_id: List<Id<DialogueMixin>> = listOf(),
        val speechRecognizer_id: Id<SpeechRecognizer>? = null,
        val code: String = "",
        val expectedPhrases: String = "",
        val skipGlobalIntents: Boolean = false
    ) : AbstractNode()

    data class ReInput(
        @Xml.Serialize(XmlNodeIdSerializer::class)
        override val id: Int,
        override val name: String,
        override val loc: String? = null,
        override val comment: String = "",
        override val groupId: Int? = null,
        val skipGlobalIntents: Boolean = false,
        val code: String = "",
    ) : AbstractNode()

    data class Enter(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
    ) : AbstractNode()

    data class Exit(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
    ) : AbstractNode()

    data class End(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
    ) : AbstractNode()

    data class Sleep(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val timeout: Int = 0
    ) : AbstractNode()

    data class Action(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val action: String
    ) : AbstractNode()

    data class GlobalAction(
            @Xml.Serialize(XmlNodeIdSerializer::class)
            override val id: Int,
            override val name: String,
            override val loc: String? = null,
            override val comment: String = "",
            override val groupId: Int? = null,
            val action: String
    ) : AbstractNode()

    data class Group(
        override val id: Int,
        override val name: String,
        override val loc: String? = null,
        override val comment: String = "",
        override val groupId: Int? = null,
        val label: String = "",
        val code: String = "",
    ) : AbstractNode()

    data class Link(
            val name: String,
            @Xml.Serialize(XmlLinkFromSerializer::class)
            val from: Int,
            @Xml.Serialize(XmlLinkToSerializer::class)
            val to: Int,
            val fromPort: String,
            val toPort: String,
            val points: List<String> = listOf(),
            var type: Type = Type.Outer
    ) {
        enum class Type { Inbound, Outbound, Inner, Outer }
    }
}
