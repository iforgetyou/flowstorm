package ai.flowstorm.studio.resources

import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Client"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface ClientResource {

    @GET
    @Path("deviceConfig/{deviceId}")
    fun deviceConfiguration(
            @ApiParam(required = true) @PathParam("deviceId") deviceId: String
    ): String
}