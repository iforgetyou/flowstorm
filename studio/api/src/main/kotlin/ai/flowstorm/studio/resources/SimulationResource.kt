package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Simulation
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Simulations"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface SimulationResource {

    @GET
    fun getSimulations(): List<Simulation>

    @POST
    fun createApplication(@ApiParam(required = true) application: Simulation): Simulation

    @GET
    @Path("/{simulationId}")
    fun getSimulation(
            @ApiParam(required = true) @PathParam("simulationId") simulationId: Id<Simulation>
    ): Simulation
}