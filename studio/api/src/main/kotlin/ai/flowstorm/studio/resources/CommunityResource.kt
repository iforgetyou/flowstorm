package ai.flowstorm.studio.resources

import io.swagger.annotations.Api
import ai.flowstorm.core.resources.CommunityResource
import javax.ws.rs.Consumes
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Communities"])
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
interface CommunityResource : CommunityResource