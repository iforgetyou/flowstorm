package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import ai.flowstorm.core.model.Space as CoreSpace

data class UserSpaceRole(
        var spaceId: Id<CoreSpace>,
        var name: String,
        var role: Space.Role
)
