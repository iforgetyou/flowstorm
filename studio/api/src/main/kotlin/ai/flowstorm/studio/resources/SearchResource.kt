package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.SearchResult
import ai.flowstorm.studio.model.Space
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Search"])
@Produces(MediaType.APPLICATION_JSON)
interface SearchResource {
    @GET
    @Path("/{spaceId}")
    fun search(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<Space>,
        @ApiParam(required = true) @QueryParam("entity") entity: String,
        @ApiParam(required = true) @QueryParam("search") search: String,
        @ApiParam(required = true) @QueryParam("type") type: String,
    ): List<SearchResult>

}
