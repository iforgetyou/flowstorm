package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.*
import ai.flowstorm.studio.resources.space.CommunityResource
import ai.flowstorm.studio.resources.space.TermsResource
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import ai.flowstorm.core.resources.ReportResource
import ai.flowstorm.core.resources.TtsResource
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import ai.flowstorm.core.model.Application as CoreApplication
import ai.flowstorm.core.model.DialogueEvent as CoreDialogueEvent
import ai.flowstorm.core.model.Session as CoreSession
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.core.model.User as CoreUser

@Api(tags = ["Spaces"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface SpaceResource {

    @GET
    fun find(): List<Space>

    @POST
    fun create(@ApiParam(required = true) space: Space): Space

    @PUT
    @Path("/{spaceId}")
    fun update(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) space: Space): Space

    @GET
    @Path("/{spaceId}")
    fun get(@ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>): Space

    @DELETE
    @Path("/{spaceId}")
    fun delete(@ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>)

    // Versions

    @GET
    @Path("/{spaceId}/versions")
    fun getVersions(@ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>): List<Version>

    // Users

    @GET
    @Path("/{spaceId}/users")
    fun getUsers(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = false) @QueryParam("q") query: String?
    ): List<User>

    @POST
    @Path("/{spaceId}/users")
    fun createUser(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) user: User
    ): User

    // UserAssignments

    @PUT
    @Path("/{spaceId}/join")
    fun joinCurrentUser(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): Space.UserAssignment

    @DELETE
    @Path("/{spaceId}/leave")
    fun leave(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    )

    @GET
    @Path("/{spaceId}/userAssignments")
    fun getUserAssignments(@ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>): List<Space.UserAssignment>

    @GET
    @Path("/{spaceId}/userAssignments/{userAssignmentId}")
    fun getUserAssignment(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("userAssignmentId") userAssignmentId: Id<Space.UserAssignment>
    ):Space.UserAssignment

    @POST
    @Path("/{spaceId}/userAssignments")
    fun createUserAssignment(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) userAssignment: Space.UserAssignment
    ): Space.UserAssignment

    @PUT
    @Path("/{spaceId}/userAssignments/{userId}/applications/{applicationId}")
    fun addApplicationForUser(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("userId") userId: Id<CoreUser>,
            @ApiParam(required = true) @PathParam("applicationId") applicationId: Id<CoreApplication>
    ): Space.UserAssignment

    @GET
    @Path("/{spaceId}/userAssignments/{userId}/applications")
    fun getApplicationsForUserInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("userId") userId: Id<CoreUser>): List<Application>

    @DELETE
    @Path("/{spaceId}/userAssignments/{userId}/applications/{applicationId}")
    fun deleteApplicationForUserInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("userId") userId: Id<CoreUser>,
            @ApiParam(required = true) @PathParam("applicationId") applicationId: Id<CoreApplication>): Space

    @PUT
    @Path("/{spaceId}/userAssignments/{userAssignmentId}")
    fun updateUserAssignment(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("userAssignmentId") userAssignmentId: Id<Space.UserAssignment>,
            @ApiParam(required = true) userAssignment: Space.UserAssignment
    ): Space.UserAssignment

    @DELETE
    @Path("/{spaceId}/userAssignments/{userAssignmentId}")
    fun deleteUserAssignment(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("userAssignmentId") userAssignmentId: Id<Space.UserAssignment>
    )

    // UserGroups

    @GET
    @Path("/{spaceId}/userGroups")
    fun getUserGroupsInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): List<Space.UserGroup>

    @POST
    @Path("/{spaceId}/userGroups")
    fun createUserGroupInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true, value = "New object of group to be created") userGroup: Space.UserGroup
    ): Space.UserGroup

    @GET
    @Path("/{spaceId}/userGroups/{groupName}")
    fun getUserGroupInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("groupName") groupName: String
    ): Space.UserGroup

    @PUT
    @Path("/{spaceId}/userGroups/{groupName}")
    fun updateUserGroupInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("groupName") groupName: String,
            @ApiParam(required = true, value = "New object of group to be updated") userGroup: Space.UserGroup
    ): Space.UserGroup

    @DELETE
    @Path("/{spaceId}/userGroups/{groupName}")
    fun deleteUserGroupInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("groupName") groupName: String
    )

    // Invitations

    @GET
    @Path("/{spaceId}/invitations")
    fun getInvitations(@ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>): List<Invitation>

    @GET
    @Path("/{spaceId}/invitations/{invitationId}")
    fun getInvitation(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("invitationId") invitationId: Id<Invitation>
    ): Invitation

    @POST
    @Path("/{spaceId}/invitations/{invitationId}")
    fun resendInvitation(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("invitationId") invitationId: Id<Invitation>
    ): Invitation

    @POST
    @Path("/{spaceId}/invitations")
    fun createInvitation(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) invitation: Invitation
    ): Invitation

    @DELETE
    @Path("/{spaceId}/invitations/{invitationId}")
    fun deleteInvitation(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("invitationId") invitationId: Id<Invitation>
    )

    // Devices

    @GET
    @Path("/{spaceId}/devices")
    fun getDevicesInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): List<Device>

    @GET
    @Path("/{spaceId}/devices/{deviceId}")
    fun getDeviceInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("deviceId") deviceId: Id<Device>
    ): Device

    @PUT
    @Path("/{spaceId}/devices/{deviceId}")
    fun updateDeviceInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("deviceId") deviceId: Id<Device>,
            @ApiParam(required = true, value = "New object of device to be updated") device: Device
    ): Device

    @GET
    @Path("/{spaceId}/devices/pair")
    fun pairDeviceInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @QueryParam("pairingCode") pairingCode: String
    ): Device

    // Simulations

    @GET
    @Path("/{spaceId}/simulations")
    fun getSimulations(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): List<Simulation>

    @POST
    @Path("/{spaceId}/simulations")
    fun createSimulationInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) simulation: Simulation
    ): Simulation

    @PUT
    @Path("/{spaceId}/simulations/{simulationId}")
    fun updateSimulationInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("simulationId") simulationId: Id<Simulation>,
            @ApiParam(required = true, value = "Simulation in space to be updated") simulation: Simulation
    ): Simulation

    @DELETE
    @Path("/{spaceId}/simulations/{simulationId}")
    fun deleteSimulation(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("simulationId") simulationId: Id<Simulation>
    )

    // Sessions

    @GET
    @Path("/{spaceId}/sessions")
    fun getSessionsInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): List<CoreSession>

    @GET
    @Path("/{spaceId}/sessions/{sessionId}")
    fun getSessionInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) @PathParam("sessionId") sessionId: String
    ): CoreSession

    @GET
    @Path("/{spaceId}/initiations")
    fun getInitiationsInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): List<Initiation>

    @POST
    @Path("/{spaceId}/initiations")
    fun createInitiationInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>,
            @ApiParam(required = true) initiation: Initiation
    ): Initiation

    @GET
    @Path("/{spaceId}/dialogueEvents")
    fun getDialogueEventsInSpace(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): List<CoreDialogueEvent>

    @Path("/{spaceId}/reports")
    @Produces(MediaType.APPLICATION_JSON)
    fun reports(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): ReportResource

    @Path("/{spaceId}/activities")
    @Produces(MediaType.APPLICATION_JSON)
    fun activities(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): ActivityResource

    @Path("/{spaceId}/dialogueSources")
    fun dialogueSources(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): DialogueSourceResource

    @Path("/{spaceId}/dialogueMixins")
    fun dialogueMixins(
            @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): DialogueMixinResource

    @Path("/{spaceId}/entityDatasets")
    fun entityDatasets(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): EntityDatasetResource

    @Path("/{spaceId}/dialogueSnippets")
    fun dialogueSnippets(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): DialogueSnippetResource

    @Path("/{spaceId}/files")
    fun files(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): FileResource

    @Path("/{spaceId}/communities")
    fun communities(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): CommunityResource

    @Path("/{spaceId}/profiles")
    fun profiles(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): ProfileResource

    @Path("/{spaceId}/voices")
    fun voices(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): VoiceResource

    @Path("/{spaceId}/speechRecognizers")
    fun speechRecognizers(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): SpeechRecognizerResource

    @Path("/{spaceId}/terms")
    fun terms(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): TermsResource

    @Path("/{spaceId}/tts")
    fun tts(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): TtsResource

    @Path("/{spaceId}/applications")
    fun applications(
        @ApiParam(required = true) @PathParam("spaceId") spaceId: Id<CoreSpace>
    ): ApplicationResource

    fun findAnonymousSpace(anonymousUser: User): Space
}
