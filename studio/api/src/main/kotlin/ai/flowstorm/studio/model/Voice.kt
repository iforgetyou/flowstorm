package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.Described
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.model.Space as CoreSpace

data class Voice(
    override val _id: Id<Voice> = newId(),
    val name: String,
    val ttsConfig: TtsConfig,
    override var space_id: Id<CoreSpace>,
    override val description: String = ""
) : Described, SpaceEntity