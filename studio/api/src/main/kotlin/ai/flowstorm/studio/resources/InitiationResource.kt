package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Initiation
import ai.flowstorm.studio.model.Space
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import ai.flowstorm.core.model.Application
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import ai.flowstorm.core.model.Space as CoreSpace

@Api(tags = ["Initiation"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface InitiationResource {

    @GET
    @Path("/device")
    fun listForDevice(
            @ApiParam(required = true) @QueryParam("deviceId") deviceId: String,
            @ApiParam(required = true) @QueryParam("deviceToken") deviceToken: String
    ): List<Initiation>

    @GET
    @Path("/call/{applicationId}")
    @Produces(MediaType.APPLICATION_XML)
    fun getCallTwiML(
        @ApiParam(required = true) @PathParam("applicationId") applicationId: Id<Application>,
        @ApiParam @QueryParam("From") deviceId: String?,
        @ApiParam @QueryParam("FromZip") zip: String?,
        @ApiParam @QueryParam("FromCity") city: String?,
        @ApiParam @QueryParam("FromState") state: String?,
        @ApiParam @QueryParam("FromCountry") country: String?

    ): String

    fun list(spaceId: Id<CoreSpace>): List<Initiation>

    fun create(initiation: Initiation, space: Space): Initiation
}