package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.DialogueMixin
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["DialogueMixin"])
@Produces(MediaType.APPLICATION_JSON)
interface DialogueMixinResource {

    @GET
    fun find(): List<DialogueMixin>

    @GET
    @Path("/{id}")
    fun get(@ApiParam(required = true) @PathParam("id") id: Id<DialogueMixin>): DialogueMixin

    @POST
    fun create(@ApiParam(required = true) entity: DialogueMixin): DialogueMixin

    @PUT
    @Path("/{id}")
    fun update(
            @ApiParam(required = true) @PathParam("id") id: Id<DialogueMixin>,
            @ApiParam(required = true) entity: DialogueMixin): DialogueMixin

    @DELETE
    @Path("/{id}")
    fun delete(@ApiParam(required = true) @PathParam("id") id: Id<DialogueMixin>)
}