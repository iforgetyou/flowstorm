package ai.flowstorm.studio.resources.space

import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import ai.flowstorm.core.model.Community
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Community"])
@Produces(MediaType.APPLICATION_JSON)
interface CommunityResource {

    @GET
    fun getCommunities(): List<Community>

    @GET
    @Path("/{communityName}")
    fun getCommunity(@ApiParam(required = true) @PathParam("communityName") communityName: String): Community

    @POST
    fun createCommunity(
            @ApiParam(required = true) community: Community
    ): Community

    @PUT
    @Path("/{communityName}")
    fun updateCommunity(
            @ApiParam(required = true) @PathParam("communityName") communityName: String,
            @ApiParam(required = true) community: Community): Community
}
