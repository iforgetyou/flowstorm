package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Device
import ai.flowstorm.studio.model.Space
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Devices"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface DeviceResource {

    @GET
    fun find(): List<Device>

    @POST
    fun create(@ApiParam(required = true) device: Device): Device

    @GET
    @Path("/{deviceId}")
    fun get(@ApiParam(required = true) @PathParam("deviceId") deviceId: Id<Device>): Device

    fun get(deviceId: String): Device

    @PUT
    @Path("/{deviceId}")
    fun update(@ApiParam(required = true) @PathParam("deviceId") deviceId: Id<Device>, @ApiParam device: Device): Device

    @PUT
    @Path("/{deviceId}/move")
    fun move(
            @ApiParam(required = true) @PathParam("deviceId") deviceId: Id<Device>,
            @ApiParam(required = true) parameters: MoveParameters
    ): Device

    data class MoveParameters(val spaceId: Id<Space>?)
}
