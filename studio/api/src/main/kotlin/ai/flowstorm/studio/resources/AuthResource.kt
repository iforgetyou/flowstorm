package ai.flowstorm.studio.resources

import io.swagger.annotations.Api
import io.swagger.annotations.Authorization
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Accounts"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface AuthResource {
    data class Tokens(val id: String, val access: String)

    @GET
    @Path("/{deviceId}")
    fun authDevice(@PathParam("deviceId") deviceId: String): Tokens
}