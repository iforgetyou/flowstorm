package ai.flowstorm.studio.model

import ai.flowstorm.studio.model.Space.CoreAuthType.ApiKey
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.Application as CoreApplication
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.core.model.User as CoreUser

data class Space(
    override var _id: Id<CoreSpace> = newId(),
    override var name: String,
    var coreUrl: String? = null,
    var coreAuthType: CoreAuthType = ApiKey,
    var coreKey: String? = null,
    var account_id: Id<Account>? = null,
    var deviceConfiguration: String? = null,
    var twilioAccountID: String? = null,
    var twilioAuthToken: String? = null,
    var userAssignments: MutableList<UserAssignment> = mutableListOf(),
    var groups: MutableList<UserGroup> = mutableListOf(),
    var availableApplications_id: MutableList<Id<CoreApplication>> = mutableListOf()
) : CoreSpace {

    enum class CoreAuthType {
        ApiKey
    }

    open class Configuration(open var settings: Settings, open var applications_id: MutableList<Id<CoreApplication>>)

    data class UserAssignment(
            var user_id: Id<CoreUser>,
            var role: Role,
            var note: String? = null,
            override var settings: Settings = Settings(""),
            override var applications_id: MutableList<Id<CoreApplication>> = mutableListOf(),
            var _user: User? = null
    ) : Configuration(settings, applications_id)

    data class UserGroup(
            //var _id: String = ObjectId().toHexString(),
            var name: String,
            var note: String?,
            var users_id: MutableList<Id<CoreUser>>,
            override var settings: Settings,
            override var applications_id: MutableList<Id<CoreApplication>>
    ) : Configuration(settings, applications_id)

    data class Settings(
            var description: String
    )

    enum class Role {
        Consumer,
        Maintainer,
        Editor,
        Admin,
        Owner;

        companion object {
            private val order = listOf(Consumer, Maintainer, Editor, Admin, Owner)

            fun isAllowed(required: Role, actual: Role): Boolean = order.indexOf(required) <= order.indexOf(actual)
        }
    }

    companion object {
        const val COLLECTION_NAME = "space"
    }
}