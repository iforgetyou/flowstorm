package ai.flowstorm.studio.model

import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import java.io.Serializable
import ai.flowstorm.core.model.Space as CoreSpace
import ai.flowstorm.core.model.User as CoreUser

data class User(
    override var _id: Id<CoreUser> = newId(),
    override var username: String,
    override var name: String,
    override var surname: String,
    override var nickname: String,
    override var phoneNumber: String? = null,
    override var agreement: Instant? = null,
    var isAnonymous: Boolean = true,
    var language: String? = null,
    var lastTalk: Instant? = null,
    var excludePublicApps: Boolean = false,
    var shouldLaunchAssistant: Boolean = true,
    val seenHints: List<String> = listOf(),
    var space_id: Id<CoreSpace>? = null // space where the user was created (directly added consumers without email)
) : CoreUser(
    username = username,
    name = name,
    surname = surname,
    nickname = nickname,
    agreement = agreement
), Serializable {

    init {
        if(username.isEmpty()) username = "$_id@user.flowstorm.ai"
    }

    val fullName get() = "$name $surname"

    companion object {
        const val DEFAULT_NAME = "User"
        const val DEFAULT_ANONYMOUS_NAME = "Anonym"
    }
}