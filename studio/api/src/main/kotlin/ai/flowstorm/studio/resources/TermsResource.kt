package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.Terms
import ai.flowstorm.studio.model.TermsConsent
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Terms"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface TermsResource {

    @GET
    @Path("/{termsId}")
    fun find(
            @ApiParam(required = true) @PathParam("termsId") termsId: Id<Terms>
    ): Terms?

    @GET
    @Path("/{termsId}/consent")
    fun getConsent(
            @ApiParam(required = true) @PathParam("termsId") termsId: Id<Terms>
    ): TermsConsent

    @POST
    @Path("/{termsId}/consent")
    fun giveConsent(
        @ApiParam(required = true) @PathParam("termsId") termsId: Id<Terms>
    ): TermsConsent

    @DELETE
    @Path("/{termsId}/consent")
    fun revokeConsent(
        @ApiParam(required = true) @PathParam("termsId") termsId: Id<Terms>
    )

    @GET
    @Path("/application/{appId}")
    fun findByApp(
        @ApiParam(required = true) @PathParam("appId") appId: Id<Application>
    ): Terms?
}
