package ai.flowstorm.studio.model

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.User as CoreUser
import ai.flowstorm.core.model.Space as CoreSpace

data class File(
    override val _id: Id<File> = newId(),
    val name: String,
    var type: String,
    var source: String? = null,
    val tags: MutableList<String> = mutableListOf(),
    val description: String = "",
    var datetime: Instant = Clock.System.now(),
    val owner_id: Id<CoreUser>? = null,
    override var space_id: Id<CoreSpace>
) : SpaceEntity
