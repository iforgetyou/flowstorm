package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.DialogueSource
import ai.flowstorm.studio.model.DialogueSourceV2
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["DialogueSource"])
@Produces(MediaType.APPLICATION_JSON)
interface DialogueSourceSimpleResource {
    @GET
    @Path("/{dialogueId}")
    fun get(
        @ApiParam(required = true) @PathParam("dialogueId") id: Id<DialogueSource>
    ): DialogueSourceV2

}