package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Application
import ai.flowstorm.studio.model.Space
import ai.flowstorm.studio.model.User
import ai.flowstorm.studio.model.UserSpaceRole
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Users"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface UserResource {

    @GET
    fun find(): List<User>

    @POST
    fun create(@ApiParam(required = true) user: User): User

    @PUT
    @Path("/{userId}")
    fun update(
            @ApiParam(required = true) @PathParam("userId") id: Id<User>,
            @ApiParam(required = true) user: User): User

    @DELETE
    @Path("/{userId}")
    fun delete(@ApiParam(required = true) @PathParam("userId") id: Id<User>)

    @GET
    @Path("/{userId}")
    fun get(@ApiParam(required = true) @PathParam("userId") id: Id<User>): User

    @GET
    @Path("/{userId}/roles")
    fun getRolesForUser(@ApiParam(required = true) @PathParam("userId") id: Id<User>): List<UserSpaceRole>

    @GET
    @Path("/{userId}/spaces")
    fun getSpaces(@ApiParam(required = true) @PathParam("userId") id: Id<User>): List<Space>

    @GET
    @Path("/{userId}/appplications")
    fun getApplications(@ApiParam(required = true) @PathParam("userId") id: Id<User>): List<Application>
}
