package ai.flowstorm.studio.model

import ai.flowstorm.core.model.Space
import ai.flowstorm.core.model.Versionable
import ai.flowstorm.core.type.NamedEntity
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId

data class Terms(
    override val _id: Id<Terms> = newId(),
    override val name: String,
    override var version: Int,
    val title: String,
    val text: String,
    override var space_id: Id<Space>,
    var datetime: Instant = Clock.System.now()
) : SpaceEntity, NamedEntity, Versionable
