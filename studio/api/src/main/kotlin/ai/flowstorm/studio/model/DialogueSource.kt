package ai.flowstorm.studio.model

import ai.flowstorm.common.model.Entity
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.litote.kmongo.Id
import ai.flowstorm.core.model.Described
import ai.flowstorm.core.model.DialogueModel
import kotlinx.datetime.Instant
import ai.flowstorm.core.model.Space as CoreSpace

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = DialogueSource.TYPE_PROPERTY, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, defaultImpl = DialogueSourceV2::class)
@JsonSubTypes(value = [
    JsonSubTypes.Type(value = DialogueSourceV2::class, name = DialogueSource.V2)
])
interface DialogueSource : DialogueModel, Described, Entity {

    override val _id: Id<DialogueSourceV2>
    override val dialogueName: String
    override val version: Int
    override val description: String
    val contextClass: String?
    val comment: String
    val language: String
    val voice_id: Id<Voice>?
    val speechRecognizer_id: Id<SpeechRecognizer>?
    val background: String?
    val parameters: String?
    val space_id: Id<CoreSpace>
    var datetime: Instant

    companion object {
        const val TYPE_PROPERTY = "type"
        const val V2 = "v2" // standard V2 tree based model
    }
}