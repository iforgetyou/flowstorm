package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.File
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Files"])
@Produces(MediaType.APPLICATION_JSON)
interface FileResource {

    @GET
    fun find(): List<File>

    @POST
    fun create(@ApiParam(required = true) file: File): File

    @GET
    @Path("/{fileId}")
    fun get(@ApiParam(required = true) @PathParam("fileId") id: Id<File>): File

    @PUT
    @Path("/{fileId}")
    fun update(@ApiParam(required = true) @PathParam("fileId") id: Id<File>, @ApiParam file: File): File

    @DELETE
    @Path("/{fileId}")
    fun delete(@ApiParam(required = true) @PathParam("fileId") id: Id<File>)
}
