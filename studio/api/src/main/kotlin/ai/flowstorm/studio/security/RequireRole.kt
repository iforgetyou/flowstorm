package ai.flowstorm.studio.security

import ai.flowstorm.studio.model.Space
import javax.ws.rs.NameBinding

@NameBinding
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class RequireRole(val role: Space.Role = Space.Role.Consumer)