package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Voice
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Voice"])
@Produces(MediaType.APPLICATION_JSON)
interface VoiceResource {

    @GET
    fun find(): List<Voice>

    @GET
    @Path("/{id}")
    fun get(@ApiParam(required = true) @PathParam("id") id: Id<Voice>): Voice

    @POST
    fun create(@ApiParam(required = true) entity: Voice): Voice

    @PUT
    @Path("/{id}")
    fun update(
        @ApiParam(required = true) @PathParam("id") id: Id<Voice>,
        @ApiParam(required = true) entity: Voice): Voice

    @DELETE
    @Path("/{id}")
    fun delete(@ApiParam(required = true) @PathParam("id") id: Id<Voice>)
}