package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.*
import io.swagger.annotations.Api
import io.swagger.annotations.Authorization
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.PUT
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Current user"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface CurrentUserResource  {

    data class SignUpParams(val deviceId: String? = null)

    @GET
    fun getUser(): User

    @PUT
    fun update(user:User): User

    @GET
    @Path("/roles")
    fun getUserRoles():  List<UserSpaceRole>

    @GET
    @Path("/invitations")
    fun getInvitations():  List<Invitation>

    @GET
    @Path("/spaces")
    fun getSpaces():  List<Space>

    @GET
    @Path("/applications")
    fun getApplications():  Set<Application>

    @POST
    @Path("/signup")
    fun signUp(params: SignUpParams)

    @POST
    @Path("/signupAnonymous")
    fun signUpAnonymous()
}