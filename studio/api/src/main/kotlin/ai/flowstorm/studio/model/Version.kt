package ai.flowstorm.studio.model

data class Version(val name: String, val value: String?)