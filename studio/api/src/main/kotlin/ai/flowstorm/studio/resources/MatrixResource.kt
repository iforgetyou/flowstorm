package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Matrix
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import javax.ws.rs.POST
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Matrix"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface MatrixResource {

    @POST
    fun generateMatrix(@ApiParam(required = true) request: Matrix): String
}