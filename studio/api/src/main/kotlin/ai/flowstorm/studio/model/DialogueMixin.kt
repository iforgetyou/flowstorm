package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.Described
import ai.flowstorm.core.model.Space as CoreSpace

data class DialogueMixin(
        override val _id: Id<DialogueMixin> = newId(),
        val type: Type,
        val name: String,
        val text: String,
        val language: String?,
        override val description: String = "",
        override var space_id: Id<CoreSpace>
) : Described, SpaceEntity {

    enum class Type {
        Intent, Speech, Command, Function, UserInput, Init
    }
}