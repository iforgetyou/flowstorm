package ai.flowstorm.studio.resources

import io.swagger.annotations.Api
import io.swagger.annotations.Authorization
import ai.flowstorm.core.model.DialogueEvent
import javax.ws.rs.GET
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Dialogue Event"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface DialogueEventResource {
    @GET
    fun getDialogueEvents():List<DialogueEvent>
}