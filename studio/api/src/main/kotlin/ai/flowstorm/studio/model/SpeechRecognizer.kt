package ai.flowstorm.studio.model

import ai.flowstorm.core.model.Described
import ai.flowstorm.core.model.Space
import ai.flowstorm.core.model.SttConfig
import org.litote.kmongo.Id
import org.litote.kmongo.newId

data class SpeechRecognizer(
    override val _id: Id<SpeechRecognizer> = newId(),
    val name: String,
    val sttConfig: SttConfig,
    override var space_id: Id<Space>,
    override val description: String = ""
) : Described, SpaceEntity