package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import ai.flowstorm.common.model.Entity
import ai.flowstorm.core.model.Space as CoreSpace

interface SpaceEntity : Entity {
    var space_id: Id<CoreSpace>
}