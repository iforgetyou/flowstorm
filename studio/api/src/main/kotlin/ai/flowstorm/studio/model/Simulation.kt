package ai.flowstorm.studio.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.DialogueModel
import ai.flowstorm.core.type.DEFAULT_LOCATION
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.core.type.Location
import kotlinx.datetime.Instant
import ai.flowstorm.core.model.Space as CoreSpace

open class Simulation(
    var _id: Id<Simulation> = newId(),
    var name: String,
    var datetime: Instant,
    var timeSpeed: Int = 10,
    var adaptiveSpeed: Boolean = true,
    var description: String,
    var dialogue_id: Id<DialogueModel>,
    var space_id: Id<CoreSpace>,
    var waypoints: MutableList<Waypoint> = mutableListOf()
) {
    data class Waypoint(
        var name: String,
        var time: Long,
        var location: Location = DEFAULT_LOCATION,
        var attributes: Dynamic = Dynamic()
    )
}