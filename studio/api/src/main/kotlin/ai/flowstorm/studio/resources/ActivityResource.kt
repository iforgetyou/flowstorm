package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Activity
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import org.litote.kmongo.Id
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(tags = ["Activities"])
@Produces(MediaType.APPLICATION_JSON)
interface ActivityResource {

    @GET
    fun find(): List<Activity>

    @GET
    @Path("/{ActivityId}")
    fun get(@ApiParam(required = true) @PathParam("ActivityId") id: Id<Activity>): Activity
}