package ai.flowstorm.studio.resources

import ai.flowstorm.studio.model.Invitation
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Authorization
import org.litote.kmongo.Id
import javax.ws.rs.PUT
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Invitations"], authorizations = [Authorization("Authorization")])
@Produces(MediaType.APPLICATION_JSON)
interface InvitationResource {

    fun get(invitationId: Id<Invitation>): Invitation
    fun delete(invitation: Invitation): Invitation
    fun delete(invitationId: Id<Invitation>)

    @PUT
    @Path("/{invitationId}/accept")
    fun accept(
            @ApiParam(required = true) @PathParam("invitationId") invitationId: Id<Invitation>
    ):Invitation
}