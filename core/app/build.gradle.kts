group = "ai.flowstorm.core"
description = "Flowstorm Core App"

val jettyVersion = findProperty("jetty.version") as String
val kotlinxCliVersion = findProperty("kotlinx-cli.version") as String
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
    application
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                api("org.jetbrains.kotlinx:kotlinx-cli:$kotlinxCliVersion")
                implementation(project(":flowstorm-client-lib"))
            }
        }
        val jvmMain by getting {
            dependencies {
                api(project(":flowstorm-connector-reddit"))
                api(project(":flowstorm-nlp-illusionist-lib"))
                api(project(":flowstorm-nlp-skimmer"))
                api(project(":flowstorm-nlp-profanity"))
                api(project(":flowstorm-nlp-normalizer"))
                api(project(":flowstorm-nlp-pipeline"))
                api(project(":flowstorm-nlp-stt-google"))
                api(project(":flowstorm-nlp-stt-amazon"))
                api(project(":flowstorm-nlp-stt-microsoft"))
                api(project(":flowstorm-nlp-stt-deepgram"))
                api(project(":flowstorm-nlp-tts-google"))
                api(project(":flowstorm-nlp-tts-amazon"))
                api(project(":flowstorm-nlp-tts-microsoft"))
                api(project(":flowstorm-nlp-tts-lovo"))
                api(project(":flowstorm-nlp-generative-lib"))
                api(project(":flowstorm-channel-socket"))
                api(project(":flowstorm-channel-alexa"))
                api(project(":flowstorm-channel-google"))
                api(project(":flowstorm-storage-local"))
                api(project(":flowstorm-storage-google"))
                api(project(":flowstorm-storage-aws"))
                api(project(":flowstorm-storage-azure"))
                implementation(project(":flowstorm-connector-lib"))
                implementation("org.eclipse.jetty.websocket:websocket-server:$jettyVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
            }
        }
    }
}

application {
    mainClass.set("ai.flowstorm.core.Application")
}

tasks.getByName<Jar>("jvmJar") {
    archiveAppendix.set("") // discard jvm appendix to make distribution bundle work
}
