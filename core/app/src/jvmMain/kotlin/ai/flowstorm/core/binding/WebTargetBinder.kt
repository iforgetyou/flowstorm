package ai.flowstorm.core.binding

import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.client.RestClient
import ai.flowstorm.common.config.Config
import org.glassfish.hk2.utilities.binding.AbstractBinder
import javax.ws.rs.client.WebTarget

class WebTargetBinder(private val config: Config) : AbstractBinder() {

    override fun configure() {
        // Intent recognition
        bind(
            RestClient.webTarget(ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.illusionist))
                .queryParam("key", config["illusionist.apiKey"])
        ).to(WebTarget::class.java).named("illusionist")

        // Duckling
        bind(
            RestClient.webTarget(ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.duckling))
                .path("/parse")
        ).to(WebTarget::class.java).named("duckling")

        // Triton
        bind(
            RestClient.webTarget(ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.triton))
                .path("/v2/models")
        ).to(WebTarget::class.java).named("triton")
    }
}