package ai.flowstorm.core.builder

import org.litote.kmongo.Id


interface EntityModelBuilder {
    fun trainEntityModel(dataset: EntityDataset)
    fun modelStatus(id: Id<EntityDataset>): EntityDataset.Status?
    fun deleteEntityModel(id: Id<EntityDataset>)
}