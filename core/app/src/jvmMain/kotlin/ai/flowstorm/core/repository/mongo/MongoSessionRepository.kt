package ai.flowstorm.core.repository.mongo

import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.Application
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.model.Turn
import ai.flowstorm.core.model.User
import ai.flowstorm.core.repository.SessionRepository
import com.mongodb.client.model.Field
import org.bson.Document
import org.bson.types.ObjectId
import org.litote.kmongo.*
import org.litote.kmongo.id.toId

class MongoSessionRepository : MongoAbstractEntityRepository<Session>(Session::class), SessionRepository {

    private val turnLookup = listOf(
        lookup(
            "turn",
            listOf(Session::_id.variableDefinition("sid")),
            Session::turns,
            match(
                expr(
                    MongoOperator.eq from listOf(Turn::session_id, "\$\$sid")
                )
            ),
            sort(ascending(Turn::_id))
        ),
        addFields(Field("turnsCount", Document.parse("{\$size: \"\$turns\"}")))
    )

    override fun findBy(sessionId: String): Session? = Pipeline().apply {
        addAll(turnLookup)
        add(match(Session::sessionId eq sessionId))
    }.run().singleOrNull()

    override fun find(query: Query): List<Session> = query(query).apply {
        addAll(turnLookup)

        query.search?.let {
            val id = if (ObjectId.isValid(it)) ObjectId(it) else null

            val filter = when {
                id != null -> Session::_id eq id.toId()
                isSessionId(it) -> Session::sessionId eq it
                else -> or(
                    Session::sessionId eq it,
                    Session::user / User::username eq it,
                    Session::application / Application::name eq it,
                )
            }

            add(
                match(filter)
            )
        }
    }.run()

    private fun isSessionId(str: String) =
        str.matches("^[a-fA-F0-9]{40}$".toRegex()) //SHA1

    override fun update(entity: Session, upsert: Boolean): Session = super.update(clearTurns(entity), upsert)

    override fun create(entity: Session): Session = super.create(clearTurns(entity))

    private fun clearTurns(entity: Session) = entity.copy(turns = emptyList())

    override fun findBy(userId: Id<User>): List<Session> = Pipeline().apply {
        addAll(turnLookup)
        add(match(Session::user / User::_id eq userId))
    }.run()
}
