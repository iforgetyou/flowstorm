package ai.flowstorm.core.binding

import ai.flowstorm.core.cli.*
import kotlinx.cli.Subcommand
import org.glassfish.hk2.utilities.binding.AbstractBinder
import javax.inject.Singleton

class CliBinder : AbstractBinder() {

    override fun configure() {
        bind(ServerSubcommand::class.java).to(Subcommand::class.java).`in`(Singleton::class.java)
        bind(NlpSttSubcommand::class.java).to(Subcommand::class.java).`in`(Singleton::class.java)
        bind(NlpPipelineSubcommand::class.java).to(Subcommand::class.java).`in`(Singleton::class.java)
        bind(StorageCopySubcommand::class.java).to(Subcommand::class.java).`in`(Singleton::class.java)
        bind(ReIndexSubcommand::class.java).to(Subcommand::class.java).`in`(Singleton::class.java)
    }
}
