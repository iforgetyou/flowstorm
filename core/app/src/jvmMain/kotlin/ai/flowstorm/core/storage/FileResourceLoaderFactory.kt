package ai.flowstorm.core.storage

import ai.flowstorm.common.config.Config
import ai.flowstorm.core.runtime.FileResourceLoader
import org.glassfish.hk2.api.Factory
import javax.inject.Inject

class FileResourceLoaderFactory : Factory<FileResourceLoader> {
    @Inject
    lateinit var fileStorage: FileStorage

    @Inject
    lateinit var config: Config

    override fun provide(): FileResourceLoader = FileResourceLoader(
        "dialogue",
        config.get("loader.noCache", "false") == "true",
        config.get("loader.useScript", "false") == "true"
    ).apply {
        fileStorage = this@FileResourceLoaderFactory.fileStorage
    }

    override fun dispose(loader: FileResourceLoader?) {}
}