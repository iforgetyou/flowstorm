package ai.flowstorm.core.indexer

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.indexer.Elastic
import ai.flowstorm.common.jackson.IgnoreIdMixin
import ai.flowstorm.common.model.Entity
import ai.flowstorm.core.indexer.RuntimeDataIndexer.BulkResult
import ai.flowstorm.core.model.*
import ai.flowstorm.core.type.Attributes
import ai.flowstorm.core.type.Memorable
import ai.flowstorm.core.type.Memory
import ai.flowstorm.core.type.MemoryMutableList
import ai.flowstorm.util.Log
import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider
import javax.inject.Inject
import kotlin.reflect.KClass

class RuntimeDataIndexerImpl : RuntimeDataIndexer {

    @Inject
    lateinit var elastic: Elastic

    override fun save(profile: Profile) = elastic.save(jsonWriter, profile)
    override fun save(session: Session) = elastic.save(jsonWriter, session)
    override fun save(turn: Turn) = elastic.save(jsonWriter, turn)
    override fun save(clientEvent: ClientEvent) = elastic.save(jsonWriter, clientEvent)
    override fun save(fullContext: FullContext) = elastic.save(jsonWriter, fullContext)
    override fun save(session: Session, turn: Turn, profile: Profile) {
        save(session)
        save(turn)
        save(profile)
        save(FullContext(session, turn, profile))
    }

    override fun save(dialogueEvent: DialogueEvent) = elastic.save(jsonWriter, dialogueEvent)

    override fun <E: Entity> getById(entityClass: KClass<out E>, id: String) =
        elastic.getById(entityClass, id).let {
            it.sourceAsMap["_id"] = id
            ObjectUtil.defaultMapper.convertValue(it.sourceAsMap, entityClass.java)
        }

    override fun <E : Entity> updateById(entityClass: KClass<out E>, id: String, updateString: String) {
        elastic.updateById(entityClass, id, updateString)
    }

    override fun saveAll(entities: List<Entity>): BulkResult {
        val result = elastic.saveAll(jsonWriter, entities) ?: return BulkResult(0, 0, null)

        return with(result) {
            BulkResult(items.size, items.count { it.isFailed }, if (hasFailures()) buildFailureMessage() else null)
        }
    }

    internal interface SessionMixin {
        @JsonIgnore
        fun getTurns(): MutableList<Turn> = mutableListOf()

        @JsonIgnore
        fun getClientLogEntries(): MutableList<Log.Entry> = mutableListOf()
    }

    internal interface AttributesMixin {
        @JsonIgnore
        fun getAttributes(): Attributes = Attributes()

        @JsonInclude @JsonProperty("attributes")
        fun  indexedAttributes(): Map<String, MutableMap<String, Any>>
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    @JsonFilter(FILTER_NAME)
    internal interface MemorableMixin

    companion object {
        private const val FILTER_NAME = "valueFilter"
        private var filters = SimpleFilterProvider()
            .addFilter(
                FILTER_NAME, SimpleBeanPropertyFilter
                    .filterOutAllExcept(Memory<*>::_value.name, MemoryMutableList<*>::values.name)
            )

        private val jsonWriter = ObjectUtil.createMapper()
            .addMixIn(Entity::class.java, IgnoreIdMixin::class.java)
            .addMixIn(HasAttributes::class.java, AttributesMixin::class.java)
            .addMixIn(Session::class.java, SessionMixin::class.java)
            .addMixIn(Memorable::class.java, MemorableMixin::class.java)
            .setFilterProvider(filters)
    }
}
