package ai.flowstorm.core.repository.mongo

import org.litote.kmongo.*
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.Profile
import ai.flowstorm.core.model.Space
import ai.flowstorm.core.model.User
import ai.flowstorm.core.repository.ProfileRepository
import ai.flowstorm.util.LoggerDelegate

class MongoProfileRepository : MongoAbstractEntityRepository<Profile>(Profile::class), ProfileRepository {

    val logger by LoggerDelegate()
    
    override fun findBy(userId: Id<User>, spaceId: Id<Space>): Profile? {
        return collection.find(Profile::user_id eq userId, Profile::space_id eq spaceId)
            .sort(orderBy(Profile::_id))
            .toList().also {
                if (it.size > 1)
                    logger.warn("Multiple profiles found for user id=$userId, space id=$spaceId")
            }.firstOrNull()
    }

    override fun update(entity: Profile, upsert: Boolean): Profile {
        logger.info("Profile update: [${entity._id}, ${entity.space_id}, ${entity.user_id}]")
        return super.update(entity, upsert)
    }

    override fun create(entity: Profile): Profile {
        logger.info("Profile create: [${entity._id}, ${entity.space_id}, ${entity.user_id}]")
        return super.create(entity)
    }
}
