package ai.flowstorm.core.repository.dynamodb

import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.DynamoAbstractEntityRepository
import ai.flowstorm.core.model.Community
import ai.flowstorm.core.repository.CommunityRepository
import com.amazonaws.services.dynamodbv2.document.KeyAttribute
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec
import com.amazonaws.services.dynamodbv2.document.utils.NameMap
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap
import org.litote.kmongo.Id

class DynamoCommunityRepository : DynamoAbstractEntityRepository<Community>(), CommunityRepository {

    override val tableName = "community"

    override fun getCommunitiesInSpace(spaceId: String): List<Community> {
        val spec = QuerySpec()
            .withFilterExpression("#spaceid = :value")
            .withNameMap(
                NameMap()
                .with("#spaceid", "space_id"))
            .withValueMap(
                ValueMap()
                .withString(":value", spaceId.toString())
            )
        return table.getIndex("space_id").query(spec).toEntityList()
    }

    override fun find(id: Id<Community>): Community? {
        return table.getItem(KeyAttribute("_id", id.toString()))?.toEntity()
    }

    override fun find(query: Query) = TODO("")

    override fun all(): List<Community> = table.scan().toEntityList()
}