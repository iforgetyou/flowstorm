package ai.flowstorm.core.repository.mongo

import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.DialogueEvent
import ai.flowstorm.core.model.User
import ai.flowstorm.core.repository.DialogueEventRepository
import com.mongodb.client.model.Filters.regex
import org.bson.types.ObjectId
import org.litote.kmongo.*
import org.litote.kmongo.id.toId

class MongoDialogueEventRepository : MongoAbstractEntityRepository<DialogueEvent>(DialogueEvent::class), DialogueEventRepository {

    override fun find(query: Query) = query(query, DialogueEvent::datetime, false).apply {
        query.search?.let {
            val r = ".*${it}.*"
            val id = if (ObjectId.isValid(it)) ObjectId(it) else null
            add(
                match(
                    or(
                        regex(DialogueEvent::text.path(), r, "i"),
                        id?.let { DialogueEvent::dialogue_id eq it.toId() },
                        regex((DialogueEvent::user / User::username).path(), r, "i"),
                        regex(DialogueEvent::applicationName.path(), r, "i"),
                    )
                )
            )
        }
    }.run()
}
