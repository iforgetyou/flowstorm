package ai.flowstorm.core.resources

import org.litote.kmongo.*
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.model.DialogueEvent
import ai.flowstorm.core.repository.DialogueEventRepository
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/dialogueEvent")
@Produces(MediaType.APPLICATION_JSON)
@Authorized
class DialogueEventResourceImpl: DialogueEventResource {

    @Inject
    lateinit var dialogueEventRepository: DialogueEventRepository

    @Inject
    lateinit var query: Query

    override fun list(): List<DialogueEvent> = dialogueEventRepository.find(query)

    override fun create(dialogueEvent: DialogueEvent) = dialogueEventRepository.create(dialogueEvent).let {  }

    override fun get(eventId: Id<DialogueEvent>): DialogueEvent = dialogueEventRepository.get(eventId)
}