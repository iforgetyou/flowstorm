package ai.flowstorm.core

import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.nlp.pipeline.PipelineComponent
import ai.flowstorm.core.nlp.pipeline.PipelineOpenStreamComponent
import ai.flowstorm.core.nlp.pipeline.PipelineResponseItemComponent
import ai.flowstorm.core.nlp.pipeline.PipelineStreamType
import ai.flowstorm.core.nlp.stt.SttStreamFactory
import ai.flowstorm.nlp.*
import ai.flowstorm.nlp.normalizer.Normalizer
import ai.flowstorm.nlp.pipeline.PipelineAudioRecorder
import ai.flowstorm.nlp.pipeline.PipelineV2
import ai.flowstorm.nlp.profanity.ProfanityFilter
import ai.flowstorm.nlp.skimmer.Skimmer
import org.glassfish.hk2.api.IterableProvider
import javax.inject.Inject

open class DefaultPipeline : PipelineV2() {

    @ConfigValue("pipeline.stt-multiple", "false") var sttMultiple = false
    @ConfigValue("pipeline.record-audio", "false") var recordAudio = false
    @Inject lateinit var audioRecorder: PipelineAudioRecorder
    @Inject lateinit var actionResolver: ActionResolver
    @Inject lateinit var profanityFilter: ProfanityFilter
    @Inject lateinit var tokenizer: InternalTokenizer
    @Inject lateinit var ir: IllusionistIR
    @Inject lateinit var ner: IllusionistNER
    @Inject lateinit var simpleNer: SimpleNER
    @Inject lateinit var skimmer: Skimmer
    @Inject lateinit var normalizer: Normalizer
    @Inject lateinit var triton: Triton
    @Inject lateinit var duckling: Duckling
    @Inject lateinit var sttStreamFactories: IterableProvider<SttStreamFactory>

    override val openStream = PipelineOpenStreamComponent { scope, type ->
        scope.apply {
            if (sttMultiple) {
                // use all available STT services
                sttStreamFactories.forEach { sttService ->
                    sttStream(sttService)
                }
            } else {
                sttStream() // use only STT service requested by dialogue models
            }
            if (type == PipelineStreamType.Any && recordAudio)
                stream(audioRecorder)
        }
    }

    override val processInput = PipelineComponent { scope, span ->
        scope.apply {
            process(actionResolver)
            //launch(nrg)
            if (context.input.isNotAction()) {
                process(normalizer)
                process(tokenizer)
                process(profanityFilter)
                launch(skimmer)
                launch(triton)
                // Entity recognition components
                launch(duckling)
                launch(ner)
                launch(simpleNer)

                launch(ir)
            }
        }
    }

    override val processReInput = PipelineComponent { scope, span ->
        scope.apply {
            process(actionResolver)
            if (context.input.isNotAction()) {
                process(ir)
            }
        }
    }

    override val processResponseItem = PipelineResponseItemComponent { scope, item ->
        scope.ttsProcess(item)
    }
}
