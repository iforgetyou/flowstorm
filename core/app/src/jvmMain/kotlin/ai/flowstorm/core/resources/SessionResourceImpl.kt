package ai.flowstorm.core.resources

import ai.flowstorm.common.query.Query
import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.repository.SessionRepository
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/sessions")
@Produces(MediaType.APPLICATION_JSON)
@Authorized
class SessionResourceImpl: SessionResource {

    @Inject
    lateinit var sessionRepository: SessionRepository

    @Inject
    lateinit var query: Query

    override fun find(): List<Session> = sessionRepository.find(query)
}
