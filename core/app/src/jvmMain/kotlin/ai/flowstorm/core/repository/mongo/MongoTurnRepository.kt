package ai.flowstorm.core.repository.mongo

import org.litote.kmongo.Id
import org.litote.kmongo.`in`
import org.litote.kmongo.eq
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.model.Turn
import ai.flowstorm.core.repository.TurnRepository

class MongoTurnRepository : MongoAbstractEntityRepository<Turn>(Turn::class), TurnRepository {

    override fun find(query: Query): List<Turn> = query(query, Turn::datetime, false).run()

    override fun findBy(session_id: Id<Session>): List<Turn> = collection.find(Turn::session_id eq session_id).toList()

    override fun findBy(session_ids: List<Id<Session>>): List<Turn> = collection.find(Turn::session_id `in` session_ids).toList()
}