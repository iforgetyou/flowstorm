package ai.flowstorm.core

import ai.flowstorm.common.*
import ai.flowstorm.core.binding.CliBinder
import kotlinx.cli.ArgParser
import kotlinx.cli.Subcommand
import org.glassfish.hk2.utilities.ServiceLocatorUtilities.bind

class Application(private val container: ServiceContainer) {

    fun run(args: Array<String>) {
        if (args.isNotEmpty() && args[0] == "server") {
            JettyServer.run(ServerApplication(container), container)
        } else {
            bind(container.serviceLocator, CliBinder())
            with(ArgParser("flowstorm")) {
                subcommands(*container.serviceLocator.getAllServices(Subcommand::class.java).toTypedArray())
                parse(args)
            }
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) = Application(ServiceContainer()).run(args)
    }
}
