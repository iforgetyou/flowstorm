package ai.flowstorm.core

import ai.flowstorm.common.AppConfig
import ai.flowstorm.common.ServiceLocatorProvider
import ai.flowstorm.common.binding.ConfigBinder
import ai.flowstorm.common.config.Config
import ai.flowstorm.core.binding.ComponentBinder
import ai.flowstorm.core.binding.WebTargetBinder
import org.glassfish.hk2.api.ServiceLocator
import org.glassfish.hk2.api.ServiceLocatorFactory
import org.glassfish.hk2.utilities.ServiceLocatorUtilities

open class ServiceContainer(val config: Config = AppConfig) : ServiceLocatorProvider {

    open val binders = listOf(
        ComponentBinder(config),
        WebTargetBinder(config)
    )

    override val serviceLocator: ServiceLocator by lazy {
        val serviceLocatorFactory = ServiceLocatorFactory.getInstance()
        val serviceLocator = serviceLocatorFactory.create(null)
        ServiceLocatorUtilities.bind(serviceLocator, ConfigBinder(config))
        binders.forEach { binder ->
            ServiceLocatorUtilities.bind(serviceLocator, binder)
        }
        serviceLocator
    }
}