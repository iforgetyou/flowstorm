package ai.flowstorm.core.repository.dynamodb

import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.DynamoAbstractEntityRepository
import ai.flowstorm.core.model.ClientEvent
import ai.flowstorm.core.repository.ClientEventRepository
import com.amazonaws.services.dynamodbv2.document.KeyAttribute
import org.litote.kmongo.Id

class DynamoClientEventRepository: DynamoAbstractEntityRepository<ClientEvent>(), ClientEventRepository {

    override val tableName = "clientEvent"

    override fun find(query: Query) = TODO()

    override fun all(): List<ClientEvent> {
        return table.scan().toEntityList()
    }

    override fun find(id: Id<ClientEvent>): ClientEvent? {
        return table.getItem(KeyAttribute("_id", id.toString()))?.toEntity()
    }
}