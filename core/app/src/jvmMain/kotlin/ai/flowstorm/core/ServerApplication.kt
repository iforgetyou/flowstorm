package ai.flowstorm.core

import ai.flowstorm.channel.alexa.servlets.AmazonAlexaServlet
import ai.flowstorm.channel.google.servlets.GoogleAssistantServlet
import ai.flowstorm.channel.socket.servlets.*
import ai.flowstorm.common.JerseyApplication
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.ServerConfig
import ai.flowstorm.core.model.Space
import ai.flowstorm.core.model.SpaceImpl
import ai.flowstorm.core.runtime.DialogueSecurityManager
import com.fasterxml.jackson.core.Version
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver
import com.fasterxml.jackson.databind.module.SimpleModule

open class ServerApplication(container: ServiceContainer) : JerseyApplication(container.config) {

    override val serverConfig: ServerConfig get() =
        ServerConfig(
            this, super.serverConfig.servlets +
                mapOf(
                    ClientHttpSocketServlet::class.java to "/client/*",
                    ClientWebSocketServlet::class.java to "/socket/*",
                    ClientHttpPollingServlet::class.java to "/polling/*",
                    TestHttpPollingServlet::class.java to "/polling.test/*",
                    TwilioWebSocketServlet::class.java to "/call/*",
                    AmazonAlexaServlet::class.java to "/alexa/*",
                    GoogleAssistantServlet::class.java to "/google/*",
                )
        )

    init {
        config["name"] = "core"
        logger.info("Creating application (nameSuffix=${config.nameSuffix}, dataSuffix=${config.dataSuffix})")
        if (!"false".equals(config.getOrNull("security.securityManager"))) {
            System.setSecurityManager(
                DialogueSecurityManager(
                    config.get(
                        "security.raiseExceptions",
                        "true"
                    ) != "false"
                )
            )
        }
        container.binders.forEach {
            register(it)
        }
        registerModelImplementations()
    }

    private fun registerModelImplementations() {
        val module = SimpleModule("Models", Version.unknownVersion())
        val resolver = SimpleAbstractTypeResolver()
        resolver.addMapping(Space::class.java, SpaceImpl::class.java)
        module.setAbstractTypes(resolver)
        ObjectUtil.defaultMapper.registerModule(module)
    }
}