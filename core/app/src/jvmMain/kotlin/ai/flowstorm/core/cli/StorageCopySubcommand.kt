package ai.flowstorm.core.cli

import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.core.storage.FileStorageFactory
import ai.flowstorm.core.storage.StorageType
import kotlinx.cli.ArgType
import kotlinx.cli.Subcommand
import kotlinx.cli.default
import kotlinx.cli.optional
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.inject.Inject
import ai.flowstorm.security.Digest.md5
import kotlinx.datetime.Instant

class StorageCopySubcommand: Subcommand("storage:copy", "Storage Copy") {

    private val path by argument(ArgType.String, "Path").optional()
    private val sourceType by option(ArgType.Choice<StorageType>(), "source", "s", "Source storage type").default(StorageType.FileSystem)
    private val destinationType by option(ArgType.Choice<StorageType>(), "destination", "d", "Destination storage type").default(StorageType.AmazonS3)
    private val doCheck by option(ArgType.Boolean, "check", "c", "Compute check sum for uploaded files").default(false)
    private val all by option(ArgType.Boolean, "all", "a", "Whether to skip existing (with newer mtime) files").default(false)


    @Inject
    private lateinit var storageFactory: FileStorageFactory

    override fun execute() {
        val sourceStorage = storageFactory.provide(sourceType)
        val destinationStorage = storageFactory.provide(destinationType)
        val checkSumMap = mutableMapOf<String, String>()
        println("source = $sourceStorage, destination = $destinationStorage, path = \"$path\"")

        sourceStorage.listFiles(path ?: "").forEach {
            val sourceFile = sourceStorage.getFile(it)
            val destinationFile = if (!all) {
                try {
                    destinationStorage.getFile(it)
                } catch (e: FileStorage.NotFoundException) {
                    null
                }
            } else {
                null
            }
            if (destinationFile == null || destinationFile.updateTime < sourceFile.updateTime) {
                println("Copying $it ...")
                val buf = ByteArrayOutputStream()
                sourceStorage.readFile(it, buf)
                checkSumMap[it] = md5(buf.toByteArray())
                destinationStorage.writeFile(it, sourceFile.contentType, sourceFile.metadata
                    ?.map { it -> "${it.key}:${it.value}" } ?: listOf(), ByteArrayInputStream(buf.toByteArray()))
            } else if (!all) {
                println("File $it already exists in the destination storage " +
                        "(dest mtime: ${Instant.fromEpochMilliseconds(destinationFile.updateTime)}, " +
                        "source mtime: ${Instant.fromEpochMilliseconds(sourceFile.updateTime)})")
            }
        }

        // Check the integrity of the uploaded files
        if (doCheck) {
            println("Checking the integrity of the uploaded files...")
            checkSumMap.forEach {
                val buf = ByteArrayOutputStream()
                destinationStorage.readFile(it.key, buf)
                assert(it.value == md5(buf.toByteArray()))
            }
            println("Checksum OK for all files.")
        }
    }
}