package ai.flowstorm.core.resources

import ai.flowstorm.common.security.Authorized
import ai.flowstorm.nlp.tts.TtsProcessor
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.nlp.tts.TtsRequest
import ai.flowstorm.nlp.tts.TtsResponse
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/tts")
@Produces(MediaType.APPLICATION_JSON)
@Authorized
class TtsResourceImpl : TtsResource {

    @Inject
    lateinit var ttsProcessor: TtsProcessor

    override fun synthesize(request: TtsRequest): TtsResponse {
        if (request.text.matches(Regex("<.*[^\\>]>")))
            request.isSsml = true

        val fileType = AudioFileType.mp3
        val path = ttsProcessor.process(null, request, fileType)
        return TtsResponse(path, fileType.contentType)
    }

    override fun synthesizeFormat(request: TtsRequest, fileType: AudioFileType): TtsResponse {
        if (request.text.matches(Regex("<.*[^\\>]>")))
            request.isSsml = true
        val path = ttsProcessor.process(null, request, fileType)
        return TtsResponse(path, fileType.contentType)
    }
}
