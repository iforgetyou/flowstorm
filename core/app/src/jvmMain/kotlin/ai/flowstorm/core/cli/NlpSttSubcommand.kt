package ai.flowstorm.core.cli

import ai.flowstorm.client.io.StreamCallback
import ai.flowstorm.client.io.Microphone
import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.common.TextConsole
import ai.flowstorm.concurrent.launch
import ai.flowstorm.concurrent.sleep
import ai.flowstorm.core.*
import ai.flowstorm.core.model.SttConfig
import ai.flowstorm.core.nlp.stt.SttCallback
import ai.flowstorm.core.nlp.stt.SttStream
import ai.flowstorm.core.type.beautify
import ai.flowstorm.nlp.stt.SttProvider
import ai.flowstorm.nlp.stt.AnySttStreamFactory
import ai.flowstorm.time.currentTime
import ai.flowstorm.util.LoggerDelegate
import kotlinx.cli.ArgType
import kotlinx.cli.Subcommand
import kotlinx.cli.default
import java.io.File
import java.util.*
import javax.inject.Inject

class NlpSttSubcommand: Subcommand("nlp:stt", "NLP Speech-To-Text") {

    private val sampleRate by option(ArgType.Int, "sample-rate", "sr", "Sample rate").default(44100)
    private val input by option(ArgType.String, "input", "i", "Input")
    private val expectedPhrases by option(ArgType.String, "expected-phrases-file", "epf", "Expected phrases file")
    private val logLevel by option(ArgType.String, "log-level", "L", "Logging severity").default("WARN")
    private val provider by option(ArgType.Choice<SttProvider>(), "provider", "p", "STT provider").default(SttProvider.Google)
    private val language by option(ArgType.String, "language", "l", "Language tag (e.g. es, es-MX)").default(Defaults.locale.toLanguageTag())
    private val maxSilence by option(ArgType.Int, "max-silence", "ms", "Max silence (ms)").default(5000)
    private val maxPause by option(ArgType.Int, "max-pause", "mp", "Max pause (ms)").default(500)

    @Inject
    private lateinit var sttStreamFactory: AnySttStreamFactory

    private var firstRecognizedTime: Long? = null
    private var lastRecognizedTime: Long? = null
    private var out = System.out
    private var isOpen = false
    private lateinit var stream: SttStream
    private val logger by LoggerDelegate()

    private fun SttStream.write(path: String, removeWavHeader: Boolean = true) {
        val data = File(path).readBytes()
        var offset = if (removeWavHeader) 44 else 0
        out.println("Writing ${data.size - offset} bytes from $path")
        val blockSize = sampleRate * 2 / 10 // 100 ms
        while (offset < data.size) {
            write(data, offset, blockSize)
            sleep(100)
            offset += blockSize
        }
    }

    override fun execute() {
        val audioFormat = AudioFormat(sampleRate)
        val streamConfig = SttStreamConfig(
            Locale.forLanguageTag(language),
            audioFormat.sampleRate,
            audioFormat.encoding,
            SttMode.SingleUtterance,
            sttConfig = SttConfig(provider.name, maxSilence, maxPause)
        )
        out.println(streamConfig.toString())
        val inputAudioDevice = Microphone(audioFormat = audioFormat)
        stream = sttStreamFactory.create(streamConfig, object : SttCallback {

            override fun onOpen(stream: SttStream) {
                logger.info("STT stream open")
                inputAudioDevice.start()
            }
            override fun onClosed(stream: SttStream) {
                out.println("STT stream closed")
                isOpen = false
            }

            override fun onRecognized(stream: SttStream, input: Input, isFinal: Boolean) {
                if (firstRecognizedTime == null)
                    firstRecognizedTime = currentTime()
                if (!isFinal)
                    lastRecognizedTime = currentTime()
                out.println("Recognized " + (if (!isFinal) "interim" else "final") + " text \"${input.transcript.text.beautify()}\"")
                if (!isFinal) {
                    lastRecognizedTime = currentTime()
                } else {
                    val json = ObjectUtil.defaultMapper.writeValueAsString(input)
                    out.println("Input $json")
                    if (input.transcript.text.substring(0, 4).lowercase() == "stop")
                        stream.close()
                    out.println("Finalized after "
                            + (firstRecognizedTime?.let { currentTime() - it } ?: "?")
                            + " ms from first / "
                            + (lastRecognizedTime?.let { currentTime() - it } ?: "?")
                            + " ms from last recognized text")
                    firstRecognizedTime = null
                    lastRecognizedTime = null
                }
            }

            override fun onError(stream: SttStream, e: Throwable) = e.printStackTrace()
        }, expectedPhrases?.let { path ->
            File(path).readLines().map { ExpectedPhrase(it.trim()) }
        } ?: listOf())
        out.println("Created $stream")
        inputAudioDevice.callback = object : StreamCallback {
            override fun onStart() = out.println("Mic started")
            override fun onWake() = out.println("Wake word detected")
            override fun onStop() = out.println("Mic stopped")
            override fun onData(data: ByteArray, size: Int): Boolean {
                stream.write(data, 0, size)
                return true
            }
        }
        if (input != null) {
            try {
                stream.write(input!!)
            } catch (e: Throwable) {
                out.println("Error: ${e.message}")
                stream.close()
            }
        } else {
            inputAudioDevice.launch()
            object : TextConsole() {
                override fun afterInput(text: String) {
                    val parts = text.toLowerCase().split(' ')
                    when (parts[0]) {
                        "mic" -> when (parts[1]) {
                            "start" -> inputAudioDevice.start()
                            "stop" -> inputAudioDevice.stop()
                        }
                        "input" -> stream.write(parts[1])
                    }
                }

                override fun done() {
                    inputAudioDevice.close()
                }
            }.run()
        }
        while (stream.isOpen)
            sleep(100)
        out.println("Done")
    }
}