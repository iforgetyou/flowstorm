package ai.flowstorm.core.repository.dynamodb

import ai.flowstorm.common.query.Query
import ai.flowstorm.common.repository.DynamoAbstractEntityRepository
import ai.flowstorm.core.model.Property
import ai.flowstorm.core.repository.PropertyRepository
import com.amazonaws.services.dynamodbv2.document.KeyAttribute
import org.litote.kmongo.Id

class DynamoPropertyRepository : DynamoAbstractEntityRepository<Property>(), PropertyRepository {

    override val tableName = "property"

    override fun getValues(key: String): List<String> {
        val index = table.getIndex("key")
        return index.query(KeyAttribute("key", key)).toEntityList<Property>().map { it.value }
    }

    override fun all(): List<Property> = error("Not supported")

    override fun find(id: Id<Property>) = error("Not supported")

    override fun find(query: Query) = error("Not supported")
}