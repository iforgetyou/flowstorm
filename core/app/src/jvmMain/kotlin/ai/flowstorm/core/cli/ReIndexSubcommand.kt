package ai.flowstorm.core.cli

import ai.flowstorm.common.model.Entity
import ai.flowstorm.common.repository.BatchAll
import ai.flowstorm.core.cli.ReIndexSubcommand.EntityOption.*
import ai.flowstorm.core.indexer.RuntimeDataIndexer
import ai.flowstorm.core.repository.DialogueEventRepository
import ai.flowstorm.core.repository.ProfileRepository
import ai.flowstorm.core.repository.SessionRepository
import ai.flowstorm.core.repository.TurnRepository
import kotlinx.cli.*
import javax.inject.Inject

class ReIndexSubcommand : Subcommand("indexer:reindex", "Reindex runtime data.") {

    private val entity by argument(ArgType.Choice<EntityOption>(), "entity")
    private val skip by option(ArgType.Int, "skip").default(0)
    private val batchSize by option(ArgType.Int, "batchSize").default(100)

    enum class EntityOption { Profile, Turn, Session, DialogueEvent }

    @Inject
    lateinit var indexer: RuntimeDataIndexer

    @Inject
    lateinit var sessionRepository: SessionRepository

    @Inject
    lateinit var turnRepository: TurnRepository

    @Inject
    lateinit var profileRepository: ProfileRepository

    @Inject
    lateinit var dialogueEventRepository: DialogueEventRepository


    private val repo
        get() = when (entity) {
            Profile -> profileRepository
            Turn -> turnRepository
            Session -> sessionRepository
            DialogueEvent -> dialogueEventRepository
        } as? BatchAll<Entity> ?: error("Repository must implement BatchAll.")

    override fun execute() {
        indexEntities(repo, batchSize, skip)

    }

    data class Stats(
        var failedLoad: Int = 0,
        var failedIndex: Int = 0,
        var indexed: Int = 0,
        var processed: Int = 0,
    ) {
        val failed get() = failedLoad + failedIndex

        override fun toString(): String = "STATS: processed: $processed, indexed: $indexed, failed: $failed"


        private var lastPrint: Int = 0
        fun stats(every: Int = 1000) {
            if (processed >= lastPrint + every) {
                lastPrint = processed
                println(this)
            }
        }
    }

    private fun <E : Entity> indexEntities(repository: BatchAll<E>, bsize: Int = 100, skip: Int = 0) {
        val s = Stats()

        var loadingBatchSize: Int = bsize

        val toSave = mutableListOf<E>()

        do {
            var interupted = false
            val sessions = try {
                repository.allAsIterable(skip + s.processed, loadingBatchSize, false).iterator()
            } catch (t: Throwable) {
                println("\nFAILED Loading batch (batch size = $loadingBatchSize) " + t.message)
                interupted = true
                if (loadingBatchSize == 1) {
                    s.processed++
                    s.failedLoad++
                    s.stats(10 * bsize)
                } else {
                    loadingBatchSize /= 2
                }
                continue
            }

            loadingBatchSize = bsize

            while (true) {
                val entity: E = try {
                    if (!sessions.hasNext()) break
                    s.processed++
                    sessions.next()

                } catch (t: Throwable) {
                    println("\nFAILED Loading next " + t.message)
                    interupted = true
                    s.processed++
                    s.failedLoad++
                    break
                }

                toSave += entity


                if (toSave.size == bsize)
                    try {
                        val r = indexer.saveAll(toSave)
                        val saved = r.processed - r.failed

                        println("INDEXING (indexed:${saved} failed: ${r.failed})")
                        r.failures?.let { println(it) }

                        s.failedIndex += r.failed
                        s.indexed += saved
                    } catch (t: Throwable) {
                        println("\nINDEXING FAILED" + t.message)
                    } finally {
                        toSave.clear()
                    }
                s.stats(10 * bsize)
            }
        } while (interupted)

        println("=".repeat(50))
        println(s)
        println("DONE")
    }
}
