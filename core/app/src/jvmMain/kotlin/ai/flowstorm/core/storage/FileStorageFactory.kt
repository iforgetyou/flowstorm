package ai.flowstorm.core.storage

import ai.flowstorm.common.config.Config
import ai.flowstorm.storage.aws.AmazonS3Storage
import ai.flowstorm.storage.google.GoogleStorage
import ai.flowstorm.storage.local.LocalFileStorage
import org.glassfish.hk2.api.Factory
import java.io.File
import javax.inject.Inject

class FileStorageFactory : Factory<FileStorage> {

    @Inject
    lateinit var config: Config

    val defaultStorageName get() = "filestore-${config.dataSuffix}"

    fun provide(type: StorageType) = when(type) {
        StorageType.FileSystem -> LocalFileStorage(File(config.get("storage.base", "/storage")))
        StorageType.AmazonS3 -> AmazonS3Storage(config.get("storage.name", defaultStorageName), config)
        StorageType.Google -> GoogleStorage(config.get("storage.name", defaultStorageName))
    }

    override fun provide() = provide(StorageType.valueOf(config.get("storage.type", "Google")))

    override fun dispose(storage: FileStorage?) {}
}