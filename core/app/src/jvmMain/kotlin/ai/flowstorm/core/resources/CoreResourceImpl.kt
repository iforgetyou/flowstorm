package ai.flowstorm.core.resources

import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.core.*
import ai.flowstorm.core.model.*
import ai.flowstorm.core.nlp.pipeline.Pipeline
import ai.flowstorm.core.nlp.pipeline.PipelineConfig
import ai.flowstorm.core.nlp.pipeline.PipelineHandler
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.security.Digest
import ai.flowstorm.util.Log
import java.util.*
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.MediaType
import kotlin.properties.Delegates

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
class CoreResourceImpl : CoreResource {

    @Inject
    lateinit var pairingResource: DevicePairingResource

    @Inject
    lateinit var pipeline: Pipeline

    private val coreUrl by lazy { ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.core) }

    override fun process(request: Request): Response {

        val pipelineConfig = with (request) {
            PipelineConfig(appKey, token, deviceId, sessionId ?: Digest.sha1(), input.locale, input.zoneId.id)
        }
        var pipelineScope: PipelineScope by Delegates.notNull()
        val responseItems = mutableListOf<Response.Item>()
        var response: Response by Delegates.notNull()

        pipelineScope = pipeline.createScope(pipelineConfig, object : PipelineHandler {

            override fun onProcessError(obj: Any, e: Throwable) {
                response = processException(pipelineScope, request, e)
            }

            override fun onRecognitionError(error: Throwable, source: String) {
                response = processException(pipelineScope, request, error)
            }

            override fun onRecognizedInput(input: Input, isFinal: Boolean) {
            }

            override fun onResponseItem(item: Response.Item) {
                responseItems += item
            }

            override fun onResponseDone(logs: List<Log.Entry>, sessionEnded: Boolean, sleepTimeout: Int, turnId: String) {
                response = Response(logs, responseItems, sessionEnded, sleepTimeout, turnId)
            }
        })
        pipelineScope.process(request.input, null)

        return response
    }

    private fun processException(pipelineScope: PipelineScope, request: Request, e: Throwable): Response {
        val type = e::class.simpleName!!
        var code = 1
        val text: String?
        e.printStackTrace()
        when (e) {
            is WebApplicationException -> {
                code = e.response.status
                text = if (e.response.hasEntity())
                    e.response.readEntity(String::class.java)
                else
                    e.message
            }
            else ->
                text = (e.cause?:e).message
        }
        pipelineScope.log.logger.error(DialogueEvent.toText(e))

        return Response(pipelineScope.log.entries, mutableListOf<Response.Item>().apply {
            if (text?.startsWith("studio:NotFoundException: Device") == true) {
                val devicePairing = DevicePairing(deviceId = request.deviceId)
                pairingResource.createOrUpdateDevicePairing(devicePairing)
                val pairingCode = devicePairing.pairingCode.toCharArray().joinToString(", ")
                if (request.input.locale.language == "cs")
                    add(
                        Response.Item(ttsConfig = TtsConfig.forLanguage("cs"),
                        text = getMessageResourceString("cs", "PAIRING", listOf(pairingCode))))
                else
                    add(
                        Response.Item(ttsConfig = TtsConfig.forLanguage("en"),
                        text = getMessageResourceString("en", "PAIRING", listOf(pairingCode))))
            } else {
                if (request.input.locale.language == "cs")
                    add(
                        Response.Item(ttsConfig = TtsConfig.forLanguage("cs"),
                        text = getMessageResourceString("cs", type, listOf(code))))
                else
                    add(
                        Response.Item(ttsConfig = TtsConfig.forLanguage("en"),
                        text = getMessageResourceString("en", type, listOf(code))))
                if (text != null)
                    add(
                        Response.Item(ttsConfig = TtsConfig.forLanguage("en"),
                        text = text))
            }
        }, true)
    }

    private fun getMessageResourceString(language: String, type: String, params: List<Any> = listOf()): String {
        val resourceBundle = ResourceBundle.getBundle("messages", Locale(language))
        val key = if (resourceBundle.containsKey(type)) type else "OTHER"

        return resourceBundle.getString(key).replace("\\{(\\d)\\}".toRegex()) {
            params[it.groupValues[1].toInt()].toString()
        }
    }

    override fun twilioText(
            appKey: String,
            text: String,
            deviceId: String,
            zip: String?,
            city: String?,
            state: String?,
            country: String
    ): String {
        val input = input(text, Locale.getAvailableLocales().firstOrNull { it.country == country } ?: Defaults.locale)
        val request = Request(appKey, deviceId, sessionId = "SMS${deviceId.replace("+", "00")}", input = input)
        val response = process(request)
        return """
            <?xml version="1.0" encoding="UTF-8"?>
            <Response>
                <Message>${response.text()}</Message>
            </Response>
        """.trimIndent()
    }

    override fun twilioCall(
            appKey: String,
            deviceId: String?,
            zip: String?,
            city: String?,
            state: String?,
            country: String?
    ): String {
        return """
            <?xml version="1.0" encoding="UTF-8"?>
            <Response>
                <Connect>
                    <Stream url="$coreUrl/call/">
                        <Parameter name="zip" value="$zip" />
                        <Parameter name="city" value="$city" />
                        <Parameter name="state" value="$state" />
                        <Parameter name="country" value="$country" />
                        <Parameter name="deviceId" value="$deviceId" />
                        <Parameter name="appKey" value="$appKey" />
                    </Stream>  
                </Connect>
            </Response>
        """.trimIndent()
    }
}