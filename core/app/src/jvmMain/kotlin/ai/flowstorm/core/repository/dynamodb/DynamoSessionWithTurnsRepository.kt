@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")

package ai.flowstorm.core.repository.dynamodb

import org.litote.kmongo.Id
import ai.flowstorm.common.query.Query
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.model.User
import ai.flowstorm.core.repository.SessionRepository
import ai.flowstorm.core.repository.TurnRepository
import javax.inject.Inject

class DynamoSessionWithTurnsRepository(@Inject val repo:DynamoSessionRepository) : SessionRepository by repo {

    @Inject
    lateinit var turnRepository: TurnRepository

    override fun findBy(userId: Id<User>): List<Session> = repo.findBy(userId).apply { load(this) }

    override fun findBy(sessionId: String): Session? = repo.findBy(sessionId).apply { load(this) }

    override fun find(id: Id<Session>): Session? =repo.find(id).apply { load(this) }

    override fun find(query: Query)= repo.find(query).apply { load(this) }

    override fun all(): List<Session> = repo.all().apply { load(this) }

    private fun load(sessions: List<Session>) = sessions.filter { it.turns.isEmpty() }.let { emptySessions ->
        val turns = turnRepository.findBy(emptySessions.map { it._id })
        emptySessions.forEach { s ->
            s.turns = turns.filter { turn -> turn.session_id == s._id }
        }
    }

    private fun load(session: Session?) = session?.apply {
        if (turns.isEmpty()) {
            turns = turnRepository.findBy(session._id)
        }
    }
}
