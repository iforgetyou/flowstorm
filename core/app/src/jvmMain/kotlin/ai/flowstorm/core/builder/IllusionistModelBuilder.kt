package ai.flowstorm.core.builder

import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.client.RestClient
import ai.flowstorm.common.config.ConfigValue
import ai.flowstorm.core.builder.IntentModelBuilder.Output
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.model.IntentModel
import ai.flowstorm.nlp.normalizer.Normalizer
import ai.flowstorm.util.LoggerDelegate
import org.litote.kmongo.Id
import java.net.URL
import java.util.*
import javax.annotation.PostConstruct
import javax.inject.Inject
import javax.ws.rs.WebApplicationException

class IllusionistModelBuilder : IntentModelBuilder, EntityModelBuilder {

    @ConfigValue("illusionist.apiKey")
    lateinit var apiKey: String

    @ConfigValue("illusionist.embedding", "UniversalSentenceEncoder")
    lateinit var embedding: String

    @ConfigValue("illusionist.algorithm", "hybrid")
    lateinit var algorithm: String

    @Inject
    lateinit var normalizer: Normalizer

    companion object {
        const val buildTimeout = 180000
        const val outOfDomainActionName = "outofdomain"
    }

    private val logger by LoggerDelegate()
    private val url by lazy {
        with (ServiceUrlResolver) {
            when (detectedRunMode) {
                ServiceUrlResolver.RunMode.container -> getEndpointUrl(ServiceUrlResolver.Service.illusionist_training)
                else -> getEndpointUrl(ServiceUrlResolver.Service.illusionist).let {
                    if (it.startsWith("http://localhost") || it.startsWith("http://flowstorm-illusionist"))
                        getEndpointUrl(ServiceUrlResolver.Service.illusionist_training)
                    else
                        it
                }
            }
        } + "/training"
    }

    @PostConstruct
    fun init() {
        logger.info("Created with API URL $url (embedding=$embedding, algorithm=$algorithm)")
    }

    override fun build(irModel: IntentModel, language: Locale, intents: List<AbstractDialogue<*>.Intent>) {
        build(irModel.id, irModel.name, language, intents)
    }


    override fun build(modelId: String, name: String, language: Locale, intents: List<AbstractDialogue<*>.Intent>) {
        val items = mutableMapOf<String, Output.Item>()
        intents.forEach { intent ->
            val positiveExamples = (intent.utterances.toSet()).plus(intent.utterances.map { s -> normalizer.normalize(s, language.language) }).toTypedArray()
            val negativeExamples = (intent.negativeUtterances.toSet()).plus(intent.negativeUtterances.map { s -> normalizer.normalize(s, language.language) }).toTypedArray()
            items[intent.name] = Output.Item(positiveExamples, negativeExamples, intent.id.toString(), intent.threshold)
        }

        build(modelId, name, language, items)
    }

    override fun build(modelId: String, name: String, language: Locale, intents: Map<String, IntentModelBuilder.Output.Item>) {
        val output = Output(Output.Model(name, language.toString(), embedding = embedding, algorithm = algorithm), intents)

        val url = URL("$url/models/$modelId?key=$apiKey")
//        logger.info("$url < $output")
        try {
            RestClient.call(url, "POST", output = output, timeout = buildTimeout)
        } catch (e: WebApplicationException) {
            RestClient.call(url, "PUT", output = output, timeout = buildTimeout)
        }
        logger.info("Built intent model name=$name, id=$modelId")
    }

    override fun trainEntityModel(dataset: EntityDataset) {
        val url = URL("$url/entity/${dataset._id}?space_id=${dataset.space_id}&language=${dataset.language}" )
        RestClient.call(url, "PUT", output = dataset, timeout = buildTimeout)
    }

    override fun modelStatus(id: Id<EntityDataset>): EntityDataset.Status? {
        val url = URL("$url/entity/$id/status" )
        try {
            return RestClient.call(url, responseType = EntityDataset.Status::class.java, timeout = buildTimeout)
        } catch (e: WebApplicationException) {
            return null
        }
    }

    override fun deleteEntityModel(id: Id<EntityDataset>) {
        val url = URL("$url/entity/$id" )
        RestClient.call(url, "DELETE", timeout = buildTimeout)
    }
}