package ai.flowstorm.core.resources

import ai.flowstorm.core.repository.JobLogRepository
import ai.flowstorm.core.job.*
import ai.flowstorm.core.model.JobLog
import ai.flowstorm.util.LoggerDelegate
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import org.glassfish.hk2.api.IterableProvider
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.reflect.full.findAnnotation

@Path("/job")
@Produces(MediaType.APPLICATION_JSON)
class JobResourceImpl : JobResource {

    @Inject
    lateinit var boundJobs: IterableProvider<Job>

    @Inject
    lateinit var jobLogRepository: JobLogRepository

    private val logger by LoggerDelegate()

    private fun run(job: Job): JobLog? {
        val name = job::class.qualifiedName ?: "unknown"
        var jobLog = jobLogRepository.findBy(name, JobLog.Status.Running)
        return if (jobLog != null && jobLog.status == JobLog.Status.Running) {
            val now = Clock.System.now()
            if (jobLog.time.toEpochMilliseconds() + Job.TIMEOUT < now.toEpochMilliseconds()) {
                jobLog.status = JobLog.Status.TimedOut
                jobLogRepository.update(jobLog)
                logger.warn("Job ${jobLog.name} timed out")
            } else {
                logger.warn("Job ${jobLog.name} still running, cannot run again")
            }
            jobLog
        } else {
            jobLog = JobLog(name = name)
            if ((job::class.findAnnotation<ScheduleAt>()?.isScheduled(jobLog.time) ?:
                job::class.findAnnotation<ScheduleEvery>()?.isScheduled(jobLog.time)) == true
            ) {
                try {
                    jobLogRepository.create(jobLog)
                    logger.info(jobLog.toString())
                    job.run()
                    jobLog.status = JobLog.Status.Finished
                } catch (e: Throwable) {
                    jobLog.status = JobLog.Status.Failed
                    jobLog.result = e.message
                } finally {
                    jobLogRepository.update(jobLog)
                    logger.info(jobLog.toString())
                }
                jobLog
            } else
                null
        }
    }

    override fun run(name: String) = boundJobs.firstOrNull { it::class.qualifiedName == name }?.let {
        run(it)
    }

    override fun runAll() = mutableListOf<JobLog>().apply {
        boundJobs.forEach { job ->
            run(job)?.let { jobLog ->
                add(jobLog)
            }
        }
    }

    private fun ScheduleAt.isScheduled(time: Instant) =
        (month == -1 || month == time.toLocalDateTime(TimeZone.currentSystemDefault()).monthNumber) &&
        (day == -1 || day == time.toLocalDateTime(TimeZone.currentSystemDefault()).dayOfMonth) &&
        (hours == -1 || hours == time.toLocalDateTime(TimeZone.currentSystemDefault()).hour) &&
        (minutes == -1 || minutes == time.toLocalDateTime(TimeZone.currentSystemDefault()).minute)

    private fun ScheduleEvery.isScheduled(time: Instant) =
        (month == -1 || time.toLocalDateTime(TimeZone.currentSystemDefault()).monthNumber.mod(month) == 0) &&
        (day == -1 || time.toLocalDateTime(TimeZone.currentSystemDefault()).dayOfMonth.mod(day) == 0) &&
        (hours == -1 || time.toLocalDateTime(TimeZone.currentSystemDefault()).hour.mod(hours) == 0) &&
        (minutes == -1 || time.toLocalDateTime(TimeZone.currentSystemDefault()).minute.mod(minutes) == 0)
}