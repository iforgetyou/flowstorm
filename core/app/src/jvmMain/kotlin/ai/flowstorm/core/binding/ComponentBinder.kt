package ai.flowstorm.core.binding

import ai.flowstorm.common.ResourceBinder
import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.binding.MongoDatabaseFactory
import ai.flowstorm.common.config.Config
import ai.flowstorm.common.indexer.ElasticClientFactory
import ai.flowstorm.common.indexer.Elastic
import ai.flowstorm.common.messaging.MailgunSender
import ai.flowstorm.common.messaging.MessageSender
import ai.flowstorm.common.messaging.StdOutSender
import ai.flowstorm.common.mongo.KMongoIdParamConverterProvider
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.common.monitoring.SentryMonitoring
import ai.flowstorm.common.monitoring.LoggerMonitoring
import ai.flowstorm.common.query.Query
import ai.flowstorm.common.query.QueryValueFactory
import ai.flowstorm.common.security.ApiKeyAuthorizationAdapter
import ai.flowstorm.common.security.AuthorizationAdapter
import ai.flowstorm.connector.*
import ai.flowstorm.connector.mailgun.Contact
import ai.flowstorm.connector.mailgun.Mailgun
import ai.flowstorm.connector.reddit.RedditConnector
import ai.flowstorm.core.BasicContext
import ai.flowstorm.core.Context
import ai.flowstorm.core.DatabaseType
import ai.flowstorm.core.DefaultPipeline
import ai.flowstorm.core.builder.DialogueBuilder
import ai.flowstorm.core.builder.EntityModelBuilder
import ai.flowstorm.core.builder.IllusionistModelBuilder
import ai.flowstorm.core.builder.IntentModelBuilder
import ai.flowstorm.core.connector.CommunityConnector
import ai.flowstorm.core.connector.FileConnector
import ai.flowstorm.core.job.HeartbeatJob
import ai.flowstorm.connector.reddit.job.RedditJob
import ai.flowstorm.core.indexer.RuntimeDataIndexer
import ai.flowstorm.core.indexer.RuntimeDataIndexerImpl
import ai.flowstorm.core.job.Job
import ai.flowstorm.core.nlp.pipeline.DialogueManager
import ai.flowstorm.core.nlp.pipeline.Pipeline
import ai.flowstorm.core.nlp.stt.SttStreamFactory
import ai.flowstorm.core.repository.*
import ai.flowstorm.core.repository.dynamodb.*
import ai.flowstorm.core.repository.mongo.*
import ai.flowstorm.core.resources.ContentDistributionResource
import ai.flowstorm.core.resources.DevicePairingResource
import ai.flowstorm.core.resources.DevicePairingResourceImpl
import ai.flowstorm.core.runtime.Loader
import ai.flowstorm.core.storage.FileResourceLoaderFactory
import ai.flowstorm.core.storage.FileStorage
import ai.flowstorm.core.storage.FileStorageFactory
import ai.flowstorm.nlp.*
import ai.flowstorm.nlp.dm.DialogueFactory
import ai.flowstorm.nlp.dm.DialogueManagerV2
import ai.flowstorm.nlp.normalizer.Normalizer
import ai.flowstorm.nlp.normalizer.provider.RemoteTextNormalizationRulesProvider
import ai.flowstorm.nlp.normalizer.provider.TextNormalizationRulesProvider
import ai.flowstorm.nlp.pipeline.ContextPersister
import ai.flowstorm.nlp.pipeline.PipelineAudioRecorder
import ai.flowstorm.nlp.pipeline.SessionFactory
import ai.flowstorm.nlp.profanity.ProfanityFilter
import ai.flowstorm.nlp.skimmer.Skimmer
import ai.flowstorm.nlp.stt.AnySttStreamFactory
import ai.flowstorm.nlp.stt.amazon.AmazonSttStreamFactory
import ai.flowstorm.nlp.stt.deepgram.DeepgramSttStreamFactory
import ai.flowstorm.nlp.stt.google.GoogleSttStreamFactory
import ai.flowstorm.nlp.stt.microsoft.MicrosoftSttStreamFactory
import ai.flowstorm.nlp.tts.TtsService
import ai.flowstorm.nlp.tts.TtsProcessor
import ai.flowstorm.nlp.tts.amazon.AmazonTtsService
import ai.flowstorm.nlp.tts.google.GoogleTtsService
import ai.flowstorm.nlp.tts.lovo.LovoTtsService
import ai.flowstorm.nlp.tts.microsoft.MicrosoftTtsService
import ai.flowstorm.util.Log
import ai.flowstorm.util.RunLog
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.mongodb.client.MongoDatabase
import org.elasticsearch.client.RestHighLevelClient
import org.glassfish.hk2.api.PerLookup
import org.glassfish.jersey.process.internal.RequestScoped
import javax.inject.Singleton
import javax.ws.rs.ext.ParamConverterProvider

class ComponentBinder(private val config: Config) : ResourceBinder() {
    override fun configure() {
        (if (config.getOrNull("sentry.dsn") != null)
            bind(SentryMonitoring::class.java)
        else
            bind(LoggerMonitoring::class.java)
                ).to(Monitoring::class.java).`in`(Singleton::class.java)
        bindFactory(FileResourceLoaderFactory::class.java).to(Loader::class.java).`in`(Singleton::class.java)
        bindFactory(FileStorageFactory::class.java).to(FileStorage::class.java).`in`(Singleton::class.java)
        bindAsContract(FileStorageFactory::class.java)
        bind(DevicePairingResourceImpl::class.java).to(DevicePairingResource::class.java) // obsolete - should be implemented as DevicePairingRepository

        /**
         * NLP pipeline components
         */
        bind(DefaultPipeline::class.java).to(Pipeline::class.java).`in`(Singleton::class.java)
        bindAsContract(DialogueManagerV2::class.java).to(DialogueManager::class.java).`in`(Singleton::class.java)
        bindAsContract(DialogueFactory::class.java).`in`(Singleton::class.java)

        bind(BasicContext::class.java).to(Context::class.java).`in`(PerLookup::class.java)
        bindAsContract(ContextPersister::class.java).`in`(Singleton::class.java)
        bindAsContract(SessionFactory::class.java).`in`(Singleton::class.java)

        // STT services and other pipeline streams
        bind(GoogleSttStreamFactory::class.java).to(SttStreamFactory::class.java).`in`(Singleton::class.java)
        bind(AmazonSttStreamFactory::class.java).to(SttStreamFactory::class.java).`in`(Singleton::class.java)
        bind(MicrosoftSttStreamFactory::class.java).to(SttStreamFactory::class.java).`in`(Singleton::class.java)
        bind(DeepgramSttStreamFactory::class.java).to(SttStreamFactory::class.java).`in`(Singleton::class.java)
        bindAsContract(AnySttStreamFactory::class.java).`in`(Singleton::class.java)
        bindAsContract(PipelineAudioRecorder::class.java).`in`(Singleton::class.java)

        // TTS services
        bind(GoogleTtsService::class.java).to(TtsService::class.java).`in`(Singleton::class.java)
        bind(AmazonTtsService::class.java).to(TtsService::class.java).`in`(Singleton::class.java)
        bind(MicrosoftTtsService::class.java).to(TtsService::class.java).`in`(Singleton::class.java)
        bind(LovoTtsService::class.java).to(TtsService::class.java).`in`(Singleton::class.java)
        bindAsContract(TtsProcessor::class.java).`in`(Singleton::class.java)

        // Components
        bindAsContract(ActionResolver::class.java).`in`(Singleton::class.java)
        bindAsContract(ProfanityFilter::class.java).`in`(Singleton::class.java)
        bindAsContract(InternalTokenizer::class.java).`in`(Singleton::class.java)
        bindAsContract(IllusionistIR::class.java).`in`(Singleton::class.java)
        bindAsContract(IllusionistNER::class.java).`in`(Singleton::class.java)
        bindAsContract(SimpleNER::class.java).`in`(Singleton::class.java)
        bindAsContract(Duckling::class.java).`in`(Singleton::class.java)
        bindAsContract(Triton::class.java).`in`(Singleton::class.java)
        bindAsContract(Skimmer::class.java).`in`(Singleton::class.java)
        bind(RemoteTextNormalizationRulesProvider::class.java).to(TextNormalizationRulesProvider::class.java).`in`(Singleton::class.java)
        bindAsContract(Normalizer::class.java).`in`(Singleton::class.java)

        // Connectors
        bindAsContract(CommunityConnector::class.java).`in`(Singleton::class.java)
        bindAsContract(FileConnector::class.java).`in`(Singleton::class.java)
        bindAsContract(SangixConnector::class.java).`in`(Singleton::class.java)
        bindAsContract(TmdbConnector::class.java).`in`(Singleton::class.java)
        bindAsContract(TmdbConnectorV2::class.java).`in`(Singleton::class.java)
        bindAsContract(LastFMConnector::class.java).`in`(Singleton::class.java)
        bindAsContract(ParkopediaConnector::class.java).`in`(Singleton::class.java)
        bindAsContract(WcitiesConnector::class.java).`in`(Singleton::class.java)
        bindAsContract(RedditConnector::class.java).`in`(Singleton::class.java)

        /**
         * Scheduled jobs
         */
        bind(MongoJobLogRepository::class.java).to(JobLogRepository::class.java)
        bind(HeartbeatJob::class.java).to(Job::class.java).named("heartbeat-job")
        bind(RedditJob::class.java).to(Job::class.java).named("reddit-job")

        /**
         * Other components
         */
        bind(StdOutSender::class.java).to(MessageSender::class.java).`in`(Singleton::class.java)

        when (DatabaseType.valueOf(config.get("database.type", "Mongo"))) {
            DatabaseType.Mongo -> {
                bindFactory(MongoDatabaseFactory::class.java).to(MongoDatabase::class.java).`in`(Singleton::class.java)
                bind(MongoCommunityRepository::class.java).to(CommunityRepository::class.java).`in`(Singleton::class.java)
                bind(MongoPropertyRepository::class.java).to(PropertyRepository::class.java).`in`(Singleton::class.java)
                bind(MongoProfileRepository::class.java).to(ProfileRepository::class.java).`in`(Singleton::class.java)
                bind(MongoSessionRepository::class.java).to(SessionRepository::class.java).`in`(Singleton::class.java)
                bind(MongoTurnRepository::class.java).to(TurnRepository::class.java).`in`(Singleton::class.java)
                bind(MongoClientEventRepository::class.java).to(ClientEventRepository::class.java).`in`(Singleton::class.java)
                bind(MongoDialogueEventRepository::class.java).to(DialogueEventRepository::class.java).`in`(Singleton::class.java)
            }
            DatabaseType.Dynamo -> {
                bindTo(DynamoDB::class.java, DynamoDB(AmazonDynamoDBClientBuilder.standard().build()))
                bind(DynamoCommunityRepository::class.java).to(CommunityRepository::class.java).`in`(Singleton::class.java)
                bind(DynamoPropertyRepository::class.java).to(PropertyRepository::class.java).`in`(Singleton::class.java)
                bind(DynamoProfileRepository::class.java).to(ProfileRepository::class.java).`in`(Singleton::class.java)
                bind(DynamoSessionRepository::class.java).to(SessionRepository::class.java).`in`(Singleton::class.java)
                bind(DynamoTurnRepository::class.java).to(TurnRepository::class.java).`in`(Singleton::class.java)
                bind(DynamoClientEventRepository::class.java).to(ClientEventRepository::class.java).`in`(Singleton::class.java)
                bind(DynamoDialogueEventRepository::class.java).to(DialogueEventRepository::class.java).`in`(Singleton::class.java)
            }
        }
        bind(RunLog::class.java).to(Log::class.java).`in`(Singleton::class.java)

        bind(KMongoIdParamConverterProvider::class.java).to(ParamConverterProvider::class.java).`in`(Singleton::class.java)
        bindFactory(QueryValueFactory::class.java).to(Query::class.java).`in`(PerLookup::class.java)

        bindAsContract(Elastic::class.java).`in`(Singleton::class.java)
        config.getOrNull("es.host")?.let {
            bindFactory(ElasticClientFactory::class.java).to(RestHighLevelClient::class.java).`in`(Singleton::class.java)
        }

        bind(RuntimeDataIndexerImpl::class.java).to(RuntimeDataIndexer::class.java).`in`(Singleton::class.java)

        // Builder
        bind(IllusionistModelBuilder::class.java).to(IntentModelBuilder::class.java).`in`(Singleton::class.java)
        bind(IllusionistModelBuilder::class.java).to(EntityModelBuilder::class.java).`in`(Singleton::class.java)
        bindAsContract(DialogueBuilder::class.java)

        // Mailgun
        val mailgun = Mailgun.Builder(config["mailgun.domain"], config["mailgun.apikey"])
            .baseUrl(config["mailgun.baseUrl"])
            .build()

        bind(
            MailgunSender(mailgun, Contact(
                config.get("sender.from.email", "bot@flowstorm.ai"),
                config.get("sender.from.name", "Flowstorm Bot"))
            )
        ).to(MessageSender::class.java).ranked(100)

        // Content distribution service
        bindTo(ContentDistributionResource::class.java, ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.studio) + "/contentDistribution")

        bind(ApiKeyAuthorizationAdapter::class.java).to(AuthorizationAdapter::class.java).`in`(RequestScoped::class.java)
    }
}
