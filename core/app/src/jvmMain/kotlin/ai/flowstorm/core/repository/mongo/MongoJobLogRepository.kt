package ai.flowstorm.core.repository.mongo

import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.repository.JobLogRepository
import ai.flowstorm.core.model.JobLog
import org.litote.kmongo.eq
import org.litote.kmongo.find

class MongoJobLogRepository : MongoAbstractEntityRepository<JobLog>(JobLog::class), JobLogRepository {

    override fun findBy(name: String, status: JobLog.Status) =
        collection.find(JobLog::name eq name, JobLog::status eq status).lastOrNull()
}