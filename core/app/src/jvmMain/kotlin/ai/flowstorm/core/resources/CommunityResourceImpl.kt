package ai.flowstorm.core.resources

import ai.flowstorm.common.security.Authorized
import ai.flowstorm.core.model.Community
import ai.flowstorm.core.repository.CommunityRepository
import javax.inject.Inject
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/communities")
@Produces(MediaType.APPLICATION_JSON)
@Authorized
class CommunityResourceImpl : CommunityResource {

    @Inject
    lateinit var communityRepository: CommunityRepository

    override fun getCommunities() = communityRepository.all()

    override fun getCommunitiesInSpace(spaceId: String) = communityRepository.getCommunitiesInSpace(spaceId)

    override fun get(communityName: String, spaceId: String) = communityRepository.get(communityName, spaceId)

    override fun create(community: Community) { communityRepository.create(community) }

    override fun update(community: Community) { communityRepository.update(community, true) }
}