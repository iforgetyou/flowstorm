package ai.flowstorm.core.builder

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.AppenderBase

class BuildLogAppender : AppenderBase<ILoggingEvent>() {

    override fun append(event: ILoggingEvent) {
        eventList.get().add(event)
    }
    companion object {
        private val eventList = object : ThreadLocal<MutableList<ILoggingEvent>>() {
            override fun initialValue() = mutableListOf<ILoggingEvent>()
        }

        fun getEvents(): List<String> = with(eventList.get()) {
            val start = first().timeStamp
            return map {
                val time = "%.2f".format((it.timeStamp - start).toFloat() / 1000)
                "[+$time ${it.level}] ${it.message}"
            }
        }

        fun clearEvents() = eventList.get().clear()
    }
}