package ai.flowstorm.core.cli

import ai.flowstorm.core.storage.FileStorageFactory
import kotlinx.cli.ArgType
import kotlinx.cli.Subcommand
import kotlinx.cli.required
import javax.inject.Inject

class NlpPipelineSubcommand: Subcommand("nlp:pipeline", "NLP Pipeline") {

    private val contentDistributionRef by option(ArgType.String, "ref", "r", "Content distribution reference (URL or file path)").required()

    @Inject
    private lateinit var storageFactory: FileStorageFactory

    override fun execute() {
        println("Not ready yet")
    }
}