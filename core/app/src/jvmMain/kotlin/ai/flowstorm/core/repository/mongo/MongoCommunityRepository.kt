package ai.flowstorm.core.repository.mongo

import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.Community
import ai.flowstorm.core.repository.CommunityRepository
import org.litote.kmongo.eq

class MongoCommunityRepository : MongoAbstractEntityRepository<Community>(Community::class), CommunityRepository {

    override fun getCommunitiesInSpace(spaceId: String) = collection.find(Community::space_id eq spaceId).toList()
}