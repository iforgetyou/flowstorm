package ai.flowstorm.core

import ai.flowstorm.connector.*
import ai.flowstorm.nlp.Nrg
import ai.flowstorm.connector.reddit.RedditConnector
import ai.flowstorm.core.runtime.Api
import ai.flowstorm.nlp.profanity.ProfanityFilter
import javax.inject.Inject

open class BasicContext : Context() {

    val api = Api
    val nrg = Nrg(this) { profanityFilter }

    @Inject lateinit var profanityFilter: ProfanityFilter
    @Inject lateinit var sangix: SangixConnector
    @Inject lateinit var tmdb: TmdbConnector
    @Inject lateinit var tmdb2: TmdbConnectorV2
    @Inject lateinit var lastFM: LastFMConnector
    @Inject lateinit var parkopedia: ParkopediaConnector
    @Inject lateinit var wcities: WcitiesConnector
    @Inject lateinit var reddit: RedditConnector
}