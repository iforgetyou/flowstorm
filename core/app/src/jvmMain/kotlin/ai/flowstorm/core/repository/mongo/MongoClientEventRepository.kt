package ai.flowstorm.core.repository.mongo

import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.ClientEvent
import ai.flowstorm.core.repository.ClientEventRepository

class MongoClientEventRepository : MongoAbstractEntityRepository<ClientEvent>(ClientEvent::class), ClientEventRepository