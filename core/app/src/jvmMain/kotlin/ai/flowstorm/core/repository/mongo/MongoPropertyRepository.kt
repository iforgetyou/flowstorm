package ai.flowstorm.core.repository.mongo

import ai.flowstorm.common.repository.MongoAbstractEntityRepository
import ai.flowstorm.core.model.Property
import ai.flowstorm.core.repository.PropertyRepository
import org.litote.kmongo.eq
import org.litote.kmongo.match

class MongoPropertyRepository : MongoAbstractEntityRepository<Property>(Property::class), PropertyRepository {

    override fun getValues(key: String) = Pipeline().apply {
        add(match(Property::key eq key))
    }.run().toList().map { it.value }

}