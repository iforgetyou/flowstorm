group = "ai.flowstorm.core"
description = "Flowstorm Core Lib"

val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-multiplatform")
}

kotlin {
    nativeTargets(true)
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":flowstorm-common-lib"))
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
            }
        }
        val jvmMain by getting {
            dependencies {
                api("org.jetbrains.kotlin:kotlin-compiler-embeddable:${findProperty("kotlin.version")}")
            }
        }
        val nativeMain by creating {
            dependsOn(commonMain)
        }
        nativeSourceSets(nativeMain, true)
    }
}

