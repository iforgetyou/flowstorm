package ai.flowstorm.core.type

actual open class DynamicMap<K, V> : MutableMap<K, V> {

    private val map = LinkedHashMap<K, V>()

    actual constructor()
    actual constructor(original: Map<out K, V>) { map.putAll(original) }

    actual override val size get() = map.size
    actual override fun containsKey(key: K) = map.containsKey(key)
    actual override fun containsValue(value: V) = map.containsValue(value)
    actual override fun get(key: K) = map[key]
    actual override fun isEmpty() = map.isEmpty()

    actual override val entries get() = map.entries
    actual override val keys get() = map.keys
    actual override val values get() = map.values
    actual override fun clear() = map.clear()

    actual override fun put(key: K, value: V) = map.put(key, value)
    actual override fun putAll(from: Map<out K, V>) = map.putAll(from)
    actual override fun remove(key: K) = map.remove(key)
}