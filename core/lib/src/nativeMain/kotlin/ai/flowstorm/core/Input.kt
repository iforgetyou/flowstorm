package ai.flowstorm.core

import ai.flowstorm.util.Locale

actual class Input : InputBase() {
    data class Transcript(
        var text: String,
        val confidence: Float = 1.0F,
        val provider: String? = null,
        val locale: Locale? = null,
        var normalizedText: String = text
    )
}