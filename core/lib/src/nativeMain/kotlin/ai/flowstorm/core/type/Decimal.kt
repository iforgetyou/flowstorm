package ai.flowstorm.core.type

actual class Decimal {

    private val i: Long
    private val d: Int

    actual constructor(v: Int) { i = v.toLong(); d = 0 }
    actual constructor(v: Long) { i = v; d = 0 }
    actual constructor(v: Double) : this(v.toString())
    actual constructor(v: String) {
        v.split(',').let {
            i = it[0].toLong()
            d = if (it.isNotEmpty()) it[1].toInt() else 0
        }
    }
}