package ai.flowstorm.core.runtime

import ai.flowstorm.core.Context
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.dialogue.AbstractDialogue.Run
import ai.flowstorm.util.LoggerDelegate

object DialogueRuntime {

    private val logger by LoggerDelegate()
    private val threadRun = ThreadLocal<Run>()

    val isRunning get() = (threadRun.get() != null)

    val run get() = threadRun.get() ?: error("Dialogue is not running")

    fun ifRunning(block: Run.() -> Unit) {
        if (isRunning)
            block(run)
    }

    fun <T: Any?> run(context: Context, node: AbstractDialogue<*>.Node, block: () -> T): T {
        logger.info("Running ${node.javaClass.simpleName} ${node.dialogue.dialogueName}#${node.id}")
        val alreadyRunning = isRunning
        val securityManager = System.getSecurityManager() as? DialogueSecurityManager
        if (alreadyRunning && run.node != node)
            context.logger.warn("Dialogue nested run using different node ($node != ${run.node})")
        try {
            if (!alreadyRunning)
                threadRun.set(Run(node, context))
            securityManager?.enable()
            return block()
        } catch (e: Throwable) {
            throw DialogueException(node, e)
        } finally {
            securityManager?.disable()
            if (!alreadyRunning)
                threadRun.remove()
        }
    }
}