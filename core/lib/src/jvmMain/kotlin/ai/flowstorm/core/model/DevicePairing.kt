package ai.flowstorm.core.model

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import kotlin.math.abs

data class DevicePairing(
        val _id: Id<DevicePairing> = newId(),
        val deviceId: String,
        val pairingCode: String = (abs(deviceId.hashCode()).rem(90000) + 10000).toString(),
        val date: Instant = Clock.System.now()
)