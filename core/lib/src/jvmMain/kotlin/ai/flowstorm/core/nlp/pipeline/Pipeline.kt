package ai.flowstorm.core.nlp.pipeline

interface Pipeline {

    val openStream: PipelineOpenStreamComponent
    val processInput: PipelineComponent
    val processReInput: PipelineComponent
    val processResponseItem: PipelineResponseItemComponent

    fun createScope(pipelineConfig: PipelineConfig, pipelineHandler: PipelineHandler): PipelineScope
}
