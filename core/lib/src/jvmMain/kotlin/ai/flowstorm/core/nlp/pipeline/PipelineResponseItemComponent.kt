package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.core.Response

fun interface PipelineResponseItemComponent {
    fun process( scope: PipelineScope, item: Response.Item): List<Response.Item>
}
