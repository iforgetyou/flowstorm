package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import ai.flowstorm.common.model.Id
import ai.flowstorm.common.model.newId
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

data class JobLog(
    override val _id: Id<Profile> = newId(),
    val name: String,
    val time: Instant = Clock.System.now(),
    var endTime: Instant? = null,
    var status: Status = Status.Running,
    var result: String? = null,
    var output: String? = null
) : Entity {
    enum class Status { Running, Finished, Failed, TimedOut }
}