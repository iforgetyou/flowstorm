package ai.flowstorm.core.storage

import ai.flowstorm.core.ReadOnlyFileStorage
import ai.flowstorm.io.WritableStream
import java.io.InputStream
import java.io.OutputStream

interface FileStorage : ReadOnlyFileStorage {

    companion object {
        const val defaultContentType = "application/octet-stream"
    }

    class NotFoundException(message: String) : Exception(message)

    override fun readFile(path: String, output: OutputStream)
    fun getFile(path: String): StorageFile
    fun getWritableStream(path: String, contentType: String, meta: List<String>): WritableStream
    fun writeFile(path: String, contentType: String, meta: List<String>, input: InputStream)
    fun deleteFile(path: String): Boolean
}