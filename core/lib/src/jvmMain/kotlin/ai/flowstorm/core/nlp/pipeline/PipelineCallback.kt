package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.core.Context
import ai.flowstorm.core.Input
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.io.WritableStream

interface PipelineCallback {

    fun onStreamOpen(stream: WritableStream)
    fun onStreamClose(stream: WritableStream)
    fun onRecognizedInput(input: Input)
    fun onProcessInput(input: Input)
    fun onUserInput(userInput: AbstractDialogue<*>.UserInput, context: Context)
    fun onProcessComponent(component: PipelineComponent)
    fun onResponseDone()
}