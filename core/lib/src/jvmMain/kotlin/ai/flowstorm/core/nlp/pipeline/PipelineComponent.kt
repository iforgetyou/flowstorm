package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.common.monitoring.performance.PerformanceSpan

fun interface PipelineComponent {
    fun process(scope: PipelineScope, span: PerformanceSpan)
}
