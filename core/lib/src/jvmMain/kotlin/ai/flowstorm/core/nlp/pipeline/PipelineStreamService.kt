package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.io.WritableStream

fun interface PipelineStreamService {

    fun createStream(scope: PipelineScope): WritableStream
}