package ai.flowstorm.core.repository

import ai.flowstorm.common.repository.EntityRepository
import ai.flowstorm.core.model.Community

interface CommunityRepository : EntityRepository<Community> {

    fun getCommunitiesInSpace(spaceId: String): List<Community>

    fun get(name: String, spaceId: String) = getCommunitiesInSpace(spaceId).firstOrNull { it.name == name }
}