package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import ai.flowstorm.core.type.Attributes
import ai.flowstorm.core.type.Memory
import ai.flowstorm.core.type.isPrimitive

interface HasAttributes {

    val attributes: Attributes

    @Suppress("unused")
    fun indexedAttributes(): Map<String, Map<String, Map<String, Any>>> = attributes.mapValues { entry ->
        mutableMapOf<String, Map<String, Any>>().also {
            entry.value.forEach { name, memorable ->
                if (memorable is Memory<*> && memorable.indexable && memorable.value.isPrimitive)
                    it[name] = mapOf("_value" to memorable.value)
            }
        }
    }
}
