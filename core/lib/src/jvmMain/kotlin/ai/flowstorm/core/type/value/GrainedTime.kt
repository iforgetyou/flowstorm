package ai.flowstorm.core.type.value

import ai.flowstorm.core.type.DateTime
import com.fasterxml.jackson.annotation.JsonFormat

data class GrainedTime(
    @JsonFormat(without = [JsonFormat.Feature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE]) val value: DateTime,
    val grain: String = ""
): Value()