package ai.flowstorm.core.connector

import ai.flowstorm.core.storage.FileStorage
import java.io.OutputStream
import javax.inject.Inject

class FileConnector {

    @Inject
    lateinit var fileStorage: FileStorage

    fun readFile(path: String, output: OutputStream) = fileStorage.readFile(path, output)
}