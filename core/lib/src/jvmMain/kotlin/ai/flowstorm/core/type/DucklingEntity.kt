package ai.flowstorm.core.type

import ai.flowstorm.core.InputEntity
import ai.flowstorm.core.type.value.*
import ai.flowstorm.core.type.value.Number
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonDeserialize(using = DucklingEntity.Deserializer::class)
class DucklingEntity(val start: Int = 0,
                     val end: Int = 0,
                     val latent: Boolean = false,
                     override val value: Value = Text("")): InputEntity("Text", "", 1.0F, "duckling") {
    var dim: String = ""
        set(value) {
            field = value
            className = Deserializer.mapping[value]?.simpleName ?: error("cannot find value class for duckling dim \"$value\"")
        }

    override var text = ""

    // Custom deserializer since the Duckling format has a mysterious structure
    class Deserializer : JsonDeserializer<DucklingEntity?>() {
        companion object {
            val mapping = mapOf(
                "time" to GrainedTime::class,
                "quantity" to Quantity::class,
                "amount-of-money" to Currency::class,
                "distance" to Distance::class,
                "temperature" to Temperature::class,
                "volume" to Volume::class,
                "credit-card-number" to CreditCard::class,
                "duration" to Duration::class,
                "number" to Number::class,
                "ordinal" to Ordinal::class,
                "email" to Email::class,
                "phone-number" to Phone::class,
                "url" to URL::class,
            )
        }

        override fun deserialize(jsonParser: JsonParser, deserializationContext: DeserializationContext?): DucklingEntity {
            val node: JsonNode = jsonParser.codec.readTree(jsonParser)
            val dimField = node["dim"]?.asText() ?: ""

            val rawValue = node["value"]
            val clazz = mapping[dimField]!!.java
            return DucklingEntity(start = node.get("start")?.intValue() ?: 0,
                                  end = node.get("end")?.intValue() ?: 0,
                                  latent = node.get("latent")?.booleanValue() ?: false,
                                  value = parseValue(rawValue, clazz, jsonParser).apply {
                                      alternativeValues = rawValue["values"]?.map { parseValue(it, clazz, jsonParser) }
                                          ?: listOf()
                                  })
                .apply {
                    text = node.get("body")?.asText() ?: ""
                    dim = dimField
                }
        }

        private fun parseValue(node: JsonNode, clazz: Class<out Value>, jsonParser: JsonParser) =
            if (node.has("type") && node["type"].asText() == "interval") {
                val from = if (node.has("from")) jsonParser.codec.treeToValue(node["from"], clazz) else null
                val to = if (node.has("to")) jsonParser.codec.treeToValue(node["to"], clazz) else null
                Interval(from, to)
            } else {
                jsonParser.codec.treeToValue(node, clazz)!!
            }
    }
}