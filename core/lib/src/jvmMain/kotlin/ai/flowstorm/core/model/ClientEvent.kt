package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.litote.kmongo.Id
import org.litote.kmongo.newId

data class ClientEvent(
    override val _id: Id<Turn> = newId(),
    val datetime: Instant = Clock.System.now(),
    val type: String,
    val category: String,
    val text: String,
    val deviceId: String,
    val sessionId: String? = null,
    val turnId: String? = null,
    val nodeId: String? = null,
) : Entity