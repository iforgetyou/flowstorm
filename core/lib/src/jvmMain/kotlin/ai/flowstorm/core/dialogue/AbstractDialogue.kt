package ai.flowstorm.core.dialogue

import ai.flowstorm.core.Context
import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.model.DialogueModel
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.model.SttConfig
import ai.flowstorm.core.runtime.DialogueRuntime
import ai.flowstorm.core.runtime.DialogueRuntime.run
import ai.flowstorm.core.runtime.DialogueException
import ai.flowstorm.core.runtime.Loader
import ai.flowstorm.core.type.*
import ai.flowstorm.util.Locale
import kotlin.random.Random
import kotlin.reflect.KProperty
import kotlin.reflect.KTypeProjection
import kotlin.reflect.full.createType
import kotlin.reflect.full.isSubtypeOf

abstract class AbstractDialogue<C : Context> : DialogueModel {

    //dialogue config - must/may be overridden
    open val buildId: String = "unknown" // used for generated classes, others are unknown
    open val language: String = "en"
    open val background: String? = null
    open val sttConfig: SttConfig? = null
    open val ttsConfig: TtsConfig? = null
    val locale by lazy { Locale(language) }

    abstract val clientLocation: Location

    //runtime dependencies
    lateinit var loader: Loader
    val logger get() = run.context.logger

    val nodes: MutableSet<Node> = mutableSetOf()
    var nextId: Int = 0

    val enter = Enter(nextId--)
    @Deprecated("Use enter instead.")
    val start = enter

    var goBack = GoBack(Int.MAX_VALUE)

    val exit = Exit(Int.MAX_VALUE - 3)
    @Deprecated("Use exit instead.")
    val stop  = exit

    val end = End(Int.MAX_VALUE - 1)
    @Deprecated("Use end instead.")
    val stopSession = end

    abstract inner class Node(val id: Int) {
        val dialogue get() = this@AbstractDialogue
        val isSingleton by lazy { this is Enter || this is Exit || this is End }
        init { nodes.add(this) }
        override fun hashCode(): Int = id
        override fun equals(other: Any?) = if (other is AbstractDialogue<*>.Node) other.id == id && other.dialogue.dialogueId == dialogueId else false
        override fun toString(): String = stringName + (if (isSingleton) "" else "#$id")
        val stringName: String get() =
            when(this) {
                is StartDialogue -> "Enter"
                is StopDialogue -> "Exit"
                is StopSession -> "End"
                else -> javaClass.simpleName ?: ""
            }
    }

    inner class NodeSelector(
        val node: SelectableNode,
        var scope: Scope = Scope.Session,
        var beforeSelect: Context.(NodeState) -> NodeRef? = { _ -> null },
        var afterSelect: Context.(NodeState) -> Unit = { _ -> }
    ) {
        fun ref(vararg labels: String) = NodeRef(node.id, scope, labels.toSet())
    }

    interface SelectableNode {
        val id: Int
        val selector: AbstractDialogue<*>.NodeSelector
    }

    abstract inner class TransitNode(id: Int): Node(id) {
        lateinit var next: Node
        fun isNextInitialized() = ::next.isInitialized
    }

    data class Transition(val node: AbstractDialogue<*>.Node)

    open inner abstract class Input(
        id: Int,
        open var skipGlobalIntents: Boolean,
        open val intents: Array<Intent>,
        open val actions: Array<Action>,
    ) : Node(id) {
        abstract fun process(context: Context): Transition?
    }

    inner class UserInput(
        id: Int,
        override var skipGlobalIntents: Boolean,
        val sttConfig: SttConfig? = null,
        val expectedPhrases: List<ExpectedPhrase>,
        override val intents: Array<Intent>,
        override val actions: Array<Action>,
        val lambda: (C.(UserInput) -> Transition?)
    ): Input(id, skipGlobalIntents, intents, actions) {

        constructor(id: Int, skipGlobalIntents: Boolean, sttConfig: SttConfig? = null, intents: Array<Intent>, actions: Array<Action>, lambda: (C.(UserInput) -> Transition?)) :
                this(id, skipGlobalIntents, sttConfig, listOf(), intents, actions, lambda)

        constructor(id: Int, skipGlobalIntents: Boolean, intents: Array<Intent>, actions: Array<Action>, lambda: (C.(UserInput) -> Transition?)) :
                this(id, skipGlobalIntents, null, intents, actions, lambda)

        constructor(id: Int, intents: Array<Intent>, actions: Array<Action>, lambda: (C.(UserInput) -> Transition?)) :
                this(id, false, null, intents, actions, lambda)

        constructor(intents: Array<Intent>, actions: Array<Action>, lambda: (C.(UserInput) -> Transition?)) :
                this(nextId--, false, null, intents, actions, lambda)

        override fun process(context: Context): Transition? {
            val transition = run(context, this) { lambda(context as C, this) } as Transition?
            if (transition == null && intents.isEmpty() && actions.isEmpty())
                throw DialogueException(this, Exception("Can not pass processing to pipeline, there are no intents or actions following the user input node."))

            return transition
        }
    }

    inner class ReInput(
        id: Int,
        override var skipGlobalIntents: Boolean,
        override val intents: Array<Intent>,
        override val actions: Array<Action>,
        val lambda: (C.(ReInput) -> Transition?)
    ): Input(id, skipGlobalIntents, intents, actions) {
        override fun process(context: Context) = run(context, this) { lambda(context as C, this) }
    }

    open inner class Intent(
            id: Int,
            open val name: String,
            open val threshold: Float,
            open val entities: List<String>,
            open val requiredEntities: List<String>,
            open val utterance: List<String>,
            open val negativeUtterances: List<String>
    ): TransitNode(id) {
        val utterances get() = utterance

        constructor(id: Int, name: String, threshold: Float, entities: List<String>, requiredEntities: List<String>, vararg utterance: String) : this(id, name, threshold, entities, requiredEntities, utterance.asList(), listOf())
        constructor(id: Int, name: String, threshold: Float, requiredEntities: List<String>, vararg utterance: String) : this(id, name, threshold, listOf(), requiredEntities, *utterance)
        constructor(id: Int, name: String, threshold: Float, vararg utterance: String) : this(id, name, threshold, listOf(), *utterance)
        constructor(id: Int, name: String, vararg utterance: String) : this(id, name, 0.0F, *utterance)
        constructor(name: String, vararg utterance: String) : this(nextId--, name, *utterance)
    }

    inner class GlobalIntent(
            id: Int,
            override val name: String,
            override val threshold: Float,
            override val entities: List<String>,
            override val requiredEntities: List<String>,
            override val utterance: List<String>,
            override val negativeUtterances: List<String>
    ): Intent(id, name, threshold, entities, requiredEntities, utterance, negativeUtterances) {
        constructor(id: Int, name: String, threshold: Float, entities: List<String>, requiredEntities: List<String>, vararg utterance: String) : this(id, name, threshold, entities, requiredEntities, utterance.asList(), listOf())
        constructor(id: Int, name: String, threshold: Float, requiredEntities: List<String>, vararg utterance: String) : this(id, name, threshold, listOf(), requiredEntities,  *utterance)
        constructor(id: Int, name: String, threshold: Float, vararg utterance: String) : this(id, name, threshold, listOf(), *utterance)
        constructor(id: Int, name: String, vararg utterance: String) : this(id, name, 0.0F, *utterance)
    }

    open inner class Action(
            id: Int,
            open val name: String,
            open val action: String
    ): TransitNode(id) {
        constructor(name: String, action: String) : this(nextId--, name, action)
    }

    inner class GlobalAction(
            id: Int,
            override val name: String,
            override val action: String
    ): Action(id, name, action) {
        constructor(name: String, action: String) : this(nextId--, name, action)
    }

    open inner class Response(
        id: Int,
        val isRepeatable: Boolean = true,
        val ttsConfig: TtsConfig? = null,
        val background: String? = null,
        val code: (C.(Response) -> String?) = { null },
        val image: String? = null,
        val audio: String? = null,
        val video: String? = null,
        vararg text: (C.(Response) -> String)
    ): TransitNode(id) {
        val texts = text

        constructor(
            id: Int,
            isRepeatable: Boolean = true,
            ttsConfig: TtsConfig? = null,
            background: String? = null,
            image: String? = null,
            audio: String? = null,
            video: String? = null,
            code: String? = null,
            vararg text: (C.(Response) -> String)
        ) : this(id, isRepeatable, ttsConfig, background, { code }, image, audio, video, *text)

        constructor(
            id: Int,
            isRepeatable: Boolean = true,
            background: String? = null,
            image: String? = null,
            audio: String? = null,
            video: String? = null,
            code: String? = null,
            vararg text: (C.(Response) -> String)
        ) : this(id, isRepeatable, null, background, image, audio, video, code, *text)

        constructor(
            id: Int,
            isRepeatable: Boolean = true,
            background: String? = null,
            image: String? = null,
            audio: String? = null,
            video: String? = null,
            vararg text: (C.(Response) -> String)
        ) : this(id, isRepeatable, background, image, audio, video, null, *text)

        constructor(
            id: Int,
            isRepeatable: Boolean,
            background: String?,
            code: (C.(Response) -> String?),
            ttsConfig: TtsConfig?,
            vararg text: (C.(Response) -> String)
        ) : this(id, isRepeatable, ttsConfig, background, code, null, null, null, *text)

        constructor(
            id: Int,
            isRepeatable: Boolean,
            background: String?,
            ttsConfig: TtsConfig?,
            vararg text: (C.(Response) -> String)
        ) : this(id, isRepeatable, ttsConfig, background, null, null, null, null, *text)

        constructor(
            id: Int,
            isRepeatable: Boolean,
            background: String?,
            vararg text: (C.(Response) -> String)
        ) : this(id, isRepeatable, background, null, null, null, null, *text)

        constructor(
            id: Int,
            isRepeatable: Boolean,
            vararg text: (C.(Response) -> String)
        ) : this(id, isRepeatable, null, null, null, null, null, *text)

        constructor(
            id: Int,
            vararg text: (C.(Response) -> String)
        ) : this(id, true, *text)

        constructor(vararg text: (C.(Response) -> String)) : this(nextId--, true, *text)

        fun getText(context: Context, index: Int = -1) = run(context, this) {
            if (texts.isNotEmpty())
                texts[
                    when {
                        (texts.size == 1) -> 0
                        (index > -1) -> index
                        else -> {
                            val memory = context.profile.attributes[dialogueName].getOrPut("response$id") {
                                Memorable.pack(Memory(0))
                            } as Memory<Int>
                            val shuffledIndexes = List(texts.size) { it }.shuffled(Random(id.xor(context.profile.user_id.hashCode())))
                            val nextIndex = (++memory.value).rem(texts.size)
                            shuffledIndexes[nextIndex]
                        }
                    }
                ](context as C, this)
            else null
        }

        fun getCode(context: Context) = run(context, this) { code.invoke(context as C, this) }
    }

    inner class Function(
            id: Int,
            val lambda: (C.(Function) -> Transition)
    ): Node(id) {
        constructor(lambda: (C.(Function) -> Transition)) : this(nextId--, lambda)
        fun exec(context: Context): Transition = run(context, this) { lambda(context as C, this) } as Transition
    }

    inner class Command(
            id: Int,
            val command: String,
            val lambda: (C.(Command) -> String)
    ): TransitNode(id) {
        constructor(id: Int, command: String, code: String) : this(id, command, { code })
        constructor(command: String, code: String) : this(nextId--, command, { code })
        constructor(command: String, lambda: (C.(Command) -> String)) : this(nextId--, command, lambda)
        fun getCode(context: Context) = run(context, this) { lambda.invoke(context as C, this) }
    }

    inner class SubDialogue(
            id: Int,
            val dialogueId: String,
            val lambda: C.(SubDialogue) -> PropertyMap
    ): TransitNode(id), SelectableNode {

        override val selector = NodeSelector(this)

        fun getConstructorArgs(context: Context): PropertyMap =
                run(context, this) { lambda(context as C, this) } as PropertyMap

        fun create(vararg args: Pair<String, Any>): PropertyMap = args.toMap()
    }

    @Deprecated("Used Enter instead.")
    inner class StartDialogue(id: Int) : Enter(id)
    open inner class Enter(id: Int) : TransitNode(id)

    open inner class GoBack(id: Int, val repeat: Boolean = false) : Node(id)

    @Deprecated("Use Exit instead.")
    open inner class StopDialogue(id: Int) : Node(id)
    inner class Exit (id: Int) : StopDialogue(id)

    @Deprecated("Use End instead.")
    inner class StopSession(id: Int) : End(id)
    open inner class End(id: Int) : Node(id)

    inner class Sleep(id: Int, val timeout: Int = 60) : TransitNode(id)

    val dialogueNameWithoutVersion get() = with (dialogueName) {
        if (count { it == '/' } > 1)
            substringBeforeLast("/")
        else
            this
    }

    @Deprecated("Use dialogueName instead", ReplaceWith("dialogueName"))
    open val name get() = dialogueName

    @Deprecated("Use dialogueNameWithoutVersion instead", ReplaceWith("dialogueNameWithoutVersion"))
    val nameWithoutVersion get() = dialogueNameWithoutVersion

    override val version = 0

    val intents: List<Intent> get() = nodes.filterIsInstance<Intent>()

    val globalIntents: List<GlobalIntent> get() = nodes.filterIsInstance<GlobalIntent>()

    val actions: List<Action> get() = nodes.filterIsInstance<Action>()

    val globalActions: List<GlobalAction> get() = nodes.filterIsInstance<GlobalAction>()

    val userInputs: List<UserInput> get() = nodes.filterIsInstance<UserInput>()

    val reInputs: List<ReInput> get() = nodes.filterIsInstance<ReInput>()

    val responses: List<Response> get() = nodes.filterIsInstance<Response>()

    val functions: List<Function> get() = nodes.filterIsInstance<Function>()

    val commands: List<Command> get() = nodes.filterIsInstance<Command>()

    val subDialogues: List<SubDialogue> get() = nodes.filterIsInstance<SubDialogue>()

    val selectableNodes: List<SelectableNode> get() = nodes.filterIsInstance<SelectableNode>()

    val context get() = run.context as C

    fun inContext(block: C.() -> Any) = block(context)

    fun node(id: Int): Node = nodes.find { it.id == id }?:error("Node $id not found in $this")

    fun intentNode(id: Int) = intents.find { it.id == id } ?: error("Intent $id not found in $this")

    val nodeMap: Map<String, AbstractDialogue<C>.Node> by lazy {
        javaClass.kotlin.members.filter {
            it is KProperty && it.returnType.isSubtypeOf(Node::class.createType(listOf(KTypeProjection.STAR)))
        }.map { it.name to it.call(this) as AbstractDialogue<C>.Node }.toMap()
    }

    fun validate() {
        for (node in nodes) {
            val name by lazy { "${this::class.qualifiedName}: ${node::class.simpleName}(${node.id})" }
            if (node is TransitNode && !node.isNextInitialized())
                error("$name missing next node")
        }
    }

    fun describe(): String {
        val sb = StringBuilder()
        nodeMap.forEach {
            sb.append(it.key).append(" = ").appendLine(it.value)
        }
        return sb.toString()
    }

    class Run(val node: AbstractDialogue<*>.Node, val context: Context)

    companion object {

        const val GENERATED_USER_INPUT_ID = 10000

        @Deprecated("Use `DialogueRuntime.ifRunning` instead")
        fun ifRunning(block: Run.() -> Unit) = DialogueRuntime.ifRunning(block)
    }
}
