package ai.flowstorm.core.indexer

import ai.flowstorm.common.model.Entity
import ai.flowstorm.core.model.*
import kotlin.reflect.KClass

interface RuntimeDataIndexer {
    fun save(profile: Profile)
    fun save(session: Session)
    fun save(turn: Turn)
    fun save(clientEvent: ClientEvent)
    fun save(fullContext: FullContext)
    fun save(session: Session, turn: Turn, profile: Profile)
    fun save(dialogueEvent: DialogueEvent)

    fun saveAll(entities: List<Entity>): BulkResult

    fun <E : Entity> getById(entityClass: KClass<out E>, id: String): E
    fun <E : Entity> updateById(entityClass: KClass<out E>, id: String, updateString: String)

    data class BulkResult(
        val processed: Int,
        val failed: Int,
        val failures: String? = null,
    )
}
