package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.io.WritableStream

interface PipelineStreamCallback<S: WritableStream, I : Any> {

    fun onRecognized(stream: S, input: I, isFinal: Boolean)

    fun onOpen(stream: S)

    fun onClosed(stream: S)

    fun onError(stream: S, error: Throwable)
}