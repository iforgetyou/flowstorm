package ai.flowstorm.core

import ai.flowstorm.common.monitoring.performance.PerformanceSpan
import ai.flowstorm.core.connector.CommunityConnector
import ai.flowstorm.core.connector.FileConnector
import ai.flowstorm.core.model.*
import ai.flowstorm.core.nlp.pipeline.PipelineScope
import ai.flowstorm.core.type.Attributes
import javax.inject.Inject

open class Context {

    lateinit var pipelineScope: PipelineScope
    lateinit var performanceSpan: PerformanceSpan
    var performanceTrace: String? = null

    val session get() = pipelineScope.session
    val profile get() = pipelineScope.profile
    val logger get() = pipelineScope.log.logger
    val turn get() = pipelineScope.turn

    val input get() = turn.input
    val user get() = session.user
    val locale get() = input.locale
    val attributes get() = turn.attributes
    val application get() = session.application
    val previousTurns get() = session.turns.reversed()

    // connectors
    @Inject lateinit var file: FileConnector
    @Inject lateinit var community: CommunityConnector

    // pipeline process variables
    val communities: MutableMap<String, Community> = mutableMapOf()
    var intentModels: List<Model> = listOf()
    var entityTypes: Set<String> = setOf()
    var dialogueEvent: DialogueEvent? = null
    var sleepTimeout: Int = 0
    var errors: MutableList<Pair<Any, Throwable>> = mutableListOf()
    var isProcessed: Boolean = false
    var currentFrame: Session.DialogueStackFrame? = null

    @Deprecated("Use processInput instead", replaceWith = ReplaceWith("processInput"))
    fun processPipeline(): Context = processInput()

    fun processInput(): Context {
        if (!isProcessed) {
            pipelineScope.processInput()
            isProcessed = true
        } else {
            logger.warn("Pipeline has been already processed, skipping")
        }
        return this //returning context only for backward compatibility to keep method signature same and avoid rebuilding models
    }

    fun processReInput(): Context {
        pipelineScope.processInput(true)
        return this
    }

    fun createDialogueEvent(e: Throwable) = pipelineScope.createDialogueEvent(e)

    fun getAttributes(name: String): Attributes =
        if (name.startsWith("user") || name.startsWith("clientUser"))
            profile.attributes
        else if (name.startsWith("turn") || name.startsWith("clientTurn"))
            turn.attributes
        else
            session.attributes

    fun getAttribute(name: String, namespace: String = Defaults.globalNamespace) = getAttributes(name)[namespace][name]
}