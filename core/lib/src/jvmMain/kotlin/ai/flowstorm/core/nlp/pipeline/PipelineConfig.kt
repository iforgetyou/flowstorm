package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.core.*
import ai.flowstorm.util.AudioEncoding
import ai.flowstorm.util.Locale

data class PipelineConfig(
    val key: String,
    val token: String?,
    val deviceId: String,
    val sessionId: String,
    val locale: Locale,
    val timeZone: String,
    val trace: String? = null,
    val sttMode: SttMode = SttMode.SingleUtterance,
    val sttModel: SttModel = SttModel.General,
    var sttSampleRate: Int = Defaults.audioFormat.sampleRate,
    var sttEncoding: AudioEncoding = Defaults.audioFormat.encoding,
    val ttsFileType: AudioFileType = AudioFileType.mp3
)
