package ai.flowstorm.core.nlp.stt

import ai.flowstorm.core.SttStreamConfig
import ai.flowstorm.io.WritableStream
import ai.flowstorm.util.Locale

interface SttStream: WritableStream {

    val config: SttStreamConfig
    val locale: Locale
    val isOpen: Boolean

    override val streamName get() = "${super.streamName}/$locale" + if (isOpen) "+" else "-"
}