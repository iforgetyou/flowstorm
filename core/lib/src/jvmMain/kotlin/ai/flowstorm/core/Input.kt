package ai.flowstorm.core

import ai.flowstorm.common.annotation.Transient
import ai.flowstorm.common.model.newId
import ai.flowstorm.core.model.Sentiment
import ai.flowstorm.core.model.Turn
import ai.flowstorm.core.type.Decimal
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import ai.flowstorm.core.type.value.Value
import ai.flowstorm.util.Locale
import com.fasterxml.jackson.core.type.TypeReference
import java.lang.reflect.ParameterizedType

actual open class Input : InputBase() {

    @get:Transient
    val id = newId<Turn>()
    val classes: MutableList<Class> = mutableListOf()
    var tokens: MutableList<Token> = mutableListOf()

    data class Transcript(var text: String, val confidence: Float = 1.0F, val provider: String? = null, val locale: Locale? = null, var normalizedText: String = text)

    data class Class(val type: Type, val name: String, val score: Float = 1.0F, val model_id: String = "") {
        enum class Type { Intent, Entity, Sentiment, Profanity }
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
    @JsonSubTypes(
            JsonSubTypes.Type(value = Word::class, name = "Word"),
            JsonSubTypes.Type(value = Punctuation::class, name = "Punctuation")
    )
    open class Token(open val text: String) {
        override fun equals(other: Any?): Boolean = text.equals(other)
        override fun hashCode(): Int = text.hashCode()
    }

    data class Word(override val text: String, val classes: MutableList<Class> = mutableListOf(), val startTime: Float = 0F, val confidence: Float = 1.0F, val endTime: Float = 0F, val speakerId: String? = null) : Token(text) {
        fun hasClass(type: Class.Type, name: String) = classes.any { it.type == type && it.name == name }
        fun isEntity(name: String) = hasClass(Class.Type.Entity, name)
        override fun hashCode() = text.hashCode()
        override fun equals(other: Any?) = if (other is Word) text == other.text else super.equals(other)
    }

    data class Punctuation(override val text: String) : Token(text)

    class WordList(words: List<Word>) : ArrayList<Word>() {
        init {
            addAll(words)
        }

        fun entities(name: String) = filter { it.classes.any { it.type == Class.Type.Entity && it.name == name } }
    }

    @get:Transient
    val words get() = WordList(tokens.filterIsInstance<Word>())

    @get:Transient
    val intents get() = classes.filter { it.type == Class.Type.Intent }

    @get:Transient
    val intent get() = intents.firstOrNull() ?: error("No intent class recognized in input")

    @get:Transient
    val sentiments get() = classes.filter { it.type == Class.Type.Sentiment }

    @get:Transient
    val sentiment get() = sentiments.firstOrNull() ?: Class(Class.Type.Sentiment, Sentiment.UNKNOWN.name)

    @get:Transient
    val isProfane get() = classes.any { it.type == Class.Type.Profanity }

    @get:Transient
    val numbers: List<Number> get() {
        val numbers = mutableListOf<Number>()
        transcript.text.replace(Regex("(-?[\\d]+(\\.?,?\\d+)?)")) {
            val s = it.groupValues[1].replace(',', '.')
            numbers.add(Decimal(s))
            s
        }
        return numbers
    }

    var action: String? = null

    @get:Transient
    val entityMap: MutableMap<String, MutableList<InputEntity>> = mutableMapOf()

    fun isAction() = (action != null)
    fun isNotAction() = (action == null)

    fun containsEntity(className: String) = entityMap.containsKey(className)

    inline fun <reified V : Value> containsEntity() = containsEntity(V::class.simpleName!!)

    fun entities(className: String) = entityMap[className] ?: listOf()

    fun filteredEntities(className: String) = entityMap[className]?.filter { it.modelId.endsWith("filtered") } ?: listOf()

    inline fun <reified V : Value> entities(): List<V> {
        val type = object : TypeReference<V>() {}.type
        val className = if (type is ParameterizedType) {
            assert(type.actualTypeArguments.size == 1)
                { "Container type of the entity must have exactly one type parameter. (Only Interval<V: Value> is currently supported)" }
            type.actualTypeArguments.first().typeName.split(".").last()
        } else {
            V::class.simpleName!!
        }
        return if (entityMap.containsKey(className))
            entityMap[className]?.map { it.value as? V }?.filterNotNull() ?: listOf()
        else
            listOf()
    }

    fun entity(className: String) = entities(className).first()

    inline fun <reified V : Value> entity(): V = entities<V>().first()
}
