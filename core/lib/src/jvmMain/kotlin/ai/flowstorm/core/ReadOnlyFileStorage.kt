package ai.flowstorm.core

import java.io.OutputStream

interface ReadOnlyFileStorage {
    fun readFile(path: String, output: OutputStream)
    fun listFiles(path: String, recursive: Boolean = true): List<String>
}