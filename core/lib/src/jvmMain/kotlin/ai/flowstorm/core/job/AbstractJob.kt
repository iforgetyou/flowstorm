package ai.flowstorm.core.job

import ai.flowstorm.common.monitoring.performance.measure
import ai.flowstorm.concurrent.call
import ai.flowstorm.util.LoggerDelegate

abstract class AbstractJob : Job {

    protected val logger by LoggerDelegate()

    protected abstract fun execute()

    override fun run() {
        try {
            call(Job.TIMEOUT) {
                measure({ _, time ->
                    logger.info("${this::class.simpleName} executed after $time ms")
                }) {
                    execute()
                }
            }
        } catch (t: Throwable) {
            logger.error("${this::class.simpleName} failed", t)
        }
    }
}