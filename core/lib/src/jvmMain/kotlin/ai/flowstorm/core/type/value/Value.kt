package ai.flowstorm.core.type.value

import com.fasterxml.jackson.annotation.JsonIgnore

abstract class Value {
    @JsonIgnore
    var alternativeValues: List<Value> = listOf()
    val type: String = ""
}