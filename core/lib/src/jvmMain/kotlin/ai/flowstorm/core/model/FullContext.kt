package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import ai.flowstorm.common.model.Id

data class FullContext (val session:Session, val turn:Turn, val profile:Profile): Entity {
    override val _id: Id<FullContext> = turn._id.cast()
}