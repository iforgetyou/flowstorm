package ai.flowstorm.core.connector

import ai.flowstorm.core.model.Community
import ai.flowstorm.core.repository.CommunityRepository
import javax.inject.Inject

class CommunityConnector {

    @Inject
    lateinit var communityRepository: CommunityRepository

    fun get(name: String, spaceId: String) = communityRepository.get(name, spaceId)

    fun create(community: Community) = communityRepository.create(community)
}