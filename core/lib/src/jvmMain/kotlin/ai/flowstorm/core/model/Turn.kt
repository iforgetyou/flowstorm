package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.Session.DialogueStackFrame
import ai.flowstorm.core.Input
import ai.flowstorm.core.Response
import ai.flowstorm.core.type.Attributes
import ai.flowstorm.core.type.DateTime
import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.util.Locale
import ai.flowstorm.util.Log
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.toJavaInstant
import java.time.ZoneId

data class Turn(
    override val _id: Id<Turn> = newId(),
    val user_id: Id<User>? = null,
    val session_id: Id<Session>? = null, // cannot be NullId due to backward compatibility with turns stored in session entity
    val dialogue_id: Id<DialogueModel>? = null, // same issue
    val application_id: Id<Application>? = null, // same issue
    var locale: Locale? = null,
    var input: Input,
    var inputId: Int? = null,
    var nextId: Int? = null,
    var datetime: Instant = Clock.System.now(),
    override var attributes: Attributes = Attributes(),
    var endFrame: DialogueStackFrame? = null, //where the turn ends (input node)
    val responseItems: MutableList<Response.Item> = mutableListOf(),
    var sttConfig: SttConfig? = null,
    var duration: Long? = null,
    val votes: MutableMap<String, Int> = mutableMapOf(),
    val log: MutableList<Log.Entry> = mutableListOf()
) : Entity, HasAttributes {
    data class Request(var attributes: PropertyMap? = null)
    data class FlowNode(val dialogueId: String, val dialogueName: String, val dialogueVersion: Int,
                        val nodeName: String, val nodeId: Int)

    val time: DateTime get() = DateTime.ofInstant(datetime.toJavaInstant(), ZoneId.systemDefault())
    val flow: MutableList<FlowNode> = mutableListOf()
}
