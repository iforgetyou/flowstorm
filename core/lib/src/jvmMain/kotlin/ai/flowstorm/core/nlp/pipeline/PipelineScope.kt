package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.core.Context
import ai.flowstorm.core.Input
import ai.flowstorm.core.Response
import ai.flowstorm.core.model.Profile
import ai.flowstorm.core.model.Session
import ai.flowstorm.core.model.Turn
import ai.flowstorm.core.nlp.stt.SttStreamFactory
import ai.flowstorm.io.WritableStream
import ai.flowstorm.util.Locale
import ai.flowstorm.util.Log

interface PipelineScope : WritableStream {

    val log: Log
    val locale: Locale
    val config: PipelineConfig
    val context: Context
    val session: Session
    val profile: Profile
    val turn: Turn
    val streamInput: StreamInput
    val isStreamingInput: Boolean
    val isProcessingInput: Boolean
    fun processInput(reInput: Boolean = false)
    fun process(input: Input, trace: String? = null)
    fun process(component: PipelineComponent)
    fun launch(component: PipelineComponent)
    fun stream(stream: WritableStream)
    fun stream(streamService: PipelineStreamService)
    fun sttStream(sttServiceFactory: SttStreamFactory? = null, locale: Locale? = null)
    fun ttsProcess(item: Response.Item): List<Response.Item>
    fun wait(vararg components: PipelineComponent)
    fun openStream(type: PipelineStreamType = PipelineStreamType.Any, trace: String? = null)
    fun closeStreams(type: PipelineStreamType = PipelineStreamType.Any)
    fun createDialogueEvent(e: Throwable)
    fun addResponseItem(item: Response.Item)
    fun addClientLogEntries(entries: List<Log.Entry>)
}