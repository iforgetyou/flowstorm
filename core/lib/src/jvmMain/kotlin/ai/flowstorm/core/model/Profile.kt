package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.type.Attributes

data class Profile(
        override val _id: Id<Profile> = newId(),
        val user_id: Id<User>,
        val space_id: Id<Space>,
        override val attributes: Attributes = Attributes()
) : Entity, HasAttributes
