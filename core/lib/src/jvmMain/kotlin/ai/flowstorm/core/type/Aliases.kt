package ai.flowstorm.core.type

import ai.flowstorm.core.Defaults

actual typealias DynamicMap<K, V> = java.util.LinkedHashMap<K, V>
actual typealias DateTime = java.time.ZonedDateTime
actual typealias Decimal = java.math.BigDecimal

actual val ZERO_TIME = DateTime.of(0, 1, 1, 0, 0, 0, 0, Defaults.zoneId)
