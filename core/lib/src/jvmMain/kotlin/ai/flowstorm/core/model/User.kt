package ai.flowstorm.core.model

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.common.model.Entity
import kotlinx.datetime.Instant

open class User(
    override var _id: Id<User> = newId(),
    open var username: String,
    open var name: String,
    open var surname: String,
    open var nickname: String,
    open var agreement: Instant? = null,
    open var phoneNumber: String? = null,
) : Entity
