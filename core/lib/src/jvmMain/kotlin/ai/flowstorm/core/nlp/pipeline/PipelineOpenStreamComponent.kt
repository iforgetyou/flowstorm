package ai.flowstorm.core.nlp.pipeline

fun interface PipelineOpenStreamComponent {
    fun process( scope: PipelineScope, type: PipelineStreamType)
}