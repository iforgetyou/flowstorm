package ai.flowstorm.core.model

import ai.flowstorm.common.annotation.Transient
import ai.flowstorm.security.Digest
import ai.flowstorm.util.Log

@get:Transient
val TtsConfig.code get() = Digest.md5((provider + locale.toString() + gender.name + name + engine).toByteArray())

fun Log.Entry.toString() = "+${"%.3f".format(relativeTime)}:$level[$text]"