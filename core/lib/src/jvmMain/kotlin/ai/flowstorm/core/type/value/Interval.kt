package ai.flowstorm.core.type.value

data class Interval<V: Value>(val from: V?, val to: V?): Value()