package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.common.monitoring.performance.PerformanceTransaction
import ai.flowstorm.core.Input
import ai.flowstorm.time.currentTime
import com.fasterxml.jackson.annotation.JsonIgnore

class StreamInput(
    @Transient @JsonIgnore
    val performanceTransaction: PerformanceTransaction,
    val createdTime: Long = currentTime(),
    var updatedTime: Long = currentTime(),
    var textRecognized: Int = 0,
    var byteCount: Int = 0,
) : Input() {
    val startTime by lazy { currentTime() }
    val allRecognizedInputs: MutableList<Input> = mutableListOf()

    fun getBestRecognizedInput(): Input? {
        return if (allRecognizedInputs.isEmpty())
            null
        else if (allRecognizedInputs.size == 1)
            allRecognizedInputs[0]
        else {
            val lastInput = allRecognizedInputs.last()
            val secondLastInput = allRecognizedInputs[allRecognizedInputs.size - 2]
            if (lastInput.transcript.text.length < secondLastInput.transcript.text.length)
                secondLastInput
            else
                lastInput
        }
    }
}