package ai.flowstorm.core.nlp.pipeline

enum class PipelineStreamType { Stt, Other, Any }