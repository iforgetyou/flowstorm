package ai.flowstorm.core.nlp.stt

import ai.flowstorm.core.Input
import ai.flowstorm.core.nlp.pipeline.PipelineStreamCallback

interface SttCallback : PipelineStreamCallback<SttStream, Input>