package ai.flowstorm.core.model

import ai.flowstorm.common.ServiceUrlResolver
import ai.flowstorm.common.annotation.Transient
import ai.flowstorm.common.model.Entity
import ai.flowstorm.common.model.Id
import ai.flowstorm.common.model.newId
import ai.flowstorm.core.type.MutablePropertyMap

open class Application(
    override var _id: Id<Application> = newId(),
    open var name: String,
    open var dialogue_id: Id<DialogueModel>? = null,
    open var public: Boolean = false,
    open var anonymousAccessAllowed: Boolean = false,
    open var icon: String? = null,
    open var skimmerConfigUrl: String? = null,
    open var properties: MutablePropertyMap = mutableMapOf()
) : Entity {
    @get:Transient
    val link get() = ServiceUrlResolver.getEndpointUrl(ServiceUrlResolver.Service.bot, domain = "flowstorm.ai") + "/" + _id
}