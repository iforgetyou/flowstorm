package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.core.Input
import ai.flowstorm.core.Response
import ai.flowstorm.util.Log

interface PipelineHandler {

    fun onRecognizedInput(input: Input, isFinal: Boolean)
    fun onRecognitionError(error: Throwable, source: String)
    fun onResponseItem(item: Response.Item)
    fun onResponseDone(logs: List<Log.Entry>, sessionEnded: Boolean, sleepTimeout: Int, turnId: String)
    fun onProcessError(obj: Any, e: Throwable)
}