package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import ai.flowstorm.core.model.metrics.Metric
import ai.flowstorm.core.type.*
import ai.flowstorm.core.Defaults
import ai.flowstorm.util.Log
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import java.util.*

data class Session(
    override val _id: Id<Session> = newId(),
    var datetime: Instant = Clock.System.now(),
    val sessionId: String,
    val test: Boolean = false,
    var user: User,
    var device: Device? = null, //nullable only for BC
    var application: Application,
    val space_id: Id<Space> = NullId(),  //nullable only for backward compatibility
    var location: Location? = null,
    val locale: Locale? = null,
    val initiationId: String? = null,
    val hasConsent: Boolean = false,
    var turns: List<Turn> = emptyList(),
    val metrics: MutableList<Metric> = mutableListOf(),
    val properties: MutablePropertyMap = mutableMapOf(),
    override val attributes: Attributes = Attributes(),
    val dialogueStack: DialogueStack = LinkedList(),
    val clientLogEntries: MutableList<Log.Entry> = mutableListOf(),
) : Entity, HasAttributes {
    val turnsCount get() = turns.size
    val isInitiated get() = initiationId != null
    val isEnded get() = dialogueStack.isEmpty()
    val clientType get() = attributes[Defaults.globalNamespace].get("clientType")?.let {
        (it as Memory<String>).value
    } ?: "unknown"

    data class DialogueStackFrame(
            val id: String? = null, // nullable to be able to load older sessions
            val buildId: String,
            val args: PropertyMap,
            val nodeId: Int = 0,
            val name: String? = null
    )
}
