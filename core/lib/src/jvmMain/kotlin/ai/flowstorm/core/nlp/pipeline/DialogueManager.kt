package ai.flowstorm.core.nlp.pipeline

import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.dialogue.AbstractDialogue
import ai.flowstorm.core.model.Session
import ai.flowstorm.util.Logger

interface DialogueManager : PipelineComponent {

    fun getUserInput(session: Session, logger: Logger): AbstractDialogue<*>.UserInput?
    fun getExpectedPhrases(intents: Array<out AbstractDialogue<*>.Intent>): List<ExpectedPhrase>
    fun <T: Any> getPropertyFromDialogueStack(session: Session, logger: Logger, callback: ((AbstractDialogue<*>) -> T?)): T?
}