package ai.flowstorm.core.nlp.stt

import ai.flowstorm.core.ExpectedPhrase
import ai.flowstorm.core.SttStreamConfig

interface SttStreamFactory {

    fun create(config: SttStreamConfig, callback: SttCallback, expectedPhrases: List<ExpectedPhrase>): SttStream

}