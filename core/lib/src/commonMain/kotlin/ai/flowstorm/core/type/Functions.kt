package ai.flowstorm.core.type

fun String.beautify() = replace(Regex("\\<.*?\\>"), "")
        .trim()
        .replace(Regex("^,"), "")
        .trim()
        .replaceFirstChar(Char::uppercase)
        .replace(Regex("[\\s]+"), " ")
        .replace(Regex(",\$"), "...")
        .replace(Regex("([^\\.\\!\\?…:])\$"), "$1.")
        .replace(Regex(",([\\p{L}])"), ", $1")
        .replace(Regex("([^\\.])\\.\\.([^\\.])"), "$1.$2")
        .replace(Regex("([\\!\\?] )(\\D)")) { m -> m.groupValues[1] + m.groupValues[2].uppercase() }
        .replace(Regex("([^\\.])[\\.]{2}$"), "$1.")
        .replace(" .", ".")
        .replace(" ,", ",")
        .replace(",,", ",")
        .replace(",.", ".")
        .replace(",!", "!")
        .replace(", !", "!")
        .replace(",?", "?")
        .replace(", ?", "?")
        .replace(", -", "-")
        .replace(Regex("--.\$"), "")
        .trim()

val Any.isPrimitive get() = this is CharSequence || this is Number || this is Boolean || this is DateTime