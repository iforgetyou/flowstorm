package ai.flowstorm.core.dialogue

data class SelectorState(val nodeRefs: MutableList<NodeRef> = mutableListOf())