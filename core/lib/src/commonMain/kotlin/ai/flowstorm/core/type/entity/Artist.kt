package ai.flowstorm.core.type.entity

interface Artist {
    val name: String
}