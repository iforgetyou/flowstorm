package ai.flowstorm.core

import ai.flowstorm.core.type.PropertyMap

data class Request(
    val appKey: String,
    val deviceId: String,
    val token: String? = null,
    val sessionId: String? = null,
    val initiationId: String? = null,
    val input: Input,
    var attributes: PropertyMap = mapOf()
)