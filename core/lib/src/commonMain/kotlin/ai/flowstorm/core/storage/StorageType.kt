package ai.flowstorm.core.storage

enum class StorageType { FileSystem, Google, AmazonS3 }