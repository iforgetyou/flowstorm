package ai.flowstorm.core

import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.time.ZoneId
import ai.flowstorm.util.Locale

expect class Input() : InputBase

fun input(
    text: String,
    locale: Locale = Defaults.locale,
    zoneId: ZoneId = Defaults.zoneId,
    confidence: Float = 1.0F,
    provider: String? = null,
    attributes: PropertyMap = mapOf()
) = Input().also {
    it.locale = locale
    it.zoneId = zoneId
    it.transcript = Transcript(text, confidence, provider, locale)
    it.alternatives += it.transcript
    it.attributes = attributes
}