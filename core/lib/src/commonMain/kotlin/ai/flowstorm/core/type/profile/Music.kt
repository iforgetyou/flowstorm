package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Music(
    var playsInstrument: Boolean? = null, // true if plays instrument
) : Extendable()