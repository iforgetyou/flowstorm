package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable
import ai.flowstorm.core.type.entity.Movie

class Movies(
    var isMovieFan: Boolean? = null, // is true if user likes movies
    var favoriteMovie: Movie? = null, //contains user favorite movie if any
    var futureMovie: Movie? = null //contains user favorite movie if any
) : Extendable()