package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Traveling(
    var isHitchhiker: Boolean? = null, // is true if user hitchhikes
    var wasAbroad: Boolean? = null, // is true if user was abroad
    var wantsAbroad: Boolean? = null, // is true if user wants to go abroad
    var visitedCountry: String = "", // stores country that user visited
    var wantedCountry: String = "" // stores country user wants to visit
) : Extendable()