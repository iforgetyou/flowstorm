package ai.flowstorm.core.dialogue

/**
 * [LabelOverlapSelector] selects the next selectable node in dialogue based on the overlap of the labels
 * if the previously selected dialogue and a dialogue candidate. The candidates are sorted by a number of common
 * labels with the previous dialogue. If there is no common label, the next dialogue selection
 * depends on the [fallbackStrategy]. Each dialogue can be triggered only once per session.
 * There are several options for the [fallbackStrategy]:
 *  * [FallbackStrategy.RANDOM] - if there is no label overlap, the next dialogue is selected randomly from a list of unused
 *    selectable dialogues.
 *  * [FallbackStrategy.POOL] - with this strategy, a non-empty list [fallbackPool] of [NodeRef] must be provided.
 *    If there is no label overlap, the next dialogue is selected randomly from the [fallbackPool].
 *  * [FallbackStrategy.NONE] - if there is no label overlap, [select] function returns null.
 */
class LabelOverlapSelector(
    val fallbackStrategy: FallbackStrategy = FallbackStrategy.NONE,
    val fallbackPool: List<NodeRef> = listOf()
) : Selector {
    init {
        if (fallbackStrategy == FallbackStrategy.POOL && fallbackPool.isEmpty()) {
            error("If pool strategy is selected, an non-empty list of NodeRef instances must be provided as fallbackPool")
        }
    }

    enum class FallbackStrategy { RANDOM, POOL, NONE }

    override fun select(model: SelectorModel, relevantNodeRefs: List<NodeRef>): NodeRef? {
        // Filter out dialogues that were already triggered
        val unusedDialogueRefs = relevantNodeRefs.filter { nodeRef ->
            model.getNodeState(nodeRef.id, nodeRef.scope).run { count == 0 && !isLocked }
        }

        val previousLabels = model.selectorState.nodeRefs.lastOrNull()?.labels ?: mutableSetOf()
        // Find dialogues with related labels
        val candidates = previousLabels.map { previous ->
            unusedDialogueRefs.filter { it.labels.contains(previous) }
        }.flatten().toSet()

        var best = mutableListOf<NodeRef>()
        var bestScore = 0

        candidates.forEach { dialogue ->
            val topicOverlap = overlapScore(dialogue.labels, previousLabels)

            if (topicOverlap > bestScore) {
                bestScore = topicOverlap
                best = mutableListOf()
            }
            if (topicOverlap >= bestScore) {
                best.add(dialogue)
            }
        }

        return if (best.isNotEmpty()) {
            best.random()
        } else if (fallbackStrategy == FallbackStrategy.POOL ){
            fallbackPool.random()
        } else if (fallbackStrategy == FallbackStrategy.RANDOM ){
            unusedDialogueRefs.ifEmpty { null }?.random()
        } else {
            null
        }
    }

    private fun overlapScore(setOne: Set<String>, setTwo: Set<String>) = setOne.intersect(setTwo).size

    companion object {
        /**
         * Creates a [NodeRef] from a list of labels. This is used if the [SelectorState.nodeRefs]
         * is updated using a custom dialogue logic. Note that the [NodeRef.id] and [NodeRef.scope] parameters
         * are constant as they have no effect on selection when using [LabelOverlapSelector].
         */
        fun labelsToNodeRef(vararg labels: String) = NodeRef(1, Scope.Session, labels.toSet())
    }
}