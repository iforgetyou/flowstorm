package ai.flowstorm.core.dialogue

data class NodeRef(
    val id: Int,
    var scope: Scope,
    var labels: Set<String>
)