package ai.flowstorm.core.dialogue

interface Selector {

    fun select(model: SelectorModel, relevantNodeRefs: List<NodeRef>): NodeRef?
}