package ai.flowstorm.core

import ai.flowstorm.util.Locale
import ai.flowstorm.time.zoneId

object Defaults {

    val locale = Locale("en")
    val zoneId = zoneId("Europe/Prague")
    val locales = mapOf(
        "en" to Locale("en", "US"),
        "de" to Locale("de", "DE"),
        "fr" to Locale("fr", "FR"),
        "es" to Locale("es", "ES"),
        "cs" to Locale("cs", "CZ")
    )
    const val globalNamespace: String = "_global"
    val audioFormat = AudioFormat()
}