package ai.flowstorm.core.dialogue

import kotlin.random.Random

object RandomSelector : Selector {

    override fun select(model: SelectorModel, relevantNodeRefs: List<NodeRef>) = with (relevantNodeRefs) {
        // find minimum count
        var minCount = Int.MAX_VALUE
        forEach { ref ->
            val state = model.getNodeState(ref.id, ref.scope)
            if (state.count < minCount)
                minCount = state.count
        }
        // select randomly from topics with minimum count
        filter { ref ->
            model.getNodeState(ref.id, ref.scope).count == minCount
        }.let { refs ->
            refs[Random.nextInt(refs.size)]
        }
    }
}