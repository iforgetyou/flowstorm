package ai.flowstorm.core.model

interface Versionable {
    val version: Int
}