package ai.flowstorm.core.model

import ai.flowstorm.common.annotation.Transient
import ai.flowstorm.security.Digest
import ai.flowstorm.util.Locale

private val en_US = Locale("en", "US")
private val en_GB = Locale("en", "GB")
private val cs_CZ = Locale("cs", "CZ")

data class TtsConfig(
    val provider: String,
    val locale: Locale,
    val gender: Gender,
    val name: String,
    val engine: String? = null,
    val amazonAlexa: String? = null,
    val googleAssistant: String? = null
) {
    constructor(provider: String, locale: Locale, gender: Gender, name: String, engine: String?) :
            this(provider, locale, gender, name, engine, null, null)

    enum class Gender { Male, Female }

    @get:Transient
    val language: String get() = locale.getLanguage()

    @get:Transient
    val code = Digest.md5(provider + locale.toString() + gender.name + name + engine)

    enum class Voice(val config: TtsConfig) {
        George(TtsConfig("Google", en_US, Gender.Male, "en-US-Standard-B")),
        Grace(TtsConfig("Google", en_US, Gender.Female, "en-US-Standard-C")),
        Gordon(TtsConfig("Google", en_GB, Gender.Male, "en-GB-Wavenet-B")),
        Gwyneth(TtsConfig("Google", en_GB, Gender.Female, "en-GB-Wavenet-C")),
        Gabriela(TtsConfig("Google", cs_CZ, Gender.Female, "cs-CZ-Standard-A")),
        Anthony(TtsConfig("Amazon", en_US, Gender.Male,"Matthew", "neural")),
        Audrey(TtsConfig("Amazon", en_US, Gender.Female,"Joanna", "neural")),
        Arthur(TtsConfig("Amazon", en_GB, Gender.Male,"Brian", "neural")),
        Amy(TtsConfig("Amazon", en_GB, Gender.Female,"Amy", "neural")),
        Michael(TtsConfig("Microsoft", en_US, Gender.Male, "en-US-GuyNeural")),
        Mary(TtsConfig("Microsoft", en_US, Gender.Female, "en-US-AriaNeural")),
        Milan(TtsConfig("Microsoft", cs_CZ, Gender.Male,"cs-CZ-Jakub"));
    }

    companion object {
        private val defaults = mapOf("en" to Voice.Audrey, "cs" to Voice.Gabriela)
        fun forLanguage(language: String) = (defaults[language] ?: defaults["en"]!!).config
    }
}
