package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Education(
    var studiedAbroad: Boolean? = null, // is true if user studied abroad
    var foreignLanguage: Boolean? = null // is true if user speaks any foreign language
) : Extendable()