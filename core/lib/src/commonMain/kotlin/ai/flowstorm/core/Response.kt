package ai.flowstorm.core

import ai.flowstorm.util.Log
import ai.flowstorm.core.model.TtsConfig
import ai.flowstorm.core.type.PropertyMap

data class Response(
    val logs: List<Log.Entry>,
    val items: List<Item> = listOf(),
    val sessionEnded: Boolean = false,
    val sleepTimeout: Int = 0,
    val turnId: String? = null,
    @Deprecated(message = "Will be removed soon")
    val attributes: PropertyMap = mapOf(),
) {
    data class Item(
        var text: String? = null,
        var ssml: String? = null,
        var confidence: Double = 1.0,
        var image: String? = null,
        var audio: String? = null,
        var video: String? = null,
        var code: String? = null,
        var background: String? = null,
        var repeatable: Boolean = true,
        var ttsConfig: TtsConfig? = null,
        val turnId: String? = null,
        val dialogueNodeId: String? = null,
        val nodeId: Int? = null
    ) {
        fun text() = text ?: ""
    }

    enum class IVA { AmazonAlexa, GoogleAssistant }

    fun text() = items.joinToString("\n") { it.text ?: "..." }.trim()

    fun ssml(iva: IVA) = items.joinToString("\n") {
        val ssml = it.ssml ?: if (it.text != null && !it.text!!.startsWith('#')) it.text!! else ""
        if (ssml.isNotBlank() && it.ttsConfig != null) {
            val ttsConfig = it.ttsConfig!!
            when (iva) {
                IVA.AmazonAlexa -> {
                    when {
                        ttsConfig.amazonAlexa != null ->
                            "<voice name=\"${ttsConfig.amazonAlexa}\">$ssml</voice>"
                        ttsConfig.provider == "Amazon" ->
                            "<voice name=\"${ttsConfig.name}\">$ssml</voice>"
                        else -> ssml
                    }
                }
                IVA.GoogleAssistant ->
                    "<voice gender=\"${ttsConfig.gender.name.lowercase()}\" variant=\"${ttsConfig.googleAssistant ?: 1}\">$ssml</voice>"
            }
        } else ssml
    }.trim()

    override fun toString() = "Response(${items.size} item(s), sessionEnded=$sessionEnded, sleepTimeout=$sleepTimeout, turnId=$turnId)"
}