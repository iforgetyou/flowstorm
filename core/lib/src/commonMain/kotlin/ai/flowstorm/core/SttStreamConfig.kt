package ai.flowstorm.core

import ai.flowstorm.core.model.SttConfig
import ai.flowstorm.util.AudioEncoding
import ai.flowstorm.util.Locale

data class SttStreamConfig(
    val locale: Locale,
    val sampleRate: Int,
    val encoding: AudioEncoding = Defaults.audioFormat.encoding,
    val mode: SttMode = SttMode.SingleUtterance,
    val model: SttModel = SttModel.General,
    val sttConfig: SttConfig = SttConfig("Google")
)