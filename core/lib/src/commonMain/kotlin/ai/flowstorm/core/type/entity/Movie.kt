package ai.flowstorm.core.type.entity

interface Movie {
    val name: String
}