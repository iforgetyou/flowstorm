package ai.flowstorm.core.dialogue

interface SelectorModel {

    val selectorState: SelectorState
    fun getNodeState(id: Int, scope: Scope): NodeState
}