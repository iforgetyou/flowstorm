package ai.flowstorm.core

enum class SttMode {
    SingleUtterance,
    Continuous,
    @Deprecated("Use SingleUtterance instead", ReplaceWith("SingleUtterance")) Default,
    @Deprecated("Use Continuous instead", ReplaceWith("Continuous")) Duplex
}
