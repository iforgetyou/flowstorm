package ai.flowstorm.core.model

interface Model {
    val id: String
    val name: String
}