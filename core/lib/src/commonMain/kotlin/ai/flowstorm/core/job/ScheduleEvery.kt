package ai.flowstorm.core.job

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class ScheduleEvery(val month: Int = -1, val day: Int = -1, val hours: Int = -1, val minutes: Int = -1)