package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable
import ai.flowstorm.core.type.entity.Person

class UserProfile(
    var person: Person = Person(), // user related variables
    var family: Family = Family(), // family related variables
    var movies: Movies = Movies(), // movies related variables
    var sports: Sports = Sports(), // sports related variables
    var books: Books = Books(), // books related variables
    var fashion: Fashion = Fashion(), // fashion related variables
    var foods: Foods = Foods(), // foods related variables
    var habits: Habits = Habits(), // habits related variables
    var traveling: Traveling = Traveling(), // traveling related variables
    var education: Education = Education(), // education related variables
    var pandemic: Pandemic = Pandemic(), // pandemic related variables
    var games: Games = Games(), // video game related variables
    var pets: Pets = Pets(), // pets related variables
    var music: Music = Music() // music related variables
) : Extendable()