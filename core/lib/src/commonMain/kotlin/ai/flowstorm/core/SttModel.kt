package ai.flowstorm.core

enum class SttModel(val googleName: String) {
    General("default"),
    PhoneCall("phone_call")
}