package ai.flowstorm.core.dialogue

enum class Scope { Turn, Session, User, Client }