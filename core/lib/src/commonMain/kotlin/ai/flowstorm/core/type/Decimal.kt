package ai.flowstorm.core.type

expect class Decimal(v: Int) {
    constructor(v: Long)
    constructor(v: Double)
    constructor(v: String)
}