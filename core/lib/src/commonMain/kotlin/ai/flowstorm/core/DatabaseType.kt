package ai.flowstorm.core

enum class DatabaseType { Mongo, Dynamo }