package ai.flowstorm.core.type.entity

interface Track {
    val name: String
}