package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Foods(
    var userDiet: Boolean? = null, // is true if user has some special diet
    var eatsOut: Boolean? = null, // is true if user eats in restaurants
    var favoriteFoodType: String = "" // saves user's favourite type of food
) : Extendable()