package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Books(
    var isBookWorm: Boolean? = null, // is true if user likes books
    var likesComics: Boolean? = null, // is true if user likes comics
    var likesPoetry: Boolean? = null // is true if user likes poetry
) : Extendable()