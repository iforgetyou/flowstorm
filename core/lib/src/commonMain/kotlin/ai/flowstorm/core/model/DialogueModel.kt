package ai.flowstorm.core.model

interface DialogueModel : Versionable {

    val dialogueId: String
    val dialogueName: String
}