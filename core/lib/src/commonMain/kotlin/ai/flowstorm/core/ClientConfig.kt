package ai.flowstorm.core

import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.time.ZoneId
import ai.flowstorm.util.Locale

data class ClientConfig(
    val url: String,
    val key: String,
    val deviceId: String,
    val attributes: PropertyMap,
    var introText: String = "#intro",
    val token: String? = null,
    val autoStart: Boolean = true,
    var sessionId: String? = null,
    val initiationId: String? = null,
    val locale: Locale = Defaults.locale,
    val zoneId: ZoneId = Defaults.zoneId,
    val sttInterimResults: Boolean = false,
    val sendResponseItems: Boolean = false,
    val hasInputAudio: Boolean = true,
    val hasOutputAudio: Boolean = true
)