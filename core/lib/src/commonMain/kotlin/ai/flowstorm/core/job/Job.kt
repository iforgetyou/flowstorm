package ai.flowstorm.core.job

import ai.flowstorm.concurrent.Runnable

interface Job : Runnable {

    companion object {
        const val TIMEOUT = 180000 // 3 min
    }
}