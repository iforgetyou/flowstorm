package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Sports(
    var likesSwim: Boolean? = null // is true if user likes swimming
) : Extendable()