package ai.flowstorm.core.type

typealias Extendable = Dynamic
typealias PropertyMap = Map<String, Any>
typealias MutablePropertyMap = MutableMap<String, Any>
