package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Games(
    var mentionedGame: String = "", // stores a game user has mentioned
    var favoriteGame: String = "", //  stores favorite game if any
) : Extendable()