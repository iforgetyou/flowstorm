package ai.flowstorm.core.model

import ai.flowstorm.common.model.Entity
import ai.flowstorm.common.model.Id
import ai.flowstorm.common.model.newId

data class Property(
    override val _id: Id<Property> = newId(),
    val key: String,
    val value: String
) : Entity