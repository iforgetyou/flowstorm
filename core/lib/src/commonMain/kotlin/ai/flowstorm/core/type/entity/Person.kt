package ai.flowstorm.core.type.entity

import ai.flowstorm.core.Defaults
import ai.flowstorm.core.type.Extendable
import ai.flowstorm.time.ZoneId

class Person(var name: String = "", // contains user's name
             var zoneId: ZoneId = Defaults.zoneId, // contains user's zoneId as String
             var country: String = "", // contains the country where user lives
             var grewUpPlace: String = "", // is blank if user did not respond or the response was not specific; possible values are "city", "town", "nature"
    // var grewUpGPE: GPE? = null, // GPE class has name, type and upper parameters; e.g. GPE("Adelaide", "city", "South Australia")
             var feelsBad: Boolean? = null, // is true if user feels bad
             var isStudent: Boolean? = null, // is true if user is student
             var isWorker: Boolean? = null, // is true if user works
             var activities: List<String> = listOf(), // list of selected activities
             var topics: List<String> = listOf() // list of selected topics
) : Extendable()