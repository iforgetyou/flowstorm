package ai.flowstorm.core

import ai.flowstorm.core.type.PropertyMap
import ai.flowstorm.util.Locale

open class InputBase {
    var zoneId = Defaults.zoneId
    var locale: Locale = Defaults.locale
    val alternatives: MutableList<Transcript> = mutableListOf()
    var attributes: PropertyMap = mapOf()

    lateinit var transcript: Transcript

    fun silence() =
        if (::transcript.isInitialized)
            false
        else {
            transcript = Transcript("#silence")
            true
        }

    fun isTranscriptInitialized(): Boolean = ::transcript.isInitialized

    override fun toString() =
        "${this::class.simpleName}(" +
                (if (::transcript.isInitialized) "transcript=$transcript" else "no transcript") + ", locale=$locale, zoneId=$zoneId, attributes=$attributes, alternatives[${alternatives.size}])"
}