package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Fashion(
    var favoriteColor: String = "" // saves user's favorite color
) : Extendable()