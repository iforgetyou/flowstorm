package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Family(
    var sisterMentioned: Boolean? = null, // is true if user mentioned sister
    var brotherMentioned: Boolean? = null, // is true if user mentioned brother
    var motherMentioned: Boolean? = null, // is true if user mentioned mother
    var fatherMentioned: Boolean? = null, // is true if user mentioned father
    var wifeMentioned: Boolean? = null, // is true if user mentioned wife
    var husbandMentioned: Boolean? = null, // is true if user mentioned husband
    var girlfriendMentioned: Boolean? = null, // is true if user mentioned girlfriend
    var boyfriendMentioned: Boolean? = null, // is true if user mentioned boyfriend
    var sonMentioned: Boolean? = null, // is true if user mentioned son
    var daughterMentioned: Boolean? = null, // is true if user mentioned daughter
    var granddaughterMentioned: Boolean? = null, // is true if user mentioned granddaughter
    var grandsonMentioned: Boolean? = null, // is true if user mentioned grandson
    var childrenMentioned: Boolean? = null, // is true if user mentioned children
    var grandchildrenMentioned: Boolean? = null, // is true if user mentioned grandchildren
    var familyClose: Boolean? = null, // is true if user is close to his family
    var bigFamily: Boolean? = null, // is true if user has a big family
) : Extendable()