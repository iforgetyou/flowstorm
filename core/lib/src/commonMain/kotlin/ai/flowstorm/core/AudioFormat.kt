package ai.flowstorm.core

import ai.flowstorm.util.AudioEncoding

data class AudioFormat(
    val sampleRate: Int = 16000,
    val encoding: AudioEncoding = AudioEncoding.LINEAR16,
    val channels: Int = 1,
    val signed: Boolean = true,
    val bigEndian: Boolean = false
)