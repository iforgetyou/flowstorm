package ai.flowstorm.core.type

expect val ZERO_TIME: DateTime
const val ZERO_TEMPERATURE = -273.15
val DEFAULT_LOCATION = Location(50.101, 14.395)