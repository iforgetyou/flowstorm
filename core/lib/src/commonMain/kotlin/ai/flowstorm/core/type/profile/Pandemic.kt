package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Pandemic(
    var missThing: String = "", // stores a thing that user miss during pandemic
    var talkAboutCovid: Boolean? = null // is true if user wants to talk about covid
) : Extendable()