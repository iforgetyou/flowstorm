package ai.flowstorm.core.type.entity

interface Actor {
    val name: String
}