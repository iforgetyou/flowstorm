package ai.flowstorm.core

import ai.flowstorm.util.Locale

expect class Transcript(text: String, confidence: Float = 1.0F, provider: String? = null, locale: Locale? = null, normalizedText: String = text) {
    var text: String
    val confidence: Float
    val provider: String?
    val locale: Locale?
    var normalizedText: String
}