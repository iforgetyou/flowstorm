package ai.flowstorm.core.type.profile

import ai.flowstorm.core.type.Extendable

class Habits(
    var remembersDreams: Boolean? = null // is true if user remembers dreams
) : Extendable()