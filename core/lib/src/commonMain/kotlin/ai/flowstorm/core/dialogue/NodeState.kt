package ai.flowstorm.core.dialogue

import ai.flowstorm.core.type.DateTime

open class NodeState(
    val id: Int,
    var isLocked: Boolean = false,
    var count: Int = 0,
    var lastTime: DateTime? = null
) {
    override fun hashCode() = id.hashCode()
    override fun toString() = "${this::class.simpleName}($id)"
}

