package ai.flowstorm.core.type

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.core.type.value.*
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.time.Month

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DucklingEntityTest {
    val mapper = ObjectUtil.defaultMapper

    private fun getInput(name: String) = mapper.readValue<List<DucklingEntity>>(this::class.java.getResourceAsStream(name))

    @Test
    fun testTime() {
        val res: List<DucklingEntity> = getInput("time.json")
        val res0 = res[0].value as GrainedTime
        assertEquals(6, res0.value.hour)
        assertEquals("+02:00", res0.value.zone.id)
        assertEquals("GrainedTime", res[0].className)
        // Alternatives
        assertEquals(3, res0.alternativeValues.size)
        assert(res0.alternativeValues.all { (it as GrainedTime).value.hour == 6 })

        val res1 = res[1].value as Interval<GrainedTime>
        assertEquals(17, res1.from!!.value.hour)
        assertEquals(21, res1.to!!.value.hour)
        // Alternatives
        assertEquals(1, res1.alternativeValues.size)
        assert(res1.alternativeValues.all { (it as Interval<GrainedTime>).from!!.value.hour == 17 })
        assert(res1.alternativeValues.all { (it as Interval<GrainedTime>).to!!.value.hour == 21 })

        val res2 = res[2].value as Interval<GrainedTime>
        assertEquals(Month.FEBRUARY, res2.from!!.value.month)
        assertNull(res2.to)
        // Alternatives
        assertEquals(1, res2.alternativeValues.size)
        assert(res2.alternativeValues.all { res2.from!!.value.month == Month.FEBRUARY })
        assert(res2.alternativeValues.all { res2.to == null })
    }

    @Test
    fun testQuantity() {
        val res: List<DucklingEntity> = getInput("quantity.json")
        assertEquals(2F, (res[0].value as Quantity).value)
        assertEquals("coffee", (res[0].value as Quantity).product)
        assertEquals("cup", (res[0].value as Quantity).unit)
        assertEquals("Quantity", res[0].className)

        assertEquals(3.5F, (res[1].value as Quantity).value)
        assertEquals("cup", (res[1].value as Quantity).unit)
    }

    @Test
    fun testAmountOfMoney() {
        val res: List<DucklingEntity> = getInput("amount-of-money.json")
        assertEquals("22".toBigDecimal(), (res[0].value as Currency).value)
        assertEquals("EUR", (res[0].value as Currency).unit)
        assertEquals("Currency", res[0].className)

        assertEquals("5".toBigDecimal(), (res[1].value as Currency).value)
        assertEquals("$", (res[1].value as Currency).unit)

        assertEquals("2".toBigDecimal(), (res[2].value as Interval<Currency>).from!!.value)
        assertEquals("$", (res[2].value as Interval<Currency>).from!!.unit)
        assertEquals("3".toBigDecimal(), (res[2].value as Interval<Currency>).to!!.value)
        assertEquals("$", (res[2].value as Interval<Currency>).to!!.unit)
    }

    @Test
    fun testDistance() {
        val res: List<DucklingEntity> = getInput("distance.json")
        assertEquals("6005".toBigDecimal(), (res[0].value as Distance).value)
        assertEquals("metre", (res[0].value as Distance).unit)
        assertEquals("Distance", res[0].className)
    }

    @Test
    fun testEmail() {
        val res: List<DucklingEntity> = getInput("email.json")
        assertEquals("dummy@flowstorm.ai", (res[0].value as Email).value)
        assertEquals("Email", res[0].className)
    }

    @Test
    fun testNumber() {
        val res: List<DucklingEntity> = getInput("number.json")
        assertEquals(22000F, (res[0].value as Numeric).value)
        assertEquals("Number", res[0].className)
    }

    @Test
    fun testOrdinal() {
        val res: List<DucklingEntity> = getInput("ordinal.json")
        assertEquals(3F, (res[0].value as Ordinal).value)
        assertEquals("Ordinal", res[0].className)
    }

    @Test
    fun testPhoneNumber() {
        val res: List<DucklingEntity> = getInput("phone-number.json")
        assertEquals("420777777777", (res[0].value as Phone).value)
        assertEquals("Phone", res[0].className)
    }

    @Test
    fun testTemperature() {
        val res: List<DucklingEntity> = getInput("temperature.json")
        assertEquals("34".toBigDecimal(), (res[0].value as Temperature).value)
        assertEquals("celsius", (res[0].value as Temperature).unit)
        assertEquals("Temperature", res[0].className)
    }

    @Test
    fun testURL() {
        val res: List<DucklingEntity> = getInput("url.json")
        assertEquals("www.flowstorm.ai/info", (res[0].value as URL).value)
        assertEquals("flowstorm.ai", (res[0].value as URL).domain)
        assertEquals("URL", res[0].className)
    }

    @Test
    fun testVolume() {
        val res: List<DucklingEntity> = getInput("volume.json")
        assertEquals("4".toBigDecimal(), (res[0].value as Volume).value)
        assertEquals("litre", (res[0].value as Volume).unit)
        assertEquals("Volume", res[0].className)
    }
}