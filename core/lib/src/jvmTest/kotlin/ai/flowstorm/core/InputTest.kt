package ai.flowstorm.core

import ai.flowstorm.common.ObjectUtil
import ai.flowstorm.core.type.DucklingEntity
import ai.flowstorm.core.type.DucklingEntityTest
import ai.flowstorm.core.type.value.GrainedTime
import ai.flowstorm.core.type.value.Interval
import com.fasterxml.jackson.module.kotlin.readValue
import io.mockk.every
import io.mockk.mockkClass
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InputTest {

    @Test
    fun `input`() {
        val intentName = "-1"
        val input = input("I see a dog, a red rose and a cat.").apply {
            classes += Input.Class(Input.Class.Type.Intent, intentName)
            tokens += Input.Word("i")
            tokens += Input.Word("see")
            tokens += Input.Word("dog", mutableListOf(Input.Class(Input.Class.Type.Entity, "animal")))
            tokens += Input.Punctuation(",")
            tokens += Input.Word("red", mutableListOf(Input.Class(Input.Class.Type.Entity, "B-flower")))
            tokens += Input.Word("rose", mutableListOf(Input.Class(Input.Class.Type.Entity, "I-flower")))
            tokens += Input.Word("and")
            tokens += Input.Word("cat", mutableListOf(Input.Class(Input.Class.Type.Entity, "animal")))
            tokens += Input.Punctuation(".")
        }
        input.entityMap.putAll(InputEntity.fromAnnotation(input.words))

        assertEquals(intentName, input.intent.name)
        assertEquals(2, input.entityMap.size)
        assertEquals(listOf("dog", "cat"), input.entities("animal").map { it.text })
    }

    @Test
    fun testMultiModelEntity() {
        val intentName = "-1"
        val input = input("I saw president Barack Obama, Michelle Obama and their dog.").apply {
            classes += Input.Class(Input.Class.Type.Intent, intentName)
            tokens += Input.Word("i")
            tokens += Input.Word("saw")
            tokens += Input.Word(
                "president",
                mutableListOf(Input.Class(Input.Class.Type.Entity, "B-PER", model_id = "model2"))
            )
            tokens += Input.Word(
                "barack", mutableListOf(
                    Input.Class(Input.Class.Type.Entity, "B-PER", model_id = "model1"),
                    Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model2")
                )
            )
            tokens += Input.Word(
                "obama", mutableListOf(
                    Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model1"),
                    Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model2")
                )
            )
            tokens += Input.Punctuation(",")
            tokens += Input.Word(
                "michelle",
                mutableListOf(Input.Class(Input.Class.Type.Entity, "B-PER", model_id = "model1"))
            )
            tokens += Input.Word("obama", mutableListOf(Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model1")))
            tokens += Input.Word("and")
            tokens += Input.Word("their")
            tokens += Input.Word("dog", mutableListOf(Input.Class(Input.Class.Type.Entity, "animal", model_id = "model2")))
            tokens += Input.Punctuation(".")
        }
        input.entityMap.putAll(InputEntity.fromAnnotation(input.words))

        assertEquals(intentName, input.intent.name)
        assertEquals(2, input.entityMap.size)
        assertEquals(listOf("dog"), input.entities("animal").map { it.text })
        assertEquals(listOf("president barack obama", "barack obama", "michelle obama"), input.entities("PER").map { it.text })
    }

    @Test
    fun testInvalidEntityAnnotation() {
        val intentName = "-1"
        val input = input("I saw president Barack Obama and Michelle Obama.").apply {
            classes += Input.Class(Input.Class.Type.Intent, intentName)
            tokens += Input.Word("i")
            tokens += Input.Word("saw")
            tokens += Input.Word("president")
            tokens += Input.Word(
                "barack", mutableListOf(
                    Input.Class(Input.Class.Type.Entity, "B-PER", model_id = "model1"),
                    Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model2")
                )
            )
            tokens += Input.Word(
                "obama", mutableListOf(
                    Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model1"),
                    Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model2")
                )
            )
            tokens += Input.Word("and")
            tokens += Input.Word(
                "michelle",
                mutableListOf(Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model1"))
            )
            tokens += Input.Word("obama", mutableListOf(Input.Class(Input.Class.Type.Entity, "I-PER", model_id = "model1")))
            tokens += Input.Punctuation(".")
        }
        input.entityMap.putAll(InputEntity.fromAnnotation(input.words))

        assertEquals(intentName, input.intent.name)
        assertEquals(1, input.entityMap.size)
        assertEquals(listOf("barack obama", "barack obama", "michelle obama"), input.entities("PER").map { it.text })
    }

    @Test
    fun testInvalidEntityAnnotation2() {
        val intentName = "-1"
        val input = input("I saw president Barack Obama.").apply {
            classes += Input.Class(Input.Class.Type.Intent, intentName)
            tokens += Input.Word("i")
            tokens += Input.Word("saw")
            tokens += Input.Word("president")
            tokens += Input.Word("barack", mutableListOf(Input.Class(Input.Class.Type.Entity, "B-PER", model_id = "model1")))
            tokens += Input.Word("obama", mutableListOf(Input.Class(Input.Class.Type.Entity, "I-LOC", model_id = "model1")))
            tokens += Input.Punctuation(".")
        }
        input.entityMap.putAll(InputEntity.fromAnnotation(input.words))
        
        assertEquals(intentName, input.intent.name)
        assertEquals(2, input.entityMap.size)
        assertEquals(listOf("barack"), input.entities("PER").map { it.text })
        assertEquals(listOf("obama"), input.entities("LOC").map { it.text })
    }

    @Test
    fun testGetIntervalEntities() {
        val entityMap = mutableMapOf<String, MutableList<InputEntity>>()
        ObjectUtil.defaultMapper.readValue<List<DucklingEntity>>(DucklingEntityTest::class.java.getResourceAsStream("time.json")).forEach { entity ->
            entityMap.getOrPut(entity.className) { mutableListOf() }.add(entity)
        }
        val input = mockkClass(Input::class)
        every { input.entityMap } returns entityMap

        val intervals = input.entities<Interval<GrainedTime>>()
        val times = input.entities<GrainedTime>()
        assertEquals(entityMap["GrainedTime"]!!.subList(0, 1).map { it.value }, times)
        assertEquals(entityMap["GrainedTime"]!!.subList(1, 3).map { it.value }, intervals)
    }
}