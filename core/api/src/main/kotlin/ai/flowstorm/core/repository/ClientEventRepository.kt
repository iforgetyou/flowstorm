package ai.flowstorm.core.repository

import ai.flowstorm.common.repository.EntityRepository
import ai.flowstorm.core.model.ClientEvent

interface ClientEventRepository : EntityRepository<ClientEvent>