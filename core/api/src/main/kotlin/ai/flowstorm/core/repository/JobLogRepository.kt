package ai.flowstorm.core.repository

import ai.flowstorm.common.repository.EntityRepository
import ai.flowstorm.core.model.JobLog

interface JobLogRepository : EntityRepository<JobLog> {
    fun findBy(name: String, status: JobLog.Status): JobLog?
}