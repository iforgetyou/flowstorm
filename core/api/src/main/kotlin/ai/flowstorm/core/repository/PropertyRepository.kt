package ai.flowstorm.core.repository

import ai.flowstorm.common.repository.EntityRepository
import ai.flowstorm.core.model.Property

interface PropertyRepository : EntityRepository<Property> {
    fun getValues(key: String): List<String>
    fun getValue(key: String) = getValues(key).firstOrNull()
}