package ai.flowstorm.core.resources

import ai.flowstorm.core.model.*
import ai.flowstorm.core.type.MutablePropertyMap
import io.swagger.annotations.Api
import java.util.*
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(tags = ["Content Distribution"])
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
interface ContentDistributionResource {

    data class ContentRequest(
            val deviceId: String,
            val token: String?,
            val appKey: String,
            val language: String?
    )

    data class ContentResponse(
            val application: Application,
            val locale: Locale,
            val space: Space,
            val device: Device,
            val user: User,
            val test: Boolean,
            val hasConsent: Boolean,
            val sessionProperties: MutablePropertyMap
    )

    @POST
    fun resolve(contentRequest: ContentRequest): ContentResponse
}