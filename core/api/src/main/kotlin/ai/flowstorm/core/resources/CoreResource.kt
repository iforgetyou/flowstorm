package ai.flowstorm.core.resources

import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import ai.flowstorm.core.Request
import ai.flowstorm.core.Response
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(description = "Core Service")
interface CoreResource {
    @PUT
    @Path("process")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun process(@ApiParam("Request", required = true) request: Request): Response

    @GET
    @Path("twilio/text/{appKey}")
    @Produces(MediaType.APPLICATION_XML)
    fun twilioText(
            @ApiParam(required = true) @PathParam("appKey") appKey: String,
            @ApiParam @QueryParam("Body") text: String,
            @ApiParam @QueryParam("From") deviceId: String,
            @ApiParam @QueryParam("FromZip") zip: String?,
            @ApiParam @QueryParam("FromCity") city: String?,
            @ApiParam @QueryParam("FromState") state: String?,
            @ApiParam @QueryParam("FromCountry") country: String
    ): String

    @GET
    @Path("twilio/call/{appKey}")
    @Produces(MediaType.APPLICATION_XML)
    fun twilioCall(
            @ApiParam(required = true) @PathParam("appKey") appKey: String,
            @ApiParam @QueryParam("From") deviceId: String?,
            @ApiParam @QueryParam("FromZip") zip: String?,
            @ApiParam @QueryParam("FromCity") city: String?,
            @ApiParam @QueryParam("FromState") state: String?,
            @ApiParam @QueryParam("FromCountry") country: String?
    ): String
}