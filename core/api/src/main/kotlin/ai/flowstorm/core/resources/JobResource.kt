package ai.flowstorm.core.resources

import ai.flowstorm.core.model.JobLog
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Api(description = "Job")
@Produces(MediaType.APPLICATION_JSON)
interface JobResource {

    @GET
    @Path("/run/{name}")
    fun run(
        @ApiParam(required = true) @PathParam("name")  name: String
    ): JobLog?

    @GET
    @Path("/runAll")
    fun runAll(): List<JobLog>

}