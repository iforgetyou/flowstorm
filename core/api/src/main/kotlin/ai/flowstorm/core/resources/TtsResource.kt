package ai.flowstorm.core.resources

import ai.flowstorm.core.AudioFileType
import ai.flowstorm.nlp.tts.TtsRequest
import ai.flowstorm.nlp.tts.TtsResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiParam
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Api(description = "TTS")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
interface TtsResource {

    @POST
    @Path("synthesize")
    fun synthesize(
        @ApiParam(required = true) request: TtsRequest
    ): TtsResponse

    @POST
    @Path("synthesize/{fileType}")
    fun synthesizeFormat(
        @ApiParam(required = true) request: TtsRequest,
        @ApiParam(required = true) @PathParam("fileType") fileType: AudioFileType
    ): TtsResponse
}
