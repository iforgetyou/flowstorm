group = "ai.flowstorm.core"
description = "Flowstorm Core API"
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String

plugins {
    id("ai.flowstorm.kotlin-jvm")
}

dependencies {
    api(project(":flowstorm-nlp-tts-api"))
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")

}

