# Flowstorm Conversational AI Platform Open-Source Project

![flowstorm-color](https://user-images.githubusercontent.com/10587131/128861488-797688ed-063a-414b-a861-be1b507e02f6.png)

## CREATE DIGITAL PERSONAS NOT SIMPLE BOTS!
[Flowstorm](https://www.flowstorm.ai/) is a low-code platform for the development of digital personas and advanced conversational AI applications.

Thanks to a user-friendly interface, a set of reusable assets and other tools, Flowstorm opens up new horizons for developers, conversation designers, and all other creatives.

Developed by [PromethistAI](https://promethist.ai/).

## Get Started
- **Use cloud platform:** [Flowstorm](https://www.flowstorm.ai/)
- **Watch quick start guide:** [Create a Voice app in 3 minutes](https://www.youtube.com/watch?v=Bo1vqkJJ2xE&t=49s&ab_channel=Flowstorm)
- **Read the docs:**  [Documentation](https://docs.flowstorm.ai/)
- **Learn from the blog:** [Flowstorm Blog](https://medium.com/flowstorm)
- **Make an issue:** [Issue Tracking](https://promethist.myjetbrains.com/youtrack/issues)


## Follow Us

- [Facebook](https://www.facebook.com/FlowstormAI)
- [Instagram](https://www.instagram.com/flowstorm.ai/)
- [Twitter](https://twitter.com/flowstormAI)
- [LinkedIn](https://www.linkedin.com/company/flowstorm/)
- [Reddit](https://www.reddit.com/r/flowstorm_ai/)
- [YouTube](https://www.youtube.com/channel/UCMuvPwdskGcPHKs2UduaECg)


## Licence
[Apache License 2.0](https://gitlab.com/promethistai/flowstorm/blob/master/LICENSE.txt)

## Project Structure
| Directory                                 | Description |
|-------------------------------------------|----------------------------|
| [`common`](common)                        | Flowstorm common modules |
| [`common/lib`](common/lib)                | Common library |
| [`common/api`](common/api)                | REST Client |
| [`common/app`](common/app)                | Server application fundaments |
| [`core`](core)                            | Platform core modules |
| [`core/lib`](core/lib)                    | Core library (basic platform classes used across other modules) |
| [`core/api`](core/api)                    | Core API (runtime and builder resource interfaces) |
| [`core/app`](core/app)                    | Core application, providing runtime and builder web resources |
| [`client`](client)                        | Platform clients |
| [`client/lib`](client/lib)                | Client library |
| [`client/app`](client/app)                | Client app |
| [`client/web`](client/web)                | Web client |
| [`channel`](channel)                      | Client channel implementations |
| [`channel/api`](channel/api)              | Channel API (used by channel socket clients) |
| [`channel/socket`](channel/socket)        | Web socket channel (currently incl. call socket for Twilio inbound calls) |
| [`channel/alexa`](channel/alexa)          | Amazon Alexa channel |
| [`channel/google`](channel/google)        | Google Assistant channel |
| [`storage`](storage)                      | File storage services |
| [`storage/local`](storage/local)          | Local file storage |
| [`storage/google`](storage/google)        | Google storage |
| [`storage/aws`](storage/aws)              | Amazon S3 storage |
| [`storage/azure`](storage/azure)          | Azure Blob storage (TODO) |
| [`nlp`](nlp)                              | NLP services |
| [`nlp/pipeline`](nlp/pipeline)            | NLP pipeline implementation (incl. internal tokenizer and adapter components for external NLP services - IR, NER, sentiment detection) |
| [`nlp/dm`](nlp/dm)                        | Dialogue Manager |
| [`nlp/stt`](nlp/stt)                      | Speech-To-Text NLP components |
| [`nlp/stt/api`](nlp/stt/api)              | Speech-To-Text API |
| [`nlp/stt/google`](nlp/stt/google)        | Google STT adapter component |
| [`nlp/stt/microsoft`](nlp/stt/microsoft)  | Microsoft STT adapter component |
| [`nlp/tts`](nlp/tts)                      | Text-To-Speech NLP components |
| [`nlp/tts/api`](nlp/tts/api)              | Text-To-Speech API |
| [`nlp/tts/google`](nlp/tts/google)        | Google TTS adapter component |
| [`nlp/tts/amazon`](nlp/tts/amazon)        | Amazon Polly TTS adapter component |
| [`nlp/tts/microsoft`](nlp/tts/microsoft)  | Microsoft TTS adapter component |
| [`connector`](connector)                  | 3rd party API connectors |
| [`connector/lib`](connector/lib)          | Existing dialogue API connectors (TODO separate them + redefine Dialogue Connector API!) |

## Multiplatform support

Most of platform modules are JVM only. Several modules listed below are Kotlin multiplatform. Multiplatform support is however still under construction. 


* common-lib
* core-lib 
* client-lib
* channel-api
* client-app
* studio-app

## Ops notes

### How to setup kubernetes environment
```
kubectl -n develop delete secret google-sa --ignore-not-found
kubectl -n develop create secret generic google-sa --from-file=key.json=google.local.key

kubectl -n develop delete secret flowstorm --ignore-not-found
kubectl -n develop create secret generic flowstorm --from-file=etc/develop/app.local.properties
kubectl -n preview create secret generic flowstorm --from-file=etc/preview/app.local.properties
kubectl -n default create secret generic flowstorm --from-file=etc/production/app.local.properties

kubectl -n develop create secret generic illusionist-local --from-file=etc/develop/illusionist/config.local.yaml
kubectl -n preview create secret generic illusionist-local --from-file=etc/preview/illusionist/config.local.yaml
kubectl -n default create secret generic illusionist-local --from-file=etc/production/illusionist/config.local.yaml
```

### How to deploy
```
helm repo add flowstorm https://repository.flowstorm.ai/helm

helm upgrade --install promethistai/flowstorm-app (--set app.image.tag=x.y)
helm upgrade --install promethistai/flowstorm-studio (--set app.image.tag=x.y)
helm upgrade --install promethistai/illusionist (--set app.image.tag=x.y)
helm upgrade --install promethistai/duckling (--set app.image.tag=x.y)

# preview
helm -n preview repo add flowstorm https://repository.flowstorm.ai/helm

helm upgrade --install promethistai/flowstorm-app -n preview --recreate-pods --set app.image.tag=snapshot --set imagePullPolicy=Always
helm upgrade --install promethistai/flowstorm-studio -n preview --recreate-pods --set app.image.tag=snapshot --set imagePullPolicy=Always
helm upgrade --install promethistai/illusionist -n preview --recreate-pods --set imagePullPolicy=Always
helm upgrade --install promethistai/duckling -n preview --recreate-pods --set imagePullPolicy=Always